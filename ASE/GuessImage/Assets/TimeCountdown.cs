﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCountdown : MonoBehaviour {

	public float CountdownTime;

	public Text timeText;

	private bool gameover = false;

	// Use this for initialization
	void Start () {
		CountdownTime = Time.time + 20;
	}
	
	// Update is called once per frame
	void Update () {
		float countdownValue = Mathf.Round (CountdownTime - Time.time);

		if (countdownValue > 0)
			timeText.text = countdownValue.ToString ();
		else {
			timeText.text = "0";
			if (!gameover) {
				GetComponent<GridManager> ().GameOver ();
				gameover = true;
			}
		}
	}
}
