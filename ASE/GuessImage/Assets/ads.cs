﻿using UnityEngine;
using UnityEngine.Advertisements;

public class ads : MonoBehaviour
{

	public float minPlayTimeBeforeAd;
	public float timeBetweenAds;

	void Start()
	{
		float currentTIme = Time.time;
		string lastAdTimeKey = "LastAdTime";

		if (!PlayerPrefs.HasKey (lastAdTimeKey)) //No Key, create one
			PlayerPrefs.SetFloat (lastAdTimeKey, currentTIme - timeBetweenAds);

		if (currentTIme < minPlayTimeBeforeAd && currentTIme < PlayerPrefs.GetFloat (lastAdTimeKey)) //Need to reset key for this session
			PlayerPrefs.SetFloat (lastAdTimeKey, currentTIme - timeBetweenAds);

		if (currentTIme > minPlayTimeBeforeAd && currentTIme - PlayerPrefs.GetFloat (lastAdTimeKey) > timeBetweenAds) { //Time to play an ad
			PlayerPrefs.SetFloat (lastAdTimeKey, currentTIme);
			ShowAd ();
		}
	}

	public void ShowAd()
	{
		if (Advertisement.IsReady())
		{
			Advertisement.Show();
		}
	}
}