﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectShake : MonoBehaviour {

	void Start(){
		Input.gyro.enabled = true;
	}

	void Update () {

		if (Input.gyro.userAcceleration.sqrMagnitude > 1.75f)
			GetComponent<GridManager> ().increaseCubes ();
	}
}
