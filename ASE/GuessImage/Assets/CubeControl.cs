﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeControl : MonoBehaviour {

	void OnEnable()
	{
		GridManager.StopMovingCubes += StopMoving;
		GridManager.ClearAllCubes += DestroyCube;
	}

	void OnDisable()
	{
		GridManager.StopMovingCubes -= StopMoving;
		GridManager.ClearAllCubes -= DestroyCube;
	}

	void StopMoving()
	{
		GetComponent<Rigidbody> ().isKinematic = true;
		transform.position = new Vector3(transform.position.x, 10, transform.position.z);
	}

	void DestroyCube()
	{
		Destroy (gameObject);
	}
}
