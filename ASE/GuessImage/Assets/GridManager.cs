﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridManager : MonoBehaviour {

	//Passing commands to cubes
	public delegate void delegateFunction ();
	public static event delegateFunction StopMovingCubes;
	public static event delegateFunction ClearAllCubes;

	//cube prefab
	public GameObject cube;
	public Material cubeMat;

	//List of images
	public List<Texture> images;

	private int size;
	private int maxSize = 26;

	//Game random elements
	public Texture gameImage;
	public List<string> options = new List<string> ();

	//Buton text
	public Text optionText1;
	public Text optionText2;
	public Text optionText3;
	public Text optionText4;

	public Text resultText;

	public GameObject gameoverUI;
	public GameObject buttonsUI;

	public Camera cam;

	void Start()
	{
		size = 2;

		setImagesAndOptions ();
		increaseCubes ();
	}

	void setImagesAndOptions()
	{
		options.Clear ();

		gameImage = getRandomImage ();

		options.Add (gameImage.name);

		while (options.Count < 4) {
			string testOptions = getRandomImage ().name;
			if (!options.Contains (testOptions))
				options.Add (testOptions);
		}

		options.Sort ();

		optionText1.text = options [0].ToUpper ();
		optionText2.text = options [1].ToUpper ();
		optionText3.text = options [2].ToUpper ();
		optionText4.text = options [3].ToUpper ();
	}

	public void increaseCubes()
	{
		if (size < maxSize) {
			if(transform.childCount > 0)
				ClearAllCubes ();

			size += 2;

			spawnCubesBasedOnSize ();
		}
	}

	Texture getRandomImage()
	{
		int x = Random.Range (0, images.Count);
		return images [x];
	}

	void spawnCubesBasedOnSize()
	{
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				spawnCube (j, i);
			}
		}

		//adjust camera
		cam.transform.position = new Vector3 ((size - 1) / 2.0f + 0.5f, size + 20, ((size - 1) / 2.0f) - 3);
	}

	void spawnCube(int column, int row)
	{
		Texture2D pixelTest = gameImage as Texture2D;
		int x = Mathf.FloorToInt (column * 1.0f / size * pixelTest.width);
		int y = Mathf.FloorToInt (row * 1.0f / size * pixelTest.height);

		if (pixelTest.GetPixel (x, y).a > 0.6f) {
			GameObject newCube = Instantiate (cube);
			newCube.transform.SetParent (transform);
			newCube.transform.position = new Vector3 (column, Random.Range (1, 12), row);

			newCube.GetComponent<Rigidbody> ().velocity = new Vector3(0, Random.Range (5, 10), 0);

			newCube.GetComponent<Renderer> ().material.SetTexture ("_image", gameImage);
			newCube.GetComponent<Renderer> ().material.SetFloat ("_Uscale", column * 1.0f / size);
			newCube.GetComponent<Renderer> ().material.SetFloat ("_Vscale", row * 1.0f / size);
		}
	}

	public void GameOver()
	{
		size = maxSize - 2;
		increaseCubes ();
		StopMovingCubes ();
		gameoverUI.SetActive (true);
		buttonsUI.SetActive (false);
	}

	public void ButtonCheck(Text t)
	{
		if (gameImage.name.ToUpper () == t.text)
			resultText.text = "YOU WIN";
		else
			resultText.text = "YOU LOSE";

		GameOver ();
	}
}
