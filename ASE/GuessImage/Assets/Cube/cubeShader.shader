// Shader created with Shader Forge v1.34 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.34;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:32913,y:32567,varname:node_4013,prsc:2|emission-1241-RGB,alpha-7880-OUT;n:type:ShaderForge.SFN_Tex2d,id:1241,x:32475,y:32658,varname:node_1241,prsc:2,tex:e7477a4512e994ae796da6bfa7b326ad,ntxv:0,isnm:False|UVIN-2047-OUT,TEX-1881-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:1881,x:32083,y:32851,ptovrint:False,ptlb:image,ptin:_image,varname:node_1881,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:e7477a4512e994ae796da6bfa7b326ad,ntxv:0,isnm:False;n:type:ShaderForge.SFN_ValueProperty,id:8324,x:32067,y:32469,ptovrint:False,ptlb:Uscale,ptin:_Uscale,varname:node_8324,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_ValueProperty,id:4488,x:32067,y:32597,ptovrint:False,ptlb:Vscale,ptin:_Vscale,varname:_Uscale_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Append,id:2047,x:32245,y:32513,varname:node_2047,prsc:2|A-8324-OUT,B-4488-OUT;n:type:ShaderForge.SFN_If,id:7880,x:32664,y:32822,varname:node_7880,prsc:2|A-1241-A,B-891-OUT,GT-5276-OUT,EQ-5276-OUT,LT-8823-OUT;n:type:ShaderForge.SFN_Vector1,id:891,x:32460,y:32856,varname:node_891,prsc:2,v1:0.6;n:type:ShaderForge.SFN_Vector1,id:5276,x:32460,y:32919,varname:node_5276,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:8823,x:32475,y:32994,varname:node_8823,prsc:2,v1:0;proporder:1881-8324-4488;pass:END;sub:END;*/

Shader "Shader Forge/cubeMat" {
    Properties {
        _image ("image", 2D) = "white" {}
        _Uscale ("Uscale", Float ) = 0.5
        _Vscale ("Vscale", Float ) = 0.5
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform sampler2D _image; uniform float4 _image_ST;
            uniform float _Uscale;
            uniform float _Vscale;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_FOG_COORDS(0)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float2 node_2047 = float2(_Uscale,_Vscale);
                float4 node_1241 = tex2D(_image,TRANSFORM_TEX(node_2047, _image));
                float3 emissive = node_1241.rgb;
                float3 finalColor = emissive;
                float node_7880_if_leA = step(node_1241.a,0.6);
                float node_7880_if_leB = step(0.6,node_1241.a);
                float node_5276 = 1.0;
                fixed4 finalRGBA = fixed4(finalColor,lerp((node_7880_if_leA*0.0)+(node_7880_if_leB*node_5276),node_5276,node_7880_if_leA*node_7880_if_leB));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
