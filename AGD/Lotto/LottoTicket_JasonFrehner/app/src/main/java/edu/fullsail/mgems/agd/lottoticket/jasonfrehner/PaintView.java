package edu.fullsail.mgems.agd.lottoticket.jasonfrehner;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;

/**
 * Created by jasonfrehner on 1/22/17.
 * http://stackoverflow.com/questions/31517647/how-to-erase-image-with-finger-in-android/31533178#31533178
 */


public class PaintView extends View implements View.OnTouchListener {

    Bitmap Bitmap1, Bitmap2;
    Bitmap Transparent;
    int X = -100;
    int Y = -100;
    int width;
    Canvas c2;
    private boolean isTouched = false;
    Paint paint = new Paint();
    Path drawPath = new Path();
    public boolean locked = false;
    public ArrayList<PaintView> neighbors = new ArrayList<>();
    MediaPlayer scratchSound, winSound, loseSound;
    float scratchTime = 0;
    private boolean winningSpace = false;
    private boolean resultSoundPlayed = false;

    public  PaintView(Context context)
    {
        super(context);
    }

    public PaintView(Context context, int x, int y, boolean winner)
    {
        super(context);

        setFocusable(true);
        setFocusableInTouchMode(true);
        this.setOnTouchListener(this);
        this.setY(y/4);
        width = x;

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        Transparent = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        if(winner)
        {
            Bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.die);
            winningSpace = true;
        }
        else
            Bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.die1);
        Bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.scratch);

        c2 = new Canvas();
        c2.setBitmap(Transparent);
        //c2.drawBitmap(Bitmap2, 0, 0, paint);
        c2.drawBitmap(Bitmap2,null, new Rect(0,0,x,x), null);

        paint.setAlpha(0);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        paint.setAntiAlias(true);

        scratchSound = MediaPlayer.create(context, R.raw.dice);
        scratchSound.setLooping(true);
        winSound = MediaPlayer.create(context, R.raw.aha);
        loseSound = MediaPlayer.create(context, R.raw.groan);



    }


    @Override
    protected void onDraw(Canvas canvas)
    {

        if(isTouched)
        {
            canvas.drawBitmap(Bitmap1, null, new Rect(0,0,width,width), null);

        }
        canvas.drawBitmap(Transparent, 0, 0, null);

        if(!resultSoundPlayed)
        {
            if (scratchTime > 100.0)
            {
                if (winningSpace)
                {
                    winSound.start();
                    MainActivity.resultText(true);
                }
                else
                {
                    loseSound.start();
                    MainActivity.resultText(false);
                }
                resultSoundPlayed = true;
            }
        }

    }


    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        int time = (int) (System.currentTimeMillis());
        if(!locked)
        {
            for(int i = 0; i < neighbors.size(); i++)
            {
                neighbors.get(i).locked = true;
            }

            isTouched = true;
            X = (int) event.getX();
            Y = (int) event.getY();

            paint.setStrokeWidth(60);

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    drawPath.moveTo(X, Y);
                    c2.drawPath(drawPath, paint);
                    scratchSound.start();
                    time = (int) (System.currentTimeMillis());
                    break;
                case MotionEvent.ACTION_MOVE:
                    drawPath.lineTo(X, Y);
                    c2.drawPath(drawPath, paint);
                    scratchTime += (int) (System.currentTimeMillis()) - time;
                    time = (int) (System.currentTimeMillis());
                    break;
                case MotionEvent.ACTION_UP:
                    drawPath.lineTo(X, Y);
                    c2.drawPath(drawPath, paint);
                    drawPath.reset();
                    scratchSound.pause();
                    scratchTime += (int) (System.currentTimeMillis()) - time;
                    //count=0;
                    break;
                default:
                    return false;
            }
        }
        //Log.d("Scratch Time", Float.toString(scratchTime));
        invalidate();
        return true;
    }

}