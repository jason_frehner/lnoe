package edu.fullsail.mgems.agd.lottoticket.jasonfrehner;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener
{
    private ViewGroup mContentView;
    private ViewGroup mOne, mTwo, mThree;
    private int mScreenWidth, mScreenHeight;
    public PaintView[] cells;
    private MediaPlayer music;
    public static int state = 0;
    public static TextView t;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Load activity Layout
        setContentView(R.layout.activity_main);

        //Get content reference
        mContentView = (ViewGroup) findViewById(R.id.contentView);
        mOne = (ViewGroup) findViewById(R.id.one);
        mTwo = (ViewGroup) findViewById(R.id.two);
        mThree = (ViewGroup) findViewById(R.id.three);
        if(mContentView == null) throw new AssertionError();

        music = MediaPlayer.create(this, R.raw.music);
        music.setLooping(true);
        music.setVolume(50,50);

        t = (TextView) findViewById(R.id.result);

        //Wait for layout to be drawn
        ViewTreeObserver viewTreeObserver = mContentView.getViewTreeObserver();
        if(viewTreeObserver.isAlive())
        {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
            {
                @Override
                public void onGlobalLayout()
                {
                    //Get content size
                    mContentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    mScreenWidth = mContentView.getWidth();
                    mScreenHeight = mContentView.getHeight();

                    //what to do after screen is loaded
                    createScratch();
                    music.start();

                }
            });
        }
    }

    public void createScratch()
    {
        int randomSpace = (int) (Math.random() * 3);
        Log.d("Winning Space", Integer.toString(randomSpace));

        //Create and draw cells
        cells = new PaintView[3];
        cells[0] = new PaintView(this, mScreenWidth / 3, mScreenHeight / 2, 0 == randomSpace);
        cells[1] = new PaintView(this, mScreenWidth / 3, mScreenHeight / 2, 1 == randomSpace);
        cells[2] = new PaintView(this, mScreenWidth / 3, mScreenHeight / 2, 2 == randomSpace);
        mOne.addView(cells[0]);
        mTwo.addView(cells[1]);
        mThree.addView(cells[2]);

        //Connect cells
        for (int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 3; j++)
            {
                if(j != i)
                {
                    cells[i].neighbors.add(cells[j]);
                }
            }
        }
    }


    //Credits dialog
    public void credits(View view)
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Made by");
        dialog.setMessage(R.string.nameDate);
        dialog.setPositiveButton(" OK ", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //Reset game
    public void restart(View view)
    {
        music.stop();
        cells = new PaintView[0];
        this.finish();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

        //recreate();
    }

    public static void resultText(boolean win)
    {
        if (win)
            t.setText("You Win! Claim your dice.");
        else
            t.setText("Sorry, better luck next time.");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Runtime.getRuntime().gc();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        if(event.getAction() == MotionEvent.ACTION_DOWN)
        {
            if(state == 1)
                resultText(true);
            else if(state == 2)
                resultText(false);
        }
        Log.d("State", Integer.toString(state));
        return false;
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        music.pause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        music.start();
    }
}
