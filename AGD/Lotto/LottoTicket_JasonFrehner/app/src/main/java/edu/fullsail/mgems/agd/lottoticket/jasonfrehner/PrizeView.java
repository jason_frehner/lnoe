package edu.fullsail.mgems.agd.lottoticket.jasonfrehner;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by jasonfrehner on 1/21/17.
 */

public class PrizeView extends ImageView implements View.OnTouchListener
{
    public Bitmap resin, prize;
    public Paint alphaPaint;
    private Canvas mCanvas;
    private boolean isTouched;
    Paint paint = new Paint();
    Path drawPath = new Path();
    Canvas c2;

    public PrizeView(Context context)
    {
        super(context);
    }

    public PrizeView(Context context, int x, int y)
    {
        super(context);
        //this.setX(0);
        this.setY(y/3);
        Log.d("Position", Integer.toString(x) + " " + Integer.toString(y));

        //this.setImageResource(R.drawable.scratch);

        prize = BitmapFactory.decodeResource(getResources(), R.drawable.die);

        Bitmap resinOrig = BitmapFactory.decodeResource(getResources(), R.drawable.scratch);
        resin = Bitmap.createBitmap(resinOrig.getWidth(),resinOrig.getHeight(), Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(resin);
        mCanvas.drawBitmap(resinOrig,null, new Rect(0,0,x,x), null);

        //ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(x,x);
        //setLayoutParams(params);

        setOnTouchListener(this);


    }
    public void init() {
        //getHolder().addCallback(this);
        setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        /*alphaPaint = new Paint();
        alphaPaint.setStyle(Paint.Style.FILL);
        alphaPaint.setARGB(255,0,0,0);
        alphaPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        Canvas can = new Canvas(resin);
        can.drawCircle(event.getX(), event.getY(), 10, alphaPaint);
        */

        /*isTouched = true;
        int x = (int) event.getX();
        int y = (int) event.getY();

        paint.setStrokeWidth(60);

        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(x, y);
                c2.drawPath(drawPath, paint);
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(x, y);
                c2.drawPath(drawPath, paint);
                break;
            case MotionEvent.ACTION_UP:
                drawPath.lineTo(x,y);
                c2.drawPath(drawPath, paint);
                drawPath.reset();
                break;
            default:
                return false;

        }

        invalidate();*/
        return true;
    }



}
