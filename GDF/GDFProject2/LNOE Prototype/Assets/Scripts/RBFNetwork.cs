﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

//RBF Network Based on pseudo code in Artificial intelligence for humans

/// <summary>
/// For each output
/// 	for each input
/// 		the sum of all
/// 			output weight * gaussian of (distance from((vector weight * vector point) - vector center)
/// 
///
/// 
/// gaussian = e^distance^2
/// </summary>
/// 
/// zombies killed, heroes killed, rounds done, target value
/// -1, -1, -1, 0    // 0,0,0,0  // start of game
/// 0.86, -1, -1, 0  // 14,0,0,0 // Heroes dominating
/// -1, 0, 0.86, 1   // 0,1,14,1 // Last turn, one hero killed

public class inputNode
{
	float value;
	List<float> weights;

	public inputNode(float v, float w1, float w2)
	{
		value = v;
		weights.Add (w1);
		weights.Add (w2);
	}
}

public class hiddenNode
{
	float value;
	float weight;
	float center;

	public hiddenNode(float v, float w)
	{
		value = v;
		weight = w;
	}
}

public class RBFNetwork
{
	/*public int inputCount;
	public int ouputCount;

	public int indexInputWeights;
	public int indexOutputWeights;

	public int longTermMemory;

	public List<RBFunction> rbf = new List<RBFunction>();*/

	public RBFNetwork()
	{
		
	}

	public void initRBF()
	{
		//3 inputs
		List<inputNode> inputs = new List<inputNode>();

		//3 hidden
		List<hiddenNode> hidden = new List<hiddenNode>();

		//1 output
		float output;


	}

	public void runRBF(List<inputNode> inputs)
	{
		//For each input
		for (int i = 0; i < inputs.Count; i++) {
			double sum = 0;

		}
	}


	/*public RBFNetwork initRBFNetwork(int inputCount, int rbfCount, int outputCount)
	{
		RBFNetwork result = new RBFNetwork ();
		result.inputCount = inputCount;
		result.ouputCount = outputCount;

		int inputWeightCount = inputCount * rbfCount;
		int outputWeightCount = (rbfCount + 1) * outputCount;
		int rbfParams = (inputCount + 1) * rbfCount;

		//allocate memory?

		result.indexInputWeights = 0;
		result.indexOutputWeights = inputWeightCount + rbfParams;

		for (int i = 0; i < rbfCount; i++) {
			int rbfIndex = inputWeightCount + ((inputCount + 1) * i);
			//result.rbf [i] = new GaussianFunction (inputCount, result.longTermMemory, rbfIndex);
		}

		return result;
	}

	public void computeRBFNetwork(float[] Input, RBFNetwork network)
	{
		float[] rbfOutput = new float[network.rbf.Count + 1];

		rbfOutput [rbfOutput.Length - 1] = 1.0f;

		for (int i = 0; i < network.rbf.Count; i++) {

		}
	}*/
}

public class RBFunction
{
	public RBFunction()
	{
		
	}
}

public class GaussianFunction
{
	public GaussianFunction (){
	} 

	public GaussianFunction(int inputCount, int longTermMemory, int rbfIndex)
	{
		
	}

	//Only One Diminsion value between 0 and 1
	public Double gaussian(float center, float position)
	{
		float r = Math.Abs (center - position);
		return Math.Exp (-Math.Pow (r, 2.0f));
	}
}

public class Normalization
{
	public Normalization (){
	}

	//betwen -1.0 and 1.0
	public float intNormalization (int x, int max)
	{
		float p = (x * 1.0f) / (max * 1.0f);

		return -1.0f + (2.0f * p);
	}
}


