﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//enum for what phase a card can be used.
public enum playDurningPhase {StartOfTurn, Immediately, Move, PreFight, FightAfterRoll, EndOfTurn};

//TODO know when remains in play is canceled.

#region Class for A zombie card.
public class ZombieCard
{
	public string name;
	public playDurningPhase phase;
	public string originalText;
	public string flavorText;
	public bool remainsInPlay = false;
	public ZombieTurn zmt = GameObject.Find ("ZombieManager").GetComponent<ZombieTurn> ();

	public Building building;
	public int newZombies;

	public ZombieCard()
	{
	}

	public virtual void cardSetup()
	{
	}

	public virtual void effect()
	{
		zmt.Decks.DiscardACard (zmt.cardBeingPlayed);
	}

	public virtual bool playable()
	{
		return true;
	}
}
#endregion

#region Basic zombie deck cards.

#region Start of turn cards.
public class Cornered : ZombieCard
{
	public Cornered()
	{
		name = "Cornered";
		phase = playDurningPhase.StartOfTurn;
		originalText = "Play this card at the start of a Zombie Turn. " +
		"Until the start of the next Zombie Turn, Zombies roll 2 extra fight dice but Heroes win on ties.";
		flavorText = "Good Lord!";
	}

	public override void cardSetup()
	{
		originalText = "Until the start of the next Zombie Turn, Zombies roll 2 extra fight dice but Heroes win on ties.";
	}

	public override void effect()
	{
		zmt.stats.FightDice += 2;
		zmt.ZombiesWinOnTie = false;

		zmt.Decks.DiscardACard (zmt.cardBeingPlayed);
	}
}

public class LightsOut : ZombieCard
{
	public LightsOut()
	{
		name = "Lights Out";
		phase = playDurningPhase.StartOfTurn;
		originalText = "Play this card at the start of a Zombie Turn on any building to place a Lights Out marker on it. " +
		"The Zombies have cut the power. " +
		"Any Hero moving into a space within this building immediately ends their turn.";
		remainsInPlay = true;
	}

	public override void cardSetup()
	{
		building = zmt.stats.mapTiles.GetRandomBuildingInGameTiles ();

		while(building.lightsOut)
			building = zmt.stats.mapTiles.GetRandomBuildingInGameTiles ();


		name = "Lights Out in " + building.name;
		originalText = "The Zombies have cut the power to " + building.name + ". \n" +
			"Any Hero moving into a space within this building immediately ends their turn.";
	}

	public override void effect()
	{
		building.lightsOut = true;

		zmt.Decks.DiscardACard (zmt.cardBeingPlayed);
	}
}

public class RelentlessAdvance : ZombieCard
{
	public RelentlessAdvance()
	{
		name = "Relentless Advance";
		phase = playDurningPhase.StartOfTurn;
		originalText = "Play at the start of a Zombie Turn. " +
		"Roll a D6. " +
		"Immediately move that many Zombies one space. " +
		"Those Zombies may move and fight normally this turn.";
		flavorText = "How do ya stop these things?!";
	}

	public override void cardSetup()
	{
		//TODO Choose from list of targets
		originalText = "Move " + zmt.action.RollD6 () + " Zombies one space towards closest Hero";
	}

	public override bool playable()
	{
		if (zmt.stats.ZombiesOnBoard > 0)
			return true;
		else
			return false;
	}
}
#endregion

#region Play immediately cards
//TODO visual reminder that this is in play
public class HeavyRain : ZombieCard
{
	public HeavyRain()
	{
		name = "Heavy Rain";
		phase = playDurningPhase.Immediately;
		originalText = "All Heroes who start their move outside of a building must subtract 1 from their Movement Dice roll (to a minimum of 1)";
		remainsInPlay = true;
	}
}

public class ATownOverrun : ZombieCard
{
	public ATownOverrun()
	{
		name = "A Town Overrun";
		phase = playDurningPhase.Immediately;
		originalText = "Immediately reveal and discard the top 10 Hero Cards. " +
		"If this discards the last Hero Card, the Heroes automatically lose.";
		flavorText = "What do we do now?";
	}
}

public class MyGodTheyveTakenThe : ZombieCard
{
	public MyGodTheyveTakenThe()
	{
		name = "My God, They've Taken the...";
		phase = playDurningPhase.Immediately;
		originalText = "Roll a Random Building and place a Taken Over maker on it. " +
		"Place a Zombie from the Zombie Pool in each empty space of this building. " +
		"No Hero may enter this building or Search here. " +
		"If the building has already been Taken Over, Re-roll. " +
		"Zombies placed here may not move this turn.";
		remainsInPlay = true;
	}

	//TODO change movement info so that these zombies don't move on this turn.
	public override void cardSetup()
	{
		building = zmt.stats.mapTiles.GetRandomBuildingInGameTiles ();

		while(building.takenOver)
			building = zmt.stats.mapTiles.GetRandomBuildingInGameTiles ();

		newZombies = Mathf.Min (building.spaces, zmt.stats.ZombiesInPool);

		name = "My God, They've Taken the " + building.name + "!";
		originalText = "Place a Taken Over maker on " + building.name +
			" and put " + newZombies + " Zombies in it. " +
			"No Hero may enter this building or Search here. ";
		
	}

	public override void effect()
	{
		building.takenOver = true;

		for (int i = 0; i < newZombies; i++) {
			zmt.SpawnAZombie ();
		}
		zmt.Decks.DiscardACard (zmt.cardBeingPlayed);
	}
}

public class NewSpawningPit : ZombieCard
{
	public NewSpawningPit()
	{
		name = "New Spawning Pit";
		phase = playDurningPhase.Immediately;
		originalText =  "Roll a Random Building. " +
		"The Zombie Player places a new Zombie Spawing Pit marker in any space of the building.";
	}

	public override void cardSetup()
	{
		building = zmt.stats.mapTiles.GetRandomBuildingInGameTiles ();

		while(building.spawningPit)
			building = zmt.stats.mapTiles.GetRandomBuildingInGameTiles ();

		originalText = "Place a Zombie Spawning Pit marker in any space of the" + building.name + ".";

	}

	public override void effect()
	{
		building.spawningPit = true;
		zmt.Decks.DiscardACard (zmt.cardBeingPlayed);
	}

	public override bool playable()
	{
		foreach (MapTile t in zmt.stats.mapTiles.FourRandomTilesForGame) {
			foreach (Building b in t.BuildingsOnTile) {
				if (!b.spawningPit)
					return true;
			}
		}
		return false;
	}
}

public class OhTheHorror : ZombieCard
{
	public OhTheHorror()
	{
		name = "Oh the Horror!";
		phase = playDurningPhase.Immediately;
		originalText = "Draw three Zombie Cards. " +
		"At the strat of you next turn, if you have more cards than your hand size will allow, discard down to you normal limit";
	}

	public override void cardSetup()
	{
		originalText = "Zombie is gathering more resources.\nDrawing more cards.";
	}

	public override void effect()
	{
		for (int i = 0; i < 3; i++) {
			zmt.Decks.DrawCard ();
		}
		zmt.Decks.DiscardACard (zmt.cardBeingPlayed);
	}
}

public class SurpriseAttack : ZombieCard
{
	public SurpriseAttack()
	{
		name = "Surprise Attack";
		phase = playDurningPhase.Immediately;
		originalText = "Take a Zombie from your Zombie Pool and place it in the same space as any Hero within a building. " +
		"If there are no Zombies in the pool or Heroes in buildings, discard this card with no effect.";
	}

	public override void cardSetup()
	{
		originalText = "Place a Zombie from the Zombie Pool on the same space as any Hero within a building.\n" +
			"If there are no Heroes in buildings, place Zombie on a Spawning Pit.";
	}

	public override void effect()
	{
		zmt.SpawnAZombie ();

		zmt.Decks.DiscardACard (zmt.cardBeingPlayed);
	}

	public override bool playable()
	{
		if (zmt.stats.ZombiesInPool > 0)
			return true;
		else
			return false;
	}
}
#endregion

#region Movement cards

public class Shamble : ZombieCard
{
	public Shamble()
	{
		name = "Shamble";
		phase = playDurningPhase.Move;
		originalText = "Play this card to move a Zombie D6 spaces instead of its normal move.";
		flavorText = "Look out!";
	}
	//TODO target priority?
	public override void cardSetup()
	{
		int i = zmt.action.RollD6 () - 1;
		originalText = "Move the closest Zombie to a Hero " + i + " spaces towards the Hero";
		if (i == 1) {
			zmt.Card.GetComponent<Animator> ().SetTrigger ("close");
			zmt.Decks.DiscardACard (zmt.cardBeingPlayed);
		}
	}
}
#endregion

#region Fight cards
//PreFight
//TODO Need to be able to reset if there is a chain of fights
public class Uuuurrrggghh : ZombieCard
{
	public Uuuurrrggghh()
	{
		name = "Uuuurrrggghh!";
		phase = playDurningPhase.PreFight;
		originalText = "Play this card to let a Zombie roll 2 extra Fight Dice.";
		flavorText = "Gurgle, gurgle! " +
			"That's not Tom anymore, it's not even human!";
	}

	public override void effect()
	{
		zmt.stats.FightDice += 2;
		zmt.stats.TempFightDice += 2;

		zmt.Decks.DiscardACard (zmt.cardBeingPlayed);
	}

}

//After Roll
public class Braains : ZombieCard
{
	public Braains()
	{
		name = "Braains!";
		phase = playDurningPhase.FightAfterRoll;
		originalText = "Play this card to add +1 to a Zombie's highest Fight Dice roll.";
		flavorText = "Tasty braaains!";
	}
	//TODO use if would die
}

public class UndeadHateTheLiving : ZombieCard
{
	public UndeadHateTheLiving()
	{
		name = "Undead Hate The Living";
		phase = playDurningPhase.FightAfterRoll;
		originalText = "Play this card to hate a Hero, forcing them to Re-roll any number of their Fight Dice (Zombies choice).";
		flavorText = "Nummm...Numm...";
	}
	//TODO use if would die
}
#endregion

#region End Of Turn
public class TheresTooMany : ZombieCard
{
	public TheresTooMany()
	{
		name = "There's Too Many!";
		phase = playDurningPhase.EndOfTurn;
		originalText = "Play this card at the end of the Zombie Turn to spawn D6 new Zombies. " +
		"You may remove them from anywhere on the board if there are not enough out of play.";
		flavorText = "We'll never make it through!";
	}

	public override void cardSetup()
	{
		int roll = zmt.action.RollD6 ();

		newZombies = Mathf.Min (roll, zmt.stats.ZombiesInPool);

		if (newZombies == 0) {
			//Debug.Log ("CANCELED THERE'S TOO MANY");
			zmt.Card.GetComponent<Animator> ().SetTrigger ("close");
			zmt.Decks.DiscardACard (zmt.cardBeingPlayed);
		}

		originalText = "Place " + newZombies + " new Zombies spred evenly across the Zombie Spawn Pits.";

	}

	public override void effect()
	{
		for (int i = 0; i < newZombies; i++) {
			zmt.SpawnAZombie ();
		}

		zmt.Decks.DiscardACard (zmt.cardBeingPlayed);
	}

	public override bool playable()
	{
		if (zmt.stats.ZombiesInPool > 0)
			return true;
		else
			return false;
	}
}
#endregion
#endregion

public class ZombieCards : ZombieCard
{
	public ZombieCards ()
	{
	}
}


