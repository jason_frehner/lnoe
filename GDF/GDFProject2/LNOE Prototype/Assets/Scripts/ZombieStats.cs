﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ZombieStats
{
	public int ZombiesInPool;
	public int ZombiesOnBoard;
	public int ZombiesKilledCount = 0;

	public bool ZombieTurn = true;

	public int FightDice = 1;
	public int TempFightDice = 0;
	//private int Round = -10;
	//public int HeroesDiedCount = 0;


	public Tiles mapTiles = new Tiles();

	public void ZombieDied()
	{
		if (ZombiesOnBoard > 0) {
			ZombiesOnBoard--;
			ZombiesKilledCount++;
			ZombiesInPool++;
		}
		if (ZombiesKilledCount > 14)
			Debug.Log ("Heroes Win");//TODO Lose screen
	}

	public double GameStateDifficulty(int zombiesKilled, int heroesKilled, int roundsFinished)
	{
		float zombiesKilledNormalized =  new Normalization ().intNormalization (zombiesKilled, 15);
		float heroesKilledNormalized = new Normalization ().intNormalization (heroesKilled, 2);
		float roundsLeftNormalized = new Normalization ().intNormalization (roundsFinished, 15);

		double zombiesKilledGaussian = new GaussianFunction ().gaussian (zombiesKilledNormalized, 0.8f);
		double heroesKilledGaussian = new GaussianFunction ().gaussian (heroesKilledNormalized, 0.0f);
		double roundsLeftGaussian = new GaussianFunction ().gaussian (roundsLeftNormalized, 0.8f);


		Debug.Log (zombiesKilledNormalized);
		Debug.Log (heroesKilledNormalized);
		Debug.Log (roundsLeftNormalized);

		Debug.Log (zombiesKilledGaussian);
		Debug.Log (heroesKilledGaussian);
		Debug.Log (roundsLeftGaussian);

		//Heroe objective comparred to Zombie objective
		return ((1.0f - roundsLeftGaussian) * heroesKilledGaussian) - (roundsLeftGaussian * zombiesKilledGaussian);
	}
}