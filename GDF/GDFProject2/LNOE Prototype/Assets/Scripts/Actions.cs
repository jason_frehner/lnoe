﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//TODO clear fight roll text when going into the fight screen
public class Actions
{
	public int RollD6()
	{
		return Random.Range (1, 7);
	}

	public string RollRandomBuilding()
	{
		return null;
	}

	public string FightRoll(int fightDice, int tempFightDice)
	{
		//TODO Hero can make Zombie re roll dice

		List<int> rollNumbers = new List<int>();
		string rollString = "";
		for (int i = 0; i < fightDice; i++) {
			int x = RollD6 ();
			rollNumbers.Add (x);
			rollString += x.ToString () + " ";
			Debug.Log ("Fight Roll: " + x);
		}

		if (tempFightDice > 0) {
			fightDice -= tempFightDice;
			tempFightDice = 0;
		}

		return rollString;
	}
}