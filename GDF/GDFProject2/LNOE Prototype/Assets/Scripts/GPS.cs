﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GPS : MonoBehaviour
{
	public Text t;


	public void getLocation(int accuracy)
	{
		StartCoroutine (location (accuracy));
	}

	//From Unity Documentation
	public IEnumerator location(int accuracy)
	{
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser) {
			t.text = "Location service not on";
			yield break;
		}

		// Start service before querying location
		Input.location.Start (accuracy);

		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
			yield return new WaitForSeconds (1);
			maxWait--;
		}

		// Service didn't initialize in 20 seconds
		if (maxWait < 1) {
			t.text = "Timed out";
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed) {
			//print ("Unable to determine device location");
			t.text = "Failed";
			yield break;
		} else {
			// Access granted and location value could be retrieved
			//print ("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
			t.text = "Position: " + Input.location.lastData.latitude + " " +
				Input.location.lastData.longitude + " " +
				Input.location.lastData.altitude;
		}

		// Stop service if there is no need to query location updates continuously
		Input.location.Stop ();
	}
}

