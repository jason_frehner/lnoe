﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using KEggGameStudio.NFC;

public class NFC : MonoBehaviour
{

	public NFC_Mgr nfcManger;

	public Text status;
	public Text outputText;
	public InputField inputText;

	private string ReceivingFunName = "OnReceivingMsg";

	void Start ()
	{
		//Setup nfc manager
		//nfcManger = new NFC_Mgr();
		nfcManger.SetCodingType(DefineNFC.CodingType.US_ASCII);
		nfcManger.SetListener(gameObject, ReceivingFunName);
		Read ();
	}

	public void Read()
	{
		nfcManger.SetOperational (DefineNFC.NFC_Operational.READ);
		status.text = "Ready to read NFC tag.";
	}

	public void Write()
	{
		
		nfcManger.SetOperational (DefineNFC.NFC_Operational.WRITE);
		nfcManger.WriteTagData (inputText.text);
		status.text = "Ready to write to NFC tag.";
	}

	public void Clear()
	{
		nfcManger.SetOperational (DefineNFC.NFC_Operational.CLEAR);
		status.text = "Clear Tag formatting.";

	}

	// Receive Tag Msg
	private void OnReceivingMsg(string str)
	{
		outputText.text = str;
	}
}
