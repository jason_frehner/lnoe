﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieDeck
{
	//Three decks to be managed
	public List<ZombieCard> deck = new List<ZombieCard> ();
	public List<ZombieCard> discard = new List<ZombieCard> ();
	public List<ZombieCard> hand = new List<ZombieCard> ();


	public void AddCardsTooDeck()
	{
		deck = new List<ZombieCard>();

		#region Creates basic zombie deck.
		//Start of turn cards.
		deck.Add (new Cornered());

		for (int i = 0; i < 2; i++) {
			deck.Add (new LightsOut());
		}
		for (int i = 0; i < 2; i++) {
			deck.Add (new RelentlessAdvance());
		}

		//Play immediately cards
		deck.Add (new HeavyRain());
		deck.Add (new ATownOverrun());
		for (int i = 0; i < 3; i++) {
			deck.Add (new MyGodTheyveTakenThe());
		}
		for (int i = 0; i < 2; i++) {
			deck.Add (new NewSpawningPit());
		}
		for (int i = 0; i < 2; i++) {
			deck.Add (new OhTheHorror());
		}
		for (int i = 0; i < 2; i++) {
			deck.Add (new SurpriseAttack());
		}

		//Movement cards
		for (int i = 0; i < 6; i++) {
			deck.Add (new Shamble());
		}

		//Fight cards
		deck.Add (new Braains());
		for (int i = 0; i < 5; i++) {
			deck.Add (new UndeadHateTheLiving());
		}
		for (int i = 0; i < 5; i++) {
			deck.Add (new Uuuurrrggghh());
		}

		//End of turn cards
		for (int i = 0; i < 2; i++) {
			deck.Add (new TheresTooMany());
		}

		// 5 cards not used from basic deck
		// 2x Loss of Faith, won't know if player played faith card or is checking if weapon breaks.
		// 1x Trip, won't know what player's movement roll is.
		// 2x Resilient, don't know if zombie was killed by a gun. TODO add a check for this?

		#endregion

		ShuffleDeck ();
	}
	
	//Used to move a card from deck to hand.
	public void DrawCard()
	{
		if (deck.Count < 1)
			RecyleDiscard ();
		
		hand.Add (deck[0]);	
		deck.RemoveAt (0);
	}

	public List<ZombieCard> CheckIfInHand(playDurningPhase cardPhase)
	{
		List<ZombieCard> tempList = new List<ZombieCard>();

		foreach (ZombieCard c in hand) {
			if (c.phase == cardPhase && c.playable ())
				tempList.Add (c);
		}

		return tempList;
	}

	public void ShuffleDeck()
	{
		List<ZombieCard> tempDeck = new List<ZombieCard> ();

		for (int i = 0; i < 3; i++) {
			while (deck.Count > 0) {
				int randomNumber = Random.Range (0, deck.Count);
				tempDeck.Add (deck [randomNumber]);
				deck.RemoveAt (randomNumber);
			}
			while (tempDeck.Count > 0) {
				int randomNumber = Random.Range (0, tempDeck.Count);
				deck.Add (tempDeck [randomNumber]);
				tempDeck.RemoveAt (randomNumber);
			}
		}
	}

	public void RecyleDiscard()
	{
		deck = discard;
		ShuffleDeck ();
		//clear the discard TODO
	}

	public void DiscardACard(ZombieCard c)
	{
		discard.Add (c);
		hand.Remove (c);
	}
}