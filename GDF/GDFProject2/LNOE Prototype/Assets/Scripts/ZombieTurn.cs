﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ZombieTurn : MonoBehaviour
{
	/* Basic game
	 * Die Zombies, die! mission
	 * 	15 turn
	 * 	Heroes need to kill 15 zombies
	 * 	Zombies, need to last 15 Turns or kill 2 Heroes
	 */

	/* 
	AI choices
	Card priority
	Movement priority
	*/

	delegate void TurnPhaseDelegateDeclare();
	TurnPhaseDelegateDeclare TurnPhaseDelegate;

	public Actions action = new Actions();
	//public Tiles mapTiles = new Tiles();

	[Header("Decks")]
	public ZombieDeck Decks = new ZombieDeck();
	public List<ZombieCard> CardsToPlay;
	[Space(20)]

	[Header("Tracking Variables")]
	public ZombieStats stats = new ZombieStats();
	public bool Spawn = false;
	//public int FightDice = 1;
	//public int TempFightDice = 0;
	public bool ZombiesWinOnTie = true;
	public bool FightCardUsed = false;
	private int Round = -10;
	public int HeroesDiedCount = 0;
	public ZombieCard cardBeingPlayed;
	[Space(20)]

	//Reference to other gameobjects
	[Header("Scene objects to control.")]
	public GameObject Card;
	public Text CardTitle;
	public Text CardText;
	public Text CardFlavor;
	[Space(10)]
	public Text RoundNumbertext;
	public Text ZombiesOnBoardText;
	[Space(10)]
	public Text ZombiesKilledText;
	public GameObject ZombiesKilledSprite;
	public Text HeroesDiedText;
	public GameObject HeroesDiedSprite;
	[Space(10)]
	public GameObject MovePanel;
	public Text MoveText;
	[Space(10)]
	public GameObject FightPanel;
	public Text FightText;
	public Text FightRollText;
	[Space(10)]
	public GameObject RemainsInPlay;
	public GameObject RemainsInPlayElement;
	[Space(10)]
	public GameObject ResultPanel;
	public Text ResultText;
	[Space(10)]
	public GameObject background;
	[Space(10)]
	public GameLog gl;

	void Start()
	{
		Debug.Log ("Setting up the game.");

		TurnPhaseDelegate = StartOfTurn;

		//Set Starting values
		stats.ZombiesInPool = 14;
		stats.ZombiesOnBoard = 0;

		//Decks created
		Decks.AddCardsTooDeck ();

		//Tiles selected
		stats.mapTiles.GameTiles ();

		//Spawn starting zombies
		int startingZombies = SpawnZombies () + SpawnZombies ();

		//TODO starting heroes

		//Display starting infomation
		ZombieCard setupcard = new ZombieCard ();
		setupcard.name = "SETUP";
		setupcard.originalText = "Use map Tiles:\n" +
			stats.mapTiles.FourRandomTilesForGame[0].name + "; " +
			stats.mapTiles.FourRandomTilesForGame[1].name + "; " +
			stats.mapTiles.FourRandomTilesForGame[2].name + "; " +
			stats.mapTiles.FourRandomTilesForGame[3].name + ".\n\n"+
			"Place " + startingZombies + " Zombies evenly across all Spawning Pits.\n" +
			"Place Heroes on the board";
		setupcard.flavorText = "The Night Begins!";

		//Play the setup card and remove it from the game
		cardBeingPlayed = setupcard;
		PlayCard ();
		Decks.discard.Remove (setupcard);
	}

	#region Phases
	public void StartOfTurn()
	{
		Debug.Log ("Phase: Start of turn.");

		TurnPhaseDelegate = StartOfTurn;
		stats.ZombieTurn = true;

		//Game Difficutly
		float difficulty = (float)stats.GameStateDifficulty (stats.ZombiesKilledCount, HeroesDiedCount, 15 - Round);
		Debug.Log ("Difficulty "+ difficulty);
		background.GetComponent<Image> ().fillAmount = difficulty;

		//Reset from effects
		stats.FightDice = 1;
		ZombiesWinOnTie = true;
		FightRollText.text = "";

		//Decrese or Set round tracker
		if (Round == -10)//First round of the game
			Round = 15;
		else
			Round--;

		//Check if game over
		if (Round < 1) {
			GameOver ("ZOMBIES");
			return;
		}

		//Update text in log
		if (gl != null) {
			RoundNumbertext.text = Round.ToString ();
			updateLog ();
		}

		//Goto next phase
		PlayStartOfTurnCards ();
	}
		
	public void PlayStartOfTurnCards()
	{
		Debug.Log ("Phase: Playing start of turn card.");

		TurnPhaseDelegate = PlayStartOfTurnCards;

		//Play start of turn cards
		CardsToPlay = Decks.CheckIfInHand (playDurningPhase.StartOfTurn);
		if (CardsToPlay.Count > 0) {
			foreach (ZombieCard c in CardsToPlay) {
				Debug.Log ("Playing at the start of turn: " + c.name);
				cardBeingPlayed = c;
				PlayCard ();
				return;
			}
		}
		DrawNewCards ();
	}

	public void DrawNewCards()
	{
		Debug.Log ("Phase: Drawing cards.");

		//discard before draw
		bool discarded = false;
		while (Decks.hand.Count > 4) {
			Decks.DiscardACard (Decks.hand [0]);
			discarded = true;
		}
		if (!discarded && Decks.hand.Count > 0)
			Decks.DiscardACard (Decks.hand [0]);

		//Draw up
		while (Decks.hand.Count < 4) {
			Decks.DrawCard ();
		}
		PlayImmediatelyCards ();
	}

	public void PlayImmediatelyCards()
	{
		Debug.Log ("Phase: Playing immediately card.");

		TurnPhaseDelegate = PlayImmediatelyCards;

		//Play immediately
		CardsToPlay = Decks.CheckIfInHand (playDurningPhase.Immediately);
		if (CardsToPlay.Count > 0) {
			foreach (ZombieCard c in CardsToPlay) {
				Debug.Log ("Playing immediately: " + c.name);
				cardBeingPlayed = c;
				PlayCard ();
				return;
			}
		}
		CheckIfNewZombiesSpawn ();
	}

	public void CheckIfNewZombiesSpawn()
	{
		Debug.Log ("Phase: Checking if new zombies will spawn.");

		//Check if new zombies Spawn
		int spawnCheck2D6Roll = action.RollD6 () + action.RollD6 ();
		Debug.Log ("New Spawn Check Roll: " + spawnCheck2D6Roll);
		if (spawnCheck2D6Roll > stats.ZombiesOnBoard)
			Spawn = true;
		else
			Spawn = false;

		MoveZombies ();
	}

	public void MoveZombies()
	{
		Debug.Log ("Phase: Moving zombies.");

		TurnPhaseDelegate = MoveZombies;

		//Play Move cards
		CardsToPlay = Decks.CheckIfInHand (playDurningPhase.Move);
		if (CardsToPlay.Count > 0) {
			foreach (ZombieCard c in CardsToPlay) {
				Debug.Log ("Playing Move Card: " + c.name);
				cardBeingPlayed = c;
				PlayCard ();
				return;
			}
		}

		MovePanel.GetComponent<Animator> ().SetTrigger ("open");

		TurnPhaseDelegate = FightHeroes;
	}

	public void FightHeroes()
	{
		Debug.Log ("Phase: Fight.");

		TurnPhaseDelegate = SpawnNewZombies;

		string winner = "Zombies";
		if (!ZombiesWinOnTie)
			winner = "Heroes";
			

		FightText.text = "Zombies roll " + stats.FightDice + " dice.\n" + winner + " win on a tie.";
		FightPanel.GetComponent<Animator> ().SetTrigger ("open");
	}

	public void SpawnNewZombies()
	{
		Debug.Log ("Phase: Spawning zombies.");

		TurnPhaseDelegate = PlayEndOfTurnCards;

		//Spawn new
		if (Spawn) {
			int spawnZombies = SpawnZombies ();

			ZombieCard spawncard = new ZombieCard ();
			spawncard.name = "SPAWN";
			spawncard.originalText = "Place " + spawnZombies + " Zombies evenly across all Spawning Pits.";
			spawncard.flavorText = "The Dead Live!";

			cardBeingPlayed = spawncard;
			PlayCard ();
			Decks.discard.Remove (spawncard);
		}
	}

	public void PlayEndOfTurnCards()
	{
		Debug.Log ("Phase: Playing end of turn card.");

		TurnPhaseDelegate = PlayEndOfTurnCards;

		//End of turn
		CardsToPlay = Decks.CheckIfInHand (playDurningPhase.EndOfTurn);
		if (CardsToPlay.Count > 0) {
			foreach (ZombieCard c in CardsToPlay) {
				Debug.Log ("Playing at the end of turn: " + c.name);
				cardBeingPlayed = c;
				PlayCard ();
				return;
			}
		}
		stats.ZombieTurn = false;
	}
	#endregion

	//Cards
	public void PlayCard()
	{
		cardBeingPlayed.cardSetup ();

		CardTitle.text = cardBeingPlayed.name;
		CardText.text = cardBeingPlayed.originalText;
		CardFlavor.text = cardBeingPlayed.flavorText;

		Card.GetComponent<Animator> ().SetTrigger ("open");
	}

	public void AddToRemainsInPlay(ZombieCard c)
	{
		GameObject element = Instantiate (RemainsInPlayElement);
		element.transform.SetParent (RemainsInPlay.transform.FindChild ("Panel/List"));
		element.transform.Find ("Info/TextTitle").GetComponent<Text> ().text = c.name;
		element.transform.FindChild ("Info/TextEffect").GetComponent<Text> ().text = c.originalText;
		element.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		//TODO When remains in play is canceled update information
	}

	//Continue From Button
	public void ContinueFromPlayingACard(bool canceled)
	{
		if (!canceled) {
			cardBeingPlayed.effect ();

			if (cardBeingPlayed.remainsInPlay) {
				AddToRemainsInPlay (cardBeingPlayed);
			}
		} else
			Decks.DiscardACard (cardBeingPlayed);
		
		Card.GetComponent<Animator> ().SetTrigger ("close");
		if(stats.ZombieTurn)
			TurnPhaseDelegate ();
	}

	public void ContinueFromMoveInfo()
	{
		MovePanel.GetComponent<Animator> ().SetTrigger ("close");
		if(stats.ZombieTurn)
			TurnPhaseDelegate ();
	}

	public void ContinueFromFightInfo()
	{
		FightPanel.GetComponent<Animator> ().SetTrigger ("close");
		if(stats.ZombieTurn)
			TurnPhaseDelegate ();
	}

	//Spawn
	public int SpawnZombies()
	{
		int spawnRoll = action.RollD6 ();
		if (spawnRoll > stats.ZombiesInPool)
			spawnRoll = stats.ZombiesInPool;
		stats.ZombiesInPool -= spawnRoll;
		stats.ZombiesOnBoard += spawnRoll;

		UpdateUIText ();

		Debug.Log ("Spawning " + spawnRoll + " new Zombies.");
		return spawnRoll;
	}

	public void SpawnAZombie()
	{
		stats.ZombiesInPool --;
		stats.ZombiesOnBoard ++;

		UpdateUIText ();
	}

	//Update UI Text
	public void UpdateUIText()
	{
		ZombiesKilledText.text = stats.ZombiesKilledCount.ToString ();
		ZombiesOnBoardText.text = stats.ZombiesOnBoard.ToString ();

		ZombiesKilledSprite.GetComponent<Image> ().fillAmount = stats.ZombiesKilledCount / 15.0f;
	}
		
	//Input From Buttons
	public void ZombieDied()
	{
		stats.ZombieDied ();
		UpdateUIText ();

		if (stats.ZombiesKilledCount > 14)
			GameOver ("HEROES");
	}

	public void HeroeDied()
	{
		HeroesDiedCount++;
		HeroesDiedText.text = HeroesDiedCount.ToString ();

		HeroesDiedSprite.GetComponent<Image> ().fillAmount = HeroesDiedCount / 2.0f;

		if (HeroesDiedCount > 1)
			GameOver ("ZOMBIES");
	}

	//Fight
	public void FightAction()
	{
		FightCardUsed = false;
		TurnPhaseDelegate = FightRoll;

		CardsToPlay = Decks.CheckIfInHand (playDurningPhase.PreFight);
		if (CardsToPlay.Count > 0 && !FightCardUsed) {
			Debug.Log ("Playing before fight: " + CardsToPlay [0].name);
			cardBeingPlayed = CardsToPlay [0];
			PlayCard ();
			FightCardUsed = true;
			return;
		}
		FightRoll ();
	}

	public void FightRoll()
	{
		//TODO Hero can make Zombie re roll dice

		FightRollText.text = action.FightRoll (stats.FightDice, stats.TempFightDice);

		TurnPhaseDelegate = SpawnNewZombies;
	}

	public void FightWouldDieTry()
	{
		CardsToPlay = Decks.CheckIfInHand (playDurningPhase.FightAfterRoll);
		if (CardsToPlay.Count > 0 && !FightCardUsed) {
			Debug.Log ("Playing after fight roll: " + CardsToPlay[0].name);
			cardBeingPlayed = CardsToPlay [0];
			PlayCard ();
			FightCardUsed = true;
		}

		TurnPhaseDelegate = FightRoll;
	}

	//Game Over
	public void GameOver(string winner)
	{
		ResultText.text = winner + " WINS";
		ResultPanel.GetComponent<Animator> ().SetTrigger ("open");
	}

	//Send update to log
	private void updateLog()
	{
		string s = "Rounds Left: " + Round + " Heroes dead: " + HeroesDiedCount + " Zombies Dead: " + stats.ZombiesKilledCount + " Zombies on Board: " + stats.ZombiesOnBoard;
		gl.addText (s);
	}
}
