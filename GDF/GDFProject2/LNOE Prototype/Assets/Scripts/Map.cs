﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Building
{
	public string name;
	public List<int> rollRange;
	public string search;
	public int spaces;
	public bool spawningPit;

	public bool takenOver = false;
	public bool lightsOut = false;

	public Building()
	{
	}

	public Building(string buildingName, List<int> range, string find, int squares, bool pit)
	{
		name = buildingName;
		rollRange = range;
		search = find;
		spaces = squares;
		spawningPit = pit;
	}
}

public class MapTile
{
	public List<Building> BuildingsOnTile= new List<Building>();

	public string name;

	public MapTile()
	{
	}

	public int TileRangeTotal()
	{
		int total = 0;

		foreach (Building b in BuildingsOnTile) {
			if (b.rollRange != null) {
				foreach (int i in b.rollRange) {
					total += i;
				}
			}
		}

		return total;
	}

	public Building RandomBuilding()
	{
		int x = Random.Range (0, BuildingsOnTile.Count);

		return BuildingsOnTile [x];
	}
}

public class Tiles
{
	public List<MapTile> BasicMapTiles = new List<MapTile>();
	public List<MapTile> FourRandomTilesForGame = new List<MapTile>();

	public Tiles()
	{
		BasicMapTiles.Add (new BankJunkyardGas());
		BasicMapTiles.Add (new BarnFarmhouse());
		BasicMapTiles.Add (new ChurchGraveyardPolic());
		BasicMapTiles.Add (new HangerHospitalDiner());
		BasicMapTiles.Add (new SchoolGym());
		BasicMapTiles.Add (new GeneralGunPlant());
	}

	public void GameTiles()
	{
		List<MapTile> tempList = BasicMapTiles;

		FourRandomTilesForGame = new List<MapTile> ();

		while (FourRandomTilesForGame.Count < 4) {
			int x = Random.Range (0, tempList.Count);
			FourRandomTilesForGame.Add (tempList[x]);
			tempList.RemoveAt (x);
		}
	}

	public Building GetRandomBuildingInGameTiles()
	{
		int x = Random.Range (0, FourRandomTilesForGame.Count);

		MapTile t = FourRandomTilesForGame [x];

		Building b = t.RandomBuilding ();

		return b;
	}

	public string BuildingZombieChoiceAI(float gameStateDificulty)
	{
		return null;
	}
}

#region Basic Map Tiles
public class BankJunkyardGas : MapTile
{
	public BankJunkyardGas()
	{
		name = "Bank, Junkyard, Gas Station";

		//Also has road out of town
		BuildingsOnTile.Add (new Building("The Bank", new List<int>(new int[]{1,2}), "", 4, false));
		BuildingsOnTile.Add (new Building("Junkyard", new List<int>(new int[]{3,4}), "Random Discard", 6, true));
		BuildingsOnTile.Add (new Building("Gas Station", new List<int>(new int[]{5,6}), "Gasoline", 4, false));
	}
}

public class BarnFarmhouse : MapTile
{
	public BarnFarmhouse()
	{
		name = "Barn, Farmhouse";

		//Also has Cornfield, players will still have to do the check for the zombies
		BuildingsOnTile.Add (new Building("The Barn", new List<int>(new int[]{1,2,3}), "Pitchfork", 4, true));
		BuildingsOnTile.Add (new Building("Farmhouse", new List<int>(new int[]{4,5,6}), "", 2, false));
	}
}

public class ChurchGraveyardPolic : MapTile
{
	public ChurchGraveyardPolic()
	{
		name = "Church, Police Station";

		//spawning pit is in the graveyard
		BuildingsOnTile.Add (new Building("Church", new List<int>(new int[]{1,2,3}), "Faith", 4, false));
		//BuildingsOnTile.Add (new Building("Graveyard", null, "", 4, true));
		BuildingsOnTile.Add (new Building("Police Station", new List<int>(new int[]{4,5,6}), "Pump Shotgun", 2, false));
	}
}

public class GeneralGunPlant : MapTile
{
	public GeneralGunPlant()
	{
		name = "General Store, Gun Shop, Plant";

		BuildingsOnTile.Add (new Building("General Store", new List<int>(new int[]{1,2}), "", 4, false));
		BuildingsOnTile.Add (new Building("Gun Shop", new List<int>(new int[]{3,4}), "Any Gun or Ammo", 4, false));
		BuildingsOnTile.Add (new Building("The Plant", new List<int>(new int[]{5,6}), "", 3, true));
	}
}

public class HangerHospitalDiner : MapTile
{
	public HangerHospitalDiner()
	{
		name = "Airplane Hanger, Hospital, Diner";

		BuildingsOnTile.Add (new Building("Airplane Hanger", new List<int>(new int[]{1,2}), "Signal Flare", 4, false));
		BuildingsOnTile.Add (new Building("Hospital", new List<int>(new int[]{3,4}), "First Aid", 7, true));
		BuildingsOnTile.Add (new Building("Diner", new List<int>(new int[]{5,6}), "Meat Cleaver", 4, false));
	}
}

public class SchoolGym : MapTile
{
	public SchoolGym()
	{
		name = "High School, Gym";

		//spawning pit not in a building
		BuildingsOnTile.Add (new Building("High School", new List<int>(new int[]{1,2,3}), "", 6, false));
		BuildingsOnTile.Add (new Building("Gym", new List<int>(new int[]{4,5,6}), "", 4, false));
	}
}
#endregion