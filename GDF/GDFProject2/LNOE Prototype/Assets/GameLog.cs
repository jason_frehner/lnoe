﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLog : MonoBehaviour {

	public GameObject contents;
	public GameObject textPrefab;
	public GameObject imagePrefab;

	private int textObjects = 0;
	private int imageObjects = 0;

	//Add text block to log
	public void addText(string s)
	{
		//Instantiate
		textObjects++;
		GameObject newText = Instantiate (textPrefab);
		setParentAndScale (newText.transform);

		//Set content
		newText.GetComponent<Text> ().text = s;

		//Adjust log
		contentsHeight ();
	}

	//Add image block to log
	public void addImage(Texture texture, float aspect)
	{
		//Instantiate
		imageObjects++;
		GameObject newImage = Instantiate (imagePrefab);

		//Set content
		newImage.GetComponent<RawImage> ().texture = texture;
		newImage.GetComponent<AspectRatioFitter> ().aspectRatio = aspect;
		setParentAndScale (newImage.transform);

		//Adjust log
		contentsHeight ();
	}

	//Adjust content height to log size
	void contentsHeight()
	{
		//How high should the content be
		float height = (450.0f * imageObjects) + (150.0f * textObjects);

		//Set the height
		RectTransform rect = contents.GetComponent<RectTransform> ();
		rect.sizeDelta = new Vector2(rect.sizeDelta.x, height);
	}


	void setParentAndScale(Transform t)
	{
		t.SetParent (contents.transform);
		t.localScale = new Vector3 (1, 1, 1);
	}
}
