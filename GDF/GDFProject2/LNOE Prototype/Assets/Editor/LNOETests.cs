﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using UnityEngine;

namespace LNOETests
{
	[TestFixture]
	[Category("Buildings")]
	internal class BuildingsTest
	{
		//Buildings b = new Buildings("test", new int[3] (1,2,3), "thing");
		[Test]
		public void GetRandomBuildingFromABankTile()
		{
			string s = new BankJunkyardGas ().RandomBuilding ().name;

			//throw new Exception (s);
			Assert.That (s is string, s);
		}

		[Test]
		public void MapTileContainMoreThanOneBuilding()
		{
			Tiles mt = new Tiles ();

			foreach (MapTile t in mt.BasicMapTiles) {
				Assert.That (t.BuildingsOnTile.Count > 1);
			}

		}

		[Test]
		public void RangeTotalInChurchTileIs21()
		{
			Assert.That (new ChurchGraveyardPolic().TileRangeTotal () == 21);
		}

		[Test]
		public void RollRandomBuildingIsNotNull()
		{
			Tiles t = new Tiles ();
			t.GameTiles ();
			string s = t.GetRandomBuildingInGameTiles ().name;

			//throw new Exception (s);
			Assert.That (s is string, s);

		}

		[Test]
		public void ThereAreFourRandomGameTiles()
		{
			Tiles t = new Tiles ();

			t.GameTiles ();

			Assert.That (t.FourRandomTilesForGame.Count == 4);
		}

		[Test]
		public void ThereAreSixBasicMapTiles()
		{
			Tiles t = new Tiles ();

			Assert.That (t.BasicMapTiles.Count == 6);
		}

		[Test]
		public void TotalOfAllTheRangeValesOnATileIs21()
		{
			Tiles mt = new Tiles ();

			foreach (MapTile t in mt.BasicMapTiles) {
				Assert.That (t.TileRangeTotal () == 21);
			}
		}
	}

	[TestFixture]
	[Category("Building Selection AI")]
	internal class BuildingSelectionAI
	{
		[Test]
		public void BuidlingZombieChoiceAIIsNotNull()
		{
			Tiles t = new Tiles ();
			t.GameTiles ();

			Assert.That (t.BuildingZombieChoiceAI (0.0f) != null);
		}
	}
		
	[TestFixture]
	[Category("Zombie Cards")]
	internal class ZombieCardsTest
	{
		ZombieStats zs = new ZombieStats();

		[Test]
		public void CheckIfInHandDoesNotReturnUnPlayableCards()
		{
			SurpriseAttack sa = new SurpriseAttack ();
			sa.zmt.stats.ZombiesInPool = 0;


			Assert.That (sa.zmt.Decks.CheckIfInHand (playDurningPhase.Immediately).Count == 0);
		}

		[Test]
		public void LightsOutFindsABuilding()
		{
			LightsOut lo = new LightsOut ();
			lo.zmt.stats.mapTiles = new Tiles ();
			lo.zmt.stats.mapTiles.GameTiles ();
			lo.cardSetup ();

			Assert.That (lo.building != null);
		}

		[Test]
		public void NewSpawingPitOnlyIfABuildingDoesNotHaveOne()
		{
			NewSpawningPit nsp = new NewSpawningPit ();
			nsp.zmt.stats.mapTiles = new Tiles ();
			nsp.zmt.stats.mapTiles.GameTiles ();

			foreach (MapTile t in nsp.zmt.stats.mapTiles.FourRandomTilesForGame) {
				foreach (Building b in t.BuildingsOnTile) {
					b.spawningPit = true;
				}
			}

			Assert.That (nsp.playable () == false);
		}

		[Test]
		public void RelentlessNotPlayableIfNoZombiesOnBoard()
		{
			RelentlessAdvance ra = new RelentlessAdvance ();

			ra.zmt.stats.ZombiesOnBoard = 0;

			Assert.That (ra.playable () == false);
		}

		[Test]
		public void MyGodTheyTakenTheAddsZombiesToTheBoard()
		{
			MyGodTheyveTakenThe my = new MyGodTheyveTakenThe ();
			my.zmt.stats = new ZombieStats ();
			my.zmt.stats.mapTiles.GameTiles ();
			my.zmt.stats.ZombiesInPool = 14;
			my.newZombies = 1;

			int zonb = my.zmt.stats.ZombiesOnBoard;

			//throw new Exception (zonb.ToString ());

			my.cardSetup ();
			my.effect ();

			Assert.That (my.zmt.stats.ZombiesOnBoard > zonb);
		}

		[Test]
		public void SurpriseAttackDoesNotPlayIfNoZombiesInThePoll()
		{
			SurpriseAttack sa = new SurpriseAttack ();
			sa.zmt.stats.ZombiesInPool = 0;

			Assert.That (sa.playable () == false);
		}

		[Test]
		public void TheresTooManyAddsZombiesIfThereIsSomeInThePool()
		{
			TheresTooMany ttm = new TheresTooMany ();

			ttm.zmt.stats.ZombiesInPool = 10;
			ttm.zmt.stats.ZombiesOnBoard = 0;
			ttm.cardSetup ();
			ttm.effect ();

			Assert.That (ttm.zmt.stats.ZombiesInPool < 10);
			Assert.That (ttm.zmt.stats.ZombiesOnBoard > 0);
		}

	}

	[TestFixture]
	[Category("Zombie Stats")]
	internal class ZombieStatsTest
	{
		ZombieStats zs = new ZombieStats();
		Actions action = new Actions();

		[Test]
		public void DiceRollIsBetween1And6()
		{
			Assert.That (action.RollD6 () > 0);
			Assert.That (action.RollD6 () < 7);
		}

		[Test]
		public void GameStateDifficultyIsEasyAtStart()
		{
			Assert.That (zs.GameStateDifficulty (0, 0, 0) < 0.5f);
		}

		[Test]
		public void GameStateDifficultyIsEasyAtZombiesAboutToWin()
		{
			Assert.That (zs.GameStateDifficulty (0, 1, 14) < 0.5f);
		}

		[Test]
		public void GameStateDifficultyIsHardAtHeroesAboutToWin()
		{
			Assert.That (zs.GameStateDifficulty (14, 0, 0) > 0.5f);
		}

		[Test]
		public void GameStateDifficultyRetrunsZeroAndPositiveOne()
		{
			double x = zs.GameStateDifficulty (0,0,0);
			Assert.That (x >= 0.0f, x.ToString ());
			Assert.That (x <= 1.0f, x.ToString ());
		}

		[Test]
		public void WhenZombieDiesItReturnsToThePool()
		{
			zs.ZombiesOnBoard = 10;
			int zombiesInPool = zs.ZombiesInPool;

			zs.ZombieDied ();

			Assert.That (zs.ZombiesInPool == zombiesInPool + 1);
		}

		[Test]
		public void IntNormalizationReturnAValidAnswer()
		{
			Normalization n = new Normalization ();
			double x = n.intNormalization (1, 2);

			Assert.That (x == 0.0f, x.ToString ());
		}

		[Test]
		public void GasussianZeroDistanceOneResult()
		{
			GaussianFunction g = new GaussianFunction ();

			Assert.That (g.gaussian (1,1) == 1.0);
		}
	}

}
