﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class voiceCommand : MonoBehaviour
{
	private SpeechRecognizerManager voiceManager = null;
	private bool isListening = false;
	private string _message = "";

	public loadScene ls;
	public ZombieTurn zt;
	public Animator fight;
	public Animator remains;
	public Text text;

	void Start ()
	{
		// We pass the game object's name that will receive the callback messages.
		voiceManager = new SpeechRecognizerManager (gameObject.name);
	}

	void OnDestroy ()
	{
		if (voiceManager != null)
			voiceManager.Release ();
	}

	#region SPEECH_CALLBACKS

	void OnSpeechEvent (string e)
	{
		switch (int.Parse (e)) {
		case SpeechRecognizerManager.EVENT_SPEECH_READY:
			StatusMessage ("Ready for speech");
			break;
		case SpeechRecognizerManager.EVENT_SPEECH_BEGINNING:
			StatusMessage ("Started speaking");
			break;
		case SpeechRecognizerManager.EVENT_SPEECH_END:
			StatusMessage ("Stopped speaking");
			break;
		}
	}

	void OnSpeechResults (string results)
	{
		isListening = false;

		StatusMessage (results);

		//Verbal commands
		switch (results.ToLower ()) {
		case "zombie killed":
		case "killed zombie":
		case "zombie died":
		case "zombie dead":
			zt.ZombieDied ();
			break;
		case "fight zombie":
		case "zombie fight":
			fight.SetTrigger ("open");
			break;
		case "hero killed":
		case "hero dead":
			zt.HeroeDied ();
			break;
		case "zombie turn":
			zt.StartOfTurn ();
			break;
		case "remains in play":
			remains.SetTrigger ("open");
			break;
		case "main menu":
			ls.goToScene ("Main");
			break;
		}
	}

	void OnSpeechError (string error)
	{
		switch (int.Parse (error)) {
		case SpeechRecognizerManager.ERROR_AUDIO:
			StatusMessage ("Error during recording the audio.");
			break;
		case SpeechRecognizerManager.ERROR_CLIENT:
			StatusMessage ("Error on the client side.");
			break;
		case SpeechRecognizerManager.ERROR_INSUFFICIENT_PERMISSIONS:
			StatusMessage ("Insufficient permissions. Do the RECORD_AUDIO and INTERNET permissions have been added to the manifest?");
			break;
		case SpeechRecognizerManager.ERROR_NETWORK:
			StatusMessage ("A network error occured. Make sure the device has internet access.");
			break;
		case SpeechRecognizerManager.ERROR_NETWORK_TIMEOUT:
			StatusMessage ("A network timeout occured. Make sure the device has internet access.");
			break;
		case SpeechRecognizerManager.ERROR_NO_MATCH:
			StatusMessage ("No recognition result matched.");
			break;
		case SpeechRecognizerManager.ERROR_NOT_INITIALIZED:
			StatusMessage ("Speech recognizer is not initialized.");
			break;
		case SpeechRecognizerManager.ERROR_RECOGNIZER_BUSY:
			StatusMessage ("Speech recognizer service is busy.");
			break;
		case SpeechRecognizerManager.ERROR_SERVER:
			StatusMessage ("Server sends error status.");
			break;
		case SpeechRecognizerManager.ERROR_SPEECH_TIMEOUT:
			StatusMessage ("No speech input.");
			break;
		default:
			break;
		}

		isListening = false;
	}

	#endregion

	public void startListening()
	{
		isListening = true;
		voiceManager.StartListening (1, "en-US");
	}

	public void stopListening()
	{
		voiceManager.StopListening ();
		isListening = false;
	}

	public void cancelListening()
	{
		voiceManager.CancelListening ();
		isListening = false;
	}


	private void StatusMessage (string message)
	{
		text.text = message;
	}
}
