﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SnapPhoto : MonoBehaviour {

	private WebCamTexture cameraTexture;
	private WebCamTexture frontCam, backCam;
	public GameObject plane;
	public GameLog gl;

	private bool CameraNativeSizeSet = false;
	private int cameraID = 0;

	void Start()
	{
		//Get camera names
		frontCam = new WebCamTexture (WebCamTexture.devices[0].name);
		backCam = new WebCamTexture (WebCamTexture.devices[WebCamTexture.devices.Length - 1].name);

		//Set back as default
		cameraTexture = backCam;

		//Pass Camera feed to Raw Image
		RawImage raw = plane.GetComponent<RawImage> ();
		raw.texture = cameraTexture;
	}

	void Update()
	{
		//Update aspect ratio
		if (cameraTexture.isPlaying && cameraTexture.height > 100 && !CameraNativeSizeSet) {

			//Match aspect ratio
			plane.GetComponent<AspectRatioFitter> ().aspectRatio = (float) cameraTexture.width / (float) cameraTexture.height;

			//Match orientation TODO may not be the correct way to do this
			plane.transform.rotation = plane.transform.rotation * Quaternion.AngleAxis (cameraTexture.videoRotationAngle, Vector3.up);

			//Adjust for mirrored image
			if (cameraTexture.videoVerticallyMirrored)
				plane.GetComponent<RawImage> ().uvRect = new Rect (0, 1, 1, -1);
			else
				plane.GetComponent<RawImage> ().uvRect = new Rect (0, 0, 1, 1);

			CameraNativeSizeSet = true;
		}
	}

	//TODO Not used, may no longer need
	public IEnumerator cameraSettings()
	{
		cameraTexture.Play ();
		CameraNativeSizeSet = false;

		return null;
	}

	//Start Camera
	public void startCamera()
	{
		cameraTexture.Play ();
		CameraNativeSizeSet = false;
	}

	//Stop Camera
	public void stopCamera()
	{
		cameraTexture.Stop ();
		CameraNativeSizeSet = false;
	}

	//Get a photo to use in the game
	public void captureImage()
	{
		//Create a pixel array
		Color32[] pixels = new Color32[cameraTexture.width * cameraTexture.height];

		//Get an image
		//Not encoded so can not use it directly TODO is this correct?
		Texture2D capturedImage = new Texture2D (cameraTexture.width, cameraTexture.height);
		capturedImage.SetPixels32 (cameraTexture.GetPixels32 (pixels));

		//Convert the images to bytes
		byte[] imageBytes = capturedImage.EncodeToPNG ();

		//Create a new image and load the bytes
		Texture2D imageConverted = new Texture2D (cameraTexture.width, cameraTexture.height, capturedImage.format, false);
		imageConverted.LoadImage (imageBytes);

		//Add the image to the log
		gl.addImage (imageConverted, (float) cameraTexture.width / (float) cameraTexture.height);
	}

	//Switch camera being used
	public void cameraSwitch()
	{
		//Stop
		stopCamera ();

		//Switch
		if (cameraTexture.deviceName == frontCam.deviceName)
			cameraTexture = backCam;
		else
			cameraTexture = frontCam;

		//Reset the feed image
		RawImage raw = plane.GetComponent<RawImage> ();
		raw.texture = cameraTexture;

		//Start the camera back up
		startCamera ();
	}

}
