// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.34 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.34;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:33236,y:32461,varname:node_3138,prsc:2|emission-3079-RGB;n:type:ShaderForge.SFN_TexCoord,id:8628,x:32247,y:32490,varname:node_8628,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Lerp,id:8626,x:32810,y:32441,varname:node_8626,prsc:2|A-9237-RGB,B-8991-RGB,T-1595-OUT;n:type:ShaderForge.SFN_Color,id:9237,x:32620,y:32215,ptovrint:False,ptlb:node_9237,ptin:_node_9237,varname:node_9237,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.272,c2:0.1725779,c3:0.06585208,c4:1;n:type:ShaderForge.SFN_Color,id:8991,x:32538,y:32396,ptovrint:False,ptlb:node_8991,ptin:_node_8991,varname:node_8991,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tau,id:6285,x:32284,y:32683,varname:node_6285,prsc:2;n:type:ShaderForge.SFN_Multiply,id:6787,x:32498,y:32717,varname:node_6787,prsc:2|A-8628-U,B-6285-OUT,C-9166-OUT;n:type:ShaderForge.SFN_RemapRange,id:1595,x:32918,y:32779,varname:node_1595,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-1412-OUT;n:type:ShaderForge.SFN_Slider,id:9166,x:32048,y:32829,ptovrint:False,ptlb:node_9166,ptin:_node_9166,varname:node_9166,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:9.230769,max:10;n:type:ShaderForge.SFN_Sin,id:1412,x:32714,y:32755,varname:node_1412,prsc:2|IN-6787-OUT;n:type:ShaderForge.SFN_Tex2d,id:3079,x:32975,y:32464,ptovrint:False,ptlb:node_3079,ptin:_node_3079,varname:node_3079,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:3cba19858e01047688ca9866e5703b41,ntxv:0,isnm:False;proporder:9237-8991-9166-3079;pass:END;sub:END;*/

Shader "Shader Forge/Background" {
    Properties {
        _node_9237 ("node_9237", Color) = (0.272,0.1725779,0.06585208,1)
        _node_8991 ("node_8991", Color) = (0.5,0.5,0.5,1)
        _node_9166 ("node_9166", Range(0, 10)) = 9.230769
        _node_3079 ("node_3079", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _node_3079; uniform float4 _node_3079_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 _node_3079_var = tex2D(_node_3079,TRANSFORM_TEX(i.uv0, _node_3079));
                float3 emissive = _node_3079_var.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
