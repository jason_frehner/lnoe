// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33527,y:32513,varname:node_3138,prsc:2|emission-2110-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32959,y:32636,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1290549,c2:0.5661765,c3:0.2586842,c4:1;n:type:ShaderForge.SFN_TexCoord,id:1297,x:31901,y:32439,varname:node_1297,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ComponentMask,id:1594,x:32189,y:32326,varname:node_1594,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1297-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:5874,x:32193,y:32521,varname:node_5874,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-1297-UVOUT;n:type:ShaderForge.SFN_OneMinus,id:3410,x:32402,y:32319,varname:node_3410,prsc:2|IN-1594-OUT;n:type:ShaderForge.SFN_OneMinus,id:8003,x:32402,y:32476,varname:node_8003,prsc:2|IN-5874-OUT;n:type:ShaderForge.SFN_Min,id:8605,x:32639,y:32374,varname:node_8605,prsc:2|A-3410-OUT,B-8003-OUT,C-5874-OUT,D-1594-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:8251,x:32828,y:32364,varname:node_8251,prsc:2,min:0,max:0.1|IN-8605-OUT;n:type:ShaderForge.SFN_RemapRange,id:1512,x:33003,y:32364,varname:node_1512,prsc:2,frmn:0,frmx:0.1,tomn:-1,tomx:1|IN-8251-OUT;n:type:ShaderForge.SFN_Multiply,id:2110,x:33257,y:32494,varname:node_2110,prsc:2|A-1512-OUT,B-7241-RGB;proporder:7241;pass:END;sub:END;*/

Shader "Shader Forge/BasicSpace" {
    Properties {
        _Color ("Color", Color) = (0.1290549,0.5661765,0.2586842,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float node_1594 = i.uv0.r;
                float node_5874 = i.uv0.g;
                float3 emissive = ((clamp(min(min(min((1.0 - node_1594),(1.0 - node_5874)),node_5874),node_1594),0,0.1)*20.0+-1.0)*_Color.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
