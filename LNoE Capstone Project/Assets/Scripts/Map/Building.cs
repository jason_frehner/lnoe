﻿using System.Collections.Generic;

/// <summary>
/// A Building Class.
/// </summary>
public class Building
{
	//Different for each building.
	private readonly string name;
	private readonly List<int> rollRange;
	private readonly string pickUpItem;
	private readonly int spaces;
	private readonly int numDoors;
	private int numLockedDoors;

	enum ColorChannel
	{
		red, green, blue
	}

	private ColorChannel matColorCode;

	/// <summary>
	/// Initializes a new instance of the <see cref="Building"/> class.
	/// </summary>
	/// <param name="buildingName">Building name.</param>
	/// <param name="range">Roll Range.</param>
	/// <param name="find">Pick up items.</param>
	/// <param name="squares">Squares.</param>
	/// <param name="doors">How many doors.</param>
	/// <param name="pit">Does it have a pit.</param>
	public Building(string buildingName, List<int> range, string find, int squares, int doors, bool pit)
	{
		name = buildingName;
		rollRange = range;
		pickUpItem = find;
		spaces = squares;
		numDoors = doors;
		numLockedDoors = 0;
		HasSpawningPit = pit;
	}

	/// <summary>
	/// Gets the name.
	/// </summary>
	/// <returns>The name.</returns>
	public string GetName()
	{
		return name;
	}

	/// <summary>
	/// Gets the roll range.
	/// </summary>
	/// <returns>The roll range.</returns>
	public List<int> GetRollRange()
	{
		return rollRange;
	}

	/// <summary>
	/// Gets the pick up item.
	/// </summary>
	/// <returns>The pick up item.</returns>
	public string GetPickUpItem()
	{
		return pickUpItem;
	}

	/// <summary>
	/// Gets how many spaces the building has.
	/// </summary>
	/// <returns>How many spaces.</returns>
	public int GetHowManySpaces()
	{
		return spaces;
	}

	/// <summary>
	/// Locks a door.
	/// </summary>
	public void LockADoor()
	{
		numLockedDoors++;
	}

	/// <summary>
	/// Unlocks a door.
	/// </summary>
	public void UnlockADoor()
	{
		numLockedDoors--;
	}

	/// <summary>
	/// Checks if any doors can be locked.
	/// </summary>
	/// <returns>The count.</returns>
	public int HowManyUnlockedDoors()
	{
		return numDoors - numLockedDoors;
	}
	
	public int HowManyLockedDoors()
	{
		return numLockedDoors;
	}

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="Building"/> has a spawning pit.
	/// </summary>
	/// <value><c>true</c> if this instance has spawning pit; otherwise, <c>false</c>.</value>
	public bool HasSpawningPit { get; set; }

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="Building"/> taken over.
	/// </summary>
	/// <value><c>true</c> if taken over; otherwise, <c>false</c>.</value>
	public bool IsTakenOver { get; set; }

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="Building"/> lights out.
	/// </summary>
	/// <value><c>true</c> if lights out; otherwise, <c>false</c>.</value>
	public bool HasLightsOut { get; set; }
	
	public bool IsDestroyed { get; set; }

	/// <summary>
	/// Compare building to prioritize which should be destroyed. 
	/// </summary>
	/// <param name="x">Building to compare.</param>
	/// <param name="y">Building to compare against.</param>
	/// <returns>Compare result</returns>
	public static int CompareDestroy(Building x, Building y)
	{
		if(x.HasSpawningPit != y.HasSpawningPit) // Spawning pit
			return x.HasSpawningPit.CompareTo(y.HasSpawningPit);
		if(x.IsTakenOver != y.IsTakenOver)  // Taken over
			return x.IsTakenOver.CompareTo(y.IsTakenOver);
		if(x.HasLightsOut != y.HasLightsOut) // Lights out
			return x.HasLightsOut.CompareTo(y.HasLightsOut);
		if(x.HowManyUnlockedDoors() != y.HowManyUnlockedDoors()) // Unlocked doors
			return x.HowManyUnlockedDoors().CompareTo(y.HowManyUnlockedDoors());
		return x.spaces.CompareTo(y.spaces); // Number of spaces in building.
//		return x.pickUpItem.CompareTo(y.pickUpItem);
	}
}