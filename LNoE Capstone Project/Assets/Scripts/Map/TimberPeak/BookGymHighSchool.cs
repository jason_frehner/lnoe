using System.Collections.Generic;

public class BookGymHighSchool : MapTile
{
    public BookGymHighSchool()
    {
        Name = "High School, Gym, Book Store";
		
        // Spawning pit not in building.
        // Building share a door, High School owns door for system.
        BuildingsOnTile.Add(new Building("High School", new List<int>(new []{1,2}), "Fire Extinguisher", 6, 3, false));
        BuildingsOnTile.Add(new Building("Gym", new List<int>(new []{3,4}), "Lucky Break", 4, 2, false));
        BuildingsOnTile.Add(new Building("Book Store", new List<int>(new []{5,6}), "", 4, 3, false));
    }
}