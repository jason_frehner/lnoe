using System.Collections.Generic;

public class BowlingTavern : MapTile
{
    public BowlingTavern()
    {
        Name = "Bowling Alley, Tavern";
		
        // Spawning pit not in building.
        // Road out of town.
        BuildingsOnTile.Add(new Building("Bowling Alley", new List<int>(new []{1,2,3}), "Roll to search", 5, 2, false));
        BuildingsOnTile.Add(new Building("Tavern", new List<int>(new []{4,5,6}), "Marker", 4, 2, false));
    }
}