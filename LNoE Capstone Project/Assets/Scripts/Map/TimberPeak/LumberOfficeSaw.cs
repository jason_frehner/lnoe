using System.Collections.Generic;

public class LumberOfficeSaw : MapTile
{
    public LumberOfficeSaw()
    {
        Name = "Saw Mill, Lumber Yard, Office Trailer";
		
        // Road out of town.
        // Spawning pit not in building.
        // Buildings share a door, Saw Mill own for the system.
        BuildingsOnTile.Add(new Building("Saw Mill", new List<int>(new []{1,2}), "Chainsaw", 3, 2, false));
        BuildingsOnTile.Add(new Building("Lumber Yard", new List<int>(new []{3,4}), "Torch", 7, 3, false));
        BuildingsOnTile.Add(new Building("Office Trailer", new List<int>(new []{5,6}), "", 2, 1, false));
    }
}