using System.Collections.Generic;

public class DinerPowerStation : MapTile
{
    public DinerPowerStation()
    {
        Name = "Diner, Power Relay Station";
		
        BuildingsOnTile.Add(new Building("Diner", new List<int>(new []{1,2,3}), "Pump Shotgun", 4, 2, false));
        BuildingsOnTile.Add(new Building("Power Relay Station", new List<int>(new []{4,5,6}), "", 8, 4, true));
        // If destroyed all buildings are lights out.
    }
}