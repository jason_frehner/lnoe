using System.Collections.Generic;

public class RefineryTrain : MapTile
{
    public RefineryTrain()
    {
        Name = "Refinery, Train Station";
		
        BuildingsOnTile.Add(new Building("Refinery", new List<int>(new []{1,2,3}), "Explosive", 6, 3, true));
        BuildingsOnTile.Add(new Building("Train Station", new List<int>(new []{4,5,6}), "Random Discard", 4, 3, false));
    }
}