using System.Collections.Generic;

public class HardwareHospitalPolice : MapTile
{
    public HardwareHospitalPolice()
    {
        Name = "Hardware Store, Hospital, Police Station";
		
        BuildingsOnTile.Add(new Building("Hardware Store", new List<int>(new []{1,2}), "Hand Weapon", 4, 2, false));
        BuildingsOnTile.Add(new Building("Hospital", new List<int>(new []{3,4}), "First Aid", 6, 3, true));
        BuildingsOnTile.Add(new Building("Police Station", new List<int>(new []{5,6}), "Police Shotgun", 2, 2, false));
    }
}