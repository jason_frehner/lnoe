﻿using UnityEngine;

public class PlaceMapImages : MonoBehaviour
{
	public Sprite[] MapImages;
	public SpriteRenderer[] SpriteRenderers = new SpriteRenderer[5];

	private bool isDebugLog;

	private void Awake()
	{
		isDebugLog = PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
	}

	private void Start()
	{
		AssignMapImages();
	}

    /// <summary>
    /// Assigns the map images.
    /// </summary>
    public void AssignMapImages()
	{
		MapImages = new[]
		{
			// Core
			Resources.Load<Sprite>(Strings.Sprites.Manor),
			Resources.Load<Sprite>(Strings.Sprites.Open),
			Resources.Load<Sprite>(Strings.Sprites.AirHosDin),
			Resources.Load<Sprite>(Strings.Sprites.BanJunGas),
			Resources.Load<Sprite>(Strings.Sprites.ChurPol),
			Resources.Load<Sprite>(Strings.Sprites.CornFarm),
			Resources.Load<Sprite>(Strings.Sprites.GenGunPlant),
			Resources.Load<Sprite>(Strings.Sprites.HighGym),
			// Growing Hunger
			Resources.Load<Sprite>(Strings.Sprites.AntFactLib),
			Resources.Load<Sprite>(Strings.Sprites.DrugPostSuper)
		};
		
		
	}

    /// <summary>
    /// Sets the map.
    /// </summary>
    /// <param name="tileNames">The tile names.</param>
    public void SetMap(string[] tileNames)
	{
		// Renderers are null
		if(SpriteRenderers[0] == null)
		{
			if(isDebugLog)
			{
				Debug.Log("Should be testing");
			}
			return;
		}
		//Maps are null
		if(MapImages == null)
		{
			AssignMapImages();
		}
		
		for (int i = 0; i < 5; i++)
		{
			Sprite sprite = null;
			foreach (Sprite image in MapImages)
			{
				if(image == null)
				{
					if(isDebugLog)
					{
						Debug.Log("Place Map Images MapImages are null");
					}
					break;
				}
				if(image.name.Contains(tileNames[i]))
				{
					sprite = image;
				}
				
			}
			if(sprite == null)
			{
				if(isDebugLog)
				{
					Debug.Log("No Image with name: " + tileNames[i]);
				}
				break;
			}
			SpriteRenderers[i].sprite = sprite;
			if(isDebugLog)
			{
				Debug.Log(sprite.name);
			}
			
			// Add material
			Material instanceMaterial = new Material(Resources.Load<Shader>("MapTile"));
			instanceMaterial.SetTexture("_MainTex", sprite.texture);
			instanceMaterial.SetTexture("_EffectsLayers", Resources.Load<Sprite>(sprite.name + " Effects").texture);
			SpriteRenderers[i].material = instanceMaterial;
//			SpriteRenderers[i].material = new Material(Resources.Load<Shader>("MapTile"));
//			SpriteRenderers[i].material.SetTexture("_MainTex", sprite.texture);
			
//			SpriteRenderers[i].material.SetTexture("_EffectsLayers", Resources.Load<Sprite>(sprite.name + " Effects").texture);
		}
	}
}
