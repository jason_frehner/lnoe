using System.Collections.Generic;

public class AntiqueFactoryLibrary : MapTile
{
    public AntiqueFactoryLibrary()
    {
        Name = "Antique Shop, The Factory, Library";
		
        // Spawning pit not in a building.
        // Building share a door, Antique Shop owns that door for the system.
        BuildingsOnTile.Add(new Building("Library", new List<int>(new []{1,2}), "First event", 4, 2, false));
        BuildingsOnTile.Add(new Building("Antique Shop", new List<int>(new []{3,4}), "2 discarded cards back into hero deck", 3, 2, false ));
        BuildingsOnTile.Add(new Building("Factory", new List<int>(new []{5,6}), "Welding Torch", 3, 2, false));
    }
}