using System.Collections.Generic;

public class DrugPostOfficeSupermarket : MapTile
{
    public DrugPostOfficeSupermarket()
    {
        Name = "Drug Store, Post Office, Supermarket";
		
        // Buildings share a door, Drug Store owns that door for the system. Post office owns the other door.
        BuildingsOnTile.Add(new Building("Drug Store", new List<int>(new []{1,2}), "First Aid", 3, 2, false));
        BuildingsOnTile.Add(new Building("Super Market", new List<int>(new []{3,4}), "Discard 3, Draw 2", 10, 2, true));
        BuildingsOnTile.Add(new Building("Post Office", new List<int>(new []{5,6}), "", 2, 2, false));
    }
}