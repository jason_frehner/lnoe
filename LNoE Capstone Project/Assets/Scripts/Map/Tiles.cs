using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Collection of all the tiles.
/// </summary>
public class Tiles
{
    private readonly List<MapTile> coreMapTiles = new List<MapTile>
    {
		
        new BankJunkyardGas(),
        new BarnFarmhouse(),
        new ChurchGraveyardPolice(),
        new HangarHospitalDiner(),
        new SchoolGym(),
        new GeneralGunPlant()
    };
    private readonly List<MapTile> growingHungerMapTiles = new List<MapTile>
    {
        new AntiqueFactoryLibrary(),
        new DrugPostOfficeSupermarket()
    };
    private List<MapTile> fourRandomTilesForGame = new List<MapTile>();

    /// <summary>
    /// Gets the four random tiles for the game.
    /// </summary>
    /// <returns>The four random tiles for game.</returns>
    public List<MapTile> GetFourRandomTilesForGame()
    {
        return fourRandomTilesForGame;
    }

    /// <summary>
    /// Set the four random tiles needed for the game.
    /// </summary>
    public void GameTiles()
    {
        List<MapTile> tempList = coreMapTiles;
        if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.GH) == 1)
        {
            tempList.AddRange(growingHungerMapTiles);
        }

        fourRandomTilesForGame = new List<MapTile> ();

        // Add tiles from lost game
        if(PlayerPrefs.HasKey("Tile1"))
        {
            List<string> keys = new List<string>{"Tile1", "Tile2", "Tile3", "Tile4"};

            foreach(string key in keys)
            {
                foreach(MapTile tile in tempList)
                {
                    if(tile.GetName() == PlayerPrefs.GetString(key))
                    {
                        fourRandomTilesForGame.Add(tile);
                    }
                }
            }
        }
        else // Get random tiles
        {
            while(fourRandomTilesForGame.Count < 4)
            {
                int x = Random.Range(0, tempList.Count);
                fourRandomTilesForGame.Add(tempList[x]);
                tempList.RemoveAt(x);
            }
        }

        // Save the tile names.
        PlayerPrefs.SetString("Tile1", fourRandomTilesForGame[0].GetName());
        PlayerPrefs.SetString("Tile2", fourRandomTilesForGame[1].GetName());
        PlayerPrefs.SetString("Tile3", fourRandomTilesForGame[2].GetName());
        PlayerPrefs.SetString("Tile4", fourRandomTilesForGame[3].GetName());
    }

    /// <summary>
    /// Gets the random building on game tiles.
    /// </summary>
    /// <returns>A random building on game tiles.</returns>
    public Building GetRandomBuildingInGameTiles()
    {
        int x = Random.Range (0, fourRandomTilesForGame.Count);

        MapTile t = fourRandomTilesForGame [x];

        Building b = t.RandomBuilding ();

        return b;
    }

    /// <summary>
    /// Find the building on the board with the game.
    /// </summary>
    /// <param name="name">Name of building to find.</param>
    /// <returns>The building.</returns>
    public Building GetBuildingByName(string name)
    {
        foreach(MapTile mapTile in fourRandomTilesForGame)
        {
            foreach(Building building in mapTile.GetBuildingsOnTile())
            {
                if(building.GetName() == name)
                {
                    return building;
                }
            }
        }

        return null;
    }

    /// <summary>
    /// Gets a string list of all the buildings being used in this game session.
    /// </summary>
    /// <returns>The string list.</returns>
    public string GetListOfBuildingsInThisGameSession()
    {
        StringBuilder buildingList = new StringBuilder();
		
        foreach (MapTile mapTile in fourRandomTilesForGame)
        {
            foreach (Building building in mapTile.GetBuildingsOnTile())
            {
                buildingList.Append(building.GetName() + ",");
            }
        }

        buildingList.Remove(buildingList.Length - 1, 1);
        return buildingList.ToString();
    }

    /// <summary>
    /// Create a priority list of buildings to be destroyed.
    /// </summary>
    /// <returns>List of building names.</returns>
    public List<string> GetBuildingToDestroyPriority()
    {
        List<Building> buildingsInGame = new List<Building>();
		
        foreach(MapTile mapTile in fourRandomTilesForGame)
        {
            foreach(Building building in mapTile.GetBuildingsOnTile())
            {
                if(!building.IsDestroyed)
                {
                    buildingsInGame.Add(building);
                }
            }
        }

        buildingsInGame.Sort(Building.CompareDestroy);
		
        List<string> listSorted = new List<string>();
        foreach(Building building in buildingsInGame)
        {
            listSorted.Add(building.GetName());
        }

        return listSorted;
    }

    /// <summary>
    /// Get all the tile names.
    /// </summary>
    /// <returns></returns>
    public string[] GetStringArrayOfTileNames(string centerTile)
    {
        string[] result = new string[5];
        result[0] = centerTile;
        result[1] = fourRandomTilesForGame[0].GetBuildingsOnTile()[0].GetName();
        result[2] = fourRandomTilesForGame[1].GetBuildingsOnTile()[0].GetName();
        result[3] = fourRandomTilesForGame[2].GetBuildingsOnTile()[0].GetName();
        result[4] = fourRandomTilesForGame[3].GetBuildingsOnTile()[0].GetName();
        return result;
    }

    /// <summary>
    /// Zombie Chooses a Building.
    /// </summary>
    /// <returns>The building name.</returns>
    public string BuildingZombieChoiceAi()
    {
        return GetRandomBuildingInGameTiles().GetName();//TODO get from AI choice US#129
    }

    /// <summary>
    /// Counts how many buildings are Taken Over.
    /// </summary>
    /// <returns>The count.</returns>
    public int HowManyBuildingAreTakenOver()
    {
        int count = 0;
        count += fourRandomTilesForGame.Sum(
            mapTile => mapTile.GetBuildingsOnTile().Count(building => building.IsTakenOver));
        return count;
    }

    /// <summary>
    /// Counts how many buildings have Lights Out.
    /// </summary>
    /// <returns>The count.</returns>
    public int HowManyBuildingsHaveLightsOut()
    {
        int count = 0;
        count += fourRandomTilesForGame.Sum(
            maptile => maptile.GetBuildingsOnTile().Count(building => building.HasLightsOut));
        return count;
    }

    /// <summary>
    /// Counts how many buildings have Spawning Pits.
    /// </summary>
    /// <returns>The count.</returns>
    public int HowManyBuildingsHaveSpawningPits()
    {
        int count = 0;
        count += fourRandomTilesForGame.Sum(
            maptile => maptile.GetBuildingsOnTile().Count(building => building.HasSpawningPit));
        return count;
    }
	
	
}