using System.Collections.Generic;

public class SchoolGym : MapTile
{
    public SchoolGym()
    {
        Name = "High School, Gym";

        // Spawning pit not in a building.
        // Buildings share a door, high school owns that door for the system.
        BuildingsOnTile.Add (new Building("High School", new List<int>(new[]{1,2,3}), "", 6, 2, false));
        BuildingsOnTile.Add (new Building("Gym", new List<int>(new[]{4,5,6}), "", 4, 1, false));
    }
}