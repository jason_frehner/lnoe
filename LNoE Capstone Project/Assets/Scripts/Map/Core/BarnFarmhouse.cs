using System.Collections.Generic;

public class BarnFarmhouse : MapTile
{
    public BarnFarmhouse()
    {
        Name = "Barn, Farmhouse";

        //Also has Cornfield, players will still have to do the check for the zombies
        BuildingsOnTile.Add (new Building("The Barn", new List<int>(new[]{1,2,3}), "Pitchfork", 4, 1, true));
        BuildingsOnTile.Add (new Building("Farmhouse", new List<int>(new[]{4,5,6}), "", 2, 2, false));
    }
}