using System.Collections.Generic;

public class ChurchGraveyardPolice : MapTile
{
    public ChurchGraveyardPolice()
    {
        Name = "Church, Police Station";

        //spawning pit is in the graveyard
        BuildingsOnTile.Add (new Building("Church", new List<int>(new[]{1,2,3}), "Faith", 4, 1, false));
        //BuildingsOnTile.Add (new Building("Graveyard", null, "", 4, true));
        BuildingsOnTile.Add (new Building("Police Station", new List<int>(new[]{4,5,6}), "Pump Shotgun", 2, 1, false));
    }
}