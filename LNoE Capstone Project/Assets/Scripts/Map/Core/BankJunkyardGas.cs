using System.Collections.Generic;

public class BankJunkyardGas : MapTile
{
    public BankJunkyardGas()
    {
        Name = "Bank, Junkyard, Gas Station";

        //Also has road out of town
        //Junkyard and gas station share a door, junkyard owns the door for this system.
        BuildingsOnTile.Add (new Building("Bank", new List<int>(new[]{1,2}), "", 4, 2, false));
        BuildingsOnTile.Add (new Building("Junkyard", new List<int>(new[]{3,4}), "Random Discard", 6, 3, true));
        BuildingsOnTile.Add (new Building("Gas Station", new List<int>(new[]{5,6}), "Gasoline", 4, 2, false));
    }
}