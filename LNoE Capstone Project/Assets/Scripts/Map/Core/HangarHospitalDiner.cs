using System.Collections.Generic;

public class HangarHospitalDiner : MapTile
{
    public HangarHospitalDiner()
    {
        Name = "Airplane Hangar, Hospital, Diner";

        BuildingsOnTile.Add (new Building("Airplane Hangar", new List<int>(new[]{1,2}), "Signal Flare", 4, 2, false));
        BuildingsOnTile.Add (new Building("Hospital", new List<int>(new[]{3,4}), "First Aid", 7, 2, true));
        BuildingsOnTile.Add (new Building("Diner", new List<int>(new[]{5,6}), "Meat Cleaver", 4, 2, false));
    }
}