using System.Collections.Generic;

public class GeneralGunPlant : MapTile
{
    public GeneralGunPlant()
    {
        Name = "General Store, Gun Shop, Plant";

        BuildingsOnTile.Add (new Building("General Store", new List<int>(new[]{1,2}), "", 4, 2, false));
        BuildingsOnTile.Add (new Building("Gun Shop", new List<int>(new[]{3,4}), "Any Gun or Ammo", 4, 2, false));
        BuildingsOnTile.Add (new Building("Plant", new List<int>(new[]{5,6}), "", 3, 1, true));
    }
}