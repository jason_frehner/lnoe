using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// A Map tile Class.
/// </summary>
public class MapTile
{
    protected readonly List<Building> BuildingsOnTile= new List<Building>();
    protected string Name;

    /// <summary>
    /// Range total on tile.
    /// Used as a check.
    /// </summary>
    /// <returns>The range total.</returns>
    public int TileRangeTotal()
    {
        return BuildingsOnTile.Where(b => b.GetRollRange() != null).Sum(b => b.GetRollRange().Sum());
    }

    /// <summary>
    /// Gets a random building.
    /// </summary>
    /// <returns>The building.</returns>
    public Building RandomBuilding()
    {
        int x = Random.Range (0, BuildingsOnTile.Count);

        return BuildingsOnTile [x];
    }

    /// <summary>
    /// Gets the buildings on tile.
    /// </summary>
    /// <returns>The buildings on tile.</returns>
    public List<Building> GetBuildingsOnTile()
    {
        return BuildingsOnTile;
    }

    /// <summary>
    /// Gets the name.
    /// </summary>
    /// <returns>The name.</returns>
    public string GetName()
    {
        return Name;
    }
}