using System.Collections.Generic;

public class RangerStation : MapTile
{
    public RangerStation()
    {
        Name = "Ranger Station";
		
        // Spawning pit not in building.
        // Other roll range values are wood spaces.
        BuildingsOnTile.Add(new Building("Ranger Station", new List<int>(new []{3,4}), "First Aid, Flare", 1, 0, false));
    }
}