using System.Collections.Generic;

public class OldCabin : MapTile
{
    public OldCabin()
    {
        Name = "Old Cabin";
		
        // Spawning pit not in building.
        // Other roll range values are wood spaces.
        BuildingsOnTile.Add(new Building("Old Cabin", new List<int>(new []{3}), "", 1, 0, false));
    }
}