using System.Collections.Generic;

public class TowerRelay : MapTile
{
    public TowerRelay()
    {
        Name = "Tower Relay";
		
        // Spawning pit not in building.
        // Other roll range values are wood spaces.
        BuildingsOnTile.Add(new Building("Tower Relay", new List<int>(new []{4,5}), "Event", 1, 0, false));
    }
}