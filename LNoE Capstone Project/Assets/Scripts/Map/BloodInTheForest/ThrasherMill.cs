using System.Collections.Generic;

public class ThrasherMill : MapTile
{
    public ThrasherMill()
    {
        Name = "Thrasher's Mill, Mine Shaft";
		
        // Spawning pit not in building.
        // Other roll range values are wood spaces.
        BuildingsOnTile.Add(new Building("Thrasher's Mill", new List<int>(new []{1,2}), "", 1, 0, false));
        BuildingsOnTile.Add(new Building("Mine Shaft", new List<int>(new []{4}), "Dynamite", 1, 0, false));
    }
}