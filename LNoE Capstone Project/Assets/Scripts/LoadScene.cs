﻿using System.Collections;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Load scene.
/// 
/// Displaying loading screen if needed:
/// http://blog.teamtreehouse.com/make-loading-screen-unity
/// </summary>
public class LoadScene : MonoBehaviour
{

	public GameObject LoadingScreen;
//	private bool loadScene = false;
	private AudioSource clickAudio;

	private void Start()
	{
		clickAudio = GameObject.Find(Strings.GameObjectNames.AudioClick).GetComponent<AudioSource>();
	}

	/// <summary>
	/// Go to scene.
	/// </summary>
	/// <param name="sceneToGoTo">Scene to go to.</param>
	[UsedImplicitly]
	public void GoToScene(string sceneToGoTo)
	{
		SceneManager.LoadScene (sceneToGoTo);
		clickAudio.Play();
	}

	/// <summary>
	/// Go to scene and display loading screen.
	/// </summary>
	/// <param name="sceneToGoTo"></param>
	[UsedImplicitly]
	public void GoToSceneWithLoadingScreen(string sceneToGoTo)
	{
		if(SceneManager.GetActiveScene().name == "Game")
		{
			GameObject.Find("ZombieManager").GetComponent<ZombieTurn>().AutoSaveGame();
		}
		
		StartCoroutine(LoadingScene(sceneToGoTo));

		if (LoadingScreen)
		{
			LoadingScreen.SetActive(true);
		}
		
		clickAudio.Play();
	}

	/// <summary>
	/// Quit this instance.
	/// </summary>
	[UsedImplicitly]
	public void Quit()
	{
		Application.Quit ();
	}

	/// <summary>
	/// Coroutine for loading scene so game isn't momentary frozen.
	/// </summary>
	/// <param name="scene">Scene to load</param>
	/// <returns></returns>
	private static IEnumerator LoadingScene(string scene)
	{
		AsyncOperation async = SceneManager.LoadSceneAsync(scene);

		while (!async.isDone) {
			yield return null;
		}
	}
}
