﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Adapted from James McCaffrey
/// Neural Network Using C# Succinctly
/// </summary>
public class Normalize : MonoBehaviour {
	
	/// <summary>
	/// Example of raw data
	/// </summary>
	private readonly string[] sourceData = new string[]
	{
		"Sex     Age    Locale     Income    Politices",
		"===============================================",
		"Male    25     Rural      62,000     Conservative",
		"Female  36     Suburban   55,000     Liberal",
		"Male    40     Urban      74,000     Moderate",
		"Female  23     Rural      28,000     Liberal"
	};

	/// <summary>
	/// Example of encoded data.
	/// </summary>
	private readonly string[] encodedData = new string[]
	{
		"-1   25   1   0   63000   1 0 0",
		" 1   36   0   1   55000   0 1 0",
		"-1   40  -1  -1   74000   0 0 1",
		" 1   23   1   0   28000   0 1 0"
	};
	
	/// <summary>
	/// Matrix to hold data
	/// </summary>
	private readonly double[][] numericData = new double[4][];

	private void Start () {
		Debug.Log("Begin normalization demo");
		
		Debug.Log("Dummy data");
		ShowData(sourceData);
		
		Debug.Log("Endoded data");
		ShowData(encodedData);
		
		//Add data to matrix
		numericData[0] = new[] {-1, 25.0, 1, 0, 63000, 1, 0, 0};
		numericData[1] = new[] { 1, 36.0, 0, 1, 55000, 0, 1, 0};
		numericData[2] = new[] {-1, 40.0,-1,-1, 74000, 0, 0, 1};
		numericData[3] = new[] { 1, 23.0, 1, 0, 28000, 0, 1, 0};

		ShowMatrix(numericData, 2);

		GaussNormal(numericData, 1);
		MinMaxNormal(numericData, 4);
		
		Debug.Log("After Normalization");
		
		ShowMatrix(numericData, 2);
		
		Debug.Log("End normalization demo");
	}

	/// <summary>
	/// Prints each string in an Array to the log.
	/// </summary>
	/// <param name="rawData">An Array of Strings.</param>
	private static void ShowData(IEnumerable<string> rawData)
	{
		foreach (string s in rawData)
		{
			Debug.Log(s);
		}
		Debug.Log("");
	}

	/// <summary>
	/// Prints out all doubles in an array out.
	/// Shows the to a set number of decimals.
	/// </summary>
	/// <param name="matrix">Array of doubles</param>
	/// <param name="decimals">How many deciamls to show.</param>
	private static void ShowMatrix(IEnumerable<double[]> matrix, int decimals)
	{
		foreach (double[] t in matrix)
		{
			foreach (double t1 in t)
			{
				double v = Math.Abs(t1);
				var s = t1 >= 0.0 ? " " : "-";
				Debug.Log(s + v.ToString("F" + decimals).PadRight(5) + " ");
			}
			Debug.Log("");
		}
	}

	/// <summary>
	/// (x - mean) / standard deviation
	/// </summary>
	/// <param name="data">Array of doubles</param>
	/// <param name="column">What column to normalize</param>
	private static void GaussNormal(ICollection<double[]> data, int column)
	{
		double sum = data.Sum(i => i[column]);
		double mean = sum / data.Count;

		double sumSquares = data.Sum(i => (i[column] - mean) * (i[column] - mean));
		double steDev = Math.Sqrt(sumSquares / data.Count);

		foreach (double[] i in data)
		{
			i[column] = (i[column] - mean) / steDev;
		}
	}

	/// <summary>
	/// (x - min) / (max - min)
	/// </summary>
	/// <param name="data">Array of doubles</param>
	/// <param name="column">What column to normalize</param>
	private static void MinMaxNormal(IList<double[]> data, int column)
	{
		double min = data[0][column];
		double max = data[0][column];

		foreach (double[] i in data)
		{
			if (i[column] < min)
				min = i[column];
			if (i[column] > max)
				max = i[column];
		}
		double range = max - min;
		if (Math.Abs(range) < 0.0)
		{
			foreach (double[] i in data)
				i[column] = 0.5;
			return;
		}

		foreach (double[] i in data)
		{
			i[column] = (i[column] - min) / range;
		}
	}
}