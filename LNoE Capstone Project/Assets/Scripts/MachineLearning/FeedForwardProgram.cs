﻿using System.Collections;
using System.Linq;
using UnityEngine;

/// <summary>
/// Adapted from James McCaffrey
/// Neural Network Using C# Succinctly
/// </summary>
public class FeedForwardProgram : MonoBehaviour
{
	private const int NumInput = 3;
	private const int NumHidden = 4;
	private const int NumOutput = 2;

	private readonly double[] weights = {
		0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.10,
		0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.20,
		0.21, 0.22, 0.23, 0.24, 0.25, 0.26
	};

	private readonly double[] inputValues = {
		1.0, 2.0, 3.0
	};


	private IEnumerator Start () {
		Debug.Log("Feed Forwqrd Demo");
		
		NeuralNetwork nn = new NeuralNetwork(NumInput, NumHidden, NumOutput);
		yield return new WaitForEndOfFrame();
		Debug.Log("Weights");
		ShowVector(weights);
		
		Debug.Log("Inputs");
		ShowVector(inputValues);
		yield return new WaitForEndOfFrame();
		double[] outputValues = nn.ComputeOutputs(inputValues);
		Debug.Log("Outputs");
		ShowVector(outputValues);
		yield return new WaitForEndOfFrame();
		Debug.Log("End Demo");
	}

	private static void ShowVector(double[] vector)
	{
		string s = vector.Aggregate("", (current, d) => current + (d + " "));

		Debug.Log(s);
	}

	
}