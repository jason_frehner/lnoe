using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

/// <summary>
/// A Perceptron Class
/// </summary>
public class Perceptron
{
    private readonly int numInput;
    private readonly double[] inputs;
    private readonly double[] weights;
    private double bias;
    private readonly Random rnd;

    /// <summary>
    /// Persecptron Construction
    /// </summary>
    /// <param name="numInput">The number of Inputs it has</param>
    public Perceptron(int numInput)
    {
        this.numInput = numInput;
        inputs = new double[numInput];
        weights = new double[numInput];
        rnd = new Random(0);
        InitializeWeights();
    }

    /// <summary>
    /// Set all the inital weights.
    /// </summary>
    private void InitializeWeights()
    {
        const double low = -0.01;
        const double high = 0.01;

        for (int i = 0; i < weights.Length; i++)
            weights[i] = (high - low) * rnd.NextDouble() + low;
        bias = (high - low) * rnd.NextDouble() + low;
    }

    /// <summary>
    /// Calculate the Perceptron output.
    /// </summary>
    /// <param name="inputValues">Array of doubles</param>
    /// <returns>The Result</returns>
    /// <exception cref="Exception">Array not the right size.</exception>
    public int ComputeOutput(double[] inputValues)
    {
        if (inputValues.Length != numInput)
            throw new Exception("Bad inputValues in ComputeOutput");
		
        // Store the input values
        for (int i = 0; i < inputValues.Length; i++)
            inputs[i] = inputValues[i];
		
        // Sum of all the weights * their input.
        double sum = 0.0;
        for (int i = 0; i < numInput; i++)
            sum += inputs[i] * weights[i];
		
        // Add the bias.
        sum += bias;
		
        // Get the result.
        int result = Activation(sum);
        return result;
    }

    /// <summary>
    /// Simple activation function.
    /// </summary>
    /// <param name="value">The computed output.</param>
    /// <returns>1 or -1</returns>
    private static int Activation(double value)
    {
        if (value >= 0.0)
            return +1;
        return -1;
    }

    /// <summary>
    /// Shuffles a list
    /// </summary>
    /// <param name="sequence"></param>
    private void Shuffle(IList<int> sequence)
    {
        for (int i = 0; i < sequence.Count; i++)
        {
            int r = rnd.Next(i, sequence.Count);
            int tmp = sequence[r];
            sequence[r] = sequence[i];
            sequence[i] = tmp;
        }
    }

    /// <summary>
    /// Simple training of a perceptron.
    /// 
    /// loop
    /// 	for each training item
    /// 		compute output using x-values
    /// 		compare computed output to known output
    /// 		if computed is too large
    /// 			make weights and bias values smaller
    /// 		else if computed is too small
    /// 			make weights and bias values larger end if
    /// 		end if
    /// 	end for
    /// end loop
    /// 
    /// </summary>
    /// <param name="trainData">The training data to use.</param>
    /// <param name="alpha">learn rate</param>
    /// <param name="maxEpochs">How many times to train.</param>
    /// <returns>The trained weights</returns>
    public IEnumerable<double> Train(double[][] trainData, double alpha, int maxEpochs)
    {
        int epoch = 0;
        double[] inputValues = new double[numInput];

        int[] sequence = new int[trainData.Length];
        for (int i = 0; i < sequence.Length; i++)
        {
            sequence[i] = i;
        }

        while (epoch < maxEpochs)
        {
            Shuffle(sequence);
            for (int i = 0; i < trainData.Length; i++)
            {
                int index = sequence[i];
                Array.Copy(trainData[index], inputValues, numInput);
                var desired = (int)trainData[index][numInput];
                int computed = ComputeOutput(inputValues);
                UpdateWeights(computed, desired, alpha);
            }
            epoch++;
            Debug.Log("Epcoch: " + epoch);
        }
        double[] results = new double[numInput +1];
        Array.Copy(weights, results, numInput);
        results[results.Length - 1] = bias;
        return results;
    }

    /// <summary>
    /// Adjust the weights based on the results
    /// </summary>
    /// <param name="computed"></param>
    /// <param name="desired"></param>
    /// <param name="alpha"></param>
    private void UpdateWeights(int computed, int desired, double alpha)
    {
        if (computed == desired) return;

        int delta = computed - desired;
        Debug.Log("Delta: " + delta);

        for (int i = 0; i < weights.Length; i++)
        {
            // If computed is greater than desired
            if (computed > desired && inputs[i] >= 0.0) // Input is positive, decrease weight
                weights[i] = weights[i] - (alpha * delta * inputs[i]);
            else if (computed > desired && inputs[i] < 0.0) // Input is negitive, increase weight
                weights[i] = weights[i] + (alpha * delta * inputs[i]);
            // If computed is less than desired
            else if (computed < desired && inputs[i] >= 0.0) // Input is positive, decrease weight
                weights[i] = weights[i] - (alpha * delta * inputs[i]);
            else if (computed < desired && inputs[i] < 0.0) // Input is negitive, increase weight
                weights[i] = weights[i] + (alpha * delta * inputs[i]);
        }
        bias = bias - (alpha * delta);
    }
}