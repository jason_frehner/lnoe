﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// For each output
/// 	for each input set
/// 		the sum of all
/// 			output weight * Gaussian of (distance from(vector point - vector center)
/// 		+ output biases 
/// 
/// Heaton also uses a weight on the input vector
/// Some examples do not use a bias
/// 
/// Some examples also have adjustable widths for the Gaussian
/// 
/// Gaussian = e^ -(width * distance^2)
/// 
/// Adapted from Jeff Heaton's books
/// Artificial Intelligence for Humans
/// 
/// Also referenced James McCaffrey's articles on RBF networks
/// 
/// </summary>
public class RadialBasisFunctionNetwork
{
	private int numInputs;
	private int numRadialBasisFunctions;
	private int numOutputs;
	private double[] inputs;
	private List<RBFNode> rbfNodes;
	private double[] outputBiases;
	private double[] outputs;

	private bool IsDebugLog()
	{
		return PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
	}

	/// <summary>
	/// Constructor for RBF network.
	/// Used when all details will be set dynamically.
	/// </summary>
	public RadialBasisFunctionNetwork()
	{}

	/// <summary>
	/// Constructor for the RBF network
	/// </summary>
	/// <param name="numInputs">Number of inputs.</param>
	/// <param name="numRbfNodes">Number of RBF nodes.</param>
	/// <param name="numOutputs">Number of outputs.</param>
	public RadialBasisFunctionNetwork(int numInputs, int numRbfNodes, int numOutputs)
	{
		this.numInputs = numInputs;
		numRadialBasisFunctions = numRbfNodes;
		this.numOutputs = numOutputs;
		CreateNetworkArrays();
	}

	/// <summary>
	/// Helper for settings up network
	/// </summary>
	private void CreateNetworkArrays()
	{
		inputs = new double[numInputs];
		rbfNodes = new List<RBFNode>();
		for (int i = 0; i < numRadialBasisFunctions; i++)
		{
			rbfNodes.Add(new RBFNode(numInputs, numRadialBasisFunctions, numOutputs));
		}
		outputBiases = new double[numOutputs];
		for (int i = 0; i < outputBiases.Length; i++)
		{
			outputBiases[i] = 0; //Random.Range(-0.1f, 0.1f);
		}
		outputs = new double[numOutputs];
	}

	/// <summary>
	/// Compute the output for a give input array.
	/// </summary>
	/// <param name="inputValues">Array of inputs</param>
	/// <returns>The Result</returns>
	public double[] ComputeOutputs(double[] inputValues)
	{
		// Copy over the input values
		Array.Copy(inputValues, inputs, inputs.Length);
		
		// Where to store the sums
		double[] outputSums = new double[numOutputs];

		// For each output
		for (int i = 0; i < numOutputs; i++)
		{
			double sum = 0.0;
			// For each RBF
			for (int j = 0; j < numRadialBasisFunctions; j++)
			{
				// Add product of the RBF result to the weight
				sum += rbfNodes[j].WeightedFunctionOut(i, inputs);
			}
			
			// Add the bias
			sum += outputBiases[i];
			outputSums[i] = sum;
		}

		// Convert results to softmax
		// Total of all results sum to 1
		double[] softOut = Softmax(outputSums);
		for (int i = 0; i < numOutputs; i++)
		{
				outputs[i] = softOut[i];
		}

		return outputs;
	}
	
	/// <summary>
	/// Makes all outputs sum to 1.
	/// </summary>
	/// <param name="oSums">Array of outputs</param>
	/// <returns>The converted array.</returns>
	public double[] Softmax(double[] oSums)
	{
		// Get the max value from the array.
		double max = oSums[0];
		max = oSums.Concat(new[] {max}).Max();

		// Get scaling factor - sum of exp(each val - max).
		double scale = oSums.Sum(value => Math.Exp(value - max));

		// Convert each result.
		double[] result = new double[oSums.Length];
		for (int i = 0; i < oSums.Length; i++)
		{
			result[i] = Math.Exp(oSums[i] - max) / scale;
		}

		if(IsDebugLog())
		{
			Debug.Log("softmax " + result[0] + " " + result[1]);
		}
		return result;
	}

	/// <summary>
	/// Train the RBF network.
	/// </summary>
	/// <param name="trainData">Data to train from</param>
	public void Train(double[][] trainData)
	{
		if(IsDebugLog())
		{
			Debug.Log("Begin Training");
		}
		
		// Kmeans
		double[][] centers = Kmeans(trainData, numRadialBasisFunctions);
		for (int i = 0; i < numRadialBasisFunctions; i++)
		{
			Array.Copy(centers[i], rbfNodes[i].Centers, numInputs);
			rbfNodes[i].Width = centers[i][numInputs];
		}
		
		// Least Mean Square
		// As everything past the RBF nodes are just linear equations
		// This can quickly get a satisfactory result.
		// Rather than a slow iteration process with gradient descent.
		// This version Adapted from
		// http://csharphelper.com/blog/2014/10/find-a-linear-least-squares-fit-for-a-set-of-points-in-c/
		
		// Collect the various sums needed.
		double[] sumRbf = new double[numRadialBasisFunctions];
		double[] sumOutputs = new double[numOutputs];
		double[] sumRbfSquared = new double[numRadialBasisFunctions];
		double[][] sumRbfOutputProduct = new double[numRadialBasisFunctions][];
		for (int i = 0; i < sumRbfOutputProduct.Length; i++)
		{
			sumRbfOutputProduct[i] = new double[numOutputs];
		}
		
		// Gather all the sums from each training data
		for (int i = 0; i < trainData.Length; i++)
		{
			ComputeOutputs(trainData[i]);

			for (int j = 0; j < numRadialBasisFunctions; j++)
			{
				sumRbf[j] += rbfNodes[j].FunctionOut;
				sumRbfSquared[j] += rbfNodes[j].FunctionOut * rbfNodes[j].FunctionOut;

				for (int k = 0; k < numOutputs; k++)
				{
					sumRbfOutputProduct[j][k] += rbfNodes[j].FunctionOut * trainData[i][k + numInputs];
				}
			}

			for (int j = 0; j < numOutputs; j++)
			{
				sumOutputs[j] += trainData[i][j + numInputs];
			}
		}
		
		// Calculate the weight and biases
		for (int i = 0; i < numRadialBasisFunctions; i++)
		{
			for (int j = 0; j < numOutputs; j++)
			{
				rbfNodes[i].ToOutputWeights[j] = (sumRbfOutputProduct[i][j] * trainData.Length - sumRbf[i] * sumOutputs[j])
												/ (sumRbfSquared[i] * trainData.Length - sumRbf[i] * sumRbf[i]);
				
				outputBiases[j] += (sumRbfOutputProduct[i][j] * sumRbf[i] - sumOutputs[j] * sumRbfSquared[i]) / 
				                   (sumRbf[i] * sumRbf[i] - trainData.Length * sumRbfSquared[i]);
			}	
		}
		
		// Average each biases
		for (int i = 0; i < outputBiases.Length; i++)
		{
			outputBiases[i] = outputBiases[i] / numRadialBasisFunctions;
		}


		if(IsDebugLog())
		{
			Debug.Log("    current accuracy = " + Accuracy(trainData) + 
			          "    current error: " + MeanSquaredError(trainData));
			Debug.Log("End Training");
		}
	}

	/// <summary>
	/// Get the error used to check against a stopping threshold.
	/// </summary>
	/// <param name="trainData">The data</param>
	/// <returns>The error</returns>
	public double MeanSquaredError(double[][] trainData)
	{
		double sumSquaredError = 0.0;
		double[] inputValues = new double[numInputs];
		double[] targetValues = new double[numOutputs];

		// For each data element
		foreach (double[] data in trainData)
		{
			
			Array.Copy(data, inputValues, numInputs);
			Array.Copy(data, numInputs, targetValues, 0, numOutputs);
			double[] yValues = ComputeOutputs(inputValues);

			// Get the difference of the result to the target
			// Square it and add it to the sum
			for (int j = 0; j < numOutputs; j++)
			{
				double error = targetValues[j] - yValues[j];
				sumSquaredError += error * error;
			}
		}
		double result = sumSquaredError / trainData.Length;
		return result;
	}

	/// <summary>
	/// Get the accuracy of the data
	/// </summary>
	/// <param name="testData">The data</param>
	/// <returns>The accuracy</returns>
	public double Accuracy(double[][] testData)
	{
		int numCorrect = 0;
		int numWrong = 0;
		double[] inputValues = new double[numInputs];
		double[] targetValues = new double[numOutputs];

		// For each data element
		foreach (double[] data in testData)
		{
			Array.Copy(data, inputValues, numInputs);
			Array.Copy(data, numInputs, targetValues, 0, numOutputs);
			double[] resultValues = ComputeOutputs(inputValues);
			int maxIndex = MaxIndex(resultValues);

			// Check if the result index is the same as the target
			if (Math.Abs(targetValues[maxIndex] - 1.0) < 0.0000000001)
			{
				numCorrect++;
			}
			else
			{
				numWrong++;
			}
		}
		return (numCorrect * 1.0) / (numCorrect + numWrong);
	}
	
	/// <summary>
	/// Get the index that has the largest value.
	/// </summary>
	/// <param name="vector">The vector to search</param>
	/// <returns>The index</returns>
	private static int MaxIndex(double[] vector)
	{
		int bigIndex = 0;
		double biggestValue = vector[0];
		for (int i = 0; i < vector.Length; i++)
		{
			// ReSharper disable once InvertIf
			if (vector[i] > biggestValue)
			{
				biggestValue = vector[i];
				bigIndex = i;
			}
		}
		return bigIndex;
	}
	

	/// <summary>
	/// K-means clustering
	/// 
	/// Randomly assign inputs to a cluster
	/// loop until no change
	/// 	find cluster center
	/// 	assign input to closest center
	/// 
	/// </summary>
	/// <param name="trainData">Values.</param>
	/// <param name="numClusters">Number of clusters to use</param>
	/// <returns>The center values.</returns>
	private double[][] Kmeans(double[][] trainData, int numClusters)
	{
		// Create the results array
		double[][] results = new double[numClusters][];
		
		// Create the centers
		List<ClusterCenter> centers = new List<ClusterCenter>();
		for (int i = 0; i < numClusters; i++)
		{
			centers.Add(new ClusterCenter(i, new double[numInputs]));
		}

		// Using a cluster point class to hold each input value and track what cluster it belongs to.
		List<ClusterPoint> inputPoints = new List<ClusterPoint>();
		for (int i = 0; i < trainData.Length; i++)
		{
			double[] dataInputs = new double[numInputs];
			Array.Copy(trainData[i], dataInputs, numInputs);
			inputPoints.Add(new ClusterPoint(dataInputs, Random.Range(0, numClusters)));
		}

		bool centersChanged = true;
		int iteration = 0;
		while (centersChanged)
		{
			iteration++;
			centersChanged = false;
			// Finding the centers for each cluster.
			foreach (ClusterCenter center in centers)
			{
				double[] sum = new double[numInputs];
				int pointsInCluster = 0;

				foreach (ClusterPoint point in inputPoints)
				{
					if (point.AssignedCluster == center.ClusterId)
					{
						for (int i = 0; i < numInputs; i++)
						{
							sum[i] += point.Value[i];
						}
						pointsInCluster++;
					}
				}

				// Get new center value
				double[] newCenterValue = new double[numInputs];
				for (int i = 0; i < newCenterValue.Length; i++)
				{
					newCenterValue[i] = sum[i] / pointsInCluster;
				}
				if (Distance(newCenterValue, center.Value) > 0.00001)
				{
					center.Value = newCenterValue;
					centersChanged = true;
				}
				else
				{
					if(IsDebugLog())
					{
						Debug.Log("K-means interations: " + iteration);
					}
				}
			}
			
			// Reassign points to closest center
			foreach (ClusterPoint point in inputPoints)
			{
				double dist = double.MaxValue;
				foreach (ClusterCenter center in centers)
				{
					double distanceFromCenter = Distance(point.Value, center.Value);
					if (distanceFromCenter < dist)
					{
						point.AssignedCluster = center.ClusterId;
						dist = distanceFromCenter;
					}
				}
			}
		}
		
		// Get the width for each cluster center
		foreach (ClusterCenter center in centers)
		{
			double maxDist = 0;
			foreach (ClusterPoint point in inputPoints)
			{
				if (point.AssignedCluster == center.ClusterId)
				{
					double dist = Distance(point.Value, center.Value);
					if (dist > maxDist)
					{
						maxDist = dist;
					}
				}
			}
			if (Math.Abs(maxDist) < 0.00001)
			{
				center.Width = numClusters;
			}
			else
			{
				center.Width = 1 / maxDist;
			}
			
		}
		
		// Copy the cluster center values to the result array.
		foreach (ClusterCenter center in centers)
		{
			results[center.ClusterId] = new double[center.Value.Length + 1];
			//results[center.ClusterID] = center.Value;
			for (int i = 0; i < center.Value.Length; i++)
			{
				results[center.ClusterId][i] = center.Value[i];
			}
			results[center.ClusterId][center.Value.Length] = center.Width;
		}
		
		return results;
	}
	
	/// <summary>
	/// Get the distance from two point.
	/// Works in any number of dimensions.
	/// </summary>
	/// <param name="input"></param>
	/// <param name="center"></param>
	/// <returns></returns>
	private static double Distance(double[] input, double[] center)
	{
		double squaredSum = 0;
		for (int i = 0; i < input.Length; i++)
		{
			double difference = input[i] - center[i];
			squaredSum += difference * difference;
		}
		return Math.Sqrt(squaredSum);
	}

	/// <summary>
	/// Used for KMeans
	/// </summary>
	private class ClusterPoint
	{
     	public readonly double[] Value;
     	public int AssignedCluster;
     
     	public ClusterPoint(double[] inputValue, int cluster)
     	{
     		Value = inputValue;
     		AssignedCluster = cluster;
     	}
    }

	/// <summary>
	/// Used for Kmeans
	/// </summary>
	private class ClusterCenter
	{
     	public readonly int ClusterId;
     	public double[] Value;
		public double Width;
     
     	public ClusterCenter(int id, double[] centerValue)
     	{
     		ClusterId = id;
     		Value = centerValue;
     	}
	}
	
	// ReSharper disable once InconsistentNaming
	/// <summary>
	/// The Radial Basis Function Node
	/// </summary>
	public class RBFNode
	{
		public double[] Centers;
		public double[] ToOutputWeights;
		public double FunctionOut;
		public double Width;

		// ReSharper disable once InconsistentNaming
		public RBFNode(int numInputs, int numRBF, int numOutputs)
		{
			Centers = new double[numInputs];
			ToOutputWeights = new double[numOutputs];
			for (int i = 0; i < ToOutputWeights.Length; i++)
			{
				ToOutputWeights[i] = Random.Range(-0.1f, 0.1f);
			}
			//width = 2 / (numRBF * 1.0);
			Width = numRBF;
		}

		/// <summary>
		/// The output of the function.
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public double RawFunctionOut(double[] input)
		{
			double r = Distance(input, Centers);
			double result = Math.Exp (-1 * Width * Math.Pow (r, 2.0));
			FunctionOut = result;
			return result;
		}
		
		/// <summary>
		/// Output of the function with corresponding weight.
		/// </summary>
		/// <param name="outputIndex"></param>
		/// <param name="input"></param>
		/// <returns></returns>
		public double WeightedFunctionOut(int outputIndex, double[] input)
		{
			double result = ToOutputWeights[outputIndex] * RawFunctionOut(input);
			return result;
		}
	}


	/// <summary>
	/// Debug logging information
	/// </summary>
	public void SaveAllNetworkSettings()
	{
		double[][] networkSettings = new double[9][];
		/*
		0 = number of inputs
		1 = number of RBF nodes
		2 = number of outputs
		3 = all the center values
		4 = all the width values
		5 = all the weight values
		6 = all the bias values
		7 = normalization ranges
		*/

		networkSettings[0] = new[] {Convert.ToDouble(numInputs)};
		networkSettings[1] = new[] {Convert.ToDouble(numRadialBasisFunctions)};
		networkSettings[2] = new[] {Convert.ToDouble(numOutputs)};
		networkSettings[3] = new double[numInputs * numRadialBasisFunctions];
		networkSettings[4] = new double[numRadialBasisFunctions];
		networkSettings[5] = new double[numRadialBasisFunctions * numOutputs];
		networkSettings[6] = new double[numOutputs];

		int networkCenterIndex = 0;
		int networkWidthIndex = 0;
		int networkWeightIndex = 0;
		foreach (RBFNode node in rbfNodes)
		{
			foreach (double d in node.Centers)
			{
				networkSettings[3][networkCenterIndex] = d;
				networkCenterIndex++;
			}

			networkSettings[4][networkWidthIndex] = node.Width;
			networkWidthIndex++;

			foreach (double weight in node.ToOutputWeights)
			{
				networkSettings[5][networkWeightIndex] = weight;
				networkWeightIndex++;
			}
		}

		for (int i = 0; i < numOutputs; i++)
		{
			networkSettings[6][i] = outputBiases[i];
		}

		// Add the ranges to the settings array
		string[][] rangeStrings = FileIo.LoadData("lowhighranges.txt");
		double[][] ranges = Normalization.MakeDoubleMatrixFromStringMatrix(rangeStrings);
		networkSettings[7] = ranges[0];
		networkSettings[8] = ranges[1];

		string[][] stringNetworkValues = Normalization.MakeStringMatrixFromDoubleMatrix(networkSettings);

		
		FileIo.SaveData(stringNetworkValues, "network_settings.txt");
	}

    /// <summary>
    /// Loads the network settings.
    /// </summary>
    /// <param name="networkSettings">The network settings.</param>
    public void LoadNetworkSettings(double[][] networkSettings)
	{
		// Set the dimension of the network.
		numInputs = (int) networkSettings[0][0];
		numRadialBasisFunctions = (int) networkSettings[1][0];
		numOutputs = (int) networkSettings[2][0];
		
		CreateNetworkArrays();
		
	}
}


public class GaussianFunction
{
	//Only One Dimension value between 0 and 1
	/// <summary>
	/// Gaussian Function.
	/// </summary>
	/// <param name="center"></param>
	/// <param name="position"></param>
	/// <returns></returns>
	public static double Gaussian(float center, float position)
	{
		float r = Math.Abs (center - position);
		return Math.Exp (-Math.Pow (r, 2.0f));
	}
}