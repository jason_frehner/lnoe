﻿using UnityEngine;

public class rbfTest : MonoBehaviour
{
	private int numInputs = 4;
	private int numRBFs = 10;
	private int numOutputs = 3;
	
	private readonly double[][] allDataIris = new double[150][];
	private readonly double[][] normalizedIrisdata = new double[150][];
	private double[][] trainData;
	private double[][] testData;

	public RadialBasisFunctionNetwork rbfn;
	public RadialBasisFunctionNetwork rbfnTwo;

	private void Start()
	{
		#region iris traing data

		allDataIris[0] = new[] { 5.1, 3.5, 1.4, 0.2, 0, 0, 1};
		allDataIris[1] = new[] { 4.9, 3.0, 1.4, 0.2, 0, 0, 1 }; 
		allDataIris[2] = new[] { 4.7, 3.2, 1.3, 0.2, 0, 0, 1 };
		allDataIris[3] = new[] { 4.6, 3.1, 1.5, 0.2, 0, 0, 1 }; 
		allDataIris[4] = new[] { 5.0, 3.6, 1.4, 0.2, 0, 0, 1 }; 
		allDataIris[5] = new[] { 5.4, 3.9, 1.7, 0.4, 0, 0, 1 }; 
		allDataIris[6] = new[] { 4.6, 3.4, 1.4, 0.3, 0, 0, 1 }; 
		allDataIris[7] = new[] { 5.0, 3.4, 1.5, 0.2, 0, 0, 1 }; 
		allDataIris[8] = new[] { 4.4, 2.9, 1.4, 0.2, 0, 0, 1 }; 
		allDataIris[9] = new[] { 4.9, 3.1, 1.5, 0.1, 0, 0, 1 };
		allDataIris[10] = new[] { 5.4, 3.7, 1.5, 0.2, 0, 0, 1 }; 
		allDataIris[11] = new[] { 4.8, 3.4, 1.6, 0.2, 0, 0, 1 }; 
		allDataIris[12] = new[] { 4.8, 3.0, 1.4, 0.1, 0, 0, 1 }; 
		allDataIris[13] = new[] { 4.3, 3.0, 1.1, 0.1, 0, 0, 1 }; 
		allDataIris[14] = new[] { 5.8, 4.0, 1.2, 0.2, 0, 0, 1 }; 
		allDataIris[15] = new[] { 5.7, 4.4, 1.5, 0.4, 0, 0, 1 }; 
		allDataIris[16] = new[] { 5.4, 3.9, 1.3, 0.4, 0, 0, 1 }; 
		allDataIris[17] = new[] { 5.1, 3.5, 1.4, 0.3, 0, 0, 1 }; 
		allDataIris[18] = new[] { 5.7, 3.8, 1.7, 0.3, 0, 0, 1 }; 
		allDataIris[19] = new[] { 5.1, 3.8, 1.5, 0.3, 0, 0, 1 };
		allDataIris[20] = new[] { 5.4, 3.4, 1.7, 0.2, 0, 0, 1 }; 
		allDataIris[21] = new[] { 5.1, 3.7, 1.5, 0.4, 0, 0, 1 }; 
		allDataIris[22] = new[] { 4.6, 3.6, 1.0, 0.2, 0, 0, 1 }; 
		allDataIris[23] = new[] { 5.1, 3.3, 1.7, 0.5, 0, 0, 1 }; 
		allDataIris[24] = new[] { 4.8, 3.4, 1.9, 0.2, 0, 0, 1 }; 
		allDataIris[25] = new[] { 5.0, 3.0, 1.6, 0.2, 0, 0, 1 }; 
		allDataIris[26] = new[] { 5.0, 3.4, 1.6, 0.4, 0, 0, 1 }; 
		allDataIris[27] = new[] { 5.2, 3.5, 1.5, 0.2, 0, 0, 1 }; 
		allDataIris[28] = new[] { 5.2, 3.4, 1.4, 0.2, 0, 0, 1 }; 
		allDataIris[29] = new[] { 4.7, 3.2, 1.6, 0.2, 0, 0, 1 };
		allDataIris[30] = new[] { 4.8, 3.1, 1.6, 0.2, 0, 0, 1 }; 
		allDataIris[31] = new[] { 5.4, 3.4, 1.5, 0.4, 0, 0, 1 }; 
		allDataIris[32] = new[] { 5.2, 4.1, 1.5, 0.1, 0, 0, 1 }; 
		allDataIris[33] = new[] { 5.5, 4.2, 1.4, 0.2, 0, 0, 1 }; 
		allDataIris[34] = new[] { 4.9, 3.1, 1.5, 0.1, 0, 0, 1 }; 
		allDataIris[35] = new[] { 5.0, 3.2, 1.2, 0.2, 0, 0, 1 }; 
		allDataIris[36] = new[] { 5.5, 3.5, 1.3, 0.2, 0, 0, 1 }; 
		allDataIris[37] = new[] { 4.9, 3.1, 1.5, 0.1, 0, 0, 1 }; 
		allDataIris[38] = new[] { 4.4, 3.0, 1.3, 0.2, 0, 0, 1 }; 
		allDataIris[39] = new[] { 5.1, 3.4, 1.5, 0.2, 0, 0, 1 };
		allDataIris[40] = new[] { 5.0, 3.5, 1.3, 0.3, 0, 0, 1 }; 
		allDataIris[41] = new[] { 4.5, 2.3, 1.3, 0.3, 0, 0, 1 }; 
		allDataIris[42] = new[] { 4.4, 3.2, 1.3, 0.2, 0, 0, 1 }; 
		allDataIris[43] = new[] { 5.0, 3.5, 1.6, 0.6, 0, 0, 1 }; 
		allDataIris[44] = new[] { 5.1, 3.8, 1.9, 0.4, 0, 0, 1 }; 
		allDataIris[45] = new[] { 4.8, 3.0, 1.4, 0.3, 0, 0, 1 }; 
		allDataIris[46] = new[] { 5.1, 3.8, 1.6, 0.2, 0, 0, 1 }; 
		allDataIris[47] = new[] { 4.6, 3.2, 1.4, 0.2, 0, 0, 1 }; 
		allDataIris[48] = new[] { 5.3, 3.7, 1.5, 0.2, 0, 0, 1 }; 
		allDataIris[49] = new[] { 5.0, 3.3, 1.4, 0.2, 0, 0, 1 };
		allDataIris[50] = new[] { 7.0, 3.2, 4.7, 1.4, 0, 1, 0 }; 
		allDataIris[51] = new[] { 6.4, 3.2, 4.5, 1.5, 0, 1, 0 }; 
		allDataIris[52] = new[] { 6.9, 3.1, 4.9, 1.5, 0, 1, 0 }; 
		allDataIris[53] = new[] { 5.5, 2.3, 4.0, 1.3, 0, 1, 0 }; 
		allDataIris[54] = new[] { 6.5, 2.8, 4.6, 1.5, 0, 1, 0 }; 
		allDataIris[55] = new[] { 5.7, 2.8, 4.5, 1.3, 0, 1, 0 }; 
		allDataIris[56] = new[] { 6.3, 3.3, 4.7, 1.6, 0, 1, 0 }; 
		allDataIris[57] = new[] { 4.9, 2.4, 3.3, 1.0, 0, 1, 0 }; 
		allDataIris[58] = new[] { 6.6, 2.9, 4.6, 1.3, 0, 1, 0 };
		allDataIris[59] = new[] { 5.2, 2.7, 3.9, 1.4, 0, 1, 0 };
		allDataIris[60] = new[] { 5.0, 2.0, 3.5, 1.0, 0, 1, 0 }; 
		allDataIris[61] = new[] { 5.9, 3.0, 4.2, 1.5, 0, 1, 0 }; 
		allDataIris[62] = new[] { 6.0, 2.2, 4.0, 1.0, 0, 1, 0 }; 
		allDataIris[63] = new[] { 6.1, 2.9, 4.7, 1.4, 0, 1, 0 }; 
		allDataIris[64] = new[] { 5.6, 2.9, 3.6, 1.3, 0, 1, 0 }; 
		allDataIris[65] = new[] { 6.7, 3.1, 4.4, 1.4, 0, 1, 0 }; 
		allDataIris[66] = new[] { 5.6, 3.0, 4.5, 1.5, 0, 1, 0 }; 
		allDataIris[67] = new[] { 5.8, 2.7, 4.1, 1.0, 0, 1, 0 }; 
		allDataIris[68] = new[] { 6.2, 2.2, 4.5, 1.5, 0, 1, 0 }; 
		allDataIris[69] = new[] { 5.6, 2.5, 3.9, 1.1, 0, 1, 0 };
		allDataIris[70] = new[] { 5.9, 3.2, 4.8, 1.8, 0, 1, 0 }; 
		allDataIris[71] = new[] { 6.1, 2.8, 4.0, 1.3, 0, 1, 0 }; 
		allDataIris[72] = new[] { 6.3, 2.5, 4.9, 1.5, 0, 1, 0 }; 
		allDataIris[73] = new[] { 6.1, 2.8, 4.7, 1.2, 0, 1, 0 }; 
		allDataIris[74] = new[] { 6.4, 2.9, 4.3, 1.3, 0, 1, 0 }; 
		allDataIris[75] = new[] { 6.6, 3.0, 4.4, 1.4, 0, 1, 0 }; 
		allDataIris[76] = new[] { 6.8, 2.8, 4.8, 1.4, 0, 1, 0 }; 
		allDataIris[77] = new[] { 6.7, 3.0, 5.0, 1.7, 0, 1, 0 }; 
		allDataIris[78] = new[] { 6.0, 2.9, 4.5, 1.5, 0, 1, 0 }; 
		allDataIris[79] = new[] { 5.7, 2.6, 3.5, 1.0, 0, 1, 0 };
		allDataIris[80] = new[] { 5.5, 2.4, 3.8, 1.1, 0, 1, 0 }; 
		allDataIris[81] = new[] { 5.5, 2.4, 3.7, 1.0, 0, 1, 0 }; 
		allDataIris[82] = new[] { 5.8, 2.7, 3.9, 1.2, 0, 1, 0 }; 
		allDataIris[83] = new[] { 6.0, 2.7, 5.1, 1.6, 0, 1, 0 }; 
		allDataIris[84] = new[] { 5.4, 3.0, 4.5, 1.5, 0, 1, 0 }; 
		allDataIris[85] = new[] { 6.0, 3.4, 4.5, 1.6, 0, 1, 0 }; 
		allDataIris[86] = new[] { 6.7, 3.1, 4.7, 1.5, 0, 1, 0 }; 
		allDataIris[87] = new[] { 6.3, 2.3, 4.4, 1.3, 0, 1, 0 }; 
		allDataIris[88] = new[] { 5.6, 3.0, 4.1, 1.3, 0, 1, 0 }; 
		allDataIris[89] = new[] { 5.5, 2.5, 4.0, 1.3, 0, 1, 0 };
		allDataIris[90] = new[] { 5.5, 2.6, 4.4, 1.2, 0, 1, 0 }; 
		allDataIris[91] = new[] { 6.1, 3.0, 4.6, 1.4, 0, 1, 0 }; 
		allDataIris[92] = new[] { 5.8, 2.6, 4.0, 1.2, 0, 1, 0 }; 
		allDataIris[93] = new[] { 5.0, 2.3, 3.3, 1.0, 0, 1, 0 }; 
		allDataIris[94] = new[] { 5.6, 2.7, 4.2, 1.3, 0, 1, 0 }; 
		allDataIris[95] = new[] { 5.7, 3.0, 4.2, 1.2, 0, 1, 0 }; 
		allDataIris[96] = new[] { 5.7, 2.9, 4.2, 1.3, 0, 1, 0 }; 
		allDataIris[97] = new[] { 6.2, 2.9, 4.3, 1.3, 0, 1, 0 }; 
		allDataIris[98] = new[] { 5.1, 2.5, 3.0, 1.1, 0, 1, 0 }; 
		allDataIris[99] = new[] { 5.7, 2.8, 4.1, 1.3, 0, 1, 0 };
		allDataIris[100] = new[] { 6.3, 3.3, 6.0, 2.5, 1, 0, 0 }; 
		allDataIris[101] = new[] { 5.8, 2.7, 5.1, 1.9, 1, 0, 0 }; 
		allDataIris[102] = new[] { 7.1, 3.0, 5.9, 2.1, 1, 0, 0 }; 
		allDataIris[103] = new[] { 6.3, 2.9, 5.6, 1.8, 1, 0, 0 }; 
		allDataIris[104] = new[] { 6.5, 3.0, 5.8, 2.2, 1, 0, 0 }; 
		allDataIris[105] = new[] { 7.6, 3.0, 6.6, 2.1, 1, 0, 0 }; 
		allDataIris[106] = new[] { 4.9, 2.5, 4.5, 1.7, 1, 0, 0 }; 
		allDataIris[107] = new[] { 7.3, 2.9, 6.3, 1.8, 1, 0, 0 }; 
		allDataIris[108] = new[] { 6.7, 2.5, 5.8, 1.8, 1, 0, 0 }; 
		allDataIris[109] = new[] { 7.2, 3.6, 6.1, 2.5, 1, 0, 0 };
		allDataIris[110] = new[] { 6.5, 3.2, 5.1, 2.0, 1, 0, 0 }; 
		allDataIris[111] = new[] { 6.4, 2.7, 5.3, 1.9, 1, 0, 0 }; 
		allDataIris[112] = new[] { 6.8, 3.0, 5.5, 2.1, 1, 0, 0 }; 
		allDataIris[113] = new[] { 5.7, 2.5, 5.0, 2.0, 1, 0, 0 };
		allDataIris[114] = new[] { 5.8, 2.8, 5.1, 2.4, 1, 0, 0 }; 
		allDataIris[115] = new[] { 6.4, 3.2, 5.3, 2.3, 1, 0, 0 }; 
		allDataIris[116] = new[] { 6.5, 3.0, 5.5, 1.8, 1, 0, 0 }; 
		allDataIris[117] = new[] { 7.7, 3.8, 6.7, 2.2, 1, 0, 0 }; 
		allDataIris[118] = new[] { 7.7, 2.6, 6.9, 2.3, 1, 0, 0 }; 
		allDataIris[119] = new[] { 6.0, 2.2, 5.0, 1.5, 1, 0, 0 };
		allDataIris[120] = new[] { 6.9, 3.2, 5.7, 2.3, 1, 0, 0 }; 
		allDataIris[121] = new[] { 5.6, 2.8, 4.9, 2.0, 1, 0, 0 }; 
		allDataIris[122] = new[] { 7.7, 2.8, 6.7, 2.0, 1, 0, 0 }; 
		allDataIris[123] = new[] { 6.3, 2.7, 4.9, 1.8, 1, 0, 0 }; 
		allDataIris[124] = new[] { 6.7, 3.3, 5.7, 2.1, 1, 0, 0 }; 
		allDataIris[125] = new[] { 7.2, 3.2, 6.0, 1.8, 1, 0, 0 }; 
		allDataIris[126] = new[] { 6.2, 2.8, 4.8, 1.8, 1, 0, 0 }; 
		allDataIris[127] = new[] { 6.1, 3.0, 4.9, 1.8, 1, 0, 0 }; 
		allDataIris[128] = new[] { 6.4, 2.8, 5.6, 2.1, 1, 0, 0 }; 
		allDataIris[129] = new[] { 7.2, 3.0, 5.8, 1.6, 1, 0, 0 };
		allDataIris[130] = new[] { 7.4, 2.8, 6.1, 1.9, 1, 0, 0 }; 
		allDataIris[131] = new[] { 7.9, 3.8, 6.4, 2.0, 1, 0, 0 }; 
		allDataIris[132] = new[] { 6.4, 2.8, 5.6, 2.2, 1, 0, 0 }; 
		allDataIris[133] = new[] { 6.3, 2.8, 5.1, 1.5, 1, 0, 0 }; 
		allDataIris[134] = new[] { 6.1, 2.6, 5.6, 1.4, 1, 0, 0 }; 
		allDataIris[135] = new[] { 7.7, 3.0, 6.1, 2.3, 1, 0, 0 }; 
		allDataIris[136] = new[] { 6.3, 3.4, 5.6, 2.4, 1, 0, 0 }; 
		allDataIris[137] = new[] { 6.4, 3.1, 5.5, 1.8, 1, 0, 0 }; 
		allDataIris[138] = new[] { 6.0, 3.0, 4.8, 1.8, 1, 0, 0 }; 
		allDataIris[139] = new[] { 6.9, 3.1, 5.4, 2.1, 1, 0, 0 };
		allDataIris[140] = new[] { 6.7, 3.1, 5.6, 2.4, 1, 0, 0 }; 
		allDataIris[141] = new[] { 6.9, 3.1, 5.1, 2.3, 1, 0, 0 }; 
		allDataIris[142] = new[] { 5.8, 2.7, 5.1, 1.9, 1, 0, 0 }; 
		allDataIris[143] = new[] { 6.8, 3.2, 5.9, 2.3, 1, 0, 0 }; 
		allDataIris[144] = new[] { 6.7, 3.3, 5.7, 2.5, 1, 0, 0 }; 
		allDataIris[145] = new[] { 6.7, 3.0, 5.2, 2.3, 1, 0, 0 }; 
		allDataIris[146] = new[] { 6.3, 2.5, 5.0, 1.9, 1, 0, 0 }; 
		allDataIris[147] = new[] { 6.5, 3.0, 5.2, 2.0, 1, 0, 0 }; 
		allDataIris[148] = new[] { 6.2, 3.4, 5.4, 2.3, 1, 0, 0 }; 
		allDataIris[149] = new[] { 5.9, 3.0, 5.1, 1.8, 1, 0, 0 };

		#endregion

	
		// Create a normalized set of data
		// Get input low and highs
		double[] inputLows = new double[numInputs];
		for (int i = 0; i < inputLows.Length; i++)
		{
			inputLows[i] = double.MaxValue;
		}
		double[] inputHighs = new double[numInputs];
		for (int i = 0; i < inputHighs.Length; i++)
		{
			inputHighs[i] = double.MinValue;
		}
		for (int i = 0; i < allDataIris.Length; i++)
		{
			for (int j = 0; j < numInputs; j++)
			{
				if (allDataIris[i][j] < inputLows[j])
				{
					inputLows[j] = allDataIris[i][j];
				}
				if (allDataIris[i][j] >  inputHighs[j])
				{
					inputHighs[j] = allDataIris[i][j];
				}
			}
		}
		// Normalize
		for (int i = 0; i < allDataIris.Length; i++)
		{
			normalizedIrisdata[i] = new double[numInputs + numOutputs];
			for (int j = 0; j < numInputs; j++)
			{
				normalizedIrisdata[i][j] =
					Normalization.DoubleNormalization(allDataIris[i][j], inputLows[j], inputHighs[j], -1.0, 1.0);
			}
			for (int j = 0; j < numOutputs; j++)
			{
				normalizedIrisdata[i][j + numInputs] = allDataIris[i][j + numInputs];
			}
		}
		
		
		Debug.Log("Start RBF test.");
	
		rbfn = new RadialBasisFunctionNetwork(numInputs, numRBFs, numOutputs);
		Debug.Log("Networks Built.");

		MakeTrainTest(allDataIris, out trainData, out testData);
		rbfn.Train(trainData);
	
	
		double trainAccuracy = rbfn.Accuracy(trainData);
		double trainError = rbfn.MeanSquaredError(trainData);
		Debug.Log("Accuracy of training data: " + trainAccuracy.ToString("F" + 2).PadRight(5));
		Debug.Log("Mean Square error of training data: " + trainError.ToString("F" + 2).PadRight(5));

		double testAccuracy = rbfn.Accuracy(testData);
		double testError = rbfn.MeanSquaredError(testData);
		Debug.Log("Accuracy of test data: " + testAccuracy.ToString("F" + 2).PadRight(5));
		Debug.Log("Mean Square error of test data: " + testError.ToString("F" + 2).PadRight(5));
	
		Debug.Log("****** Normalized Data ********");
		
		rbfnTwo = new RadialBasisFunctionNetwork(numInputs, numRBFs, numOutputs);
		MakeTrainTest(normalizedIrisdata, out trainData, out testData);
		rbfnTwo.Train(trainData);
		trainAccuracy = rbfnTwo.Accuracy(trainData);
		trainError = rbfnTwo.MeanSquaredError(trainData);
		Debug.Log("Accuracy of training data: " + trainAccuracy.ToString("F" + 2).PadRight(5));
		Debug.Log("Mean Square error of training data: " + trainError.ToString("F" + 2).PadRight(5));

		testAccuracy = rbfnTwo.Accuracy(testData);
		testError = rbfnTwo.MeanSquaredError(testData);
		Debug.Log("Accuracy of test data: " + testAccuracy.ToString("F" + 2).PadRight(5));
		Debug.Log("Mean Square error of test data: " + testError.ToString("F" + 2).PadRight(5));
		
	
		Debug.Log("End test.");
	}

	/// <summary>
	/// Seperate the data into training and test.
	/// </summary>
	/// <param name="allData">All the data.</param>
	/// <param name="trainData"></param>
	/// <param name="testData"></param>
	private static void MakeTrainTest(double[][] allData, out double[][] trainData, out double[][] testData)
	{
		int totalRows = allData.Length;
		int numberOfColumns = allData[0].Length;
		int trainRows = (int)(totalRows * 0.8f);
		int testRows = totalRows - trainRows;
		trainData = new double[trainRows][];
		testData = new double[testRows][];
	
		// Copy allDataIris
		double[][] copyOfData = new double[allData.Length][];
		for (int i = 0; i < copyOfData.Length; i++)
		{
			copyOfData[i] = allData[i];
		}
	
		// Randomize copy
		for (int i = 0; i < copyOfData.Length; i++)
		{
			int r = Random.Range(i, copyOfData.Length);
			double[] temp = copyOfData[r];
			copyOfData[r] = copyOfData[i];
			copyOfData[i] = temp;
		}
	
		// Get the train data
		for (int i = 0; i < trainRows; i++)
		{
			trainData[i] = new double[numberOfColumns];
			for (int j = 0; j < numberOfColumns; j++)
			{
				trainData[i][j] = copyOfData[i][j];
			}
		}
	
		// Get the test data
		for (int i = 0; i < testRows; i++)
		{
			testData[i] = new double[numberOfColumns];
			for (int j = 0; j < numberOfColumns; j++)
			{
				testData[i][j] = copyOfData[i + trainRows][j]; // offest for training rows
			}
		}

		Debug.Log("Train / Test || " + trainData.Length + " / " + testData.Length);
	}
}
