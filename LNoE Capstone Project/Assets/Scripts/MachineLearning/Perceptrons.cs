﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Adapted from James McCaffrey
/// Neural Network Using C# Succinctly
/// </summary>
public class Perceptrons : MonoBehaviour {
	private readonly double[][] trainData = new double[8][];
	private const int NumInputs = 2;
	private const double Alpha = 0.001;
	private const int MaxEpochs = 100;

	private void Start () 
	{
		Debug.Log("Percetion Demo");
		
		trainData[0] = new[] {1.5, 2.0, -1};
		trainData[1] = new[] {2.0, 3.5, -1};
		trainData[2] = new[] {3.0, 5.0, -1};
		trainData[3] = new[] {3.5, 2.5, -1};
		trainData[4] = new[] {4.5, 5.0, 1};
		trainData[5] = new[] {5.0, 7.0, 1};
		trainData[6] = new[] {5.5, 8.0, 1};
		trainData[7] = new[] {6.0, 6.0, 1};
		

		Perceptron p = new Perceptron(NumInputs);
		IEnumerable<double> weights = p.Train(trainData, Alpha, MaxEpochs);
		
		Debug.Log("Best Weights:");
		foreach (double weight in weights)
		{
			Debug.Log(weight);
		}
		
		double[][] newData = new double[6][];
		newData[0] = new[] { 3.0, 4.0 }; // Should be -1. 
		newData[1] = new[] { 0.0, 1.0 }; // Should be -1. 
		newData[2] = new[] { 2.0, 5.0 }; // Should be -1. 
		newData[3] = new[] { 5.0, 6.0 }; // Should be 1. 
		newData[4] = new[] { 9.0, 9.0 }; // Should be 1. 
		newData[5] = new[] { 4.0, 6.0 }; // Should be 1.

		foreach (double[] d in newData)
		{
			Debug.Log("Predicion: " + p.ComputeOutput(d));
		}
		
		Debug.Log("End Demo");
	}
	
}