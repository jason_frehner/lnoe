﻿using System;
using System.Linq;
using UnityEngine;
using Random = System.Random;

// ReSharper disable once InconsistentNaming
public class xorTest : MonoBehaviour {

	private const int NumInput = 2;
	private const int NumHidden = 5;
	private const int NumOutput = 2;
	
	double[][] xorData = new double[4][];
	

	private const double LearnRate = 0.05;
	private const double Momentum = 0.01;
	private const int MaxEpochs = 100000;

	private double[][] trainData;
	private double[][] testData;
	
// Use this for initialization
	void Start () 
	{
		xorData[0] = new[] {-1.0, -1.0, 0.0, 1.0};
		xorData[1] = new[] {1.0, -1.0, 1.0, 0.0};
		xorData[2] = new[] {-1.0, 1.0, 1.0, 0.0};
		xorData[3] = new[] {1.0, 1.0, 0.0, 1.0};
		
		double[][] repeatedData = new double[80][];

		for (int i = 0; i < 80; i +=4)
		{
			Array.Copy(xorData, 0, repeatedData, i, 4);
		}

		foreach (double[] d in repeatedData)
		{
			d[0] = d[0] * UnityEngine.Random.value;
			d[1] = d[1] * UnityEngine.Random.value;
			ShowVector(d);
		}
		
		MakeTrainTest(repeatedData, 0, out trainData, out testData);
		
		/*
		double[][] trainOne = new double[2][];
		Array.Copy(xorData, trainOne, 2);
		
		double[][] trainTwo = new double[3][];
		Array.Copy(xorData, trainTwo, 3);
		*/
		NeuralNetwork nn = new NeuralNetwork(NumInput, NumHidden, NumOutput);
		/*
		Debug.Log("Train 1/2");
		StartCoroutine(nn.Train(trainOne, MaxEpochs, LearnRate, Momentum));
		
		Debug.Log("Train 3/4");
		StartCoroutine(nn.Train(trainTwo, MaxEpochs, LearnRate, Momentum));
		
		Debug.Log("Train all");
		*/
		nn.Train(trainData, MaxEpochs, LearnRate, Momentum);
		
		double trainAccuracy = nn.Accuracy(trainData);
		double trainError = nn.MeanSquaredError(trainData);
		Debug.Log("Accuracy of training data: " + trainAccuracy.ToString("F" + 2).PadRight(5));
		Debug.Log("Mean Square error of training data: " + trainError.ToString("F" + 2).PadRight(5));

		double testAccuracy = nn.Accuracy(testData);
		double testError = nn.MeanSquaredError(testData);
		Debug.Log("Accuracy of test data: " + testAccuracy.ToString("F" + 2).PadRight(5));
		Debug.Log("Mean Square error of test data: " + testError.ToString("F" + 2).PadRight(5));

		/*
		foreach (var data in xorData)
		{
			double[] i= new double[NumInput];
			Array.Copy(data, i, NumInput);
			double[] o = nn.ComputeOutputs(i);
			Debug.Log("Test data: ");
			ShowVector(data);
			Debug.Log(" - output: ");
			ShowVector(o);
		}
		*/
	}
	
	private static void MakeTrainTest(double[][] allData, int seed, out double[][] trainData, out double[][] testData)
	{
		Random rnd = new Random(seed);

		int totalRows = allData.Length;
		int numberOfColumns = allData[0].Length;
		int trainRows = (int)(totalRows * 0.8f);
		int testRows = totalRows - trainRows;
		trainData = new double[trainRows][];
		testData = new double[testRows][];
		
		// Copy allDataIris
		double[][] copyOfData = new double[allData.Length][];
		for (int i = 0; i < copyOfData.Length; i++)
		{
			copyOfData[i] = allData[i];
		}
		
		// Randomize copy
		for (int i = 0; i < copyOfData.Length; i++)
		{
			int r = rnd.Next(i, copyOfData.Length);
			double[] temp = copyOfData[r];
			copyOfData[r] = copyOfData[i];
			copyOfData[i] = temp;
		}
		
		// Get the train data
		for (int i = 0; i < trainRows; i++)
		{
			trainData[i] = new double[numberOfColumns];
			for (int j = 0; j < numberOfColumns; j++)
			{
				trainData[i][j] = copyOfData[i][j];
			}
		}
		
		// Get the test data
		for (int i = 0; i < testRows; i++)
		{
			testData[i] = new double[numberOfColumns];
			for (int j = 0; j < numberOfColumns; j++)
			{
				testData[i][j] = copyOfData[i + trainRows][j]; // offest for training rows
			}
		}

		Debug.Log("Train / Test || " + trainData.Length + " / " + testData.Length);
	}
	
	public static void ShowVector(double[] vector)
	{
		string s = vector.Aggregate("", (current, d) => current + (d + " "));

		Debug.Log(s);
	}
	
}
