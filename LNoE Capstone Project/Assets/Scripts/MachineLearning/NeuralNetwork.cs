using System;
using System.Linq;
using UnityEngine;
using Random = System.Random;

/// <summary>
/// Class for Neural Network
/// </summary>
public class NeuralNetwork
{
    private static Random rnd;
	
    // Inputs
    private readonly int numInput;
    private readonly double[] inputs;
    private readonly double[][] inputToHiddenWeights;
    private readonly double[][] inputToHiddenPrevWeightsDelta;
	
    // Hidden Layer
    private readonly int numHidden;
    private readonly double[] hiddenBiases;
    private readonly double[][] hiddenToOutputWeights;
    private readonly double[] hiddenOutputs;
    private readonly double[] hiddenGradients;
    private readonly double[] hiddenPrevBiasesDelta;
    private readonly double[][] hiddenToOutputPrevWeightDelta;
	
    // Outputs
    private readonly int numOutput;
    private readonly double[] outputs;
    private readonly double[] outputBiases;
    private readonly double[] outputGradients;
    private readonly double[] outputPrevBiasesDelta;
	
	

    /// <summary>
    /// Constructor for Neural Network
    /// </summary>
    /// <param name="numInput"></param>
    /// <param name="numHidden"></param>
    /// <param name="numOutput"></param>
    public NeuralNetwork(int numInput, int numHidden, int numOutput)
    {	
        rnd = new Random(0);
		
        this.numInput = numInput;
        this.numHidden = numHidden;
        this.numOutput = numOutput;
			
        inputs = new double[numInput];
        inputToHiddenWeights = MakeMatrix(numInput, numHidden);
        hiddenBiases = new double[numHidden];
        hiddenOutputs = new double[numHidden];
        hiddenToOutputWeights = MakeMatrix(numHidden, numOutput);
        outputBiases = new double[numOutput];
        outputs = new double[numOutput];
		
        outputGradients = new double[numOutput];
        hiddenGradients = new double[numHidden];

        inputToHiddenPrevWeightsDelta = MakeMatrix(numInput, numHidden);
        hiddenPrevBiasesDelta = new double[numHidden];
        hiddenToOutputPrevWeightDelta = MakeMatrix(numHidden, numOutput);
        outputPrevBiasesDelta = new double[numOutput];
		
        InitializeWeights();

        /*
		InitMatrix(inputToHiddenPrevWeightsDelta, 0.011);
		InitVector(hiddenPrevBiasesDelta, 0.011);
		InitMatrix(hiddenToOutputPrevWeightDelta, 0.011);
		InitVector(outputPrevBiasesDelta, 0.011);
		*/
    }

    /// <summary>
    /// Create a matrix of doubles.
    /// </summary>
    /// <param name="rows">How many rows.</param>
    /// <param name="columns">How many columns.</param>
    /// <returns></returns>
    private static double[][] MakeMatrix(int rows, int columns)
    {
        double[][] result = new double[rows][];
        for (int i = 0; i < rows; i++)
        {
            result[i] = new double[columns];
        }
        return result;
    }

    /// <summary>
    /// Set all the network's weights.
    /// </summary>
    /// <param name="weights">Weights to use.</param>
    /// <exception cref="Exception">Not the correct size array.</exception>
    public void SetWeights(double[] weights)
    {
        // Check if weights parameter is the right size.
        int numWeights = (numInput * numHidden) + numHidden + (numHidden * numOutput) + numOutput;
        if (weights.Length != numWeights)
        {
            throw new Exception("Bad weights array");
        }

        // index for moving through the weights array.
        int index = 0;

        // Set the input to hidden weights.
        for (int i = 0; i < numInput; i++)
        {
            for (int j = 0; j < numHidden; j++)
            {
                inputToHiddenWeights[i][j] = weights[index++];
            }
        }

        // Set the hidden biases.
        for (int i = 0; i < numHidden; i++)
        {
            hiddenBiases[i] = weights[index++];
        }

        // Set the hidden to output weights.
        for (int i = 0; i < numHidden; i++)
        {
            for (int j = 0; j < numOutput; j++)
            {
                hiddenToOutputWeights[i][j] = weights[index++];
            }
        }

        // Set output biases.
        for (int i = 0; i < numOutput; i++)
        {
            outputBiases[i] = weights[index++];
        }
    }

    private void InitializeWeights()
    {
        int numWeights = (numInput * numHidden) + numHidden + (numHidden * numOutput) + numOutput;
        double[] initialWeights = new double[numWeights];
        double low = -0.01;
        double high = 0.01;
        for (int i = 0; i < initialWeights.Length; i++)
        {
            initialWeights[i] = (high - low) * rnd.NextDouble() + low;
        }
        SetWeights(initialWeights);
    }

    /// <summary>
    /// Get all the weights in the network.
    /// </summary>
    /// <returns>Array of the weignts.</returns>
    public double[] GetWeights()
    {
        int numWeights = (numInput * numHidden) + numHidden + (numHidden * numOutput) + numOutput;
        double[] result = new double[numWeights];
        int index = 0;

        // Get the input to hidden weights.
        for (int i = 0; i < numInput; i++)
        {
            for (int j = 0; j < numHidden; j++)
            {
                result[index] = inputToHiddenWeights[i][j];
                index++;
            }
        }

        // Get the hidden biases.
        for (int i = 0; i < numHidden; i++)
        {
            result[index] = hiddenBiases[i];
            index++;
        }

        // Get the hidden to output weights.
        for (int i = 0; i < numHidden; i++)
        {
            for (int j = 0; j < numOutput; j++)
            {
                result[index] = hiddenToOutputWeights[i][j];
                index++;
            }
        }

        // Get the output biases.
        for (int i = 0; i < numOutput; i++)
        {
            result[index] = outputBiases[i];
        }

        return result;
    }

    /// <summary>
    /// Run the network with the current weights and given inputs.
    /// </summary>
    /// <param name="inputValues">Array of input values.</param>
    /// <returns>Array out output values</returns>
    /// <exception cref="Exception">Input lenth is not correct.</exception>
    public double[] ComputeOutputs(double[] inputValues)
    {
        if (inputValues.Length != numInput)
            throw new Exception("Bad inputs array");
			
        double[] hiddenSums = new double[numHidden];
        double[] outputSums = new double[numOutput];

        // Set the input values in the network.
        for (int i = 0; i < inputValues.Length; i++)
        {
            inputs[i] = inputValues[i];
        }

        // For each hidden node the input * weights.
        for (int j = 0; j < numHidden; j++)
        {
            for (int i = 0; i < numInput; i++)
            {
                hiddenSums[j] += inputs[i] * inputToHiddenWeights[i][j];
            }
        }

        // Add the biases to each hidden node.
        for (int i = 0; i < numHidden; i++)
        {
            hiddenSums[i] += hiddenBiases[i];
        }

        // Activate each hidden node with a Hyper Tan function.
        for (int i = 0; i < numHidden; i++)
        {
            hiddenOutputs[i] = HyperTan(hiddenSums[i]);
        }

        // For each output node hidden value * weights.
        for (int j = 0; j < numOutput; j++)
        {
            for (int i = 0; i < numHidden; i++)
            {
                outputSums[j] += hiddenSums[i] * hiddenToOutputWeights[i][j];
            }
        }

        // Add the biases to each output node.
        for (int i = 0; i < numOutput; i++)
        {
            outputSums[i] += outputBiases[i];
        }

        // Activate each ouput node with a softmax funcion.
        double[] softOut = Softmax(outputSums);//
        for (int i = 0; i < outputs.Length; i++)
        {
            outputs[i] = softOut[i];
        }
		
        // Return the output values.
        double[] result = new double[numOutput];
        for (int i = 0; i < outputs.Length; i++)
        {
            result[i] = outputs[i];
        }
        return result;
    }

    /// <summary>
    /// Hyper Tan function.
    /// </summary>
    /// <param name="v">Input value</param>
    /// <returns>A value between -1 and 1.</returns>
    private static double HyperTan(double v)
    {
        if (v < -20.0)
            return -1.0;
        else if (v > 20.0)
            return 1.0;
        else
            return Math.Tanh(v);
    }

    /// <summary>
    /// Makes all outputs sum to 1.
    /// </summary>
    /// <param name="oSums">Array of outputs</param>
    /// <returns>The converted array.</returns>
    private static double[] Softmax(double[] oSums)
    {
        // Get the max value from the array.
        double max = oSums[0];
        max = oSums.Concat(new[] {max}).Max();

        // Get scaling factor - sum of exp(each val - max).
        double scale = oSums.Sum(value => Math.Exp(value - max));

        // Convert each result.
        double[] result = new double[oSums.Length];
        for (int i = 0; i < oSums.Length; i++)
        {
            result[i] = Math.Exp(oSums[i] - max) / scale;
        }

        return result;
    }

    /// <summary>
    /// Basic back propagation for a single train value.
    /// 
    /// loop until some exit condition is met
    /// 	FeedForward
    /// 	BackPropagation
    /// end loop
    /// 
    /// </summary>
    /// <param name="tValues">Target Values</param>
    /// <param name="xValues">Input Values</param>
    /// <param name="learnRate">Learn Rate</param>
    /// <param name="momentum">Momentum</param>
    /// <param name="maxEpochs">Max Epochs</param>
    public void FindWeights(double[] tValues, double[] xValues, double learnRate, double momentum, int maxEpochs)
    {
        int epoch = 0;
        while (epoch <= maxEpochs)
        {
            // Feed Forward
            double[] yValues = ComputeOutputs(xValues);
			
            // Back Propation
            UpdateWeights(tValues, learnRate, momentum);

            // Output to log
            if (epoch % 100 == 0)
            {
                Debug.Log("epoch = " + epoch + "    curr outputs = ");
                BackProp.ShowVector(yValues);
            }

            epoch++;
        }
    }

    /// <summary>
    /// Back Propagation
    /// </summary>
    /// <param name="tVales">Taget Values</param>
    /// <param name="learnRate">Learn Rate</param>
    /// <param name="momentum">Momentum</param>
    private void UpdateWeights(double[] tVales, double learnRate, double momentum)
    {
        // Get the output gradients
        for (int i = 0; i < outputGradients.Length; i++)
        {
            double derivative = (1 - outputs[i]) * outputs[i];
            outputGradients[i] = derivative * (tVales[i] - outputs[i]);
        }

        // Get the hidden gradients
        for (int i = 0; i < hiddenGradients.Length; i++)
        {
            double derivative = (1 - hiddenOutputs[i]) * (1 + hiddenOutputs[i]);
            double sum = 0.0;

            for (int j = 0; j < numOutput; j++)
            {
                sum += outputGradients[j] * hiddenToOutputWeights[i][j];
            }
            hiddenGradients[i] = derivative * sum;
        }

        // Adjust the input to hidden weights.
        for (int i = 0; i < inputToHiddenWeights.Length; i++)
        {
            for (int j = 0; j < inputToHiddenWeights[i].Length; j++)
            {
                double delta = learnRate * hiddenGradients[j] * inputs[i];
                inputToHiddenWeights[i][j] += delta;
                inputToHiddenWeights[i][j] += momentum * inputToHiddenPrevWeightsDelta[i][j];
                inputToHiddenPrevWeightsDelta[i][j] = delta;
            }
        }

        // Adjust the hidden biases.
        for (int i = 0; i < hiddenBiases.Length; i++)
        {
            double delta = learnRate * hiddenGradients[i];
            hiddenBiases[i] += delta;
            hiddenBiases[i] += momentum * hiddenPrevBiasesDelta[i];
            hiddenPrevBiasesDelta[i] = delta;
        }

        // Adjust the hidden to output weights.
        for (int i = 0; i < hiddenToOutputWeights.Length; i++)
        {
            for (int j = 0; j < hiddenToOutputWeights[i].Length; j++)
            {
                double delta = learnRate * outputGradients[j] * hiddenOutputs[i];
                hiddenToOutputWeights[i][j] += delta;
                hiddenToOutputWeights[i][j] += momentum * hiddenToOutputPrevWeightDelta[i][j];
                hiddenToOutputPrevWeightDelta[i][j] = delta;
            }
        }

        // Adjust the output biases
        for (int i = 0; i < outputBiases.Length; i++)
        {
            double delta = learnRate * outputGradients[i];
            outputBiases[i] += delta;
            outputBiases[i] += momentum * outputPrevBiasesDelta[i];
            outputPrevBiasesDelta[i] = delta;
        }
    }

    /// <summary>
    /// Main Back Propagation training method.
    /// </summary>
    /// <param name="trainData">Data to use for training.</param>
    /// <param name="maxEpochs">Max epochs to train.</param>
    /// <param name="learnRate">The learning rate.</param>
    /// <param name="momentum">The learning momentum.</param>
    public void Train(double[][] trainData, int maxEpochs, double learnRate, double momentum)
    {
        int epoch = 0;
        double[] inputValues = new double[numInput];
        double[] targetValues = new double[numOutput];

        // Initialize a sequence
        int[] sequence = new int[trainData.Length];
        for (int i = 0; i < sequence.Length; i++)
        {
            sequence[i] = i;
        }

        // Main training loop
        while (epoch < maxEpochs)
        {
            // how offent to check the mean square error
            if (epoch % 100 == 0)
            {
                double meanSquareError = MeanSquaredError(trainData);
                if (meanSquareError < 0.040) break; //Error threashold
            }

            // Randomize the sequence
            Shuffle(sequence);
			
            // For each training data test and update
            for (int i = 0; i < trainData.Length; i++)
            {
                // Use the random sequence order rather than the order of the training data.
                int index = sequence[i];
                Array.Copy(trainData[index], inputValues, numInput); // Copy training datat input values.
                Array.Copy(trainData[index], numInput, targetValues, 0, numOutput); // Copy target values.
                ComputeOutputs(inputValues);
                UpdateWeights(targetValues, learnRate, momentum);
            }
			
            // Output to log
            if (epoch % 10000 == 0)
            {
                Debug.Log("epoch = " + epoch + 
                          "    current accuracy = " + Accuracy(trainData) + 
                          "    current error: " + MeanSquaredError(trainData));
            }

            epoch++;
        }
        Debug.Log("Final epoch: " + epoch);
    }

    /// <summary>
    /// Shuffle of a sequence
    /// </summary>
    /// <param name="sequence">The sequence</param>
    private static void Shuffle(int[] sequence)
    {
        for (int i = 0; i < sequence.Length; i++)
        {
            int r = rnd.Next(i, sequence.Length);
            int temp = sequence[r];
            sequence[r] = sequence[i];
            sequence[i] = temp;
        }
    }

    /// <summary>
    /// Get the error used to check against a stoping threashold.
    /// </summary>
    /// <param name="trainData">The data</param>
    /// <returns>The error</returns>
    public double MeanSquaredError(double[][] trainData)
    {
        double sumSquaredError = 0.0;
        double[] inputValues = new double[numInput];
        double[] targetValues = new double[numOutput];

        // For each data element
        foreach (double[] data in trainData)
        {
            Array.Copy(data, inputValues, numInput);
            Array.Copy(data, numInput, targetValues, 0, numOutput);
            double[] yValues = ComputeOutputs(inputValues);

            // Get the difference of the result to the target
            // Square it and add it to the sum
            for (int j = 0; j < numOutput; j++)
            {
                double error = targetValues[j] - yValues[j];
                sumSquaredError += error * error;
            }
        }
        return sumSquaredError / trainData.Length;
    }

    /// <summary>
    /// Get the accuracy of the data
    /// </summary>
    /// <param name="testData">The data</param>
    /// <returns>The accuracy</returns>
    public double Accuracy(double[][] testData)
    {
        int numCorrect = 0;
        int numWrong = 0;
        double[] inputValues = new double[numInput];
        double[] targetValues = new double[numOutput];

        // For each data element
        foreach (double[] data in testData)
        {
            Array.Copy(data, inputValues, numInput);
            Array.Copy(data, numInput, targetValues, 0, numOutput);
            double[] resultValues = ComputeOutputs(inputValues);
            int maxIndex = MaxIndex(resultValues);

            // Check if the result index is the same as the target
            if (Math.Abs(targetValues[maxIndex] - 1.0) < 0.0000000001)
            {
                numCorrect++;
            }
            else
            {
                numWrong++;
            }
        }
        return (numCorrect * 1.0) / (numCorrect + numWrong);
    }

    /// <summary>
    /// Get the index that has the largest value.
    /// </summary>
    /// <param name="vector">The vector to search</param>
    /// <returns>The index</returns>
    private static int MaxIndex(double[] vector)
    {
        int bigIndex = 0;
        double biggestValue = vector[0];
        for (int i = 0; i < vector.Length; i++)
        {
            // ReSharper disable once InvertIf
            if (vector[i] > biggestValue)
            {
                biggestValue = vector[i];
                bigIndex = i;
            }
        }
        return bigIndex;
    }
}