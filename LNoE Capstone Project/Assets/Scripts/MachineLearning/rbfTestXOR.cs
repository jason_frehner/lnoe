﻿using UnityEngine;

public class rbfTestXOR : MonoBehaviour
{

	private readonly double[][] xorData = new double[4][];
	private readonly double[][] xorDataTwo = new double[4][];

	public RadialBasisFunctionNetwork rbfn;
	public RadialBasisFunctionNetwork rbfnTwo;

	private void Start()
	{

		xorData[0] = new[] {0.0, 0.0, 0.0, 1.0};
		xorData[1] = new[] {1.0, 0.0, 1.0, 0.0};
		xorData[2] = new[] {0.0, 1.0, 1.0, 0.0};
		xorData[3] = new[] {1.0, 1.0, 0.0, 1.0};

		xorDataTwo[0] = new[] {-1.0, -1.0, 0.0, 1.0};
		xorDataTwo[1] = new[] {1.0, -1.0, 1.0, 0.0};
		xorDataTwo[2] = new[] {-1.0, 1.0, 1.0, 0.0};
		xorDataTwo[3] = new[] {1.0, 1.0, 0.0, 1.0};

		Debug.Log("Start RBF test.");

		rbfn = new RadialBasisFunctionNetwork(2, 4, 2);
		rbfnTwo = new RadialBasisFunctionNetwork(2, 4, 2);

		Debug.Log("Networks Built.");

		rbfn.Train(xorData);
		rbfnTwo.Train(xorDataTwo);

		Debug.Log("***** FIRST *****");
		double firstAccuracy = rbfn.Accuracy(xorData);
		double firstError = rbfn.MeanSquaredError(xorData);
		Debug.Log("Accuracy of training data: " + firstAccuracy.ToString("F" + 2).PadRight(5));
		Debug.Log("Mean Square error of training data: " + firstError.ToString("F" + 2).PadRight(5));

		Debug.Log("***** SECOND *****");
		double secondAccuracy = rbfnTwo.Accuracy(xorDataTwo);
		double secondError = rbfnTwo.MeanSquaredError(xorDataTwo);
		Debug.Log("Accuracy of test data: " + secondAccuracy.ToString("F" + 2).PadRight(5));
		Debug.Log("Mean Square error of test data: " + secondError.ToString("F" + 2).PadRight(5));

		Debug.Log("End test.");
	}
}
