﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

/// <summary>
/// Utility class for connecting with the Google Sheets for Unity plugin (GSFU).
/// Adapted from the demo script that came with the plugin.
/// </summary>
public class GsfuUtilsLnoe
{
	public static string[][] TrainData;

	private static bool IsDebugLog()
	{
		return PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
	}


	/// <summary>
	/// Game Log structure to be submitted to the Google spreadsheet.
	/// </summary>
	[Serializable]
	private struct GameLog
	{
		public string GameSessionName;
		public string Mission;
		public string Buildings;
		public string Heroes;
		public int RoundsLeft;
		public int ZombiesDead;
		public int ZombiesOnBoard;
		public int HeroesDead;
		public int BuildingsTakenOver;
		public int BuildingsLightsOut;
		public int SpawningPits;
		public string GameWon;
	}

	public static int NetworkSettingsLength = 7;
	[Serializable]
	private struct NetworkSettings
	{
		// These names must match what is in Google sheets
		public string NumInputs;
		public string NumRbfNodes;
		public string NumOutputs;
		public string Centers;
		public string Width;
		public string Weights;
		public string Bias;
		public string InputRangeLow;
		public string InputRangeHigh;
	}
	
	private static GameLog gameLog;
	private const string GameLogTableName = "Game Log";

	/// <summary>
	/// Prepare the game log for submission
	/// </summary>
	/// <param name="gd">The Game Data object.</param>
	/// <param name="roundNumber">Which round of data to use</param>
	public static void SetGameLog(GameData gd, int roundNumber)
	{
		gameLog = new GameLog
		{
			GameSessionName = gd.GetGameSessionName(),
			Mission = gd.GetMission(),
			Buildings = gd.GetBuildings(),
			Heroes = gd.GetRoundData(roundNumber).GetHeroes(),
			RoundsLeft = gd.GetRoundData(roundNumber).GetRoundsLeft(),
			ZombiesDead = gd.GetRoundData(roundNumber).GetZombiesDead(),
			ZombiesOnBoard = gd.GetRoundData(roundNumber).GetZombiesOnBoard(),
			HeroesDead = gd.GetRoundData(roundNumber).GetHeroesDead(),
			BuildingsTakenOver = gd.GetRoundData(roundNumber).GetBuildingsTakenOver(),
			BuildingsLightsOut = gd.GetRoundData(roundNumber).GetBuildingsLightsOut(),
			SpawningPits = gd.GetRoundData(roundNumber).GetSpawningPits(),
			GameWon = gd.GameWon
		};
	}

	/// <summary>
	/// Create the game log table.
	/// Only used from the editor menu.
	/// May never be needed again?
	/// </summary>
	/// <param name="runtime"></param>
	[UsedImplicitly]
	public static void CreateGameLogTable(bool runtime)
	{
		if(IsDebugLog())
		{
			Debug.Log("<color=yellow>Creating a table in the cloud for game log data.</color>");
		}
		
		// Creating an string array for field (table headers) names.
		string[] fieldNames = new string[9];
		fieldNames[0] = "GameSessionName";
		fieldNames[1] = "Mission";
		fieldNames[2] = "Buildings";
		fieldNames[3] = "RoundsLeft";
		fieldNames[4] = "ZombiesDead";
		fieldNames[5] = "ZombiesOnBoard";
		fieldNames[6] = "HeroesDead";
		fieldNames[7] = "BuildingsTakenOver";
		fieldNames[8] = "BuildingsLightsOut";
		fieldNames[9] = "SpawningPits";
		fieldNames[10] = "GameWon";
		// Request for the table to be created on the cloud.
		CloudConnectorCore.CreateTable(fieldNames, GameLogTableName, runtime);
	}
	
	/// <summary>
	/// Save the game log information to Google dive.
	/// </summary>
	/// <param name="runtime"></param>
	public static void SaveGameLog(bool runtime)
	{
		// Get the JSON string of the object.
		string gamelog = JsonUtility.ToJson(gameLog);

		if(IsDebugLog())
		{
			Debug.Log("<color=yellow>Sending following gameLog to the cloud: \n</color>" + gamelog);
		}
		
		// Save the object on the cloud, in a table called like the object type.
		CloudConnectorCore.CreateObject(gamelog, GameLogTableName, runtime);
	}

	/// <summary>
	/// Save the network settings.
	/// </summary>
	/// <param name="runtime"></param>
	public static void SaveNetworkSettings(bool runtime)
	{
		string[][] settings = FileIo.LoadData("network_settings.txt");
		NetworkSettings settingsStruct = new NetworkSettings
		{
			NumInputs = settings[0][0],
			NumRbfNodes = settings[1][0],
			NumOutputs = settings[2][0]
		};

		foreach (string s in settings[3])
		{
			settingsStruct.Centers = settingsStruct.Centers + s + ",";
		}
		settingsStruct.Centers = settingsStruct.Centers.Trim(',');
		
		foreach (string s in settings[4])
		{
			settingsStruct.Width = settingsStruct.Width + s + ",";
		}
		settingsStruct.Width = settingsStruct.Width.Trim(',');
		
		foreach (string s in settings[5])
		{
			settingsStruct.Weights = settingsStruct.Weights + s + ",";
		}
		settingsStruct.Weights = settingsStruct.Weights.Trim(',');
		
		foreach (string s in settings[6])
		{
			settingsStruct.Bias = settingsStruct.Bias + s + ",";
		}
		settingsStruct.Bias = settingsStruct.Bias.Trim(',');

		foreach (string s in settings[7])
		{
			settingsStruct.InputRangeLow = settingsStruct.InputRangeLow + s + ",";
		}
		settingsStruct.InputRangeLow = settingsStruct.InputRangeLow.Trim(',');

		foreach (string s in settings[8])
		{
			settingsStruct.InputRangeHigh = settingsStruct.InputRangeHigh + s + ",";
		}
		settingsStruct.InputRangeHigh = settingsStruct.InputRangeHigh.Trim(',');


		string settingsJson = JsonUtility.ToJson(settingsStruct);
		CloudConnectorCore.CreateObject(settingsJson, "network", runtime);

		if(IsDebugLog())
		{
			Debug.Log(settingsJson);
			Debug.Log("Network Settings Uploaded");
		}
	}

    /// <summary>
    /// Gets the network settings.
    /// </summary>
    /// <param name="runtime">if set to <c>true</c> [runtime].</param>
    public static void GetNetworkSettings(bool runtime)
	{
		CloudConnectorCore.GetTable("network", runtime);
	}
	
	/// <summary>
	/// Download all the saved game logs.
	/// </summary>
	/// <param name="runtime"></param>
	public static void GetAllGameLogs(bool runtime)
	{
		if(IsDebugLog())
		{
			Debug.Log("<color=yellow>Retrieving all Game Logs from the Cloud.</color>");
		}
		
		// Get all objects from table 'GameLog'.
		CloudConnectorCore.GetTable(GameLogTableName, runtime);
	}
	
	/// <summary>
	/// Parse the data and save it for local use.
	/// </summary>
	/// <param name="query"></param>
	/// <param name="objTypeNames"></param>
	/// <param name="jsonData"></param>
	public static void ParseData(CloudConnectorCore.QueryType query, List<string> objTypeNames, List<string> jsonData)
	{
		foreach (string t in objTypeNames)
		{
			if(IsDebugLog())
			{
				Debug.Log("Data type/table: " + t);
			}
		}


		// First check the type of answer.
		if (query == CloudConnectorCore.QueryType.getTable)
		{
			// Check if the type is correct.
			
			// Getting game log data
			// ReSharper disable once StringCompareIsCultureSpecific.1
			if (string.Compare(objTypeNames[0], GameLogTableName) == 0)
			{
				// Parse from JSON to the desired object type.
				GameLog[] session = GSFUJsonHelper.JsonArray<GameLog>(jsonData[0]);
				
				string logMsg = "<color=yellow>" + session.Length + " objects retrieved from the cloud and parsed:</color>";
				TrainData = new string[session.Length][];
				
				// For each recorded round
				for (int roundNumber = 0; roundNumber < session.Length; roundNumber++)
				{
					// Log raw data to console
					logMsg += "\n" +
					          "<color=blue>Session: " + session[roundNumber].GameSessionName + "</color>\n" +
					          "Mission: " + session[roundNumber].Mission + "\n" +
					          "Buildings: " + session[roundNumber].Buildings + "\n" +
					          "Heroes: " + session[roundNumber].Heroes + "\n" +
					          "Rounds left: " + session[roundNumber].RoundsLeft + "\n" +
					          "Zombies Dead: " + session[roundNumber].ZombiesDead + "\n" +
					          "Zombies on Board: " + session[roundNumber].ZombiesOnBoard + "\n" +
					          "Heroes Dead: " + session[roundNumber].HeroesDead + "\n" +
					          "Buildings Taken Over: " + session[roundNumber].BuildingsTakenOver + "\n" +
					          "Buildings Lights out: " + session[roundNumber].BuildingsLightsOut + "\n" +
					          "Spawning pits: " + session[roundNumber].SpawningPits + "\n" +
					          "Winner: " + session[roundNumber].GameWon + "\n";
					
					// Add to array of the raw data for training
					TrainData[roundNumber] = new string[12];
					TrainData[roundNumber][0] = session[roundNumber].GameSessionName;
					TrainData[roundNumber][1] = session[roundNumber].Mission;
					TrainData[roundNumber][2] = session[roundNumber].Buildings;
					TrainData[roundNumber][3] = session[roundNumber].Heroes;
					TrainData[roundNumber][4] = session[roundNumber].RoundsLeft.ToString();
					TrainData[roundNumber][5] = session[roundNumber].ZombiesDead.ToString();
					TrainData[roundNumber][6] = session[roundNumber].ZombiesOnBoard.ToString();
					TrainData[roundNumber][7] = session[roundNumber].HeroesDead.ToString();
					TrainData[roundNumber][8] = session[roundNumber].BuildingsTakenOver.ToString();
					TrainData[roundNumber][9] = session[roundNumber].BuildingsLightsOut.ToString();
					TrainData[roundNumber][10] = session[roundNumber].SpawningPits.ToString();
					TrainData[roundNumber][11] = session[roundNumber].GameWon;
					
				}

				if(IsDebugLog())
				{
					Debug.Log(logMsg);
					Debug.Log(TrainData.Length);
				}
				FileIo.SaveData(TrainData, "raw_data.txt");
			}

			// Getting network data
			// ReSharper disable once StringCompareIsCultureSpecific.1
			if (string.Compare(objTypeNames[0], "network") == 0)
			{
				// Has all settings on the table.
				NetworkSettings[] settings = GSFUJsonHelper.JsonArray<NetworkSettings>(jsonData[0]);
				int last = settings.Length - 1;
				
				string[][] lastNetworkSetting = new string[7][];
				lastNetworkSetting[0] = new string[1];
				lastNetworkSetting[0][0] = settings[last].NumInputs;
				lastNetworkSetting[1] = new string[1];
				lastNetworkSetting[1][0] = settings[last].NumRbfNodes;
				lastNetworkSetting[2] = new string[1];
				lastNetworkSetting[2][0] = settings[last].NumOutputs;
				lastNetworkSetting[3] =
					new string[Convert.ToInt16(settings[last].NumInputs) * Convert.ToInt16(settings[last].NumRbfNodes)];
				lastNetworkSetting[3] = settings[last].Centers.Split(',');
				lastNetworkSetting[4] = new string[Convert.ToInt16(settings[last].NumRbfNodes)];
				lastNetworkSetting[4] = settings[last].Width.Split(',');
				lastNetworkSetting[5] =
					new string[Convert.ToInt16(settings[last].NumRbfNodes) * Convert.ToInt16(settings[last].NumOutputs)];
				lastNetworkSetting[5] = settings[last].Weights.Split(',');
				lastNetworkSetting[6] = new string[Convert.ToInt16(settings[last].NumOutputs)];
				lastNetworkSetting[6] = settings[last].Bias.Split(',');
				
				FileIo.SaveData(lastNetworkSetting, "downloadedNetwork.txt");
			}
		}
	}
}