﻿using System;
using System.Collections.Generic;
using Helper;
using UnityEngine;

/// <summary>
/// Control and access for the different elements in the game.
/// </summary>
public class GameSession
{
    private readonly Missions missionOptions = new Missions();
    private Mission currentMission;
    private readonly Tiles mapTiles = new Tiles();
    private readonly Heroes heroCharacters = new Heroes();
    private readonly Zombies zombies = new Zombies();
    private RadialBasisFunctionNetwork rbfSession;
    private double[] currentGameDifficulty; // [Hero, Zombie]
    private bool networkCreated;

    /// <summary>
    /// Setup a session
    /// Get a mission, tiles, and heroes.
    /// </summary>
    public void SetupSession()
    {
        if (FileIo.CheckIfFileExist("raw_data.txt"))
        {
            Normalization.ConvertRawData(FileIo.LoadData("raw_data.txt"));
        }
        
        // Get the mission, check if campaign or not
        currentMission = PlayerPrefs.GetString(Strings.PlayerPrefKeys.GameType) == Strings.PlayerPrefKeys.GameTypeCampaign
            ? CampaignManager.GetNextMission()
            : missionOptions.GetSessionMission();
        
        // Add any additional special rules
        List<Mission.SpecialRulesTypes> allRuleTypes = new List<Mission.SpecialRulesTypes> // match string list
        {
            Mission.WellStockedBuilding,
            Mission.HeroesReplenish,
            Mission.HeroStartingCards1,
            Mission.HeroStartingCards2,
            Mission.FreeSearchMarkers,
            Mission.ZombieGraveDead,
            Mission.ZombieAutoSpawn,
            Mission.ZombieHorde21
        };

        for(int i = 0; i < Strings.PlayerPrefKeys.SpecialRules.Length; i++)
        {
            if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.SpecialRules[i]) == 1)
            {
                bool addRule = true;
                
                foreach(var specialRule in currentMission.SpecialRules)
                {
                    if(specialRule.KeyMatch(Strings.PlayerPrefKeys.SpecialRules[i]))
                    {
                        addRule = false;
                    }
                }

                if(addRule)
                {
                    currentMission.SpecialRules.Add(allRuleTypes[i]);
                    DebugLogger.WriteLog("Adding rule: " + allRuleTypes[i].Name);
                }
            }
        }
        
        mapTiles.GameTiles();
        heroCharacters.HeroesInTheGame();
        
        // If zombie horde
        DebugLogger.WriteLog(currentMission.Name);
        DebugLogger.WriteLog(currentMission.SpecialRules);
        if (currentMission.SpecialRules.Contains(Mission.ZombieHorde21))
        {
            zombies.GetZombieTypes()[0].ZombiesInPool = 21;
        }
        
        // If plague zombies
        if (currentMission.SpecialRules.Contains(Mission.PlagueCarriers))
        {
            zombies.AddPlagueCarrierZombies();
        }
        
        //Change any hero start location information.
        foreach (Hero hero in heroCharacters.GameHeroes)
        {
            // If mission overrides all Heroes starting locations.
            if (currentMission.AllHeroStartLocationChanged)
            {
                hero.StartLocation = currentMission.HeroStartLocation;
            }
            else
            {
                if (mapTiles.GetListOfBuildingsInThisGameSession().Contains(hero.StartLocation)) continue;

                // If mission overrides the Hero location if they would normally start in the center.
                if (currentMission.CenterHeroStartLocationChanged)
                {
                    hero.StartLocation = currentMission.HeroStartLocation;
                }

                // Set Hero start location, check if Hero should start in random building or not.
                hero.StartLocation = hero.StartLocation == "Random Building"
                    ? mapTiles.GetRandomBuildingInGameTiles().GetName()
                    : "Center Space";
            }
        }
        
        // Get the information for the rbf network then create it.
        GsfuUtilsLnoe.GetNetworkSettings(true);
        CloudConnectorCore.processedResponseCallback.AddListener(GsfuUtilsLnoe.ParseData);

        CreateNetwork();

    }

    /// <summary>
    /// Create the RBF network to use during the session.
    /// </summary>
    private void CreateNetwork()
    {
        rbfSession = new RadialBasisFunctionNetwork();
        
        if (FileIo.CheckIfFileExist("downloadedNetwork.txt"))
        {
            string[][] networkSettingsString = FileIo.LoadData("downloadedNetwork.txt");
            double[][] networkSettings = Normalization.MakeDoubleMatrixFromStringMatrix(networkSettingsString);
            rbfSession.LoadNetworkSettings(networkSettings);
            networkCreated = true;
        }
        else
        {
            DebugLogger.WriteLog("NETWORK NOT CREATED");
        }
    }

    /// <summary>
    /// Get the game session mission.
    /// </summary>
    /// <returns>The mission.</returns>
    public Mission GetCurrentMission()
    {
        return currentMission;
    }

    /// <summary>
    /// Get access to the map tiles.
    /// </summary>
    /// <returns>The tiles.</returns>
    public Tiles GetMapTiles()
    {
        return mapTiles;
    }

    /// <summary>
    /// Get access to the heroes.
    /// </summary>
    /// <returns>The heroes.</returns>
    public Heroes GetHeroCharacters()
    {
        return heroCharacters;
    }

    /// <summary>
    /// Get access to the zombie types.
    /// </summary>
    /// <returns>The zombie types.</returns>
    public Zombies GetZombieTypes()
    {
        return zombies;
    }

    /// <summary>
    /// Compute the current game state difficulty.
    /// </summary>
    public void SetGameDifficulty(double[] gameState, double heroProgress, double zombieProgress)
    {
        if (gameState != null && networkCreated)
        {
            currentGameDifficulty = rbfSession.ComputeOutputs(gameState);
            double[] progress = new double[2];
            double progressMax = Math.Max(heroProgress, zombieProgress);
            progress[0] = Normalization.DoubleNormalization(heroProgress, 0, progressMax, 0, 1);
            progress[1] = Normalization.DoubleNormalization(zombieProgress, 0, progressMax, 0, 1);

            progress = rbfSession.Softmax(progress);
        
            // Adjust based on objective progress
            // TODO add all the objectives to recorded game data
            currentGameDifficulty[0] = currentGameDifficulty[0] * progress[0] / progress[1];
            currentGameDifficulty[1] = currentGameDifficulty[1] * progress[1] / progress[0];
            currentGameDifficulty = rbfSession.Softmax(currentGameDifficulty);

            DebugLogger.WriteLog("<color=red>Difficulty " + currentGameDifficulty[0] + " " + currentGameDifficulty[1] + "</color>");
        }
        else
        {
            currentGameDifficulty = new[] {0.5, 0.5};
            CreateNetwork();
        }
    }

    /// <summary>
    /// Gets the current game difficulty.
    /// </summary>
    /// <returns>The difficulty array. [Hero, Zombie]</returns>
    public double[] GetGameDifficulty()
    {
        if(currentGameDifficulty == null)
        {
            currentGameDifficulty = new[] {0.5, 0.5};
        }
        return currentGameDifficulty;
    }
}