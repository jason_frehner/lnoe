﻿using UnityEngine;

[ExecuteInEditMode]
public class ButtonDamageOverlay : MonoBehaviour
{
	[Range(0.0f, 3.0f)]
	public float Intensity;

	public Material damageMaterial;

	private void Start ()
	{
		damageMaterial.SetFloat(Strings.MaterialProperties.Step, Intensity);
	}

	private void Update()
	{
		if(damageMaterial == null)
		{
			Debug.Log("No damageMaterial");
			damageMaterial = Resources.Load<Material>(Strings.Materials.Splatter);
		}
		
		damageMaterial.SetFloat(Strings.MaterialProperties.Step, Intensity);
	}
}
