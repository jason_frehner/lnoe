﻿using UnityEngine;

public static class Actions
{
	public static int RollD6()
	{
		return Random.Range (1, 7);
	}
	
	public static int RollD3()
	{
		return Random.Range (1, 4);
	}
	
	public static int[] FightRollArray(int numberFightDice)
	{
		int[] roll = new int[numberFightDice];
		for(int i = 0; i < numberFightDice; i++)
			roll[i] = RollD6();
		return roll;
	}
}