﻿using UnityEngine;

public class ButtonOverlayPosition: MonoBehaviour
{

	public bool RightAngles;
	
	private void Start()
	{
		// Position
		Transform buttonsParentTransform = gameObject.transform.parent.parent;
		Transform button = transform.parent;
		if(buttonsParentTransform.childCount > 0)
		{
			if(buttonsParentTransform.GetChild(0).GetInstanceID() == button.GetInstanceID())
			{
				float adjustAmount = GetComponent<RectTransform>().sizeDelta.y / 2;
				
				GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, adjustAmount, 0);
			}
			
			if(buttonsParentTransform.GetChild(buttonsParentTransform.childCount - 1).GetInstanceID() == button.GetInstanceID())
			{
				float adjustAmount = GetComponent<RectTransform>().sizeDelta.y / 2;
				
				GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, adjustAmount, 0);
			}
		}
		
		
		// Rotation
		float newAngle;
		
		if(RightAngles)
		{
			float[] angles = {0.0f, 90.0f, 180.0f, 270.0f};
			int random = Random.Range(0, 3);
			newAngle = angles[random];
		}
		else
		{
			newAngle = Random.Range(0.0f, 360.0f);
		}
		
		gameObject.GetComponent<RectTransform>().Rotate(new Vector3(0,0,1), newAngle);
		
	}
}
