﻿using System.IO;
using System.Text;
using Helper;
using UnityEngine;

public static class FileIo
{
    /// <summary>
    /// Save raw data to project folder
    /// </summary>
    /// <param name="raw"></param>
    /// <param name="fileName"></param>
    public static void SaveData( string[][] raw, string fileName)
    {
        // ReSharper disable once RedundantAssignment
        string filePath = Application.persistentDataPath + "/data/" + fileName;
        
#if UNITY_EDITOR_OSX
        filePath = Application.dataPath + "/RBF/" + fileName;
#endif
 
        string delimiter = ";";  
 
        string[][] output = raw;
		
        int length = output.GetLength(0);
        StringBuilder sb = new StringBuilder();  
        for (int index = 0; index < length; index++)  
            sb.AppendLine(string.Join(delimiter, output[index]));

        // Check if path exist
        if (!Directory.Exists(Path.GetDirectoryName(filePath)))
        {
            string directory = Path.GetDirectoryName(filePath);
            if (directory != null)
            {
                Directory.CreateDirectory(directory);
            }
        }
        
        File.WriteAllText(filePath, sb.ToString());
    }

    /// <summary>
    /// Load data from file
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public static string[][] LoadData(string fileName)
    {
        string fileContents;
        
        // ReSharper disable once RedundantAssignment
        string path = Application.persistentDataPath + "/data/" + fileName;

        
        
#if UNITY_EDITOR_OSX
        path = Application.dataPath + "/RBF/" + fileName;
#endif

        if (!CheckIfFileExist(fileName))
        {
            fileContents = ((TextAsset) Resources.Load(fileName)).text;
        }
        else
        {
            fileContents = File.ReadAllText(path);
        }

        string[] rows = fileContents.Trim().Split('\n');
		
        string[][] allData = new string[rows.Length][];
        for (int i = 0; i < rows.Length; i++)
        {
            string[] r = rows[i].Trim().Split(';');
            allData[i] = r;
            DebugLogger.WriteLog(r);
        }

        DebugLogger.WriteLog("File loaded: " + path);
        
        return allData;
    }

    /// <summary>
    /// Checks if file exist.
    /// </summary>
    /// <param name="fileName">Name of the file.</param>
    /// <returns></returns>
    public static bool CheckIfFileExist(string fileName)
    {
        // ReSharper disable once RedundantAssignment
        string path = Application.persistentDataPath + "/data/" + fileName;
        
#if UNITY_EDITOR_OSX
//        path = Application.dataPath + "/RBF/" + fileName;
#endif
        
        if (File.Exists(path))
        {
            return true;
        }
        return false;
    }
}