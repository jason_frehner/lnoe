﻿using System;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

//https://forum.unity3d.com/threads/nfc-for-android-unity-5-6.485551/

public class NFC : MonoBehaviour
{
	public enum NFCMode
	{
		Read, Write
	}
	
	public NFCMode Mode = NFCMode.Write;

	public Text ScanText;
	public Text OutputText;
	public InputField InputText;

	public GameObject[] AfterScanButtons;

	private bool tagFound;

	private AndroidJavaClass nfcPluginClass;
	private AndroidJavaObject currentActivity;
	private AndroidJavaObject pendingIntent;
	private AndroidJavaObject currentIntent;
	private string currentAction;
	private LoadScene loadScene;

	private string payload;

	private const string UnityPlayer = "com.unity3d.player.UnityPlayer";
	private const string Parcelable = "getParcelableExtra";
	private const string ExtraTag = "android.nfc.extra.TAG";

	private string sceneName;
	
	private bool isDebugLog;

	private void Awake()
	{
#if DEBUG
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.DebugMode, 1);
#endif
		isDebugLog = PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
#if UNITY_EDITOR
		PlayerPrefs.SetInt("nfcEnabled", 0);
		
#elif UNITY_ANDROID
		nfcPluginClass = new AndroidJavaClass("com.jasonfrehner.nfcplugin.NFC");
		nfcPluginClass.CallStatic("foreground");
#endif
	}

#if !UNITY_EDITOR
	// Use this for initialization
	private void Start()
	{
		nfcPluginClass = new AndroidJavaClass("com.jasonfrehner.nfcplugin.NFC");

		loadScene = GameObject.Find(Strings.GameObjectNames.Manager).GetComponent<LoadScene>();

		sceneName = SceneManager.GetActiveScene().name;
		
		if (sceneName == "Main")
		{
			CheckIfNFCEnabled();
		}
		else
		{
			if (sceneName == "LoadNFC")
			{
				Mode = NFCMode.Read;
			}
			ClearTag();
		}
	}

	private void Update()
	{
		if (sceneName == "Main") return;
		
		// Get Android activity
		currentActivity = new AndroidJavaClass(UnityPlayer).GetStatic<AndroidJavaObject>("currentActivity");
		currentIntent = currentActivity.Call<AndroidJavaObject>("getIntent");
		currentAction = currentIntent.Call<string>("getAction");

		if(isDebugLog)
		{
			Debug.Log("Tag Found: " + tagFound);
		}
		if (tagFound) return;
		// NDEF
		switch (currentAction)
		{
			case "android.nfc.action.NDEF_DISCOVERED":
				if(isDebugLog)
				{
					Debug.Log("Tag type NDEF");
				}
				break;
			case "android.nfc.action.TECH_DISCOVERED":
				if(isDebugLog)
				{
					Debug.Log("TAG DISCOVERED");
				}

				// Get Tag object
				AndroidJavaObject scannedTag = currentIntent.Call<AndroidJavaObject>(Parcelable, ExtraTag);
				if(isDebugLog)
				{
					Debug.Log("Have tag: " + scannedTag);
				}

				if (scannedTag != null)
				{
					switch (Mode)
					{
						case NFCMode.Read: // Read Tag
							payload = nfcPluginClass.CallStatic<string>("readTag", scannedTag);
							if(isDebugLog)
							{
								Debug.Log(payload);
							}
							if (payload == null || payload.Length < 10)
							{
								OutputText.text = "Tag is empty.";
							
								ScanText.enabled = false;
								AfterScanButtons[1].SetActive(true);
							}
							else
							{
								payload = payload.Substring(6);
								OutputText.text = CampaignManager.NFCSummary(payload);
							
								ScanText.enabled = false;
								foreach (GameObject button in AfterScanButtons)
								{
									button.SetActive(true);
								}
							}
							break;
						case NFCMode.Write: // Write Tag
							nfcPluginClass.CallStatic("writeTag", PlayerPrefs.GetString(Strings.PlayerPrefKeys.NFC));
							PlayerPrefs.DeleteKey(Strings.PlayerPrefKeys.NFC);
							if(isDebugLog)
							{
								Debug.Log("writing to tag.");
							}
							loadScene.GoToScene(Strings.SceneNames.CampSetup);
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}

					tagFound = true;
				}
				else
				{
					ClearTag();
				}
				break;
				
			case "android.nfc.action.TAG_DISCOVERED":
				if(isDebugLog)
				{
					Debug.Log("Some other type of tag.");
				}
				break;
			default:
				OutputText.text = "No tag...";
				break;
		}
	}
#endif

    /// <summary>
    /// Clears the tag.
    /// </summary>
    [UsedImplicitly]
	public void ClearTag()
	{
		nfcPluginClass.CallStatic("removeTagFromIntent");
		tagFound = false;
		OutputText.text = "...";
	}

	/// <summary>
	/// Check if this device has NFC enabled.
	/// </summary>
	[UsedImplicitly]
	public void CheckIfNFCEnabled()
	{
		int nfcEnabled = 0;
		if (nfcPluginClass.CallStatic<bool>("nfcEnabled"))
		{
			nfcEnabled = 1;
		}
		PlayerPrefs.SetInt("nfcEnabled", nfcEnabled);
		if(isDebugLog)
		{
			Debug.Log("nfcEnabled: " + nfcEnabled);
		}
	}

    /// <summary>
    /// Loads the game from NFC.
    /// </summary>
    [UsedImplicitly]
	public void LoadGameFromNFC()
	{
		PlayerPrefs.SetString(Strings.PlayerPrefKeys.NFC, payload);
		loadScene.GoToSceneWithLoadingScreen(Strings.SceneNames.CampSetup);
		
		ClearTag();
	}

    /// <summary>
    /// Scans the again.
    /// </summary>
    [UsedImplicitly]
	public void ScanAgain()
	{
		ScanText.enabled = true;
		foreach (GameObject button in AfterScanButtons)
		{
			button.SetActive(false);
		}
		ClearTag();
	}
}