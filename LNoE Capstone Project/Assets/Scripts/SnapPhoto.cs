﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Snap photo.
/// </summary>
public class SnapPhoto : MonoBehaviour {

	private WebCamTexture cameraTexture;
	private WebCamTexture frontCam, backCam;

	public GameObject CameraViewPlane;
	public GameLog GameLog;

	private bool cameraNativeSizeSet;
//	private int cameraID = 0;

	private void Start()
	{
		//Get camera names
		frontCam = new WebCamTexture (WebCamTexture.devices[0].name);
		backCam = new WebCamTexture (WebCamTexture.devices[WebCamTexture.devices.Length - 1].name);

		//Set back as default
		cameraTexture = backCam;

		frontCam.requestedWidth = 700;
		backCam.requestedWidth = 700;

		//Pass Camera feed to Raw Image
		RawImage raw = CameraViewPlane.GetComponent<RawImage> ();
		raw.texture = cameraTexture;
	}

	private void Update()
	{
		//Update aspect ratio
		if (cameraTexture.isPlaying && cameraTexture.height > 100 && !cameraNativeSizeSet) {

			//Match aspect ratio
			CameraViewPlane.GetComponent<AspectRatioFitter> ().aspectRatio = (float) cameraTexture.width / cameraTexture.height;

			//Match orientation
			CameraViewPlane.transform.rotation = CameraViewPlane.transform.rotation *
			                           Quaternion.AngleAxis(cameraTexture.videoRotationAngle, Vector3.up);

			//Adjust for mirrored image
			CameraViewPlane.GetComponent<RawImage>().uvRect = cameraTexture.videoVerticallyMirrored
				? new Rect(0, 1, 1, -1)
				: new Rect(0, 0, 1, 1);

			cameraNativeSizeSet = true;
		}
	}

	/// <summary>
	/// Starts the camera.
	/// </summary>
	[UsedImplicitly]
	public void StartCamera()
	{
		cameraTexture.Play ();
		cameraNativeSizeSet = false;
	}

	/// <summary>
	/// Stops the camera.
	/// </summary>
	[UsedImplicitly]
	public void StopCamera()
	{
		cameraTexture.Stop ();
		cameraNativeSizeSet = false;
	}

	/// <summary>
	/// Captures the image from the camera.
	/// </summary>
	[UsedImplicitly]
	public void CaptureImage()
	{
		//Create a pixel array
		Color32[] pixels = new Color32[cameraTexture.width * cameraTexture.height];

		//Get an image
		//Not encoded so can not use it directly
		Texture2D capturedImage = new Texture2D (cameraTexture.width, cameraTexture.height);
		capturedImage.SetPixels32 (cameraTexture.GetPixels32 (pixels));
//		capturedImage.Compress(false);

		//Convert the images to bytes
//		byte[] imageBytes = capturedImage.GetRawTextureData();
		byte[] imageBytes = capturedImage.EncodeToJPG();

		//Create a new image and load the bytes
		Texture2D imageConverted = new Texture2D(cameraTexture.width, cameraTexture.height, TextureFormat.RGB24, false);
		imageConverted.LoadImage (imageBytes);

		//Add the image to the log
		GameLog.AddImage (imageConverted);
	}

	/// <summary>
	/// Switches the camera.
	/// </summary>
	[UsedImplicitly]
	public void CameraSwitch()
	{
		//Stop
		StopCamera ();

		//Switch
		cameraTexture = cameraTexture.deviceName == frontCam.deviceName ? backCam : frontCam;

		//Reset the feed image
		RawImage raw = CameraViewPlane.GetComponent<RawImage> ();
		raw.texture = cameraTexture;

		//Start the camera back up
		StartCamera ();
	}

}
