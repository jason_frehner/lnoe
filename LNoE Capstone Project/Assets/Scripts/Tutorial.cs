﻿
public class Tutorial
{
                      public const string Intro =
                                            "Last Night on Earth is a board game that takes place over several rounds, " +
                                            "and players take the role of heroes in the game and fight zombies and try " +
                                            "to accomplish their objective. All the players work together and win or " +
                                            "lose together. \n\n This companion app is meant to be used with the " +
                                            "physical game.";

                      public const string Setup =
                                            "The companion assists with setup by showing how the game map should be " +
                                            "built and selecting the heroes for the players to use. When starting a " +
                                            "new mission, you can choose the mission and heroes if you want.";
                      
                      public const string PlayingCards =
                                            "The companion takes care of all the Zombie's actions on its turn. This " +
                                            "includes playing cards and managing the Zombie cards. Here a Zombie is " +
                                            "playing a card if you have the ability and want to prevent this card's " +
                                            "effect press 'cancel.' Otherwise perform any task the card says and then " +
                                            "press 'continue.'";
                      
                      public const string Movement =
                                            "Here the companion indicates how the Zombies should move. The action " +
                                            "should be attempted from top to bottom for each zombie. If the first option " +
                                            "does not apply to the zombie, proceed to the next continue in this way until " +
                                            "one of the options is completed. Then move on to the next zombie.";

                      public const string Fight = 
                                            "When fighting a zombie, the companion may play additional " +
                                            "cards and will display the zombie final dice roll. If the zombie or hero " +
                                            "is killed during this fight, press the corresponding button to let the " +
                                            "system know.";
                      
                      public const string RemainsInPlay =
                                            "Cards that are Remains in Play, are added to this log where they can be " +
                                            "reviewed and removed. \n\n You may also find it helpful to still use these " +
                                            "physical cards near your heroes as a reminder.";
                      
                      public const string Speak =
                                            "The companion can also recognize some voice commands. Press the 'Speak' " +
                                            "button and try saying 'Kill Zombie.'";
}