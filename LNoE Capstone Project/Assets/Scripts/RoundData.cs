﻿using System;

/// <summary>
/// Class for the data of each Round.
/// </summary>
[Serializable]
public class RoundData
{
    private readonly string heroes;
    private readonly int roundsLeft;
    private readonly int zombiesDead;
    private readonly int zombiesOnBoard;
    private readonly int heroesDead;
    private readonly int buildingsTakenOver;
    private readonly int buildingsLightsOut;
    private readonly int spawningPits;

    /// <summary>
    /// Constructor of the RoundData class.
    /// Created for each round.
    /// </summary>
    /// <param name="heroes">Heroes in the game.</param>
    /// <param name="roundsLeft">Rounds left in the game</param>
    /// <param name="zombiesDead">The number of Zombies have dies so far.</param>
    /// <param name="zombiesOnBoard">The number of Zombies on the board.</param>
    /// <param name="heroesDead">The number of heroes that have died.</param>
    /// <param name="buildingsTakenOver">The number of buildings taken over.</param>
    /// <param name="buildingsLightsOut">The number of buildings with lights out.</param>
    /// <param name="spawningPits">The number of spawning pits.</param>
    public RoundData(string heroes, int roundsLeft, int zombiesDead, int zombiesOnBoard, int heroesDead, int buildingsTakenOver, int buildingsLightsOut, int spawningPits)
    {
        this.heroes = heroes;
        this.roundsLeft = roundsLeft;
        this.zombiesDead = zombiesDead;
        this.zombiesOnBoard = zombiesOnBoard;
        this.heroesDead = heroesDead;
        this.buildingsTakenOver = buildingsTakenOver;
        this.buildingsLightsOut = buildingsLightsOut;
        this.spawningPits = spawningPits;
    }

    /// <summary>
    /// Gets the heroes.
    /// </summary>
    /// <returns></returns>
    public string GetHeroes()
    {
        return heroes;
    }

    /// <summary>
    /// Gets the rounds left.
    /// </summary>
    /// <returns></returns>
    public int GetRoundsLeft()
    {
        return roundsLeft;
    }

    /// <summary>
    /// Gets the zombies dead.
    /// </summary>
    /// <returns></returns>
    public int GetZombiesDead()
    {
        return zombiesDead;
    }

    /// <summary>
    /// Gets the zombies on board.
    /// </summary>
    /// <returns></returns>
    public int GetZombiesOnBoard()
    {
        return zombiesOnBoard;
    }

    /// <summary>
    /// Gets the heroes dead.
    /// </summary>
    /// <returns></returns>
    public int GetHeroesDead()
    {
        return heroesDead;
    }

    /// <summary>
    /// Gets the buildings taken over.
    /// </summary>
    /// <returns></returns>
    public int GetBuildingsTakenOver()
    {
        return buildingsTakenOver;
    }

    /// <summary>
    /// Gets the buildings lights out.
    /// </summary>
    /// <returns></returns>
    public int GetBuildingsLightsOut()
    {
        return buildingsLightsOut;
    }

    /// <summary>
    /// Gets the spawning pits.
    /// </summary>
    /// <returns></returns>
    public int GetSpawningPits()
    {
        return spawningPits;
    }
}