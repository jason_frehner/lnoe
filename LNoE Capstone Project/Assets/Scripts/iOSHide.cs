﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class iOSHide : MonoBehaviour
{

	public GameObject[] AppleHideObjects;
	
	private void Awake()
	{
		foreach(GameObject o in AppleHideObjects)
		{
			o.SetActive(false);
		}
	}
}
