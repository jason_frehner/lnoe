﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoSaveContinue : MonoBehaviour {

	// Use this for initialization
	private void Start ()
	{
		if(!PlayerPrefs.HasKey(Strings.PlayerPrefKeys.AutoSave))
		{
			PlayerPrefs.SetInt(Strings.PlayerPrefKeys.AutoSave, 0);
		}
		
		if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.AutoSave) == 0)
		{
			gameObject.SetActive(false);
		}	
	}
}
