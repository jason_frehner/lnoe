public enum ZombieType
{
    Behemoth, Feral, Grave, Normal, Hero, Plague
}