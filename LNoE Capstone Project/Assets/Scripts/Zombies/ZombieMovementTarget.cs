public enum ZombieMovementTarget
{
    ClosestHero, ClosestBuilding, Center, PickHero, PickBuilding
}