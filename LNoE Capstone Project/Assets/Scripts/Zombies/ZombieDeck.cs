﻿using System.Collections.Generic;
using System.Linq;
using Helper;
using UnityEngine;

/// <summary>
/// Zombie deck.
/// </summary>
public class ZombieDeck
{
	//Three decks to be managed
	private List<ZombieCard> deck = new List<ZombieCard>();

	private readonly List<ZombieCard> discard = new List<ZombieCard>();
	private readonly List<ZombieCard> hand = new List<ZombieCard>();
	private readonly List<ZombieCard> handDoNotPlay = new List<ZombieCard>();
	// ReSharper disable once CollectionNeverQueried.Local
	private readonly List<ZombieCard> remains = new List<ZombieCard>();
	private List<ZombieCard> uniqueCards = new List<ZombieCard>();

	private bool IsDebugLog()
	{
		return PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
	}

	/// <summary>
	/// Adds the cards too deck.
	/// </summary>
	public void AddCardsToDeck()
	{
		deck = new List<ZombieCard>();
		
		if(PlayerPrefs.GetString(Strings.PlayerPrefKeys.GameType) == "tutorial")
		{
			AddTutorialCards();
		}
		else
		{
			AddCoreBasicCards();
			AddCoreAdvancedCards();
			if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.GH) == 1)
			{
				AddGrowingHungerCards();
			}

			if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.HP1) == 1)
			{
				AddHeroPackOneCards();
			}

			if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.SOTF) == 1)
			{
				AddSurvivalOfTheFittestCards();
			}
			if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.SU) == 1)
			{
				AddStockUpCards();
			}
			if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.ST) == 1)
			{
				AddSoundtrackCard();
			}
			if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.ROTD) == 1)
			{
				AddRevengeOfTheDeadCards();
			}
			if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.ZP) == 1)
			{
				AddZombiePillageCards();
			}
			if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.AA) == 1)
			{
				AddAdvancedAbilitiesCards();
			}
            ShuffleDeck();
		}

		// Add cards to debug card list
		foreach(ZombieCard card in deck)
		{
			if(uniqueCards.All(uniqueCard => uniqueCard.GetName() != card.GetName()))
			{
				uniqueCards.Add(card);
			}
		}
		uniqueCards = uniqueCards.OrderBy(card=>card.GetName()).ToList();
	}

	/// <summary>
	/// Used in unit test.
	/// </summary>
	public void ClearAllDecks()
	{
		deck.Clear();
		discard.Clear();
		hand.Clear();
		handDoNotPlay.Clear();
		remains.Clear();
		uniqueCards.Clear();
	}

	#region Expansions
	
	/// <summary>
	/// Adds the core advanced cards to the deck.
	/// </summary>
	private void AddCoreAdvancedCards()
	{
		// Start of turn cards.
		deck.Add(new HauntedByThePast());
		deck.Add(new IDontTrustEm());
		deck.Add(new IveGotToGetToThe());
		deck.Add(new MyGodHesAZombie());
		deck.Add(new Overconfidence());
		deck.Add(new TeenAngst());
		deck.Add(new ThisCantBeHappening());

		// Play Immediately cards.
		deck.Add(new LivingNightmare());
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new LockedDoor());
		}
		deck.Add(new NightThatNeverEnds());
		deck.Add(new RottenBodies());
		deck.Add(new TheresNoTimeLeaveIt());

		// Hero wounded card.
		//deck.Add(new IFellKindaStrange()); // x2 TODO add back in US#137

		// 5 cards not used from advanced deck, would require knowing hero location.
		//deck.Add(new Bickering());
		//deck.Add(new ThisCouldBeOurLastNightOnEarth()); // x2
		//deck.Add(new UnnecessarySelfSacrifice()); // x2

		if(IsDebugLog())
		{
			Debug.Log("Core Advanced cards added.");
		}
	}

	/// <summary>
	/// Adds the core basic cards to the deck.
	/// </summary>
	private void AddCoreBasicCards()
	{
		// Start of turn cards.
		deck.Add(new Cornered());
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new LightsOut());
		}
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new RelentlessAdvance());
		}

		// Play immediately cards
		deck.Add(new HeavyRain());
		deck.Add(new ATownOverrun());
		for(int i = 0; i < 3; i++)
		{
			deck.Add(new MyGodTheyveTakenThe());
		}
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new NewSpawningPit());
		}
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new OhTheHorror());
		}
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new SurpriseAttack());
		}

		// Movement cards
		for(int i = 0; i < 6; i++)
		{
			deck.Add(new Shamble());
		}

		// Fight cards
		deck.Add(new Braains());
		for(int i = 0; i < 5; i++)
		{
			deck.Add(new UndeadHateTheLiving());
		}
		for(int i = 0; i < 5; i++)
		{
			deck.Add(new Uuuurrrggghh());
		}

		// End of turn cards
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new TheresTooMany());
		}

		// Killed by gun cards
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new Resilient());
		}

		// 3 cards not used from basic deck
		// 2x Loss of Faith, won't know if player played faith card or is checking if weapon breaks.
		// 1x Trip, won't know what player's movement roll is or if they are going to move.

		DebugLogger.WriteLog("Core Basic cards added.");
	}

	/// <summary>
	/// Adds the Growing Hunger cards to the deck.
	/// </summary>
	private void AddGrowingHungerCards()
	{
		// Start of turn cards.
		deck.Add(new Loner());
		
		// Play immediately cards.
		deck.Add(new DesperateForFlesh());
		deck.Add(new GrowingHunger());
		deck.Add(new NowhereToHide());
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new TheyreComingFromThe());
		}
		
		// Movement cards.
		deck.Add(new DraggingMeat());
		
		// Fight cards.
		deck.Add(new CaughtOffGuard());
		deck.Add(new FightingInstincts());
		deck.Add(new Overwhelmed());
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new ScratchAndClaw());
		}
		
		// After fight.
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new Bitten());
		}
		
		// End of turn.
		deck.Add(new TheyreEverywhere());
		deck.Add(new LetsSplitUp());
		
		// Not used at this time
		// Catfight, don't know when 2 heroes are in the same space.
		// Despair x 2, don't know when Hero Event card is played.
		// ItsStuck x 2, don't know when hand weapons are used.
		// KnockedAway, don't know if Hero takes a wound
		// TheLineIsDead, don't know when a Hero is searching.
	}

	/// <summary>
	/// Add the Hero Pack 1 cards to the deck.
	/// </summary>
	private void AddHeroPackOneCards()
	{
		deck.Add(new NowhereToRun());
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new ZombieSurge());
		}
		
		// Not used at this time
		// Hungry Dead
		// Twisted Ankle
	}

	/// <summary>
	/// Add the Survival of the Fittest cards to the deck.
	/// </summary>
	private void AddSurvivalOfTheFittestCards()
	{
		deck.Add(new FightForSurvival());
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new Hopeless());
		}
		for(int i = 0; i < 2; i++)
		{
			deck.Add(new TideOfTheDead());
		}
		deck.Add(new ToolsOfTheGrave());
		
		// Not used at this time
		// Deadly Surprise X2
	}

	private void AddStockUpCards()
	{
		deck.AddRange(new List<ZombieCard>
		{
			new NoItCantBe(),
			new LegionsOfTheDead(),
			new TownSecrets(),
			new Unstoppable(),
			new Unstoppable()
		});
	}

	private void AddSoundtrackCard()
	{
		deck.Add(new DanceOfTheDead());
	}

	private void AddRevengeOfTheDeadCards()
	{
		deck.AddRange(new List<ZombieCard>
		{
			new FeelsNoPain(),
			new FeelsNoPain(),
			new RisenFromTheGrave(),
			new TheSmellOfBrains()
		});
		
		//Not using DivideAndConquer
	}

	private void AddZombiePillageCards()
	{
		deck.AddRange(new List<ZombieCard>
		{
			new AngryDead(),
			new ClosingIn(),
			new FallingDarkness(),
			new Trapped()
		});
		
		//Not using Hysteria
	}

	private void AddAdvancedAbilitiesCards()
	{
		deck.AddRange(new List<ZombieCard>
		{
			new AnxiousToFeed(),
			new AnxiousToFeed(),
			new TheHungryOne(),
			new TheHungryOne()
		});
	}
	
	#endregion
	
	/// <summary>
	/// Add tutorial cards to the deck.
	/// </summary>
	private void AddTutorialCards()
	{
		//Start of turn cards.
		deck.Add(new LightsOut());
		deck.Add(new Uuuurrrggghh());
		deck.Add(new OhTheHorror());
		deck.Add(new TheresTooMany());
		
		deck.Add(new RelentlessAdvance());
		deck.Add(new Shamble());
		deck.Add(new UndeadHateTheLiving());
		deck.Add(new Uuuurrrggghh()); 
		
		deck.Add(new Shamble());
		deck.Add(new Shamble());
		deck.Add(new SurpriseAttack());
		deck.Add(new Uuuurrrggghh());

		if(IsDebugLog())
		{
			Debug.Log("Tutorial cards added.");
		}
	}
	
	#region Draw Deck
	
	/// <summary>
	/// Draws the card from deck to hand.
	/// </summary>
	public void DrawCard()
	{
		if(deck.Count < 1)
			RecycleDiscard();

		if(deck.Count <= 1) return;
		
		hand.Add(deck[0]);
		deck.RemoveAt(0);

		if(IsDebugLog())
		{
			Debug.Log("<color=white>" + hand[hand.Count - 1].GetName() + "</color>");
		}
	}
	
	/// <summary>
	/// Shuffles the deck.
	/// </summary>
	private void ShuffleDeck()
	{
		List<ZombieCard> tempDeck = new List<ZombieCard>();

		for(int i = 0; i < 3; i++) {
			while(deck.Count > 0) {
				int randomNumber = Random.Range(0, deck.Count);
				tempDeck.Add(deck [randomNumber]);
				deck.RemoveAt(randomNumber);
			}
			while(tempDeck.Count > 0) {
				int randomNumber = Random.Range(0, tempDeck.Count);
				deck.Add(tempDeck [randomNumber]);
				tempDeck.RemoveAt(randomNumber);
			}
		}
	}

	#endregion

	#region discard deck
	
	/// <summary>
	/// Gets the count of discarded cards.
	/// </summary>
	/// <returns></returns>
	public int NumberOfDiscardedCards()
	{
		return discard.Count;
	}

	public int NumberOfDiscardedRemainsInPlayCards()
	{
		int x = 0;
		foreach(ZombieCard card in discard)
		{
			if(card.IsRemainsInPlay())
			{
				x++;
			}
		}

		return x;
	}

	public void ReturnRandomDiscardedRemainsInPlayCardToHand()
	{
		List<ZombieCard> cardOptions = new List<ZombieCard>();

		foreach(ZombieCard card in discard)
		{
			if(card.IsRemainsInPlay())
			{
				cardOptions.Add(card);
			}
		}

		int x = Random.Range(0, cardOptions.Count);
		
		hand.Add(discard[x]);
		discard.RemoveAt(x);
	}

	/// <summary>
	/// Recycles the discard deck back to the draw deck.
	/// </summary>
	private void RecycleDiscard()
	{
		deck = new List<ZombieCard>(discard);
		ShuffleDeck();
		discard.Clear();
	}

	/// <summary>
	/// Returns discarded cards to the draw deck.
	/// </summary>
	public void ReturnDiscardedCardsBackToDeck()
	{
		int randomIndex = Random.Range(0, discard.Count);
		string lookingForCard = discard[randomIndex].GetName(); // TODO AI choose US#138
		if(IsDebugLog())
		{
			Debug.Log("Adding back to the deck: " + lookingForCard);
		}
		for(int i = discard.Count - 1; i >= 0; i--)
		{
			if(discard[i].GetName() != lookingForCard) continue;
			deck.Add(discard[i]);
			discard.RemoveAt(i);
		}
		ShuffleDeck();
	}

	/// <summary>
	/// Remove a random discarded card and all copies from the game.
	/// </summary>
	public void RemoveRandomDiscardedCardsFromGame()
	{
		int randomIndex = Random.Range(0, discard.Count);
		string lookingForCard = discard[randomIndex].GetName();

		for(int i = discard.Count - 1; i >= 0; i--)
		{
			if(discard[i].GetName() != lookingForCard) continue;
			discard.RemoveAt(i);
		}
	}

	/// <summary>
	/// Discards A card.
	/// </summary>
	/// <param name="card">Card.</param>
	public void DiscardACard(ZombieCard card)
	{
		discard.Add(card);
		hand.Remove(card);
	}

	/// <summary>
	/// Destroys the card.
	/// </summary>
	public void DestroyCard()
	{
		discard.RemoveAt(discard.Count - 1);
	}
	
	#endregion
	
	#region Hand deck
	
	/// <summary>
	/// Gets the hand.
	/// </summary>
	/// <returns>The hand.</returns>
	public List<ZombieCard> GetHand()
	{
		return hand;
	}

	public List<ZombieCard> GetDiscard()
	{
		return discard;
	}

	public List<ZombieCard> GetRemains()
	{
		return remains;
	}
	
	/// <summary>
	/// Checks if in hand.
	/// </summary>
	/// <returns>The if in hand.</returns>
	/// <param name="cardPhase">Card phase.</param>
	public List<ZombieCard> CheckIfInHand(PlayDuringPhase cardPhase)
	{
		return hand.Where(c => c.GetPhase() == cardPhase && c.Playable()).ToList();
	}
	
	/// <summary>
	/// Move card selected not to play this turn to a temp deck for holding.
	/// </summary>
	/// <param name="card">The card to hold</param>
	public void DoNotPlayCard(ZombieCard card)
	{
		handDoNotPlay.Add(card);
		hand.Remove(card);
	}

	/// <summary>
	/// Move the cards back from temporary do not play deck.
	/// </summary>
	public void ResetHandCards()
	{
		foreach(ZombieCard card in handDoNotPlay)
		{
			hand.Add(card);
		}
		handDoNotPlay.Clear();
	}
	
	#endregion

	#region Remains in play

	/// <summary>
	/// Returns the number of the cards that are in remains in play.
	/// </summary>
	/// <returns>The count.</returns>
	public int RemainsInPlayDeckSize()
	{
		return remains.Count;
	}
	
	/// <summary>
	/// Keeps the card in play.
	/// </summary>
	/// <param name="card">Card.</param>
	public void PutInToRemainsInPlay(ZombieCard card)
	{
		remains.Add(card);
		hand.Remove(card);
	}

	/// <summary>
	/// Removes card from play.
	/// </summary>
	/// <param name="card">Card.</param>
	public void RemoveFromRemainsInPlay(ZombieCard card)
	{
		discard.Add(card);
		remains.Remove(card);
	}

	/// <summary>
	/// Remove all copies of a card from the remains in play deck.
	/// </summary>
	/// <param name="cardName">Card to remove.</param>
	public void RemoveRemainsInPlayForCardFromName(string cardName)
	{
		foreach(ZombieCard card in discard)
		{
			if(card.GetName() != cardName) continue;
			card.RemainsInPlayReverse();
			RemoveFromRemainsInPlay(card);
		}
	}

	/// <summary>
	/// Get Random card that is in the remains in play deck.
	/// </summary>
	/// <returns>The card.</returns>
	public ZombieCard RandomCardFromRemainsDeck()
	{
		int randomCard = Random.Range(0, remains.Count);
		return remains[randomCard];
	}
	
	#endregion

	public List<string> CardListFromDeck(List<ZombieCard> deckOfCards)
	{
		List<string> cardList = new List<string>();
		foreach(ZombieCard card in deckOfCards)
		{
			cardList.Add(card.GetName());
		}

		return cardList;
	}

	public void SetDecksFromLists(List<string> handCards, List<string> discardCards, List<string> remainsCards)
	{
		AddCardsToDeck();

		AddCardsFromMainDeckToADeckFromList(handCards, hand);
		AddCardsFromMainDeckToADeckFromList(discardCards, discard);
		AddCardsFromMainDeckToADeckFromList(remainsCards, remains);
	}

	private void AddCardsFromMainDeckToADeckFromList(List<string> listOfcards, List<ZombieCard> deckToAddTo)
	{
		foreach(string cardName in listOfcards)
		{
			foreach(ZombieCard card in deck)
			{
				if(card.GetName() == cardName)
				{
					deckToAddTo.Add(card);
					deck.Remove(card);
					break;
				}
			}
		}
	}

	#region Debug
	
	/// <summary>
	/// Get the list of unique cards in the game.
	/// </summary>
	/// <returns>the cards.</returns>
	public List<ZombieCard> GetUniqueCards()
	{
		return uniqueCards;
	}

	/// <summary>
	/// Find and return the card with matching name.
	/// </summary>
	/// <param name="name">The name of the card.</param>
	/// <returns>The card.</returns>
	public ZombieCard GetCard(string name)
	{
		return uniqueCards.FirstOrDefault(card => card.GetName() == name);
	}

	/// <summary>
	/// Add a zombie card to the zombie player's hand.
	/// </summary>
	/// <param name="card">Card to add.</param>
	public void AddCardToHand(ZombieCard card)
	{
		hand.Add(card);
	}
	
	#endregion
}