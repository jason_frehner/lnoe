﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class ZombieDeadScreen : MonoBehaviour
{
	public GameObject Option1;
	public GameObject Option2;
	public GameObject Option3;
	public GameObject Option4;
	public GameObject Option5;

	public GameObject ZombieDeadPanel;
	
	private ZombieTurn zombieTurn;

	private void Start () {
		zombieTurn = GameObject.Find("ZombieManager").GetComponent<ZombieTurn>();
	}

	/// <summary>
	/// Set the options for the zombie to kill.
	/// </summary>
	/// <param name="zombies"></param>
	public void SetOptions(List<Zombie> zombies)
	{	
		Option1.transform.GetChild(1).GetComponent<Text>().text = zombies[0].GetZombieName();
		Option2.transform.GetChild(1).GetComponent<Text>().text = zombies[1].GetZombieName();
		if (zombies.Count > 2)
		{
			Option3.transform.GetChild(1).GetComponent<Text>().text = zombies[2].GetZombieName();
			Option3.SetActive(true);
		}
		else
		{
			Option3.SetActive(false);
		}
		if (zombies.Count > 3)
		{
			Option4.transform.GetChild(1).GetComponent<Text>().text = zombies[3].GetZombieName();
			Option4.SetActive(true);
		}
		else
		{
			Option4.SetActive(false);
		}
		if (zombies.Count > 4)
		{
			Option5.transform.GetChild(1).GetComponent<Text>().text = zombies[4].GetZombieName();
			Option5.SetActive(true);
		}
		else
		{
			Option5.SetActive(false);
		}
	}

	/// <summary>
	/// From buttons, kill the selected zombie.
	/// </summary>
	/// <param name="text"></param>
	[UsedImplicitly]
	public void ZombieTypeKilled(Text text)
	{
		if(zombieTurn.GetGameStats().GetZombieTypes().ZombieDied(text.text))
		{
			zombieTurn.ZombieDeathUIUpdate();
			zombieTurn.PlayClickAudio();
			GetComponent<Animator>().SetTrigger("close");
		}
		else
		{
			zombieTurn.ZombieIgnoresDeathUIUpdate();
			zombieTurn.PlayClickAudio();
			GetComponent<Animator>().SetTrigger("close");
		}
	}
}
