using System.Collections.Generic;
using System.Linq;
using Helper;
using UnityEngine;
using UnityEngine.Analytics;

/// <summary>
/// Class for tracking a tracking all information for each zombie type.
/// </summary>
public class Zombies
{
    private readonly List<Zombie> zombieTypes = new List<Zombie>();
    
    private int zombiesKilledCount;
    private bool zombieTurn = true;
    private bool zombiesCanCanBeKilled = true;
    private int tempFightDice;
    private int bonusFightDice;
    private int extraRoundDice;
    private bool multipleFightCards;

    public delegate void ZombiesOnBoard(bool value);
    public event ZombiesOnBoard ZombiesOnBoardChanged;

    public delegate void ZombiesAreKillable(bool value);
    public event ZombiesAreKillable ZombiesAreKillableChanged;

    public delegate void HeroZombieOnBoard(bool value);
    public event HeroZombieOnBoard HeroZombieOnBoardChanged;
    
    /// <summary>
    /// Constructor
    /// </summary>
    public Zombies()
    {
        // Add normal zombies to the pool.
        Zombie normal = new Zombie("Zombie", ZombieType.Normal, 1, ZombieSpeed.One, 1, 1, true,
            ZombieMovementTarget.ClosestHero) {ZombiesInPool = 14};
        zombieTypes.Add(normal);
    }

    /// <summary>
    /// Get access to a given zombie type.
    /// </summary>
    /// <param name="zombieType">The type of zombie.</param>
    /// <returns>The zombie class.</returns>
    public Zombie GetZombieOfType(ZombieType zombieType)
    {
        return zombieTypes.FirstOrDefault(zombie => zombie.GetZombieType() == zombieType);
    }

    /// <summary>
    /// Get the Zombie types in the game.
    /// </summary>
    /// <returns>List of zombies.</returns>
    public List<Zombie> GetZombieTypes()
    {
        return zombieTypes;
    }

    /// <summary>
    /// Add a zombie hero.
    /// </summary>
    /// <param name="name">The hero name.</param>
    /// <param name="health">Sets their health.</param>
    public void NewZombieHero(string name, int health)
    {
        zombieTypes.Add(new Zombie("Zombie " +  name, ZombieType.Hero, 1, ZombieSpeed.D3, health, 1, true, ZombieMovementTarget.ClosestHero));
        // Set no more in the pool and 1 on the board.
        zombieTypes[zombieTypes.Count - 1].ZombiesInPool = 0;
        zombieTypes[zombieTypes.Count - 1].ZombiesOnBoard = 1;
        Analytics.CustomEvent("zombieHero", new Dictionary<string, object>
        {
            {"name", name}
        });

        DebugLogger.WriteLog("Zombie Type Count" + zombieTypes.Count);

        if(HeroZombieOnBoardChanged != null)
            HeroZombieOnBoardChanged(true);
    }

    /// <summary>
    /// Add plague Carrier zombies to the game.
    /// Growing Hunger.
    /// </summary>
    public void AddPlagueCarrierZombies()
    {
        Zombie plague = new Zombie("Plague", ZombieType.Plague, 1, ZombieSpeed.None, 2, 2, false,
            ZombieMovementTarget.ClosestHero) {ZombiesOnBoard = 7};
        zombieTypes.Add(plague);
    }

    /// <summary>
    /// Add the Grave Dead Zombie to the game.
    /// </summary>
    public void AddGraveDeadZombies()
    {
        List<Zombie> graveDead = new List<Zombie>
        {
            new Zombie("Very Rotten Zombie", ZombieType.Grave, 1, ZombieSpeed.One, 1, 1, true, ZombieMovementTarget.ClosestHero) {ZombiesInPool = 7, IgnoreWoundOnFourPlus = true},
            new Zombie("Brainthirsty Zombie", ZombieType.Grave, 1, ZombieSpeed.One, 2, 1, true, ZombieMovementTarget.ClosestHero) {ZombiesInPool = 7},
            new Zombie("Freshly Dead Zombie", ZombieType.Grave, 1, ZombieSpeed.D3, 1, 1, true, ZombieMovementTarget.ClosestHero) {ZombiesInPool = 7},
            new Zombie("Ravenous Zombie", ZombieType.Grave, 2, ZombieSpeed.One, 1, 1, true, ZombieMovementTarget.ClosestHero) {ZombiesInPool = 7},
            //new Zombie("Pack Mentality Zombie", ZombieType.Grave, 1, ZombieSpeed.One, 1, 1, true, ZombieMovementTarget.ClosestHero) {ZombiesInPool = 7, CanSpawnWithOtherZombie = true}, //TODO
            //after wound on roll of 5+ hero becomes zombie hero
            //new Zombie("Carrier Zombie", ZombieType.Grave, 1, ZombieSpeed.One, 1, 1,  true, ZombieMovementTarget.ClosestHero) {ZombiesInPool = 7, MakesZombieHeroOnFivePlus = true} //TODO
        };

        int x = Random.Range(0, graveDead.Count - 1);
        zombieTypes.Add(graveDead[x]);
    }

    /// <summary>
    /// Feral Dead
    /// Blood in the forest.
    /// </summary>
    public void AddFeralDeadZombie()
    {
        zombieTypes.Add(new Zombie("Feral", ZombieType.Feral, 1, ZombieSpeed.Two, 1, 1, true, ZombieMovementTarget.ClosestHero));
        // TODO check if count to spawn check
        // TODO Cost spawn 2, experience 1
        // 1 Fight dice
        // Abilities: Relentless - Anytime the Feral Dead wins a Fight, the Hero must immediately Fight that Feral Dead again.
    }

    /// <summary>
    /// Behemoth zombie
    /// Blood in the forest
    /// </summary>
    public void AddBehemothZombie()
    {
        zombieTypes.Add(new Zombie("Behemoth", ZombieType.Behemoth, 2, ZombieSpeed.D3, 3, 1, true, ZombieMovementTarget.ClosestHero));
        // TODO check if counts to spawn check
        // TODO Cost spawn 6, experience 2
        // 2 Fight dice
        // Abilities: Tough - Ignores any wounds he would take on the D6 roll of 4+ each.
        // Chainsaw - For each natural roll of 6 the Behemoth gets on its Fight Dice, it does 1 extra wound if it wins the Fight.
    }
    

    /// <summary>
    /// Helper.
    /// Gets the zombie type from the list of zombies.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <returns></returns>
    private Zombie GetZombie(string name)
    {
        return zombieTypes.FirstOrDefault(zombie => zombie.GetZombieName() == name);
    }

    /// <summary>
    /// Kill the zombie.
    /// </summary>
    /// <param name="zombie"></param>
    private void KillZombie(Zombie zombie)
    {
        if (zombie.ZombiesOnBoard <= 0) return;
        if (!zombiesCanCanBeKilled) return;

        zombie.ZombiesOnBoard--;
        zombiesKilledCount++;
        
        // Only add normal zombies back to the pool
        if(zombie.GetZombieType() == ZombieType.Normal)
        {
            zombie.ZombiesInPool++;
        }
        

        // Remove the zombie
        if (zombie.GetZombieName() != "Zombie")
        {
            if (zombie.ZombiesOnBoard == 0)
            {
                zombieTypes.Remove(zombie);
            }
        }

        if(GetZombieOfType(ZombieType.Hero) == null)
        {
            if(HeroZombieOnBoardChanged != null)
                HeroZombieOnBoardChanged(false);
        }

        Analytics.CustomEvent("zombieDied", new Dictionary<string, object>
        {
            {"zombieKilled", zombie.GetZombieName()}
        });
    }

    /// <summary>
    /// Find and kill the zombie from the name.
    /// </summary>
    /// <param name="name"></param>
    public bool ZombieDied(string name)
    {
        Zombie zombie = GetZombie(name);

        if(zombie.IgnoreWoundOnFourPlus)
        {
            if(Actions.RollD6() > 3)
            {
                return false;
            }
        }
        
        KillZombie(zombie);
        return true;
    }

    /// <summary>
    /// Gets the zombie killed count.
    /// </summary>
    /// <returns></returns>
    public int GetZombiesKilledCount()
    {
        return zombiesKilledCount;
    }

    /// <summary>
    /// Get or Set if zombie can play more than one fight card.
    /// </summary>
    public bool IsMultipleFightCards
    {
        get { return multipleFightCards; }
        set { multipleFightCards = value; }
    }
    
    /// <summary>
    /// Gets or Set if it is the zombie's turn.
    /// </summary>
    public bool IsZombieTurn
    {
        get { return zombieTurn; }
        set { zombieTurn = value; }
    }
    
    /// <summary>
    /// Checks if zombies can be killed.
    /// </summary>
    /// <returns>The value.</returns>
    public bool IsZombiesCanBeKilled
    {
        get { return zombiesCanCanBeKilled; }
        set
        {
            zombiesCanCanBeKilled = value;
            if(ZombiesAreKillableChanged != null)
                ZombiesAreKillableChanged(value);
        }
    }

    /// <summary>
    /// Get the number of zombies in the pool.
    /// </summary>
    /// <returns>The count.</returns>
    public int GetZombiesInPool()
    {
        return zombieTypes.Sum(zombie => zombie.ZombiesInPool);
    }

    /// <summary>
    /// Get the number of zombies on the board.
    /// </summary>
    /// <returns>The count.</returns>
    public int GetZombiesOnBoardCount()
    {   
        int count = zombieTypes.Sum(zombie => zombie.ZombiesOnBoard);
        if(ZombiesOnBoardChanged != null)
            ZombiesOnBoardChanged(count != 0);
        return count;
    }

    /// <summary>
    /// Reset the fight dice from any temp effects.
    /// </summary>
    public void ResetFightDice()
    {
        foreach (Zombie zombie in zombieTypes)
        {
            zombie.ResetFightDice();
        }
    }

    /// <summary>
    /// Counts only the zombies on the board that count towards the spawn check.
    /// </summary>
    /// <returns></returns>
    public int SpawnCheck()
    {
        return zombieTypes.Where(zombie => zombie.CountTowardSpawnCheck).Sum(zombie => zombie.ZombiesOnBoard);
    }

    /// <summary>
    /// Get the total number of base fight dice.
    /// </summary>
    /// <returns>The total.</returns>
    public int TotalFightDiceNumber()
    {
        return tempFightDice + bonusFightDice + extraRoundDice;
    }

    /// <summary>
    /// Adjust the temp fight dice number.
    /// </summary>
    /// <param name="adjustValue">Adjustment by number.</param>
    /// <returns></returns>
    public void SetTempFightDice (int adjustValue)
    {
        tempFightDice += adjustValue;
    }

    /// <summary>
    /// Reset the temp fight dice number.
    /// </summary>
    public void ResetTempFightDice()
    {
        tempFightDice = 0;
    }

    /// <summary>
    /// Adjust the bonus fight dice number.
    /// </summary>
    /// <param name="adjustValue">Adjust by number.</param>
    public void SetBonusDice(int adjustValue)
    {
        bonusFightDice += adjustValue;
    }

    public void AddExtraRoundDice()
    {
        extraRoundDice++;
    }

    public void ResetExtraRoundDice()
    {
        extraRoundDice = 0;
    }
}