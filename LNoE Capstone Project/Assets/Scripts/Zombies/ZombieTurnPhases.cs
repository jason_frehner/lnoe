using System.Collections.Generic;
using System.Text;
using Helper;
using UnityEngine;

public class ZombieTurnPhases
{
    public delegate void TurnPhaseDelegateDeclare();
    public TurnPhaseDelegateDeclare TurnPhaseDelegate;

	private ZombieTurn zombieTurn;
	public int MoveZombieIndex;
	public ZombieTurnPhaseName PhaseName;

	/// <summary>
    /// Sets the zombie turn.
    /// </summary>
    /// <param name="zt">The Zombie Turn object.</param>
    public void SetZombieTurn(ZombieTurn zt)
	{
		zombieTurn = zt;
	}

    /// <summary>
    /// Sets the delegate to the start of turn.
    /// </summary>
    public void TurnDelegateStartOfTurn()
	{
		TurnPhaseDelegate = PhaseStartOfTurn;
	}

	/// <summary>
	/// Set the delegate to the given name.
	/// </summary>
	/// <param name="name">Name of delegate to set to.</param>
	public void SetZombieTurnPhase(ZombieTurnPhaseName name)
	{
		switch(name)
		{
			case ZombieTurnPhaseName.StartOfTurn:
				TurnPhaseDelegate = PhaseStartOfTurn;
				break;
			case ZombieTurnPhaseName.PlayStartOfTurnCards:
				TurnPhaseDelegate = PhasePlayStartOfTurnCards;
				break;
			case ZombieTurnPhaseName.DrawNewCards:
				TurnPhaseDelegate = PhaseDrawNewCards;
				break;
			case ZombieTurnPhaseName.PlayImmediatelyCards:
				TurnPhaseDelegate = PhasePlayImmediatelyCards;
				break;
			case ZombieTurnPhaseName.CheckIfNewZombiesSpawn:
				TurnPhaseDelegate = PhaseCheckIfNewZombiesSpawn;
				break;
			case ZombieTurnPhaseName.MoveZombies:
				TurnPhaseDelegate = PhaseMoveZombies;
				break;
			case ZombieTurnPhaseName.FightHeroes:
				TurnPhaseDelegate = PhaseFightHeroes;
				break;
			case ZombieTurnPhaseName.FightResults:
				TurnPhaseDelegate = PhaseFightResults;
				break;
			case ZombieTurnPhaseName.NewZombies:
				TurnPhaseDelegate = PhaseSpawnNewZombies;
				break;
			case ZombieTurnPhaseName.EndOfTurnCards:
				TurnPhaseDelegate = PhasePlayEndOfTurnCards;
				break;
			case ZombieTurnPhaseName.HeroesTurn:
				TurnPhaseDelegate = PhaseHeroesTurn;
				break;
			default:
				TurnPhaseDelegate = PhaseHeroesTurn;
				break;
		}
	}

    /// <summary>
    /// Phases the start of turn.
    /// </summary>
    public void PhaseStartOfTurn()
    {
	    DebugLogger.WriteLog("<color=green>Phase: Start of turn.</color>");
	    
	    PhaseName = ZombieTurnPhaseName.StartOfTurn;

        TurnPhaseDelegate = PhaseStartOfTurn;
        zombieTurn.GetGameStats().GetZombieTypes().IsZombieTurn = true;

        // Reset from effects
        zombieTurn.GetGameStats().GetZombieTypes().ResetFightDice();
        zombieTurn.GetGameStats().GetZombieTypes().IsZombiesCanBeKilled = true;
        zombieTurn.SetZombieWinOnTie(true);
        zombieTurn.ClearFightRoll();
        zombieTurn.GetDecks().ResetHandCards();

        // Decrease or Set round tracker
        if(zombieTurn.Round == -10)//First round of the game
            zombieTurn.Round = zombieTurn.GetGameStats().GetCurrentMission().Rounds;
        else
            zombieTurn.Round--;

        // Update text in log
        if(zombieTurn.GameLog != null) {
            zombieTurn.UpdateLogRoundDetails();
        }
		
        // Separate card log
        zombieTurn.CardsPlayed.Add("Rounds Left: " + zombieTurn.Round);

        //Goto next phase
        PhasePlayStartOfTurnCards();
    }
    
    /// <summary>
	/// Play the start of turn cards.
	/// </summary>
	private void PhasePlayStartOfTurnCards()
	{
		
		DebugLogger.WriteLog("<color=lightblue>Phase: Playing start of turn card.</color>");

		PhaseName = ZombieTurnPhaseName.PlayStartOfTurnCards;

		// Set the turn phase delegate
		TurnPhaseDelegate = PhasePlayStartOfTurnCards;
		
		// Let all the zombie types know it is the zombie turn.
		foreach(Zombie type in zombieTurn.GetGameStats().GetZombieTypes().GetZombieTypes())
		{
			type.SetZombieTurn();
		}

		//Play start of turn cards
		zombieTurn.CardsToPlay = zombieTurn.GetDecks().CheckIfInHand(PlayDuringPhase.StartOfTurn);
		if(zombieTurn.CardsToPlay.Count > 0) {
			foreach(ZombieCard card in zombieTurn.CardsToPlay) {
				if(!zombieTurn.PlayCardBasedOnDifficulty())
				{
					DebugLogger.WriteLog("<color=orange>Not Playing card " + zombieTurn.CardsToPlay[0].GetName() + "</color>");
					
					zombieTurn.CardsPlayed.Add(zombieTurn.CardsToPlay[0].GetName());
					zombieTurn.CardsPlayed.Add("");
					zombieTurn.GetDecks().DoNotPlayCard(card);
				}
				else
				{
					DebugLogger.WriteLog("Playing at the start of turn: " + card.GetName());
					
					zombieTurn.CardBeingPlayed = card;
					zombieTurn.StartPlayCard();
					return;
				}
			}
		}
		PhaseDrawNewCards();
	}

	/// <summary>
	/// Draws new cards.
	/// </summary>
	private void PhaseDrawNewCards()
	{
		DebugLogger.WriteLog("<color=lightblue>Phase: Drawing cards.</color>");

		PhaseName = ZombieTurnPhaseName.DrawNewCards;

		// Discard before draw
		bool discarded = false;
		while(zombieTurn.GetDecks().GetHand().Count > 4) { // Discard down to hand limit.
			zombieTurn.GetDecks().DiscardACard(zombieTurn.GetDecks().GetHand() [0]);
			discarded = true;
		}
		if(!discarded && zombieTurn.GetDecks().GetHand().Count > 0) // Discard oldest card.
			zombieTurn.GetDecks().DiscardACard(zombieTurn.GetDecks().GetHand() [0]);

		//Draw up
		int counter = 0;
		while(zombieTurn.GetDecks().GetHand().Count < 4 && ++counter < 5) {
			zombieTurn.GetDecks().DrawCard();
		}
		
		PhasePlayImmediatelyCards();
	}

	/// <summary>
	/// Play the immediately cards.
	/// </summary>
	private void PhasePlayImmediatelyCards()
	{
		DebugLogger.WriteLog("<color=lightblue>Phase: Playing immediately card.</color>");

		PhaseName = ZombieTurnPhaseName.PlayImmediatelyCards;
		
		if(PlayerPrefs.GetString(Strings.PlayerPrefKeys.Scenario) == "Tutorial")
		{
			zombieTurn.TutorialText.text = Tutorial.PlayingCards;
			zombieTurn.TutorialPanel.GetComponent<Animator>().SetTrigger("open");
		}

		TurnPhaseDelegate = PhasePlayImmediatelyCards;

		//Play immediately
		zombieTurn.CardsToPlay = zombieTurn.GetDecks().CheckIfInHand(PlayDuringPhase.Immediately);
		if(zombieTurn.CardsToPlay.Count > 0) {
			foreach(ZombieCard c in zombieTurn.CardsToPlay) {
				DebugLogger.WriteLog("Playing immediately: " + c.GetName());
				zombieTurn.CardBeingPlayed = c;
				zombieTurn.StartPlayCard();
				return;
			}
		}
		PhaseCheckIfNewZombiesSpawn();
	}

	/// <summary>
	/// Checks if new zombies zombieTurn.Spawn.
	/// </summary>
	private void PhaseCheckIfNewZombiesSpawn()
	{
		DebugLogger.WriteLog("<color=lightblue>Phase: Checking if new zombies will zombieTurn.Spawn.</color>");

		PhaseName = ZombieTurnPhaseName.CheckIfNewZombiesSpawn;

		if(zombieTurn.GetGameStats().GetZombieTypes().GetZombiesInPool() > 0)
		{
			if(zombieTurn.GetGameStats().GetCurrentMission().SpecialRules.Contains(Mission.ZombieAutoSpawn))
			{
				DebugLogger.WriteLog("<color=yellow>Auto Spawn Enabled.</color>");
				zombieTurn.Spawn = true;
			}
			else
			{
				//Check if new zombies Spawn
				int spawnCheck2D6Roll = Actions.RollD6() + Actions.RollD6();
				DebugLogger.WriteLog("New Spawn Check Roll: " + spawnCheck2D6Roll);
				zombieTurn.Spawn = spawnCheck2D6Roll > zombieTurn.GetGameStats().GetZombieTypes().SpawnCheck();
			}
		}
		else
		{
			zombieTurn.Spawn = false;
		}

		MoveZombieIndex = zombieTurn.GetGameStats().GetZombieTypes().GetZombieTypes().Count;
		PhaseMoveZombies();
	}

	/// <summary>
	/// Moves the zombies.
	/// </summary>
	private void PhaseMoveZombies()
	{
		DebugLogger.WriteLog("<color=lightblue>Phase: Moving zombies.</color>");

		PhaseName = ZombieTurnPhaseName.MoveZombies;

		TurnPhaseDelegate = PhaseMoveZombies;
		
		if(PlayerPrefs.GetString(Strings.PlayerPrefKeys.Scenario) == "Tutorial")
		{
			zombieTurn.TutorialText.text = Tutorial.Movement;
			zombieTurn.TutorialPanel.GetComponent<Animator>().SetTrigger("open");
		}

		//Play Move cards
		zombieTurn.CardsToPlay = zombieTurn.GetDecks().CheckIfInHand(PlayDuringPhase.Move);
		if(zombieTurn.CardsToPlay.Count > 0) {
			foreach(ZombieCard card in zombieTurn.CardsToPlay) {
				if(!zombieTurn.PlayCardBasedOnDifficulty())
				{
					DebugLogger.WriteLog("<color=orange>Not Playing card " + zombieTurn.CardsToPlay[0].GetName() + "</color>");
					zombieTurn.CardsPlayed.Add(zombieTurn.CardsToPlay[0].GetName());
					zombieTurn.CardsPlayed.Add("");
					zombieTurn.GetDecks().DoNotPlayCard(zombieTurn.CardsToPlay[0]);
				}

				DebugLogger.WriteLog("Playing Move Card: " + card.GetName());
				zombieTurn.CardBeingPlayed = card;
				zombieTurn.StartPlayCard();
				return;
			}
		}

		zombieTurn.MoveText.text = "";
		Zombie zombieMoving = zombieTurn.GetGameStats().GetZombieTypes().GetZombieTypes()[MoveZombieIndex - 1];
		DebugLogger.WriteLog(zombieTurn.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Hero));
		zombieTurn.MoveZombieTypeText.text = zombieMoving.GetZombieName();
		
		// Move the zombies
		zombieTurn.MoveText.text = zombieTurn.GetMovementText(zombieMoving);

		if(zombieTurn.Round % 3 == 0)
		{
			zombieMoving.SetRandomMovementTarget();
		}
		
		MoveZombieIndex--;
		zombieTurn.MovePanel.GetComponent<Animator>().SetTrigger("open");
	}

	

	/// <summary>
	/// Fights the heroes.
	/// </summary>
	public void PhaseFightHeroes()
	{
		DebugLogger.WriteLog("<color=lightblue>Phase: Fight.</color>");

		PhaseName = ZombieTurnPhaseName.FightHeroes;
		
		if(PlayerPrefs.GetString(Strings.PlayerPrefKeys.Scenario) == "Tutorial")
		{
			zombieTurn.TutorialText.text = Tutorial.Fight;
			zombieTurn.TutorialPanel.GetComponent<Animator>().SetTrigger("open");
		}

		// Destroy building zombie objective
		if(zombieTurn.GetGameStats().GetCurrentMission().ZombieObjective.Contains(Mission.Destroy6Buildings))
		{
			zombieTurn.DestroyBuildingZombieSlider.value = 0.0f;
			zombieTurn.DestroyBuildingDropdown.ClearOptions();
			List<string> buildingsList = new List<string> {"None"};
			buildingsList.AddRange(zombieTurn.GetGameStats().GetMapTiles().GetBuildingToDestroyPriority());
			zombieTurn.DestroyBuildingDropdown.AddOptions(buildingsList);
			zombieTurn.DestroyBuildingPanel.GetComponent<Animator>().SetTrigger("open");
		}

		TurnPhaseDelegate = PhaseSpawnNewZombies;

		zombieTurn.SetFightInfoText();

		if(zombieTurn.FightPanel.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("CardUIOut"))
		{
			zombieTurn.FightPanel.GetComponent<Animator>().SetTrigger("open");
		}
	}

	/// <summary>
	/// Fight Results.
	/// Go here when there are no more fights.
	/// </summary>
	public void PhaseFightResults()
	{
		DebugLogger.WriteLog("<color=lightblue>Phase: Fight Result</color>");

		PhaseName = ZombieTurnPhaseName.FightResults;

		TurnPhaseDelegate = PhaseSpawnNewZombies;
		TurnPhaseDelegate();
	}

	/// <summary>
	/// Spawns the new zombies.
	/// </summary>
	public void PhaseSpawnNewZombies()
	{
		DebugLogger.WriteLog("<color=lightblue>Phase: Spawning zombies.</color>");

		PhaseName = ZombieTurnPhaseName.NewZombies;

		TurnPhaseDelegate = PhasePlayEndOfTurnCards;
		
		//Spawn new
		if(zombieTurn.Spawn) {
			int[] spawnZombies = zombieTurn.SpawnZombies();

			zombieTurn.SpawnText.text = "Place " + spawnZombies[0] + " Zombie(s) evenly\nacross all Spawning Pits.\n";

			// Grave dead spawn.
			if(spawnZombies[1] > 0)
			{
				zombieTurn.SpawnText.text += "Place " + spawnZombies[1] + " Grave Dead Zombie(s) evenly\nacross all Spawning Pits.";
			}

			// Move after spawn.
			if(zombieTurn.IsZombieMovesAfterSpawn())
			{
				zombieTurn.SpawnText.text += "All spawned Zombie(s) move 1 space.";
			}
			
			zombieTurn.SpawnPanel.GetComponent<Animator>().SetTrigger("open");
		} else
			TurnPhaseDelegate();
	}

	/// <summary>
	/// Play the end of turn cards.
	/// </summary>
	private void PhasePlayEndOfTurnCards()
	{
		DebugLogger.WriteLog("<color=lightblue>Phase: Playing end of turn card.</color>");

		PhaseName = ZombieTurnPhaseName.EndOfTurnCards;

		TurnPhaseDelegate = PhasePlayEndOfTurnCards;

		//End of turn
		zombieTurn.CardsToPlay = zombieTurn.GetDecks().CheckIfInHand(PlayDuringPhase.EndOfTurn);
		if(zombieTurn.CardsToPlay.Count > 0) {
			foreach(ZombieCard c in zombieTurn.CardsToPlay) {
				if(!zombieTurn.PlayCardBasedOnDifficulty())
				{
					DebugLogger.WriteLog("<color=orange>Not Playing card " + c.GetName() + "</color>");
					zombieTurn.CardsPlayed.Add(zombieTurn.CardsToPlay[0].GetName());
					zombieTurn.CardsPlayed.Add("");
					zombieTurn.GetDecks().DoNotPlayCard(c);
				}

				DebugLogger.WriteLog("Playing at the end of turn: " + c.GetName());
				zombieTurn.CardBeingPlayed = c;
				zombieTurn.StartPlayCard();
				return;
			}
		}

		TurnPhaseDelegate = PhaseHeroesTurn;
		TurnPhaseDelegate();
	}

	/// <summary>
	/// Logs that it is the Hero turn.
	/// </summary>
	private void PhaseHeroesTurn()
	{
		DebugLogger.WriteLog("<color=lightblue>Phase: Heroes Turn.</color>");

		PhaseName = ZombieTurnPhaseName.HeroesTurn;

		zombieTurn.GetGameStats().GetZombieTypes().IsZombieTurn = false;
		
		zombieTurn.GetGameStats().GetZombieTypes().ResetExtraRoundDice();
		
		zombieTurn.TextAnimation.Play("Heroes Turn");
		
		//Build string of reminders for the users. 
		StringBuilder reminders = new StringBuilder();
		foreach(Mission.SpecialRulesTypes specialRule in zombieTurn.GetGameStats().GetCurrentMission().SpecialRules)
		{
			if(specialRule.Reminder != null)
			{
				reminders.AppendLine(specialRule.Reminder);
			}
		}
		foreach(ZombieCard card in zombieTurn.GetDecks().GetRemains())
		{
			if(card.Reminder() != null)
			{
				reminders.AppendLine(card.Reminder());
			}
		}
		if(reminders.Length > 0)
		{
			zombieTurn.HeroTurnReminderPanel.GetComponent<Animator>().SetTrigger("open");
			zombieTurn.HeroTurnReminderText.text = reminders.ToString();
		}
		
		
		if(PlayerPrefs.GetString(Strings.PlayerPrefKeys.Scenario) != "Tutorial") return;
		zombieTurn.TutorialText.text = Tutorial.RemainsInPlay;
		zombieTurn.TutorialPanel.GetComponent<Animator>().SetTrigger("open");
	}
}