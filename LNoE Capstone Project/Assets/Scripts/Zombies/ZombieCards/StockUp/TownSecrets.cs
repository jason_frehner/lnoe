public class TownSecrets : ZombieCard
{
    public TownSecrets()
    {
        Name = "Town Secrets";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Immediately reveal and discard the top 5 Hero Cards. If this discards the last Hero Card, the " +
                       "Heroes automatically lose." +
                       "or" +
                       "Choose any Hero Player to immediately discard one of their event cards in hand (Hero's choice).";
        FlavorText = "With each victim, their numbers grow.";
    }

    public override void CardSetup()
    {
        OriginalText = "Immediately reveal and discard the top 5 Hero Cards. If this discards the last " +
                       "Hero Card, the Heroes automatically lose.";
    }
}