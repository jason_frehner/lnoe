public class Unstoppable : ZombieCard
{
    public Unstoppable()
    {
        Name = "Unstoppable";
        Phase = PlayDuringPhase.KilledByGun;
        OriginalText = "Prevent all wounds being done to a single Zombie from a Ranged Attack or an exploding gas marker.";
        FlavorText = "Did you miss?\nNo, but I only took off an arm!";
    }

    public override void CardSetup()
    {
        OriginalText = "Prevent all wounds being done to a single Zombie from a Ranged Attack.";
    }
}