using System.Text;

public class LegionsOfTheDead : ZombieCard
{
    public LegionsOfTheDead()
    {
        Name = "Legions of the Dead";
        Phase = PlayDuringPhase.EndOfTurn;
        OriginalText = "Play this Card at the end of the Zombie Turn to immediately place D6 Zombies from your Zombie " +
                       "Pool, each in a different space with at least one other existing Zombie.";
        FlavorText = "With each victim, their numbers grow.";
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesInPool() > 6;
    }

    public override void CardSetup()
    {
        NewZombies = Actions.RollD6();
        StringBuilder cardText = new StringBuilder("Place D6 Zombies from your Zombie Pool, each in a different space " +
                                                   "with at least one other existing Zombie.");
        cardText.Replace("D6", NewZombies.ToString());
        OriginalText = cardText.ToString();
    }

    public override void Effect()
    {
        for(int i = 0; i < NewZombies; i++)
        {
            ZombieTurnManager.SpawnAZombie();
        }
        base.Effect();
    }
}