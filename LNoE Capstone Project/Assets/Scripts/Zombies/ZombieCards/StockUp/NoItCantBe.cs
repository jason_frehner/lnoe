using System.Text;

public class NoItCantBe : ZombieCard
{
    public NoItCantBe()
    {
        Name = "No...It can't be!";
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Sacrifice: Remove 4 of your Zombies from anywhere on the board." +
                       "Draw a Hero from the Unused Hero Character Sheet stack and place its model in a Random Building. " +
                       "You now control that character as a Zombie Hero.";
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesOnBoardCount() > 8;
    }

    public override void CardSetup()
    {
        HeroToEffect = ZombieTurnManager.GetGameStats().GetHeroCharacters().GetNameOfRandomHeroFromPool();
        Building = ZombieTurnManager.GetGameStats().GetMapTiles().GetRandomBuildingInGameTiles();
		
        StringBuilder cardText = new StringBuilder("Remove 4 Zombies from the board.\n" +
                                                   "HERO comes into the game as a Zombie Hero, place anywhere in the BUILDING");

        cardText.Replace("HERO", HeroToEffect);
        cardText.Replace("BUILDING", Building.GetName());
		
        OriginalText = cardText.ToString();
    }

    public override void Effect()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesOnBoard -= 4;
        ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool += 4;
		
        Hero zombieHero = ZombieTurnManager.GetGameStats().GetHeroCharacters().GetSelectionHeroByName(HeroToEffect);
        ZombieTurnManager.GetGameStats().GetZombieTypes().NewZombieHero(zombieHero.Name, zombieHero.Health);
        ZombieTurnManager.GetGameStats().GetHeroCharacters().RemoveHeroFromHeroPool(HeroToEffect);
		
        ZombieTurnManager.UpdateUIText();
        base.Effect();
    }
}