using System.Text;

public class DanceOfTheDead : ZombieCard
{
    public DanceOfTheDead()
    {
        Name = "Dance of the Dead";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Roll a D6 and remove that many Zombies from anywhere on the board (return them to the Zombie " +
                       "Pool, they do not count as having been Killed). Every other Zombie may immediately move one " +
                       "space. Those Zombies may move and fight normally this turn.";
    }

    public override void CardSetup()
    {
        StringBuilder cardText = new StringBuilder("Remove X Zombie(s) from anywhere on the board." +
                                                   "All other Zombies move one space. ");
        NewZombies = Actions.RollD6();
        cardText.Replace("X", NewZombies.ToString());
        OriginalText = cardText.ToString();
    }

    public override void Effect()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool += NewZombies;
        ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesOnBoard -= NewZombies;
        ZombieTurnManager.UpdateUIText();
        base.Effect();
    }
}