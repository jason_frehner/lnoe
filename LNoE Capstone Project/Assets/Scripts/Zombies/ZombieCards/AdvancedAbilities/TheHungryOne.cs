public class TheHungryOne : ZombieCard
{
    public TheHungryOne()
    {
        Name = "The Hungry One";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "At the start of the Move Zombies Phase, choose 1 Zombie to move D3 spaces this turn instead " +
                       "of its normal move.";
        RemainsInPlay = true;
    }

    public override void Effect()
    {
        ZombieTurnManager.SetTheHungryOne(true);
        base.Effect();
    }

    public override void RemainsInPlayReverse()
    {
        ZombieTurnManager.SetTheHungryOne(false);
        base.RemainsInPlayReverse();
    }
}