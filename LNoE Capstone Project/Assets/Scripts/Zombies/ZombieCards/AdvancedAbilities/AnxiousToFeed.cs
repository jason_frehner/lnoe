public class AnxiousToFeed : ZombieCard
{
    public AnxiousToFeed()
    {
        Name = "Anxious to Feed";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Any time a Zombie is Spawned at a Spawning Pit, it may immediately move 1 space.";
        RemainsInPlay = true;
    }

    public override void Effect()
    {
        ZombieTurnManager.SetZombieMovesAfterSpawn(true);
        base.Effect();
    }

    public override void RemainsInPlayReverse()
    {
        ZombieTurnManager.SetZombieMovesAfterSpawn(false);
        base.RemainsInPlayReverse();
    }
}