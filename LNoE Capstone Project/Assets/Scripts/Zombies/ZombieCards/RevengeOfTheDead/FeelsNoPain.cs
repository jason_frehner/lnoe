using System.Collections.Generic;

public class FeelsNoPain : ZombieCard
{
    public FeelsNoPain()
    {
        Name = "Feels no Pain";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.FightAfterRoll;
        OriginalText = "Fight: Play this card to let a Zombie Re-roll any number of its Fight Dice." +
                       "or" +
                       "Play to prevent a Zombie from being 'instantly' Killed.";
        RemainsInPlay = true;
    }

    public override bool Playable()
    {
        return ZombieTurnManager.HighestFightRollValue() < 6;
    }

    public override void CardSetup()
    {
        OriginalText = "Zombie will Re-roll its Fight dice.";
    }

    public override void Effect()
    {
        List<int> rollValues = ZombieTurnManager.ZombieFightRollValues();
        for(int i = 0; i < rollValues.Count; i++)
        {
            if(rollValues[i] < 6)
            {
                ZombieTurnManager.ReRollFightDie(i);
            }
        }
        base.Effect();
    }
}