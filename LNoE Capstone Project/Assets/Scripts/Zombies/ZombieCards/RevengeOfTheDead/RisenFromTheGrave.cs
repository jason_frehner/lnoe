public class RisenFromTheGrave : ZombieCard
{
    public RisenFromTheGrave()
    {
        Name = "Risen From The Grave";
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Sacrifice: Remove 2 of your Zombies from anywhere on the board." +
                       "Choose any one card from the Zombie Cards discard pile and take it into hand.";
        FlavorText = "... there shall be a resurrection of the dead, both of the just and the unjust.";
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetDecks().NumberOfDiscardedRemainsInPlayCards() > 1 && 
               ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesOnBoardCount() > 4;
    }

    public override void CardSetup()
    {
        OriginalText = "Remove 2 Zombies from anywhere on the board. " +
                       "Zombie returns a discarded card to it's hand.";
    }

    public override void Effect()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesOnBoard -= 2;
        ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool += 2;
		
        ZombieTurnManager.UpdateUIText();
		
        ZombieTurnManager.GetDecks().ReturnRandomDiscardedRemainsInPlayCardToHand();
		
        base.Effect();
    }
}