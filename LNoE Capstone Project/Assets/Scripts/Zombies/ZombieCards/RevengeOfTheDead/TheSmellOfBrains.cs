public class TheSmellOfBrains : ZombieCard
{
    public TheSmellOfBrains()
    {
        Name = "The Smell of Brains";
        Phase = PlayDuringPhase.PreFight;
        OriginalText = "Fight: Play this card to let a Zombie roll an extra Fight Dice. If a Hero takes a Wound as a " +
                       "result of this Fight, immediately move all other Zombies within 2 spaces into this space " +
                       "(ignoring Zombie Hunger).";
        FlavorText = "It's the smell... it's driving them CRAZY!";
    }

    public override void CardSetup()
    {
        OriginalText = "Zombie rolls an extra Fight Dice.\n" +
                       "If a Hero takes a Wound as a result of this Fight, immediately move all other Zombies within " +
                       "2 spaces into this space.";
    }

    public override void Effect()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().SetTempFightDice(1);
        base.Effect();
    }
}