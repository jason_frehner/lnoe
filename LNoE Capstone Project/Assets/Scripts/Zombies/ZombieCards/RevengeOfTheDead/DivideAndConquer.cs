public class DivideAndConquer : ZombieCard
{
    public DivideAndConquer()
    {
        Name = "Divide and Conquer";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play this card at the start of the Zombie Turn. While in play, Zombies roll an extra Fight Dice " +
                       "when fighting any Hero that has no other Heroes on the same board section.";
        RemainsInPlay = true;
    }
}