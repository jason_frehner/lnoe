public enum PlayDuringPhase {StartOfTurn, Immediately, Move, PreFight, FightAfterRoll, EndOfTurn, KilledByGun, 
    HeroWounded, HeroDied}