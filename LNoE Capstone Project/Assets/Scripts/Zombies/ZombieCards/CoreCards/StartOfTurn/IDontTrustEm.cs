/// <inheritdoc />
/// <summary>
/// Heroes can't trade items for a turn.
/// </summary>
public class IDontTrustEm : ZombieCard
{
    public IDontTrustEm()
    {
        Name = "I Don't Trust 'em";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play this card at the start of the Zombie Turn:" +
                       "Until the start of the next Zombie Turn, Heroes may not trade items. Discard this card." +
                       "or" +
                       "Choose a Hero with keyword Strange. THat Hero may not exchange items with any other Hero.";
        RemainsInPlay = true;

    }

    public override void CardSetup()
    {
        // Check for strange hero
        HeroToEffect = null;
        HeroToEffect = ZombieTurnManager.GetGameStats().GetHeroCharacters().RandomHeroWithKeyword(Hero.KeywordType.Strange);
        if (HeroToEffect != null)
        {
            OriginalText = HeroToEffect + " may not exchange items with any other Hero. Remains in play.";
        }
        else
        {
            RemainsInPlay = false;
            OriginalText = "Until the start of the next Zombie Turn, Heroes may not trade items.";
        }
    }
}