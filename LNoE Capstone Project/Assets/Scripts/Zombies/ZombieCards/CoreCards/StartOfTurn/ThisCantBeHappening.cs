/// <inheritdoc />
/// <summary>
/// While hero is alone they roll less fight dice.
/// </summary>
public class ThisCantBeHappening : ZombieCard
{
    public ThisCantBeHappening()
    {
        Name = "This Can't\nBe Happening!";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play this card at the start of a Zombie turn on any Hero. The Hero rolls one less Fight Dice " +
                       "than normal. Discard if the Hero starts their turn in the same space as another Hero.";
        FlavorText = "Just leave me along!";
        RemainsInPlay = true;
    }

    public override void CardSetup()
    {
        string pickedHero = ZombieTurnManager.GetGameStats().GetHeroCharacters().RandomHero();
        OriginalText = pickedHero + " rolls one less Fight Dice than normal. Remains in play until Hero starts their " +
                       "turn in the same space as another Hero. If " + pickedHero + " is already in a space with a here " +
                       "cancel this card.";
    }
}