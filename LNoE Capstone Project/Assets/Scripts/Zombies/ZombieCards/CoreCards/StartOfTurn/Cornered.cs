/// <inheritdoc />
/// <summary>
/// Until the next Zombie turn, Zombie get 2 extra fight dice but Heroes win on ties.
/// </summary>
public class Cornered : ZombieCard
{
    public Cornered()
    {
        Name = "Cornered";
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play this card at the start of a Zombie Turn. " +
                       "Until the start of the next Zombie Turn, Zombies roll 2 extra fight dice but Heroes win on ties.";
        FlavorText = "Good Lord!";
    }

    public override void CardSetup()
    {
        OriginalText = "Until the start of the next Zombie Turn, Zombies roll 2 extra fight dice but Heroes win on ties.";
    }

    /// <inheritdoc />
    /// <summary>
    /// Increase the amount of fight dice.
    /// Makes zombies lose on tie.
    /// </summary>
    public override void Effect()
    {
        foreach (Zombie zombie in ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieTypes())
        {
            zombie.FightDice += 2;
        }
        ZombieTurnManager.SetZombieWinOnTie (false);

        base.Effect();
    }
}