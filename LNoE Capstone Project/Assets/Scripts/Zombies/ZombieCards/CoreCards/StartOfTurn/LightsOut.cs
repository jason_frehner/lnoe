/// <inheritdoc />
/// <summary>
/// Play on any building to place a Lights Out marker on it.
/// </summary>
public class LightsOut : ZombieCard
{
    public LightsOut()
    {
        Name = "Lights Out";
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play this card at the start of a Zombie Turn on any building to place a Lights Out marker on " +
                       "it. The Zombies have cut the power. Any Hero moving into a space within this building " +
                       "immediately ends their turn.";
        RemainsInPlay = true;
    }

    public override void CardSetup()
    {
        Building = ZombieTurnManager.GetGameStats().GetMapTiles().GetRandomBuildingInGameTiles();

        while (Building.HasLightsOut)
            Building = ZombieTurnManager.GetGameStats().GetMapTiles().GetRandomBuildingInGameTiles();


        RemainsInPlayName = "Lights Out in\n" + Building.GetName ();
        OriginalText = "The Zombies have cut the power to the " + Building.GetName () + ". \n" +
                       "Any Hero moving into a space within this building immediately ends their turn.";
    }

    public override void Effect()
    {
        Building.HasLightsOut = true;

        base.Effect();
    }

    public override void RemainsInPlayReverse()
    {
        Building.HasLightsOut = false;
        base.RemainsInPlayReverse();
    }
}