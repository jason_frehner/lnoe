/// <inheritdoc />
/// <summary>
/// Place a zombie next to a Hero.
/// </summary>
public class MyGodHesAZombie : ZombieCard
{
    public MyGodHesAZombie()
    {
        Name = "My God, He's A Zombie";
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play to cancel any Townsfolk Event card on the roll of 4+ (and remove it from the game)." +
                       "or" +
                       "Play at the start of a Zombie Turn to place a Zombie from your Zombie Pool in any space " +
                       "adjacent to a Hero.";
    }
	
    public override bool Playable()
    {
        // Check if zombies in pool
        return ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesInPool() > 0;
    }

    public override void CardSetup()
    {
        string pickedHero = ZombieTurnManager.GetGameStats().GetHeroCharacters().RandomHero();
        ZombieTurnManager.SpawnAZombie();
        OriginalText = "Place a Zombie from the Zombie pool in any space adjacent to " + pickedHero + ".";
    }
}