/// <inheritdoc />
/// <summary>
/// While Hero is with another Hero they roll less fight dice.
/// </summary>
public class TeenAngst : ZombieCard
{
    public TeenAngst()
    {
        Name = "Teen Angst";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play at the start of a Zombie Turn on any Hero with Keyword Student. That Hero rolls one less " +
                       "Fight Dice than normal as long as they are in a space with another Hero.";
        FlavorText = "Just leave me along!";
        RemainsInPlay = true;
    }

    public override bool Playable()
    {
        // Find student hero
        HeroToEffect = null;
        HeroToEffect = ZombieTurnManager.GetGameStats().GetHeroCharacters()
            .RandomHeroWithKeyword(Hero.KeywordType.Student);
        return HeroToEffect != null;
    }

    public override void CardSetup()
    {
        OriginalText = HeroToEffect + " rolls one less Fight Dice than normal as long as they are in a space with " +
                       "another Hero. Remains in play.";
    }
}