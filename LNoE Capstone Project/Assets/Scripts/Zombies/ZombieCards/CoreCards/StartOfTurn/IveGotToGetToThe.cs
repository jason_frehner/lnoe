/// <inheritdoc />
/// <summary>
/// Hero can't search until they move into a building.
/// </summary>
public class IveGotToGetToThe : ZombieCard
{
    public IveGotToGetToThe()
    {
        Name = "I've Got to\nGet to the...";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play this card at the start of a Zombie Turn on any Hero and roll a Random Building." +
                       "That Hero may not Search. Discard this card when the Hero moves into a space of that building." +
                       "If the Hero is already in the building or cannot enter it, Re-roll";
        RemainsInPlay = true;
    }

    public override void CardSetup()
    {
        // Get a hero and building
        string pickedHero = ZombieTurnManager.GetGameStats().GetHeroCharacters().RandomHero();
        Building pickedBuilding = ZombieTurnManager.GetGameStats().GetMapTiles().GetRandomBuildingInGameTiles();
		
        // Make sure building isn't taken over
        while (pickedBuilding.IsTakenOver)
        {
            pickedBuilding = ZombieTurnManager.GetGameStats().GetMapTiles().GetRandomBuildingInGameTiles();
        }

        OriginalText = pickedHero + " may not search. Remains in play until " + pickedHero + " moves into a space of " +
                       pickedBuilding.GetName() + ". If " + pickedHero + " is already in " + pickedBuilding.GetName() + 
                       " cancel this card.";
    }
}