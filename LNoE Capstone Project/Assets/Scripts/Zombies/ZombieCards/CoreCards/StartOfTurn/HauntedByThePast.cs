/// <inheritdoc />
/// <summary>
/// A Hero gains "Strange" and cant have townsfolk events played on them.
/// </summary>
public class HauntedByThePast : ZombieCard
{
    public HauntedByThePast()
    {
        Name = "Haunted By the Past";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play this card at the start of a Zombie Turn on any Hero that does not have Keyword Student. " +
                       "That Hero gains the Keyword Strange and may not have any Townsfolk Events played on them (or " +
                       "their Fight Dice.)";
        RemainsInPlay = true;
    }

    public override bool Playable()
    {
        // Check if there is a non student hero.
        HeroToEffect = null;
        HeroToEffect = ZombieTurnManager.GetGameStats().GetHeroCharacters()
            .RandomHeroWithoutKeyword(Hero.KeywordType.Student);
        return HeroToEffect != null;
    }

    public override void CardSetup()
    {
        OriginalText = HeroToEffect + " gains the Keyword Strange and may not have any Townsfolk Events played on " +
                       "them (or their Fight Dice). Remains in play.";
    }
}