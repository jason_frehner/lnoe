/// <inheritdoc />
/// <summary>
/// Move D6 amount of Zombies.
/// </summary>
public class RelentlessAdvance : ZombieCard
{
    public RelentlessAdvance()
    {
        Name = "Relentless Advance";
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play at the start of a Zombie Turn. " +
                       "Roll a D6. " +
                       "Immediately move that many Zombies one space. " +
                       "Those Zombies may move and fight normally this turn.";
        FlavorText = "How do ya stop these things?!";
    }

    public override void CardSetup()
    {
        //TODO Choose from list of targets US#129
        int x = Actions.RollD6();
        if (x > ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesOnBoardCount())
        {
            x = ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesOnBoardCount();
        }
        OriginalText = "Move " + x + " Zombie(s) that are not next to a Hero one space towards closest Hero.";
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesOnBoardCount() > 0;
    }
}