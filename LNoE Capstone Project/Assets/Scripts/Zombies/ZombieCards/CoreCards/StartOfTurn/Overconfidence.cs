/// <inheritdoc />
/// <summary>
/// Hero can't move from zombie until they lose a fight.
/// </summary>
public class Overconfidence : ZombieCard
{
    public Overconfidence()
    {
        Name = "Overconfidence";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play this card at the start of a Zombie Turn on any Hero. While the Hero is in a space with a " +
                       "Zombie, they may not move away (they may still Search instead of moving)." +
                       "If the Hero loses a fight, discard this card on the roll of 4+";
        RemainsInPlay = true;
    }

    public override void CardSetup()
    {
        string pickedHero = ZombieTurnManager.GetGameStats().GetHeroCharacters().RandomHero();
        OriginalText = "While " + pickedHero + " is in a space with a Zombie, they may not move away (they may still " +
                       "Search instead of Moving). Remains in play until, if " + pickedHero + " loses a fight remove " +
                       "this effect on a roll of 4+.";
    }
}