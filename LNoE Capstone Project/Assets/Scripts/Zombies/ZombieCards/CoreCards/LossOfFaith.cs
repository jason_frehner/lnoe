/// <summary>
/// Cancel faith or force re-roll of weapon break check.
/// </summary>
/// Can not use.
public class LossOfFaith : ZombieCard
{
    public LossOfFaith()
    {
        Name = "Loss of Faith";
        //Phase = null;
        OriginalText = "Play to cancel a Faith card." +
                       "or" +
                       "Play to force a Hero to Re-roll their dice when checking to see if a Hand Weapon Breaks after use.";
        FlavorText = "Why God...Why?";
    }
}