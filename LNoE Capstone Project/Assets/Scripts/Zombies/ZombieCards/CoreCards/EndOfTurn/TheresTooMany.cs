using UnityEngine;

/// <inheritdoc />
/// <summary>
/// Spawn D6 new Zombies.
/// </summary>
public class TheresTooMany : ZombieCard
{
    public TheresTooMany()
    {
        Name = "There's Too Many!";
        Phase = PlayDuringPhase.EndOfTurn;
        OriginalText = "Play this card at the end of the Zombie Turn to spawn D6 new Zombies. " +
                       "You may remove them from anywhere on the board if there are not enough out of play.";
        FlavorText = "We'll never make it through!";
    }

    public override void CardSetup()
    {
        CardRollValue = Actions.RollD6 ();

        NewZombies = Mathf.Min (CardRollValue, ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesInPool());

        if (NewZombies == 0) 
        {
            ZombieTurnManager.Card.GetComponent<Animator>().SetTrigger ("close");
            ZombieTurnManager.GetDecks().DiscardACard(ZombieTurnManager.GetCardBeingPlayed());
        }

        OriginalText = "Place " + NewZombies + " new Zombie(s) spread evenly across the Zombie Spawn Pits.";

        if(ZombieTurnManager.IsZombieMovesAfterSpawn())
        {
            OriginalText += "Move spawned Zombie(s) one space"; //Anxious to feed effect
        }
    }

    public override void Effect()
    {
        for (int i = 0; i < NewZombies; i++) 
        {
            ZombieTurnManager.SpawnAZombie ();
        }
        base.Effect();
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesInPool() > 0;
    }
}