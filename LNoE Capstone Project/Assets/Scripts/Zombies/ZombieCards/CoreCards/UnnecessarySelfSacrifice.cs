/// <summary>
/// In place of a fight, one of the Heroes in the same space can take a wound.
/// </summary>
/// Don't know if there is more than one hero in the space.
public class UnnecessarySelfSacrifice : ZombieCard
{
    public UnnecessarySelfSacrifice()
    {
        Name = "Unnecessary Self Sacrifice";
        Phase = PlayDuringPhase.PreFight;
        OriginalText = "Play this card when two Heroes are in the same space and a Zombie is about to fight one of them. " +
                       "Instead of fighting, the Zombie automatically wounds one of them (Hero's choice).";
        FlavorText = "Stand back, I'll hold them off!";
    }
}