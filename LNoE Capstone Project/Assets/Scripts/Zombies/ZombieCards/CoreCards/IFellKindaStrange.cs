/// <summary>
/// On a wounded Hero, next time wounded they turn into Zombie Hero.
/// </summary>
/// Not used, not tracking when Hero is wounded.
public class IFellKindaStrange : ZombieCard
{
    public IFellKindaStrange()
    {
        Name = "I Fell Kinda Strange";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.HeroWounded;
        OriginalText = "Play this card on a Hero when they take a wound from a Zombie. The next time the Hero would be " +
                       "wounded, instead they are turned into a Zombie Hero.";
        FlavorText = "...like I'm hungry...for...braaaiinnsss.";
        RemainsInPlay = true;
    }
	
    // TODO PHASE DOES NOT EXIST YET US#137
	
}