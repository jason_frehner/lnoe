/// <inheritdoc />
/// <summary>
/// Force Hero to re-roll their fight dice.
/// </summary>
public class UndeadHateTheLiving : ZombieCard
{
    public UndeadHateTheLiving()
    {
        Name = "Undead Hate\nThe Living";
        Phase = PlayDuringPhase.FightAfterRoll;
        OriginalText = "Play this card to hate a Hero, forcing them to Re-roll any number of their Fight Dice (Zombies choice).";
        FlavorText = "Nummm...Numm...";
    }
	
    public override void CardSetup()
    {
        int highNumber = ZombieTurnManager.HighestFightRollValue();

        OriginalText = "Re-roll any Fight Dice higher than " + (highNumber - 1);
    }

    public override bool Playable()
    {
        int zombieHighRoll = ZombieTurnManager.HighestFightRollValue();
        return zombieHighRoll > 3;
    }
}