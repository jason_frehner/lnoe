using System.Collections.Generic;
using UnityEngine;

/// <inheritdoc />
/// <summary>
/// Adds one to the Zombie fight roll.
/// </summary>
public class Braains : ZombieCard
{
    public Braains()
    {
        Name = "Braains!";
        Phase = PlayDuringPhase.FightAfterRoll;
        OriginalText = "Play this card to add +1 to a Zombie's highest Fight Dice roll.";
        FlavorText = "Tasty braaains!";
    }

    public override void CardSetup()
    {
        OriginalText = "Zombie's highest roll gets +1";
    }

    public override void Effect()
    {
        List<int> fightRoll = ZombieTurnManager.ZombieFightRollValues();
        if(IsDebugLog())
        {
            Debug.Log(fightRoll);
        }
        int highNumber = ZombieTurnManager.HighestFightRollValue();
        List<int> newRoll = fightRoll;
        for(int i = 0; i < newRoll.Count; i++)
        {
            if(newRoll[i] != highNumber) continue;
            newRoll[i]++;
            break;
        }

        if(IsDebugLog())
        {
            Debug.Log(newRoll);
        }
        ZombieTurnManager.UpdateZombieFightRollvalues(newRoll); 
        base.Effect();
    }

    public override bool Playable()
    {
        int zombieHighRoll = ZombieTurnManager.HighestFightRollValue();
        return zombieHighRoll > 3 && zombieHighRoll < 6;
    }
}