/// <inheritdoc />
/// <summary>
/// Add 2 extra Zombie fight dice for this fight.
/// </summary>
public class Uuuurrrggghh : ZombieCard
{
    public Uuuurrrggghh()
    {
        Name = "Uuuurrrggghh!";
        Phase = PlayDuringPhase.PreFight;
        OriginalText = "Play this card to let a Zombie roll 2 extra Fight Dice.";
        FlavorText = "Gurgle, gurgle! " +
                     "That's not Tom anymore, it's not even human!";
    }

    public override void CardSetup()
    {
        OriginalText = "Zombie rolls 2 extra Fight Dice for one Fight.";
    }

    public override void Effect()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().SetTempFightDice(2);
        base.Effect();
    }

}