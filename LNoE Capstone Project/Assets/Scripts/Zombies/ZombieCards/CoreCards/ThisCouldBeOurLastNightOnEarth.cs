/// <summary>
/// Male and Female Heroes in same space lose turn.
/// </summary>
/// Don't know where heroes are.
public class ThisCouldBeOurLastNightOnEarth : ZombieCard
{
    public ThisCouldBeOurLastNightOnEarth()
    {
        Name = "This Could Be Our last Night On Earth";
        Phase = PlayDuringPhase.EndOfTurn;
        OriginalText = "Play this card at the start of the Hero Turn. Choose any Male and Female pair of characters in " +
                       "the same space. Both characters lose their Hero Turn (may not do anything at all - they do not " +
                       "need to fight Zombies there).";
        FlavorText = "Gimme some sugar, baby.";
    }
}