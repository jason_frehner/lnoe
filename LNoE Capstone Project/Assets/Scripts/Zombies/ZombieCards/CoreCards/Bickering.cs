/// <summary>
/// Heroes in same space lose their turn.
/// </summary>
/// Don't know when more than one hero is on a space
public class Bickering : ZombieCard
{
    public Bickering()
    {
        Name = "Bickering";
        Phase = PlayDuringPhase.EndOfTurn;
        OriginalText = "Play this card at the start of the Hero Turn on any space with more than one Hero in it. All " +
                       "characters in that space lose their Hero Turn (may not do anything at all - they do not need " +
                       "to fight Zombies there).";
        FlavorText = "Who put YOU in charge?";
    }
}