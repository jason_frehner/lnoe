/// <inheritdoc />
/// <summary>
/// Discard a hero card each turn.
/// </summary>
public class LivingNightmare : ZombieCard
{
    // TODO heroes lose
    public LivingNightmare()
    {
        Name = "Living Nightmare";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "At the start of each Zombie Turn (including this one), discard the top card from the Hero deck." +
                       "If this discards the last Hero Card, the Heroes automatically lose.\n" +
                       "Zombie Heroes roll 2 extra Fight Dice";
        RemainsInPlay = true;
    }
	
    public override bool Playable()
    {
        // Check for zombie hero in play
        return ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Hero) != null;
    }

    // TODO remind about discarding a card US#135
    public override void Effect()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Hero).BonusDice += 2;
        base.Effect();
    }

    public override void RemainsInPlayReverse()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Hero).BonusDice -= 2;
        base.RemainsInPlayReverse();
    }
}