/// <inheritdoc />
/// <summary>
/// Place a zombie from the pool with a Hero in a building.
/// </summary>
public class SurpriseAttack : ZombieCard
{
    public SurpriseAttack()
    {
        Name = "Surprise Attack";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Take a Zombie from your Zombie Pool and place it in the same space as any Hero within a building. " +
                       "If there are no Zombies in the pool or Heroes in buildings, discard this card with no effect.";
    }

    public override void CardSetup()
    {
        OriginalText = "Place a Zombie from the Zombie Pool on the same space as any Hero within a building.\n" +
                       "If there are no Heroes in buildings, place Zombie on a Spawning Pit.";
    }

    public override void Effect()
    {
        ZombieTurnManager.SpawnAZombie ();
        base.Effect();
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool > 0;
    }
}