/// <inheritdoc />
/// <summary>
/// Remove Zombies so that others cannot be killed.
/// </summary>
public class RottenBodies : ZombieCard
{
    public RottenBodies()
    {
        Name = "Rotten Bodies";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Roll a D6 and remove that many Zombies from anywhere on the board as they fall apart (return " +
                       "them to the Zombie Pool, they do not count as having been Killed). Until the start of the next " +
                       "Zombie Turn, no Zombie may be killed in any way.";
    }

    public override void CardSetup()
    {
        Zombie normalZombie = ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal);
        CardRollValue = Actions.RollD6();
        if (normalZombie.ZombiesOnBoard < CardRollValue)
        {
            CardRollValue = normalZombie.ZombiesOnBoard;
        }
        OriginalText = "Remove " + CardRollValue + " zombies from the board that are furthest away from the Heroes. Until the " +
                       "start of the next Zombie Turn, no Zombie may be killed in any way.";
		
    }

    public override void Effect()
    {
        Zombie normalZombie = ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal);
        normalZombie.ZombiesOnBoard -= CardRollValue;
        normalZombie.ZombiesInPool += CardRollValue;
        ZombieTurnManager.GetGameStats().GetZombieTypes().IsZombiesCanBeKilled = false;
        ZombieTurnManager.UpdateUIText();
        base.Effect();
    }
}