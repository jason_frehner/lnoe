using System.Linq;

/// <inheritdoc />
/// <summary>
/// Place a Zombie Spawning pit in a random building.
/// </summary>
public class NewSpawningPit : ZombieCard
{
    public NewSpawningPit()
    {
        Name = "New Spawning Pit";
        Phase = PlayDuringPhase.Immediately;
        OriginalText =  "Roll a Random Building. " +
                        "The Zombie Player places a new Zombie Spawning Pit marker in any space of the building.";
    }

    public override void CardSetup()
    {
        Building = ZombieTurnManager.GetGameStats ().GetMapTiles().GetRandomBuildingInGameTiles ();

        while(Building.HasSpawningPit)
            Building = ZombieTurnManager.GetGameStats ().GetMapTiles().GetRandomBuildingInGameTiles ();

        OriginalText = "Place a Zombie Spawning Pit marker in any space of the " + Building.GetName () + ".";

    }

    public override void Effect()
    {
        Building.HasSpawningPit = true;
        base.Effect();
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetGameStats().GetMapTiles().GetFourRandomTilesForGame().Any(
            t => t.GetBuildingsOnTile().Any(
                b => !b.HasSpawningPit));
    }
}