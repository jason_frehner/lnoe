/// <inheritdoc />
/// <summary>
/// Heroes subtract 1 from movement when starting outside.
/// </summary>
public class HeavyRain : ZombieCard
{
    public HeavyRain()
    {
        Name = "Heavy Rain";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "All Heroes who start their move outside of a building must subtract 1 from their Movement " +
                       "Dice roll (to a minimum of 1)";
        RemainsInPlay = true;
    }

    public override void Effect()
    {
        ZombieTurnManager.Rain.Play();
        base.Effect();
    }

    public override string Reminder()
    {
        return "Heavy Rain: Heroes that start outside subtract 1 from their Movement.";
    }

    public override void RemainsInPlayReverse()
    {
        ZombieTurnManager.Rain.Stop();
        base.RemainsInPlayReverse();
    }
}