using UnityEngine;

/// <inheritdoc />
/// <summary>
/// Take Over a random building and place zombies on it.
/// </summary>
public class MyGodTheyveTakenThe : ZombieCard
{
    public MyGodTheyveTakenThe()
    {
        Name = "My God,\nThey've Taken the...";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Roll a Random Building and place a Taken Over marker on it. " +
                       "Place a Zombie from the Zombie Pool in each empty space of this building. " +
                       "No Hero may enter this building or Search here. " +
                       "If the building has already been Taken Over, Re-roll. " +
                       "Zombie(s) placed here may not move this turn.";
        RemainsInPlay = true;
    }

    public override void CardSetup()
    {
        Building = ZombieTurnManager.GetGameStats().GetMapTiles().GetRandomBuildingInGameTiles();

        while (Building.IsTakenOver)
            Building = ZombieTurnManager.GetGameStats().GetMapTiles().GetRandomBuildingInGameTiles();

        ZombieTurnManager.SetTakenOverBuilding(Building.GetName());
		
        NewZombies = Mathf.Min(Building.GetHowManySpaces(),
            ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesInPool());

        RemainsInPlayName = "My God, They've Taken\nthe " + Building.GetName () + "!";
        OriginalText = "Place a Taken Over marker on " + Building.GetName () +
                       " and put " + NewZombies + " Zombie(s) in it. " +
                       "No Hero may enter this building or Search here. ";
    }

    public override void Effect()
    {
        Building.IsTakenOver = true;

        for (int i = 0; i < NewZombies; i++) {
            ZombieTurnManager.SpawnAZombie ();
        }
        base.Effect();
    }

    public override void RemainsInPlayReverse()
    {
        Building.IsTakenOver = false;
        base.RemainsInPlayReverse();
    }
}