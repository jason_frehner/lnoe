/// <inheritdoc />
/// <summary>
/// Each side gets to return discarded cards to the draw deck.
/// </summary>
public class NightThatNeverEnds : ZombieCard
{
    public NightThatNeverEnds()
    {
        Name = "Night That Never Ends";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "The Zombie Player chooses a card title from any card in the Zombie discard pile, then shuffles " +
                       "all copies of that card from the from the discard pile back into the deck. The Hero Players" +
                       "may now do the same for the Hero discard pile.";
    }

    public override bool Playable()
    {
        // Check if cards are in the discard pile
        int discardCount = ZombieTurnManager.GetDecks().NumberOfDiscardedCards();
        return discardCount > 0;
    }

    public override void CardSetup()
    {
        Name = "Night That\nNever Ends";
        OriginalText = "Zombies are returning discarded cards back to the draw deck. Heroes as a group may choose a " +
                       "card title from any card in the Hero discard pile, then shuffle all copies of that card from " +
                       "the discard pile back into the Hero deck.";
    }

    public override void Effect()
    {
        ZombieTurnManager.GetDecks().ReturnDiscardedCardsBackToDeck();
        base.Effect();
    }
}