/// <inheritdoc />
/// <summary>
/// Lock a door.
/// </summary>
public class LockedDoor : ZombieCard
{
    public LockedDoor()
    {
        Name = "Locked Door";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Play this card when a Hero tries to move through a door space. The door is locked and the " +
                       "Hero's move ends in the space before the door.";
        FlavorText = "...Damn!";
        RemainsInPlay = true;
    }

    // TODO select a door in high priority building. US#129
    // TODO show locked door token US#142
    public override void CardSetup()
    {
        Building = ZombieTurnManager.GetGameStats().GetMapTiles().GetRandomBuildingInGameTiles();
        while (Building.HowManyUnlockedDoors() == 0)
        {
            Building = ZombieTurnManager.GetGameStats().GetMapTiles().GetRandomBuildingInGameTiles();
        }
        OriginalText = "A door at the " + Building.GetName() + " is now locked. Heroes can not move through that door. " +
                       "Remains in play.";
    }

    public override void Effect()
    {
        Building.LockADoor();
        base.Effect();
    }

    public override void RemainsInPlayReverse()
    {
        Building.UnlockADoor();
        base.RemainsInPlayReverse();
    }
}