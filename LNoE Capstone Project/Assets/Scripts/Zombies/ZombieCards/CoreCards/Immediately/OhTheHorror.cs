/// <inheritdoc />
/// <summary>
/// Zombie draws more cards.
/// </summary>
public class OhTheHorror : ZombieCard
{
    public OhTheHorror()
    {
        Name = "Oh the Horror!";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Draw three Zombie Cards. " +
                       "At the start of you next turn, if you have more cards than your hand size will allow, " +
                       "discard down to you normal limit";
    }

    public override void CardSetup()
    {
        OriginalText = "Zombies are gathering more resources.\n(Drawing more cards.)";
    }

    public override void Effect()
    {
        for (int i = 0; i < 3; i++) {
            ZombieTurnManager.GetDecks ().DrawCard ();
        }
        base.Effect();
    }
}