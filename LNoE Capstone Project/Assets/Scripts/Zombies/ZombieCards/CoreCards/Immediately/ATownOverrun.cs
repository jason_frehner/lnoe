/// <inheritdoc />
/// <summary>
/// Discard the top 10 Hero cards.
/// </summary>
public class ATownOverrun : ZombieCard
{
    public ATownOverrun()
    {
        Name = "A Town Overrun";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Immediately reveal and discard the top 10 Hero Cards. " +
                       "If this discards the last Hero Card, the Heroes automatically lose.";
        FlavorText = "What do we do now?";
    }
}