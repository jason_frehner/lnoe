/// <inheritdoc />
/// <summary>
/// Remove discarded Hero cards from the game.
/// </summary>
public class TheresNoTimeLeaveIt : ZombieCard
{
    public TheresNoTimeLeaveIt()
    {
        Name = "There's No Time,\nLeave It!";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "The Zombie player may choose any card from the Hero Cards discard pile and remove it from the game.";
        FlavorText = "Forget about it!";
    }

    public override void CardSetup()
    {
        OriginalText = "Randomly select a card from the Hero discard pile. Remove it and any other copies of it in the " +
                       "discard pile from the game.";
    }
}