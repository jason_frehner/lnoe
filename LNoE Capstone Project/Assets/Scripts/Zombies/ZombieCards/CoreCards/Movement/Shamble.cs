using UnityEngine;

/// <inheritdoc />
/// <summary>
/// Replace a Zombie's move with a D6 move.
/// </summary>
public class Shamble : ZombieCard
{
    public Shamble()
    {
        Name = "Shamble";
        Phase = PlayDuringPhase.Move;
        OriginalText = "Play this card to move a Zombie D6 spaces instead of its normal move.";
        FlavorText = "Look out!";
    }
    //TODO target priority? US#133
    public override void CardSetup()
    {
        int moveRoll = Actions.RollD6 () - 1;
        OriginalText = "Move the closest Zombie that is at least 2 spaces away from a Hero up to " + moveRoll + 
                       " space(s) towards the Hero.";
        if(IsDebugLog())
        {
            Debug.Log("<color=cyan>Shamble Roll: " + moveRoll + "</color>");
        }
        if (moveRoll >= 1) return;
        SkipCard = true;
        ZombieTurnManager.GetDecks ().DiscardACard (ZombieTurnManager.GetCardBeingPlayed ());
        ZombieTurnManager.Phases.TurnPhaseDelegate();
    }
}