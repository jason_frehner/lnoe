/// <inheritdoc />
/// <summary>
/// If killed by gun, Hero discards gun.
/// </summary>
/// Can not use, yet.
public class Resilient : ZombieCard
{
    public Resilient()
    {
        Name = "Resilient";
        Phase = PlayDuringPhase.KilledByGun;
        OriginalText = "Play this card when a Zombie is Killed by a Gun item to immediately discard that gun.";
        FlavorText = "Why...won't...you...die?!?";
    }

    public override void CardSetup()
    {
        OriginalText = "Discard the gun used to kill the zombie.";
    }
}