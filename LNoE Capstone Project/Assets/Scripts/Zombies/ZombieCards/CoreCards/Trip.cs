/// <summary>
/// Force Hero to re-roll their movement.
/// </summary>
/// Can not use
public class Trip : ZombieCard
{
    public Trip()
    {
        Name = "Trip";
        //Phase = null;
        OriginalText = "Play this card on any Hero to force them to Re-roll their Movement Dice roll.";
        FlavorText = "Aaaaaaaaahh...Oof!";
    }
}