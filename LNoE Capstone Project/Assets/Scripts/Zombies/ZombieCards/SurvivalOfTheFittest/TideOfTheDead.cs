using System.Text;

public class TideOfTheDead : ZombieCard
{
    public TideOfTheDead()
    {
        Name = "Tide of the Dead";
        Phase = PlayDuringPhase.Move;
        OriginalText = "Sacrifice: The Zombie player(s) must discard a total of 2 cards from hand." +
                       "All Zombies may move 2 spaces this turn (Zombie Heroes may move D6 spaces) instead of their " +
                       "normal move.";
        FlavorText = "We're gonna need more ammo!";
    }

    public override void CardSetup()
    {
        StringBuilder cardText = new StringBuilder();
        cardText.Append("Zombies discard two cards. All Zombies move 1 space.");
		
        if(ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Hero) != null)
        {
            int heroMovement = Actions.RollD3();
            cardText.Append("\nZombie Heroes move XX space.");
            cardText.Replace("XX", heroMovement.ToString());
            if(heroMovement != 1)
            {
                cardText.Replace("space", "spaces");
            }
        }
        OriginalText = cardText.ToString();
    }

    public override void Effect()
    {
        ZombieTurnManager.GetDecks().DiscardACard(ZombieTurnManager.GetDecks().GetHand()[0]);
        ZombieTurnManager.GetDecks().DiscardACard(ZombieTurnManager.GetDecks().GetHand()[0]);
        base.Effect();
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetDecks().GetHand().Count > 2;
    }
}