public class Hopeless : ZombieCard
{
    public Hopeless()
    {
        Name = "Hopeless";
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Sacrifice: Choose and discard any Zombie card that currently Remains in Play." +
                       "Choose any Hero on the board. that Hero must immediately discard an Item, Take a Wound, or Lose" +
                       "their next Hero turn (Hero's Choice)";
        FlavorText = "Nooooo... Let go!";
    }

    public override void CardSetup()
    {
        HeroToEffect = ZombieTurnManager.GetGameStats().GetHeroCharacters().RandomHero();
        CardToEffect = ZombieTurnManager.GetDecks().RandomCardFromRemainsDeck();
        OriginalText = CardToEffect.GetName() + " gets removed from play.\n" +
                       HeroToEffect + " must immediately choose to discard an Item, take a Wound or lose their next " +
                       "Hero turn.";
    }

    public override void Effect()
    {
        ZombieTurnManager.GetDecks().RemoveFromRemainsInPlay(CardToEffect);
        ZombieTurnManager.RemoveFromRemainsInPlayPanel(CardToEffect.GetName());
        base.Effect();
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetDecks().RemainsInPlayDeckSize() > 0;
    }
}