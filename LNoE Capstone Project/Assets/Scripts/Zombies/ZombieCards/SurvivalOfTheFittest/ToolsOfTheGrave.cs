public class ToolsOfTheGrave : ZombieCard
{
    public ToolsOfTheGrave()
    {
        Name = "Tools of the Grave";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Choose any board section and roll a D6 for each Zombie there that does not have a Grave Weapon." +
                       "On the roll of 4, 5, 6, immediately draw a Grave Weapon for that Zombie." +
                       "If Grave weapons are not being used all Zombies on the entire board roll an extra Fight Dice " +
                       "until the end of the turn instead.";
    }

    public override void CardSetup()
    {
        OriginalText = "Zombies roll an extra Fight Dice for the rest of this turn.";
    }

    public override void Effect()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().AddExtraRoundDice();
        base.Effect();
    }
}