public class DeadlySurprise : ZombieCard
{
    public DeadlySurprise()
    {
        Name = "Deadly Surprise";
        Phase = PlayDuringPhase.PreFight;
        OriginalText = "Play this card at the start of Fight to immediately draw a Grave Weapon for the Zombie." +
                       "or" +
                       "Play when a Zombie wins a Fight to cancel any card that Remains in Play on the D6 foll of 4, " +
                       "5, or 6.";
    }
}