public class FightForSurvival : ZombieCard
{
    public FightForSurvival()
    {
        Name = "Fight for Survival";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "The Zombie Player chooses a card title from any card in the Hero Cards discard pile, then " +
                       "removes all copies of that card in the discard pile from the game (you may not choose a " +
                       "Scenario Search Item). The Hero Players may now do the same for the Zombie Cards discard pile.";
    }

    public override void CardSetup()
    {
        OriginalText = "Randomly select a card from the Hero discard pile. Remove it and any other copies of it in the " +
                       "discard pile from the game. Zombies will do the same.";
    }

    public override void Effect()
    {
        ZombieTurnManager.GetDecks().RemoveRandomDiscardedCardsFromGame();
        base.Effect();
    }
}