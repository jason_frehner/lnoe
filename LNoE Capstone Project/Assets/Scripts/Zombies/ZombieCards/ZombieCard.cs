﻿using UnityEngine;

//enum for what phase a card can be used.

/// <summary>
/// Base Zombie Card class.
/// </summary>
public class ZombieCard
{
	protected string Name;
	protected string RemainsInPlayName;
	protected PlayDuringPhase Phase;
	protected string OriginalText;
	protected string FlavorText;
	protected bool RemainsInPlay;
	private readonly bool tempCard;
	protected bool SkipCard;

	protected ZombieTurn ZombieTurnManager = GameObject.Find ("ZombieManager").GetComponent<ZombieTurn> ();
	protected Building Building;
	protected string HeroToEffect;
	protected int NewZombies;
	protected int CardRollValue;
	protected ZombieCard CardToEffect;

	protected static bool IsDebugLog()
	{
		return PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
	}

	// ReSharper disable once UnusedMember.Local
	private void Awake()
	{
		ZombieTurnManager = GameObject.Find ("ZombieManager").GetComponent<ZombieTurn> ();
	}
	
	protected ZombieCard()
	{}

	public ZombieCard(string cardName, string cardText, string flavor, bool temp = false)
	{
		Name = cardName;
		OriginalText = cardText;
		FlavorText = flavor;
		tempCard = temp;
	}
	
    public bool Equals(ZombieCard other)
	{
		return Name == other.Name;
	}
    
	public string GetName()
	{
		return Name;
	}
	
	public string GetRemainsInPlayName()
	{
		return RemainsInPlayName;
	}
	
	public PlayDuringPhase GetPhase()
	{
		return Phase;
	}

	public Building GetBuilding()
	{
		return Building;
	}
	
	public string GetCardText()
	{
		return OriginalText;
	}
	
	public string GetFlavorText()
	{
		return FlavorText;
	}

	public void SetRemainsInPlayDetailsFromAutoSave(string remainsName, string cardText)
	{
		RemainsInPlayName = remainsName;
		OriginalText = cardText;
	}
	
	public bool IsRemainsInPlay()
	{
		return RemainsInPlay;
	}

	public bool IsTemp()
	{
		return tempCard;
	}

    public bool IsActionSkipped()
	{
		return SkipCard;
	}

	/// <summary>
	/// Make change to the card before using.
	/// </summary>
	public virtual void CardSetup()
	{}

	public virtual void Effect()
	{	
		if (IsTemp()) return;
		if (RemainsInPlay)
			ZombieTurnManager.GetDecks().PutInToRemainsInPlay(this);
		else
			ZombieTurnManager.GetDecks().DiscardACard(this);
	}

	public virtual bool Playable()
	{
		return true;
	}

	/// <summary>
	/// Reminder for Heroes turn.
	/// </summary>
	/// <returns>Text to display.</returns>
	public virtual string Reminder()
	{
		return null;
	}

	/// <summary>
	/// Used to reverses the remains in play effect if the card gets canceled.
	/// </summary>
	public virtual void RemainsInPlayReverse()
	{
		ZombieTurnManager.GetDecks().RemoveFromRemainsInPlay(this);
	}
}
