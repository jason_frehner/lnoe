using UnityEngine;

/// <summary>
/// A Hero draws a card and new Zombies spawn.
/// </summary>
public class ZombieSurge : ZombieCard
{
    // Similar to Theres Too Many
    public ZombieSurge()
    {
        Name = "Zombie Surge";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Sacrifice: Choose any Hero on the board to immediately draw a Hero Card.\n" +
                       "Immediately spawn D6 Zombies. Those Zombies may move and Fight normally this turn.";
        FlavorText = "We're note going to make it, are we";
    }

    public override void CardSetup()
    {
        CardRollValue = Actions.RollD6();

        NewZombies = Mathf.Min(CardRollValue, ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesInPool());

        if (NewZombies == 0)
        {
            ZombieTurnManager.Card.GetComponent<Animator>().SetTrigger("close");
            ZombieTurnManager.GetDecks().DiscardACard(ZombieTurnManager.GetCardBeingPlayed());
        }

        HeroToEffect = ZombieTurnManager.GetGameStats().GetHeroCharacters().RandomHero();

        OriginalText = HeroToEffect + " draws a Hero card.\n" +
                       "Place " + NewZombies + " new Zombies(s) spread evenly across the Zombie Spawn Pits.";

        if(ZombieTurnManager.IsZombieMovesAfterSpawn())
        {
            OriginalText += "Move spawned Zombie(s) one space"; //Anxious to feed effect
        }
    }

    public override void Effect()
    {
        for (int i = 0; i < NewZombies; i++)
        {
            ZombieTurnManager.SpawnAZombie();
        }
        base.Effect();
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesInPool() > 3;
    }
}