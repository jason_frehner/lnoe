/// <summary>
/// When Hero rolls 1 for movement, subtract 1 from future movement rolls.
/// </summary>
public class TwistedAnkle : ZombieCard
{
    public TwistedAnkle()
    {
        Name = "Twisted Ankle";
        RemainsInPlayName = Name;
        OriginalText = "Play this card on any Hero when they roll a 1 for movement.\n" +
                       "That Hero must now subtract 1 from their Movement Dice rolls (minimum of 1).";
        RemainsInPlay = true;
    }
}