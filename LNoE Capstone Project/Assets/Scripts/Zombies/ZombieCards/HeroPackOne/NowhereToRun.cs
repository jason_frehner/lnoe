/// <summary>
/// Discard 10 Hero cards.
/// </summary>
public class NowhereToRun : ZombieCard
{
    public NowhereToRun()
    {
        Name = "Nowhere to Run";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Immediately reveal and discard the top 10 Hero Cards. If the discards the last Hero Card, the " +
                       "Heroes automatically lose.";
        FlavorText = "It's a dead end!";
    }
}