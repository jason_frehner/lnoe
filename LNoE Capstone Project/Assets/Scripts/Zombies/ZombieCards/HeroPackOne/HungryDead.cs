/// <summary>
/// Draw a card if Heroes lose fights.
/// </summary>
/// Don't know if fights where lost or fended off.
public class HungryDead : ZombieCard
{
    public HungryDead()
    {
        Name = "Hungry Dead";
        RemainsInPlayName = Name;
        OriginalText = "Sacrifice: Remove 3 Zombies from anywhere on the board.\n" +
                       "Any time a Hero loses a Fight to a Zombie, that Zombie Player may immediately draw a Zombie Card.";
        RemainsInPlay = true;
    }
}