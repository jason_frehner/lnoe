/// <summary>
/// Cancel Hero event card.
/// </summary>
/// Don't know when Hero events are played.
public class Despair : ZombieCard
{
    public Despair()
    {
        Name = "Despair";
        OriginalText = "Play this card to cancel any Hero Event Card on the roll of 5+.";
        FlavorText = "We're doomed...";
    }
}