/// <summary>
/// Cancel a Heroes search action.
/// </summary>
/// Don't know when Heroes search.
public class TheLineIsDead : ZombieCard
{
    public TheLineIsDead()
    {
        Name = "The Line is Dead";
        OriginalText = "Play this card when a Hero is about to search. Cancel that Search (no card is drawn).";
        FlavorText = "Zombies must have cut the wire...";
    }
}