/// <summary>
/// Two females on the same space lose their turn.
/// </summary>
/// Don't know where players are.
public class Catfight : ZombieCard
{
    public Catfight()
    {
        Name = "Catfight";
        OriginalText = "Play this card at the start of the Hero Turn on any two Female characters in the same space. " +
                       "Both of those characters lose their Hero Turn (may not do anything at all - they do not need to " +
                       "fight Zombies there).";
        FlavorText = "Oh you did NOT just...";
    }
}