/// <inheritdoc />
/// <summary>
/// Move zombies.
/// </summary>
public class DraggingMeat : ZombieCard
{
    public DraggingMeat()
    {
        Name = "Dragging Meat";
        Phase = PlayDuringPhase.Move;
        OriginalText = "Play during the Move Zombies phase to roll a D6. That many of your Zombies may move D3 spaces " +
                       "each this turn instead of the normal one space (or D6 spaces if a Zombie Hero).";
        FlavorText = "What's that sound?!";
    }
	
    public override void CardSetup()
    {
        int zombiesToMove = Actions.RollD6();
        int moveRoll = Actions.RollD3();
        int moveRollHero = Actions.RollD6();
        OriginalText = "Move " + zombiesToMove + " Zombie(s) that are at least 2 spaces away from a Hero up to " + moveRoll + 
                       " space(s) towards that Hero. If a Zombie Hero moves, it moves " + moveRollHero + " spaces instead. " +
                       "These Zombies do not move again this turn.";
    }
}