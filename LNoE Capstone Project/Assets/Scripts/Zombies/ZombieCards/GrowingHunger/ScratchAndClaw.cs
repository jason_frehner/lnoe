/// <inheritdoc />
/// <summary>
/// Zombie gets an extra fight dice.
/// </summary>
public class ScratchAndClaw : ZombieCard
{
    public ScratchAndClaw()
    {
        Name = "Scratch and Claw";
        Phase = PlayDuringPhase.PreFight;
        OriginalText = "Fight: Play this card to let a Zombie roll an extra Fight Dice for each Zombie in its space " +
                       "(including itself).";
        FlavorText = "Yyyyaaaarrrr...";
    }
	
    public override void CardSetup()
    {
        OriginalText = "Zombie rolls an extra Fight Dice for one Fight.";
    }

    public override void Effect()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().SetTempFightDice(1);
        base.Effect();
    }
}