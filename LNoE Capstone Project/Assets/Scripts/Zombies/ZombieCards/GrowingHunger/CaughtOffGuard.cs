/// <inheritdoc />
/// <summary>
/// Hero can't use an item for this fight.
/// </summary>
public class CaughtOffGuard : ZombieCard
{
    public CaughtOffGuard()
    {
        Name = "Caught Off-Guard";
        Phase = PlayDuringPhase.PreFight;
        OriginalText = "Play at the start of a fight, before any dice are rolled. Fight: Choose any item the Hero has. " +
                       "That Hero may not use the chosen item during this fight.";
        FlavorText = "Rrrrraaaawwwww";
    }

    public override void CardSetup()
    {
        OriginalText = "The Hero may not use and item during this fight. If the Hero has more than one item, randomly " +
                       "select one that they can not use.";
    }
}