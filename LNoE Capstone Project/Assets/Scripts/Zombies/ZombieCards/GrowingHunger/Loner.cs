/// <inheritdoc />
/// <summary>
/// Hero can't end movement with another Hero.
/// </summary>
public class Loner : ZombieCard
{
    public Loner()
    {
        Name = "Loner";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play at the start of a Zombie Turn on any Hero. That Hero may not end their Move Action in the " +
                       "same space as another Hero.";
        FlavorText = "Back off! I got it covered.";
        RemainsInPlay = true;
    }
	
    public override void CardSetup()
    {
        string pickedHero = ZombieTurnManager.GetGameStats().GetHeroCharacters().RandomHero();
        OriginalText = pickedHero + " may not end their Move Action in the same space as another Hero. Remains in play.";
    }
}