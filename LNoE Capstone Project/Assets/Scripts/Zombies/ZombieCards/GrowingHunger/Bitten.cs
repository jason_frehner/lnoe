/// <inheritdoc />
/// <summary>
/// When a zombie is killed, the next time the Hero that killed it is wounded is turned into a Zombie Hero.
/// </summary>
public class Bitten : ZombieCard
{
    public Bitten()
    {
        Name = "Bitten";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.FightAfterRoll;
        OriginalText = "Play this card on a Hero when they kill a Zombie in a fight. The next time the Hero would be " +
                       "wounded, instead they are turned into a Zombie Hero.";
        FlavorText = "ouch...dammit!";
        RemainsInPlay = true;
    }
	
    // TODO Player needs to force the effect.
}