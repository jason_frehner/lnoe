/// <inheritdoc />
/// <summary>
/// Zombies can play more than one fright card.
/// </summary>
public class DesperateForFlesh : ZombieCard
{
    public DesperateForFlesh()
    {
        Name = "Desperate for Flesh";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "While the card is in play, Zombies are no longer limited to one Fight: card per combat.";
        FlavorText = "Brrraaaaaaaains.";
        RemainsInPlay = true;
    }

    public override void CardSetup()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().IsMultipleFightCards = true;
    }
	
    public override void RemainsInPlayReverse()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().IsMultipleFightCards = false;
        base.RemainsInPlayReverse();
    }
}