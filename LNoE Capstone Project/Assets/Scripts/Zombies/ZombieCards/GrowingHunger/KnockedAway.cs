/// <summary>
/// In place of being wounded, Hero discards item.
/// </summary>
public class KnockedAway : ZombieCard
{
    public KnockedAway()
    {
        Name = "Knocked Away";
        OriginalText = "If a Hero would be wounded in a fight, you may instead choose one item that the Hero has to " +
                       "discard (except a Scenario Search Item).";
        FlavorText = "You...Dirty Zombie";
    }
    //TODO 
}