/// <inheritdoc />
/// <summary>
/// Zombie gets a weapon for a fight.
/// </summary>
public class FightingInstincts : ZombieCard
{
    public FightingInstincts()
    {
        Name = "Fighting Instincts";
        Phase = PlayDuringPhase.PreFight;
        OriginalText = "Play at the start of the fight, before any Fight Dice are rolled. Fight: Choose any Hand Weapon " +
                       "from the Hero discard pile. The Zombie may use that weapon's Combat Bonus for this one fight " +
                       "(for weapon text, treat the Zombie as the 'Hero' and the target Hero as a 'Zombie').";
    }

    public override void CardSetup()
    {
        OriginalText = "Choose a random Hand Weapon from the Hero discard pile. The Zombie gets that weapon's bonuses for " +
                       "this fight.";
    }
}