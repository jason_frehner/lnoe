using UnityEngine;

/// <inheritdoc />
/// <summary>
/// Place a taken over and new zombie pit token.
/// </summary>
public class TheyreComingFromThe : ZombieCard
{
    public TheyreComingFromThe()
    {
        Name = "They're coming from the...";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Roll a Random Building and place a Taken Over marker on it as well as a New Spawning Pit. No " +
                       "Hero may enter this building or Search here. If the building has already been taken over, re-roll.";
        RemainsInPlay = true;
    }
	
    public override void CardSetup()
    {
        Building = ZombieTurnManager.GetGameStats().GetMapTiles().GetRandomBuildingInGameTiles();

        while (Building.IsTakenOver)
            Building = ZombieTurnManager.GetGameStats().GetMapTiles().GetRandomBuildingInGameTiles();

        ZombieTurnManager.SetTakenOverBuilding(Building.GetName());
		
        NewZombies = Mathf.Min(Building.GetHowManySpaces(),
            ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesInPool());

        RemainsInPlayName = "They're coming from\nthe " + Building.GetName () + "!";
        OriginalText = "Place a Taken Over and a New Spawning Pit markers on " + Building.GetName () +
                       ". No Hero may enter this building or Search here.";
    }

    public override void Effect()
    {
        Building.IsTakenOver = true;
        Building.HasSpawningPit = true;
        base.Effect();
    }

    public override void RemainsInPlayReverse()
    {
        Building.IsTakenOver = false;
        base.RemainsInPlayReverse();
    }
}