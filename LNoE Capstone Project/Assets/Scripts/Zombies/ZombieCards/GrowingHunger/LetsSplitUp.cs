/// <inheritdoc />
/// <summary>
/// Hero takes a wound if they end in the same building as another Hero.
/// </summary>
public class LetsSplitUp : ZombieCard
{
    public LetsSplitUp()
    {
        Name = "Let's Split Up";
        Phase = PlayDuringPhase.EndOfTurn;
        OriginalText = "Play at the start of a Hero Turn. Until the end of the turn, any Hero that ends their Move " +
                       "Action in the same space or building as another Hero immediately takes a wound.";
        FlavorText = "This was NOT a good idea.";
    }

    public override void CardSetup()
    {
        OriginalText = "Until the end of the turn, any Hero that ends their Move Action in the same space or building " +
                       "as another Hero immediately takes a wound.";
    }
}