using UnityEngine;

/// <inheritdoc />
/// <summary>
/// Spawn Zombies.
/// </summary>
public class TheyreEverywhere : ZombieCard
{
    public TheyreEverywhere()
    {
        Name = "They're Everywhere!";
        Phase = PlayDuringPhase.EndOfTurn;
        OriginalText = "Play this card at the end of the Zombie Turn to place one Zombie from your Zombie Pool on each " +
                       "Spawning Pit.";
        FlavorText = "Run!";
    }
	
    public override void CardSetup()
    {
        NewZombies = ZombieTurnManager.GetGameStats().GetMapTiles().HowManyBuildingsHaveSpawningPits();
        NewZombies = Mathf.Min (NewZombies, ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesInPool());

        if (NewZombies == 0) {
            ZombieTurnManager.Card.GetComponent<Animator> ().SetTrigger ("close");
            ZombieTurnManager.GetDecks ().DiscardACard (ZombieTurnManager.GetCardBeingPlayed ());
        }

        OriginalText = "Place " + NewZombies + " new Zombie(s) spread evenly across the Zombie Spawn Pits.";

        if(ZombieTurnManager.IsZombieMovesAfterSpawn())
        {
            OriginalText += "Move spawned Zombie(s) one space"; //Anxious to feed effect
        }
    }

    public override void Effect()
    {
        for (int i = 0; i < NewZombies; i++) {
            ZombieTurnManager.SpawnAZombie ();
        }
        base.Effect();
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesInPool() > 0;
    }
}