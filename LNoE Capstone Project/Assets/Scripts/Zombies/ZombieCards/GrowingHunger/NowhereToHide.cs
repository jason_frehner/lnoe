/// <inheritdoc />
/// <summary>
/// Discard 10 Hero cards.
/// </summary>
public class NowhereToHide : ZombieCard
{
    public NowhereToHide()
    {
        Name = "Nowhere to hide";
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "Immediately reveal and discard the top 10 Hero Cards. If this discards the last Hero Card, the " +
                       "Heroes automatically lose.";
        FlavorText = "Sniff ... Sniff ...";
    }
}