/// <inheritdoc />
/// <summary>
/// If more Zombies than Heroes, no Zombie can die.
/// </summary>
public class Overwhelmed : ZombieCard
{
    public Overwhelmed()
    {
        Name = "Overwhelmed";
        Phase = PlayDuringPhase.PreFight;
        OriginalText = "Play on any space that has at least twice as many Zombies as Heroes. Every Zombie in that space " +
                       "rolls an extra Fight Dice and may not be Killed in any way until the end of the turn.";
        FlavorText = "...and the tide of darkness swept forth, and he stood strong before them!";
    }

    public override void CardSetup()
    {
        OriginalText = "If the space of the zombie you are fighting has at least twice as many Zombies as Heroes, no " +
                       "Zombie on that space may be killed in any way until the end of the turn.";
    }
}