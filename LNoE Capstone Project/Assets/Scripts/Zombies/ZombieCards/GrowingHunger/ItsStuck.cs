/// <summary>
/// Try to discard a Hero weapon after use.
/// </summary>
/// Don't know when Hero uses weapons.
public class ItsStuck : ZombieCard
{
    public ItsStuck()
    {
        Name = "It's Stuck!";
        OriginalText = "Play when a Hero uses a Hand Weapon's Combat Bonus in a fight. Roll a D6. On the roll of 4+, " +
                       "after the Combat Bonus of the weapon is taken into account, the Hero must discard the item.";
        FlavorText = "Leave It!";
    }
}