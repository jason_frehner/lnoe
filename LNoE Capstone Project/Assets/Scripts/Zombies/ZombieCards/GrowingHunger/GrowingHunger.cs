/// <inheritdoc />
/// <summary>
/// Zombies get extra fight dice until Hero is killed.
/// </summary>
public class GrowingHunger : ZombieCard
{
    public GrowingHunger()
    {
        Name = "Growing Hunger";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.Immediately;
        OriginalText = "While this card is in play, all Zombies roll an extra Fight Dice. Discard this card if any Hero " +
                       "is killed (or turned into a Zombie Hero).";
        RemainsInPlay = true;
    }

    public override void Effect ()
    {		
        ZombieTurnManager.GetGameStats().GetZombieTypes().SetBonusDice(1);
        base.Effect();
    }

    public override void RemainsInPlayReverse()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().SetBonusDice(-1);
        base.RemainsInPlayReverse();
    }
}