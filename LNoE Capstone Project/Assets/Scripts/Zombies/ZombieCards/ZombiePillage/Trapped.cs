using System.Text;

public class Trapped : ZombieCard
{
    public Trapped()
    {
        Name = "Trapped!";
        Phase = PlayDuringPhase.EndOfTurn;
        OriginalText = "Play this card at the end of the Zombie Turn to choose a Hero and roll a D6. Immediately " +
                       "place that many Zombies from your Zombie Pool in any spaces adjacent to the chosen Hero " +
                       "(Limit one new Zombie per adjacent space).";
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool > 5;
    }

    public override void CardSetup()
    {
        StringBuilder cardText = new StringBuilder("Place XX zombie(s) in any of the adjacent spaces around HERO, limit " +
                                                   "one new Zombie per space.");
		
        HeroToEffect = ZombieTurnManager.GetGameStats().GetHeroCharacters().RandomHero();
        NewZombies = Actions.RollD6();

        cardText.Replace("XX", NewZombies.ToString());
        cardText.Replace("HERO", HeroToEffect);

        OriginalText = cardText.ToString();
    }

    public override void Effect()
    {
        for(int i = 0; i < NewZombies; i++)
        {
            ZombieTurnManager.SpawnAZombie();
        }
        base.Effect();
    }
}