public class AngryDead : ZombieCard
{
    public AngryDead()
    {
        Name = "Angry Dead";
        RemainsInPlayName = Name;
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "The Zombie Player(s) must discard a total of 2 cards from hand. " +
                       "While this card is in play, any time a Hero loses a fight, reveal and discard the top 3 Hero " +
                       "Cards. If this discards the last Hero Card, the Heroes automatically lose.";
        RemainsInPlay = true;
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetDecks().GetHand().Count > 2;
    }

    public override void CardSetup()
    {
        OriginalText = "While this card is in play, any time a Hero loses a fight, reveal and discard the top 3 Hero " +
                       "Cards. If this discards the last Hero Card, the Heroes automatically lose.";
    }

    public override void Effect()
    {
        base.Effect();
		
        ZombieTurnManager.GetDecks().DiscardACard(ZombieTurnManager.GetDecks().GetHand()[0]);
        ZombieTurnManager.GetDecks().DiscardACard(ZombieTurnManager.GetDecks().GetHand()[0]);
    }
}