public class Hysteria : ZombieCard
{
    public Hysteria()
    {
        Name = "Hysteria";
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play this card at the start of the Zombie Turn on any Hero. While this card is in play, " +
                       "cancel any Hero Event card played on this Hero on the D6 roll of 3+.";
        FlavorText = "Stay back...All of you!";
    }
}