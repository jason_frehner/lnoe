public class FallingDarkness : ZombieCard
{
    public FallingDarkness()
    {
        Name = "Falling Darkness";
        Phase = PlayDuringPhase.StartOfTurn;
        OriginalText = "Play at the start of a Zombie Turn to cancel any Hero Card that is marked Remains in Play on " +
                       "the D6 roll of 4+.";
        FlavorText = "What hope do we have left?";
    }

    public override bool Playable()
    {
        if(ZombieTurnManager.Round >= 10) return false;
        if(Actions.RollD6() > 3)
        {
            return true;
        }
			
        Effect();
        return false;

    }

    public override void CardSetup()
    {
        OriginalText = "Cancel any Hero Card that is marked Remains in Play";
    }
}