public class ClosingIn : ZombieCard
{
    public ClosingIn()
    {
        Name = "Closing In";
        Phase = PlayDuringPhase.Move;
        OriginalText = "Sacrifice: Remove 3 of your Zombies from anywhere on the board." +
                       "Choose a board section. Every Zombie on that board section may move D6 spaces this turn " +
                       "instead of their normal move.";
        FlavorText = "If you have a plan, NOW might be a good time!";
    }

    public override bool Playable()
    {
        return ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombiesOnBoardCount() > 10;
    }

    public override void CardSetup()
    {
        int x = Actions.RollD6();
        OriginalText = "Remove 3 of your Zombies from anywhere on the board.\n" +
                       "Then for the board section that has the most Zombies, every Zombie on that section moves " +
                       x + " spaces.";
    }

    public override void Effect()
    {
        ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesOnBoard -= 3;
        ZombieTurnManager.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool += 3;
        base.Effect();
    }
}