﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Class for a zombie type.
/// </summary>
public class Zombie
{
    private readonly string name;
    private readonly ZombieType type;
    private readonly ZombieSpeed speed;
    // ReSharper disable once NotAccessedField.Local
    private int health;
    private readonly int hungerRange;
    private readonly bool countTowardSpawnCheck;
    private ZombieMovementTarget movementTarget;
    private ZombieTurn zombieTurn;

    private bool ignoreWoundOnFourPlus;
    private bool canSpawnWithOtherZombie;
    private bool makesZombieHeroOnFivePlus;
    
    private int zombiesInPool;
    private int zombiesOnBoard;
    private readonly int baseFightDice;
    private int fightDice;
    private int bonusDice;
//    private int tempFightDice;

    public Zombie(string name, ZombieType type, int fightDice, ZombieSpeed speed, int health, int hungerRange, 
                  bool spawncheck, ZombieMovementTarget target)
    {
        this.name = name;
        this.type = type;
        baseFightDice = fightDice;
        this.fightDice = fightDice;
        this.speed = speed;
        this.health = health;
        this.hungerRange = hungerRange;
        countTowardSpawnCheck = spawncheck;
        movementTarget = target;
    }

    /// <summary>
    /// Get the name of the zombie type;
    /// </summary>
    /// <returns>The name.</returns>
    public string GetZombieName()
    {
        return name;
    }

    /// <summary>
    /// Set if it is the zombie's turn.
    /// </summary>
    public void SetZombieTurn()
    {
        zombieTurn = GameObject.Find("ZombieManager").GetComponent<ZombieTurn>();
    }

    /// <summary>
    /// Gets the type of this zombie.
    /// </summary>
    /// <returns>The type.</returns>
    public ZombieType GetZombieType()
    {
        return type;
    }
    
    /// <summary>
    /// Gets or sets the zombies in pool.
    /// </summary>
    /// <value>The zombies in pool.</value>
    public int ZombiesInPool
    {
        get{
            return zombiesInPool;
        }
        set{
            zombiesInPool = value;
        }
    }

    /// <summary>
    /// Gets or sets the zombies on board.
    /// </summary>
    /// <value>The zombies on board.</value>
    public int ZombiesOnBoard
    {
        get{
            return zombiesOnBoard;
        }
        set{
            zombiesOnBoard = value;
        }
    }
    
    /// <summary>
    /// Gets or sets how many fight dice.
    /// </summary>
    /// <value>How many fight dice.</value>
    public int FightDice
    {
        get{
            return fightDice;
        }
        set{
            fightDice = value;
        }
    }

    /// <summary>
    /// Gets or sets the zombie's ability to ignore wounds on a roll of 4+.
    /// </summary>
    public bool IgnoreWoundOnFourPlus
    {
        get { return ignoreWoundOnFourPlus; }
        set { ignoreWoundOnFourPlus = value; }
    }

    /// <summary>
    /// Gets or sets a value indicating whether this instance can spawn with other zombie.
    /// </summary>
    /// <value>
    ///   <c>true</c> if this instance can spawn with other zombie; otherwise, <c>false</c>.
    /// </value>
    public bool CanSpawnWithOtherZombie
    {
        get { return canSpawnWithOtherZombie; }
        set { canSpawnWithOtherZombie = value; }
    }

    /// <summary>
    /// Gets or sets a value indicating whether [makes zombie hero on five plus].
    /// </summary>
    /// <value>
    ///   <c>true</c> if [makes zombie hero on five plus]; otherwise, <c>false</c>.
    /// </value>
    public bool MakesZombieHeroOnFivePlus
    {
        get { return makesZombieHeroOnFivePlus; }
        set { makesZombieHeroOnFivePlus = value; }
    }

    /// <summary>
    /// Reset the number of fight dice to the zombies base number of fight dice.
    /// </summary>
    public void ResetFightDice()
    {
        fightDice = baseFightDice;
    }
    
   /// <summary>
   /// Gets and set how many bonus dice.
   /// </summary>
    public int BonusDice
    {
        get { return bonusDice; }
        set { bonusDice = value; }
    }

    /// <summary>
    /// Gets or sets how many temp fight dice.
    /// </summary>
    /// <value>How many temp fight dice.</value>
//    public int TempFightDice
//    {
//        get{
//            return tempFightDice;
//        }
//        set{
//            tempFightDice = value;
//        }
//    }

    /// <summary>
    /// Checks if zombies count towards spawn check.
    /// </summary>
    public bool CountTowardSpawnCheck
    {
        get { return countTowardSpawnCheck; }
    }

    /// <summary>
    /// Get the total fight dice needed for a fight.
    /// </summary>
    /// <returns>The number of dice.</returns>
    public int GetTotalFightDice()
    {
        return fightDice + bonusDice;
    }

    /// <summary>
    /// Get the text for the movement target.
    /// </summary>
    /// <returns>The target text.</returns>
    public string GetMovementTarget()
    {
        switch (movementTarget)
        {
            case ZombieMovementTarget.ClosestHero:
                return "the closest Hero";
            case ZombieMovementTarget.ClosestBuilding:
                return "the closest Building";
            case ZombieMovementTarget.Center:
                return "the center tile";
            case ZombieMovementTarget.PickHero:
                string hero = zombieTurn.GetGameStats().GetHeroCharacters().GetPriorityHero();
                return hero;
            case ZombieMovementTarget.PickBuilding:
                string building = zombieTurn.GetGameStats().GetMapTiles().BuildingZombieChoiceAi();
                return "the " + building;
            default:
                return "the closest Hero";
        }
    }

    /// <summary>
    /// Change the zombies movement target.
    /// </summary>
    /// <param name="target">The new target.</param>
    private void SetMovementTarget(ZombieMovementTarget target)
    {
        movementTarget = target;
    }

    /// <summary>
    /// Set the zombie movement target to a random enum option.
    /// </summary>
    public void SetRandomMovementTarget()
    {
        string[] targets = Enum.GetNames(typeof(ZombieMovementTarget));
        int randomIndex = Random.Range(0, targets.Length);
        string randomEnum = Enum.GetName(typeof(ZombieMovementTarget), randomIndex);
        
        if(randomEnum == null) return;
        
        ZombieMovementTarget newTarget = (ZombieMovementTarget) Enum.Parse(typeof(ZombieMovementTarget), randomEnum);
        SetMovementTarget(newTarget);
    }

    /// <summary>
    /// Get the text for the movement speed.
    /// </summary>
    /// <returns>The speed text.</returns>
    public string GetMovementSpeed()
    {
        switch (speed)
        {
            case ZombieSpeed.None:
                return null;
            case ZombieSpeed.One:
                return "1 space";
            case ZombieSpeed.D3:
                int numOfSpaces = Actions.RollD3();
                if (numOfSpaces > 1)
                    return numOfSpaces + " spaces";
                return "1 space";
            case ZombieSpeed.Two:
                return "2 spaces";
            default:
                return null;
        }
    }

    /// <summary>
    /// Get the text for the hunger movement.
    /// </summary>
    /// <returns>The hunger distance text.</returns>
    public string GetMovementHunger()
    {
        if (hungerRange > 1)
            return hungerRange + "spaces";
        return "1 space";
    }
}