﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;
using Random = UnityEngine.Random;

/// <inheritdoc />
/// <summary>
/// Zombie turn.
/// </summary>
public class ZombieTurn : MonoBehaviour
{
	private readonly List<string> setupState = new List<string>();
	
	#region private vars
	
	private ZombieDeck decks = new ZombieDeck();
	private GameSession gameStats = new GameSession();
	private bool spawn;
	private bool zombiesWinOnTie = true;
	private bool zombieMovesAfterSpawn;
	private bool theHungryOne;
	private bool fightCardUsed;
	private int round = -10;
	private int heroesDiedCount;
	private ZombieCard cardBeingPlayed;
	private GameData gameData;
	private List<GameObject> gameObjectives = new List<GameObject>();
	private string currentSetupState;
	private List<string> takenOverBuilding = new List<string>();
	private string zombieToFight;
	private int numberOfHeroesInGame = 4;
	private List<string> cardsPlayed = new List<string>();

	public readonly ZombieTurnPhases Phases = new ZombieTurnPhases();

	#endregion

	#region Reference to other game objects

	[Header("Scene objects to control.")]
	public GameObject SetupPanel;
	public Text SetupText;
	public GameObject MainUI;
	[Space(10)]
	public GameObject Card;
	public Text CardTitle;
	public Text CardText;
	public Text CardFlavor;
	[Space(10)] 
	public Text ZombiesOnBoardText;
	public Text ScenarioName;
	[Space(10)]
	public GameObject MovePanel;
	public Text MoveText;
	public Text MoveZombieTypeText;
	[Space(10)]
	public GameObject FightPanel;
	public Text FightText;
	public GameObject FightRollResultParent;
	public GameObject FightRollResultPrefab;
	[Space(10)]
	public GameObject SpawnPanel;
	public Text SpawnText;
	[Space(10)]
	public GameObject RemainsInPlay;
	public GameObject RemainsInPlayElement;
	[Space(10)]
	public GameObject ResultPanel;
	public Text ResultText;
	public Text ResultNarrativeText;
	[Space(10)]
	public GameObject DeadHeroPanel;
	public GameObject DeadZombiePanel;
	[Space(10)]
	public TextAnimation TextAnimation;
	[Space(10)]
	public GameLog GameLog;
	[Space(10)]
	public PlaceMapImages MapImages;
	[Space(10)]
	public GameObject ZombieObjectivePanel;
	public GameObject HeroObjectivePanel;
	public GameObject ObjectivePrefab;
	[Space(10)]
	public GameObject NarrativePanel;
	public Text NarrativeTitle;
	public Text NarrativeText;
	[Space(10)] 
	public GameObject DestroyBuildingPanel;
	public Slider DestroyBuildingZombieSlider;
	public Dropdown DestroyBuildingDropdown;
	public Text DestroyBuildingDeadZombiesText;
	public GameObject ZombiePitPanel;
	[Space(10)]
	public GameObject TutorialPanel;
	public Text TutorialText;
	[Space(10)]
	public Dropdown DebugCardList;
	[Space(10)]
	public GameObject HeroTurnReminderPanel;
	public Text HeroTurnReminderText;
	public RainCameraController Rain;

	private Material damageMaterial;
	
	private AudioSource clickAudio;
	private AudioSource audioZombieDead;
	private AudioSource audioZombieEvades;
	private AudioSource audioHeroDead;

	private bool isDebugLog;
	
	[Space(10)]
	public ViewableUI ViewableUI;

	#endregion

	#region public methods

	/// <summary>
	/// Gets the decks.
	/// </summary>
	/// <returns>The decks.</returns>
	public ZombieDeck GetDecks()
	{
		return decks;
	}

	/// <summary>
	/// Gets the game stats.
	/// </summary>
	/// <returns>The game stats.</returns>
	public GameSession GetGameStats()
	{
		return gameStats;
	}

	/// <summary>
	/// Sets the zombie win on tie.
	/// </summary>
	/// <param name="value">If set to <card>true</card> value.</param>
	public void SetZombieWinOnTie(bool value)
	{
		zombiesWinOnTie = value;
	}

	/// <summary>
	/// Gets if a zombie wins on tie or not.
	/// </summary>
	/// <returns>The value.</returns>
	public bool IsZombiesWinOnTie()
	{
		return zombiesWinOnTie;
	}

	public void SetZombieMovesAfterSpawn(bool value)
	{
		zombieMovesAfterSpawn = value;
	}
	
	public bool IsZombieMovesAfterSpawn()
	{
		return zombieMovesAfterSpawn;
	}

	public void SetTheHungryOne(bool value)
	{
		theHungryOne = value;
	}

	/// <summary>
	/// Gets the card being played.
	/// </summary>
	/// <returns>The card being played.</returns>
	public ZombieCard GetCardBeingPlayed()
	{
		return cardBeingPlayed;
	}

	/// <summary>
	/// Set the taken over building to be used in movement phase.
	/// </summary>
	/// <param name="building">The building</param>
	public void SetTakenOverBuilding(string building)
	{
		takenOverBuilding.Add(building);
	}

	public int Round
	{
		get { return round; }
		set { round = value; }
	}

	public List<string> CardsPlayed
	{
		get { return cardsPlayed; }
	}

	public List<ZombieCard> CardsToPlay { get; set; }

	public ZombieCard CardBeingPlayed
	{
		get { return cardBeingPlayed; }
		set { cardBeingPlayed = value; }
	}

	public bool Spawn
	{
		get { return spawn; }
		set { spawn = value; }
	}

	#endregion

	private void Awake()
	{
		isDebugLog = PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
	}

	/// <summary>
	/// Start the game.
	/// </summary>
	public void Start()
	{
		ViewableUI = gameObject.GetComponent<ViewableUI>();
		RemainsInPlayElement = Resources.Load<GameObject>("RemainsInPlayElement");
		
		Phases.SetZombieTurn(this);

		damageMaterial = Resources.Load<Material>(Strings.Materials.Splatter);
		if(damageMaterial != null)
		{
			damageMaterial.SetFloat(Strings.MaterialProperties.Step, 0);
		}
		
		// Move camera
		Camera.main.orthographicSize = 6.5f;
		GameObject cameraSetupPositionObject = GameObject.Find(Strings.GameObjectNames.CameraSetupPosition);
		if(cameraSetupPositionObject != null)
		{
			Vector3 cameraPosition = cameraSetupPositionObject.GetComponent<RectTransform>().TransformPoint(Vector3.zero);
			if(isDebugLog)
			{
				Debug.Log("Board area position: " + cameraPosition);
			}

			cameraPosition.z = 1.0f;
			GameObject.Find("MapPositions").transform.position = cameraPosition;
		}

		// Get audio sources
		if(GameObject.Find("SFX") != null)
		{
			clickAudio = GameObject.Find(Strings.GameObjectNames.AudioClick).GetComponent<AudioSource>();
			audioZombieDead = GameObject.Find("ZombieDeath").GetComponent<AudioSource>();
			audioZombieEvades = GameObject.Find("ZombieGroan").GetComponent<AudioSource>();
			audioHeroDead = GameObject.Find("HeroDeath").GetComponent<AudioSource>();
		}

		if(ObjectivePrefab == null)
		{
			ObjectivePrefab = Resources.Load<GameObject>("Objective");
		}

		if(isDebugLog)
		{
			Debug.Log("<color=yellow>Setting up the game.</color>");
		}
		
		setupState.AddRange(new []{"map", "zombie", "hero", "scenario", "special", "done"});
		currentSetupState = setupState[0];
		
		// Setup mission / tiles / heroes
		gameStats.SetupSession();
		ScenarioName.text = gameStats.GetCurrentMission().Name;
		
		// Get the session map tiles.
		MapImages.SetMap(gameStats.GetMapTiles().GetStringArrayOfTileNames(gameStats.GetCurrentMission().CenterTile));

		// Create the game objectives objects.
		foreach(Mission.ObjectiveTypes objective in gameStats.GetCurrentMission().HeroObjective)
		{
			CreateObjectiveUI(objective, true);
		}
		foreach(Mission.ObjectiveTypes objective in gameStats.GetCurrentMission().ZombieObjective)
		{
			CreateObjectiveUI(objective, false);
		}

		decks.AddCardsToDeck();

		gameData = new GameData(SystemInfo.deviceModel + "-" + DateTime.Now.ToString(CultureInfo.CurrentCulture),
								gameStats.GetCurrentMission().Name, gameStats.GetMapTiles().GetListOfBuildingsInThisGameSession());

		Analytics.CustomEvent("gameStarated", new Dictionary<string, object>
		{
			{"newGame", gameStats.GetCurrentMission().Name},
			{"mapTile1", gameStats.GetMapTiles().GetFourRandomTilesForGame()[0].GetName()},
			{"maptile2", gameStats.GetMapTiles().GetFourRandomTilesForGame()[1].GetName()},
			{"maptile3", gameStats.GetMapTiles().GetFourRandomTilesForGame()[2].GetName()},
			{"mapTile4", gameStats.GetMapTiles().GetFourRandomTilesForGame()[3].GetName()},
			{"hero1", gameStats.GetHeroCharacters().GameHeroes[0].Name},
			{"hero2", gameStats.GetHeroCharacters().GameHeroes[1].Name},
			{"hero3", gameStats.GetHeroCharacters().GameHeroes[2].Name},
			{"hero4", gameStats.GetHeroCharacters().GameHeroes[3].Name}
		});
		
		Phases.TurnPhaseDelegate = GameSetup;
		
		if(PlayerPrefs.GetString(Strings.PlayerPrefKeys.Scenario) == "Tutorial")
		{
			TutorialText.text = Tutorial.Intro;
			TutorialPanel.GetComponent<Animator>().SetTrigger("open");
		}
		
		// Add list of cards to debug dropdown.
		List<ZombieCard> uniquecards = decks.GetUniqueCards();
		DebugCardList.options.Add(new Dropdown.OptionData("None"));
		foreach(ZombieCard card in uniquecards)
		{
			DebugCardList.options.Add(new Dropdown.OptionData(card.GetName()));
		}

		// Load saved game or start new one.
		if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.AutoSave) == 1)
		{
			LoadSaveGame();
			currentSetupState = setupState.Last();
			ContinueSetup(true);
			UpdateUIText();
		}
		else
		{
			Phases.TurnPhaseDelegate();
		}
	}

	/// <summary>
	/// Display the setup of the game text.
	/// </summary>
	private void GameSetup()
	{
		string setupInfoText = "<size=16><b>" + gameStats.GetCurrentMission().Name + "</b></size>\n\n";

		// Map info
		if(currentSetupState == setupState[0])
		{
			setupInfoText += "Use map Tiles:\n\n" +
			                 gameStats.GetMapTiles().GetFourRandomTilesForGame()[0].GetName() + ";\n\n" +
			                 gameStats.GetMapTiles().GetFourRandomTilesForGame()[1].GetName() + ";\n\n" +
			                 gameStats.GetMapTiles().GetFourRandomTilesForGame()[2].GetName() + ";\n\n" +
			                 gameStats.GetMapTiles().GetFourRandomTilesForGame()[3].GetName();
		}
		// Starting zombie info
		else if(currentSetupState == setupState[1])
		{
			int[] spawnRoll1 = SpawnZombies();
			int[] spawnRoll2 = SpawnZombies();
			int normalZombiesSpawnCount = spawnRoll1[0] + spawnRoll2[0];
			int graveDeadZombiesSpawnCount = spawnRoll1[1] + spawnRoll2[1];
			setupInfoText += "Place " + normalZombiesSpawnCount + " Zombies evenly across all Spawning Pits.";
			if(graveDeadZombiesSpawnCount > 0)
			{
				setupInfoText += "Place " + graveDeadZombiesSpawnCount + " Grave Dead Zombies evenly across all Spawning Pits.";
			}

			if(PlayerPrefs.GetString(Strings.PlayerPrefKeys.Scenario) == "Tutorial")
			{
				TutorialText.text = Tutorial.Setup;
				TutorialPanel.GetComponent<Animator>().SetTrigger("open");
			}
		}
		// Hero info
		else if(currentSetupState == setupState[2])
		{
			setupInfoText += "Use the following Heroes:\n\n" +
			                 gameStats.GetHeroCharacters().GameHeroes[0].Name + " starts in the " +
			                 gameStats.GetHeroCharacters().GameHeroes[0].StartLocation + ".\n" +
			                 gameStats.GetHeroCharacters().GameHeroes[1].Name + " starts in the " +
			                 gameStats.GetHeroCharacters().GameHeroes[1].StartLocation + ".\n" +
			                 gameStats.GetHeroCharacters().GameHeroes[2].Name + " starts in the " +
			                 gameStats.GetHeroCharacters().GameHeroes[2].StartLocation + ".\n" +
			                 gameStats.GetHeroCharacters().GameHeroes[3].Name + " starts in the " +
			                 gameStats.GetHeroCharacters().GameHeroes[3].StartLocation + ".";
		}
		// Scenario info
		else if(currentSetupState == setupState[3])
		{
			setupInfoText += gameStats.GetCurrentMission().Text + "\n\n";
		}

		else if(currentSetupState == setupState[4])
		{
			if(gameStats.GetCurrentMission().SpecialRules.Count > 0)
			{
				setupInfoText += "<b>Special Rules</b>\n";
				foreach(Mission.SpecialRulesTypes specialRule in gameStats.GetCurrentMission().SpecialRules)
				{
					setupInfoText += specialRule.SetupText + "\n\n";
				}
			}

			if(gameStats.GetCurrentMission().ScenarioSearchItems.Count > 0)
			{
				setupInfoText += "<b>Scenario Search Items</b>\n";
				foreach(Mission.ScenarioSearchItemTypes searchItem in gameStats.GetCurrentMission().ScenarioSearchItems)
				{
					setupInfoText += searchItem + "\n";
				}
			}
		}

		SetupText.text = setupInfoText;

		currentSetupState = setupState[setupState.IndexOf(currentSetupState) + 1];
	}

    #region Cards

    /// <summary>
    /// Starts the play card method.
    /// </summary>
    public void StartPlayCard()
	{
		StartCoroutine(PlayCard());
	}
	
	/// <summary>
	/// Play the card.
	/// </summary>
	private IEnumerator PlayCard()
	{
		Animator cardAnimator = Card.GetComponent<Animator>();
		while(!cardAnimator.GetCurrentAnimatorStateInfo(0).IsName("CardUIOut"))
		{
			yield return null;
		}

		if(isDebugLog)
		{
			Debug.Log("Playing Card: " + cardBeingPlayed.GetName());
		}
		cardsPlayed.Add(cardBeingPlayed.GetName());
		cardsPlayed.Add("");
		
		cardBeingPlayed.CardSetup();

		CardTitle.text = cardBeingPlayed.GetName();
		CardText.text = cardBeingPlayed.GetCardText();
		CardFlavor.text = cardBeingPlayed.GetFlavorText();

		if(!cardBeingPlayed.IsActionSkipped())
		{
			Card.GetComponent<Animator>().SetTrigger("open");
		}
	}

	/// <summary>
	/// Adds to the remains in play log.
	/// </summary>
	/// <param name="card">Card.</param>
	private void AddToRemainsInPlayPanel(ZombieCard card)
	{
		if(RemainsInPlayElement != null)
		{
			// Instantiate and set text
			GameObject element = Instantiate(RemainsInPlayElement);
			element.transform.SetParent(RemainsInPlay.transform);
			element.transform.Find("Info/TextTitle").GetComponent<Text>().text = card.GetRemainsInPlayName().Replace('\n', ' ');
			element.transform.Find("Info/TextEffect").GetComponent<Text>().text = card.GetCardText();
			element.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

			// Set the button reference to the card.
			element.transform.Find("Button").GetComponent<DestroyParent>().Card = card;
		
			// Resize scroll view
			RectTransform scrollRect = RemainsInPlay.transform.parent.GetComponent<RectTransform>();
			scrollRect.sizeDelta = new Vector2(scrollRect.sizeDelta.x, 100 * RemainsInPlay.transform.childCount);
		}
	}

	public void RemoveFromRemainsInPlayPanel(string cardName)
	{
		for(int i = 0; i < RemainsInPlay.transform.childCount; i++)
		{
			Transform child = RemainsInPlay.transform.GetChild(i);
			string childName = child.GetChild(0).GetChild(0).GetComponent<Text>().text;
			if(childName == cardName)
			{
				Destroy(child.gameObject);
			}
		}
	}
	
	#endregion

	#region continue from buttons

	/// <summary>
	/// Continue through setup.
	/// </summary>
	[UsedImplicitly]
	public void ContinueSetup(bool fromLoadedGame)
	{
		if(currentSetupState == setupState.Last())
		{
			// Change UI menus
			SetupPanel.gameObject.SetActive(false);
			MainUI.gameObject.SetActive(true);
			
			// Move camera
			GameObject boardArea = GameObject.Find("CameraPlayPosition");
			Vector3 cameraPosition = boardArea.transform.position;
			Vector2 boardAreaSize = boardArea.GetComponent<RectTransform>().rect.size;
			if(isDebugLog)
			{
				Debug.Log(Screen.height + " " + boardAreaSize);
			}
			if(boardAreaSize.x + 20 < boardAreaSize.y)
			{
				Camera.main.orthographicSize = Math.Abs(boardAreaSize.x) /(Screen.height / 6.5f) + 6.5f;
			}
			else
			{
				Camera.main.orthographicSize = 6.9f;
			}
			cameraPosition.z = 1.0f;
			GameObject.Find("MapPositions").transform.position = cameraPosition;
			
			// Start the game
			if(!fromLoadedGame)
			{
				TextAnimation.Play("Game Begins!");
				if(PlayerPrefs.GetString(Strings.PlayerPrefKeys.GameType) == Strings.PlayerPrefKeys.GameTypeCampaign)
				{
					ShowCampaignNarrative("Start", gameStats.GetCurrentMission().StartNarrativeText[0]);
				}
				Phases.TurnDelegateStartOfTurn();
			}
		}
		
		Phases.TurnPhaseDelegate();
		PlayClickAudio();
	}
	
	/// <summary>
	/// Continues from playing A card.
	/// </summary>
	/// <param name="canceled">If set to <card>true</card> canceled.</param>
	[UsedImplicitly]
	public void ContinueFromPlayingACard(bool canceled)
	{
		if(!canceled)
		{
			cardBeingPlayed.Effect();

			if(cardBeingPlayed.IsRemainsInPlay())
			{
				AddToRemainsInPlayPanel(cardBeingPlayed);
			}
		} 
		else if(!cardBeingPlayed.IsTemp())
		{
			decks.DiscardACard(cardBeingPlayed);
		}
		
		Card.GetComponent<Animator>().SetTrigger("close");
		Phases.TurnPhaseDelegate();
		
		PlayClickAudio();
	}

	/// <summary>
	/// Continues from move info.
	/// </summary>
	[UsedImplicitly]
	public void ContinueFromMoveInfo()
	{
		if(Phases.MoveZombieIndex == 0)
		{
			Phases.TurnPhaseDelegate = Phases.PhaseFightHeroes;
		}
		
		MovePanel.GetComponent<Animator>().SetTrigger("close");
		if(gameStats.GetZombieTypes().IsZombieTurn)
			Phases.TurnPhaseDelegate();
		
		PlayClickAudio();
	}

	/// <summary>
	/// Continues from fight info.
	/// </summary>
	[UsedImplicitly]
	public void ContinueFromFightInfo()
	{
		Phases.TurnPhaseDelegate = Phases.PhaseSpawnNewZombies;
		
		if(gameStats.GetZombieTypes().IsZombieTurn)
			Phases.TurnPhaseDelegate();
		
		ClearFightRoll();
		
		PlayClickAudio();
	}

	/// <summary>
	/// Continues from spawning new zombies
	/// </summary>
	[UsedImplicitly]
	public void ContinueFromSpawningNewZombies()
	{
		if(gameStats.GetZombieTypes().IsZombieTurn)
		{
			Phases.TurnPhaseDelegate();
		}
		PlayClickAudio();
	}

	/// <summary>
	/// Start the next round.
	/// </summary>
	[UsedImplicitly]
	public void StartNewRound()
	{
		AutoSaveGame();
		
		SetObjective("Morning");
		Phases.PhaseStartOfTurn();
		
		PlayClickAudio();
	}

    /// <summary>
    /// Zombie killed button pressed.
    /// </summary>
	[UsedImplicitly]
    public void ZombieKilledButton()
	{
		ZombieDied(null);
	}

    /// <summary>
    /// Zombie shot button pressed.
    /// </summary>
    public void ZombieShotButton()
	{
		ZombieKilledByGun();
	}

	[UsedImplicitly]
	public void DestroyBuildingSliderChange(Slider slider)
	{
		float x = slider.value;
		DestroyBuildingDeadZombiesText.text = x.ToString();
	}

	[UsedImplicitly]
	public void DestroyBuildingSubmit(Slider slider)
	{
		float x = slider.value;
		if(!(x > 0.0f)) return; // Check slider value
		
		string buildingDestroyed = DestroyBuildingDropdown.options[DestroyBuildingDropdown.value].text;
		if(buildingDestroyed != "None") // Check building selected
		{
			Zombie normalZombie = gameStats.GetZombieTypes().GetZombieOfType(ZombieType.Normal);
			normalZombie.ZombiesInPool += Convert.ToInt16(x);
			normalZombie.ZombiesOnBoard -= Convert.ToInt16(x);
	
			foreach(GameObject objective in gameObjectives)
			{
				if(objective.GetComponent<Objective>().Name == "Destroy Buildings")
				{
					objective.GetComponent<Objective>().ButtonUpdateProgress();
					if(objective.GetComponent<Objective>().CurrentProgress > 3)
					{
						ZombiePitPanel.GetComponent<Animator>().SetTrigger("open");
					}
				}
			}

			// Set the building a destroyed.
			gameStats.GetMapTiles().GetBuildingByName(buildingDestroyed).IsDestroyed = true;
		}	
		UpdateUIText();
		GameLog.AddText("<color=red>Building Destroyed.</color>");
	}
	
	#endregion

	#region spawn
	/// <summary>
	/// Spawns the zombies.
	/// </summary>
	/// <returns>The zombies.</returns>
	public int[] SpawnZombies()
	{
		int[] spawnRoll = {Actions.RollD6(), 0}; // Normal, GraveDead

		if(PlayerPrefs.GetInt(Strings.PlayerPrefKeys.GraveDead) == 1 && SpawnSpecialZombieBasedOnDifficulty()) //TODO check mission
		{
			Zombie graveDeadZombie = gameStats.GetZombieTypes().GetZombieOfType(ZombieType.Grave);
			if(spawnRoll[0] > 1 && graveDeadZombie.ZombiesInPool > 0)
			{
				spawnRoll[0] -= 2;
				spawnRoll[1] += 1;
				graveDeadZombie.ZombiesInPool -= 1;
				graveDeadZombie.ZombiesOnBoard += 1;
			}
		}
		
		Zombie normalZombie = gameStats.GetZombieTypes().GetZombieOfType(ZombieType.Normal);
		if(spawnRoll[0] > normalZombie.ZombiesInPool)
			spawnRoll[0] = normalZombie.ZombiesInPool;
		normalZombie.ZombiesInPool -= spawnRoll[0];
		normalZombie.ZombiesOnBoard += spawnRoll[0];

		UpdateUIText();

		if(isDebugLog)
		{
			Debug.Log("Spawning " + spawnRoll + " new Zombies.");
		}
		return spawnRoll;
	}

	/// <summary>
	/// Spawns A zombie.
	/// Used by cards.
	/// </summary>
	public void SpawnAZombie()
	{
		Zombie normalZombie = gameStats.GetZombieTypes().GetZombieOfType(ZombieType.Normal);
		normalZombie.ZombiesInPool --;
		normalZombie.ZombiesOnBoard ++;

		UpdateUIText();
	}
	#endregion

	#region Deaths
	
	/// <summary>
	/// Zombie died.
	/// </summary>
	public void ZombieDied(string killedMethod)
	{
		if(!gameStats.GetZombieTypes().IsZombiesCanBeKilled) return; // TODO let user know zombies can't be killed.
		if(isDebugLog)
		{
			Debug.Log("<color=red>Zombie Died</color>");
		}
		if(gameStats.GetZombieTypes().GetZombieTypes().Count > 1 && DeadZombiePanel != null)
		{
			DeadZombiePanel.GetComponent<ZombieDeadScreen>().SetOptions(gameStats.GetZombieTypes().GetZombieTypes());
			DeadZombiePanel.GetComponent<Animator>().SetTrigger("open");
		}
		else
		{
			if(gameStats.GetZombieTypes().ZombieDied(gameStats.GetZombieTypes().GetZombieTypes()[0].GetZombieName()))
			{
				ZombieDeathUIUpdate();
			}
			else
			{
				ZombieIgnoresDeathUIUpdate();
			}
		}
		
		// Add death to game log
		if(killedMethod != null)
		{
			GameLog.AddText("<color=red>Zombie died from " + killedMethod + ".</color>");
		}
		else
		{
			GameLog.AddText("<color=red>Zombie died.</color>");
		}
		
	}

	/// <summary>
	/// Update the UI and play sound for zombie death.
	/// </summary>
	public void ZombieDeathUIUpdate()
	{
		UpdateUIText();

		if(TextAnimation != null)
		{
			TextAnimation.Play("Zombie Died");
		}

		SetObjective("Kill Zombies");

		if(audioZombieDead != null)
		{
			audioZombieDead.Play();
		}
	}

	/// <summary>
	/// Inform user that zombie ignored the wound.
	/// </summary>
	public void ZombieIgnoresDeathUIUpdate()
	{
		TextAnimation.Play("Zombie Ignores Wound");
		audioZombieEvades.Play();
	}

	/// <summary>
	/// A zombie is killed by a gun.
	/// Check if card should be played.
	/// </summary>
	private void ZombieKilledByGun()
	{
		if(gameStats.GetZombieTypes().IsZombiesCanBeKilled)
		{
			// Play card
			CardsToPlay = decks.CheckIfInHand(PlayDuringPhase.KilledByGun);
			if(CardsToPlay.Count > 0)
			{
				cardBeingPlayed = CardsToPlay[0];
				StartCoroutine(PlayCard());
			}
			ZombieDied("gun");
		}

		if(clickAudio != null)
		{
			clickAudio.Play();
		}
	}

	/// <summary>
	/// Hero died.
	/// </summary>
	public void HeroDied()
	{
		if(isDebugLog)
		{
			Debug.Log("<color=red>Hero Died</color>");
		}
		TextAnimation.Play("Hero Died");
		
		DeadHeroPanel.GetComponent<HeroDeadScreen>().SetOptions(gameStats.GetHeroCharacters().GameHeroes);
		DeadHeroPanel.GetComponent<Animator>().SetTrigger("open");
		
		audioHeroDead.Play();
	}

    /// <summary>
    /// Increases the hero dead count.
    /// </summary>
    public void IncreaseHeroDeadCount()
	{
		heroesDiedCount++;
		damageMaterial.SetFloat(Strings.MaterialProperties.Step, heroesDiedCount);
	}
	
	#endregion

	#region Fight Actions
	
	/// <summary>
	/// Complete the fight action.
	/// </summary>
	[UsedImplicitly]
	public void FightAction(string zombieType)
	{
		fightCardUsed = false;
		Phases.TurnPhaseDelegate = FightRoll;
		zombieToFight = zombieType;
		
		ClearFightRoll();

		// Play a fight card
		CardsToPlay = decks.CheckIfInHand(PlayDuringPhase.PreFight);
		if(CardsToPlay.Count > 0 && !fightCardUsed) {
			if(!PlayCardBasedOnDifficulty())
			{
				if(isDebugLog)
				{
					Debug.Log("<color=orange>Not Playing card " + CardsToPlay[0].GetName() + "</color>");
				}
				cardsPlayed.Add(CardsToPlay[0].GetName());
				cardsPlayed.Add("");
				decks.DoNotPlayCard(CardsToPlay[0]);
			}

			if(isDebugLog)
			{
				Debug.Log("Playing before fight: " + CardsToPlay[0].GetName());
			}
			cardBeingPlayed = CardsToPlay[0];
			StartCoroutine(PlayCard());
			fightCardUsed = true;
		}
		else
			FightRoll();
		
		PlayClickAudio();
	}

	/// <summary>
	/// Clears the fight roll text.
	/// </summary>
	public void ClearFightRoll()
	{
		for(int i = 0; i < FightRollResultParent.transform.childCount; i++)
		{
			if(Application.isPlaying)
			{
				Destroy(FightRollResultParent.transform.GetChild(i).gameObject);
			}
		}

		if(ViewableUI != null)
		{
			ViewableUI.ShowFightDeathOptions(false);
		}
	}

	/// <summary>
	/// Complete the fight roll.
	/// </summary>
	private void FightRoll()
	{
		Zombie heroZombie = gameStats.GetZombieTypes().GetZombieOfType(ZombieType.Hero);
		Zombie normalZombie = gameStats.GetZombieTypes().GetZombieOfType(ZombieType.Normal);
		int totalFightDice;

		//TODO what if more than one zombie hero
		// If fighting a hero zombie.
		if(zombieToFight == "hero")
		{
			totalFightDice = heroZombie.GetTotalFightDice() + gameStats.GetZombieTypes().TotalFightDiceNumber();
		}
		else
		{
			totalFightDice = normalZombie.GetTotalFightDice() + gameStats.GetZombieTypes().TotalFightDiceNumber();
		}

		int[] fightDiceResult = Actions.FightRollArray(totalFightDice);

		for(int i = 0; i < totalFightDice; i++)
		{
			GameObject dieResult = Instantiate(FightRollResultPrefab);
			dieResult.transform.SetParent(FightRollResultParent.transform);
			dieResult.transform.localScale = new Vector3(1,1,1);
			dieResult.transform.GetChild(0).GetComponent<Text>().text = fightDiceResult[i].ToString();
			Button button = dieResult.GetComponent<Button>();
			var index = i;
			button.onClick.AddListener(delegate { ReRollFightDie(index); });
		}

		// Reset temp fight dice, from cards.
		gameStats.GetZombieTypes().ResetTempFightDice();

		if(gameStats.GetZombieTypes().IsZombieTurn)
			Phases.TurnPhaseDelegate = Phases.PhaseFightResults;

		if(ViewableUI != null)
		{
			ViewableUI.ShowFightDeathOptions(true);
		}
	}

	/// <summary>
	/// Get the highest value of the zombie roll.
	/// </summary>
	/// <returns>The high value.</returns>
	public int HighestFightRollValue()
	{
		List<int> numbers = ZombieFightRollValues();

		if(numbers.Count > 0)
		{
			return numbers.Max();
		}

		return 0;
	}

	/// <summary>
	/// Get the string of the complete fight roll text.
	/// </summary>
	/// <returns>The string.</returns>
	public List<int> ZombieFightRollValues()
	{
		int numberOfDice = FightRollResultParent.transform.childCount;
		List<int> numbers = new List<int>();

		for(int i = 0; i < numberOfDice; i++)
		{
			string textValue = FightRollResultParent.transform.GetChild(i).GetChild(0).GetComponent<Text>().text;
			int value = Convert.ToInt32(textValue);
			numbers.Add(value);
		}

		return numbers;
	}

	public void ReRollFightDie(int index)
	{
		int newRoll = Actions.RollD6();
		FightRollResultParent.transform.GetChild(index).GetChild(0).GetComponent<Text>().text = newRoll.ToString();
	}

	/// <summary>
	/// Update the fight roll values with new values.
	/// </summary>
	/// <param name="newValues">New fight dice values.</param>
	public void UpdateZombieFightRollvalues(List<int> newValues)
	{
		int numberOfDice = FightRollResultParent.transform.childCount;

		for(int i = 0; i < numberOfDice; i++)
		{
			FightRollResultParent.transform.GetChild(i).GetChild(0).GetComponent<Text>().text = newValues[i].ToString();
		}
	}
	
	/// <summary>
	/// Try if would die in fight.
	/// </summary>
	[UsedImplicitly]
	public void FightWouldDieTry()
	{
		if(gameStats.GetZombieTypes().IsZombiesCanBeKilled)
		{
			CardsToPlay = decks.CheckIfInHand(PlayDuringPhase.FightAfterRoll);
		}

		if(gameStats.GetZombieTypes().IsMultipleFightCards)
		{
			fightCardUsed = false;
		}
		
		if(CardsToPlay.Count > 0 && !fightCardUsed) {
			if(!PlayCardBasedOnDifficulty())
			{
				if(isDebugLog)
				{
					Debug.Log("<color=orange>Not Playing card " + CardsToPlay[0].GetName() + "</color>");
				}
				cardsPlayed.Add(CardsToPlay[0].GetName());
				decks.DoNotPlayCard(CardsToPlay[0]);
			}

			if(isDebugLog)
			{
				Debug.Log("Playing after fight roll: " + CardsToPlay[0].GetName());
			}
			cardBeingPlayed = CardsToPlay [0];
			StartCoroutine(PlayCard());
			fightCardUsed = true;
			Phases.TurnPhaseDelegate = Phases.PhaseFightHeroes;
			
			PlayClickAudio();
		}
		else
		{
			ZombieDied("fight");
		}
	}
	
	#endregion

	/// <summary>
	/// Game is over.
	/// </summary>
	/// <param name="winner">Winner.</param>
	public void GameOver(string winner)
	{
		ResultText.text = winner.ToUpper() + " WIN!";
		ResultPanel.GetComponent<Animator>().SetTrigger("open");
		gameData.SetWinner(winner);
		UpdateLogRoundDetails();

		if(PlayerPrefs.GetString(Strings.PlayerPrefKeys.GameType) == Strings.PlayerPrefKeys.GameTypeCampaign)
		{
			CampaignManager.CloseOutMission();
//			ResultNarrativeText.text = winner == "Heroes" ? gameStats.GetCurrentMission().EndNarrativeText[0] : gameStats.GetCurrentMission().EndNarrativeText[1];
		}

		if(gameStats.GetCurrentMission().EndNarrativeText != null)
		{
			ResultNarrativeText.text = winner == "Heroes" ? gameStats.GetCurrentMission().EndNarrativeText[0] : gameStats.GetCurrentMission().EndNarrativeText[1];
		}

		Analytics.CustomEvent("gameOver", new Dictionary<string, object>
		{
			{"winner", winner}
		});

		// Play audio
		if(winner == "Heroes")
		{
			GameObject.Find(gameStats.GetCurrentMission().AudioWin).GetComponent<AudioSource>().Play();
		}
		else
		{
			GameObject.Find(gameStats.GetCurrentMission().AudioLose).GetComponent<AudioSource>().Play();
		}
		
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.AutoSave, 0);
	}

	#region helpers
	
	/// <summary>
	/// Updates the user interface text.
	/// </summary>
	public void UpdateUIText()
	{
		ZombiesOnBoardText.text = "ZOMBIES ON BOARD: " + gameStats.GetZombieTypes().GetZombiesOnBoardCount();
	}

	/// <summary>
	/// Updates the log.
	/// </summary>
	public void UpdateLogRoundDetails()
	{
		// Update UI log
		GameLog.AddText("<b>Start of Round details</b>");
		GameLog.AddText("Rounds Left: " + round);
		GameLog.AddText("Heroes dead: " + heroesDiedCount);
		GameLog.AddText("Zombies Dead: " + gameStats.GetZombieTypes().GetZombiesKilledCount());
		GameLog.AddText("Zombies on Board: " + gameStats.GetZombieTypes().GetZombiesOnBoardCount());

		// Update data log
		RoundData rd = new RoundData(gameStats.GetHeroCharacters().GetListOfHeroesInGame(), round, 
			gameStats.GetZombieTypes().GetZombiesKilledCount(), gameStats.GetZombieTypes().GetZombiesOnBoardCount(), 
			heroesDiedCount, gameStats.GetMapTiles().HowManyBuildingAreTakenOver(), 
			gameStats.GetMapTiles().HowManyBuildingsHaveLightsOut(), gameStats.GetMapTiles().HowManyBuildingsHaveSpawningPits());
		
		gameData.AddRound(rd);
		GsfuUtilsLnoe.SetGameLog(gameData, gameData.GetRoundDataCount() - 1);
		GsfuUtilsLnoe.SaveGameLog(true);
		
		// Compute the game state through the RBF network
		double[] normalizedRoundData = Normalization.NormalizeRoundData(rd.GetRoundsLeft(), rd.GetZombiesDead(),
			rd.GetZombiesOnBoard(), rd.GetHeroesDead(), rd.GetBuildingsTakenOver(), rd.GetBuildingsLightsOut(),
			rd.GetSpawningPits());

		// Get current goal progress for each side.
		// TODO may not need once RBF network has a lot of data
		double heroGoalPercent = 0;
		double zombieGoalPercent = 0;
		foreach(GameObject objective in gameObjectives)
		{
			Objective obj = objective.GetComponent<Objective>();
			double objPercent =(obj.CurrentProgress + 0.01) / obj.Goal;
			
			if(obj.IsHeroGoal)
			{
				if(objPercent > heroGoalPercent)
				{
					heroGoalPercent = objPercent;
				}
			}
			else
			{
				if(objPercent > zombieGoalPercent)
				{
					zombieGoalPercent = objPercent;
				}
			}
		}
		
		gameStats.SetGameDifficulty(normalizedRoundData, heroGoalPercent, zombieGoalPercent);
	}

	/// <summary>
	/// Update the objective UI and check for win condition.
	/// </summary>
	/// <param name="objectiveName">Objective to update.</param>
	public void SetObjective(string objectiveName)
	{
		foreach(GameObject objective in gameObjectives)
		{
			//Check if right objective
			Objective objectiveComponent = objective.GetComponent<Objective>();
			if(objectiveComponent.Name != objectiveName) continue;
			GameLog.AddText("<color=teal>" + objectiveName + " increased!</color>");
			bool endGame = objectiveComponent.UpdateProgress();
			if(endGame)
			{
				GameOver(objectiveComponent.IsHeroGoal ? "Heroes" : Strings.Zombies);
			}
		}
	}

	/// <summary>
	/// Show the UI for the story narrative.
	/// </summary>
	public void ShowCampaignNarrative(int objectiveStage)
	{
		NarrativeTitle.text = gameStats.GetCurrentMission().Name;
		NarrativeText.text = gameStats.GetCurrentMission().NarrativeText[objectiveStage];
		if(NarrativeText.text == "Aaarrrrgggghhh")
		{
			GameObject.Find("ZombieGroan").GetComponent<AudioSource>().Play();
		}
		NarrativePanel.GetComponent<Animator>().SetTrigger("open");
	}

	/// <summary>
	/// Show the campaign narrative UI.
	/// </summary>
	/// <param name="title">Title for the narrative.</param>
	/// <param name="text">Narrative text.</param>
	private void ShowCampaignNarrative(string title, string text)
	{
		NarrativeTitle.text = title;
		NarrativeText.text = text;
		NarrativePanel.GetComponent<Animator>().SetTrigger("open");
	}
	
	/// <summary>
	/// Helper for settings up the objective UI.
	/// </summary>
	/// <param name="objective">The objectives.</param>
	/// <param name="hero">Is for a hero.</param>
	private void CreateObjectiveUI(Mission.ObjectiveTypes objective, bool hero)
	{
		// Create the UI gameobject
		GameObject objectiveUI = Instantiate(ObjectivePrefab);
		objectiveUI.transform.SetParent(hero ? HeroObjectivePanel.transform : ZombieObjectivePanel.transform);
		objectiveUI.transform.localScale = new Vector3(1, 1, 1);
		
		// Change the color is zombie objective
		if(!hero)
		{
			objectiveUI.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>().color = new Color(0.41f,0.29f,0.29f);
			objectiveUI.transform.GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().color = new Color(0.41f,0.0f,0.13f);
		}
		
		// Keep the button?
		if(!objective.Button)
		{
			objectiveUI.transform.GetChild(1).gameObject.SetActive(false);
			objectiveUI.GetComponent<RectTransform>().sizeDelta = new Vector2(120f, 40f);
		}
		
		// Setup values and add to game objective list
		objectiveUI.GetComponent<Objective>().SetObjective(objective.Name, objective.Value, objective.Audio, hero);
		gameObjectives.Add(objectiveUI);
	}
	
	/// <summary>
	/// Set the text on the fight panel.
	/// </summary>
	public void SetFightInfoText()
	{
		string winner = Strings.Zombies;
		if(!zombiesWinOnTie)
			winner = "Heroes";

		FightText.text = "";

		Zombie zombiePlague = gameStats.GetZombieTypes().GetZombieOfType(ZombieType.Plague);
		if(zombiePlague != null)
		{
			FightText.text += "Plague Carrier Zombies roll " +(zombiePlague.FightDice + zombiePlague.BonusDice) + " dice\n";
		}

		Zombie zombieHero = gameStats.GetZombieTypes().GetZombieOfType(ZombieType.Hero);
		if(zombieHero != null)
		{
			FightText.text += "Zombie Heroes roll " +(zombieHero.FightDice + zombieHero.BonusDice) + " dice.\n";
		}

		Zombie zombie = gameStats.GetZombieTypes().GetZombieOfType(ZombieType.Normal);
		int zombieTotalDice = zombie.FightDice + zombie.BonusDice;

		string zombiesCannotBeKilled = "";
		if(!gameStats.GetZombieTypes().IsZombiesCanBeKilled)
		{
			zombiesCannotBeKilled = "Zombies cannot be killed.";
		}

		FightText.text += "Zombies roll " + zombieTotalDice + " dice.\n" + winner + " win on a tie.\n" + zombiesCannotBeKilled;
	}

	/// <summary>
	/// Create the movement text for the zombie.
	/// </summary>
	/// <param name="zombieMoving">The zombie to move.</param>
	/// <returns>The movement text.</returns>
	public string GetMovementText(Zombie zombieMoving)
	{
		StringBuilder movementText = new StringBuilder();

		if(zombieMoving.GetZombieType() == ZombieType.Normal)
		{
			movementText.Append("Where possible a space should not contain more than 3 zombies.\n");

			if(theHungryOne)
			{
				int x = Actions.RollD6();
				movementText.Append("<color=red>One Zombie moves " + x + " space(s).</color>");
			}
			
			// Taken Over
			if(takenOverBuilding.Count > 0)
			{
				foreach(string s in takenOverBuilding)
				{
					movementText.Append("Zombies in the <color=red>" + s + "</color> don't move.\n");
				}
				takenOverBuilding.Clear(); // Reset
			}
		}

		// Same Space
		movementText.Append("If " + zombieMoving.GetZombieName() + " is on the same space as a Hero don't move it.\n");


		// Hunger
		movementText.Append("If " + zombieMoving.GetZombieName() + " is within " + zombieMoving.GetMovementHunger() +
		                " of a Hero, move it into that space.\n");
		// Otherwise
		if(zombieMoving.GetMovementSpeed() != null)
		{
			string target = zombieMoving.GetMovementTarget();
			
			// Check if mission target, then replace.
			if(gameStats.GetCurrentMission().MissionMovementTarget != "")
			{
				target = gameStats.GetCurrentMission().MissionMovementTarget;
				if(target.Contains("DESTROYBUILDING"))
				{
					string replace = "";
					List<string> buildings = gameStats.GetMapTiles().GetBuildingToDestroyPriority().GetRange(0, 3);

					foreach(string building in buildings)
					{
						replace += building + ", ";
					}

					target = target.Replace("DESTROYBUILDING", replace);
				}
			}
			
			movementText.Append("Otherwise " + zombieMoving.GetZombieName() + " moves " + zombieMoving.GetMovementSpeed() +
			                " towards <color=red>" + target.ToUpper() + "</color>.");
		}

		return movementText.ToString();
	}

	/// <summary>
	/// Check if card should be played based on the current difficulty of the game.
	/// To be checked during play card phases.
	/// 
	/// Won't apply to play immediately cards.
	/// </summary>
	/// <returns></returns>
	public bool PlayCardBasedOnDifficulty()
	{
		// Get the zombies win percentage.
		double zombieWinPercentage = gameStats.GetGameDifficulty()[1];
		
		// Check if Zombies are in favor of winning.
		// Continue if the zombies are greater than 60% odds of winning.
		if(!(zombieWinPercentage > 0.5)) return true;

		if(isDebugLog)
		{
			Debug.Log("<color=orange>Check card play based on difficulty</color>");
		}
		
		// Determine what a random number has to get to allow the card to be played.
		// At 0 - 60% of zombie winning all cards playable
		// At 100%(not possible) of zombies winning 0% of the cards playable.
			
		// Get the play card range from 0.0 - 1.0
		float playCardRange = 2.0f *((float) zombieWinPercentage - 0.5f);
		
		// Get a random number
		float randomNumber = Random.Range(0.0f, 1.0f);

		cardsPlayed.Add("Odds of not playing card: " + playCardRange.ToString("F2") + ". Playing card if following " +
		                "value is higher: " + randomNumber.ToString("F2") + ".");

		// If random number is greater than play card range, play the card
		if(randomNumber > playCardRange) return true;
		
		// Otherwise do not play the card.
		if(isDebugLog)
		{
			Debug.Log("<color=orange>Not Playing Card</color>");
		}
		return false;
	}

    /// <summary>
    /// Spawns the special zombie based on difficulty.
    /// </summary>
    /// <returns>True or false.</returns>
    private bool SpawnSpecialZombieBasedOnDifficulty()
	{
		// Not during setup
		if(round < 0)
		{
			return false;
		}
		
		double zombieWinPercentage = gameStats.GetGameDifficulty()[1];
		if(!(zombieWinPercentage > 0.5))
		{
			return true;
		}

		if(zombieWinPercentage > 0.75)
		{
			return false;
		}

		return Random.Range(0.0f, 1.0f) > 0.5f;
	}
	
	#endregion

	#region Debug tools

	/// <summary>
    /// For inside the editor, draw a card.
    /// </summary>
    [UsedImplicitly]
    public void DebugDrawCard()
    {
    	decks.DrawCard();
    }

	/// <summary>
	/// Adds the selected card from the debug list to the hand.
	/// </summary>
	/// <param name="text">The selected card name.</param>
	[UsedImplicitly]
	public void AddDebugCardToHand(Text text)
	{
		if(text.text == "None"){return;}
		
		decks.AddCardToHand(decks.GetCard(text.text));
	}

	#endregion

	/// <summary>
	/// Displaying tutorial info for the speak button.
	/// </summary>
	[UsedImplicitly]
	public void TutorialSpeak()
	{
		if(PlayerPrefs.GetString(Strings.PlayerPrefKeys.Scenario) != "Tutorial") return;
		TutorialText.text = Tutorial.Speak;
		TutorialPanel.GetComponent<Animator>().SetTrigger("open");
	}

	/// <summary>
	/// Play button press sound.
	/// </summary>
	public void PlayClickAudio()
	{
		if(clickAudio != null)
		{
			clickAudio.Play();
		}
	}

	/// <summary>
	/// Show RBF info in the UI
	/// </summary>
	[UsedImplicitly]
	public void DisplayRBFInfo()
	{
		Text rbfTextObject = GameObject.Find("TextRBF").GetComponent<Text>();
		StringBuilder rbfString = new StringBuilder();

		// Current RBF state
		rbfString.Append("Current RBF Network estimated winner:" +
		                 "\nHero = HERODIFFICULTY%" +
		                 "\nZombie = ZOMBIEDIFFICULTY%" +
		                 "\n\nRound cards played information:");
		rbfString.Replace("HERODIFFICULTY", (gameStats.GetGameDifficulty()[0] * 100).ToString("F0"));
		rbfString.Replace("ZOMBIEDIFFICULTY", (gameStats.GetGameDifficulty()[1] * 100).ToString("F0"));

		foreach(string s in cardsPlayed)
		{
			if(s.Contains("Round"))
			{
				rbfString.Append("<color=white>");
			}
			rbfString.Append("\n" + s);
			if(s.Contains("Round"))
			{
				rbfString.Append("</color>");
			}
		}

		rbfTextObject.text = rbfString.ToString();

		RectTransform parentRect = rbfTextObject.transform.parent.GetComponent<RectTransform>();
		parentRect.sizeDelta = new Vector2(parentRect.sizeDelta.x, 25.0f * cardsPlayed.Count + 100.0f);
		
		PlayClickAudio();
	}
	
	/// <summary>
	/// Auto save the current game state.
	/// </summary>
	public void AutoSaveGame()
	{
		if(isDebugLog)
		{
			Debug.Log("Auto saving game.");
		}
		
		GameSaveData game = new GameSaveData
		{
			HandDeck = decks.CardListFromDeck(decks.GetHand()),
			DiscardDeck = decks.CardListFromDeck(decks.GetDiscard()),
			RemainsDeck = new List<GameSaveData.RemainsInPlayCardDetails>(),
			ZombiesOnBoard = gameStats.GetZombieTypes().GetZombiesOnBoardCount(),
			ObjectivesStates = new List<int>(),
			Spawn = spawn,
			ZombiesWinOnTie = zombiesWinOnTie,
			ZombieMovesAfterSpawn = zombieMovesAfterSpawn,
			TheHungryOne = theHungryOne,
			FightCardUsed = fightCardUsed,
			Round = round,
			HeroesDiedCount = heroesDiedCount,
			NumberOfHeroesInGame = numberOfHeroesInGame,
			GameLog = GameLog.GameLogSave,
			CardsPlayed = cardsPlayed,
			GameData = gameData,
			PhaseName = Phases.PhaseName,
			ZombieCanBeKilled = gameStats.GetZombieTypes().IsZombiesCanBeKilled
		};

		// Get remains in play card details
		foreach(ZombieCard card in decks.GetRemains())
		{
			game.RemainsDeck.Add(new GameSaveData.RemainsInPlayCardDetails{
				Name = card.GetName(),
				RemainsInPlayName = card.GetRemainsInPlayName(),
				CardText = card.GetCardText()
				});
		}
		
		// Save all the objective values
		foreach(GameObject o in gameObjectives)
		{
			game.ObjectivesStates.Add(o.GetComponent<Objective>().CurrentProgress);
		}

		// Save the game state
		AutoSave.Save(game);
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.AutoSave, 1);
	}
	
	/// <summary>
	/// Load the auto saved game state.
	/// </summary>
	private void LoadSaveGame()
	{
		GameSaveData game = AutoSave.Load();

		// Get list of remains in play cards
		List<string> remainsInPlayCardList = new List<string>();
		foreach(GameSaveData.RemainsInPlayCardDetails remainsCard in game.RemainsDeck)
		{
			remainsInPlayCardList.Add(remainsCard.Name);
		}
		
		decks.SetDecksFromLists(game.HandDeck, game.DiscardDeck, remainsInPlayCardList);
		for(int i = 0; i < game.RemainsDeck.Count; i++)
		{
			decks.GetRemains()[i].SetRemainsInPlayDetailsFromAutoSave(
				game.RemainsDeck[i].RemainsInPlayName, game.RemainsDeck[i].CardText);
			AddToRemainsInPlayPanel(decks.GetRemains()[i]);
		}
		gameStats.GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesOnBoard = game.ZombiesOnBoard;
		gameStats.GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool -= game.ZombiesOnBoard;
		if(game.ZombiesOnBoard > 0)
		{
			ViewableUI.SetZombiesKillable(game.ZombieCanBeKilled);
			ViewableUI.SetZombiesOnBoard(true);
		}
		for(int i = 0; i < gameObjectives.Count; i++)
		{
			gameObjectives[i].GetComponent<Objective>().SetProgress(game.ObjectivesStates[i]);
		}
		spawn = game.Spawn;
		zombiesWinOnTie = game.ZombiesWinOnTie;
		zombieMovesAfterSpawn = game.ZombieMovesAfterSpawn;
		theHungryOne = game.TheHungryOne;
		fightCardUsed = game.FightCardUsed;
		round = game.Round;
		heroesDiedCount = game.HeroesDiedCount;
		numberOfHeroesInGame = game.NumberOfHeroesInGame;

		foreach(GameSaveData.GameLogEntries logEntry in game.GameLog)
		{
			if(logEntry.LogType == GameSaveData.GameLogEntries.Type.Text)
			{
				GameLog.AddText(logEntry.LogText);
			}
			else
			{
				Texture2D texture2D = new Texture2D(logEntry.TextureWidth, logEntry.TextureHeight, logEntry.LogTextureFormat, false);
				texture2D.LoadRawTextureData(logEntry.LogTexture);
				texture2D.Apply();
				GameLog.AddImage(texture2D);
			}
		}

		gameData = game.GameData;
		cardsPlayed = game.CardsPlayed;
		Phases.SetZombieTurnPhase(game.PhaseName);
		gameStats.GetZombieTypes().IsZombiesCanBeKilled = game.ZombieCanBeKilled;
	}

	/// <summary>
	/// If game goes into background.
	/// </summary>
	/// <param name="hasFocus"></param>
	private void OnApplicationFocus(bool hasFocus)
	{
		if(!hasFocus)
		{
			AutoSaveGame();
		}
	}

	/// <summary>
	/// If game paused by system.
	/// </summary>
	/// <param name="pauseStatus"></param>
	private void OnApplicationPause(bool pauseStatus)
	{
		if(pauseStatus)
		{
			AutoSaveGame();
		}
	}

	/// <summary>
	/// If game terminated.
	/// </summary>
	private void OnApplicationQuit()
	{
		AutoSaveGame();
	}
}
