using System;

/// <summary>
/// Zombie turn phases.
/// </summary>
[Serializable]
public enum ZombieTurnPhaseName
{
    StartOfTurn, PlayStartOfTurnCards, DrawNewCards, PlayImmediatelyCards, CheckIfNewZombiesSpawn, MoveZombies, FightHeroes, FightResults, NewZombies, EndOfTurnCards, HeroesTurn
}