﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class AutoSave
{
    public static void Save(GameSaveData game)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/autoSave.dat");
        bf.Serialize(file, game);
        file.Close();
    }

    public static GameSaveData Load()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/autoSave.dat", FileMode.Open);
        GameSaveData game = (GameSaveData) bf.Deserialize(file);
        file.Close();
        return game;
    }
}