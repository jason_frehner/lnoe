﻿using JetBrains.Annotations;
using UnityEngine;
//using Vuforia;

public class ARControl : MonoBehaviour
{

	public GameObject GameCamera;
	public GameObject ARCamera;
	public GameObject LogCamera;
	public GameObject[] ARHideObjects;
	// private bool isARon;
	public bool AutoStart;

	// private void Awake()
	// {
	// 	VuforiaRuntime.Instance.Deinit();
	// }

	private void Start()
	{
		ARCamera.GetComponent<Camera>().enabled = false;
//		ARCamera.SetActive(false);
		if(!AutoStart) return;
		//VuforiaRuntime.Instance.InitVuforia();
		// isARon = true;
	}

	/// <summary>
	/// Enable AR
	/// </summary>
	[UsedImplicitly]
	public void EnableAR()
	{
		GameCamera.GetComponent<Camera>().enabled = false;
		LogCamera.GetComponent<Camera>().enabled = false;
		ARCamera.GetComponent<Camera>().enabled = true;
		// if(!VuforiaRuntime.Instance.HasInitialized)
		// {
		// 	VuforiaRuntime.Instance.InitVuforia();
		// }
		// VuforiaBehaviour.Instance.enabled = true;
		foreach(GameObject o in ARHideObjects)
		{
			o.SetActive(false);
		}
		// isARon = true;
	}

	/// <summary>
	/// Disable AR
	/// </summary>
	[UsedImplicitly]
	public void DisableAR()
	{
		GameCamera.GetComponent<Camera>().enabled = true;
		LogCamera.GetComponent<Camera>().enabled = true;
		ARCamera.GetComponent<Camera>().enabled = false;
		// ARCamera.GetComponent<VuforiaBehaviour>().enabled = false;
		foreach(GameObject o in ARHideObjects)
		{
			o.SetActive(true);
		}
		// isARon = false;
	}

	// private void Update()
	// {
	// 	if(isARon && !VuforiaBehaviour.Instance.enabled)
	// 	{
	// 		VuforiaBehaviour.Instance.enabled = true;
	// 	}
	// }
}
