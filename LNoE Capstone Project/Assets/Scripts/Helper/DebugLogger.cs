using UnityEngine;

namespace Helper
{
    public static class DebugLogger
    {
        public static void WriteLog(object message)
        {
            if(IsDebugLog())
            {
                Debug.Log(message);
            }
        }
        
        private static bool IsDebugLog()
        {
            return PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
        }
    }
}