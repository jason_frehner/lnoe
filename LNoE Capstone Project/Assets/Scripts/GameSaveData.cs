﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GameSaveData
{
    [Serializable]
    public class RemainsInPlayCardDetails
    {
        public string Name;
        public string RemainsInPlayName;
        public string CardText;
    }

    [Serializable]
    public class GameLogEntries
    {
        public enum Type
        {
            Text, Image
        }

        public Type LogType;
        public string LogText;
        public byte[] LogTexture;
        public TextureFormat LogTextureFormat;
        public int TextureWidth;
        public int TextureHeight;
    }
	
    public List<string> HandDeck;
    public List<string> DiscardDeck;
    public List<RemainsInPlayCardDetails> RemainsDeck;
    public List<int> ObjectivesStates;
    public int ZombiesOnBoard;
    public bool Spawn;
    public bool ZombiesWinOnTie;
    public bool ZombieMovesAfterSpawn;
    public bool TheHungryOne;
    public bool FightCardUsed;
    public int Round;
    public int HeroesDiedCount;
    public int NumberOfHeroesInGame;
    public List<GameLogEntries> GameLog;
    public List<string> CardsPlayed;
    public GameData GameData;
    //TODO RBF info
    public ZombieTurnPhaseName PhaseName;
    public bool ZombieCanBeKilled;
}