﻿using JetBrains.Annotations;
using UnityEngine;

public class ViewableUI : MonoBehaviour
{
	public ZombieTurn ZombieTurn;
	public GameObject FightHeroKilledButton;
	public GameObject FightZombieKilledButton;
	public GameObject StartFightButton;
	public GameObject FightZombieHero;

	public GameObject KillZombieButton;
	
	public GameObject DebugDrawButton;
	public GameObject DebugCardDropdown;
	public GameObject DebugToolsButton;

	private bool isZombiesOnBoard;
	public void SetZombiesOnBoard(bool value)
	{
		isZombiesOnBoard = value;
		ShowShotAndKillButtons();
	}

	private bool isZombiesKillable;
	public void SetZombiesKillable(bool value)
	{
		isZombiesKillable = value;
		ShowShotAndKillButtons();
	}

	private bool isZombieHeroOnBoard;
	private void SetZombieHeroOnBoard(bool value)
	{
		isZombieHeroOnBoard = value;
		ShowFightZombieHero();
	}
	
	private bool isDebugLog;
	
	private void Awake()
	{
		isDebugLog = PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
	}

	private void Start()
	{
#if UNITY_EDITOR
		DebugToolsButton.SetActive(true);
#endif

		ZombieTurn.GetGameStats().GetZombieTypes().ZombiesOnBoardChanged += SetZombiesOnBoard;
		ZombieTurn.GetGameStats().GetZombieTypes().ZombiesAreKillableChanged += SetZombiesKillable;
		ZombieTurn.GetGameStats().GetZombieTypes().HeroZombieOnBoardChanged += SetZombieHeroOnBoard;
		
		SetZombieHeroOnBoard(false);
		SetZombiesKillable(true);
		SetZombieHeroOnBoard(false);
	}

	/// <summary>
	/// Show Hero or Zombie died options only once dice have been rolled.
	/// </summary>
	/// <param name="fightRolled">Has fight dice been rolled.</param>
	public void ShowFightDeathOptions(bool fightRolled)
	{
		if (!fightRolled)
		{
			FightHeroKilledButton.SetActive(false);
			FightZombieKilledButton.SetActive(false);
		}
		else
		{
			FightHeroKilledButton.SetActive(true);
			if (ZombieTurn.GetGameStats().GetZombieTypes().IsZombiesCanBeKilled)
			{
				FightZombieKilledButton.SetActive(true);
			}
		}
	}

	private void ShowShotAndKillButtons()
	{
		// If zombies are on board hide buttons if they can't be killed.
		if (isZombiesOnBoard)
		{
			if(isDebugLog)
			{
				Debug.Log("Show shot and kill");
			}
			KillZombieButton.SetActive(isZombiesKillable);
			StartFightButton.SetActive(true);
		}
		else
		{
			if(isDebugLog)
			{
							Debug.Log("DO NOT Show shot and kill");
			}
			KillZombieButton.SetActive(false);
			StartFightButton.SetActive(false);
		}
	}

	private void ShowFightZombieHero()
	{
		FightZombieHero.SetActive(isZombieHeroOnBoard);
	}

	/// <summary>
	/// Toggle the debug tools in the editor on and off.
	/// </summary>
	[UsedImplicitly]
	public void ToggleDebugTools()
	{
		DebugDrawButton.SetActive(!DebugDrawButton.activeSelf);
		DebugCardDropdown.SetActive(!DebugCardDropdown.activeSelf);
	}
}
