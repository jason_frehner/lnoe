public class SisterOphelia : Hero
{
    public SisterOphelia()
    {
        Name = "Sister Ophelia";
        Title = "Reformed Nun";
        Keyword.Add(KeywordType.Holy);
        Keyword.Add(KeywordType.Strange);
        Gender = GenderType.Female;
        StartLocation = "Random Building";
        FlavorText = "I've done a lot of things I'm not proud of.";
        Health = 3;
    }
}