public class AgentCarter : Hero
{
    public AgentCarter()
    {
        Name = "Agent Carter";
        Title = "FBI";
        Keyword.Add(KeywordType.LawEnforcement);
        Gender = GenderType.Male;
        StartLocation = "Random Building";
        FlavorText = "I'm afraid that's classified.";
        Health = 3;
    }
}