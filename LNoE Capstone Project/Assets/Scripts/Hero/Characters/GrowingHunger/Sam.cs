public class Sam : Hero
{
    public Sam()
    {
        Name = Strings.HeroNames.Sam;
        Title = "The Diner Cook";
        Keyword.Add(KeywordType.Military);
        Gender = GenderType.Male;
        StartLocation = "Diner";
        FlavorText = "What's say we take this outside.";
        Health = 4;
    }
}