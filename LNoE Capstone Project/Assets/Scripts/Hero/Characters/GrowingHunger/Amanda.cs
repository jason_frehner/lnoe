public class Amanda : Hero
{
    public Amanda()
    {
        Name = Strings.HeroNames.Amanda;
        Title = "Prom Queen";
        Keyword.Add(KeywordType.Student);
        Gender = GenderType.Female;
        StartLocation = "High School";
        FlavorText = "Oh....My....God! Are you serious?!";
        Health = 2;
    }
}