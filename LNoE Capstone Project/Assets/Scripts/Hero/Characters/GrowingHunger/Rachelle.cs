public class Rachelle : Hero
{
    public Rachelle()
    {
        Name = Strings.HeroNames.Rachelle;
        Title = "Detective Winters";
        Keyword.Add(KeywordType.LawEnforcement);
        Gender = GenderType.Female;
        StartLocation = "Random Building";
        FlavorText = "It's probably nothing; I'm gonna check it out.";
        Health = 3;
        StartItem = "Revolver and Flashlight";
    }
}