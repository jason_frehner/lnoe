public class Kenny : Hero
{
    public Kenny()
    {
        Name = Strings.HeroNames.Kenny;
        Title = "Super Market Bag Boy";
        Keyword.Add(KeywordType.Student);
        Keyword.Add(KeywordType.Strange);
        Gender = GenderType.Male;
        StartLocation = "Super Market";
        FlavorText = "I always wanted to do that!";
        Health = 2;
    }
}