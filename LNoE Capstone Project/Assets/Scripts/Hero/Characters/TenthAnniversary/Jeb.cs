public class Jeb : Hero
{
    public Jeb()
    {
        Name = "Jeb";
        Title = "The Grease Monkey";
        Keyword.Add(KeywordType.Tech);
        Keyword.Add(KeywordType.Strange);
        Gender = GenderType.Male;
        StartLocation = "Gas Station";
        FlavorText = "Yeeaahhh! That's what I'm talkin' 'bout!";
        Health = 3;
    }
}