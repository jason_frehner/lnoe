public class DeputyTaylor : Hero
{
    public DeputyTaylor()
    {
        Name = "Deputy Taylor";
        Title = "Sheriff's Deputy";
        Keyword.Add(KeywordType.LawEnforcement);
        Gender = GenderType.Male;
        StartLocation = "Random Building";
        FlavorText = "Why does this kind of stuff always happen to me?";
        Health = 3;
        StartItem = "Pump Shotgun";
    }
}