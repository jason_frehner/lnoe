public class MrHyde : Hero
{
    public MrHyde()
    {
        Name = "Mr. Hyde";
        Title = "The Shop Teacher";
        Keyword.Add(KeywordType.Teacher);
        Gender = GenderType.Male;
        StartLocation = "High School";
        FlavorText = "Let's do this!";
        Health = 3;
    }
}