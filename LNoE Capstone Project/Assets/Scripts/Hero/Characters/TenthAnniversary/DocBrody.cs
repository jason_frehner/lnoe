public class DocBrody : Hero
{
    public DocBrody()
    {
        Name = "Doc Brody";
        Title = "Country Physician";
        Keyword.Add(KeywordType.Medical);
        Gender = GenderType.Male;
        StartLocation = "Hospital";
        FlavorText = "I'm gettin' too old for this!";
        Health = 3;
        StartItem = "2 Experience Tokens";
    }
}