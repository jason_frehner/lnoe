public class SheriffAndersonSurvivor : Hero
{
    public SheriffAndersonSurvivor()
    {
        Name = "Sheriff Anderson Survivor";
        Title = "Woodinvale Survivor";
        Keyword.Add(KeywordType.LawEnforcement);
        Gender = GenderType.Male;
        StartLocation = "Road out of Town";
        FlavorText = "We've got to keep moving!";
        Health = 3;
    }
}