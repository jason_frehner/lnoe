public class JakeCartwrightSurvivor : Hero
{
    public JakeCartwrightSurvivor()
    {
        Name = "Jake Cartwright Survivor";
        Title = "Woodinvale Survivor";
        Keyword.Add(KeywordType.Drifter);
        Keyword.Add(KeywordType.Strange);
        Gender = GenderType.Male;
        StartLocation = "Road out of Town";
        FlavorText = "I learned long ago, you can't save everyone.";
        Health = 3;
        StartItem = "Zombie card: Outsider";
    }
}