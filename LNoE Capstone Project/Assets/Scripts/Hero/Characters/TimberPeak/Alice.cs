public class Alice : Hero
{
    public Alice()
    {
        Name = "Alice";
        Title = "Diner Waitress";
        Keyword.Add(KeywordType.Student);
        Gender = GenderType.Female;
        StartLocation = "Diner";
        FlavorText = "Order UP!";
        Health = 2;
    }
}