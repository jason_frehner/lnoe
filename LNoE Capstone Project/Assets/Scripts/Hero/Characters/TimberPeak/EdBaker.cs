public class EdBaker : Hero
{
    public EdBaker()
    {
        Name = "Ed Baker";
        Title = "Lumberjack";
        Keyword.Add(KeywordType.Military);
        Gender = GenderType.Male;
        StartLocation = "Diner";
        FlavorText = "Now I'm getting angry!";
        Health = 4;
    }
}