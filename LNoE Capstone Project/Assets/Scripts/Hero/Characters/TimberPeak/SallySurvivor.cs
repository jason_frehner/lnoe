public class SallySurvivor : Hero
{
    public SallySurvivor()
    {
        Name = "Sally Survivor";
        Title = "Woodinvale Survivor";
        Keyword.Add(KeywordType.Student);
        Keyword.Add(KeywordType.Strange);
        Gender = GenderType.Female;
        StartLocation = "Road out of Town";
        FlavorText = "I don't feel very lucky.";
        Health = 2;
    }
}