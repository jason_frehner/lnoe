public class Nikki : Hero
{
    public Nikki()
    {
        Name = "Nikki";
        Title = "Bush Pilot";
        Keyword.Add(KeywordType.Pilot);
        Gender = GenderType.Female;
        StartLocation = "Tavern";
        FlavorText = "I've got a bad feeling about this.";
        Health = 3;
    }
}