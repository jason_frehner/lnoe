public class DrYamato : Hero
{
    public DrYamato()
    {
        Name = "Dr. Yamato";
        Title = "Chemical Engineer";
        Keyword.Add(KeywordType.Science);
        Gender = GenderType.Male;
        StartLocation = "The Plant";
        FlavorText = "I can fix this!";
        Health = 3;
    }
}