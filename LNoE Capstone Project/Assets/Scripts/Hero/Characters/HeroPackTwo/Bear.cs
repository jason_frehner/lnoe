public class Bear : Hero
{
    public Bear()
    {
        Name = "Bear";
        Title = "The Biker";
        Keyword.Add(KeywordType.Outlaw);
        Gender = GenderType.Male;
        StartLocation = "Diner";
        FlavorText = "Now...What do we have here?";
        Health = 4;
    }
}