public class Angela : Hero
{
    public Angela()
    {
        Name = "Angela";
        Title = "Cheerleader";
        Keyword.Add(KeywordType.Student);
        Gender = GenderType.Female;
        StartLocation = "Gym";
        FlavorText = "Go Team! That's the Spirit!!!";
        Health = 2;
    }
}