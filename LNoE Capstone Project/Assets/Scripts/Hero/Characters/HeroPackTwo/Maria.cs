public class Maria : Hero
{
    public Maria()
    {
        Name = "Maria";
        Title = "Bookworm";
        Keyword.Add(KeywordType.Student);
        Keyword.Add(KeywordType.Strange);
        Gender = GenderType.Female;
        StartLocation = "Library";
        FlavorText = "I knew studying would pay off!";
        Health = 2;
    }
}