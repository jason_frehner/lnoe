public class SheriffAnderson : Hero
{
    public SheriffAnderson()
    {
        Name = Strings.HeroNames.SheriffAnderson;
        Title = "Small Town Law Man";
        Keyword.Add(KeywordType.LawEnforcement);
        Gender = GenderType.Male;
        StartLocation = "Police Station";
        FlavorText = "This is my town... and I'm taking it back.";
        Health = 3;
        StartItem = "Revolver";
    }
}