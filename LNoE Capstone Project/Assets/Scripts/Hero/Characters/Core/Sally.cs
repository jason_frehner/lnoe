public class Sally : Hero
{
    public Sally()
    {
        Name = Strings.HeroNames.Sally;
        Title = "High School Sweetheart";
        Keyword.Add(KeywordType.Student);
        Gender = GenderType.Female;
        StartLocation = "High School";
        FlavorText = "Alright, you asked for it.";
        Health = 2;
    }
}