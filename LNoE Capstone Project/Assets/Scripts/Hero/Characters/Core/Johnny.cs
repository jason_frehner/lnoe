public class Johnny : Hero
{
    public Johnny()
    {
        Name = Strings.HeroNames.Johnny;
        Title = "High School Quarterback";
        Keyword.Add(KeywordType.Student);
        Gender = GenderType.Male;
        StartLocation = "Gym";
        FlavorText = "I ain't goin' out like a chump.";
        Health = 2;
    }
}