public class Becky : Hero
{
    public Becky()
    {
        Name = Strings.HeroNames.Becky;
        Title = "The Nurse";
        Keyword.Add(KeywordType.Medical);
        Gender = GenderType.Female;
        StartLocation = "Hospital";
        FlavorText = "What the hell is going on in this town?!";
        Health = 3;
    }
}