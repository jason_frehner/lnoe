public class Billy : Hero
{
    public Billy()
    {
        Name = Strings.HeroNames.Billy;
        Title = "The Sheriff's Son";
        Keyword.Add(KeywordType.Student);
        Gender = GenderType.Male;
        StartLocation = "High School";
        FlavorText = "Back off! I can handle this.";
        Health = 2;
    }
}