public class Jenny : Hero
{
    public Jenny()
    {
        Name = Strings.HeroNames.Jenny;
        Title = "The Farmer's Daughter";
        Keyword.Add(KeywordType.Student);
        Gender = GenderType.Female;
        StartLocation = "Farmhouse";
        FlavorText = "Eat dirt...Zombie!";
        Health = 2;
    }
}