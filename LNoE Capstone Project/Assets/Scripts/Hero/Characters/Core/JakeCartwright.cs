public class JakeCartwright : Hero
{
    public JakeCartwright()
    {
        Name = Strings.HeroNames.JakeCartwright;
        Title = "The Drifter";
        Keyword.Add(KeywordType.Strange);
        Gender = GenderType.Male;
        StartLocation = "Road out of Town";
        FlavorText = "It's happening. They're coming...again.";
        Health = 3;
    }
}