public class FatherJoseph : Hero
{
    public FatherJoseph()
    {
        Name = Strings.HeroNames.FatherJoseph;
        Title = "Man of the Cloth";
        Keyword.Add(KeywordType.Holy);
        Gender = GenderType.Male;
        StartLocation = "Church";
        FlavorText = "Faith is stronger than zombies, my son.";
        Health = 3;
    }
}