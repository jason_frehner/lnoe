public class Jade : Hero
{
    public Jade()
    {
        Name = Strings.HeroNames.Jade;
        Title = "High School Outcast";
        Keyword.Add(KeywordType.Student);
        Keyword.Add(KeywordType.Strange);
        Gender = GenderType.Female;
        StartLocation = "Random Building";
        FlavorText = "Drop dead, creep!";
        Health = 2;
    }
}