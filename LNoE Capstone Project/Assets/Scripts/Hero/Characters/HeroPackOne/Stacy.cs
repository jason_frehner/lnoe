public class Stacy : Hero
{
    public Stacy()
    {
        Name = Strings.HeroNames.Stacy;
        Title = "Investigative Reporter";
        Keyword.Add(KeywordType.Strange);
        Gender = GenderType.Female;
        StartLocation = "Random Building";
        FlavorText = "This could be my big break, the story of a lifetime!";
        Health = 3;
    }
}