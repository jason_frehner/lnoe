public class Victor : Hero
{
    public Victor()
    {
        Name = Strings.HeroNames.Victor;
        Title = "Escaped Prisoner";
        Keyword.Add(KeywordType.Criminal);
        Keyword.Add(KeywordType.Strange);
        Gender = GenderType.Male;
        StartLocation = "Random Building";
        FlavorText = "It's probably nothing; I'm gonna check it out.";
        Health = 3;
        StartItem = "Zombie card: I don't trust 'em";
    }
}