public class MrGoddard : Hero
{
    public MrGoddard()
    {
        Name = Strings.HeroNames.MrGoddard;
        Title = "Chemistry Teacher";
        Keyword.Add(KeywordType.Teacher);
        Keyword.Add(KeywordType.Science);
        Gender = GenderType.Male;
        StartLocation = "High School";
        FlavorText = "Now thats's what I call Science!";
        Health = 3;
    }
}