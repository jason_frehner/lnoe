﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Analytics;

/// <summary>
/// The Heroes Class.
/// </summary>
public class Heroes
{
	private bool IsDebugLog()
	{
		return PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
	}
	
	private readonly string[] heroTargetPriority = 
	{
		Strings.HeroNames.Billy,
		Strings.HeroNames.Jenny,
		Strings.HeroNames.Johnny,
		Strings.HeroNames.Sally,
		Strings.HeroNames.Becky,
		Strings.HeroNames.FatherJoseph,
		Strings.HeroNames.JakeCartwright,
		Strings.HeroNames.SheriffAnderson
	};
	
	private readonly List<Hero> coreHeroes = new List<Hero>
	{
		new Becky(),
		new Billy(),
		new FatherJoseph(),
		new JakeCartwright(),
		new Jenny(),
		new Johnny(),
		new Sally(),
		new SheriffAnderson()
	};
	private readonly List<Hero> growingHungerHeroes = new List<Hero>
	{
		new Amanda(),
		new Kenny(),
		new Sam(),
		new Rachelle()
	};
	private readonly List<Hero> heroPackOneHeroes = new List<Hero>
	{
		new Jade(),
		new MrGoddard(),
		new Stacy(),
		new Victor()
	};
	private readonly List<Hero> timberPeakHeroes = new List<Hero>
	{
		new Alice(),
		new EdBaker(),
		new JakeCartwrightSurvivor(),
		new Nikki(),
		new SallySurvivor(),
		new SheriffAndersonSurvivor()
	};
	private readonly List<Hero> bloodInTheForestHeroes = new List<Hero>
	{
		new AgentCarter(),
		new SisterOphelia()
	};
	private readonly List<Hero> tenthAnniversaryEditionHeroes = new List<Hero>
	{
		// With core heroes
		new DeputyTaylor(),
		new DocBrody(),
		new Jeb(),
		new MrHyde()
	};
	private readonly List<Hero> heroPackTwoHeroes = new List<Hero>
	{
		new Angela(),
		new Bear(),
		new DrYamato(),
		new Maria()
	};
	
	private List<Hero> selectableHeroes = new List<Hero>();

	public List<Hero> GameHeroes = new List<Hero>();

	/// <summary>
	/// Selects the Heroes for the game.
	/// </summary>
	public void HeroesInTheGame()
	{
		// TODO When player has a different core set.
		selectableHeroes = coreHeroes;
		if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.GH) == 1)
			selectableHeroes.AddRange(growingHungerHeroes);
		if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.HP1) == 1)
			selectableHeroes.AddRange(heroPackOneHeroes);
		if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.TP) == 1)
			selectableHeroes.AddRange(timberPeakHeroes);
		if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.BITF) == 1)
			selectableHeroes.AddRange(bloodInTheForestHeroes);
		if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.AE) == 1)
			selectableHeroes.AddRange(tenthAnniversaryEditionHeroes);
		if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.HP2) == 1)
			selectableHeroes.AddRange(heroPackTwoHeroes);
		

		//TODO breaks new games
		if (PlayerPrefs.GetString(Strings.PlayerPrefKeys.GameType) == Strings.PlayerPrefKeys.GameTypeCampaign)
		{
			string[] deadCampaignHeroes = CampaignManager.GetlistOfDeadHeroes().Split('\n');
			foreach (string deadHero in deadCampaignHeroes)
			{
				if (deadHero == "") continue;
				Hero heroToRemove = selectableHeroes.Find(h => h.Name.Contains(deadHero));
				selectableHeroes.Remove(heroToRemove);
			}
		}

		GameHeroes = new List<Hero>();
		string[] ppHeroKey = {"hero1", "hero2", "hero3", "hero4"};
		const int heroesToAdd = 4;
		
		for (int i = 0; i < 4; i++)
		{
			if(IsDebugLog())
			{
				Debug.Log("<color=yellow>Getting Heroes</color>");
				Debug.Log(PlayerPrefs.GetString(ppHeroKey[i]));
			}
			
			if (PlayerPrefs.GetString(ppHeroKey[i]) == "Random") continue;
			
			Hero namedHero = selectableHeroes.Find(h => h.Name.Contains(PlayerPrefs.GetString(ppHeroKey[i])));
			GameHeroes.Add(namedHero);
			selectableHeroes.Remove(namedHero);
		}

		while (GameHeroes.Count < heroesToAdd)
		{
			AddARandomHeroToGame();
		}
		
		// Save heroes to player prefs
		for(int i = 0; i < 4; i++)
		{
			PlayerPrefs.SetString(ppHeroKey[i], GameHeroes[i].Name);
		}

		Analytics.CustomEvent("startingHeroes", new Dictionary<string, object>
		{
			{"hero1", GameHeroes[0].Name},
			{"hero2", GameHeroes[1].Name},
			{"hero3", GameHeroes[2].Name},
			{"hero4", GameHeroes[3].Name}
		});
	}

	/// <summary>
	/// Get how many heroes are selectable
	/// </summary>
	/// <returns>The number</returns>
	public int NumberOfHeroesLeftInPool()
	{
		return selectableHeroes.Count;
	}

	/// <summary>
	/// Get the name of a random Hero from the Hero pool.
	/// </summary>
	/// <returns>Name of Hero.</returns>
	public string GetNameOfRandomHeroFromPool()
	{
		int x = Random.Range(0, selectableHeroes.Count);
		return selectableHeroes[x].Name;
	}

	/// <summary>
	/// Get the Hero from the selection pool by name.
	/// </summary>
	/// <param name="name">Name of hero.</param>
	/// <returns>The Hero.</returns>
	public Hero GetHeroFromPool(string name)
	{
		foreach(Hero hero in selectableHeroes)
		{
			if(hero.Name == name)
			{
				return hero;
			}
		}

		return null;
	}

	/// <summary>
	/// Removes a Hero from the pool by its name.
	/// </summary>
	/// <param name="name">Name of Hero</param>
	public void RemoveHeroFromHeroPool(string name)
	{
		foreach(Hero hero in selectableHeroes)
		{
			if(hero.Name == name)
			{
				selectableHeroes.Remove(hero);
				return;
			}
		}
	}

	/// <summary>
	/// Add one Hero to the game.
	/// </summary>
	public string AddARandomHeroToGame()
	{
		int x = Random.Range(0, selectableHeroes.Count);
		GameHeroes.Add(selectableHeroes[x]);
		selectableHeroes.RemoveAt(x);

		return GameHeroes[GameHeroes.Count - 1].Name;
	}

	/// <summary>
	/// Remove hero from game.
	/// </summary>
	/// <param name="deadHeroName">Name of the Hero that died.</param>
	/// <returns>Name of the new Hero</returns>
	public void HeroDiedRemove(string deadHeroName)
	{
		// Remove the hero from the game.
		foreach (Hero hero in GameHeroes)
		{
			if (hero.Name != deadHeroName) continue;
			GameHeroes.Remove(hero);
			break;
		}
		
		if (PlayerPrefs.GetString(Strings.PlayerPrefKeys.GameType) == Strings.PlayerPrefKeys.GameTypeCampaign)
		{
			CampaignManager.HeroDeadInCampaign(deadHeroName);
		}
		
		Analytics.CustomEvent("heroDied", new Dictionary<string, object>
		{
			{"deadHero", deadHeroName}
		});
	}

	/// <summary>
	/// Gets a string of the heroes in the game.
	/// </summary>
	/// <returns>The heroes.</returns>
	public string GetListOfHeroesInGame()
	{
		string heroes = GameHeroes.Aggregate("", (current, hero) => current + hero.Name + ",");
		heroes = heroes.TrimEnd(',');
		return heroes;
	}

	/// <summary>
	/// Goes through the hero priority list until it finds one in the game.
	/// </summary>
	/// <returns>The priority target.</returns>
	public string GetPriorityHero()
	{
		foreach (string s in heroTargetPriority)
		{
			if (GetListOfHeroesInGame().Contains(s))
			{
				return s;
			}
		}
		return RandomHero();
	}

	/// <summary>
	/// Gets the name of a random hero in the game.
	/// </summary>
	/// <returns>The name.</returns>
	public string RandomHero()
	{
		int randomIndex = Random.Range(0, GameHeroes.Count);
		return GameHeroes[randomIndex].Name;
	}

	/// <summary>
	/// Gets the name of a random hero in the game that has given keyword.
	/// </summary>
	/// <param name="keyword"></param>
	/// <returns>The name.</returns>
	public string RandomHeroWithKeyword(Hero.KeywordType keyword)
	{
		List<Hero> heroesToPickFrom = new List<Hero>();

		foreach (Hero hero in GameHeroes)
		{
			heroesToPickFrom.AddRange(from type in hero.Keyword where type == keyword select hero);
		}

		if (heroesToPickFrom.Count <= 0) return null;
		int randomIndex = Random.Range(0, heroesToPickFrom.Count);
		return heroesToPickFrom[randomIndex].Name;

	}

	/// <summary>
	/// Gets the name of a random hero that does NOT have a given keyword.
	/// </summary>
	/// <param name="keyword"></param>
	/// <returns>The name.</returns>
	public string RandomHeroWithoutKeyword(Hero.KeywordType keyword)
	{
		List<Hero> heroesToPickFrom = new List<Hero>();

		foreach (Hero hero in GameHeroes)
		{
			bool hasKeyword = false;
			
			foreach (Hero.KeywordType type in hero.Keyword)
			{
				if (type == keyword)
				{
					hasKeyword = true;
				}
			}

			if (!hasKeyword)
			{
				heroesToPickFrom.Add(hero);
			}
		}

		if (heroesToPickFrom.Count > 0)
		{
			int randomIndex = Random.Range(0, heroesToPickFrom.Count);
			return heroesToPickFrom[randomIndex].Name;
		}
		return null;
	}

	/// <summary>
	/// Get the hero object that matches this name.
	/// </summary>
	/// <param name="name">The name.</param>
	/// <returns></returns>
	public Hero GetGameHeroByName(string name)
	{
		foreach (Hero hero in GameHeroes)
		{
			if (hero.Name == name)
			{
				return hero;
			}
		}
		return null;
	}
	
	public Hero GetSelectionHeroByName(string name)
	{
		foreach (Hero hero in selectableHeroes)
		{
			if (hero.Name == name)
			{
				return hero;
			}
		}
		return null;
	}
}