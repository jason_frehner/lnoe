using System.Collections.Generic;

/// <summary>
/// A Hero Class.
/// </summary>
public class Hero
{
    protected enum GenderType
    {
        Male, Female
    }
    public enum KeywordType
    {
        Criminal, Drifter, Holy, LawEnforcement, Medical, Military, 
        Outlaw, Pilot, Science, Strange, Student, Teacher, Tech
    }
    public string Name;
    // ReSharper disable once NotAccessedField.Global
    protected string Title;
    public readonly List<KeywordType> Keyword;
    // ReSharper disable once NotAccessedField.Global
    protected GenderType Gender;
    public string StartLocation;
    // ReSharper disable once NotAccessedField.Global
    protected string FlavorText;
    public int Health;
    // ReSharper disable once NotAccessedField.Global
    protected string StartItem;

    protected Hero()
    {
        Keyword = new List<KeywordType>();
    }
}