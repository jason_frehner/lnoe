﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

//Information pulled from http://flyingfrogwiki.com/

public class HeroCards : MonoBehaviour
{
    private readonly List<HeroCard> heroCards = new List<HeroCard>();
    
    private readonly BasicHeroCards basicHeroCards = new BasicHeroCards();
    private readonly AdvancedHeroCards advancedHeroCards = new AdvancedHeroCards();

    public Text Title;
    public Text CardText;
    public GameObject TypeText;

    public GameObject TypeTextList;


    private void Start()
    {
        heroCards.AddRange(basicHeroCards.BasicCards);
        heroCards.AddRange(advancedHeroCards.AdvancedCards);
    }

    public void SetCardARInfo(string cardName)
    {
        HeroCard card = new HeroCard();
        foreach(HeroCard heroCard in heroCards)
        {
            if(heroCard.ReferenceName == cardName)
            {
                card = heroCard;
            }
        }
        
        //Clear previous text
        for(int i = 0; i < TypeTextList.transform.childCount; i++)
        {
            Destroy(TypeTextList.transform.GetChild(i).gameObject);
        }

        if(card.ReferenceName != null)
        {
            Title.text = card.Name;
//            cardARText.Replace("TITLE", card.Name);
            
            if(card.CardTypes.Contains(HeroCard.HeroCardTypes.Item))
            {
                GameObject title = Instantiate(TypeText);
                title.GetComponent<Text>().text = "Item";
                title.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
                title.transform.SetParent(TypeTextList.transform);
                GameObject info = Instantiate(TypeText);
                info.GetComponent<Text>().text = "A Hero may only carry up to 4 Items at a time. " +
                                "A Hero may NOT discard Items unless they have to. " +
                                "A single copy of a Hero Item card may NOT be used by more than one Hero in the same Turn. ";
                info.transform.SetParent(TypeTextList.transform);
            }
            if(card.CardTypes.Contains(HeroCard.HeroCardTypes.BreakTest))
            {
                GameObject title = Instantiate(TypeText);
                title.GetComponent<Text>().text = "Break Test";
                title.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
                title.transform.SetParent(TypeTextList.transform);
                GameObject info = Instantiate(TypeText);
                info.GetComponent<Text>().text = "A separate die roll needed for many Hero Hand Weapons to see if they are discarded " +
                                "after each use. Referred to on cards as ‘Breaks on X’. ";
                info.transform.SetParent(TypeTextList.transform);
            }
            if(card.CardTypes.Contains(HeroCard.HeroCardTypes.Event))
            {
                GameObject title = Instantiate(TypeText);
                title.GetComponent<Text>().text = "Event";
                title.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
                title.transform.SetParent(TypeTextList.transform);
                GameObject info = Instantiate(TypeText);
                info.GetComponent<Text>().text = "There is no limit on the number of 'Event' cards a Hero Player may have in hand. " +
                                "Event cards be played at any time, unless it says otherwise on the card. ";
                info.transform.SetParent(TypeTextList.transform);
            }
            if(card.CardTypes.Contains(HeroCard.HeroCardTypes.Weapon))
            {
                GameObject title = Instantiate(TypeText);
                title.GetComponent<Text>().text = "Weapon";
                title.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
                title.transform.SetParent(TypeTextList.transform);
                GameObject info = Instantiate(TypeText);
                info.GetComponent<Text>().text = "Only 2 of the 4 Items may be Weapons. ";
                info.transform.SetParent(TypeTextList.transform);
            }
            
            // reset scale and rotation
            for(int i = 0; i < TypeTextList.transform.childCount; i++)
            {
                TypeTextList.transform.GetChild(i).localScale = Vector3.one;
                TypeTextList.transform.GetChild(i).localEulerAngles = Vector3.zero;
            }

            CardText.text = card.Errata ?? "No additional information.";
        }
    }
}