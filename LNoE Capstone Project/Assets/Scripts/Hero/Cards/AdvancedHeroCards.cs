using System.Collections.Generic;

public class AdvancedHeroCards
{
    private static readonly HeroCard BlockedWindows = new HeroCard(
        "BlockedWindows", "Blocked Windows",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event
        },
        "Blocked Windows only prevents Zombies from moving through walls, it doesn't prevent them from spawning " +
        "inside or being placed in your space." +
        "If a Zombie can't reach an adjacent Hero for some reason (say Blocked Windows is in play), the Zombie may " +
        "move normally (ignoring the Zombie Hunger caused by that unreachable Hero).");
    
    private static readonly HeroCard Dynamite = new HeroCard(
        "Dynamite", "Dynamite",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.Explosive
        },
        null);
    
    private static readonly HeroCard EscapeThroughTheWindows = new HeroCard(
        "EscapeWindow", "Escape Through the Windows",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event
        },
        null);
    
    private static readonly HeroCard FireExtinguisher = new HeroCard(
        "FireExtinguisher", "Fire Extinguisher",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item
        },
        "The second half of the card should read, “Discard at any time (except during a Fight) to move every Zombie...”." +
        "With Fire Extinguisher, you are allowed to move any and all Zombies in your space as well as all adjacent spaces." +
        "Only Zombies are affected by the Fire Extinguisher (Heroes and Old Betsy just stay where they are). Also " +
        "remember that moving Zombies with the Fire Extinguisher is optional for the Hero on an individual basis " +
        "(you could choose to NOT move the ones out of Old Betsy's space if you wanted to keep them pinned down).");
    
    private static readonly HeroCard Gasoline = new HeroCard(
        "Gasoline", "Gasoline",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.Explosive
        },
        "When a Gas Marker explodes, it affects every adjacent space, regardless of walls.");
    
    private static readonly HeroCard HeroicResolve = new HeroCard(
        "HeroicResolve", "Heroic Resolve",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event,
            HeroCard.HeroCardTypes.Heroic
        },
        null);
    
    private static readonly HeroCard HerosRage = new HeroCard(
        "HerosRage", "Hero's Rage",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event,
            HeroCard.HeroCardTypes.Heroic
        },
        "Heroic Resolve is a 'Remains in Play' card which means that when you play it, instead of discarding it like " +
        "most Event cards, it stays in play on the Hero you played it on and continues to take affect. The card is " +
        "only discarded if that Hero searches. In this way, the card may not affect more than one Hero (the Hero it " +
        "was originally played on).");
    
    private static readonly HeroCard JustWhatINeeded = new HeroCard(
        "JustWhatINeeded", "Just What I Needed",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event
        },
        "The first half of the card should read, “Take any non-‘Play Immediately’ card from the Hero Cards discard " +
        "pile. If there are none, you may draw a new Hero Card.”");
    
    private static readonly HeroCard Lighter = new HeroCard(
        "Lighter", "Lighter",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.Fire
        },
        "Like any Fire Item, discard as a Ranged Attack to automatically start a Fire in an adjacent space.");
    
    private static readonly HeroCard OldBetsy = new HeroCard(
        "OldBetsy", "Old Betsy",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.Animal
        },
        "If you have four items including Gasoline or Old Betsy, and you gain a fifth item, can you immediately use " +
        "the Gasoline or Old Betsy to avoid having to discard an item? Yes. You could also immediately hand off an " +
        "item to any other Hero in your space.");
    
    public readonly List<HeroCard> AdvancedCards = new List<HeroCard>
    {
        BlockedWindows, Dynamite, EscapeThroughTheWindows, FireExtinguisher, Gasoline, HeroicResolve, HerosRage,
        Lighter, JustWhatINeeded, OldBetsy
    };
}