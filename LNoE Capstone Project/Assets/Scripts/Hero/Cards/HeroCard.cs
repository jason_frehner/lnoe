using System.Collections.Generic;

public class HeroCard
{
    public enum HeroCardTypes
    {
        Ammo, Animal, BreakTest, Event, Explosive, Fire, FirstAid, Flare, Gun, HandWeapon, Heroic, Item,
        RangedWeapon, Townsfolk, Weapon
    }

    public readonly string ReferenceName;
    public readonly string Name;
    public readonly List<HeroCardTypes> CardTypes;
    public readonly string Errata;
    
    public HeroCard()
    {}

    public HeroCard(string referenceName, string name, List<HeroCardTypes> cardTypes, string errata)
    {
        ReferenceName = referenceName;
        Name = name;
        CardTypes = cardTypes;
        Errata = errata;
    }
}