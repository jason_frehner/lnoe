using System.Collections.Generic;

public class BasicHeroCards
{
    private static readonly HeroCard Ammo = new HeroCard(
        "Ammo", "Ammo",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.Ammo
        },
        null);
    
    private static readonly HeroCard AtLast = new HeroCard(
        "AtLast", "At Last...",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event
        },
        null);
    private static readonly HeroCard BaseballBat = new HeroCard(
        "BaseballBat", "Baseball Bat",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.HandWeapon,
            HeroCard.HeroCardTypes.BreakTest,
            HeroCard.HeroCardTypes.Weapon
        },
        null);
    private static readonly HeroCard Chainsaw = new HeroCard(
        "Chainsaw", "Chainsaw",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.HandWeapon,
            HeroCard.HeroCardTypes.Weapon
        },
        null);
    private static readonly HeroCard Crowbar = new HeroCard(
        "Crowbar", "Crowbar",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.HandWeapon,
            HeroCard.HeroCardTypes.BreakTest,
            HeroCard.HeroCardTypes.Weapon
        },
        null);
    private static readonly HeroCard DeputyTaylor = new HeroCard(
        "DeputyTaylor", "Deputy Taylor",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event,
            HeroCard.HeroCardTypes.Townsfolk
        },
        null);
    private static readonly HeroCard DocBrody = new HeroCard(
        "DocBrody", "Doc Brody, County Physician",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event,
            HeroCard.HeroCardTypes.Townsfolk
        },
        null);
    private static readonly HeroCard Faith = new HeroCard(
        "Faith", "Faith",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event
        },
        null);
    private static readonly HeroCard FarmerSty = new HeroCard(
        "FarmerSty", "Farmer Sty",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event,
            HeroCard.HeroCardTypes.Townsfolk
        },
        null);
    private static readonly HeroCard FireAxe = new HeroCard(
        "FireAxe", "Fire Axe",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.HandWeapon,
            HeroCard.HeroCardTypes.BreakTest,
            HeroCard.HeroCardTypes.Weapon
        },
        null);
    private static readonly HeroCard FirstAid = new HeroCard(
        "FirstAid", "First Aid",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.FirstAid
        },
        "It does not end your movement, the First Aid Kit can be used any time 'except during a Fight'." +
        "It can be used on the Hero who has it.");
    private static readonly HeroCard GetBackYouDevils = new HeroCard(
        "GetBackYouDevils", "Get Back You Devils",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event
        },
        null);
    private static readonly HeroCard Jeb = new HeroCard(
        "Jeb", "Jeb, The Grease Monkey",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event,
            HeroCard.HeroCardTypes.Townsfolk
        },
        "This can be used to cancel any Zombie Card, being played or that Remains in Play.");
    private static readonly HeroCard JustAScratch = new HeroCard(
        "JustAScratch", "Just a Scratch",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event
        },
        null);
    private static readonly HeroCard Keys = new HeroCard(
        "Keys", "Keys",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item
        },
        null);
    private static readonly HeroCard MeatCleaver = new HeroCard(
        "MeatCleaver", "Meat Cleaver",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.HandWeapon,
            HeroCard.HeroCardTypes.Weapon
        },
        "With the Meat Cleaver’s Combat Bonus, if a Hero rolls a 6 on any of their Fight Dice, the Zombie they are " +
        "fighting is instantly Killed and the Fight ends immediately. Because it is ‘instant’, Zombies do NOT even get " +
        "the chance to play cards (may not force a re-roll etc). It is also important to note that the Fight is NOT " +
        "Resolved (the Hero is not hurt, even if the Zombie would normally have won the Fight). The roll MAY be " +
        "modified in order to get a 6 (adding extra dice, Re-rolling, adding 1 to a roll to change a 5 result into a " +
        "6), but once a 6 is rolled, the Hero may instantly act on it to use the Meat Cleaver’s Combat Bonus. If a " +
        "Hero decides not to use the Combat Bonus to ‘instantly’ Kill (say they do not want to Kill the Zombie for " +
        "some reason), the opportunity is wasted and the Meat Cleaver may NOT be used in conjunction with that 6 roll " +
        "for the rest of the Fight.");
    private static readonly HeroCard MrHyde = new HeroCard(
        "MrHyde", "Mr. Hyde, The Shop Teacher",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event,
            HeroCard.HeroCardTypes.Townsfolk
        },
        null);
    private static readonly HeroCard Pitchfork = new HeroCard(
        "Pitchfork", "Pitchfork",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.HandWeapon,
            HeroCard.HeroCardTypes.BreakTest,
            HeroCard.HeroCardTypes.Weapon
        },
        null);
    private static readonly HeroCard PrincipalGomez = new HeroCard(
        "PrincipalGomez", "Principal Gomez",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event,
            HeroCard.HeroCardTypes.Townsfolk
        },
        "This can be used to cancel any Zombie Card, being played or that Remains in Play.");
    private static readonly HeroCard PumpShotgun = new HeroCard(
        "PumpShotgun", "Pump Shotgun",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.RangedWeapon,
            HeroCard.HeroCardTypes.Gun,
            HeroCard.HeroCardTypes.Weapon
        },
        "You choose a target space and roll separately to hit each Zombie in that space. Once you have finished " +
        "rolling to hit the Zombies, then you make a single additional roll to see if the gun runs out of ammo on the " +
        "roll of 1 or 2. This extra ‘ammo roll’ is to prevent the Pump Shotgun from running out of ammo too quickly.");
    private static readonly HeroCard Recovery = new HeroCard(
        "Recovery", "Recovery",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Event,
            HeroCard.HeroCardTypes.FirstAid
        },
        null);
    private static readonly HeroCard Revolver = new HeroCard(
        "Revolver", "Revolver",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.RangedWeapon,
            HeroCard.HeroCardTypes.Gun,
            HeroCard.HeroCardTypes.Weapon
        },
        "With a Revolver, it is a single roll to hit. If that roll is a 1, the gun not only misses but is also out " +
        "of ammo (discarded).");
    private static readonly HeroCard SignalFlare = new HeroCard(
        "SignalFlare", "Signal Flare",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.RangedWeapon,
            HeroCard.HeroCardTypes.Flare,
            HeroCard.HeroCardTypes.Weapon
        },
        null);
    private static readonly HeroCard Torch = new HeroCard(
        "Torch", "Torch",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.Fire
        },
        null);
    private static readonly HeroCard WeldingTorch = new HeroCard(
        "WeldingTorch", "WeldingTorch",
        new List<HeroCard.HeroCardTypes>
        {
            HeroCard.HeroCardTypes.Item,
            HeroCard.HeroCardTypes.HandWeapon,
            HeroCard.HeroCardTypes.Fire,
            HeroCard.HeroCardTypes.BreakTest,
            HeroCard.HeroCardTypes.Weapon
        },
        null);
    
    public readonly List<HeroCard> BasicCards = new List<HeroCard>
    {
        Ammo, AtLast, BaseballBat, Chainsaw, Crowbar, DeputyTaylor, DocBrody, Faith, FarmerSty, FireAxe, FirstAid,
        GetBackYouDevils, Jeb, JustAScratch, Keys, MeatCleaver, MrHyde, Pitchfork, PrincipalGomez, PumpShotgun,
        Recovery, Revolver, SignalFlare, Torch, WeldingTorch
    };
}