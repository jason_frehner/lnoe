﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

/// <inheritdoc />
/// <summary>
/// Get and display the information when a hero dies.
/// </summary>
public class HeroDeadScreen : MonoBehaviour
{
	public GameObject Option1;
	public GameObject Option2;
	public GameObject Option3;
	public GameObject Option4;

	public Text TextInstructions;

	public GameObject WhatHeroPanel;
	public GameObject LastHeroPanel;
	public GameObject ContinuePanel;

	private ZombieTurn zombieTurn;
	private List<Hero> heroesInGame = new List<Hero>();
	private string deadHero;
	
	private AudioSource clickAudio;

	private void Start()
	{
		zombieTurn = GameObject.Find("ZombieManager").GetComponent<ZombieTurn>();
		clickAudio = GameObject.Find(Strings.GameObjectNames.AudioClick).GetComponent<AudioSource>();
	}

	/// <summary>
	/// Ask what hero died and give options.
	/// </summary>
	/// <param name="heroes">The heroes in the game.</param>
	public void SetOptions(List<Hero> heroes)
	{
		heroesInGame = heroes;
		
		Option1.transform.Find(Strings.GameObjectNames.Text).GetComponent<Text>().text = heroes[0].Name;
		Option2.transform.Find(Strings.GameObjectNames.Text).GetComponent<Text>().text = heroes[1].Name;
		if (heroes.Count > 2)
		{
			Option3.transform.Find(Strings.GameObjectNames.Text).GetComponent<Text>().text = heroes[2].Name;
		}
		else
		{
			Option3.SetActive(false);
		}
		if (heroes.Count > 3)
		{
			Option4.transform.Find(Strings.GameObjectNames.Text).GetComponent<Text>().text = heroes[3].Name;
		}
		else
		{
			Option4.SetActive(false);
		}
		

		TextInstructions.text = "Which Hero died?";
	}

	/// <summary>
	/// Ask if the players last hero.
	/// </summary>
	private void LastHeroForPlayer()
	{
		TextInstructions.text = "Is this the players last Hero?";
	}

	/// <summary>
	/// Make hero a zombie and get player a new hero.
	/// </summary>
	private void ZombieHeroNewHero()
	{
		if (zombieTurn.GetGameStats().GetHeroCharacters().NumberOfHeroesLeftInPool() == 0)
		{
			HeroRemoveFromGame();
			return;
		}
		
		string newHero = zombieTurn.GetGameStats().GetHeroCharacters().AddARandomHeroToGame();

		StringBuilder instructions = new StringBuilder(
			"DEADHERO is now a Zombie Hero, place a Zombie Hero marker under the figure.\n" +
			"The player brings NEWHERO into play under their control. Discard all of DEADHERO item cards, event cards " +
			"remain in the player's hand.");
		instructions.Replace("DEADHERO", deadHero);
		instructions.Replace("NEWHERO", newHero);

		TextInstructions.text = instructions.ToString();
		
		// Make dead Here a Zombie Hero
		// Grab the hero that died
		Hero zombieHero = zombieTurn.GetGameStats().GetHeroCharacters().GetGameHeroByName(deadHero);
		// Create a zombie hero from that hero
		zombieTurn.GetGameStats().GetZombieTypes().NewZombieHero(zombieHero.Name, zombieHero.Health);
		// Remove the hero from the game.
		KillTheHero();
	}

	/// <summary>
	/// Remove the hero from the game.
	/// </summary>
	private void HeroRemoveFromGame()
	{
		StringBuilder instructions = new StringBuilder(
			"Remove DEADHERO from the game, discard their item cards. Event cards remain in the player's hand.");
		instructions.Replace("DEADHERO", deadHero);
		
		TextInstructions.text = instructions.ToString();
		KillTheHero();
	}

	/// <summary>
	/// Kill The Hero
	/// </summary>
	private void KillTheHero()
	{
		zombieTurn.GetGameStats().GetHeroCharacters().HeroDiedRemove(deadHero);
		zombieTurn.IncreaseHeroDeadCount();
		zombieTurn.UpdateUIText();
		zombieTurn.GameLog.AddText("<color=red>" + deadHero + " DEAD!</color>");
		zombieTurn.SetObjective("Kill Heroes");

		if(PlayerPrefs.GetString(Strings.PlayerPrefKeys.GameType) == Strings.PlayerPrefKeys.GameTypeCampaign)
		{
			zombieTurn.GameOver(Strings.Zombies);
		}
	}

	/// <summary>
	/// Which Hero died.
	/// </summary>
	/// <param name="text"></param>
	[UsedImplicitly]
	public void DeadHeroPickedFromButton(Text text)
	{
		deadHero = text.text.Trim();
		deadHero = Regex.Replace(deadHero, "\n", " ");

		if (PlayerPrefs.GetInt("numPlayers") == 4 || 
		    heroesInGame.Count == 2 || 
		    PlayerPrefs.GetInt(Strings.PlayerPrefKeys.HeroReplenish) == 1)
		{
			// Hero is a zombie
			ZombieHeroNewHero();
			WhatHeroPanel.SetActive(false);
			ContinuePanel.SetActive(true);
		}
		else
		{
			// Ask if players last hero
			LastHeroForPlayer();
			WhatHeroPanel.SetActive(false);
			LastHeroPanel.SetActive(true);
		}
		
		// Check if Growing Hunger card is in play, and remove it.
		zombieTurn.GetDecks().RemoveRemainsInPlayForCardFromName("Growing Hunger");
		
		clickAudio.Play();
	}

	/// <summary>
	/// Gets the players response to if that was the last hero for that player.
	/// </summary>
	/// <param name="isLastHero">The response</param>
	[UsedImplicitly]
	public void PlayerLastHero(bool isLastHero)
	{
		if (isLastHero)
		{
			ZombieHeroNewHero();
			LastHeroPanel.SetActive(false);
			ContinuePanel.SetActive(true);
		}
		else
		{
			HeroRemoveFromGame();
			LastHeroPanel.SetActive(false);
			ContinuePanel.SetActive(true);
		}
		
		clickAudio.Play();
	}
}
