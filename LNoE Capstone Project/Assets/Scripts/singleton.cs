﻿using UnityEngine;

/// <summary>
/// Singleton.
/// Carries object over to next scene and makes Sure this is the only instance of the object.
/// </summary>
public class Singleton : MonoBehaviour {
	/// <summary>
	/// Gets the instance.
	/// </summary>
	/// <value>The instance.</value>
	public static Singleton Instance { get; private set; }

	/// <summary>
	/// Awake this instance.
	/// Check if there is another instance other than this one.
	/// </summary>
	private void Awake() {
		if (Instance != null && Instance != this)
		{
			Destroy(gameObject); //If so destroy it.
			return;
		} else {
			Instance = this; //If not this is the original and label it as "this".
		}

		//Keep object for next scene
		DontDestroyOnLoad(gameObject);
	}
}
