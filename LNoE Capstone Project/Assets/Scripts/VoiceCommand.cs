﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Analytics;
using UnityEngine.Audio;

/// <summary>
/// Voice command.
/// </summary>
public class VoiceCommand : MonoBehaviour
{
	private SpeechRecognizerManager voiceManager;
	private bool isListening;

	//Connect in editor
	public LoadScene loadScene;
	public ZombieTurn zombieTurn;
	public Animator fight;
	public Animator remains;
	public Text text;
	public GameObject voicePanel;
	public AudioMixer Mixer;

	private void Start ()
	{
		// We pass the game object's name that will receive the callback messages.
		voiceManager = new SpeechRecognizerManager (gameObject.name);
	}

	private void OnDestroy ()
	{
		if (voiceManager != null)
			voiceManager.Release ();
	}

	#region SPEECH_CALLBACKS

	private void OnSpeechEvent (string e)
	{
		switch (int.Parse (e)) {
		case SpeechRecognizerManager.EVENT_SPEECH_READY:
			StatusMessage ("Ready for speech");
			break;
		case SpeechRecognizerManager.EVENT_SPEECH_BEGINNING:
			StatusMessage ("Started speaking");
			break;
		case SpeechRecognizerManager.EVENT_SPEECH_END:
			StatusMessage ("Stopped speaking");
			break;
		}
	}

	private void OnSpeechResults (string results)
	{
		isListening = false;

		StatusMessage (results);

		#region speech commands
		switch (results.ToLower ()) {
		case "zombie killed":
		case "killed zombie":
		case "zombie died":
		case "zombie dead":
			zombieTurn.ZombieDied (null);
			voicePanel.SetActive (false);
			break;
		case "fight zombie":
		case "zombie fight":
			fight.SetTrigger ("open");
			voicePanel.SetActive (false);
			break;
		case "hero killed":
		case "hero dead":
			zombieTurn.HeroDied ();
			voicePanel.SetActive (false);
			break;
		case "zombie turn":
			zombieTurn.Phases.PhaseStartOfTurn ();
			voicePanel.SetActive (false);
			break;
		case "remains in play":
			remains.SetTrigger ("open");
			voicePanel.SetActive (false);
			break;
		case "main menu":
			loadScene.GoToSceneWithLoadingScreen("Main");
			voicePanel.SetActive (false);
			break;
		}

		Analytics.CustomEvent("spokenCommand", new Dictionary<string, object>
		{
			{"command", results}
		});

		#endregion
	}

	private void OnSpeechError (string error)
	{
		switch (int.Parse (error)) {
		case SpeechRecognizerManager.ERROR_AUDIO:
			StatusMessage ("Error during recording the audio.");
			break;
		case SpeechRecognizerManager.ERROR_CLIENT:
			StatusMessage ("Error on the client side.");
			break;
		case SpeechRecognizerManager.ERROR_INSUFFICIENT_PERMISSIONS:
			StatusMessage ("Insufficient permissions. Do the RECORD_AUDIO and INTERNET permissions have been added to the manifest?");
			break;
		case SpeechRecognizerManager.ERROR_NETWORK:
			StatusMessage ("A network error occurred. Make sure the device has internet access.");
			break;
		case SpeechRecognizerManager.ERROR_NETWORK_TIMEOUT:
			StatusMessage ("A network timeout occurred. Make sure the device has internet access.");
			break;
		case SpeechRecognizerManager.ERROR_NO_MATCH:
			StatusMessage ("No recognition result matched.");
			break;
		case SpeechRecognizerManager.ERROR_NOT_INITIALIZED:
			StatusMessage ("Speech recognizer is not initialized.");
			break;
		case SpeechRecognizerManager.ERROR_RECOGNIZER_BUSY:
			StatusMessage ("Speech recognizer service is busy.");
			break;
		case SpeechRecognizerManager.ERROR_SERVER:
			StatusMessage ("Server sends error status.");
			break;
		case SpeechRecognizerManager.ERROR_SPEECH_TIMEOUT:
			StatusMessage ("No speech input.");
			break;
		}

		isListening = false;
	}

	#endregion

	public void startListening()
	{
		Mixer.SetFloat(Strings.AudioMixerKeys.Master, -80.0f);
		isListening = true;
		voiceManager.StartListening (1, "en-US");
	}

	public void stopListening()
	{
		Mixer.SetFloat(Strings.AudioMixerKeys.Master, 0.0f);
		voiceManager.StopListening ();
		isListening = false;
	}

	public void cancelListening()
	{
		Mixer.SetFloat(Strings.AudioMixerKeys.Master, 0.0f);
		voiceManager.CancelListening ();
		isListening = false;
	}


	private void StatusMessage (string message)
	{
		text.text = message;
	}
}
