﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Holds all the possible missions.
/// </summary>
public class Missions
{
	private Mission activeMission;
	private static bool IsDebugLog()
	{
		return PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
	}
	
//	private readonly Mission tutorialMission = new TutorialMission();
	private readonly List<Mission> coreMissions = new List<Mission>
	{
		new BurnEmOut(),
		new DefendTheManorHouse(),
		new DieZombiesDie(),
		new EscapeInTheTruck(),
		new SaveTheTownsFolk()
	};
	private readonly List<Mission> growingHungerMissions = new List<Mission>
	{
		new BurnItToTheGround(),
		new PlagueCarriers(),
		new ZombieApocalypse()
	};

	private readonly List<Mission> heroPackOneMissions = new List<Mission>
	{
		new HuntForSurvivors()
	};
	private List<Mission> timberPeakMissions;
	private List<Mission> bloodInTheForestMissions;
	private List<Mission> survivalOfTheFittestMissions;
	private List<Mission> graveDeadMissions;
	private List<Mission> supplementMissions;
	private List<Mission> webMissions;
	private List<Mission> customMissions;

	/// <summary>
	/// Select the mission for the game session.
	/// </summary>
	/// <returns>The mission.</returns>
	public Mission GetSessionMission()
	{
//		if (PlayerPrefs.GetString(Strings.PlayerPrefKeys.Scenario) == "Tutorial")
//		{
//			return tutorialMission;
//		}
		
		List<Mission> missionToPickFrom = new List<Mission>();
		missionToPickFrom.AddRange(coreMissions);
		if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.GH) == 1)
		{
			missionToPickFrom.AddRange(growingHungerMissions);
		}

		if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.HP1) == 1)
		{
			missionToPickFrom.AddRange(heroPackOneMissions);
		}

		if (PlayerPrefs.GetString(Strings.PlayerPrefKeys.Scenario) != "Random")
		{
			if(IsDebugLog())
			{
				Debug.Log("Mission preselected: " + PlayerPrefs.GetString(Strings.PlayerPrefKeys.Scenario));
			}
			activeMission = missionToPickFrom.Find(m => m.Name.Contains(PlayerPrefs.GetString(Strings.PlayerPrefKeys.Scenario)));
			return activeMission;
		}
		
		int randomIndex = Random.Range(0, missionToPickFrom.Count);
		activeMission = missionToPickFrom[randomIndex];
		PlayerPrefs.SetString(Strings.PlayerPrefKeys.Scenario, activeMission.Name);
		if(IsDebugLog())
		{
			Debug.Log(activeMission.Name);
		}
		return activeMission;
	}

	public Mission GetMissionFromName(string name)
	{
		List<Mission> allMissions = new List<Mission>();
		allMissions.AddRange(coreMissions);
		allMissions.AddRange(growingHungerMissions);
		allMissions.AddRange(heroPackOneMissions);

		foreach(Mission mission in allMissions)
		{
			if(mission.Name == name)
			{
				return mission;
			}
		}

		return null;
	}
}

/// <summary>
/// Bass class for a mission.
/// </summary>
public class Mission
{
	/// <summary>
	/// The possible hero objectives.
	/// </summary>
	public class ObjectiveTypes
	{
		public readonly string Name;
		public readonly int Value;
		public readonly bool Button;
		public readonly string Audio;

		public ObjectiveTypes(string name, int value, string audio, bool button)
		{
			Name = name;
			Value = value;
			Audio = audio;
			Button = button;
		}
	}
	public readonly ObjectiveTypes BurnDownManor = new ObjectiveTypes("Burn Down Manor", 4, "Explosion", true);
	public readonly ObjectiveTypes Destroy3ZombiePits = new ObjectiveTypes("Destroy Zombie Pits", 3, "Explosion", true);
	public readonly ObjectiveTypes EscapeTruckButton = new ObjectiveTypes("Escape Truck", 2, "Gas Pour", true);
	public readonly ObjectiveTypes Kill7PlagueCarriersButton = new ObjectiveTypes("Kill Plague Carriers", 7, "ZombieDeath", true);
	public readonly ObjectiveTypes Kill15Zombies = new ObjectiveTypes("Kill Zombies", 15, "Null", false); //Sound already plays for zombie death.
	public readonly ObjectiveTypes Save4TownfolkButton = new ObjectiveTypes("Save Townsfolk", 4, "Sigh", true);
	public static readonly ObjectiveTypes Destroy6Buildings = new ObjectiveTypes("Destroy Buildings", 6, "Explosion", true);
	public readonly ObjectiveTypes Kill2Heroes = new ObjectiveTypes("Kill Heroes", 2, "Null", false); //Sound already plays for hero death.
	public readonly ObjectiveTypes Kill4Heroes = new ObjectiveTypes("Kill Heroes", 4, "Null", false); //Sound already plays for hero death.
	public readonly ObjectiveTypes ZombiesInHouse9 = new ObjectiveTypes("Zombies In House", 9, "Door Break", true);
	public readonly ObjectiveTypes HuntForSurvivors = new ObjectiveTypes("Hunt for Survivors", 7, "Null", true); //TODO audio
	protected static ObjectiveTypes Morning(int value)
	{
		return new ObjectiveTypes("Morning", value, "Clock Chime", false);
	}
	// Added for campaign
	protected readonly ObjectiveTypes FindZombieSkull = new ObjectiveTypes("Find Zombie Skull", 2, "Skull", true);
	protected readonly ObjectiveTypes GetSkullToDoctor = new ObjectiveTypes("Get the Skull to the Doctor", 2, "Squish", true);
	protected readonly ObjectiveTypes KillDoctorZombie = new ObjectiveTypes("Kill the Doctor", 8, "ZombieDeath", true);
	

	/// <summary>
	/// The possible scenario search items.
	/// </summary>
	public enum ScenarioSearchItemTypes
	{
		Explosive, Fire, Gasoline, Keys, Townsfolk
	}

	/// <summary>
	/// The possible special rules.
	/// </summary>
	public class SpecialRulesTypes
	{
		// ReSharper disable once NotAccessedField.Local
		public readonly string Name;
		public readonly string SetupText;
		public readonly string PlayerPrefKey;
		public readonly string Reminder;

		public SpecialRulesTypes(string name, string text, string key, string reminder = null)
		{
			Name = name;
			SetupText = text;
			PlayerPrefKey = key;
			Reminder = reminder;
		}

		public bool KeyMatch(string key)
		{
			return PlayerPrefKey == key;
		}
	}
	public static readonly SpecialRulesTypes FreeSearchMarkers = new SpecialRulesTypes(
		"Free Search Markers", 
		"Put a Free Search Marker\nin every building that does\nnot have a Hero in it.",
		Strings.PlayerPrefKeys.FreeSearch);
	public static readonly SpecialRulesTypes HeroesReplenish = new SpecialRulesTypes(
		"Heroes Replenish",
		"When a Hero dies automatically replace it.",
		Strings.PlayerPrefKeys.HeroReplenish);
	public static readonly SpecialRulesTypes HeroStartingCards1 = new SpecialRulesTypes(
		"Heroes Starting Cards 1", 
		"Each Hero starts with\n1 additional Hero card.",
		Strings.PlayerPrefKeys.StartingCards1);
	public static readonly SpecialRulesTypes HeroStartingCards2 = new SpecialRulesTypes(
		"Heroes Starting Cards 2",
		"Each Hero starts with\n2 additional Hero cards.",
		Strings.PlayerPrefKeys.StartingCards2);
	public static readonly SpecialRulesTypes PlagueCarriers = new SpecialRulesTypes(
		"Plague Carriers", 
		"The 7 red Zombie figures represent Plague Carriers.",
		Strings.PlayerPrefKeys.PlagueCarriers);
	public static readonly SpecialRulesTypes WellStockedBuilding = new SpecialRulesTypes(
		"Well Stocked Building", 
		"Heroes may use Pick Up ability on the draw deck.",
		Strings.PlayerPrefKeys.WellStocked,
		"Well Stocked Buildings: Heroes may use Pick Up ability on the draw deck.");
	public static readonly SpecialRulesTypes ZombieAutoSpawn = new SpecialRulesTypes(
		"Zombie Auto Spawn", 
		"Zombies will automatically\nspawn each turn.",
		Strings.PlayerPrefKeys.AutoSpawn);
	public static readonly SpecialRulesTypes ZombieGraveDead = new SpecialRulesTypes(
		"Zombies Grave Dead", 
		"The 7 red Zombie figures represent the Grave Dead Zombies.",
		Strings.PlayerPrefKeys.GraveDead);
	public static readonly SpecialRulesTypes ZombieHorde21 = new SpecialRulesTypes(
		"Zombie Horde 21", 
		"The Zombie pool is increased to 21.",
		Strings.PlayerPrefKeys.Horde21);

	// Mission values
	public string Name;
	public string Text;
	public List<string> StartNarrativeText;
	public List<string> NarrativeText;
	public List<string> EndNarrativeText;
	public int Rounds;
	public readonly List<ObjectiveTypes> HeroObjective;
	public readonly List<ObjectiveTypes> ZombieObjective;
	public readonly List<ScenarioSearchItemTypes> ScenarioSearchItems;
	public readonly List<SpecialRulesTypes> SpecialRules;
	public string MissionMovementTarget = "";
	public string CenterTile = "Open";
	public bool AllHeroStartLocationChanged;
	public bool CenterHeroStartLocationChanged;
	public string HeroStartLocation;
	public string AudioWin;
	public string AudioLose;

	protected Mission()
	{
		HeroObjective = new List<ObjectiveTypes>();
		ZombieObjective = new List<ObjectiveTypes>();
		ScenarioSearchItems = new List<ScenarioSearchItemTypes>();
		SpecialRules = new List<SpecialRulesTypes>();
	}
}

public class TutorialMission : Mission
{
	public TutorialMission()
	{
		Name = "Tutorial";
		Text = "Learn to play with the companion.";
		Rounds = 3;
		HeroObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Kill4Heroes);
	}
}

#region Core

/// <inheritdoc />
/// <summary>
/// Burn Em' Out mission.
/// </summary>
public class BurnEmOut : Mission
{
	public BurnEmOut()
	{
		Name = "Burn 'Em Out!";
//		Text = "The Heroes must find explosives and destroy 3 of the Zombie Spawning Pits by Sundown.\n" +
//		       "To destroy a Zombie Spawning Pit, a Hero must start in the space, give up their turn, and discard an" +
//		       "Explosive. The Hero must survive any Zombie fights in their space before the pit is destroyed.\n" +
//		       "Scenario Search Item:\n" +
//		       "Any Explosive (Such as Gasoline or Dynamite).";
		Text = "The Heroes must destroy 3 of the Zombie Spawning Pits by Sundown.\n" +
		       "See scenario card for details.";
		EndNarrativeText = new List<string>
		{
			"You destroyed the zombie lairs, and the town is safe once again.",
			"The Ghouls grab and rip you to pieces."
		};
		Rounds = 18;
		HeroObjective.Add(Destroy3ZombiePits);
		ZombieObjective.Add(Kill4Heroes);
		ZombieObjective.Add(Morning(Rounds));
		ScenarioSearchItems.Add(ScenarioSearchItemTypes.Explosive);
		AudioWin = "Fire";
		AudioLose = "Horde";
	}
}

/// <inheritdoc />
/// <summary>
/// Defend the Manor House mission.
/// </summary>
public class DefendTheManorHouse : Mission
{
	public DefendTheManorHouse()
	{
		Name = "Defend the Manor House";
//		Text = "Each Hero starts the game with a Hero card in addition to any Hero cards they would normally start " +
//		       "with (for example, if their Start: Location is not on the board).\n" +
//		       "Zombie Players do not need to Roll To Spawn New Zombies. They automatically get new Zombies in Step " +
//		       "6 of each Zombie Turn.\n" +
//		       "The Zombie Players immediately Win if there are ever 9 or more Zombies inside the Manor House at the " +
//		       "same time.\n" +
//		       "Heroes Win by holding off the Zombies until Morning.";
		Text = "Zombies immediately Win if there are ever 9 or more Zombies inside the Manor House at the " +
		       "same time.\n" +
		       "Heroes Win by holding off the Zombies until Morning.\n" + 
				"See scenario card for details.";
		EndNarrativeText = new List<string>
		{
			"You manage to fight off the zombies and barricade the group with in the manor.",
			"You realize the horde of zombies is too much for you to fight off as they brake and enter through the doors and windows."
		};
		Rounds = 17;
		HeroObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Kill4Heroes);
		ZombieObjective.Add(ZombiesInHouse9);
		SpecialRules.Add(HeroStartingCards1);
		SpecialRules.Add(ZombieAutoSpawn);
		MissionMovementTarget = "the Manor House";
		CenterTile = "Manor";
		AudioWin = "Door Shut";
		AudioLose = "Horde";
	}
}

/// <inheritdoc />
/// <summary>
/// Die Zombie Die mission.
/// </summary>
public class DieZombiesDie : Mission
{
	public DieZombiesDie()
	{
		Name = "Die Zombies, Die!";
		Text = "The Heroes win if they kill 15 Zombies by Sundown.\n" +
		       "The Zombies win if the Heroes fail, or by killing 2 Heroes.";
		EndNarrativeText = new List<string>
		{
			"You survive against the living dead.",
			"The dead are too much for you to defend against."
		};
		Rounds = 15;
		HeroObjective.Add(Kill15Zombies);
		ZombieObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Kill2Heroes);
		AudioWin = "Zombie Group Death";
		AudioLose = "Zombie Feeding";
	}
}

/// <inheritdoc />
/// <summary>
/// Escape in the Truck mission.
/// </summary>
public class EscapeInTheTruck : Mission
{
	public EscapeInTheTruck()
	{
		Name = "Escape in the Truck";
//		Text = "Place the Truck marker in the center space of the Center of Town Board.\n" +
//		       "The Heroes must find Gasoline to fill up the Truck.\n" +
//		       "To fill up the Truck, a Hero must start in the space, give up their turn, and discard Gasoline. The " +
//		       "Hero must survive any Zombie fights in their space before the Truck is filled up.\n" +
//		       "To Escape, at least 2 Heroes must end the Hero Turn together at the Truck before Sundown. The Truck " +
//		       "must be filled up and on of the Heroes there must have Keys.";
		Text = "Place the Truck marker in the center space of the Center of Town Board.\n" +
		       "The Heroes must find Gasoline to fill up the Truck.\n" +
		       "At least 2 Heroes must escape in the Truck before Sundown.\n" +
		       "See scenario card for details.";
		EndNarrativeText = new List<string>
		{
			"The engine turns over and you wast no time in speeding out of town.",
			"Getting to the truck becomes the least of your worries as the walking dead gather around you."
		};
		Rounds = 15;
		HeroObjective.Add(EscapeTruckButton);
		ZombieObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Kill4Heroes);
		ScenarioSearchItems.Add(ScenarioSearchItemTypes.Gasoline);
		ScenarioSearchItems.Add(ScenarioSearchItemTypes.Keys);
		MissionMovementTarget = "the Truck";
		AudioWin = "Truck Peel Off";
		AudioLose = "Truck Stall";
	}
}

/// <inheritdoc />
/// <summary>
/// Save the Townsfolk mission.
/// </summary>
public class SaveTheTownsFolk : Mission
{
	public SaveTheTownsFolk()
	{
		Name = "Save the Townsfolk";
//		Text = "The Heroes must rescue (collect without playing) four of the Townsfolk and keep them alive until " +
//		       "Morning. When found, Townsfolk are kept face up on the table.\n" +
//		       "The Zombies win if the Heroes fail, or by killing 2 Heroes.";
		Text = "The Heroes must rescue four of the Townsfolk and keep them alive until Morning.\n" +
		       "The Zombies win if the Heroes fail, or by killing 2 Heroes.\n" +
				"See scenario card for details.";
		EndNarrativeText = new List<string>
		{
			"The townsfolk thank you for saving them from the reanimated corpses.",
			"The town is filled with the sounds of screams as you are unable to save the townsfolk."
		};
		Rounds = 16;
		HeroObjective.Add(Save4TownfolkButton);
		HeroObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Kill2Heroes);
		ScenarioSearchItems.Add(ScenarioSearchItemTypes.Townsfolk);
		AudioWin = "Cheer";
		AudioLose = "Boo";
	}
}

#endregion

#region Growing Hunger

/// <inheritdoc />
/// <summary>
/// Burn it to the Ground mission.
/// </summary>
public class BurnItToTheGround : Mission
{
	public BurnItToTheGround()
	{
		Name = "Burn it to the Ground!";
//		Text = "Zombie Grave Dead, Free Search Markers, Heroes Replenish\n" +
//		       "Use the Manor House board and place a Zombie Spawning Pit in each of the four center spaces.\n" +
//		       "Any Heroes who do not have their Start Location on the board, instead start in a Random Building with" +
//		       "a free Hero Card.\n" +
//		       "Heroes must burn down the Manor House by finding Gasoline to douse the house with and a Fire item to " +
//		       "start the blaze.\n" +
//		       "To douse the house, Heroes must place a Gas Marker on each of the four Spawning Pits in the Manor House." +
//		       "Once all the pits have been doused, a Hero with a Fire Item may start the blaze by starting their turn " +
//		       "in any space of the Manor House and giving up their turn. The Hero must survive any Zombie fights in " +
//		       "their space before the house is burned down.";
		Text = "Place a Zombie Spawning Pit in each of the four center spaces.\n" +
		       "Heroes must burn down the Manor House.\n" +
		       "Zombies win if Heroes fail or by Killing 4 Heroes.\n" +
		       "See scenario card for details.";
		EndNarrativeText = new List<string>
		{
			"You burn down the house and with it kill the swarm of zombies trapped inside.",
			"The zombies escape from the manor and spread out into the town."
		};
		Rounds = 17;
		HeroObjective.Add(BurnDownManor);
		ZombieObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Kill4Heroes);
		SpecialRules.Add(ZombieGraveDead);
		SpecialRules.Add(FreeSearchMarkers);
		SpecialRules.Add(HeroesReplenish);
		ScenarioSearchItems.Add(ScenarioSearchItemTypes.Gasoline);
		ScenarioSearchItems.Add(ScenarioSearchItemTypes.Fire);
		CenterTile = "Manor";
		CenterHeroStartLocationChanged = true;
		HeroStartLocation = "Random Building";
		AudioWin = "Fire";
		AudioLose = "Horde";
	}
}

public class PlagueCarriers : Mission
{
	public PlagueCarriers()
	{
		Name = "Plague Carriers";
//		Text = "Hero Starting Cards (2), Heroes Replenish\n" +
//		       "All Heroes start in the center space of the Center of Town Board in stead of their usual Start Location.\n" +
//		       "Pace the 7 red Zombies in Random Buildings (limit one per building). These are the Plague Carriers.\n" +
//		       "Plague Carriers have 2 Health Boxes, roll 2 fight dice, and any Hero they kill is automatically turned " +
//		       "into a Zombie Hero. Plague Carriers may NOT move other than for Zombie Hunger (They may not be affected " +
//		       "by movement cards such as Relentless Advance, etc.). Unlike normal Zombies Plague Carriers are affected " +
//		       "by Zombie Hunger from 2 spaces away. Plague Carriers are never returned to the Zombie Pool and do not " +
//		       "count towards the number of Zombies on the board for rolling to spawn.\n" +
//		       "The Heroes must kill all 7 Plague Carriers before Sundown.";
		Text = "Place the 7 red zombies in random buildings.\n" +
		       "Heroes must kill 7 Zombie Plague Carries.\n" +
		       "Zombie win if Heroes fail or by killing 4 Heroes." +
		       "See scenario card for details.";
		EndNarrativeText = new List<string>
		{
			"The plague zombies have been killed and the disease contained.",
			"The plague zombies escape along with any hope of containing the disease."
		};
		Rounds = 18;
		HeroObjective.Add(Kill7PlagueCarriersButton);
		ZombieObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Kill4Heroes);
		SpecialRules.Add(HeroStartingCards2);
		SpecialRules.Add(HeroesReplenish);
		SpecialRules.Add(PlagueCarriers);
		AllHeroStartLocationChanged = true;
		HeroStartLocation = "Center";
		AudioWin = "Zombie Group Death";
		AudioLose = "ZombieWin";
	}
}

public class ZombieApocalypse : Mission
{
	public ZombieApocalypse()
	{
		Name = "Zombie Apocalypse";
//		Text = "Zombie Horde (21), Zombies Auto-Spawn, Hero Starting Cards (2), Heroes Replenish, Free Search Markers, " +
//		       "Well-Stocked Buildings\n" +
//		       "Zombies win by Destroying 6 buildings before Morning.\n" +
//		       "To Destroy a building, there must be at least 1 Zombie in every space of that building at the start of " +
//		       "the Zombies Fight Heroes phase with no Heroes inside the building. Remove all Zombies in the building " +
//		       "from the board (return them to the Zombie Pool) and place a face-down Taken Over or Lights Out marker " +
//		       "on the building to show that is have been Destroyed.\n" +
//		       "No models may move into or be paced in a Destroyed building (Heroes or Zombies). Any Zombie Spawning " +
//		       "Pits in the building may no longer be used an any other cards or markers on the building are discarded.\n" +
//		       "Zombies placed during the current turn may not be used to Destroy a building in the Fight Heroes phase " +
//		       "(such as models placed with cards like 'My God, They've Taken the...').";
		Text = "Heroes win by surviving the night.\n" +
		       "Zombies win if they destroy 6 buildings." +
		       "See scenario card for details.";
		EndNarrativeText = new List<string>
		{
			"The Grave dead have been defeated.",
			"The Zombie Apocalypse is upon you, the end is nigh."
		};
		Rounds = 16;
		HeroObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Destroy6Buildings);
		ZombieObjective.Add(Kill4Heroes);
		SpecialRules.Add(ZombieHorde21);
		SpecialRules.Add(ZombieAutoSpawn);
		SpecialRules.Add(HeroStartingCards2);
		SpecialRules.Add(HeroesReplenish);
		SpecialRules.Add(FreeSearchMarkers);
		SpecialRules.Add(WellStockedBuilding);
		MissionMovementTarget = "\nthe closest space of either\nDESTROYBUILDING\nwith no Zombie.";
		AudioWin = "Zombie Group Death";
		AudioLose = "Zombie Feeding";
	}
}

#endregion

#region Hero Pack One

public class HuntForSurvivors : Mission
{
	public HuntForSurvivors()
	{
		Name = "Hunt for Survivors";
//		Text = "Zombie: Zombies Auto-Spawn\n" +
//		       "Hero: Hero Starting Cards (1), Heroes Replenish, Number Counters (1-6)\n`" +
//		       "1) Place the Truck marker at the Center of Town; this is where all of the Heroes start the game. One " +
//		       "Hero also start with the Keys to the Truck (use the small red circle counter to represent the Keys). " +
//		       "The Number Counters represent possible survivors in the town.\n" +
//		       "2) When a Number Counter is revealed by a Hero, roll a D6. If the roll is less than the number on the " +
//		       "counter, the Hero has found a Townsfolk Survivor. Take a Random Townsfolk marker and place it on the " +
//		       "Hero. If the roll is higher than the number on the counter, it is nothing. If the roll is equal to the " +
//		       "number, they have found a Hero and must roll an additional D6. On 1-3 draw and place a Zombie Hero in " +
//		       "the space. One 4-6 draw and place a new Hero character in the space to immediately join the Hero team.\n" +
//		       "3) While a Hero has 1 or more Townsfolk Survivors with them, that Hero rolls an extra Fight Dice. " +
//		       "Townsfolk Survivors and the Keys marks may be Exchanged between Heroes in the same space (they do not " +
//		       "count against a Hero's carrying limit). If a Hero with one or more of these marker is killed, the " +
//		       "markers are left in the space and may be picked up for free by any Hero moving through that space.\n" +
//		       "4) The Heroes must reveal ALL of the Number Counters and then win by ending a Hero Turn with at least " +
//		       "2 Heroes (with the Keys marker) and ALL Townsfolk Survivors at the Truck before Sundown.";
		Text = "Place the Truck marker in the center space of the Center of Town Board.\n" +
		       "See scenario card for more details.";
		Rounds = 16;
		HeroObjective.Add(HuntForSurvivors);
		ZombieObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Kill4Heroes);
		SpecialRules.Add(ZombieAutoSpawn);
		SpecialRules.Add(HeroStartingCards1);
		SpecialRules.Add(HeroesReplenish);
		AllHeroStartLocationChanged = true;
		HeroStartLocation = "Truck";
	}
}

#endregion

#region Survival of the Fittest

public class HunkerDown : Mission
{
	public HunkerDown()
	{
		Name = "Hunker Down";
		Text = "Zombie: Grave Weapons, Zombies Auto-Spawn, Always Zombie Heroes, Taken Over - Choice (2)\n" +
		       "Hero: Survival Decks, Hero Card Pool (4), Barricades, Heroes Replenish\n" +
		       "1) Heroes must fully Barricade 2 Buildings and Hunker Down in both of them for 4 individual Turns over " +
		       "the course of the game to win.\n" +
		       "2) To Hunker Down in a building, it must be fully barricaded and you must have at least 1 Hero and no " +
		       "Zombies inside at the end of the Hero Turn. Use the track below to mark each successful Hero Turn that " +
		       "the Heroes are Hunkered Down in at least 2 buildings.\n" +
		       "3) Zombies win by preventing the Heroes from accomplishing their goals.";
		Rounds = 16;
		//TODO
	}
}

public class RescueMission : Mission
{
	public RescueMission()
	{
		Name = "Rescue Mission";
		Text = "Zombie: Grave Weapons, Zombies Auto-Spawn, Always Zombie Heroes\n" +
		       "Hero: Survival Decks, Barricades, Heroes Replenish, Hero Card Pool (6), Townsfolk Allies (4)\n" +
		       "1) Some Townsfolk are trapped in a building on the far end of town. The Heroes must venture out from " +
		       "their Safe house, rescue the Townsfolk, and bring them back to the Safe house. Roll a Random Building for " +
		       "the trapped Townsfolk (Re-roll if the building contains a Zombie Spawning Pit). THis building is Fully " +
		       "Barricaded and Fully Reinforced on all sides. The Hero Player places the 4 Townsfolk Allies in any " +
		       "space(s) of this building.\n" +
		       "2) Roll a Random Building on the L-shaped board section in the opposite corner of the board from the " +
		       "Townsfolk building (Re-roll if the building contains a Zombie Spawning Pit). This is the Safe house which " +
		       "starts Fully Barricaded an all of the Heroes start in this Building. Also place 3 Zombies Spawning Pits " +
		       "at the Center of Town space. Neither the Townsfolk building nor the Safe house may be Taken Over or have " +
		       "a Zombie Spawning Pit placed inside (Re-roll).\n" +
		       "3) Heroes win by getting at least 3 Townsfolk Allies into the Safe house building, with NO Zombies in the " +
		       "building, before Sundown. Zombies win by killing 2 of the Townsfolk Allies, or by overrunning the " +
		       "Safe house (having at least one Zombie in every space of the Safe house building with no Heroes inside).";
		Rounds = 15;
		//TODO
	}
}

public class SearchForTheTruth : Mission
{
	public SearchForTheTruth()
	{
		Name = "Search for the Truth";
		Text = "Zombie: Grave Weapons, Always Zombie Heroes\n" +
		       "Hero: Survival Decks, Barricades, Heroes Replenish\n" +
		       "1) The Heroes must find 2 Uniques Items that are Keyword Records. The fist Records found will determine " +
		       "what the Heroes need to do to win the game and the second Records found will determine where they need " +
		       "to go to accomplish their goal. This is based on the Letter Code of the Records.\n" +
		       "2) The first Records found determines what the Heroes need to do:\n" +
		       "A) Destroy the Source of the Outbreak - Collect 2 Explosives and get them to the final Location. Once " +
		       "there a Hero must give up a turn to plant each of the explosives. Once both are planted, the Heroes win.\n" +
		       "B) Destroy the Evidence - Collect a Fire Item and get it as well as any 2 Records to a space of the " +
		       "Final Location to destroy the evidence and protect the town.\n" +
		       "C) Confront the Townspeople - Collect 2 Townsfolk Events (without playing them) and bring any 2 Records " +
		       "to the Final Location. Once there, reveal the Townsfolk to confront them and win.\n" +
		       "Any type of card that needs to be collected immediately becomes a Scenario Search Item.\n" +
		       "3) The second Records found determines where the Heroes need to go:\n" +
		       "A) Sty Family Farm - he Final Location is the Farmhouse building.\n" +
		       "B) The Chemical Plant - The Final Location is The Plant building.\n" +
		       "C) Widowrest Hospital - The Final Location is the Hospital building.\n" +
		       "If the Final Location building is not on the board, see the Survival of the Fittest Rulebook.\n" +
		       "4) Once the second Records has been found, the Zombies gain Zombies Auto-Spawn for the rest of the game. " +
		       "Zombies win by preventing the Heroes from reaching their goals or by killing 3 Heroes.";
		Rounds = 17;
		//TODO
	}
}

public class SupplyRun : Mission
{
	public SupplyRun()
	{
		Name = "Supply Run";
		Text = "Zombies: Grave Weapons, Zombies Auto-Spawn, Always Zombie Heroes\n" +
		       "Hero: Survival Decks, Hero Card Pool (6), Barricades, Heroes Replenish, Manor Deck (10), Number Counters (1-6)\n" +
		       "Use theManor House center board. All of the Heroes start in the Manor House, which start Fully Barricaded. " +
		       "The Number Counters represent a Grouping of Supplies.\n" +
		       "1) Use the Manor House center board. All of the Heroes start in the Manor House, which starts Fully " +
		       "Barricaded. The Number Counters represent  a grouping of Supplies.\n" +
		       "2) When a Number Counter is revealed, place that man Supply markers on the building. Any hero in a " +
		       "building with supply markers may pick up 1 supply marker during their exchange items phase (including " +
		       "on the turn that they were revealed). A Hero may carry up to 3 Supply makers at a time. If a Hero is " +
		       "killed while carrying supplies, leave the markers in the space and any other Hero may pick them up for " +
		       "free during their exchange items phase. Supplies may be exchanged between Heroes just as though they " +
		       "were Items. A building with Supply markers on it may not be taken over (Re-Roll)\n" +
		       "3) A Hero in the Manor House with one or more supply markers may drop them off during their exchange " +
		       "items phase. To win, the Heroes must end a Hero turn with at least 12 supply markers collected in the " +
		       "Manor house and more Heroes than zombies inside the Manor house. Zombies win by preventing the Heroes " +
		       "from reaching their goal or by killing 3 Heroes.";
		Rounds = 17;
		//TODO
	}
}

#endregion

#region Timber Peak

public class BlowUpTheTown : Mission
{
	public BlowUpTheTown()
	{
		Name = "Blow Up the Town!";
		Text = "Zombies: Always Zombie Heroes\n" +
		       "Hero: Heroes Replenish\n" +
		       "1) The Heroes must search to find Explosives and plant them around the board to blow up the town! At " +
		       "the start of the game, place the Detonator marker in the center space of the Town Center board.\n" +
		       "2) To win, the Heroes must plant explosives in one building on each of the four L-shaped outer boards " +
		       "(4 total), and then have at least one Hero in the space with the Detonator at the end of a Hero Turn.\n" +
		       "3) To plant explosives, a hero must be in a building space and use a move action to discard a an explosive " +
		       "item or marker they currently have ( such as Gasoline or a single Dynamite marker from a crate of " +
		       "dynamite card). Place a planted explosive marker on the building. Zombies may not interact with the " +
		       "detonator or a planted explosives marker in any way.\n" +
		       "4) The Zombies win by killing 2 Heroes or if the sun sets before the Heroes blow up the town.\n" +
		       "Scenario search Items:\n" +
		       "Any explosive";
		Rounds = 15;
		//TODO
	}
}

public class LearnToSurvive : Mission
{
	public LearnToSurvive()
	{
		Name = "Learn to Survive";
		Text = "Zombies: Always Zombies Heroes\n" +
		       "Hero: Heroes Replenish\n" +
		       "1) The Heroes must learn to survive in an area overrun with the undead. Every time a Hero gains an " +
		       "Upgrade during the game (not including any Upgrades that a Hero starts with), mark it on the Hero " +
		       "Upgrade Track below. The Heroes win if they gain 6 Hero upgrades over the course of the game.\n" +
		       "2) Similarly, the Zombie win by gaining 4 Zombie Upgrades over the course of the game (marked on the " +
		       "Zombie Upgrade Track below). The Zombies also win by killing 3 Heroes or if the Sun sets before the " +
		       "Heroes get enough upgrades.\n" +
		       "3) Note that Upgrades lost or discarded during the game DO NOT get removed from the track (they still " +
		       "count towards victory).\n" +
		       "Scenario search items:\n" +
		       "Fast learner";
		Rounds = 12;
		//TODO
	}
}

public class MountainOfTheDead : Mission
{
	public MountainOfTheDead()
	{
		Name = "Mountain of the Dead";
		Text = "Zombies: Always Zombie Heroes, Zombies auto Spawn\n" +
		       "Hero: Heroes Replenish, Radio Station, Generators (4)\n" +
		       "1) Timber Peak is overrun with Zombies and the only ting keeping them at bay are the town Generators. " +
		       "At the start of the game, the Heroes place 4 generators onto the board in any building spaces (limit " +
		       "1 per building).\n" +
		       "2) To wing, the zombies must destroy 3 of the generators or kill 3 Heroes.\n" +
		       "3) Heroes win by holding out until morning.";
		Rounds = 10;
		//TODO
	}
}

public class RadioForHelp : Mission
{
	public RadioForHelp()
	{
		Name = "Radio for Help";
		Text = "Zombies: Always Zombie Heroes\n" +
		       "Hero: Heroes Replenish, Radio Station, Generators (1), Number Counters (8)\n" +
		       "1) At the start of the game, randomly place the number counters marked 1 through 8 face down into 2 " +
		       "random buildings on each of the 4 L-shaped outer boards (limit 1 per building). These 8 number counters " +
		       "represent possible locations of the tools and the repair parts needed to repair the radio.\n" +
		       "2) All of the Heroes start in the Radio Station. The Heroes also place 1 Generator in either of the " +
		       "sound booth spaces at the center of the Radio Station (see Rulebook for diagram).\n" +
		       "3) Roll a D6 to determine which number counter represents the tools and another D6 to see which " +
		       "represents the Repair parts (Re-roll if the same). Instead of searching, a Hero in a building with a " +
		       "number counter may reveal that counter. When a Hero finds the tools or Repair parts, place that marker " +
		       "on the Hero. It does not count against he Hero's carrying limit, but may be exchanged with another " +
		       "Hero as if it were an Item. IF the Hero is killed, the marker is dropped in their space and any other " +
		       "Hero moving into that space may pick it up for free.\n" +
		       "4) The Heroes win by finding both the tools and the repair parts, and having both of them in the sound " +
		       "booth of the Radio Station at the end of a Hero Turn.\n" +
		       "5) Zombies win by destroying the Generator, killing 3 Heroes, or if the sun sets.";
		Rounds = 12;
		//TODO
	}
}

#endregion

#region Blood in the Forest

public class Airstrike : Mission
{
	public Airstrike()
	{
		Name = "Airstrike";
		Text = "Hero: Heroes Replenish, Number Counters (6)\n" +
		       "Zombies: Always Zombie Heroes, Zombies Auto Spawn, Airstrike(7)\n" +
		       "1) Place the number counters marked 1 through 6 face down into Random Buildings (limit 1 per building, " +
		       "may not be placed in a building with a Hero). These number counters represent possible locations of " +
		       "extra Emergency Flares in town.\n" +
		       "2) When a Hero reveals a number counter, roll a D6. If the roll is greater than the number on a the " +
		       "counter, the Hero may take an Emergency Flare from the Hero cards discard pile or directly from the " +
		       "Hero Deck (reshuffle).\n" +
		       "3) The Heroes must find and place 4 emergency flares in the four corner spaces of the Center Board to " +
		       "create a 'save zone' that the military pilots will avoid bombing. A Hero with an Emergency flare must " +
		       "use a Move Action and discard the Flare to place it in their space. Once a Flare is placed, it cannot " +
		       "affected y Heroes or Zombies.\n" +
		       "4) Beginning on Turn 7, at the start of every Zombie Turn, an Airstrike will hit the town (as covered " +
		       "in the Airstrike Scenario Special Rule).\n" +
		       "5) The Heroes win by having all 4 Flares placed with at least 2 Heroes in the 'safe zone' (anywhere on " +
		       "the Center Board). The Zombies win by killing 3 Heroes or if the entire town is destroyed in the final " +
		       "airstrike (Turn 0).\n" +
		       "Scenario search items:\n" +
		       "Emergency Flare";
		Rounds = 14;
		//TODO
	}
}

public class EscapeInThePlane : Mission
{
	public EscapeInThePlane()
	{
		Name = "Escape in the Plane";
		Text = "Hero: Heroes Replenish, Number counters (6), Airfield board\n" +
		       "Zombies: Always Zombie Heroes, Feral Dead(6), Behemoths(2)\n" +
		       "1) Place the Airplane marker in the center space of the Airfield board, then Randomly select 2 Heroes " +
		       "to be Pilots (place a pilot marker on each of them). Any Hero with the Keyword Pilot automatically counts " +
		       "as a pilot for this scenario as well.\n" +
		       "2) Place the number counters marked 1 through 6 face down in to random buildings (limit 1 per building, " +
		       "may not be placed in a building with a Hero). These number counters represent possible locations of the " +
		       "flight plans. Roll a D6 to determine which number counter is the actual Flight plans.\n" +
		       "3) The Heroes must find the Flight plans to navigate their way out of the mountains, and Gasoline (or " +
		       "fuel can) to fuel up the plane. A Hero with Gasoline that is in the space with the plane may discard the " +
		       "Gasoline as a move action to fuel up the plane.\n" +
		       "4) To win, the Heroes must have at least 2 Heroes at the fueled up plane ( at least one of which must " +
		       "be a pilot), and one of those Heroes must have the Flight plans. The zombies win by killing 3 Heroes (or " +
		       "all of the Pilots), or if the Sun sets.\n" +
		       "Scenario search items\n" +
		       "Gasoline / Fuel can";
		Rounds = 14;
		//TODO
	}
}

public class HoldTheLine : Mission
{
	public HoldTheLine()
	{
		Name = "Hold the Line!";
		Text = "Hero: Heroes Replenish, Hero Card Pool(8)\n" +
		       "Zombies: Always Zombie Heroes, Zombies Auto Spawn, Zombie Hand (6), Extra Zombie Move (+1)\n" +
		       "1) A massive wave of Zombies is threatening to overrun the town! The Heroes must prevent as many as " +
		       "possible from getting through, and hold them off until sunrise.\n" +
		       "2) Zombies do not enter play at spawning pits. Instead, before setup, one board edge is denoted as the " +
		       "Zombie Board Edge. Spawning Zombies may be placed in any space along the entire length of the Zombie " +
		       "board edge, and there is no spawning limit per space. THis counts for starting Zombies as well as for " +
		       "Zombies spawning during the game.\n" +
		       "3) The Heroes may start anywhere on the center board and the 2 l-shaped outer boards opposite the zombie " +
		       "board edge (ignore the start location listed on the individual Heroes). New Heroes that enter mid game " +
		       "start in the center of town space and come into play with 2 free hero cards.\n" +
		       "4) Fore this scenario, do not use the normal method for rolling a random building, instead random " +
		       "buildings are alway selected on one of the two l-shaped outer boards that contain the zombie board edge. " +
		       "Simply roll a D6 to determine which Zombie board edge section the random building is on (1-3 Right 4-6 " +
		       "Left), then roll for the exact building as normal. Any new spawning pits placed on the board during the " +
		       "game may have up to 1 zombie per turn spawn there.\n" +
		       "5) The zombies win by moving 3 zombies off the opposite board edge from the zombie board edge, or by " +
		       "killing 3 Heroes. The Heroes win by holding the zombies off until Sunrise.";
		Rounds = 12;
		//TODO
	}
}

public class LostInTheWoods : Mission
{
	public LostInTheWoods()
	{
		Name = "Lost in the Woods";
		Text = "Hero: Heroes Replenish, Hero starting cards (2), Number counters (8)\n" +
		       "Zombies: Always Zombie Heroes, Zombie Auto Spawn, Forest Only Board, Feral Dead(6), Behemoths(2)\n" +
		       "1) Place the camp site marker in the center space of the forest center board. All heroes start at the " +
		       "camp site and Heroes may search at the camp site.\n" +
		       "2) Place the 8 number counters face down into 4 random buildings / spaces on each of the 2 l-shaped " +
		       "outer boards on the opposite end of the board from the tower relay building (limit 1 per space). The " +
		       "number counters represent possible locations of the radio transponder. Roll a D6 to determine which " +
		       "number counter is the actual radio transponder.\n" +
		       "3) The zombie player starts with 3D6 zombies (or D6+2 zombies each if tow zombie players) that can be " +
		       "placed in any space on the board except on the forest center board (limit 1 per space).\n" +
		       "4) The heroes win by finding the radio transponder and getting it to the tower relay building with at " +
		       "least 2 heroes there. The zombies win by killing 3 Hers or if the sun sets.\n" +
		       "Scenario search items:\n" +
		       "Climbing rope";
		Rounds = 12;
		//TODO
	}
}

public class SalvageMission : Mission
{
	public SalvageMission()
	{
		Name = "Salvage Mission";
		Text = "Hero: Heroes Replenish, Hero card pool(8), Forest Ring, Airfield Board\n" +
		       "Zombie: Always zombie heroes, zombies auto spawn, feral dead(infinite)\n" +
		       "1) Place salvage tokens face down onto the board in 2 random buildings on each of the four l-shaped town " +
		       "board, and on 1 random building / space on each of the eight for each boards in the outer ring. There " +
		       "should be 16 total salvage tokens on the board.\n" +
		       "2) Place the Airplane marker in the center space of the Airfield board. All Heroes start at the Airplane.\n" +
		       "3) Roll a D6 to determine the Hero's mission:\n" +
		       "1-2) Pit Stop - The Heroes must find 3 supplies tokens and get them back to the Airplane with the 2 " +
		       "Heroes there.\n" +
		       "3-4) Distress call - The Heroes must find both of the survivor tokens and get both of them back to the " +
		       "airplane with at least 2 heroes there.\n" +
		       "5-6) Zombie Hunt - The Heroes must find the Zombie Champion token, which always represents a super " +
		       "behemoth (a zombie behemoth, but with 5 wounds), and kill it! They must then get at least 2 Hers back " +
		       "to the Airplane.\n" +
		       "4) The zombies get all 6 feral dead on the board at the start of the game in addition to their normal " +
		       "starting zombies. These feral dead must start at the yellow spawning pits on the forest boards (limit " +
		       "1 per pit). The feral dead pool is limitless in th scenario.\n" +
		       "5) The zombies win by killing 2 heroes (3 heroes in zombie hunt) or if the sun sets.";
		Rounds = 13;
		//TODO
	}
}

#endregion

#region Supplements

// Airstrike
// Hold the line

public class StockUp : Mission
{
	public StockUp()
	{
		Name = "Stock Up!";
		Text = "1) Roll a random building. THis is the stockpile building. The stockpile may not be taken over or have " +
		       "a new spawning pit placed there.\n" +
		       "2) The Heroes must collect 4 weapons (Hand weapon and/or ranged weapon) and a first aid kit.\n" +
		       "3) The Heroes win by having all the collected items int eh stockpile building at the same time with " +
		       "not zombies in the building.\n" +
		       "4) Zombies win by preventing the completion of the stockpile or by killing 3 heroes.\n" +
		       "Scenario search items:\n" +
		       "First aid kit, any hand weapon, any ranged weapon";
		Rounds = 15;
		//TODO
	}
}

public class RevengeOfTheDead : Mission
{
	public RevengeOfTheDead()
	{
		Name = "Revenge of the Dead";
		Text = "1) All zombies roll an extra fight dice. Zombies automatically spawn every turn. The zombies card hand " +
		       "size is 6 cards (or 3 each).\n" +
		       "2) Any time a Hero is killed they are automatically turned into a zombie and that Hero player draws a " +
		       "new Hero character.\n" +
		       "3) The zombies must kill 5 Heroes to win the game no other zombie victory condition apply.\n" +
		       "4) Heroes win by holding off the zombies until morning or by killing 4 Zombie Heroes.\n" +
		       "5) Each hero comes into play with a hero card in addition to any Hero cards they would normally start with.";
		Rounds = 13;
		//TODO
	}
}

public class ZombiePillage : Mission
{
	public ZombiePillage()
	{
		Name = "Zombie Pillage";
		Text = "Zombie Pillage, Hero starting cards (1), Heroes Replenish\n" +
		       "1) At the start of th game, randomly count out 45 hero cards face down in to a separate deck and set it " +
		       "near the zombie player(s). This deck is known as the Town Deck and represents the health of the town. " +
		       "They remaining hero cards form the hero deck as normal. any cards discarded from the town deck are " +
		       "placed in the normal hero deck discard pile. When searching, heroes draw from the normal Hero deck.\n" +
		       "2) The zombie player immediately wins if the town deck ever runs out of cards. The Heroes win by " +
		       "preventing the zombies from depleting the town deck.\n" +
		       "3) Any zombie card that would discard cards from the top of the hero deck may instead target the town " +
		       "deck, but for each card that would normally be discarded from the hero deck, the zombie player instead " +
		       "gets a free pillage roll (discarding a town deck card on the roll of 4+). Anything that would shuffle " +
		       "one or more cards back into the Hero deck from the discard pile may instead try to shuffle them into the " +
		       "town deck (this is always hero's choice). Roll a D6 for each of these cards. On the roll of 4+ it is " +
		       "successfully shuffled into the town deck.";
		Rounds = 13;
		//TODO
	}
}

#endregion

#region Web Missions



#endregion

#region campaign

//With the unexpected rise of the living dead, the local doctor thinks he can find a cure if you can bring him an 
//undamaged skull of a zombie. Killing and surviving past an onslaught of zombies you finally manage to get an undamaged 
//head. 

//However, it is still alive and biting making it harder to survive the trip back to the doctor. Outside the 
//doctor’s office, the doors are barricaded with a forklift, part of your group distracts the zombies while you move 
//the forklift and clear a path for everyone to get inside. 

//The doctor is pleased and quickly gets to work, soon he is 
//sure he has synthesized a cure. But in a moment of carelessness, he gets bit and quickly turns and rushes towards your 
//group carrying the biting zombie head. 

// Campaign when Heroes lose a mission, it counts as two heroes deaths
// Campaign Heroes lose if there are 8 deaths during the campaign

public class CampaignMission1 : Mission
{
	public CampaignMission1()
	{
		Name = "Find a Zombie Skull";
		Text = "With the unexpected rise of the living dead, the local doctor thinks he can find a cure if you can " +
		       "bring him an undamaged skull of a zombie.\n" +
		       "Heroes win when they find the zombie's head, and at least two of the heroes are in the same space";
		StartNarrativeText = new List<string>
		{
			"To find a cure the doctor sends you on a mission to get an undamaged zombie skull." +
			"Place six zombies on evenly across all the spawning pits." +
			"Mix the 1-5 tokens and the skull token face down and place one under each of the zombies." +
			"These tokens stay with their respective zombies. When the zombie is killed reveal it's token." +
			"When the skull is revealed the hero can pick it up."
		};
		NarrativeText = new List<string>
		{
			"You successfully remove the Zombie head from its body, it falls to the ground still bitting. You carefully " +
			"pick it up and make your way back to the group.\n\nThe Zombie head is still bitting, the hero that is " +
			"carrying it rolls one less fight dice. The Heroes complete the mission once two Heroes are in the same " +
			"space as the Zombie's head."
		};
		EndNarrativeText = new List<string>
		{
			"You group up with each other, severed zombie head in hand and make a run for it back to the doctor.",
			
			"The situation takes a turn for the worse, as you retreat to your hideout you come across a zombie that is " +
			"badly decomposed and can't stand or crawl. You can remove and take the head as you flee."
		};
		Rounds = 15;
		HeroObjective.Add(FindZombieSkull);
		ZombieObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Kill4Heroes);
		AudioWin = "HeroWin";
		AudioLose = "ZombieWin";
	}
}

public class CampaignMission2 : Mission
{
	public CampaignMission2()
	{
		Name = "Return to the Doctor";
		Text = "The doors to Doctor's office is blocked by a forklift." +
		       "The Heroes can move the forklift by holding gas and being on the same space as the forklift.\n" +
		       "Heroes win once they get the skull inside the doctor's office.";
		StartNarrativeText = new List<string>
		{
			"As you approach the makeshift doctor's office, you see that a zombie horde has entered the area, and the " +
			"forklift has been moved to barricade the doors."
		};
		NarrativeText = new List<string>
		{
			"You quickly pour the gas into the forklift's tank, and there is a puff of smoke as you start the engine. " +
			"The noise attracts the zombies, but you are prepared and put the forklift in gear.\n\nAll Zombies move one " +
			"space towards the forklift, roll a D6 and move the forklift that number of spaces, killing any zombies in " +
			"its path."
		};
		EndNarrativeText = new List<string>
		{
			"You make it into the doctor's office then close and barricade the doors behind you. As you rush to find " +
			"the doctor, you hope all this is worth it.",
			
			"The horde appears to intensify, and you brace yourself for a final fight. However, you hear gun fire and " +
			"turn to see the other survivors on the roof, so you all make a run for it to the hideout."
		};
		Rounds = 16;
		HeroObjective.Add(GetSkullToDoctor);
		ZombieObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Kill4Heroes);
		ScenarioSearchItems.Add(ScenarioSearchItemTypes.Gasoline);
		AudioWin = "HeroWin";
		AudioLose = "ZombieWin";
	}
}

public class CampaignMission3 : Mission
{
	public CampaignMission3()
	{
		Name = "Defeat the Doctor";
		Text = "The doctor is pleased and quickly gets to work, he is sure that soon he will have a synthesized cure. " +
		       "While working and in a moment of distracted the Doctor is bit by the skull and the combination of drugs " +
		       "he has been testing causing him to quickly turn into a Zombie and grows in strength and size. He turns " +
		       "towards you holding the still alive zombie skull!";
		StartNarrativeText = new List<string>
		{
			"As the massively transformed doctor looms in front of you, all you can do is take a step back and prepare " +
			"for the worse.\n" +
			"The doctor has 8 wound markers, each time you successfully wound the doctor increase the objective tracker."
		};
		NarrativeText = new List<string>
		{
			"The doctor appears to have a moment of clarity saying 'Help Me,' but then his eyes gloss back over as he " +
			"lunges for you.",
			
			"The doctor groans at you flailing the Zombie head in front of him.",
			
			"The doctor doesn't seem to be phased by the wounds you have given him so far, and he only appears more " +
			"agitated and he moans only louder.",
			
			"The doctor throws the Zombie head at you, but you narrowly avoid its jaws, and it hits the ground.\n\n" +
			"Place the Zombie head on the same space as the closest Hero to the doctor. It is a distraction and any " +
			"Any Hero in that space rolls one less fight dice.",
			
			"The doctor's rage is intense, as he rushes towards the Heroes knocking over and breaking supplies and " +
			"equipment you have been gathering for months.",
			
			"The doctor is starting to appear affected by all of his wounds, but it is not stopping him from grabbing " +
			"and clawing at anyone he can reach.",
			
			"Aaarrrrgggghhh"
		};
		EndNarrativeText = new List<string>
		{
			"While the loss of life and risk were high, the zombie doctor is no longer a threat and you and the other " +
			"survivors are safe again inside the hideout, for now.",
			
			"The last thing you see before you fall unconscious is the doctor bellowing a roar then smashing through " +
			"the wall towards the other survivors."
		};
		Rounds = 12;
		HeroObjective.Add(KillDoctorZombie);
		ZombieObjective.Add(Morning(Rounds));
		ZombieObjective.Add(Kill4Heroes);
		AudioWin = "HeroWin";
		AudioLose = "ZombieWin";
	}
}

#endregion
