﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Normalization
{
    /// <summary>
    /// Convert the cloud data to be usable for the RBF Network.
    /// </summary>
    /// <param name="raw">The download data from the Google Drive.</param>
    public static void ConvertRawData(string[][] raw)
    {
        // TODO these will change US#130
        const int numInputs = 7;
        const int numOutputs = 2;
		
        List<List<string>> rawConversion = new List<List<string>>();
		
        // Remove session identifier
        for (int i = 0; i < raw.Length; i++)
        {
            rawConversion.Add(new List<string>());
            for (int j = 1; j < raw[i].Length; j++)
            {
                rawConversion[i].Add(raw[i][j]);
            }
        }

        // Spit winner into array
        string winner = "";
        int lastCell = rawConversion[0].Count - 1;
        for (int i = rawConversion.Count - 1; i >= 0; i--)
        {
            // Fill in winner of game for 'in progress' rounds
            if (rawConversion[i][lastCell] == "In Progress")
            {
                rawConversion[i][lastCell] = winner;
            }
			
            if (rawConversion[i][lastCell] == "HEROES")
            {
                winner = "HEROES";
                rawConversion[i].RemoveAt(lastCell);
                rawConversion[i].Add("1");
                rawConversion[i].Add("0");
            }
            else
            {
                winner = "ZOMBIES";
                rawConversion[i].RemoveAt(lastCell);
                rawConversion[i].Add("0");
                rawConversion[i].Add("1");
            }
        }
		
        // TODO Split mission, heroes and buildings US#131
        // Remove those for now
        for (int i = 0; i < rawConversion.Count; i++)
        {
            rawConversion[i].RemoveAt(2);
            rawConversion[i].RemoveAt(1);
            rawConversion[i].RemoveAt(0);
        }
		
        // Create a normalized set of data
        // Get input low and highs
        double[] inputLows = new double[numInputs];
        for (int i = 0; i < inputLows.Length; i++)
        {
            // Set to the max value so it starts at the worst possibility.
            inputLows[i] = double.MaxValue;
        }
        double[] inputHighs = new double[numInputs];
        for (int i = 0; i < inputHighs.Length; i++)
        {
            // Set to the min value so it starts at the worst possibility.
            inputHighs[i] = double.MinValue;
        }
        for (int i = 0; i < rawConversion.Count; i++)
        {
            for (int j = 0; j < numInputs; j++)
            {
                // If lower than the current low, make the new low.
                if (Convert.ToDouble(rawConversion[i][j]) < inputLows[j])
                {
                    inputLows[j] = Convert.ToDouble(rawConversion[i][j]);
                }
                // If higher than the current high, make the new high.
                if (Convert.ToDouble(rawConversion[i][j]) >  inputHighs[j])
                {
                    inputHighs[j] = Convert.ToDouble(rawConversion[i][j]);
                }
            }
        }
        // Normalize
        double[][] normalizedData = new double[rawConversion.Count][];
        for (int i = 0; i < rawConversion.Count; i++)
        {
            normalizedData[i] = new double[numInputs + numOutputs];
            for (int j = 0; j < numInputs; j++)
            {
                normalizedData[i][j] =
                    DoubleNormalization(Convert.ToDouble(rawConversion[i][j]), inputLows[j], inputHighs[j], -1.0, 1.0);
            }
            for (int j = 0; j < numOutputs; j++)
            {
                normalizedData[i][j + numInputs] = Convert.ToDouble(rawConversion[i][j + numInputs]);
            }
        }
		
        // Convert to string[][] and save
        string[][] normalizedDataString = MakeStringMatrixFromDoubleMatrix(normalizedData);
        FileIo.SaveData(normalizedDataString, "converted.txt");
        
        // Save high and low ranges
        double[][] normalizationRanges = {inputLows, inputHighs};
        string[][] normalizationRangesString = MakeStringMatrixFromDoubleMatrix(normalizationRanges);
        FileIo.SaveData(normalizationRangesString, "lowhighranges.txt");
    }

    /// <summary>
    /// Normalize the round data to use in the RBF network
    /// </summary>
    /// <param name="roundsLeft">The number of rounds are left in the game.</param>
    /// <param name="zombiesDead">The number of zombies that have died.</param>
    /// <param name="zombiesOnBoard">The number of zombies currently on the board.</param>
    /// <param name="heroesDead">The number of heroes that have died.</param>
    /// <param name="buildingsTakenOver">The number of buildings that are currently taken over.</param>
    /// <param name="buildingsLightsOut">The number of buildings that currently have lights out.</param>
    /// <param name="spawningPits">The current number of spawning pits.</param>
    /// <returns>The normalized data</returns>
    public static double[] NormalizeRoundData(int roundsLeft, int zombiesDead, int zombiesOnBoard, int heroesDead,
        int buildingsTakenOver, int buildingsLightsOut, int spawningPits)
    {
        double[] result = new double[7];

        if (FileIo.CheckIfFileExist("lowhighranges.txt"))
        {
            // Get the range information
            string[][] rangeString = FileIo.LoadData("lowhighranges.txt");
            double[][] ranges = MakeDoubleMatrixFromStringMatrix(rangeString);

            result[0] = DoubleNormalization(roundsLeft, ranges[0][0], ranges[1][0], -1, 1);
            result[1] = DoubleNormalization(zombiesDead, ranges[0][1], ranges[1][1], -1, 1);
            result[2] = DoubleNormalization(zombiesOnBoard, ranges[0][2], ranges[1][2], -1, 1);
            result[3] = DoubleNormalization(heroesDead, ranges[0][3], ranges[1][3], -1, 1);
            result[4] = DoubleNormalization(buildingsTakenOver, ranges[0][4], ranges[1][4], -1, 1);
            result[5] = DoubleNormalization(buildingsLightsOut, ranges[0][5], ranges[1][5], -1, 1);
            result[6] = DoubleNormalization(spawningPits, ranges[0][6], ranges[1][6], -1, 1);
        
            return result;
        }
        return null;
    }

    /// <summary>
    /// Convert numbers to strings for a matrix.
    /// </summary>
    /// <param name="doubleMatrix">The Numerical matrix.</param>
    /// <returns>The String matrix.</returns>
    public static string[][] MakeStringMatrixFromDoubleMatrix(double[][] doubleMatrix)
    {
        string[][] result = new string[doubleMatrix.Length][];
        for (int i = 0; i < doubleMatrix.Length; i++)
        {
            result[i] = new string[doubleMatrix[i].Length];
			
            for (int j = 0; j < doubleMatrix[i].Length; j++)
            {
                result[i][j] = doubleMatrix[i][j].ToString();
            }
        }

        return result;
    }

    /// <summary>
    /// Convert strings to numbers for a matrix.
    /// </summary>
    /// <param name="stringMatrix">The String matrix.</param>
    /// <returns>The Numerical matrix.</returns>
    public static double[][] MakeDoubleMatrixFromStringMatrix(string[][] stringMatrix)
    {
        double[][] result = new double[stringMatrix.Length][];

        for (int i = 0; i < stringMatrix.Length; i++)
        {
            result[i] = new double[stringMatrix[i].Length];
            for (int j = 0; j < stringMatrix[i].Length; j++)
            {
                if(stringMatrix[i][j] == "")
                {
                    result[i][j] = 0;
                }
                else
                {
                    result[i][j] = Convert.ToDouble(stringMatrix[i][j]);
                }
            }
        }

        return result;
    }
	
    /// <summary>
    /// Normalize a value between -1 and 1.
    /// </summary>
    /// <param name="x">The value to normalize.</param>
    /// <param name="max">The range.</param>
    /// <returns>The normalized value.</returns>
    public static double IntNormalization (int x, int max)
    {
        double p = (x * 1.0f) / (max * 1.0f);

        return -1.0f + (2.0f * p);
    }

    /// <summary>
    /// Normalize a double value.
    /// </summary>
    /// <param name="value">The value to normalize.</param>
    /// <param name="rangeLow">The low number the value can be.</param>
    /// <param name="rangeHigh">The hight number the value can be.</param>
    /// <param name="normalizedLow">The normalized low, either 0 or -1.</param>
    /// <param name="normalizedHigh">The normalized high, typically 1.</param>
    /// <returns></returns>
    public static double DoubleNormalization(double value, double rangeLow, double rangeHigh, double normalizedLow, double normalizedHigh)
    {
        double dataRange = rangeHigh - rangeLow;
        double normalizedRange = normalizedHigh - normalizedLow;

        double valuePercentage = (value - rangeLow) / dataRange;

        return normalizedLow + (normalizedRange * valuePercentage);
    }
}