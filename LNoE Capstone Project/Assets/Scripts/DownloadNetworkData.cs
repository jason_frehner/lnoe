﻿using System;
using UnityEngine;

public class DownloadNetworkData : MonoBehaviour
{
	
	/// <summary>
	/// Download the RBF settings when game launches.
	/// </summary>
	private void Start ()
	{
		// Check if lastDownload key exist.
		if (!PlayerPrefs.HasKey(Strings.PlayerPrefKeys.LastDownload))
		{
			PlayerPrefs.SetInt(Strings.PlayerPrefKeys.LastDownload, 0);
		}
		
		// Check if last download was not today.
		if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.LastDownload) != DateTime.Today.DayOfYear)
		{
			GsfuUtilsLnoe.GetNetworkSettings(true);
			CloudConnectorCore.processedResponseCallback.AddListener(GsfuUtilsLnoe.ParseData);

			GsfuUtilsLnoe.GetAllGameLogs(true);
			
			PlayerPrefs.SetInt(Strings.PlayerPrefKeys.LastDownload, DateTime.Today.DayOfYear);
		}
	}
}
