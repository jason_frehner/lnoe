﻿using UnityEngine;

public static class Strings
{
    public const string Zombies = "Zombies";
    
    public static class AudioMixerKeys
    {
        public const string Master = "Master";
        public const string Music = "Music";
        public const string SFX = "SFX";
    }
    
    public static class CampaignSave //TODO should this be here or in the campaign manager?
    {
        private const string One = "campaign1.dat";
        private const string Two = "campaign2.dat";
        private const string Three = "campaign3.dat";
        private const string Four = "campaign4.dat";

        public static readonly string[] AllSaveFiles = 
        {
            One, Two, Three, Four
        };

        public static readonly string Folder = Application.persistentDataPath + "/campaign/";

        public const string SaveSummary = "Saved game is on mission #MISSIONVALUE." +
                                          "\nDEADVALUE have died in this campaign.";
    }
    
    public static class GameObjectNames
    {
        public const string AudioClick = "Click";
        public const string CameraSetupPosition = "CameraSetupPosition";
        public const string Manager = "SceneManager";
        public const string Text = "Text";

        public static class BoardPositions //TODO use in place map images?
        {
            public const string Center = "Center";
            public const string TL = "TopLeft";
            public const string TR = "TopRight";
            public const string BL = "BottomLeft";
            public const string BR = "BottomRight";
        }
    }

    public static class HeroNames
    {
        public const string Becky = "Becky";
        public const string Billy = "Billy";
        public const string FatherJoseph = "Father Joseph";
        public const string JakeCartwright = "Jake Cartwright";
        public const string Jenny = "Jenny";
        public const string Johnny = "Johnny";
        public const string Sally = "Sally";
        public const string SheriffAnderson = "Sheriff Anderson";

        public const string Amanda = "Amanda";
        public const string Kenny = "Kenny";
        public const string Sam = "Sam";
        public const string Rachelle = "Rachelle";

        public const string Jade = "Jade";
        public const string MrGoddard = "Mr. Goddard";
        public const string Stacy = "Stacy";
        public const string Victor = "Victor";
        
        public static readonly string[] AllHeroes = 
        {
            Becky,
            Billy,
            FatherJoseph,
            JakeCartwright,
            Jenny,
            Johnny,
            Sally,
            SheriffAnderson,
        
            Amanda,
            Kenny,
            Sam,
            Rachelle,
        
            Jade,
            MrGoddard,
            Stacy,
            Victor
        };
    }
    
    public static class PlayerPrefKeys
    {
        public const string AudioMaster = "audioMaster";
        public const string AutoSpawn = "ZombiesAutoSpawn";
        public const string AutoSave = "AutoSave";
        public const string DebugMode = "DebugMode";
        
        // Expansions
        public const string GH = "GrowingHunger";
        public const string HP1 = "HeroPackOne";
        public const string SOTF = "SurvivalOfTheFittest";
        public const string GW = "GraveWeapons";
        public const string TP = "TimberPeak";
        public const string BITF = "BloodInTheForest";
        public const string AE = "AnniversaryEdition";
        public const string HP2 = "HeroPackTwo";
        public const string SU = "StockUp";
        public const string ST = "Soundtrack";
        public const string ROTD = "RevengeOfTheDead";
        public const string ZP = "ZombiePillage";
        public const string AA = "AdvancedAbilities";
        public static readonly string[] Expansions = 
        {
            GH,
            HP1,
            SOTF,
            GW,
            TP,
            BITF,
            AE,
            HP2,
            SU,
            ST,
            ROTD,
            ZP,
            AA
        };

        public const string FreeSearch = "FreeSearchMarkers";
        public const string GameType = "gameType";
        public const string GameTypeCampaign = "campaign";
        public const string GraveDead = "GraveDead";
        public const string HeroReplenish = "HeroesReplenish";
        public const string Horde21 = "ZombieHorde21";
        public const string LastDownload = "lastDownload";
        public const string Music = "music";
        public const string NFC = "nfc";
        public const string PlagueCarriers = "PlagueCarriers";
        public const string Scenario = "scenario";
        public const string SFX = "sfx";
        public static readonly string[] SpecialRules =
        {
            WellStocked,
            HeroReplenish,
            StartingCards1,
            StartingCards2,
            FreeSearch,
            GraveDead,
            AutoSpawn,
            Horde21
        };
        public const string StartingCards1 = "HeroStartingCards1";
        public const string StartingCards2 = "HeroStartingCards2";
        public const string WellStocked = "WellStockedBuildings";
    }
    
    public static class Materials
    {
        public const string Splatter = "LNOE_SplatterSlider";
    }
    
    public static class MaterialProperties
    {
        public const string Step = "_Step";
    }

    public static class Resources
    {
        public const string AudioMixer = "Audio";
    }
    
    public static class SceneNames
    {
        public const string CampSetup = "CampaignSetup";
        public const string SaveNFC = "SaveNFC";
    }
    
    public static class Sprites //TODO only used in place map images?
    {
        public const string Manor = "Center Manor";
        public const string Open = "Center Open";
        public const string AirHosDin = "Airplane Hangar Hospital Diner";
        public const string BanJunGas = "Bank Junkyard Gas Station";
        public const string ChurPol = "Church Graveyard Police Station";
        public const string CornFarm = "Cornfield The Barn Farmhouse";
        public const string GenGunPlant = "General Store Gun Shop Plant";
        public const string HighGym = "High School Gym";
        
        public const string AntFactLib = "Antique Factory Library";
        public const string DrugPostSuper = "Drug Store Post Office Super Market";
    }
}