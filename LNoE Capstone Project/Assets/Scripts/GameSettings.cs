﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    public LoadScene LoadScene;
    public GameObject CoreHeroes;
    public GameObject GHHeroes;
    public GameObject GHSpecial;
    public GameObject HP1Heroes;
    public GameObject TPHeroes;
    public GameObject BFHeroes;
    public GameObject AEHeroes;
    public GameObject HP2Heroes;

    private readonly List<string> expansionsWithHeroes = new List<string>
    {
        Strings.PlayerPrefKeys.GH,
        Strings.PlayerPrefKeys.HP1,
        Strings.PlayerPrefKeys.TP,
        Strings.PlayerPrefKeys.BITF,
        Strings.PlayerPrefKeys.AE,
        Strings.PlayerPrefKeys.HP2
    };

    private List<GameObject> heroObjects = new List<GameObject>();

    public RectTransform HeroContent;
    private int heroContentHeight = 460;

    private int numPlayers;
    public string GameType; // Set in editor in the campaignSetup scene
    private string scenario = "Random";
    private readonly List<string> coreHeroOptions = new List<string>
    {
        Strings.HeroNames.Becky,
        Strings.HeroNames.Billy,
        Strings.HeroNames.FatherJoseph,
        Strings.HeroNames.Jenny,
        Strings.HeroNames.Johnny,
        Strings.HeroNames.Sally,
        Strings.HeroNames.SheriffAnderson
    };

    private readonly List<string> selectedHeroOptions = new List<string> { "Random", "Random", "Random", "Random" };

    private bool isWellStockedBuildings;
    private bool isHeroesReplenish;
    private bool isHeroStartingCards1;
    private bool isHeroStartingCards2;
    private bool isFreeSearchMarkers;
    private bool isGraveDeadZombiesInGame;
    private bool isZombieAutoSpawn;
    private bool isZombieHorde21;

    private AudioSource clickAudio;

    public Dropdown ScenarioList;
    public int SelectedHeroes;

    public GameObject SpecialRules;

    /// <summary>
    /// Clear any stored settings.
    /// </summary>
    private void Start()
    {
        PlayerPrefs.SetInt(Strings.PlayerPrefKeys.AutoSave, 0);
        PlayerPrefs.DeleteKey("Tile1");
        PlayerPrefs.DeleteKey("Tile2");
        PlayerPrefs.DeleteKey("Tile3");
        PlayerPrefs.DeleteKey("Tile4");

        // Add the hero expansion game objects to the list.
        heroObjects = new List<GameObject>
        {
            GHHeroes,
            HP1Heroes,
            TPHeroes,
            BFHeroes,
            AEHeroes,
            HP2Heroes
        };

        clickAudio = GameObject.Find(Strings.GameObjectNames.AudioClick).GetComponent<AudioSource>();

        PlayerPrefs.DeleteKey("numPlayers");
        PlayerPrefs.DeleteKey(Strings.PlayerPrefKeys.GameType);
        PlayerPrefs.DeleteKey(Strings.PlayerPrefKeys.Scenario);
        PlayerPrefs.DeleteKey("hero1");
        PlayerPrefs.DeleteKey("hero2");
        PlayerPrefs.DeleteKey("hero3");
        PlayerPrefs.DeleteKey("hero4");

        // Campaign
        if (GameType == Strings.PlayerPrefKeys.GameTypeCampaign)
        {
            string[] deadHeroes = CampaignManager.GetlistOfDeadHeroes().Split('\n');

            foreach (string hero in deadHeroes)
            {
                GameObject toggleObject = GameObject.Find(hero);
                if (toggleObject != null)
                {
                    toggleObject.SetActive(false);
                }
            }
        }
        // Single Mission
        else
        {
            if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.GH) == 1)
            {
                SpecialRules.SetActive(true);

                ScenarioList.options.Add(new Dropdown.OptionData("Burn it to the Ground!"));
                ScenarioList.options.Add(new Dropdown.OptionData("Plague Carriers"));
                ScenarioList.options.Add(new Dropdown.OptionData("Zombie Apocalypse"));

                PlayerPrefs.SetInt(Strings.PlayerPrefKeys.GraveDead, 0);
            }

            if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.HP1) == 1)
            {
                ScenarioList.options.Add(new Dropdown.OptionData("Hunt for Survivors"));
            }

            if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.TP) == 1)
            {
                // TODO Timber peak missions
            }

            if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.BITF) == 1)
            {
                // TODO Blood in the forest missions
            }

            if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.AE) == 1)
            {
                // TODO Anniversary edition new missions
            }

            if (PlayerPrefs.GetInt(Strings.PlayerPrefKeys.HP2) == 1)
            {
                // TODO Hero pack two missions
            }
        }

        // Enable the Heroes expansion objects.
        for (int i = 0; i < expansionsWithHeroes.Count; i++)
        {
            if (PlayerPrefs.GetInt(expansionsWithHeroes[i]) != 1) continue;
            heroObjects[i].SetActive(true);
            heroContentHeight += 300;
        }

        HeroContent.sizeDelta = new Vector2(HeroContent.sizeDelta.x, heroContentHeight);
    }

    /// <summary>
    /// Store the number of players.
    /// </summary>
    /// <param name="value"></param>
    [UsedImplicitly]
    public void SetNumberPlayers(int value)
    {
        numPlayers = value;
        clickAudio.Play();
    }

    /// <summary>
    /// Store the game type.
    /// </summary>
    /// <param name="value"></param>
    [UsedImplicitly]
    public void SetGameType(string value)
    {
        GameType = value;
        clickAudio.Play();
    }

    /// <summary>
    /// Set game for tutorial.
    /// </summary>
    [UsedImplicitly]
    public void PlayTutorial()
    {
        GameType = "tutorial";
        scenario = "Tutorial";
        selectedHeroOptions[0] = coreHeroOptions[0];
        selectedHeroOptions[1] = coreHeroOptions[1];
        selectedHeroOptions[2] = coreHeroOptions[2];
        selectedHeroOptions[3] = coreHeroOptions[3];

        SetPlayerPrefs();

        LoadScene.GoToSceneWithLoadingScreen("Game");
    }

    /// <summary>
    /// Store the scenario.
    /// </summary>
    /// <param name="text"></param>
    [UsedImplicitly]
    public void SetScenario(Text text)
    {
        scenario = text.text;
    }

    /// <summary>
    /// Save all settings to player preferences.
    /// </summary>
    [UsedImplicitly]
    public void SetPlayerPrefs()
    {
        PlayerPrefs.SetInt("numPlayers", numPlayers);
        PlayerPrefs.SetString(Strings.PlayerPrefKeys.GameType, GameType);
        PlayerPrefs.SetString(Strings.PlayerPrefKeys.Scenario, scenario);
        PlayerPrefs.SetString("hero1", selectedHeroOptions[0]);
        PlayerPrefs.SetString("hero2", selectedHeroOptions[1]);
        PlayerPrefs.SetString("hero3", selectedHeroOptions[2]);
        PlayerPrefs.SetString("hero4", selectedHeroOptions[3]);

        Analytics.CustomEvent("gameSettingsFromPlayer", new Dictionary<string, object>
        {
            {"numPlayers", numPlayers},
            {Strings.PlayerPrefKeys.GameType, GameType},
            {Strings.PlayerPrefKeys.Scenario, scenario},
            {"hero1", selectedHeroOptions[0]},
            {"hero2", selectedHeroOptions[1]},
            {"hero3", selectedHeroOptions[2]},
            {"hero4", selectedHeroOptions[3]}
        });
    }

    /// <summary>
    /// Selecting heroes from toggle buttons.
    /// </summary>
    /// <param name="hero">Toggle ON or OFF</param>
    [UsedImplicitly]
    public void HeroToggled(bool hero)
    {
        // Collect all toggles.
        List<Toggle> toggles = new List<Toggle>();
        for (int i = 0; i < CoreHeroes.transform.childCount; i++)
        {
            toggles.Add(CoreHeroes.transform.GetChild(i).GetComponent<Toggle>());
        }
        for (int i = 0; i < GHHeroes.transform.childCount; i++)
        {
            toggles.Add(GHHeroes.transform.GetChild(i).GetComponent<Toggle>());
        }

        for (int i = 0; i < HP1Heroes.transform.childCount; i++)
        {
            toggles.Add(HP1Heroes.transform.GetChild(i).GetComponent<Toggle>());
        }

        for (int i = 0; i < TPHeroes.transform.childCount; i++)
        {
            toggles.Add(TPHeroes.transform.GetChild(i).GetComponent<Toggle>());
        }

        for (int i = 0; i < BFHeroes.transform.childCount; i++)
        {
            toggles.Add(BFHeroes.transform.GetChild(i).GetComponent<Toggle>());
        }

        for (int i = 0; i < AEHeroes.transform.childCount; i++)
        {
            toggles.Add(AEHeroes.transform.GetChild(i).GetComponent<Toggle>());
        }

        for (int i = 0; i < HP2Heroes.transform.childCount; i++)
        {
            toggles.Add(HP2Heroes.transform.GetChild(i).GetComponent<Toggle>());
        }

        int selectedHeroOptionIndex = 0;

        // If toggle ON
        if (hero)
        {
            SelectedHeroes++;

            // For each hero toggle
            foreach (Toggle toggle in toggles)
            {
                if (!toggle.isOn)
                {
                    // Check if 4 heroes are selected.
                    if (SelectedHeroes == 4)
                    {
                        // Turn off toggle
                        toggle.interactable = false;
                    }
                }
                else
                {
                    // Add hero to selection
                    selectedHeroOptions[selectedHeroOptionIndex] = toggle.transform.GetChild(1).GetComponent<Text>().text;
                    selectedHeroOptionIndex++;
                }
            }
        }
        // If toggle OFF
        else
        {
            SelectedHeroes--;

            // Turn toggles back on
            if (SelectedHeroes >= 4) return;
            foreach (Toggle toggle in toggles)
            {
                if (!toggle.interactable)
                {
                    toggle.interactable = true;
                }
            }
        }

        // Reset selected hero options
        for (int i = selectedHeroOptionIndex; i < 4; i++)
        {
            selectedHeroOptions[selectedHeroOptionIndex] = "Random";
        }
    }

    /// <summary>
    /// Disable rules from mission
    /// </summary>
    [UsedImplicitly]
    public void SetRulesFromMission()
    {
        clickAudio.Play();
        if (scenario == "Random") return;

        Missions missions = new Missions();
        Mission mission = missions.GetMissionFromName(scenario);

        foreach (string rule in Strings.PlayerPrefKeys.SpecialRules)
        {
            PlayerPrefs.DeleteKey(rule);
        }

        foreach (var specialRule in mission.SpecialRules)
        {
            for (int i = 0; i < SpecialRules.transform.childCount; i++)
            {
                Transform child = SpecialRules.transform.GetChild(i);
                if (child.name != specialRule.PlayerPrefKey) continue;

                child.GetChild(0).GetComponent<Toggle>().isOn = true;
                child.GetChild(0).GetComponent<Toggle>().interactable = false;
                PlayerPrefs.SetInt(specialRule.PlayerPrefKey, 1);
            }
        }
    }

    /// <summary>
    /// Toggles the is Grave dead zombie value.
    /// </summary>
    [UsedImplicitly]
    public void SetGraveDeadZombieValue()
    {
        isGraveDeadZombiesInGame = !isGraveDeadZombiesInGame;
        int value = 0;
        if (isGraveDeadZombiesInGame)
        {
            value = 1;
        }
        PlayerPrefs.SetInt(Strings.PlayerPrefKeys.GraveDead, value);
    }

    /// <summary>
    /// Sets the well stocked buildings value.
    /// </summary>
    public void SetWellStockedBuildingsValue()
    {
        isWellStockedBuildings = !isWellStockedBuildings;
        int value = 0;
        if (isWellStockedBuildings)
        {
            value = 1;
        }
        PlayerPrefs.SetInt(Strings.PlayerPrefKeys.WellStocked, value);
    }

    /// <summary>
    /// Sets the heroes replenish value.
    /// </summary>
    public void SetHeroesReplenishValue()
    {
        isHeroesReplenish = !isHeroesReplenish;
        int value = 0;
        if (isHeroesReplenish)
        {
            value = 1;
        }
        PlayerPrefs.SetInt(Strings.PlayerPrefKeys.HeroReplenish, value);
    }

    /// <summary>
    /// Sets the free search markers value.
    /// </summary>
    public void SetFreeSearchMarkersValue()
    {
        isFreeSearchMarkers = !isFreeSearchMarkers;
        int value = 0;
        if (isFreeSearchMarkers)
        {
            value = 1;
        }
        PlayerPrefs.SetInt(Strings.PlayerPrefKeys.FreeSearch, value);
    }

    /// <summary>
    /// Sets the hero starting cards1.
    /// </summary>
    public void SetHeroStartingCards1()
    {
        isHeroStartingCards1 = !isHeroStartingCards1;
        int value = 0;
        if (isHeroStartingCards1)
        {
            value = 1;
        }
        PlayerPrefs.SetInt(Strings.PlayerPrefKeys.StartingCards1, value);
    }

    /// <summary>
    /// Sets the zombies automatic spawn.
    /// </summary>
    public void SetZombiesAutoSpawn()
    {
        isZombieAutoSpawn = !isZombieAutoSpawn;
        int value = 0;
        if (isZombieAutoSpawn)
        {
            value = 1;
        }
        PlayerPrefs.SetInt(Strings.PlayerPrefKeys.AutoSpawn, value);
    }

    /// <summary>
    /// Sets the zombie horde21.
    /// </summary>
    public void SetZombieHorde21()
    {
        isZombieHorde21 = !isZombieHorde21;
        int value = 0;
        if (isZombieHorde21)
        {
            value = 1;
        }
        PlayerPrefs.SetInt(Strings.PlayerPrefKeys.Horde21, value);
    }
}
