﻿using System;
using System.Collections.Generic;
using System.IO;
using Helper;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

// native share code from: http://resocoder.com/2017/03/31/social-sharing-tutorial-unity-android-code/

/// <summary>
/// Game log.
/// </summary>
public class GameLog : MonoBehaviour {

	public GameObject Contents;
	public GameObject ContentsRender;
	public GameObject TextPrefab;
	public GameObject ImagePrefab;

	public Camera LogCamera;
	private RenderTexture logRenderTexture;
	private bool grab;
	private GameObject dupContent;

	private int textObjects;
	private int imageObjects;

	public List<GameSaveData.GameLogEntries> GameLogSave = new List<GameSaveData.GameLogEntries>();
	
//	public List<string> GameLogOrder = new List<string>();
//	public List<string> GameLogText = new List<string>();
//	public List<byte[]> GameLogTextures = new List<byte[]>();
//	public List<Vector2> GameLogTextureSizes = new List<Vector2>();

	private const float TextHeight = 50.0f;
	private const int LogWidth = 700;
	private float imageHeight = 400.0f;

	/// <summary>
	/// Adds a text block to the log.
	/// </summary>
	/// <param name="s">String.</param>
	public void AddText(string s)
	{

		if(TextPrefab == null)
		{
			DebugLogger.WriteLog("<color=red>Game log TextPrefab not attached.</color>");
			return;
		}
		
		//Instantiate
		textObjects++;
		GameObject newText = Instantiate (TextPrefab);
		SetParentAndScale (newText.transform);

		//Set content
		newText.GetComponent<Text> ().text = s;

		//Adjust log
		SetContentHeight ();
		
		//Add to list
		GameLogSave.Add(new GameSaveData.GameLogEntries
		{
			LogType = GameSaveData.GameLogEntries.Type.Text,
			LogText = s
		});
	}
		
	/// <summary>
	/// Adds a image block to the log.
	/// </summary>
	/// <param name="texture">Texture.</param>
	public void AddImage(Texture2D texture)
	{
		if(ImagePrefab == null)
		{
			DebugLogger.WriteLog("<color=red>Game log ImagePrefab not attached.</color>");
			return;
		}
		
		float aspect = (float)texture.width / texture.height;
		
		//Instantiate
		imageObjects++;
		GameObject newImage = Instantiate (ImagePrefab);

		//Set content
		newImage.GetComponent<RawImage>().texture = texture;
		newImage.GetComponent<AspectRatioFitter>().aspectRatio = aspect;
		SetParentAndScale (newImage.transform);

//		imageHeight = aspect * LogWidth;

		//Adjust log
		SetContentHeight();
		
		//Add to list
//		GameLogTextures.Add(texture.GetRawTextureData());
//		GameLogOrder.Add("t");
//		GameLogTextureSizes.Add(new Vector2(texture.width, texture.height));
		texture.Compress(false);
		GameLogSave.Add(new GameSaveData.GameLogEntries
		{
			LogType = GameSaveData.GameLogEntries.Type.Image,
			LogTexture = texture.GetRawTextureData(),
			LogTextureFormat = texture.format,
			TextureWidth = texture.width,
			TextureHeight = texture.height
		});
	}
		
	/// <summary>
	/// Sets the height of the content.
	/// </summary>
	private void SetContentHeight()
	{
		//How high should the content be.
		float height = (imageHeight * imageObjects) + (TextHeight * textObjects);
		
		//Set the height of the log canvas render texture.
		logRenderTexture = new RenderTexture(LogWidth, Convert.ToInt32(height), 24);
		LogCamera.targetTexture = logRenderTexture;
		
		// Adjust the content render to fit the convas render texture.
		ContentsRender.GetComponent<RawImage>().texture = logRenderTexture;
		ContentsRender.GetComponent<RawImage>().SetNativeSize();
		RectTransform contentRenderRect = ContentsRender.GetComponent<RectTransform>();
		contentRenderRect.sizeDelta = new Vector2(contentRenderRect.sizeDelta.x / 2, contentRenderRect.sizeDelta.y / 2);

	}


	/// <summary>
	/// Sets the parent and scale.
	/// </summary>
	/// <param name="t">Transform.</param>
	private void SetParentAndScale(Transform t)
	{
		t.SetParent (Contents.transform);
		t.localScale = new Vector3 (1, 1, 1);
		t.localPosition = new Vector3(0, 0, 0);
	}

	/// <summary>
	/// Share the game log with the android share intent.
	/// </summary>
	[UsedImplicitly]
	public void SaveLogImage()
	{
		grab = true;
		
		DebugLogger.WriteLog(Application.persistentDataPath);

		if(!Application.isEditor)
		{
			AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
			AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
			intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
			AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
			AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + Application.persistentDataPath + "/LNOE_Log.jpg");
			intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"),
				uriObject);
			intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"),
				"Played with the Last Night on Earth Companion!");
			intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");
			AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject chooser = intentClass.CallStatic<AndroidJavaObject>("createChooser",
				intentObject, "Share your game");
			currentActivity.Call("startActivity", chooser);
		}
	}

	private void OnPostRender()
	{
		if(grab)
		{
			DebugLogger.WriteLog("Rendering log");
			
			Texture2D logTexture2D = new Texture2D(LogWidth, logRenderTexture.height, TextureFormat.RGBA32, false);
			logTexture2D.ReadPixels(new Rect(0, 0, LogWidth, logRenderTexture.height), 0, 0);
			logTexture2D.Apply();

			
			
			var bytes = logTexture2D.EncodeToJPG();
		
			File.WriteAllBytes(Application.persistentDataPath + "/LNOE_Log.jpg", bytes);

			grab = false;
		}
	}
}