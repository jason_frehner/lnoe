﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Destroy the Parent Gameobject.
/// </summary>
public class DestroyParent : MonoBehaviour {

	public Button Button;
	public ZombieCard Card;
	
	private AudioSource clickAudio;

	/// <summary>
	/// Set the Button Listener Event.
	/// </summary>
	private void Start()
	{
		Button = GetComponent<Button> ();
		Button.onClick.AddListener (Destroy);
		clickAudio = GameObject.Find(Strings.GameObjectNames.AudioClick).GetComponent<AudioSource>();
	}

	/// <summary>
	/// Destroy the Parent Gameobject.
	/// </summary>
	private void Destroy()
	{
		Card.RemainsInPlayReverse();
		Destroy (transform.parent.gameObject);
		clickAudio.Play();
	}
	
	
}
