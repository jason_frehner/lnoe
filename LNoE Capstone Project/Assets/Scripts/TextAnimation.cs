﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controller for the flashing text animation.
/// </summary>
public class TextAnimation : MonoBehaviour
{

	private Animator textAnimator;
	
	private void Start ()
	{
		textAnimator = GetComponent<Animator>();
	}

	/// <summary>
	/// Sets the text and starts the Animation.
	/// </summary>
	/// <param name="textToAnimate">The string to use.</param>
	public void Play(string textToAnimate)
	{
		transform.GetChild(0).GetComponent<Text>().text = textToAnimate;
		textAnimator.SetTrigger("click");
	}
}
