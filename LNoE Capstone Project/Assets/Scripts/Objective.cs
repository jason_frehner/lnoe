﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class Objective : MonoBehaviour
{

	public string Name;
	public int CurrentProgress;
	public int Goal;

	private ZombieTurn zombieTurnManager;

	public Text ObjectiveTitle;
	public Text CurrentProgressText;
	public Image ProgressBar;
	public bool IsHeroGoal;
	public string AudioName;

	private bool isDebugLog;

	private void Awake()
	{
		isDebugLog = PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
	}

	private void Start()
	{
		zombieTurnManager = GameObject.Find ("ZombieManager").GetComponent<ZombieTurn> ();
	}

	/// <summary>
	/// Set the variables for the objective.
	/// </summary>
	/// <param name="objectiveName">The name of the objective.</param>
	/// <param name="goal">The goal.</param>
	/// <param name="audioName">Name of audio to use for this objective.</param>
	/// <param name="heroGoal">Is it a hero goal.</param>
	public void SetObjective(string objectiveName, int goal, string audioName, bool heroGoal)
	{
		Name = objectiveName;
		Goal = goal;
		IsHeroGoal = heroGoal;
		AudioName = audioName;

		if(isDebugLog)
		{
			Debug.Log(Name + Goal);
		}
		
		ObjectiveTitle.text = Name;
		SetProgress(0);
//		CurrentProgressText.text = CurrentProgress + "/" + Goal;/
//		ProgressBar.fillAmount = (float)CurrentProgress / Goal;
	}

	/// <summary>
	/// Update the progress of the goal.
	/// </summary>
	/// <returns>Update if goal has reached a complete state.</returns>
	public bool UpdateProgress()
	{
		SetProgress(CurrentProgress + 1);
//		CurrentProgress++;
//		CurrentProgressText.text = CurrentProgress + "/" + Goal;
//		ProgressBar.fillAmount = (float)CurrentProgress / Goal;

		if (Goal == CurrentProgress)
		{
			return true;
		}
		if (AudioName != "Null")
		{
			GameObject.Find(AudioName).GetComponent<AudioSource>().Play();
		}
		return false;
	}

	/// <summary>
	/// Update progress through the UI.
	/// </summary>
	[UsedImplicitly]
	public void ButtonUpdateProgress()
	{
		if (UpdateProgress())
		{
			zombieTurnManager.GameOver(IsHeroGoal ? "Heroes" : Strings.Zombies);
		}
		if (PlayerPrefs.GetString(Strings.PlayerPrefKeys.GameType) == Strings.PlayerPrefKeys.GameTypeCampaign && CurrentProgress != Goal)
		{
			zombieTurnManager.ShowCampaignNarrative(CurrentProgress - 1);
		}
		
		zombieTurnManager.PlayClickAudio();
	}

	/// <summary>
	/// Sets the progress of the objective.
	/// </summary>
	/// <param name="value">Value to set objective to.</param>
	public void SetProgress(int value)
	{
		CurrentProgress = value;
		CurrentProgressText.text = CurrentProgress + "/" + Goal;
		ProgressBar.fillAmount = (float)CurrentProgress / Goal;
	}
}
