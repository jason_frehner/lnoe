﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AdjustAudio : MonoBehaviour
{
	public AudioMixer Mixer;
	[Space(10)]
	public Slider SliderMusic;
	public Slider SliderSFX;
	[Space(10)]
	public Image AudioToggleButtonImage;
	public Sprite TurnAudioOffSprite;
	public Sprite TurnAudioOnSprite;
	
	private AudioSource clickAudio;

	/// <summary>
	/// Move the sliders to help match the values saved in player preferences.
	/// </summary>
	private void Start()
	{
		GetMixerIfNotAttached();
		SetSliderValues();

		// Master
		if(PlayerPrefs.HasKey(Strings.PlayerPrefKeys.AudioMaster))
		{
			float mixerMaster = PlayerPrefs.GetFloat(Strings.PlayerPrefKeys.AudioMaster);
			SetMasterAudio(mixerMaster < 0);
		}
		else
			PlayerPrefs.SetFloat(Strings.PlayerPrefKeys.AudioMaster, 0.0f);
		
		// Connect to audio clip
		clickAudio = GameObject.Find(Strings.GameObjectNames.AudioClick).GetComponent<AudioSource>();
	}

	private void SetSliderValues()
	{
		// Music
		float musicValue = SetSliderValue(Strings.PlayerPrefKeys.Music, SliderMusic);
		SaveMusic(musicValue);

		// SFX
		float sfxValue = SetSliderValue(Strings.PlayerPrefKeys.SFX, SliderSFX);
		SetSfx(sfxValue);
	}

	private void GetMixerIfNotAttached()
	{
		if (!Mixer)
			Mixer = Resources.Load<AudioMixer>(Strings.Resources.AudioMixer);
	}

	/// <summary>
	/// Set and save the music volume.
	/// </summary>
	/// <param name="vol">The volume</param>
	[UsedImplicitly]
	public void SaveMusic(float vol)
	{
		SetMixerLevel(Strings.AudioMixerKeys.Music, vol, Strings.PlayerPrefKeys.Music);
	}

	/// <summary>
	/// Set and save the SFX volume.
	/// </summary>
	/// <param name="vol"></param>
	[UsedImplicitly]
	public void SetSfx(float vol)
	{
		SetMixerLevel(Strings.AudioMixerKeys.SFX, vol, Strings.PlayerPrefKeys.SFX);
		if(clickAudio)
			clickAudio.Play();
	}

	/// <summary>
	/// Set the Mixer volume level and save the value to player preferences.
	/// </summary>
	/// <param name="mixer">The mixer name.</param>
	/// <param name="vol">Volume to set to.</param>
	/// <param name="preference">Player preference key.</param>
	private void SetMixerLevel(string mixer, float vol, string preference)
	{
		float value = -60.0f * Mathf.Pow(vol - 1, 2);
		Mixer.SetFloat(mixer, value);
		PlayerPrefs.SetFloat(preference, vol);
	}

	
	/// <summary>
	/// Toggle master audio levels on and off.
	/// </summary>
	[UsedImplicitly]
	public void AudioToggle()
	{
		Mixer.GetFloat(Strings.AudioMixerKeys.Master, out float mixerMaster);
		SetMasterAudio(Math.Abs(mixerMaster) < 0.1f);
		clickAudio.Play();
	}

	private void SetMasterAudio(bool setOff)
	{
		if (setOff)
			SetMasterAudioSettings(-80.0f, TurnAudioOffSprite);
		else
			SetMasterAudioSettings(0.0f, TurnAudioOnSprite);
	}

	private void SetMasterAudioSettings(float masterValue, Sprite sprite)
	{
		Mixer.SetFloat(Strings.AudioMixerKeys.Master, masterValue);
		if(AudioToggleButtonImage != null)
			AudioToggleButtonImage.sprite = sprite;
		PlayerPrefs.SetFloat(Strings.PlayerPrefKeys.AudioMaster, masterValue);
	}

	/// <summary>
	/// Sets the slider value from saved player preference value.
	/// </summary>
	/// <param name="key">Key to the player prefs value.</param>
	/// <param name="slider">Slider to set</param>
	/// <returns>The value it was set to.</returns>
	private static float SetSliderValue(string key, Slider slider)
	{
		if(!PlayerPrefs.HasKey(key) && slider != null)
			PlayerPrefs.SetFloat(key, slider.value);

		float value = PlayerPrefs.GetFloat(key);
		if(slider != null)
			slider.value = value;
		
		return value;
	}
}