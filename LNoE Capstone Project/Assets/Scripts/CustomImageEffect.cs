﻿using UnityEngine;

// Dan Moran
// https://www.youtube.com/watch?v=kpBnIAPtsj8

[ExecuteInEditMode]
public class CustomImageEffect : MonoBehaviour
{
	public Material EffectMaterial;

	private void OnRenderImage(RenderTexture src, RenderTexture dst)
	{
		if (EffectMaterial != null)
			Graphics.Blit(src, dst, EffectMaterial);
	}
}