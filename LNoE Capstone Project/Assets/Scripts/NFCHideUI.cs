﻿using UnityEngine;

public class NFCHideUI : MonoBehaviour
{

	public GameObject[] UIToHide;

	private void Start()
	{
		if (PlayerPrefs.GetInt("nfcEnabled") != 0) return;
		
		foreach (GameObject o in UIToHide)
		{
			o.SetActive(false);
		}
	}
}
