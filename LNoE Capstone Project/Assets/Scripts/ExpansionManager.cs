﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class ExpansionManager : MonoBehaviour {

	private void Start()
	{
		foreach(string expansion in Strings.PlayerPrefKeys.Expansions)
		{
			if(!PlayerPrefs.HasKey(expansion))
			{
				PlayerPrefs.SetInt(expansion, 0);
			}
			
			if (PlayerPrefs.GetInt(expansion) == 1)
			{
				GameObject.Find("ExpansionToggle" + expansion).GetComponent<Toggle>().isOn = true;
			}
		}
	}

	/// <summary>
	/// Sets the Growing Hunger expansion state.
	/// </summary>
	/// <param name="value">New state.</param>
	[UsedImplicitly]
	public void SetGrowingHunger(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.GH, value ? 1 : 0);
	}
	
	/// <summary>
	/// Sets the Hero Pack One expansion state.
	/// </summary>
	/// <param name="value">New state.</param>
	[UsedImplicitly]
	public void SetHeroPackOne(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.HP1, value ? 1 : 0);
	}
	
	/// <summary>
	/// Sets the Survival of the Fittest expansion state.
	/// </summary>
	/// <param name="value">New state.</param>
	[UsedImplicitly]
	public void SetSurvivalOfTheFittest(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.SOTF, value ? 1 : 0);
	}
	
	/// <summary>
	/// Sets the Grave Weapons expansion state.
	/// </summary>
	/// <param name="value">New state.</param>
	[UsedImplicitly]
	public void SetGraveWeapons(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.GW, value ? 1 : 0);
	}
	
	/// <summary>
	/// Sets the Timber Peak core state.
	/// </summary>
	/// <param name="value">New state.</param>
	[UsedImplicitly]
	public void SetTimberPeak(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.TP, value ? 1 : 0);
	}
	
	/// <summary>
	/// Sets the Blood in the Forest expansion state.
	/// </summary>
	/// <param name="value">New state.</param>
	[UsedImplicitly]
	public void SetBloodInTheForest(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.BITF, value ? 1 : 0);
	}
	
	/// <summary>
	/// Sets the 10th Anniversary core edition state.
	/// </summary>
	/// <param name="value">New state.</param>
	[UsedImplicitly]
	public void SetAnniversary(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.AE, value ? 1 : 0);
	}
	
	/// <summary>
	/// Sets the Hero Pack Two expansion state.
	/// </summary>
	/// <param name="value">New state.</param>
	[UsedImplicitly]
	public void SetHeroPackTwo(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.HP2, value ? 1 : 0);
	}
	
	[UsedImplicitly]
	public void SetStockUp(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.SU, value ? 1 : 0);
	}
	
	[UsedImplicitly]
	public void SetSountrack(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.ST, value ? 1 : 0);
	}
	
	[UsedImplicitly]
	public void SetRevengeOfTheDead(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.ROTD, value ? 1 : 0);
	}
	
	[UsedImplicitly]
	public void SetZombiePillage(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.ZP, value ? 1 : 0);
	}
	
	[UsedImplicitly]
	public void SetAdvancedAbilities(bool value)
	{
		PlayerPrefs.SetInt(Strings.PlayerPrefKeys.AA, value ? 1 : 0);
	}
}
