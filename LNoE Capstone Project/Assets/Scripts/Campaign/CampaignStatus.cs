﻿using UnityEngine;
using UnityEngine.UI;

public class CampaignStatus : MonoBehaviour {

	public readonly CampaignManager CampaignManager = new CampaignManager();
	private LoadScene loadScene;
	
	// Use this for initialization
	private void Start ()
	{
		// Check if nfc was used to load
		if (PlayerPrefs.HasKey(Strings.PlayerPrefKeys.NFC))
		{
			CampaignManager.LoadFromNfc();
		}
		
		loadScene = GameObject.Find(Strings.GameObjectNames.Manager).GetComponent<LoadScene>();
		
		GameObject.Find("TextMission").GetComponent<Text>().text = CampaignManager.GetNextMission().Name;

		if (CampaignManager.GetHeroDeathCount() > 0)
		{
			GameObject.Find("TextDeadHeroes").GetComponent<Text>().text = "<b>Dead Heroes</b>\n\n" + 
			                                                              CampaignManager.GetlistOfDeadHeroes();
		}
	}

    /// <summary>
    /// Saves to NFC.
    /// </summary>
    public void SaveToNFC()
	{
		CampaignManager.SaveToNfc();
		loadScene.GoToSceneWithLoadingScreen(Strings.SceneNames.SaveNFC);
	}
}
