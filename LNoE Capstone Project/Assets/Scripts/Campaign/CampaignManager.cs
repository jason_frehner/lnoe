﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using JetBrains.Annotations;
using UnityEngine;
	
public class CampaignManager
{
    /// <summary>
    /// The campaign missions
    /// </summary>
    private static readonly List<Mission> CampaignMissions = new List<Mission>
	{
		new CampaignMission1(),
		new CampaignMission2(),
		new CampaignMission3()
	};
	private static List<string> heroesDiedInCampaign = new List<string>();
	private string zombieCampaignObjective;
	private List<string> heroCampaignItems;
	private const int HeroDeathLimit = 8;
	private static int currentMissionIndex;
	private static int heroDeathCount;
	private static CampaignState campaignState;
	private static string lastCampaignFile;
	private static bool campaignOver;
	private static bool heroesWon;

	private static bool IsDebugLog()
	{
		return PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) == 1;
	}
	
	/// <summary>
	/// Save Campaign.
	/// </summary>
	[UsedImplicitly]
	public static void SaveCampaignState(string saveName)
	{
		string filePath = Strings.CampaignSave.Folder + saveName;
		
		// Check if path exist
		// ReSharper disable once AssignNullToNotNullAttribute
		if(!Directory.Exists(Path.GetDirectoryName(filePath)))
		{
			string directory = Path.GetDirectoryName(filePath);
			if(directory != null)
			{
				Directory.CreateDirectory(directory);
			}
		}
		
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		FileStream fileStream = File.Create(filePath);
		
		campaignState.MissionIndex = currentMissionIndex;
		campaignState.HeroesDied = heroesDiedInCampaign;
	
		binaryFormatter.Serialize(fileStream, campaignState);
		fileStream.Close();

		lastCampaignFile = saveName;

		if(IsDebugLog())
		{
			Debug.Log("Campaign Saved " + saveName);
			Debug.Log(Application.persistentDataPath);
		}
	}

    /// <summary>
    /// New campaign.
    /// </summary>
    /// <param name="saveName">Name of the save.</param>
    public static void NewCampaign(string saveName)
	{
		heroesDiedInCampaign = new List<string>();
		currentMissionIndex = 0;
		heroDeathCount = 0;
		SaveCampaignState(saveName);
	}

	/// <summary>
	/// Load Campaign.
	/// </summary>
	public static void LoadCampaignState(string saveName)
	{
		if (!File.Exists(Strings.CampaignSave.Folder + saveName)) return;
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		FileStream fileStream = File.Open(Strings.CampaignSave.Folder + saveName, FileMode.Open);
		campaignState =(CampaignState) binaryFormatter.Deserialize(fileStream);

		currentMissionIndex = campaignState.MissionIndex;
		heroesDiedInCampaign = campaignState.HeroesDied;
		heroDeathCount = heroesDiedInCampaign.Count;

		if(IsDebugLog())
		{
			Debug.Log(heroesDiedInCampaign);
		}

		lastCampaignFile = saveName;

		if(IsDebugLog())
		{
			Debug.Log("Campaign Loaded " + saveName);
		}
	}

	/// <summary>
	/// Delete saved file.
	/// </summary>
	/// <param name="saveName"></param>
	public static void DeleteSavedCampaign(string saveName)
	{
		if (!File.Exists(Strings.CampaignSave.Folder + saveName)) return;
		File.Delete(Strings.CampaignSave.Folder + saveName);

		if(IsDebugLog())
		{
			Debug.Log("Campaign Deleted");
		}
	}

	/// <summary>
	/// Get the campaign mission to play.
	/// </summary>
	/// <returns></returns>
	public static Mission GetNextMission()
	{
		return CampaignMissions[currentMissionIndex];
	}

	/// <summary>
	/// List of heroes that have died in the campaign.
	/// </summary>
	/// <returns>The list.</returns>
	public static string GetlistOfDeadHeroes()
	{
		StringBuilder heroes = new StringBuilder();
		foreach(string s in heroesDiedInCampaign)
		{
			heroes.AppendLine(s);
		}
		return heroes.ToString().Trim();
	}

	/// <summary>
	/// Get the Hero death count.
	/// </summary>
	/// <returns>The count.</returns>
	public static int GetHeroDeathCount()
	{
		return heroDeathCount;
	}

	public bool IsCampaignOver()
	{
		return campaignOver;
	}

	public bool IsHeroesWon()
	{
		return heroesWon;
	}

	/// <summary>
	/// Add the dead hero to the list.
	/// </summary>
	/// <param name="heroName">Hero that died.</param>
	public static void HeroDeadInCampaign(string heroName)
	{
		heroesDiedInCampaign.Add(heroName);
		
		heroDeathCount++;
		if(heroDeathCount < HeroDeathLimit) return;
		campaignOver = true;
		heroesWon = false;
	}

	/// <summary>
	/// Get ready for the next campaign mission.
	/// </summary>
	public static void CloseOutMission()
	{
		currentMissionIndex++;
		if(currentMissionIndex > 2)
		{
			currentMissionIndex = 0;
		}
		SaveCampaignState(lastCampaignFile);
	}

    /// <summary>
    /// Saves to NFC.
    /// </summary>
    public static void SaveToNfc()
	{
		// NFC string is an encoding of what mission is next and what heroes have died.

		StringBuilder saveinfo = new StringBuilder();

		saveinfo.Append(currentMissionIndex.ToString());

		foreach(string hero in Strings.HeroNames.AllHeroes)
		{
			saveinfo.Append(heroesDiedInCampaign.Contains(hero) ? "t" : "f");
		}
		
		PlayerPrefs.SetString(Strings.PlayerPrefKeys.NFC, saveinfo.ToString());
	}

    /// <summary>
    /// Loads from NFC.
    /// </summary>
    public static void LoadFromNfc()
	{
		string loadedInfo = PlayerPrefs.GetString(Strings.PlayerPrefKeys.NFC);
		PlayerPrefs.DeleteKey(Strings.PlayerPrefKeys.NFC);

		currentMissionIndex =(int) char.GetNumericValue(loadedInfo[0]);
		if(IsDebugLog())
		{
			Debug.Log("current mission index" + currentMissionIndex);
		}

		int shorterArray = Mathf.Min(loadedInfo.Length - 1, Strings.HeroNames.AllHeroes.Length);
		
		for(int i = 0; i < shorterArray; i++)
		{
			if(loadedInfo[i + 1] == 't')
			{
				HeroDeadInCampaign(Strings.HeroNames.AllHeroes[i]);
			}
		}
	}

    /// <summary>
    /// NFC summery.
    /// </summary>
    /// <param name="payload">The payload.</param>
    /// <returns>Summery.</returns>
    [UsedImplicitly] // NFC.cs
    public static string NFCSummary(string payload)
	{
		StringBuilder summary = new StringBuilder(Strings.CampaignSave.SaveSummary);
		summary.Replace("MISSIONVALUE", (char.GetNumericValue(payload[0]) + 1).ToString());
		
		int deadHeroCount = 0;
		StringBuilder deadHeroes = new StringBuilder();

		int shorterArray = Mathf.Min(payload.Length - 1, Strings.HeroNames.AllHeroes.Length);
		
		for(int i = 0; i < shorterArray; i++)
		{
			if(payload[i + 1] != 't') continue;
			deadHeroCount++;
			deadHeroes.Append(Strings.HeroNames.AllHeroes[i]);
			if(i + 1 != shorterArray)
			{
				deadHeroes.Append(", ");				
			}
		}

//		deadHeroes.Remove(deadHeroes.Length - 2, 2);

		summary.Replace("DEADVALUE", deadHeroCount == 0 ? "No Heroes" : deadHeroes.ToString());

		return summary.ToString();
	}
}