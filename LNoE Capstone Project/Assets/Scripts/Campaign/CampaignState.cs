using System;
using System.Collections.Generic;

[Serializable]
internal struct CampaignState
{
    public int MissionIndex;
    public List<string> HeroesDied;
}