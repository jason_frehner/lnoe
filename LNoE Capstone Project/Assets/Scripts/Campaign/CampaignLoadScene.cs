﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class CampaignLoadScene : MonoBehaviour
{
	public GameObject SaveSlot1;
	public GameObject SaveSlot2;
	public GameObject SaveSlot3;
	public GameObject SaveSlot4;

	private GameObject[] saveSlots;

	private LoadScene loadScene;
	private AudioSource clickAudio;
	
	private void Start()
	{
		loadScene = GameObject.Find(Strings.GameObjectNames.Manager).GetComponent<LoadScene>();
		clickAudio = GameObject.Find(Strings.GameObjectNames.AudioClick).GetComponent<AudioSource>();

		saveSlots = new[]
		{
			SaveSlot1, SaveSlot2, SaveSlot3, SaveSlot4
		};
		
		SetUIState();
	}

    /// <summary>
    /// Sets the state of the UI.
    /// </summary>
    private void SetUIState()
	{
		for(int i = 0; i < saveSlots.Length; i++)
		{
			if(File.Exists(Strings.CampaignSave.Folder + Strings.CampaignSave.AllSaveFiles[i]))
			{
				UISaveFileExist(saveSlots[i], Strings.CampaignSave.AllSaveFiles[i]);
			}
			else
			{
				UISaveFileNotExist(saveSlots[i], Strings.CampaignSave.AllSaveFiles[i]);
			}
		}
	}

    /// <summary>
    /// UIs the save file exist.
    /// </summary>
    /// <param name="parent">The parent.</param>
    /// <param name="saveName">Name of the save.</param>
    private void UISaveFileExist(GameObject parent, string saveName)
	{
		// File button
		parent.transform.GetChild(0).Find(Strings.GameObjectNames.Text).GetComponent<Text>().text = parent.name;
		parent.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
		parent.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(
			delegate { CampaignManager.LoadCampaignState(saveName); });
		parent.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(
			delegate { loadScene.GoToSceneWithLoadingScreen(Strings.SceneNames.CampSetup); });
		parent.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(SetUIState);
			
		// Trash button
		parent.transform.GetChild(1).GetComponent<Button>().onClick.RemoveAllListeners();
		parent.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(
			delegate { CampaignManager.DeleteSavedCampaign(saveName); });
		parent.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(SetUIState);
		parent.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(
			delegate { clickAudio.Play(); });
		parent.transform.GetChild(1).GetComponent<Button>().interactable = true;
	}

    /// <summary>
    /// UIs the save file not exist.
    /// </summary>
    /// <param name="parent">The parent.</param>
    /// <param name="saveName">Name of the save.</param>
    private void UISaveFileNotExist(GameObject parent,string saveName)
	{
		// File button
		parent.transform.GetChild(0).Find(Strings.GameObjectNames.Text).GetComponent<Text>().text = "EMPTY";
		parent.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
		parent.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(
			delegate { CampaignManager.NewCampaign(saveName); });
		parent.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(
			delegate { loadScene.GoToSceneWithLoadingScreen(Strings.SceneNames.CampSetup); });
		parent.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(SetUIState);
		
		// Trash button
		parent.transform.GetChild(1).GetComponent<Button>().onClick.RemoveAllListeners();
		parent.transform.GetChild(1).GetComponent<Button>().interactable = false;
	}
}