﻿using System;
using System.Collections.Generic;

/// <summary>
/// Class for the data of the session.
/// </summary>
[Serializable]
public class GameData
{
    private readonly string gameSessionName;
    private readonly string mission;
    private readonly string buildings;
    private readonly List<RoundData> roundData;
    public string GameWon { get; private set; }

    /// <summary>
    /// The game data class that holds information about the game session.
    /// </summary>
    /// <param name="gameSessionName">Identifier for the session based on a time stamp.</param>
    /// <param name="mission">What mission is being played.</param>
    /// <param name="buildings">Buildings in this session</param>
    public GameData(string gameSessionName, string mission, string buildings)
    {
        this.gameSessionName = gameSessionName;
        this.mission = mission;
        this.buildings = buildings;
        roundData = new List<RoundData>();
        GameWon = "In Progress";
    }

    /// <summary>
    /// Adds the round.
    /// </summary>
    /// <param name="thisRound">This round.</param>
    public void AddRound(RoundData thisRound)
    {
        roundData.Add(thisRound);
    }

    /// <summary>
    /// Gets the name of the game session.
    /// </summary>
    /// <returns></returns>
    public string GetGameSessionName()
    {
        return gameSessionName;
    }

    /// <summary>
    /// Gets the mission.
    /// </summary>
    /// <returns></returns>
    public string GetMission()
    {
        return mission;
    }

    /// <summary>
    /// Gets the buildings.
    /// </summary>
    /// <returns></returns>
    public string GetBuildings()
    {
        return buildings;
    }

    /// <summary>
    /// Gets the round data count.
    /// </summary>
    /// <returns></returns>
    public int GetRoundDataCount()
    {
        return roundData.Count;
    }

    /// <summary>
    /// Gets the round data.
    /// </summary>
    /// <param name="i">int.</param>
    /// <returns></returns>
    public RoundData GetRoundData(int i)
    {
        return roundData[i];
    }

    /// <summary>
    /// Sets the winner.
    /// </summary>
    /// <param name="winner">The winner.</param>
    public void SetWinner(string winner)
    {
        GameWon = winner;
    }

}