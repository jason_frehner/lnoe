﻿Shader "Unlit/ButtonDamage"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_SplatterTex ("Splatter Texture", 2D) = "white" {}
		_Step ("Amount of Splatter", Range(0, 3)) = 0
		_ButtonColor ("Button Color", Color) = (1,1,1,1)
		_SplatterColor ("Splatter Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags {
		        "RenderType"="Transparent"
		        "PreviewType"="Plane"
		        "Queue"="Transparent"
		    }
		//LOD 100
		
		

		Pass
		{
			
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			uniform sampler2D _SplatterTex;
			float _Step;
			float4 _ButtonColor;
			float4 _SplatterColor;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// Textures
				float4 buttonTexture = tex2D(_MainTex, i.uv);
				float4 splatterTexture = tex2D(_SplatterTex, i.uv);
				
                float splatterCombination = ( // Add the intensity of each splatter together.
                                                splatterTexture.r * clamp(_Step, 0, 1) +
                                                splatterTexture.g * clamp((_Step - 1.0), 0, 1) +
                                                splatterTexture.b * clamp((_Step - 2.0), 0, 1)
                                            );                                            
				
                fixed4 baseButton = buttonTexture.a * _ButtonColor;
                fixed4 splatterOverlay = splatterCombination * _SplatterColor;
                splatterOverlay.a = clamp(splatterOverlay.a - (baseButton.a * -1 + 1), 0, 1);
                
                //return buttonTexture * _SplatterColor;
                return lerp(baseButton, splatterOverlay, splatterOverlay.a);// * splatterOverlay;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
