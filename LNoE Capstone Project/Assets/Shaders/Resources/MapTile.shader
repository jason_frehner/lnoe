﻿Shader "LNOE/MapTile"
{
	Properties
	{
	    _MainTex("Texture", 2d) = "white" {}
	    _EffectsLayers ("Effects Layers", 2d) = "black" {}
	}
	
	SubShader
	{
		Tags
		{
			"PreviewType" = "Plane"
			"Queue" = "Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
		}
		
		Blend SrcAlpha OneMinusSrcAlpha
        ColorMask RGB
        Cull Off Lighting Off ZWrite Off
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
            sampler2D _EffectsLayers;
            
            // Vertext data from the mesh
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

            // Vertext to Fragment
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

            // Convert appdata to v2f
			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex); // Gets the point on the screen relative to the object.
				o.uv = v.uv;
				return o;
			}

            
            // Manipulate the fragments
			float4 frag(v2f i) : SV_Target
			{
                // Lighting
                float timeWave = abs(sin(_Time.g)); // Between 0 and 1
                float lightIntensity = ((tex2D(_EffectsLayers, i.uv).r * timeWave) + 1); // Between 1 and 2
				
				float4 color = tex2D(_MainTex, i.uv);
				color.rgb = clamp(color.rgb * lightIntensity, 0, 1);
				return color;
			}
			ENDCG
		}
	}
}