Shader "LNOE/SplatterTime" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "Queue"="Transparent"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            
            Stencil {
                Comp NotEqual
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma target 3.0
            
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                
                float4 red = float4(1,0,0,1);
            
                float4 splatterTexture = tex2D(_MainTex,i.uv);
                //float timeStep = round(sin(_Time.g / 5.0) * 1.5 + 1.5); // Normalize the time to a range between 0 - 3, with 5 seceonds between each step.
                float timeStep = round(frac(_Time.g / 10.0) * 3);
                float splatterCombination = ( // Add the intensity of each splatter together.
                                                (splatterTexture.r * clamp(timeStep, 0, 1)) +
                                                (splatterTexture.g * clamp((timeStep - 1.0), 0, 1)) +
                                                (splatterTexture.b * clamp((timeStep - 2.0), 0, 1))
                                            );
                float3 finalColor = saturate( // clamp
                                                max(
                                                       (red.rgb * (splatterCombination/3.0)), // Amount of red based on the average of splatterCombination.
                                                       (splatterCombination * 0.4333333 - 1.0) // Highlight
                                                   )
                                             );              
                return fixed4(finalColor,splatterCombination);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
