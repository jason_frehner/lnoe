﻿Shader "LNOE/AnimatedButton"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags 
		{ 
		    "PreviewType" = "Plane"
		    "Queue" = "Transparent"
		}

		Pass
		{
		
		    Blend SrcAlpha OneMinusSrcAlpha
		    
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// Original texture
				fixed4 col = tex2D(_MainTex, i.uv);
				
				float2 overlayOffest = float2(i.uv.x + 0.5, i.uv.y + 0.5);
				fixed4 overlay = tex2D(_MainTex, overlayOffest);
				overlay.r = 0;
				overlay.a *= col.a;
				
				col = fixed4(col.r - overlay.r, col.g - overlay.g, col.b - overlay.b, col.a);
				
				return col;
			}
			ENDCG
		}
	}
}
