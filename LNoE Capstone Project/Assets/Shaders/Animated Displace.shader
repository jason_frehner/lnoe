﻿Shader "LNOE/AnimatedDisplace"
{
	Properties
	{
	    _MainTex("Texture", 2d) = "white" {}
	    _DisplaceTex("Displacement Texture", 2d) = "white" {}
	    _Magnitude("Magnitude", Range(0, 0.1)) = 1
	    _Color ("Text Color", Color) = (1,1,1,1)
	}
	
	SubShader
	{
		Tags
		{
			"PreviewType" = "Plane"
			"Queue" = "Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
		}
		
		Blend SrcAlpha OneMinusSrcAlpha
        ColorMask RGB
        Cull Off Lighting Off ZWrite Off
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
            sampler2D _DisplaceTex;
            float _Magnitude;
            uniform fixed4 _Color;
            
            // Vertext data from the mesh
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
			};

            // Vertext to Fragment
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				fixed4 color : COLOR;
			};

            // Convert appdata to v2f
			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex); // Gets the point on the screen relative to the object.
				o.color = v.color * _Color;
				o.uv = v.uv;
				return o;
			}

            

            // Manipulate the fragments
			float4 frag(v2f i) : SV_Target
			{
				float2 movingUV = float2(i.uv.x + _Time.x * 2, i.uv.y + _Time.x *2);
				
				float2 displacement = tex2D(_DisplaceTex, movingUV).xy;
				displacement = ((displacement * 2 ) - 1) * _Magnitude;
				
				float4 color = tex2D(_MainTex, i.uv + displacement);
				color *= i.color;
				return color;
			}
			ENDCG
		}
	}
}