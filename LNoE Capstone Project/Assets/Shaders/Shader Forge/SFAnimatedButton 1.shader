Shader "LNOE/AnimatedButtonTest" {
    Properties {
        _OverlayTexture ("Overlay Texture", 2D) = "white" {}
        _ButtonTexture ("Button Texture", 2D) = "white" {}
        _Blend ("Blend", Range(0, 1)) = 1
        _Overlayxoffset ("Overlay x offset", Range(-0.5, 0.5)) = 0
        _Overlayyoffest ("Overlay y offest", Range(-0.5, 0.5)) = 0
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _OverlayTexture; uniform float4 _OverlayTexture_ST;
            uniform sampler2D _ButtonTexture; uniform float4 _ButtonTexture_ST;
            uniform float _Blend;
            uniform float _Overlayxoffset;
            uniform float _Overlayyoffest;
            
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
            };
            
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            
            float4 frag(VertexOutput i) : COLOR {
                float4 red = float4(1,0,0,1);
                float4 green = float4(0,1,0,1);
                float4 blue = float4(0,0,1,1);
                
                float3 objectScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float verticalScale = objectScale.y / objectScale.x;
                
                float4 _ButtonTexture_var = tex2D(_ButtonTexture,TRANSFORM_TEX(i.uv, _ButtonTexture));
                clip(_ButtonTexture_var.a - 0.5);
                
                // Overlay
                float overlayHorizontalOffset = i.uv.r + _Overlayxoffset;
                float overlayVerticalOffset = (i.uv.g + _Overlayyoffest) * verticalScale;
                float2 overlayUV = float2(overlayHorizontalOffset, overlayVerticalOffset);          
                float4 _OverlayTexture_var = tex2D(_OverlayTexture, TRANSFORM_TEX(overlayUV, _OverlayTexture));
                
                // Combine
                float stepBG = step((_ButtonTexture_var.a / 10), (_OverlayTexture_var.a * _Blend));
                float stepOverlay = step((_OverlayTexture_var.a * _Blend), (_ButtonTexture_var.a / 10));
                float3 overlayLerp = lerp((stepBG * blue.rgb) + (stepOverlay * red.rgb), green.rgb, (stepBG * stepOverlay));
                            
                float3 finalColor = (overlayLerp.r * _ButtonTexture_var.rgb + overlayLerp.g * _OverlayTexture_var + overlayLerp.b * _OverlayTexture_var);               
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
