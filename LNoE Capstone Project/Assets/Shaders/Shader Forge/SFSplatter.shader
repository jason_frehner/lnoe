// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:1,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:True,atwp:True,stva:128,stmr:255,stmw:255,stcp:7,stps:1,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33862,y:32765,varname:node_3138,prsc:2|emission-3437-OUT,alpha-2626-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32960,y:32576,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Tex2d,id:7672,x:32022,y:32523,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_7672,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:456d153fd367b411b9be8f8b5a06c508,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:3097,x:32513,y:32712,varname:node_3097,prsc:2|A-7672-R,B-8261-OUT;n:type:ShaderForge.SFN_Subtract,id:313,x:31919,y:32919,varname:node_313,prsc:2|A-3838-OUT,B-3185-OUT;n:type:ShaderForge.SFN_Vector1,id:3185,x:31624,y:33030,varname:node_3185,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:5310,x:31693,y:33128,varname:node_5310,prsc:2,v1:2;n:type:ShaderForge.SFN_Subtract,id:9472,x:31919,y:33084,varname:node_9472,prsc:2|A-3838-OUT,B-5310-OUT;n:type:ShaderForge.SFN_Add,id:2626,x:32773,y:32896,varname:node_2626,prsc:2|A-3097-OUT,B-5786-OUT,C-8171-OUT;n:type:ShaderForge.SFN_Multiply,id:5786,x:32513,y:32917,varname:node_5786,prsc:2|A-7672-G,B-7841-OUT;n:type:ShaderForge.SFN_Multiply,id:8171,x:32513,y:33110,varname:node_8171,prsc:2|A-7672-B,B-7441-OUT;n:type:ShaderForge.SFN_Multiply,id:744,x:33288,y:32604,varname:node_744,prsc:2|A-7241-RGB,B-495-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:8261,x:32098,y:32744,varname:node_8261,prsc:2,min:0,max:1|IN-3838-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:7841,x:32109,y:32919,varname:node_7841,prsc:2,min:0,max:1|IN-313-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:7441,x:32109,y:33084,varname:node_7441,prsc:2,min:0,max:1|IN-9472-OUT;n:type:ShaderForge.SFN_Divide,id:495,x:33055,y:33029,varname:node_495,prsc:2|A-2626-OUT,B-6404-OUT;n:type:ShaderForge.SFN_Vector1,id:6404,x:32785,y:33079,varname:node_6404,prsc:2,v1:3;n:type:ShaderForge.SFN_RemapRange,id:9134,x:33266,y:32777,varname:node_9134,prsc:2,frmn:0,frmx:3,tomn:-1,tomx:0.3|IN-2626-OUT;n:type:ShaderForge.SFN_Blend,id:3437,x:33535,y:32757,varname:node_3437,prsc:2,blmd:5,clmp:True|SRC-744-OUT,DST-9134-OUT;n:type:ShaderForge.SFN_Time,id:4049,x:30732,y:32840,varname:node_4049,prsc:2;n:type:ShaderForge.SFN_RemapRange,id:4674,x:31318,y:32865,varname:node_4674,prsc:2,frmn:0,frmx:1,tomn:0,tomx:3|IN-9319-OUT;n:type:ShaderForge.SFN_Round,id:3838,x:31512,y:32865,varname:node_3838,prsc:2|IN-4674-OUT;n:type:ShaderForge.SFN_Divide,id:5086,x:30963,y:32865,varname:node_5086,prsc:2|A-4049-T,B-8290-OUT;n:type:ShaderForge.SFN_Vector1,id:8290,x:30732,y:33001,varname:node_8290,prsc:2,v1:10;n:type:ShaderForge.SFN_Frac,id:9319,x:31148,y:32865,varname:node_9319,prsc:2|IN-5086-OUT;proporder:7241-7672;pass:END;sub:END;*/

Shader "Shader Forge/Splatter" {
    Properties {
        _Color ("Color", Color) = (1,0,0,1)
        _MainTex ("MainTex", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        _Stencil ("Stencil ID", Float) = 0
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilComp ("Stencil Comparison", Float) = 8
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilOpFail ("Stencil Fail Operation", Float) = 0
        _StencilOpZFail ("Stencil Z-Fail Operation", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            Stencil {
                Ref [_Stencil]
                ReadMask [_StencilReadMask]
                WriteMask [_StencilWriteMask]
                Comp [_StencilComp]
                Pass [_StencilOp]
                Fail [_StencilOpFail]
                ZFail [_StencilOpZFail]
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 node_4049 = _Time;
                float node_5086 = (node_4049.g/10.0);
                float node_9319 = frac(node_5086);
                float node_3838 = round((node_9319*3.0+0.0));
                float node_2626 = ((_MainTex_var.r*clamp(node_3838,0,1))+(_MainTex_var.g*clamp((node_3838-1.0),0,1))+(_MainTex_var.b*clamp((node_3838-2.0),0,1)));
                float3 emissive = saturate(max((_Color.rgb*(node_2626/3.0)),(node_2626*0.4333333+-1.0)));
                float3 finalColor = emissive;
                return fixed4(finalColor,node_2626);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
