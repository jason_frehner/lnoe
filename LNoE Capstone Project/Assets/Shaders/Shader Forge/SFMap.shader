// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:True,atwp:True,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:1873,x:33315,y:32724,varname:node_1873,prsc:2|emission-1086-OUT,alpha-4805-A,clip-4805-A;n:type:ShaderForge.SFN_Tex2d,id:4805,x:32551,y:32729,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:True,tagnsco:False,tagnrm:False,tex:be8dac2e7402242e490faebc54fbbae3,ntxv:0,isnm:False|UVIN-486-OUT;n:type:ShaderForge.SFN_Multiply,id:1086,x:32966,y:32784,cmnt:RGB,varname:node_1086,prsc:2|A-4805-RGB,B-1401-OUT;n:type:ShaderForge.SFN_TexCoord,id:7988,x:30873,y:32676,varname:node_7988,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Tex2d,id:2570,x:31460,y:32619,ptovrint:False,ptlb:Displace,ptin:_Displace,varname:_MainTex_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:True,tagnsco:False,tagnrm:False,tex:437ae28ea546f491c995eef4fd536760,ntxv:0,isnm:False|UVIN-2909-OUT;n:type:ShaderForge.SFN_Time,id:2347,x:30450,y:32936,varname:node_2347,prsc:2;n:type:ShaderForge.SFN_Sin,id:2047,x:30724,y:32859,varname:node_2047,prsc:2|IN-2455-OUT;n:type:ShaderForge.SFN_Add,id:2042,x:31114,y:32740,varname:node_2042,prsc:2|A-7988-UVOUT,B-3812-OUT;n:type:ShaderForge.SFN_Slider,id:2455,x:30314,y:32835,ptovrint:False,ptlb:node_2455,ptin:_node_2455,varname:node_2455,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:17.09402,max:20;n:type:ShaderForge.SFN_RemapRange,id:3812,x:30914,y:32838,varname:node_3812,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-2047-OUT;n:type:ShaderForge.SFN_Slider,id:8342,x:30589,y:32583,ptovrint:False,ptlb:node_8342,ptin:_node_8342,varname:node_8342,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:2;n:type:ShaderForge.SFN_Multiply,id:2909,x:31273,y:32658,varname:node_2909,prsc:2|A-8342-OUT,B-2042-OUT;n:type:ShaderForge.SFN_Multiply,id:2091,x:31926,y:32750,varname:node_2091,prsc:2|A-5438-OUT,B-9681-G;n:type:ShaderForge.SFN_Add,id:486,x:32336,y:32714,varname:node_486,prsc:2|A-6038-OUT,B-9164-OUT;n:type:ShaderForge.SFN_ComponentMask,id:5438,x:31682,y:32666,varname:node_5438,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-2570-RGB;n:type:ShaderForge.SFN_RemapRange,id:9164,x:32117,y:32750,varname:node_9164,prsc:2,frmn:0,frmx:1,tomn:-0.002,tomx:0.002|IN-2091-OUT;n:type:ShaderForge.SFN_Tex2d,id:9681,x:31559,y:32853,ptovrint:False,ptlb:node_9681,ptin:_node_9681,varname:node_9681,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:c5f054df71cb1423abe25e5c39b43324,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Relay,id:6038,x:31473,y:32449,varname:node_6038,prsc:2|IN-7988-UVOUT;n:type:ShaderForge.SFN_Multiply,id:2256,x:32113,y:33033,varname:node_2256,prsc:2|A-9681-R,B-2047-OUT;n:type:ShaderForge.SFN_RemapRange,id:1401,x:32465,y:32978,varname:node_1401,prsc:2,frmn:-1,frmx:1,tomn:0.5,tomx:1|IN-2256-OUT;proporder:4805-2570-2455-8342-9681;pass:END;sub:END;*/

Shader "Shader Forge/SFMap" {
    Properties {
        [PerRendererData]_MainTex ("MainTex", 2D) = "white" {}
        [PerRendererData]_Displace ("Displace", 2D) = "white" {}
        _node_2455 ("node_2455", Range(0, 20)) = 17.09402
        _node_8342 ("node_8342", Range(0, 2)) = 1
        _node_9681 ("node_9681", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        _Stencil ("Stencil ID", Float) = 0
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilComp ("Stencil Comparison", Float) = 8
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilOpFail ("Stencil Fail Operation", Float) = 0
        _StencilOpZFail ("Stencil Z-Fail Operation", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            Stencil {
                Ref [_Stencil]
                ReadMask [_StencilReadMask]
                WriteMask [_StencilWriteMask]
                Comp [_StencilComp]
                Pass [_StencilOp]
                Fail [_StencilOpFail]
                ZFail [_StencilOpZFail]
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Displace; uniform float4 _Displace_ST;
            uniform float _node_2455;
            uniform float _node_8342;
            uniform sampler2D _node_9681; uniform float4 _node_9681_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float node_2047 = sin(_node_2455);
                float2 node_2909 = (_node_8342*(i.uv0+(node_2047*0.5+0.5)));
                float4 _Displace_var = tex2D(_Displace,TRANSFORM_TEX(node_2909, _Displace));
                float4 _node_9681_var = tex2D(_node_9681,TRANSFORM_TEX(i.uv0, _node_9681));
                float2 node_486 = (i.uv0+((_Displace_var.rgb.rg*_node_9681_var.g)*0.004+-0.002));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_486, _MainTex));
                clip(_MainTex_var.a - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = (_MainTex_var.rgb*((_node_9681_var.r*node_2047)*0.25+0.75));
                float3 finalColor = emissive;
                return fixed4(finalColor,_MainTex_var.a);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _Displace; uniform float4 _Displace_ST;
            uniform float _node_2455;
            uniform float _node_8342;
            uniform sampler2D _node_9681; uniform float4 _node_9681_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float node_2047 = sin(_node_2455);
                float2 node_2909 = (_node_8342*(i.uv0+(node_2047*0.5+0.5)));
                float4 _Displace_var = tex2D(_Displace,TRANSFORM_TEX(node_2909, _Displace));
                float4 _node_9681_var = tex2D(_node_9681,TRANSFORM_TEX(i.uv0, _node_9681));
                float2 node_486 = (i.uv0+((_Displace_var.rgb.rg*_node_9681_var.g)*0.004+-0.002));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_486, _MainTex));
                clip(_MainTex_var.a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
