// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:1,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,imps:False,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:1,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:6,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:1,qpre:4,rntp:5,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:32933,y:33234,varname:node_2865,prsc:2|emission-1230-OUT;n:type:ShaderForge.SFN_TexCoord,id:4219,x:31556,y:33355,cmnt:Default coordinates,varname:node_4219,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Relay,id:8397,x:32025,y:33319,cmnt:Refract here,varname:node_8397,prsc:2|IN-4219-UVOUT;n:type:ShaderForge.SFN_Relay,id:4676,x:32503,y:33379,cmnt:Modify color here,varname:node_4676,prsc:2|IN-7542-RGB;n:type:ShaderForge.SFN_Tex2dAsset,id:4430,x:31903,y:33489,ptovrint:False,ptlb:MainTex,ptin:_MainTex,cmnt:MainTex contains the color of the scene,varname:node_9933,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:8da86438290b7d64eb1d62723389c0d3,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:7542,x:32237,y:33439,varname:node_1672,prsc:2,tex:8da86438290b7d64eb1d62723389c0d3,ntxv:0,isnm:False|UVIN-8397-OUT,TEX-4430-TEX;n:type:ShaderForge.SFN_RemapRange,id:6956,x:31384,y:33155,varname:node_6956,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-5764-UVOUT;n:type:ShaderForge.SFN_Length,id:2065,x:31662,y:33151,varname:node_2065,prsc:2|IN-6956-OUT;n:type:ShaderForge.SFN_Multiply,id:1230,x:32707,y:33325,varname:node_1230,prsc:2|A-7179-OUT,B-4676-OUT;n:type:ShaderForge.SFN_Multiply,id:4473,x:32056,y:33033,varname:node_4473,prsc:2|A-4666-OUT,B-6389-G,C-2065-OUT;n:type:ShaderForge.SFN_Tex2d,id:6389,x:31662,y:32957,ptovrint:False,ptlb:node_6389,ptin:_node_6389,varname:node_6389,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:e0a4ba693c1e149e7844fef558ef4c1f,ntxv:0,isnm:False|UVIN-8202-OUT;n:type:ShaderForge.SFN_Color,id:3509,x:32035,y:32843,ptovrint:False,ptlb:node_3509,ptin:_node_3509,varname:node_3509,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:3215,x:32268,y:32865,varname:node_3215,prsc:2|A-3509-RGB,B-4473-OUT;n:type:ShaderForge.SFN_Add,id:7179,x:32484,y:33015,varname:node_7179,prsc:2|A-3215-OUT,B-1121-OUT,C-9349-OUT;n:type:ShaderForge.SFN_RemapRange,id:8202,x:31384,y:32962,varname:node_8202,prsc:2,frmn:0,frmx:1,tomn:0.25,tomx:0.75|IN-5764-UVOUT;n:type:ShaderForge.SFN_RemapRange,id:1121,x:32268,y:33033,varname:node_1121,prsc:2,frmn:0,frmx:1,tomn:1,tomx:-1|IN-4473-OUT;n:type:ShaderForge.SFN_TexCoord,id:5764,x:30907,y:33103,varname:node_5764,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Sin,id:9781,x:30948,y:32766,varname:node_9781,prsc:2|IN-3108-OUT;n:type:ShaderForge.SFN_Time,id:6748,x:30387,y:32601,varname:node_6748,prsc:2;n:type:ShaderForge.SFN_RemapRange,id:8047,x:31584,y:32670,varname:node_8047,prsc:2,frmn:-1,frmx:1,tomn:-0.25,tomx:0.25|IN-3392-OUT;n:type:ShaderForge.SFN_Relay,id:3108,x:30773,y:32709,varname:node_3108,prsc:2|IN-8516-OUT;n:type:ShaderForge.SFN_Vector1,id:5435,x:31596,y:32838,varname:node_5435,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:3392,x:31400,y:32670,varname:node_3392,prsc:2|A-9148-OUT,B-9781-OUT;n:type:ShaderForge.SFN_Add,id:4666,x:31837,y:32784,varname:node_4666,prsc:2|A-8047-OUT,B-5435-OUT;n:type:ShaderForge.SFN_Multiply,id:4839,x:30948,y:32572,varname:node_4839,prsc:2|A-2928-OUT,B-3108-OUT;n:type:ShaderForge.SFN_Vector1,id:2928,x:30675,y:32578,varname:node_2928,prsc:2,v1:2;n:type:ShaderForge.SFN_Sin,id:9148,x:31169,y:32572,varname:node_9148,prsc:2|IN-4839-OUT;n:type:ShaderForge.SFN_RemapRange,id:9349,x:32268,y:33205,varname:node_9349,prsc:2,frmn:0.5,frmx:1,tomn:0,tomx:-0.2|IN-2065-OUT;n:type:ShaderForge.SFN_Divide,id:8516,x:30596,y:32690,varname:node_8516,prsc:2|A-6748-T,B-4824-OUT;n:type:ShaderForge.SFN_Vector1,id:4824,x:30371,y:32745,varname:node_4824,prsc:2,v1:2;proporder:4430-6389-3509;pass:END;sub:END;*/

Shader "Shader Forge/SFVignett" {
    Properties {
        _MainTex ("MainTex", 2D) = "black" {}
        _node_6389 ("node_6389", 2D) = "white" {}
        _node_3509 ("node_3509", Color) = (1,0,0,1)
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay+1"
            "RenderType"="Overlay"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZTest Always
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _node_6389; uniform float4 _node_6389_ST;
            uniform float4 _node_3509;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_6748 = _Time;
                float node_3108 = (node_6748.g/2.0);
                float2 node_8202 = (i.uv0*0.5+0.25);
                float4 _node_6389_var = tex2D(_node_6389,TRANSFORM_TEX(node_8202, _node_6389));
                float node_2065 = length((i.uv0*2.0+-1.0));
                float node_4473 = ((((sin((2.0*node_3108))*sin(node_3108))*0.25+0.0)+0.5)*_node_6389_var.g*node_2065);
                float2 node_8397 = i.uv0; // Refract here
                float4 node_1672 = tex2D(_MainTex,TRANSFORM_TEX(node_8397, _MainTex));
                float3 emissive = (((_node_3509.rgb*node_4473)+(node_4473*-2.0+1.0)+(node_2065*-0.4+0.2))*node_1672.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
