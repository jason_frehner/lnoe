// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32977,y:32522,varname:node_3138,prsc:2|emission-1051-OUT,clip-5533-A;n:type:ShaderForge.SFN_Tex2d,id:8453,x:32002,y:32752,ptovrint:False,ptlb:Overlay Texture,ptin:_OverlayTexture,varname:node_8453,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1f4dc03d2d6e3486ebbe0145c876b4ac,ntxv:0,isnm:False|UVIN-3601-OUT;n:type:ShaderForge.SFN_Tex2d,id:5533,x:32074,y:31982,ptovrint:False,ptlb:Button Texture,ptin:_ButtonTexture,varname:node_5533,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:2602b392225473f488103de030d81dbc,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:8955,x:31776,y:32225,ptovrint:False,ptlb:Red,ptin:_Red,varname:node_8955,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_If,id:6112,x:32646,y:32451,varname:node_6112,prsc:2|A-1071-OUT,B-8994-OUT,GT-8955-RGB,EQ-2315-RGB,LT-5008-RGB;n:type:ShaderForge.SFN_Color,id:2315,x:31766,y:32402,ptovrint:False,ptlb:Green,ptin:_Green,varname:node_2315,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:5008,x:31766,y:32577,ptovrint:False,ptlb:Blue,ptin:_Blue,varname:node_5008,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_ChannelBlend,id:1051,x:32805,y:32621,varname:node_1051,prsc:2,chbt:0|M-6112-OUT,R-5533-RGB,G-8453-RGB,B-8453-RGB;n:type:ShaderForge.SFN_Slider,id:6000,x:31905,y:32987,ptovrint:False,ptlb:Blend,ptin:_Blend,varname:node_6000,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:8994,x:32279,y:32890,varname:node_8994,prsc:2|A-8453-A,B-6000-OUT;n:type:ShaderForge.SFN_Divide,id:1071,x:32275,y:32246,varname:node_1071,prsc:2|A-5533-A,B-1636-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1636,x:32018,y:32176,ptovrint:False,ptlb:Normalize BG,ptin:_NormalizeBG,varname:node_1636,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:10;n:type:ShaderForge.SFN_TexCoord,id:7036,x:30710,y:32466,varname:node_7036,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Slider,id:5957,x:30863,y:33045,ptovrint:False,ptlb:Overlay x offset,ptin:_Overlayxoffset,varname:node_5957,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-0.5,cur:0,max:0.5;n:type:ShaderForge.SFN_Slider,id:6950,x:30863,y:33149,ptovrint:False,ptlb:Overlay y offest,ptin:_Overlayyoffest,varname:node_6950,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-0.5,cur:0,max:0.5;n:type:ShaderForge.SFN_ObjectScale,id:7313,x:29438,y:32662,varname:node_7313,prsc:2,rcp:False;n:type:ShaderForge.SFN_Vector1,id:6824,x:29897,y:32437,varname:node_6824,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:5955,x:30618,y:32670,varname:node_5955,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:6949,x:30066,y:32808,varname:node_6949,prsc:2,v1:2;n:type:ShaderForge.SFN_Add,id:3601,x:31522,y:32982,varname:node_3601,prsc:2|A-9002-OUT,B-1-OUT;n:type:ShaderForge.SFN_Append,id:1,x:31280,y:33056,varname:node_1,prsc:2|A-5957-OUT,B-6950-OUT;n:type:ShaderForge.SFN_RemapRange,id:8832,x:31328,y:32584,varname:node_8832,prsc:2,frmn:0,frmx:1,tomn:0.25,tomx:0.75|IN-7036-UVOUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:9002,x:30889,y:32723,cmnt:Remap,varname:node_9002,prsc:2|IN-7036-UVOUT,IMIN-5955-OUT,IMAX-6824-OUT,OMIN-7382-OUT,OMAX-978-OUT;n:type:ShaderForge.SFN_Max,id:2775,x:29679,y:32687,varname:node_2775,prsc:2|A-7313-X,B-7313-Y;n:type:ShaderForge.SFN_Divide,id:547,x:30066,y:32646,varname:node_547,prsc:2|A-6824-OUT,B-6974-OUT;n:type:ShaderForge.SFN_Divide,id:9198,x:30286,y:32808,varname:node_9198,prsc:2|A-547-OUT,B-6949-OUT;n:type:ShaderForge.SFN_Add,id:978,x:30578,y:33039,varname:node_978,prsc:2|A-2489-OUT,B-9198-OUT;n:type:ShaderForge.SFN_Subtract,id:7382,x:30578,y:32853,varname:node_7382,prsc:2|A-2489-OUT,B-9198-OUT;n:type:ShaderForge.SFN_Vector1,id:2489,x:30184,y:32958,varname:node_2489,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:6974,x:29874,y:32625,varname:node_6974,prsc:2|A-2786-OUT,B-2775-OUT;n:type:ShaderForge.SFN_Vector1,id:2786,x:29606,y:32537,varname:node_2786,prsc:2,v1:1;proporder:8453-5533-2315-5008-6000-1636-8955-5957-6950;pass:END;sub:END;*/

Shader "Shader Forge/SFAnimatedButton" {
    Properties {
        _OverlayTexture ("Overlay Texture", 2D) = "white" {}
        _ButtonTexture ("Button Texture", 2D) = "white" {}
        _Green ("Green", Color) = (0,1,0,1)
        _Blue ("Blue", Color) = (0,0,1,1)
        _Blend ("Blend", Range(0, 1)) = 1
        _NormalizeBG ("Normalize BG", Float ) = 10
        [HideInInspector]_Red ("Red", Color) = (1,0,0,1)
        _Overlayxoffset ("Overlay x offset", Range(-0.5, 0.5)) = 0
        _Overlayyoffest ("Overlay y offest", Range(-0.5, 0.5)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _OverlayTexture; uniform float4 _OverlayTexture_ST;
            uniform sampler2D _ButtonTexture; uniform float4 _ButtonTexture_ST;
            uniform float4 _Red;
            uniform float4 _Green;
            uniform float4 _Blue;
            uniform float _Blend;
            uniform float _NormalizeBG;
            uniform float _Overlayxoffset;
            uniform float _Overlayyoffest;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
                float3 objScale = 1.0/recipObjScale;
                float4 _ButtonTexture_var = tex2D(_ButtonTexture,TRANSFORM_TEX(i.uv0, _ButtonTexture));
                clip(_ButtonTexture_var.a - 0.5);
////// Lighting:
////// Emissive:
                float node_5955 = 0.0;
                float node_6824 = 1.0;
                float node_2489 = 0.5;
                float node_6949 = 2.0;
                float node_9198 = ((node_6824/(1.0*max(objScale.r,objScale.g)))/node_6949);
                float node_7382 = (node_2489-node_9198);
                float2 node_3601 = ((node_7382 + ( (i.uv0 - node_5955) * ((node_2489+node_9198) - node_7382) ) / (node_6824 - node_5955))+float2(_Overlayxoffset,_Overlayyoffest));
                float4 _OverlayTexture_var = tex2D(_OverlayTexture,TRANSFORM_TEX(node_3601, _OverlayTexture));
                float node_6112_if_leA = step((_ButtonTexture_var.a/_NormalizeBG),(_OverlayTexture_var.a*_Blend));
                float node_6112_if_leB = step((_OverlayTexture_var.a*_Blend),(_ButtonTexture_var.a/_NormalizeBG));
                float3 node_6112 = lerp((node_6112_if_leA*_Blue.rgb)+(node_6112_if_leB*_Red.rgb),_Green.rgb,node_6112_if_leA*node_6112_if_leB);
                float3 emissive = (node_6112.r*_ButtonTexture_var.rgb + node_6112.g*_OverlayTexture_var.rgb + node_6112.b*_OverlayTexture_var.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _ButtonTexture; uniform float4 _ButtonTexture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _ButtonTexture_var = tex2D(_ButtonTexture,TRANSFORM_TEX(i.uv0, _ButtonTexture));
                clip(_ButtonTexture_var.a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
