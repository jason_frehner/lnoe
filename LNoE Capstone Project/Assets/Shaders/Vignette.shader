Shader "LNOE/Vignette" {
    Properties {
        _MainTex ("MainTex", 2D) = "black" {}
        _Frame ("Frame", 2D) = "white" {}
        _Magnitude ("Magnitude", Range(0,1)) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Overlay+1"
            "RenderType"="Overlay"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            ZTest Always
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
//            #define UNITY_PASS_FORWARDBASE
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            
            uniform sampler2D _MainTex; 
            uniform float4 _MainTex_ST;
            uniform sampler2D _Frame;
            uniform float4 _Frame_ST;
            float _Magnitude;
            
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            
            float4 frag(VertexOutput i) : COLOR {
                float4 red = float4(1,0,0,1); // Red
                float4 screenGrab = tex2D(_MainTex, TRANSFORM_TEX(i.uv0, _MainTex)); // Screen
                
                // Wave pattern based on time
                float timeWave1 = sin(_Time.g / 2.0);
                float timeWave2 = sin(_Time.g * 2.0);
                float timeWaveProduct = (timeWave1 * timeWave2 * 0.25); // value between -0.25 and 0.25
                
                // Texture frame
                float2 scaledUV = (i.uv0 * 0.5 + 0.25);
                float4 frame = tex2D(_Frame,TRANSFORM_TEX(scaledUV, _Frame));
                
                // Center fall off
                float centerFallOff = length(i.uv0 * 2.0 +- 1.0);
                
                // Animates the frame.
                float frameIntensity = (timeWaveProduct + 0.5) * frame.g * centerFallOff * _Magnitude; //frame.g holds the splater pattern.
                
                // Build the frame.
                float3 redIntensity = red.rgb * frameIntensity;
                float3 frameCutout = frameIntensity * -2.0 + 1.0; // normalize -1 to 1
                float3 edgeDarkness = centerFallOff * -0.4 + 0.2; // normalize -0.2 to 0.2
                float3 completeFrame = redIntensity + frameCutout + edgeDarkness;
                
                float3 finalColor = (completeFrame * screenGrab.rgb);
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
}
