Shader "LNOE/SplatterSlider" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _Step ("Amount of Splatter", Range(0, 3)) = 0
        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        _ColorMask ("Color Mask", Float) = 15
        
    }
    SubShader {
        
        Tags {
            "Queue"="Transparent"
            "PreviewType"="Plane"
        }
        
        Pass {
            Name "FORWARD"
        
            Tags {
                "LightMode"="ForwardBase"
            }
            
            Blend SrcAlpha OneMinusSrcAlpha
            
            Stencil {
                Comp NotEqual
            }
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma target 3.0
            
            uniform sampler2D _MainTex;
            uniform float4 _MainTex_ST; 
            
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
            };
            
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            
            float _Step;
            
            float4 frag(VertexOutput i) : COLOR {
                
                float4 red = float4(1,0,0,1);
            
                float4 splatterTexture = tex2D(_MainTex,i.uv);
                float splatterCombination = ( // Add the intensity of each splatter together.
                                                (splatterTexture.r * clamp(_Step, 0, 1)) +
                                                (splatterTexture.g * clamp((_Step - 1.0), 0, 1)) +
                                                (splatterTexture.b * clamp((_Step - 2.0), 0, 1))
                                            );
                float3 finalColor = saturate( // clamp
                                                max(
                                                       (red.rgb * (splatterCombination/3.0)), // Amount of red based on the average of splatterCombination.
                                                       (splatterCombination * 0.4333333 - 1.0) // Highlight
                                                   )
                                             );              
                return fixed4(finalColor,splatterCombination);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
