using NUnit.Framework;
// ReSharper disable CheckNamespace

namespace Tests
{
    [TestFixture]
    [Category("Game Session")]
    internal class GameSessionTests
    {
        private readonly GameSession gs = new GameSession();

        [Test]
        public void SetupSetsTheMissionMapAndHeroes()
        {
            gs.SetupSession();
            Assert.That(gs.GetCurrentMission() != null);
            Assert.That(gs.GetMapTiles() != null);
            Assert.That(gs.GetHeroCharacters() != null);
        }

        [Test]
        public void ZombieTypesIsNeverNull()
        {
            Assert.That(gs.GetZombieTypes() != null);
        }
    }
}