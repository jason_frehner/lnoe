using System;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.UI;
// ReSharper disable CheckNamespace

namespace Tests
{
	[TestFixture]
	[Category("Cards")]
	internal class CardsTests
	{
		[SetUp]
		public void CardsSetup()
		{
			PlayerPrefs.SetInt(Strings.PlayerPrefKeys.AutoSave, 0);
			PlayerPrefs.SetString(Strings.PlayerPrefKeys.Scenario, "Die Zombies, Die!");

			phases = new ZombieTurnPhases();
			ZTGameObject = new GameObject("ZombieManager");

			zt = ZTGameObject.AddComponent<ZombieTurn>();

			zt.Card = new GameObject();
			zt.Card.AddComponent<Animator>();
			zt.DebugCardList = new GameObject().AddComponent<Dropdown>();
			zt.FightRollResultParent = new GameObject();
			zt.FightRollResultPrefab = new GameObject();
			zt.FightRollResultPrefab.AddComponent<Button>();
			GameObject fightResultChild0 = new GameObject();
			fightResultChild0.AddComponent<Text>();
			fightResultChild0.transform.SetParent(zt.FightRollResultPrefab.transform);
			zt.FightPanel = new GameObject();
			zt.FightPanel.AddComponent<Animator>();
			zt.FightText = new GameObject().AddComponent<Text>();
			zt.GameLog = new GameObject().AddComponent<GameLog>();
			zt.HeroObjectivePanel = new GameObject();
			zt.MapImages = new GameObject().AddComponent<PlaceMapImages>();
			zt.MoveText = new GameObject().AddComponent<Text>();
			zt.MovePanel = new GameObject();
			zt.MovePanel.AddComponent<Animator>();
			zt.MoveZombieTypeText = new GameObject().AddComponent<Text>();
			zt.RemainsInPlay = new GameObject();
			zt.RemainsInPlay.transform.SetParent(new GameObject().transform);
			zt.RemainsInPlay.transform.parent.gameObject.AddComponent<RectTransform>();
			zt.ScenarioName = new GameObject().AddComponent<Text>();
			zt.SetupText = new GameObject().AddComponent<Text>();
			zt.ZombieObjectivePanel = new GameObject();
			zt.ZombiesOnBoardText = new GameObject().AddComponent<Text>();
		}

		[TearDown]
		public void CardsTearDown()
		{
			// ReSharper disable once AccessToStaticMemberViaDerivedType
			GameObject.DestroyImmediate(ZTGameObject);
		}

		private ZombieTurnPhases phases = new ZombieTurnPhases();

		// ReSharper disable once InconsistentNaming
		private GameObject ZTGameObject;
		private ZombieTurn zt;

		[Test]
		public void Cornered()
		{
			ZombieTurnPrep();
			int before = zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].FightDice;
			ZombieCard card = new Cornered();
			AddZombieTurnPhaseCard(card);
			Assert.That(zt.CardBeingPlayed.GetName() == "Cornered");
			zt.ContinueFromPlayingACard(false);
			int after = zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].FightDice;
			Assert.That(after == before + 2);
			Assert.That(!zt.IsZombiesWinOnTie());
		}

		[Test]
		public void LightsOut()
		{
			ZombieTurnPrep();
			ZombieCard card = new LightsOut();
			AddZombieTurnPhaseCard(card);
			Assert.That(zt.CardBeingPlayed.GetName() == "Lights Out");
			zt.CardBeingPlayed.CardSetup();
			Assert.That(!zt.CardBeingPlayed.GetBuilding().HasLightsOut);
			zt.ContinueFromPlayingACard(false);
			Assert.That(zt.CardBeingPlayed.GetBuilding().HasLightsOut);
			card.RemainsInPlayReverse();
			Assert.That(!zt.CardBeingPlayed.GetBuilding().HasLightsOut);
		}

		[Test]
		public void RelentlessAdvance()
		{
			ZombieTurnPrep();
			ZombieCard card = new RelentlessAdvance();
			zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].ZombiesOnBoard = 0;
			Assert.That(!card.Playable());
			zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].ZombiesOnBoard = 1;
			Assert.That(card.Playable());
			AddZombieTurnPhaseCard(card);
			Console.WriteLine("Card: " + zt.CardBeingPlayed.GetName());
			Assert.That(zt.CardBeingPlayed.GetName() == "Relentless Advance");
		}

		[Test]
		public void HeavyRain()
		{
			ZombieTurnPrep();
			ZombieCard card = new HeavyRain();
			// add 2, one will get discarded
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Heavy Rain");
		}

		[Test]
		public void ATownOverrun()
		{
			ZombieTurnPrep();
			ZombieCard card = new ATownOverrun();
			// add 2, one will get discarded
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "A Town Overrun");
		}

		[Test]
		public void MyGodTheyHaveTakenThe()
		{
			ZombieTurnPrep();
			ZombieCard card = new MyGodTheyveTakenThe();
			// add 2, one will get discarded
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "My God,\nThey've Taken the...");
			zt.CardBeingPlayed.CardSetup();
			Assert.That(!zt.CardBeingPlayed.GetBuilding().IsTakenOver);
			zt.ContinueFromPlayingACard(false);
			Assert.That(zt.CardBeingPlayed.GetBuilding().IsTakenOver);
			card.RemainsInPlayReverse();
			Assert.That(!zt.CardBeingPlayed.GetBuilding().IsTakenOver);
		}

		[Test]
		public void NewSpawningPit()
		{
			ZombieTurnPrep();
			ZombieCard card = new NewSpawningPit();
			Assert.That(card.Playable);
			// add 2, one will get discarded
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "New Spawning Pit");
			zt.CardBeingPlayed.CardSetup();
			Assert.That(!zt.CardBeingPlayed.GetBuilding().HasSpawningPit);
			zt.ContinueFromPlayingACard(false);
			Assert.That(zt.CardBeingPlayed.GetBuilding().HasSpawningPit);
		}

		[Test]
		public void OhTheHorror()
		{
			ZombieTurnPrep();
			zt.GetDecks().AddCardsToDeck();
			ZombieCard card = new OhTheHorror();
			// add 2, one will get discarded
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Oh the Horror!");

			int handSize = zt.GetDecks().GetHand().Count;
			Debug.Log(handSize);

			zt.CardBeingPlayed.CardSetup();
			zt.CardBeingPlayed.Effect();
			Debug.Log(zt.GetDecks().GetHand().Count);
			Assert.That(zt.GetDecks().GetHand().Count == 6); // full hand minus 1 plus 3
		}

		[Test]
		public void SurpriseAttack()
		{
			ZombieTurnPrep();
			ZombieCard card = new SurpriseAttack();
			zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].ZombiesInPool = 0;
			Assert.That(!card.Playable());
			zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].ZombiesInPool = 14;
			Assert.That(card.Playable());
			// add 2, one will get discarded
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Surprise Attack");
			zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].ZombiesInPool = 14;
			zt.CardBeingPlayed.CardSetup();
			zt.CardBeingPlayed.Effect();
			Debug.Log(zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].ZombiesInPool);
			Assert.That(zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].ZombiesInPool == 13);
		}

		[Test]
		public void Shamble()
		{
			ZombieTurnPrep();
			ZombieCard card = new Shamble();
			// add 2, one will get discarded
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Shamble");
		}

		[Test]
		public void Uuuurrrggghh()
		{
			ZombieTurnPrep();
			ZombieCard card = new Uuuurrrggghh();
			// add 2, one will get discarded
			AddZombieTurnPhaseCard(card, 2);
			zt.FightAction("normal");
			Assert.That(zt.CardBeingPlayed.GetName() == "Uuuurrrggghh!");

			zt.CardBeingPlayed.CardSetup();
			zt.ContinueFromPlayingACard(false);
			Assert.That(zt.FightRollResultParent.transform.childCount == 3);
		}

		[Test]
		public void Braains()
		{
			ZombieTurnPrep();
			ZombieCard card = new Braains();
			// add 2, one will get discarded
			AddCardToHand(card, 2);
			
//		zt.FightAction("normal");
//		zt.UpdateZombieFightRollvalues(new List<int>{0});
//		Assert.That(!card.Playable());
//		
//		zt.ClearFightRoll();
//		zt.FightAction("normal");
//		zt.UpdateZombieFightRollvalues(new List<int>{6});
//		Assert.That(!card.Playable());

			zt.ClearFightRoll();
			zt.FightAction("normal");
			zt.UpdateZombieFightRollvalues(new List<int> {5});
			Assert.That(card.Playable());

			zt.FightWouldDieTry();
			Assert.That(zt.CardBeingPlayed.GetName() == "Braains!");

			zt.CardBeingPlayed.CardSetup();
			zt.ContinueFromPlayingACard(false);
			Assert.That(zt.HighestFightRollValue() == 6);
		}

		[Test]
		public void UndeadHateTheLiving()
		{
			ZombieTurnPrep();
			ZombieCard card = new UndeadHateTheLiving();
			// add 2, one will get discarded
			AddCardToHand(card, 2);
			
//		zt.FightAction("normal");
//		zt.UpdateZombieFightRollvalues(new List<int>{3});
//		Assert.That(!card.Playable());

			zt.ClearFightRoll();
			zt.FightAction("normal");
			zt.UpdateZombieFightRollvalues(new List<int> {4});
			Assert.That(card.Playable());
			zt.FightWouldDieTry();
			Assert.That(zt.CardBeingPlayed.GetName() == "Undead Hate\nThe Living");
		}

		[Test]
		public void TheresTooMany()
		{
			ZombieTurnPrep();
			zt.Spawn = false;
			ZombieCard card = new TheresTooMany();
			zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].ZombiesInPool = 0;
			Assert.That(!card.Playable());
			zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].ZombiesInPool = 14;
			Assert.That(card.Playable());
			// add 2, one will get discarded
			AddZombieSpawnPhaseCard(card, 2);
			zt.FightWouldDieTry();
			Assert.That(zt.CardBeingPlayed.GetName() == "There's Too Many!");
			zt.ContinueFromPlayingACard(false);
			Assert.That(zt.GetGameStats().GetZombieTypes().GetZombieTypes()[0].ZombiesInPool < 14);
		}

		[Test]
		public void Resilient()
		{
			ZombieTurnPrep();
			ZombieCard card = new Resilient();
			AddCardToHand(card);
			zt.ZombieShotButton();
			Assert.That(zt.CardBeingPlayed.GetName() == "Resilient");
		}

		[Test]
		public void HauntedByThePast()
		{
			ZombieTurnPrep();
			ZombieCard card = new HauntedByThePast();
			AddZombieTurnPhaseCard(card);
			Assert.That(zt.CardBeingPlayed.GetName() == "Haunted By the Past");
		}

		[Test]
		// ReSharper disable once InconsistentNaming
		public void IDontTrustEm()
		{
			ZombieTurnPrep();
			ZombieCard card = new IDontTrustEm();
			AddZombieTurnPhaseCard(card);
			Assert.That(zt.CardBeingPlayed.GetName() == "I Don't Trust 'em");
		}

		[Test]
		public void IveGotToGetToThe()
		{
			ZombieTurnPrep();
			ZombieCard card = new IveGotToGetToThe();
			AddZombieTurnPhaseCard(card);
			Assert.That(zt.CardBeingPlayed.GetName() == "I've Got to\nGet to the...");
		}

		[Test]
		public void MyGodHesAZombie()
		{
			ZombieTurnPrep();
			ZombieCard card = new MyGodHesAZombie();
			zt.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool = 0;
			Assert.That(!card.Playable());
			zt.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool = 14;
			Assert.That(card.Playable());
			AddZombieTurnPhaseCard(card);
			Assert.That(zt.CardBeingPlayed.GetName() == "My God, He's A Zombie");
		}

		[Test]
		public void Overconfidence()
		{
			ZombieTurnPrep();
			ZombieCard card = new Overconfidence();
			AddZombieTurnPhaseCard(card);
			Assert.That(zt.CardBeingPlayed.GetName() == "Overconfidence");
		}

		[Test]
		public void TeenAngst()
		{
			ZombieTurnPrep();
			ZombieCard card = new TeenAngst();
			//TODO playable
			AddZombieTurnPhaseCard(card);
			Assert.That(zt.CardBeingPlayed.GetName() == "Teen Angst");
		}

		[Test]
		public void ThisCantBeHappening()
		{
			ZombieTurnPrep();
			ZombieCard card = new ThisCantBeHappening();
			AddZombieTurnPhaseCard(card);
			Assert.That(zt.CardBeingPlayed.GetName() == "This Can't\nBe Happening!");
		}

		[Test]
		public void LivingNightmare()
		{
			ZombieTurnPrep();
			ZombieCard card = new LivingNightmare();
			Assert.That(!card.Playable());
			zt.GetGameStats().GetZombieTypes().NewZombieHero("Sally", 3);
			Assert.That(card.Playable());
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Living Nightmare");
			card.Effect();
			Assert.That(zt.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Hero).BonusDice == 2);
		}

		[Test]
		public void LockedDoor()
		{
			ZombieTurnPrep();
			ZombieCard card = new LockedDoor();
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Locked Door");
			card.CardSetup();
			card.Effect();
			Assert.That(card.GetBuilding().HowManyLockedDoors() == 1);
		}

		[Test]
		public void NightThatNeverEnds()
		{
			zt.Start();
			zt.ViewableUI = null;
			ZombieCard card = new NightThatNeverEnds();
			AddCardToHand(card);
			Assert.That(!card.Playable());
			zt.GetDecks().DiscardACard(card);
			Assert.That(card.Playable());
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Night That Never Ends");
			//TODO effect
		}

		[Test]
		public void RottenBodies()
		{
			ZombieTurnPrep();
			ZombieCard card = new RottenBodies();
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Rotten Bodies");
			//TODO effect
		}

		[Test]
		public void TheresNoTimeLeaveIt()
		{
			ZombieTurnPrep();
			ZombieCard card = new TheresNoTimeLeaveIt();
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "There's No Time,\nLeave It!");
		}

		[Test]
		public void Loner()
		{
			ZombieTurnPrep();
			ZombieCard card = new Loner();
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Loner");
		}

		[Test]
		public void DesperateForFlesh()
		{
			ZombieTurnPrep();
			ZombieCard card = new DesperateForFlesh();
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Desperate for Flesh");
		}

		[Test]
		public void GrowingHunger()
		{
			ZombieTurnPrep();
			ZombieCard card = new GrowingHunger();
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Growing Hunger");
			zt.CardBeingPlayed.Effect();
			Assert.That(zt.GetGameStats().GetZombieTypes().TotalFightDiceNumber() == 1);
		}

		[Test]
		public void NowhereToHide()
		{
			ZombieTurnPrep();
			ZombieCard card = new NowhereToHide();
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Nowhere to hide");
		}

		[Test]
		public void TheyAreComingFromThe()
		{
			ZombieTurnPrep();
			ZombieCard card = new TheyreComingFromThe();
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "They're coming from the...");
			zt.CardBeingPlayed.CardSetup();
			zt.CardBeingPlayed.Effect();
			Assert.That(zt.CardBeingPlayed.GetBuilding().IsTakenOver);
			Assert.That(zt.CardBeingPlayed.GetBuilding().HasSpawningPit);
		}

		[Test]
		public void DraggingMeat()
		{
			ZombieTurnPrep();
			ZombieCard card = new DraggingMeat();
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Dragging Meat");
		}

		[Test]
		public void CaughtOffGuard()
		{
			ZombieTurnPrep();
			ZombieCard card = new CaughtOffGuard();
			AddZombieFightPhaseCard(card, 2);
			zt.FightAction("normal");
			Assert.That(zt.CardBeingPlayed.GetName() == "Caught Off-Guard");
		}

		[Test]
		public void FightingInstincts()
		{
			ZombieTurnPrep();
			ZombieCard card = new FightingInstincts();
			AddZombieFightPhaseCard(card, 2);
			zt.FightAction("normal");
			Assert.That(zt.CardBeingPlayed.GetName() == "Fighting Instincts");
		}

		[Test]
		public void Overwhelmed()
		{
			ZombieTurnPrep();
			ZombieCard card = new Overwhelmed();
			AddZombieFightPhaseCard(card, 2);
			zt.FightAction("normal");
			Assert.That(zt.CardBeingPlayed.GetName() == "Overwhelmed");
		}

		[Test]
		public void ScratchAndClaw()
		{
			ZombieTurnPrep();
			ZombieCard card = new ScratchAndClaw();
			AddZombieFightPhaseCard(card, 2);
			zt.FightAction("normal");
			Assert.That(zt.CardBeingPlayed.GetName() == "Scratch and Claw");
			zt.CardBeingPlayed.Effect();
			Assert.That(zt.GetGameStats().GetZombieTypes().TotalFightDiceNumber() == 1);
		}

		[Test]
		public void Bitten()
		{
			ZombieTurnPrep();
			ZombieCard card = new Bitten();
			AddZombieFightPhaseCard(card, 2);
			zt.FightAction("normal");
			zt.FightWouldDieTry();
			Assert.That(zt.CardBeingPlayed.GetName() == "Bitten");
		}

		[Test]
		public void TheyAreEverywhere()
		{
			ZombieTurnPrep();
			ZombieCard card = new TheyreEverywhere();
			zt.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool = 0;
			Assert.That(!card.Playable());
			zt.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool = 14;
			Assert.That(card.Playable());
			AddZombieSpawnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "They're Everywhere!");
			zt.CardBeingPlayed.CardSetup();
			zt.CardBeingPlayed.Effect();
			Assert.That(zt.GetGameStats().GetZombieTypes().GetZombiesOnBoardCount() > 0);
		}

		[Test]
		public void LetsSplitUp()
		{
			ZombieTurnPrep();
			ZombieCard card = new LetsSplitUp();
			AddZombieSpawnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Let's Split Up");
		}

		[Test]
		public void NowhereToRun()
		{
			ZombieTurnPrep();
			ZombieCard card = new NowhereToRun();
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Nowhere to Run");
		}

		[Test]
		public void ZombieSurge()
		{
			ZombieTurnPrep();
			ZombieCard card = new ZombieSurge();
			zt.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool = 0;
			Assert.That(!card.Playable());
			zt.GetGameStats().GetZombieTypes().GetZombieOfType(ZombieType.Normal).ZombiesInPool = 14;
			Assert.That(card.Playable());
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Zombie Surge");
			zt.CardBeingPlayed.CardSetup();
			zt.CardBeingPlayed.Effect();
			Debug.Log(zt.GetGameStats().GetZombieTypes().GetZombiesInPool() + " pool");
			Debug.Log(zt.GetGameStats().GetZombieTypes().GetZombiesOnBoardCount() + " board");
			Assert.That(zt.GetGameStats().GetZombieTypes().GetZombiesOnBoardCount() > 0);
		}

		[Test]
		public void Hopeless()
		{
			ZombieTurnPrep();
			ZombieCard card = new Hopeless();
			Assert.That(!card.Playable());
			zt.GetDecks().PutInToRemainsInPlay(new HeavyRain());
			Assert.That(zt.GetDecks().RemainsInPlayDeckSize() > 0);
			Assert.That(card.Playable());
			AddZombieTurnPhaseCard(card);
			Assert.That(zt.CardBeingPlayed.GetName() == "Hopeless");
			zt.CardBeingPlayed.CardSetup();
			zt.CardBeingPlayed.Effect();
			Assert.That(!card.Playable());
		}

		[Test]
		public void FightForSurvival()
		{
			ZombieTurnPrep();
			ZombieCard card = new FightForSurvival();
			// one will get discarded
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Fight for Survival");
			zt.GetDecks().DiscardACard(new HeavyRain());
			zt.CardBeingPlayed.Effect();
			Assert.That(zt.GetDecks().NumberOfDiscardedCards() == 1);
		}

		[Test]
		public void ToolsOfTheGrave()
		{
			ZombieTurnPrep();
			ZombieCard card = new ToolsOfTheGrave();
			// one will get discarded
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Tools of the Grave");
			zt.CardBeingPlayed.Effect();
			Assert.That(zt.GetGameStats().GetZombieTypes().TotalFightDiceNumber() == 1);
		}

		[Test]
		public void TideOfTheDead()
		{
			ZombieTurnPrep();
			ZombieCard card = new TideOfTheDead();
			Assert.That(!card.Playable());
			// one will get discarded
			AddCardToHand(card, 4);
			Assert.That(card.Playable());
			AddZombieTurnPhaseCard(card);
			Assert.That(zt.CardBeingPlayed.GetName() == "Tide of the Dead");
			zt.CardBeingPlayed.Effect();
			Assert.That(zt.GetDecks().NumberOfDiscardedCards() == 4);
		}

		[Test]
		public void DanceOfTheDead()
		{
			ZombieTurnPrep();
			ZombieCard card = new DanceOfTheDead();
			AddZombieTurnPhaseCard(card, 2);
			Assert.That(zt.CardBeingPlayed.GetName() == "Dance of the Dead");
		}
		
		private void ZombieTurnPrep()
		{
			zt.Start();
			zt.ViewableUI = null;
			zt.GetDecks().ClearAllDecks();
		}

		private void AddZombieTurnPhaseCard(ZombieCard card, int count = 1)
		{
			AddZombieTurnCard(card, count);
			phases.PhaseStartOfTurn();
		}

		private void AddZombieSpawnPhaseCard(ZombieCard card, int count = 1)
		{
			AddZombieTurnCard(card, count);
			phases.PhaseSpawnNewZombies();
		}

		private void AddZombieFightPhaseCard(ZombieCard card, int count = 1)
		{
			AddZombieTurnCard(card, count);
			phases.PhaseFightHeroes();
		}

		private void AddZombieTurnCard(ZombieCard card, int count = 1)
		{
			AddCardToHand(card, count);
			phases.SetZombieTurn(zt);
		}

		private void AddCardToHand(ZombieCard card, int count = 1)
		{
			for (int i = 0; i < count; i++)
				zt.GetDecks().AddCardToHand(card);
		}
	}
}