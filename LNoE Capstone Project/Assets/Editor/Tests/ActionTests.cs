using NUnit.Framework;
// ReSharper disable CheckNamespace

namespace Tests
{
    [TestFixture]
    [Category("Actions")]
    internal class ActionTests
    {
        [Test]
        public void D6IsOnlyBetween1And6()
        {
            int x = Actions.RollD6();
		
            Assert.That(x > 0 && x < 7);
        }

        [Test]
        public void D3IsOnlyBetween1And3()
        {
            int x = Actions.RollD3();
		
            Assert.That(x > 0 && x < 4);
        }

        [Test]
        public void FightRollHasTheCorrectNumberOfResults()
        {
            int[] test1 = Actions.FightRollArray(1);
            int[] test2 = Actions.FightRollArray(2);
            int[] test3 = Actions.FightRollArray(3);
            int[] test4 = Actions.FightRollArray(4);
            int[] test5 = Actions.FightRollArray(5);
            int[] test6 = Actions.FightRollArray(6);
		
            Assert.That(test1.Length == 1);
            Assert.That(test2.Length == 2);
            Assert.That(test3.Length == 3);
            Assert.That(test4.Length == 4);
            Assert.That(test5.Length == 5);
            Assert.That(test6.Length == 6);
        }
    }
}