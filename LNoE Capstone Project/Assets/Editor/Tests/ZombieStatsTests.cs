﻿using System;
using System.Globalization;
using NUnit.Framework;
// ReSharper disable CheckNamespace

namespace Tests
{
	[TestFixture]
	[Category("Zombie Stats")]
	internal class ZombieStatsTests
	{
		private readonly Zombies zs = new Zombies();

		[Test]
		public void DiceRollIsBetween1And6()
		{
			Assert.That(Actions.RollD6() > 0);
			Assert.That(Actions.RollD6() < 7);
		}

		[Test]
		public void WhenZombieDiesItReturnsToThePool()
		{
			zs.GetZombieOfType(ZombieType.Normal).ZombiesOnBoard = 10;
			int zombiesInPool = zs.GetZombiesInPool();
			zs.ZombieDied("Zombie");
			Assert.That(zs.GetZombiesInPool() == zombiesInPool + 1);
		}

		[Test]
		public void IntNormalizationReturnAValidAnswer()
		{
			double x = Normalization.IntNormalization(1, 2);
			Assert.That(Math.Abs(x) < 0.01, x.ToString(CultureInfo.CurrentCulture));
		}

		[Test]
		public void GasussianZeroDistanceOneResult()
		{
			Assert.That(Math.Abs(GaussianFunction.Gaussian(1, 1) - 1.0) < 0.01);
		}
	}
}

