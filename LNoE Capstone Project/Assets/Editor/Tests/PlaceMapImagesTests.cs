using NUnit.Framework;
using UnityEngine;

namespace Tests
{
    [TestFixture]
    [Category("Place Map Images")]
    internal class PlaceMapImagesTests
    {
	    [OneTimeSetUp]
    	public void PlaceMapImagesSetup()
    	{
    		pmi = new GameObject().AddComponent<PlaceMapImages>();
    		pmi.AssignMapImages();
    		pmi.SpriteRenderers = new SpriteRenderer[5];
    		pmi.SpriteRenderers[0] = new GameObject().AddComponent<SpriteRenderer>();
    		pmi.SpriteRenderers[1] = new GameObject().AddComponent<SpriteRenderer>();
    		pmi.SpriteRenderers[2] = new GameObject().AddComponent<SpriteRenderer>();
    		pmi.SpriteRenderers[3] = new GameObject().AddComponent<SpriteRenderer>();
    		pmi.SpriteRenderers[4] = new GameObject().AddComponent<SpriteRenderer>();
    	}

        private PlaceMapImages pmi;
        private readonly string[] coreOneNames = {"Open", "Airplane Hangar", "Bank", "Church", "Cornfield"};
        private readonly string[] coreTwoNames = {"Manor", "General Store", "High School", "Hospital", "Junkyard"};
        private readonly string[] ghNames = {"Open", "Antique", "Drug Store", "Diner", "Gas Station"};

        [Test]
    	public void SetupMapCoreOneTest()
    	{
    		pmi.SetMap(coreOneNames);
    
    		foreach(SpriteRenderer spriteRenderer in pmi.SpriteRenderers)
    		{
    			Assert.That(spriteRenderer.sprite != null);
    		}
    	}

        [Test]
    	public void SetupMapCoreTwoTest()
    	{
    		pmi.SetMap(coreTwoNames);
    
    		foreach(SpriteRenderer spriteRenderer in pmi.SpriteRenderers)
    		{
    			Assert.That(spriteRenderer.sprite != null);
    		}
    	}

        [Test]
    	public void SetupMapGrowingHungerTest()
    	{
    		pmi.SetMap(ghNames);
    
    		foreach(SpriteRenderer spriteRenderer in pmi.SpriteRenderers)
    		{
    			Assert.That(spriteRenderer.sprite != null);
    		}
    	}
    }
}