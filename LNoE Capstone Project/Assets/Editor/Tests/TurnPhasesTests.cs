using NUnit.Framework;
using UnityEngine;
using UnityEngine.UI;
// ReSharper disable CheckNamespace

namespace Tests
{
    [TestFixture]
    [Category("Zombie Turn Phases and Cards")]
    internal class TurnPhasesTests
    {
	    [OneTimeSetUp]
    	public void TurnPhasesSetup()
    	{	
    		if(ZTGameObject == null)
    		{
    			ZTGameObject = new GameObject("ZombieManager");
    		}
    
    		zt = ZTGameObject.AddComponent<ZombieTurn>();
    		zt.FightRollResultParent = new GameObject();
    		zt.ScenarioName = new GameObject().AddComponent<Text>();
    		zt.MapImages = new GameObject().AddComponent<PlaceMapImages>();
    		zt.HeroObjectivePanel = new GameObject();
    		zt.ZombieObjectivePanel = new GameObject();
    		zt.DebugCardList = new GameObject().AddComponent<Dropdown>();
    		zt.SetupText = new GameObject().AddComponent<Text>();
    		zt.MoveText = new GameObject().AddComponent<Text>();
    		zt.MoveZombieTypeText = new GameObject().AddComponent<Text>();
    		zt.MovePanel = new GameObject();
    		zt.MovePanel.AddComponent<Animator>();
    		zt.Card = new GameObject();
    		zt.Card.AddComponent<Animator>();
    		zt.ZombiesOnBoardText = new GameObject().AddComponent<Text>();
    	}

        private readonly ZombieTurnPhases phases = new ZombieTurnPhases();

        // ReSharper disable once InconsistentNaming
        private GameObject ZTGameObject = new GameObject("ZombieManager");
        private ZombieTurn zt;

        [Test]
    	public void PhaseObjectExist()
    	{
    		Assert.That(phases != null);
    	}

        [Test]
    	public void StartOfTurnDoneEndsInMovePhase()
    	{
    		zt.Start();
    		zt.ViewableUI = null;
    		zt.GetDecks().ClearAllDecks();
    		phases.SetZombieTurn(zt);
    		phases.PhaseStartOfTurn();
    		Assert.That(phases.TurnPhaseDelegate.Method.ToString() == "Void PhaseMoveZombies()");
    	}
    }
}