using System;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Audio;
// ReSharper disable CheckNamespace

namespace Tests
{
    [TestFixture]
    [Category("Adjust Audio")]
    internal class AdjustAudioTests
    {
        [OneTimeSetUp]
        public void AdjustAudioSetup()
        {
            adjustAudio.Mixer = Resources.Load<AudioMixer>(Strings.Resources.AudioMixer);
        }

        private static readonly GameObject GameObject = new GameObject();
        private readonly AdjustAudio adjustAudio = GameObject.AddComponent<AdjustAudio>();

        [Test]
        public void SaveMusicSavesToPreferences()
        {
            adjustAudio.SaveMusic(0);
            Assert.That(Math.Abs(PlayerPrefs.GetFloat(Strings.PlayerPrefKeys.Music)) < 0.01f);
        }

        [Test]
        public void SetSfxSavesToPreferences()
        {
            adjustAudio.SetSfx(0);
            Assert.That(Math.Abs(PlayerPrefs.GetFloat(Strings.PlayerPrefKeys.SFX)) < 0.01f);
        }
    }
}