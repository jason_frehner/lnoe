using System.Collections.Generic;
using Helper;
using NUnit.Framework;
// ReSharper disable CheckNamespace

namespace Tests
{
    [TestFixture]
    [Category("Building Selection AI")]
    internal class BuildingSelectionAITests
    {
        [Test]
        public void BuildingZombieChoiceAIIsNotNull()
        {
            Tiles t = new Tiles();
            t.GameTiles();

            Assert.That(t.BuildingZombieChoiceAi() != null);
        }

        [Test]
        public void BuildingToDestroy()
        {
            Tiles t = new Tiles();
            t.GameTiles();

            List<string> list = t.GetBuildingToDestroyPriority();
		
            DebugLogger.WriteLog(list);
            Assert.That(list != null);
        }
    }
}