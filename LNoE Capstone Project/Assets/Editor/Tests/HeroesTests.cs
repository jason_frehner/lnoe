using NUnit.Framework;
// ReSharper disable CheckNamespace

namespace Tests
{
    [TestFixture]
    [Category("Heroes")]
    internal class HeroesTests
    {
        private readonly Heroes heroes = new Heroes();

        [Test]
        public void GetHeroOptionsAfterHeroesInTheGameNotNull()
        {
            heroes.HeroesInTheGame();
            Assert.That(heroes.GetListOfHeroesInGame() != null);
            Assert.That(heroes.RandomHero() != null);
            Assert.That(heroes.GetPriorityHero() != null);
            Assert.That(heroes.RandomHeroWithKeyword(Hero.KeywordType.Student) != null);
            Assert.That(heroes.RandomHeroWithoutKeyword(Hero.KeywordType.Student) != null);
        }
    }
}