using NUnit.Framework;
// ReSharper disable CheckNamespace

namespace Tests
{
    [TestFixture]
    [Category("Buildings")]
    internal class BuildingsTests
    {
        [Test]
        public void GetRandomBuildingFromABankTile()
        {
            string s = new BankJunkyardGas().RandomBuilding().GetName();

            //throw new Exception(s);
            Assert.That(s != null, s);
        }

        [Test]
        public void RangeTotalInChurchTileIs21()
        {
            Assert.That(new ChurchGraveyardPolice().TileRangeTotal() == 21);
        }

        [Test]
        public void RollRandomBuildingIsNotNull()
        {
            Tiles t = new Tiles();
            t.GameTiles();
            string s = t.GetRandomBuildingInGameTiles().GetName();

            //throw new Exception(s);
            Assert.That(s != null, s);
        }

        [Test]
        public void ThereAreFourRandomGameTiles()
        {
            Tiles t = new Tiles();

            t.GameTiles();

            Assert.That(t.GetFourRandomTilesForGame().Count == 4);
        }
    }
}