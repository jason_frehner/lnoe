﻿using UnityEngine;
using UnityEditor;

// ReSharper disable once CheckNamespace
public class RBFEditor : EditorWindow
{
	private int numberInput = 7;
	int numberHidden = 5;
	int numberOutput = 2;

	private RadialBasisFunctionNetwork rbf;
	
	[MenuItem("RBF/RBF Window")]
	private static void Window()
	{
		GetWindow<RBFEditor>(false, "RBF Network");
	}

	private void OnGUI()
	{
		numberInput = EditorGUILayout.IntField("Number of Inputs:", numberInput);
		numberHidden = EditorGUILayout.IntField("Number of RBF nodes:", numberHidden);
		numberOutput = EditorGUILayout.IntField("Number of Outputs", numberOutput);
		
		// Download the raw data.
		if (GUILayout.Button("Download Data"))
		{
			GsfuUtilsLnoe.GetAllGameLogs(false);
			CloudConnectorCore.processedResponseCallback.AddListener(GsfuUtilsLnoe.ParseData);
			Debug.Log("Download Data");
		}
		
		// Normalize the data for use in the network.
		if (GUILayout.Button("Normalize Data"))
		{
			Normalization.ConvertRawData(FileIo.LoadData("raw_data.txt"));
		}
		
		// Train the Network.
		if (GUILayout.Button("Train"))
		{
			rbf = new RadialBasisFunctionNetwork(numberInput, numberHidden, numberOutput);
			string[][] data = FileIo.LoadData("converted.txt");
			double[][] readyData = Normalization.MakeDoubleMatrixFromStringMatrix(data);
			rbf.Train(readyData);
		}
		
		// Save the settings locally.
		if (GUILayout.Button("Save Network Settings"))
		{
			rbf.SaveAllNetworkSettings();
		}
		
		// Save the settings to the Google sheet.
		if (GUILayout.Button("Upload Settings"))
		{
			GsfuUtilsLnoe.SaveNetworkSettings(false);
		}
		
		// Download the settings from the Google sheet.
		if (GUILayout.Button("Download Settings"))
		{
			GsfuUtilsLnoe.GetNetworkSettings(false);
			CloudConnectorCore.processedResponseCallback.AddListener(GsfuUtilsLnoe.ParseData);
		}

		
	}
}
