﻿using Helper;
using UnityEngine;
using UnityEditor;

// ReSharper disable once CheckNamespace
public class DebugLogsEnable : EditorWindow
{
    [MenuItem("Tools/Debug Log/Enable")]
    private static void DebugEnable()
    {
        PlayerPrefs.SetInt(Strings.PlayerPrefKeys.DebugMode, 1);
        DebugLogger.WriteLog("Debug mode set to: " + PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode));
    }
    
    [MenuItem("Tools/Debug Log/Enable", true)]
    private static bool DebugEnableValidation()
    {
        return PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) != 1;
    }
    
    
    
    [MenuItem("Tools/Debug Log/Disable")]
    private static void DebugDisable()
    {
        PlayerPrefs.SetInt(Strings.PlayerPrefKeys.DebugMode, 0);
        DebugLogger.WriteLog("Debug mode set to: " + PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode));
    }
    
    [MenuItem("Tools/Debug Log/Disable", true)]
    private static bool DebugDisableValidation()
    {
        return PlayerPrefs.GetInt(Strings.PlayerPrefKeys.DebugMode) != 0;
    }
}

public class SaveScreenShot : EditorWindow
{
    [MenuItem("Tools/Screen Shot")]
    private static void ScreenShot()
    {
        string ss = "ScreenShot";
        if(!PlayerPrefs.HasKey(ss))
        {
            PlayerPrefs.SetInt(ss, 1);
        }
        
        ScreenCapture.CaptureScreenshot("ScreenShots/" + PlayerPrefs.GetInt(ss) + ".jpg");
    
        PlayerPrefs.SetInt(ss, PlayerPrefs.GetInt(ss) + 1);
    }

    [MenuItem("Tools/Screen Shot", true)]
    private static bool ScreenShotValidation()
    {
        return Application.isPlaying;
    }
}