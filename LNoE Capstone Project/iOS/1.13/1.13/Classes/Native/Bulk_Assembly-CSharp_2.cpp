﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// ZombieTurn/<FightRoll>c__AnonStorey1
struct U3CFightRollU3Ec__AnonStorey1_t3184696250;
// ZombieTurn
struct ZombieTurn_t3562480803;
// ZombieTurn/<PlayCard>c__Iterator0
struct U3CPlayCardU3Ec__Iterator0_t1992789720;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Animator
struct Animator_t434523843;
// System.String
struct String_t;
// ZombieCard
struct ZombieCard_t2237653742;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// ZombieTurnPhases
struct ZombieTurnPhases_t3127322021;
// ZombieTurnPhases/TurnPhaseDelegateDeclare
struct TurnPhaseDelegateDeclare_t3595605765;
// GameSession
struct GameSession_t4087811243;
// Zombies
struct Zombies_t3905046179;
// ZombieDeck
struct ZombieDeck_t3038342722;
// Mission
struct Mission_t4233471175;
// UnityEngine.Object
struct Object_t631007953;
// System.Collections.Generic.List`1<Zombie>
struct List_1_t4068628460;
// Zombie
struct Zombie_t2596553718;
// System.Collections.Generic.List`1<ZombieCard>
struct List_1_t3709728484;
// System.Collections.Generic.List`1<Mission/SpecialRulesTypes>
struct List_1_t3747392069;
// Mission/SpecialRulesTypes
struct SpecialRulesTypes_t2275317327;
// System.Collections.Generic.List`1<Mission/ObjectiveTypes>
struct List_1_t4188946901;
// Mission/ObjectiveTypes
struct ObjectiveTypes_t2716872159;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// Tiles
struct Tiles_t1457048987;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t827303578;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// TextAnimation
struct TextAnimation_t2694049294;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.Generic.List`1<MapTile>
struct List_1_t680461681;
// System.Comparison`1<Building>
struct Comparison_1_t1186187174;
// System.Func`2<MapTile,System.Int32>
struct Func_2_t38978152;
// System.Func`2<Building,System.Boolean>
struct Func_2_t137751404;
// Mission/ObjectiveTypes[]
struct ObjectiveTypesU5BU5D_t3041156038;
// Mission/SpecialRulesTypes[]
struct SpecialRulesTypesU5BU5D_t3438187414;
// ZombieCard[]
struct ZombieCardU5BU5D_t1796062619;
// Zombie[]
struct ZombieU5BU5D_t3743666035;
// System.Collections.Generic.List`1<Mission/ScenarioSearchItemTypes>
struct List_1_t3444701783;
// System.Func`2<ZombieCard,System.String>
struct Func_2_t166598193;
// Zombies/ZombiesOnBoard
struct ZombiesOnBoard_t3526012930;
// Zombies/ZombiesAreKillable
struct ZombiesAreKillable_t2501657832;
// Zombies/HeroZombieOnBoard
struct HeroZombieOnBoard_t1308641688;
// System.Func`2<Zombie,System.Int32>
struct Func_2_t3217696673;
// System.Func`2<Zombie,System.Boolean>
struct Func_2_t364038885;
// Missions
struct Missions_t3398870215;
// Heroes
struct Heroes_t1064386291;
// RadialBasisFunctionNetwork
struct RadialBasisFunctionNetwork_t3778331090;
// System.Double[]
struct DoubleU5BU5D_t3413330114;
// UnityEngine.Events.UnityAction`3<CloudConnectorCore/QueryType,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<System.String>>
struct UnityAction_3_t1112121581;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// Building
struct Building_t1411255995;
// GameData
struct GameData_t415813024;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.UI.Text
struct Text_t1901882714;
// GameLog
struct GameLog_t2697247111;
// PlaceMapImages
struct PlaceMapImages_t1484071588;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// ViewableUI
struct ViewableUI_t3820486676;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// System.Collections.Generic.List`1<GameSaveData/GameLogEntries>
struct List_1_t2151963976;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t1438173104;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t4040729994;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t2924027637;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t3520241082;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t3270282352;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t3180273144;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;

extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1685793073_RuntimeMethod_var;
extern String_t* _stringLiteral2432531900;
extern String_t* _stringLiteral1495087245;
extern String_t* _stringLiteral3905732946;
extern const uint32_t U3CPlayCardU3Ec__Iterator0_MoveNext_m3498982220_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CPlayCardU3Ec__Iterator0_Reset_m1814800537_RuntimeMethod_var;
extern const uint32_t U3CPlayCardU3Ec__Iterator0_Reset_m1814800537_MetadataUsageId;
extern String_t* _stringLiteral1478849746;
extern const uint32_t ZombieTurnPhases_IsDebugLog_m2641272074_MetadataUsageId;
extern RuntimeClass* TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var;
extern const RuntimeMethod* ZombieTurnPhases_PhaseStartOfTurn_m1047464964_RuntimeMethod_var;
extern const uint32_t ZombieTurnPhases_TurnDelegateStartOfTurn_m2008126874_MetadataUsageId;
extern const RuntimeMethod* ZombieTurnPhases_PhasePlayStartOfTurnCards_m2571742252_RuntimeMethod_var;
extern const RuntimeMethod* ZombieTurnPhases_PhaseDrawNewCards_m411032853_RuntimeMethod_var;
extern const RuntimeMethod* ZombieTurnPhases_PhasePlayImmediatelyCards_m457453206_RuntimeMethod_var;
extern const RuntimeMethod* ZombieTurnPhases_PhaseCheckIfNewZombiesSpawn_m1652064919_RuntimeMethod_var;
extern const RuntimeMethod* ZombieTurnPhases_PhaseMoveZombies_m3650847429_RuntimeMethod_var;
extern const RuntimeMethod* ZombieTurnPhases_PhaseFightHeroes_m3968111430_RuntimeMethod_var;
extern const RuntimeMethod* ZombieTurnPhases_PhaseFightResults_m2355902853_RuntimeMethod_var;
extern const RuntimeMethod* ZombieTurnPhases_PhaseSpawnNewZombies_m3815212178_RuntimeMethod_var;
extern const RuntimeMethod* ZombieTurnPhases_PhasePlayEndOfTurnCards_m2517377176_RuntimeMethod_var;
extern const RuntimeMethod* ZombieTurnPhases_PhaseHeroesTurn_m1774508636_RuntimeMethod_var;
extern const uint32_t ZombieTurnPhases_SetZombieTurnPhase_m1136649717_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3660277191;
extern String_t* _stringLiteral2878338918;
extern const uint32_t ZombieTurnPhases_PhaseStartOfTurn_m1047464964_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m3732267029_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3182922466_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m360690339_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m894815359_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1104284998_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m162813861_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2411096257_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m247343563_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m4044029282_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3650518843_RuntimeMethod_var;
extern String_t* _stringLiteral4289868831;
extern String_t* _stringLiteral2089403394;
extern String_t* _stringLiteral2642543365;
extern String_t* _stringLiteral1920273779;
extern const uint32_t ZombieTurnPhases_PhasePlayStartOfTurnCards_m2571742252_MetadataUsageId;
extern String_t* _stringLiteral2226740637;
extern const uint32_t ZombieTurnPhases_PhaseDrawNewCards_m411032853_MetadataUsageId;
extern String_t* _stringLiteral3747081117;
extern String_t* _stringLiteral1014326624;
extern String_t* _stringLiteral1271060643;
extern String_t* _stringLiteral2778673522;
extern String_t* _stringLiteral2862902950;
extern const uint32_t ZombieTurnPhases_PhasePlayImmediatelyCards_m457453206_MetadataUsageId;
extern RuntimeClass* Mission_t4233471175_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Contains_m915432422_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m3173494979_RuntimeMethod_var;
extern String_t* _stringLiteral557816803;
extern String_t* _stringLiteral2194267415;
extern String_t* _stringLiteral2014980447;
extern const uint32_t ZombieTurnPhases_PhaseCheckIfNewZombiesSpawn_m1652064919_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m3871794941_RuntimeMethod_var;
extern String_t* _stringLiteral66490830;
extern String_t* _stringLiteral1001145992;
extern String_t* _stringLiteral1959744090;
extern const uint32_t ZombieTurnPhases_PhaseMoveZombies_m3650847429_MetadataUsageId;
extern RuntimeClass* List_1_t3319525431_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Contains_m92166693_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m706204246_RuntimeMethod_var;
extern const RuntimeMethod* List_1_AddRange_m3621602103_RuntimeMethod_var;
extern String_t* _stringLiteral4237942660;
extern String_t* _stringLiteral183330392;
extern String_t* _stringLiteral2791739702;
extern const uint32_t ZombieTurnPhases_PhaseFightHeroes_m3968111430_MetadataUsageId;
extern String_t* _stringLiteral4161958653;
extern const uint32_t ZombieTurnPhases_PhaseFightResults_m2355902853_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3003607055;
extern String_t* _stringLiteral1589685702;
extern String_t* _stringLiteral228571493;
extern String_t* _stringLiteral2493621018;
extern String_t* _stringLiteral448053140;
extern const uint32_t ZombieTurnPhases_PhaseSpawnNewZombies_m3815212178_MetadataUsageId;
extern String_t* _stringLiteral3039913822;
extern String_t* _stringLiteral1602784365;
extern const uint32_t ZombieTurnPhases_PhasePlayEndOfTurnCards_m2517377176_MetadataUsageId;
extern String_t* _stringLiteral2651087753;
extern String_t* _stringLiteral2381261638;
extern String_t* _stringLiteral4271152816;
extern const uint32_t ZombieTurnPhases_PhaseHeroesTurn_m1774508636_MetadataUsageId;

struct Int32U5BU5D_t385246372;
struct ObjectU5BU5D_t2843939325;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TILES_T1457048987_H
#define TILES_T1457048987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tiles
struct  Tiles_t1457048987  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<MapTile> Tiles::coreMapTiles
	List_1_t680461681 * ___coreMapTiles_0;
	// System.Collections.Generic.List`1<MapTile> Tiles::growingHungerMapTiles
	List_1_t680461681 * ___growingHungerMapTiles_1;
	// System.Collections.Generic.List`1<MapTile> Tiles::fourRandomTilesForGame
	List_1_t680461681 * ___fourRandomTilesForGame_2;

public:
	inline static int32_t get_offset_of_coreMapTiles_0() { return static_cast<int32_t>(offsetof(Tiles_t1457048987, ___coreMapTiles_0)); }
	inline List_1_t680461681 * get_coreMapTiles_0() const { return ___coreMapTiles_0; }
	inline List_1_t680461681 ** get_address_of_coreMapTiles_0() { return &___coreMapTiles_0; }
	inline void set_coreMapTiles_0(List_1_t680461681 * value)
	{
		___coreMapTiles_0 = value;
		Il2CppCodeGenWriteBarrier((&___coreMapTiles_0), value);
	}

	inline static int32_t get_offset_of_growingHungerMapTiles_1() { return static_cast<int32_t>(offsetof(Tiles_t1457048987, ___growingHungerMapTiles_1)); }
	inline List_1_t680461681 * get_growingHungerMapTiles_1() const { return ___growingHungerMapTiles_1; }
	inline List_1_t680461681 ** get_address_of_growingHungerMapTiles_1() { return &___growingHungerMapTiles_1; }
	inline void set_growingHungerMapTiles_1(List_1_t680461681 * value)
	{
		___growingHungerMapTiles_1 = value;
		Il2CppCodeGenWriteBarrier((&___growingHungerMapTiles_1), value);
	}

	inline static int32_t get_offset_of_fourRandomTilesForGame_2() { return static_cast<int32_t>(offsetof(Tiles_t1457048987, ___fourRandomTilesForGame_2)); }
	inline List_1_t680461681 * get_fourRandomTilesForGame_2() const { return ___fourRandomTilesForGame_2; }
	inline List_1_t680461681 ** get_address_of_fourRandomTilesForGame_2() { return &___fourRandomTilesForGame_2; }
	inline void set_fourRandomTilesForGame_2(List_1_t680461681 * value)
	{
		___fourRandomTilesForGame_2 = value;
		Il2CppCodeGenWriteBarrier((&___fourRandomTilesForGame_2), value);
	}
};

struct Tiles_t1457048987_StaticFields
{
public:
	// System.Comparison`1<Building> Tiles::<>f__mg$cache0
	Comparison_1_t1186187174 * ___U3CU3Ef__mgU24cache0_3;
	// System.Func`2<MapTile,System.Int32> Tiles::<>f__am$cache0
	Func_2_t38978152 * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<MapTile,System.Int32> Tiles::<>f__am$cache1
	Func_2_t38978152 * ___U3CU3Ef__amU24cache1_5;
	// System.Func`2<MapTile,System.Int32> Tiles::<>f__am$cache2
	Func_2_t38978152 * ___U3CU3Ef__amU24cache2_6;
	// System.Func`2<Building,System.Boolean> Tiles::<>f__am$cache3
	Func_2_t137751404 * ___U3CU3Ef__amU24cache3_7;
	// System.Func`2<Building,System.Boolean> Tiles::<>f__am$cache4
	Func_2_t137751404 * ___U3CU3Ef__amU24cache4_8;
	// System.Func`2<Building,System.Boolean> Tiles::<>f__am$cache5
	Func_2_t137751404 * ___U3CU3Ef__amU24cache5_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline Comparison_1_t1186187174 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline Comparison_1_t1186187174 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(Comparison_1_t1186187174 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_2_t38978152 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_2_t38978152 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_2_t38978152 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Func_2_t38978152 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Func_2_t38978152 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Func_2_t38978152 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline Func_2_t38978152 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline Func_2_t38978152 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(Func_2_t38978152 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline Func_2_t137751404 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline Func_2_t137751404 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(Func_2_t137751404 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline Func_2_t137751404 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline Func_2_t137751404 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(Func_2_t137751404 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_9() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache5_9)); }
	inline Func_2_t137751404 * get_U3CU3Ef__amU24cache5_9() const { return ___U3CU3Ef__amU24cache5_9; }
	inline Func_2_t137751404 ** get_address_of_U3CU3Ef__amU24cache5_9() { return &___U3CU3Ef__amU24cache5_9; }
	inline void set_U3CU3Ef__amU24cache5_9(Func_2_t137751404 * value)
	{
		___U3CU3Ef__amU24cache5_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILES_T1457048987_H
#ifndef LIST_1_T4188946901_H
#define LIST_1_T4188946901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Mission/ObjectiveTypes>
struct  List_1_t4188946901  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectiveTypesU5BU5D_t3041156038* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4188946901, ____items_1)); }
	inline ObjectiveTypesU5BU5D_t3041156038* get__items_1() const { return ____items_1; }
	inline ObjectiveTypesU5BU5D_t3041156038** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectiveTypesU5BU5D_t3041156038* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4188946901, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4188946901, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4188946901_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ObjectiveTypesU5BU5D_t3041156038* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4188946901_StaticFields, ___EmptyArray_4)); }
	inline ObjectiveTypesU5BU5D_t3041156038* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ObjectiveTypesU5BU5D_t3041156038** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ObjectiveTypesU5BU5D_t3041156038* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4188946901_H
#ifndef OBJECTIVETYPES_T2716872159_H
#define OBJECTIVETYPES_T2716872159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mission/ObjectiveTypes
struct  ObjectiveTypes_t2716872159  : public RuntimeObject
{
public:
	// System.String Mission/ObjectiveTypes::Name
	String_t* ___Name_0;
	// System.Int32 Mission/ObjectiveTypes::Value
	int32_t ___Value_1;
	// System.Boolean Mission/ObjectiveTypes::Button
	bool ___Button_2;
	// System.String Mission/ObjectiveTypes::Audio
	String_t* ___Audio_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ObjectiveTypes_t2716872159, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(ObjectiveTypes_t2716872159, ___Value_1)); }
	inline int32_t get_Value_1() const { return ___Value_1; }
	inline int32_t* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(int32_t value)
	{
		___Value_1 = value;
	}

	inline static int32_t get_offset_of_Button_2() { return static_cast<int32_t>(offsetof(ObjectiveTypes_t2716872159, ___Button_2)); }
	inline bool get_Button_2() const { return ___Button_2; }
	inline bool* get_address_of_Button_2() { return &___Button_2; }
	inline void set_Button_2(bool value)
	{
		___Button_2 = value;
	}

	inline static int32_t get_offset_of_Audio_3() { return static_cast<int32_t>(offsetof(ObjectiveTypes_t2716872159, ___Audio_3)); }
	inline String_t* get_Audio_3() const { return ___Audio_3; }
	inline String_t** get_address_of_Audio_3() { return &___Audio_3; }
	inline void set_Audio_3(String_t* value)
	{
		___Audio_3 = value;
		Il2CppCodeGenWriteBarrier((&___Audio_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVETYPES_T2716872159_H
#ifndef LIST_1_T3747392069_H
#define LIST_1_T3747392069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Mission/SpecialRulesTypes>
struct  List_1_t3747392069  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SpecialRulesTypesU5BU5D_t3438187414* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3747392069, ____items_1)); }
	inline SpecialRulesTypesU5BU5D_t3438187414* get__items_1() const { return ____items_1; }
	inline SpecialRulesTypesU5BU5D_t3438187414** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SpecialRulesTypesU5BU5D_t3438187414* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3747392069, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3747392069, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3747392069_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	SpecialRulesTypesU5BU5D_t3438187414* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3747392069_StaticFields, ___EmptyArray_4)); }
	inline SpecialRulesTypesU5BU5D_t3438187414* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline SpecialRulesTypesU5BU5D_t3438187414** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(SpecialRulesTypesU5BU5D_t3438187414* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3747392069_H
#ifndef LIST_1_T3709728484_H
#define LIST_1_T3709728484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<ZombieCard>
struct  List_1_t3709728484  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ZombieCardU5BU5D_t1796062619* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3709728484, ____items_1)); }
	inline ZombieCardU5BU5D_t1796062619* get__items_1() const { return ____items_1; }
	inline ZombieCardU5BU5D_t1796062619** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ZombieCardU5BU5D_t1796062619* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3709728484, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3709728484, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3709728484_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ZombieCardU5BU5D_t1796062619* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3709728484_StaticFields, ___EmptyArray_4)); }
	inline ZombieCardU5BU5D_t1796062619* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ZombieCardU5BU5D_t1796062619** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ZombieCardU5BU5D_t1796062619* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3709728484_H
#ifndef LIST_1_T4068628460_H
#define LIST_1_T4068628460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Zombie>
struct  List_1_t4068628460  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ZombieU5BU5D_t3743666035* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4068628460, ____items_1)); }
	inline ZombieU5BU5D_t3743666035* get__items_1() const { return ____items_1; }
	inline ZombieU5BU5D_t3743666035** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ZombieU5BU5D_t3743666035* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4068628460, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4068628460, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4068628460_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ZombieU5BU5D_t3743666035* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4068628460_StaticFields, ___EmptyArray_4)); }
	inline ZombieU5BU5D_t3743666035* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ZombieU5BU5D_t3743666035** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ZombieU5BU5D_t3743666035* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4068628460_H
#ifndef MISSION_T4233471175_H
#define MISSION_T4233471175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mission
struct  Mission_t4233471175  : public RuntimeObject
{
public:
	// Mission/ObjectiveTypes Mission::BurnDownManor
	ObjectiveTypes_t2716872159 * ___BurnDownManor_0;
	// Mission/ObjectiveTypes Mission::Destroy3ZombiePits
	ObjectiveTypes_t2716872159 * ___Destroy3ZombiePits_1;
	// Mission/ObjectiveTypes Mission::EscapeTruckButton
	ObjectiveTypes_t2716872159 * ___EscapeTruckButton_2;
	// Mission/ObjectiveTypes Mission::Kill7PlagueCarriersButton
	ObjectiveTypes_t2716872159 * ___Kill7PlagueCarriersButton_3;
	// Mission/ObjectiveTypes Mission::Kill15Zombies
	ObjectiveTypes_t2716872159 * ___Kill15Zombies_4;
	// Mission/ObjectiveTypes Mission::Save4TownfolkButton
	ObjectiveTypes_t2716872159 * ___Save4TownfolkButton_5;
	// Mission/ObjectiveTypes Mission::Kill2Heroes
	ObjectiveTypes_t2716872159 * ___Kill2Heroes_7;
	// Mission/ObjectiveTypes Mission::Kill4Heroes
	ObjectiveTypes_t2716872159 * ___Kill4Heroes_8;
	// Mission/ObjectiveTypes Mission::ZombiesInHouse9
	ObjectiveTypes_t2716872159 * ___ZombiesInHouse9_9;
	// Mission/ObjectiveTypes Mission::HuntForSurvivors
	ObjectiveTypes_t2716872159 * ___HuntForSurvivors_10;
	// Mission/ObjectiveTypes Mission::FindZombieSkull
	ObjectiveTypes_t2716872159 * ___FindZombieSkull_11;
	// Mission/ObjectiveTypes Mission::GetSkullToDoctor
	ObjectiveTypes_t2716872159 * ___GetSkullToDoctor_12;
	// Mission/ObjectiveTypes Mission::KillDoctorZombie
	ObjectiveTypes_t2716872159 * ___KillDoctorZombie_13;
	// System.String Mission::Name
	String_t* ___Name_23;
	// System.String Mission::Text
	String_t* ___Text_24;
	// System.Collections.Generic.List`1<System.String> Mission::StartNarrativeText
	List_1_t3319525431 * ___StartNarrativeText_25;
	// System.Collections.Generic.List`1<System.String> Mission::NarrativeText
	List_1_t3319525431 * ___NarrativeText_26;
	// System.Collections.Generic.List`1<System.String> Mission::EndNarrativeText
	List_1_t3319525431 * ___EndNarrativeText_27;
	// System.Int32 Mission::Rounds
	int32_t ___Rounds_28;
	// System.Collections.Generic.List`1<Mission/ObjectiveTypes> Mission::HeroObjective
	List_1_t4188946901 * ___HeroObjective_29;
	// System.Collections.Generic.List`1<Mission/ObjectiveTypes> Mission::ZombieObjective
	List_1_t4188946901 * ___ZombieObjective_30;
	// System.Collections.Generic.List`1<Mission/ScenarioSearchItemTypes> Mission::ScenarioSearchItems
	List_1_t3444701783 * ___ScenarioSearchItems_31;
	// System.Collections.Generic.List`1<Mission/SpecialRulesTypes> Mission::SpecialRules
	List_1_t3747392069 * ___SpecialRules_32;
	// System.String Mission::MissionMovementTarget
	String_t* ___MissionMovementTarget_33;
	// System.String Mission::CenterTile
	String_t* ___CenterTile_34;
	// System.Boolean Mission::AllHeroStartLocationChanged
	bool ___AllHeroStartLocationChanged_35;
	// System.Boolean Mission::CenterHeroStartLocationChanged
	bool ___CenterHeroStartLocationChanged_36;
	// System.String Mission::HeroStartLocation
	String_t* ___HeroStartLocation_37;
	// System.String Mission::AudioWin
	String_t* ___AudioWin_38;
	// System.String Mission::AudioLose
	String_t* ___AudioLose_39;

public:
	inline static int32_t get_offset_of_BurnDownManor_0() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___BurnDownManor_0)); }
	inline ObjectiveTypes_t2716872159 * get_BurnDownManor_0() const { return ___BurnDownManor_0; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_BurnDownManor_0() { return &___BurnDownManor_0; }
	inline void set_BurnDownManor_0(ObjectiveTypes_t2716872159 * value)
	{
		___BurnDownManor_0 = value;
		Il2CppCodeGenWriteBarrier((&___BurnDownManor_0), value);
	}

	inline static int32_t get_offset_of_Destroy3ZombiePits_1() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Destroy3ZombiePits_1)); }
	inline ObjectiveTypes_t2716872159 * get_Destroy3ZombiePits_1() const { return ___Destroy3ZombiePits_1; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Destroy3ZombiePits_1() { return &___Destroy3ZombiePits_1; }
	inline void set_Destroy3ZombiePits_1(ObjectiveTypes_t2716872159 * value)
	{
		___Destroy3ZombiePits_1 = value;
		Il2CppCodeGenWriteBarrier((&___Destroy3ZombiePits_1), value);
	}

	inline static int32_t get_offset_of_EscapeTruckButton_2() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___EscapeTruckButton_2)); }
	inline ObjectiveTypes_t2716872159 * get_EscapeTruckButton_2() const { return ___EscapeTruckButton_2; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_EscapeTruckButton_2() { return &___EscapeTruckButton_2; }
	inline void set_EscapeTruckButton_2(ObjectiveTypes_t2716872159 * value)
	{
		___EscapeTruckButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___EscapeTruckButton_2), value);
	}

	inline static int32_t get_offset_of_Kill7PlagueCarriersButton_3() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Kill7PlagueCarriersButton_3)); }
	inline ObjectiveTypes_t2716872159 * get_Kill7PlagueCarriersButton_3() const { return ___Kill7PlagueCarriersButton_3; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Kill7PlagueCarriersButton_3() { return &___Kill7PlagueCarriersButton_3; }
	inline void set_Kill7PlagueCarriersButton_3(ObjectiveTypes_t2716872159 * value)
	{
		___Kill7PlagueCarriersButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___Kill7PlagueCarriersButton_3), value);
	}

	inline static int32_t get_offset_of_Kill15Zombies_4() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Kill15Zombies_4)); }
	inline ObjectiveTypes_t2716872159 * get_Kill15Zombies_4() const { return ___Kill15Zombies_4; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Kill15Zombies_4() { return &___Kill15Zombies_4; }
	inline void set_Kill15Zombies_4(ObjectiveTypes_t2716872159 * value)
	{
		___Kill15Zombies_4 = value;
		Il2CppCodeGenWriteBarrier((&___Kill15Zombies_4), value);
	}

	inline static int32_t get_offset_of_Save4TownfolkButton_5() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Save4TownfolkButton_5)); }
	inline ObjectiveTypes_t2716872159 * get_Save4TownfolkButton_5() const { return ___Save4TownfolkButton_5; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Save4TownfolkButton_5() { return &___Save4TownfolkButton_5; }
	inline void set_Save4TownfolkButton_5(ObjectiveTypes_t2716872159 * value)
	{
		___Save4TownfolkButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___Save4TownfolkButton_5), value);
	}

	inline static int32_t get_offset_of_Kill2Heroes_7() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Kill2Heroes_7)); }
	inline ObjectiveTypes_t2716872159 * get_Kill2Heroes_7() const { return ___Kill2Heroes_7; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Kill2Heroes_7() { return &___Kill2Heroes_7; }
	inline void set_Kill2Heroes_7(ObjectiveTypes_t2716872159 * value)
	{
		___Kill2Heroes_7 = value;
		Il2CppCodeGenWriteBarrier((&___Kill2Heroes_7), value);
	}

	inline static int32_t get_offset_of_Kill4Heroes_8() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Kill4Heroes_8)); }
	inline ObjectiveTypes_t2716872159 * get_Kill4Heroes_8() const { return ___Kill4Heroes_8; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Kill4Heroes_8() { return &___Kill4Heroes_8; }
	inline void set_Kill4Heroes_8(ObjectiveTypes_t2716872159 * value)
	{
		___Kill4Heroes_8 = value;
		Il2CppCodeGenWriteBarrier((&___Kill4Heroes_8), value);
	}

	inline static int32_t get_offset_of_ZombiesInHouse9_9() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___ZombiesInHouse9_9)); }
	inline ObjectiveTypes_t2716872159 * get_ZombiesInHouse9_9() const { return ___ZombiesInHouse9_9; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_ZombiesInHouse9_9() { return &___ZombiesInHouse9_9; }
	inline void set_ZombiesInHouse9_9(ObjectiveTypes_t2716872159 * value)
	{
		___ZombiesInHouse9_9 = value;
		Il2CppCodeGenWriteBarrier((&___ZombiesInHouse9_9), value);
	}

	inline static int32_t get_offset_of_HuntForSurvivors_10() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___HuntForSurvivors_10)); }
	inline ObjectiveTypes_t2716872159 * get_HuntForSurvivors_10() const { return ___HuntForSurvivors_10; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_HuntForSurvivors_10() { return &___HuntForSurvivors_10; }
	inline void set_HuntForSurvivors_10(ObjectiveTypes_t2716872159 * value)
	{
		___HuntForSurvivors_10 = value;
		Il2CppCodeGenWriteBarrier((&___HuntForSurvivors_10), value);
	}

	inline static int32_t get_offset_of_FindZombieSkull_11() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___FindZombieSkull_11)); }
	inline ObjectiveTypes_t2716872159 * get_FindZombieSkull_11() const { return ___FindZombieSkull_11; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_FindZombieSkull_11() { return &___FindZombieSkull_11; }
	inline void set_FindZombieSkull_11(ObjectiveTypes_t2716872159 * value)
	{
		___FindZombieSkull_11 = value;
		Il2CppCodeGenWriteBarrier((&___FindZombieSkull_11), value);
	}

	inline static int32_t get_offset_of_GetSkullToDoctor_12() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___GetSkullToDoctor_12)); }
	inline ObjectiveTypes_t2716872159 * get_GetSkullToDoctor_12() const { return ___GetSkullToDoctor_12; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_GetSkullToDoctor_12() { return &___GetSkullToDoctor_12; }
	inline void set_GetSkullToDoctor_12(ObjectiveTypes_t2716872159 * value)
	{
		___GetSkullToDoctor_12 = value;
		Il2CppCodeGenWriteBarrier((&___GetSkullToDoctor_12), value);
	}

	inline static int32_t get_offset_of_KillDoctorZombie_13() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___KillDoctorZombie_13)); }
	inline ObjectiveTypes_t2716872159 * get_KillDoctorZombie_13() const { return ___KillDoctorZombie_13; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_KillDoctorZombie_13() { return &___KillDoctorZombie_13; }
	inline void set_KillDoctorZombie_13(ObjectiveTypes_t2716872159 * value)
	{
		___KillDoctorZombie_13 = value;
		Il2CppCodeGenWriteBarrier((&___KillDoctorZombie_13), value);
	}

	inline static int32_t get_offset_of_Name_23() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Name_23)); }
	inline String_t* get_Name_23() const { return ___Name_23; }
	inline String_t** get_address_of_Name_23() { return &___Name_23; }
	inline void set_Name_23(String_t* value)
	{
		___Name_23 = value;
		Il2CppCodeGenWriteBarrier((&___Name_23), value);
	}

	inline static int32_t get_offset_of_Text_24() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Text_24)); }
	inline String_t* get_Text_24() const { return ___Text_24; }
	inline String_t** get_address_of_Text_24() { return &___Text_24; }
	inline void set_Text_24(String_t* value)
	{
		___Text_24 = value;
		Il2CppCodeGenWriteBarrier((&___Text_24), value);
	}

	inline static int32_t get_offset_of_StartNarrativeText_25() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___StartNarrativeText_25)); }
	inline List_1_t3319525431 * get_StartNarrativeText_25() const { return ___StartNarrativeText_25; }
	inline List_1_t3319525431 ** get_address_of_StartNarrativeText_25() { return &___StartNarrativeText_25; }
	inline void set_StartNarrativeText_25(List_1_t3319525431 * value)
	{
		___StartNarrativeText_25 = value;
		Il2CppCodeGenWriteBarrier((&___StartNarrativeText_25), value);
	}

	inline static int32_t get_offset_of_NarrativeText_26() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___NarrativeText_26)); }
	inline List_1_t3319525431 * get_NarrativeText_26() const { return ___NarrativeText_26; }
	inline List_1_t3319525431 ** get_address_of_NarrativeText_26() { return &___NarrativeText_26; }
	inline void set_NarrativeText_26(List_1_t3319525431 * value)
	{
		___NarrativeText_26 = value;
		Il2CppCodeGenWriteBarrier((&___NarrativeText_26), value);
	}

	inline static int32_t get_offset_of_EndNarrativeText_27() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___EndNarrativeText_27)); }
	inline List_1_t3319525431 * get_EndNarrativeText_27() const { return ___EndNarrativeText_27; }
	inline List_1_t3319525431 ** get_address_of_EndNarrativeText_27() { return &___EndNarrativeText_27; }
	inline void set_EndNarrativeText_27(List_1_t3319525431 * value)
	{
		___EndNarrativeText_27 = value;
		Il2CppCodeGenWriteBarrier((&___EndNarrativeText_27), value);
	}

	inline static int32_t get_offset_of_Rounds_28() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Rounds_28)); }
	inline int32_t get_Rounds_28() const { return ___Rounds_28; }
	inline int32_t* get_address_of_Rounds_28() { return &___Rounds_28; }
	inline void set_Rounds_28(int32_t value)
	{
		___Rounds_28 = value;
	}

	inline static int32_t get_offset_of_HeroObjective_29() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___HeroObjective_29)); }
	inline List_1_t4188946901 * get_HeroObjective_29() const { return ___HeroObjective_29; }
	inline List_1_t4188946901 ** get_address_of_HeroObjective_29() { return &___HeroObjective_29; }
	inline void set_HeroObjective_29(List_1_t4188946901 * value)
	{
		___HeroObjective_29 = value;
		Il2CppCodeGenWriteBarrier((&___HeroObjective_29), value);
	}

	inline static int32_t get_offset_of_ZombieObjective_30() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___ZombieObjective_30)); }
	inline List_1_t4188946901 * get_ZombieObjective_30() const { return ___ZombieObjective_30; }
	inline List_1_t4188946901 ** get_address_of_ZombieObjective_30() { return &___ZombieObjective_30; }
	inline void set_ZombieObjective_30(List_1_t4188946901 * value)
	{
		___ZombieObjective_30 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieObjective_30), value);
	}

	inline static int32_t get_offset_of_ScenarioSearchItems_31() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___ScenarioSearchItems_31)); }
	inline List_1_t3444701783 * get_ScenarioSearchItems_31() const { return ___ScenarioSearchItems_31; }
	inline List_1_t3444701783 ** get_address_of_ScenarioSearchItems_31() { return &___ScenarioSearchItems_31; }
	inline void set_ScenarioSearchItems_31(List_1_t3444701783 * value)
	{
		___ScenarioSearchItems_31 = value;
		Il2CppCodeGenWriteBarrier((&___ScenarioSearchItems_31), value);
	}

	inline static int32_t get_offset_of_SpecialRules_32() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___SpecialRules_32)); }
	inline List_1_t3747392069 * get_SpecialRules_32() const { return ___SpecialRules_32; }
	inline List_1_t3747392069 ** get_address_of_SpecialRules_32() { return &___SpecialRules_32; }
	inline void set_SpecialRules_32(List_1_t3747392069 * value)
	{
		___SpecialRules_32 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialRules_32), value);
	}

	inline static int32_t get_offset_of_MissionMovementTarget_33() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___MissionMovementTarget_33)); }
	inline String_t* get_MissionMovementTarget_33() const { return ___MissionMovementTarget_33; }
	inline String_t** get_address_of_MissionMovementTarget_33() { return &___MissionMovementTarget_33; }
	inline void set_MissionMovementTarget_33(String_t* value)
	{
		___MissionMovementTarget_33 = value;
		Il2CppCodeGenWriteBarrier((&___MissionMovementTarget_33), value);
	}

	inline static int32_t get_offset_of_CenterTile_34() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___CenterTile_34)); }
	inline String_t* get_CenterTile_34() const { return ___CenterTile_34; }
	inline String_t** get_address_of_CenterTile_34() { return &___CenterTile_34; }
	inline void set_CenterTile_34(String_t* value)
	{
		___CenterTile_34 = value;
		Il2CppCodeGenWriteBarrier((&___CenterTile_34), value);
	}

	inline static int32_t get_offset_of_AllHeroStartLocationChanged_35() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___AllHeroStartLocationChanged_35)); }
	inline bool get_AllHeroStartLocationChanged_35() const { return ___AllHeroStartLocationChanged_35; }
	inline bool* get_address_of_AllHeroStartLocationChanged_35() { return &___AllHeroStartLocationChanged_35; }
	inline void set_AllHeroStartLocationChanged_35(bool value)
	{
		___AllHeroStartLocationChanged_35 = value;
	}

	inline static int32_t get_offset_of_CenterHeroStartLocationChanged_36() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___CenterHeroStartLocationChanged_36)); }
	inline bool get_CenterHeroStartLocationChanged_36() const { return ___CenterHeroStartLocationChanged_36; }
	inline bool* get_address_of_CenterHeroStartLocationChanged_36() { return &___CenterHeroStartLocationChanged_36; }
	inline void set_CenterHeroStartLocationChanged_36(bool value)
	{
		___CenterHeroStartLocationChanged_36 = value;
	}

	inline static int32_t get_offset_of_HeroStartLocation_37() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___HeroStartLocation_37)); }
	inline String_t* get_HeroStartLocation_37() const { return ___HeroStartLocation_37; }
	inline String_t** get_address_of_HeroStartLocation_37() { return &___HeroStartLocation_37; }
	inline void set_HeroStartLocation_37(String_t* value)
	{
		___HeroStartLocation_37 = value;
		Il2CppCodeGenWriteBarrier((&___HeroStartLocation_37), value);
	}

	inline static int32_t get_offset_of_AudioWin_38() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___AudioWin_38)); }
	inline String_t* get_AudioWin_38() const { return ___AudioWin_38; }
	inline String_t** get_address_of_AudioWin_38() { return &___AudioWin_38; }
	inline void set_AudioWin_38(String_t* value)
	{
		___AudioWin_38 = value;
		Il2CppCodeGenWriteBarrier((&___AudioWin_38), value);
	}

	inline static int32_t get_offset_of_AudioLose_39() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___AudioLose_39)); }
	inline String_t* get_AudioLose_39() const { return ___AudioLose_39; }
	inline String_t** get_address_of_AudioLose_39() { return &___AudioLose_39; }
	inline void set_AudioLose_39(String_t* value)
	{
		___AudioLose_39 = value;
		Il2CppCodeGenWriteBarrier((&___AudioLose_39), value);
	}
};

struct Mission_t4233471175_StaticFields
{
public:
	// Mission/ObjectiveTypes Mission::Destroy6Buildings
	ObjectiveTypes_t2716872159 * ___Destroy6Buildings_6;
	// Mission/SpecialRulesTypes Mission::FreeSearchMarkers
	SpecialRulesTypes_t2275317327 * ___FreeSearchMarkers_14;
	// Mission/SpecialRulesTypes Mission::HeroesReplenish
	SpecialRulesTypes_t2275317327 * ___HeroesReplenish_15;
	// Mission/SpecialRulesTypes Mission::HeroStartingCards1
	SpecialRulesTypes_t2275317327 * ___HeroStartingCards1_16;
	// Mission/SpecialRulesTypes Mission::HeroStartingCards2
	SpecialRulesTypes_t2275317327 * ___HeroStartingCards2_17;
	// Mission/SpecialRulesTypes Mission::PlagueCarriers
	SpecialRulesTypes_t2275317327 * ___PlagueCarriers_18;
	// Mission/SpecialRulesTypes Mission::WellStockedBuilding
	SpecialRulesTypes_t2275317327 * ___WellStockedBuilding_19;
	// Mission/SpecialRulesTypes Mission::ZombieAutoSpawn
	SpecialRulesTypes_t2275317327 * ___ZombieAutoSpawn_20;
	// Mission/SpecialRulesTypes Mission::ZombieGraveDead
	SpecialRulesTypes_t2275317327 * ___ZombieGraveDead_21;
	// Mission/SpecialRulesTypes Mission::ZombieHorde21
	SpecialRulesTypes_t2275317327 * ___ZombieHorde21_22;

public:
	inline static int32_t get_offset_of_Destroy6Buildings_6() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___Destroy6Buildings_6)); }
	inline ObjectiveTypes_t2716872159 * get_Destroy6Buildings_6() const { return ___Destroy6Buildings_6; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Destroy6Buildings_6() { return &___Destroy6Buildings_6; }
	inline void set_Destroy6Buildings_6(ObjectiveTypes_t2716872159 * value)
	{
		___Destroy6Buildings_6 = value;
		Il2CppCodeGenWriteBarrier((&___Destroy6Buildings_6), value);
	}

	inline static int32_t get_offset_of_FreeSearchMarkers_14() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___FreeSearchMarkers_14)); }
	inline SpecialRulesTypes_t2275317327 * get_FreeSearchMarkers_14() const { return ___FreeSearchMarkers_14; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_FreeSearchMarkers_14() { return &___FreeSearchMarkers_14; }
	inline void set_FreeSearchMarkers_14(SpecialRulesTypes_t2275317327 * value)
	{
		___FreeSearchMarkers_14 = value;
		Il2CppCodeGenWriteBarrier((&___FreeSearchMarkers_14), value);
	}

	inline static int32_t get_offset_of_HeroesReplenish_15() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___HeroesReplenish_15)); }
	inline SpecialRulesTypes_t2275317327 * get_HeroesReplenish_15() const { return ___HeroesReplenish_15; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_HeroesReplenish_15() { return &___HeroesReplenish_15; }
	inline void set_HeroesReplenish_15(SpecialRulesTypes_t2275317327 * value)
	{
		___HeroesReplenish_15 = value;
		Il2CppCodeGenWriteBarrier((&___HeroesReplenish_15), value);
	}

	inline static int32_t get_offset_of_HeroStartingCards1_16() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___HeroStartingCards1_16)); }
	inline SpecialRulesTypes_t2275317327 * get_HeroStartingCards1_16() const { return ___HeroStartingCards1_16; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_HeroStartingCards1_16() { return &___HeroStartingCards1_16; }
	inline void set_HeroStartingCards1_16(SpecialRulesTypes_t2275317327 * value)
	{
		___HeroStartingCards1_16 = value;
		Il2CppCodeGenWriteBarrier((&___HeroStartingCards1_16), value);
	}

	inline static int32_t get_offset_of_HeroStartingCards2_17() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___HeroStartingCards2_17)); }
	inline SpecialRulesTypes_t2275317327 * get_HeroStartingCards2_17() const { return ___HeroStartingCards2_17; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_HeroStartingCards2_17() { return &___HeroStartingCards2_17; }
	inline void set_HeroStartingCards2_17(SpecialRulesTypes_t2275317327 * value)
	{
		___HeroStartingCards2_17 = value;
		Il2CppCodeGenWriteBarrier((&___HeroStartingCards2_17), value);
	}

	inline static int32_t get_offset_of_PlagueCarriers_18() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___PlagueCarriers_18)); }
	inline SpecialRulesTypes_t2275317327 * get_PlagueCarriers_18() const { return ___PlagueCarriers_18; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_PlagueCarriers_18() { return &___PlagueCarriers_18; }
	inline void set_PlagueCarriers_18(SpecialRulesTypes_t2275317327 * value)
	{
		___PlagueCarriers_18 = value;
		Il2CppCodeGenWriteBarrier((&___PlagueCarriers_18), value);
	}

	inline static int32_t get_offset_of_WellStockedBuilding_19() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___WellStockedBuilding_19)); }
	inline SpecialRulesTypes_t2275317327 * get_WellStockedBuilding_19() const { return ___WellStockedBuilding_19; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_WellStockedBuilding_19() { return &___WellStockedBuilding_19; }
	inline void set_WellStockedBuilding_19(SpecialRulesTypes_t2275317327 * value)
	{
		___WellStockedBuilding_19 = value;
		Il2CppCodeGenWriteBarrier((&___WellStockedBuilding_19), value);
	}

	inline static int32_t get_offset_of_ZombieAutoSpawn_20() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___ZombieAutoSpawn_20)); }
	inline SpecialRulesTypes_t2275317327 * get_ZombieAutoSpawn_20() const { return ___ZombieAutoSpawn_20; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_ZombieAutoSpawn_20() { return &___ZombieAutoSpawn_20; }
	inline void set_ZombieAutoSpawn_20(SpecialRulesTypes_t2275317327 * value)
	{
		___ZombieAutoSpawn_20 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieAutoSpawn_20), value);
	}

	inline static int32_t get_offset_of_ZombieGraveDead_21() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___ZombieGraveDead_21)); }
	inline SpecialRulesTypes_t2275317327 * get_ZombieGraveDead_21() const { return ___ZombieGraveDead_21; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_ZombieGraveDead_21() { return &___ZombieGraveDead_21; }
	inline void set_ZombieGraveDead_21(SpecialRulesTypes_t2275317327 * value)
	{
		___ZombieGraveDead_21 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieGraveDead_21), value);
	}

	inline static int32_t get_offset_of_ZombieHorde21_22() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___ZombieHorde21_22)); }
	inline SpecialRulesTypes_t2275317327 * get_ZombieHorde21_22() const { return ___ZombieHorde21_22; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_ZombieHorde21_22() { return &___ZombieHorde21_22; }
	inline void set_ZombieHorde21_22(SpecialRulesTypes_t2275317327 * value)
	{
		___ZombieHorde21_22 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieHorde21_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSION_T4233471175_H
#ifndef ZOMBIEDECK_T3038342722_H
#define ZOMBIEDECK_T3038342722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieDeck
struct  ZombieDeck_t3038342722  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::deck
	List_1_t3709728484 * ___deck_0;
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::discard
	List_1_t3709728484 * ___discard_1;
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::hand
	List_1_t3709728484 * ___hand_2;
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::handDoNotPlay
	List_1_t3709728484 * ___handDoNotPlay_3;
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::remains
	List_1_t3709728484 * ___remains_4;
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::uniqueCards
	List_1_t3709728484 * ___uniqueCards_5;

public:
	inline static int32_t get_offset_of_deck_0() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___deck_0)); }
	inline List_1_t3709728484 * get_deck_0() const { return ___deck_0; }
	inline List_1_t3709728484 ** get_address_of_deck_0() { return &___deck_0; }
	inline void set_deck_0(List_1_t3709728484 * value)
	{
		___deck_0 = value;
		Il2CppCodeGenWriteBarrier((&___deck_0), value);
	}

	inline static int32_t get_offset_of_discard_1() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___discard_1)); }
	inline List_1_t3709728484 * get_discard_1() const { return ___discard_1; }
	inline List_1_t3709728484 ** get_address_of_discard_1() { return &___discard_1; }
	inline void set_discard_1(List_1_t3709728484 * value)
	{
		___discard_1 = value;
		Il2CppCodeGenWriteBarrier((&___discard_1), value);
	}

	inline static int32_t get_offset_of_hand_2() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___hand_2)); }
	inline List_1_t3709728484 * get_hand_2() const { return ___hand_2; }
	inline List_1_t3709728484 ** get_address_of_hand_2() { return &___hand_2; }
	inline void set_hand_2(List_1_t3709728484 * value)
	{
		___hand_2 = value;
		Il2CppCodeGenWriteBarrier((&___hand_2), value);
	}

	inline static int32_t get_offset_of_handDoNotPlay_3() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___handDoNotPlay_3)); }
	inline List_1_t3709728484 * get_handDoNotPlay_3() const { return ___handDoNotPlay_3; }
	inline List_1_t3709728484 ** get_address_of_handDoNotPlay_3() { return &___handDoNotPlay_3; }
	inline void set_handDoNotPlay_3(List_1_t3709728484 * value)
	{
		___handDoNotPlay_3 = value;
		Il2CppCodeGenWriteBarrier((&___handDoNotPlay_3), value);
	}

	inline static int32_t get_offset_of_remains_4() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___remains_4)); }
	inline List_1_t3709728484 * get_remains_4() const { return ___remains_4; }
	inline List_1_t3709728484 ** get_address_of_remains_4() { return &___remains_4; }
	inline void set_remains_4(List_1_t3709728484 * value)
	{
		___remains_4 = value;
		Il2CppCodeGenWriteBarrier((&___remains_4), value);
	}

	inline static int32_t get_offset_of_uniqueCards_5() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___uniqueCards_5)); }
	inline List_1_t3709728484 * get_uniqueCards_5() const { return ___uniqueCards_5; }
	inline List_1_t3709728484 ** get_address_of_uniqueCards_5() { return &___uniqueCards_5; }
	inline void set_uniqueCards_5(List_1_t3709728484 * value)
	{
		___uniqueCards_5 = value;
		Il2CppCodeGenWriteBarrier((&___uniqueCards_5), value);
	}
};

struct ZombieDeck_t3038342722_StaticFields
{
public:
	// System.Func`2<ZombieCard,System.String> ZombieDeck::<>f__am$cache0
	Func_2_t166598193 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Func_2_t166598193 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Func_2_t166598193 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Func_2_t166598193 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIEDECK_T3038342722_H
#ifndef ZOMBIES_T3905046179_H
#define ZOMBIES_T3905046179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zombies
struct  Zombies_t3905046179  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zombie> Zombies::zombieTypes
	List_1_t4068628460 * ___zombieTypes_0;
	// System.Int32 Zombies::zombiesKilledCount
	int32_t ___zombiesKilledCount_1;
	// System.Boolean Zombies::zombieTurn
	bool ___zombieTurn_2;
	// System.Boolean Zombies::zombiesCanCanBeKilled
	bool ___zombiesCanCanBeKilled_3;
	// System.Int32 Zombies::tempFightDice
	int32_t ___tempFightDice_4;
	// System.Int32 Zombies::bonusFightDice
	int32_t ___bonusFightDice_5;
	// System.Int32 Zombies::extraRoundDice
	int32_t ___extraRoundDice_6;
	// System.Boolean Zombies::multipleFightCards
	bool ___multipleFightCards_7;
	// Zombies/ZombiesOnBoard Zombies::ZombiesOnBoardChanged
	ZombiesOnBoard_t3526012930 * ___ZombiesOnBoardChanged_8;
	// Zombies/ZombiesAreKillable Zombies::ZombiesAreKillableChanged
	ZombiesAreKillable_t2501657832 * ___ZombiesAreKillableChanged_9;
	// Zombies/HeroZombieOnBoard Zombies::HeroZombieOnBoardChanged
	HeroZombieOnBoard_t1308641688 * ___HeroZombieOnBoardChanged_10;

public:
	inline static int32_t get_offset_of_zombieTypes_0() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___zombieTypes_0)); }
	inline List_1_t4068628460 * get_zombieTypes_0() const { return ___zombieTypes_0; }
	inline List_1_t4068628460 ** get_address_of_zombieTypes_0() { return &___zombieTypes_0; }
	inline void set_zombieTypes_0(List_1_t4068628460 * value)
	{
		___zombieTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___zombieTypes_0), value);
	}

	inline static int32_t get_offset_of_zombiesKilledCount_1() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___zombiesKilledCount_1)); }
	inline int32_t get_zombiesKilledCount_1() const { return ___zombiesKilledCount_1; }
	inline int32_t* get_address_of_zombiesKilledCount_1() { return &___zombiesKilledCount_1; }
	inline void set_zombiesKilledCount_1(int32_t value)
	{
		___zombiesKilledCount_1 = value;
	}

	inline static int32_t get_offset_of_zombieTurn_2() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___zombieTurn_2)); }
	inline bool get_zombieTurn_2() const { return ___zombieTurn_2; }
	inline bool* get_address_of_zombieTurn_2() { return &___zombieTurn_2; }
	inline void set_zombieTurn_2(bool value)
	{
		___zombieTurn_2 = value;
	}

	inline static int32_t get_offset_of_zombiesCanCanBeKilled_3() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___zombiesCanCanBeKilled_3)); }
	inline bool get_zombiesCanCanBeKilled_3() const { return ___zombiesCanCanBeKilled_3; }
	inline bool* get_address_of_zombiesCanCanBeKilled_3() { return &___zombiesCanCanBeKilled_3; }
	inline void set_zombiesCanCanBeKilled_3(bool value)
	{
		___zombiesCanCanBeKilled_3 = value;
	}

	inline static int32_t get_offset_of_tempFightDice_4() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___tempFightDice_4)); }
	inline int32_t get_tempFightDice_4() const { return ___tempFightDice_4; }
	inline int32_t* get_address_of_tempFightDice_4() { return &___tempFightDice_4; }
	inline void set_tempFightDice_4(int32_t value)
	{
		___tempFightDice_4 = value;
	}

	inline static int32_t get_offset_of_bonusFightDice_5() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___bonusFightDice_5)); }
	inline int32_t get_bonusFightDice_5() const { return ___bonusFightDice_5; }
	inline int32_t* get_address_of_bonusFightDice_5() { return &___bonusFightDice_5; }
	inline void set_bonusFightDice_5(int32_t value)
	{
		___bonusFightDice_5 = value;
	}

	inline static int32_t get_offset_of_extraRoundDice_6() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___extraRoundDice_6)); }
	inline int32_t get_extraRoundDice_6() const { return ___extraRoundDice_6; }
	inline int32_t* get_address_of_extraRoundDice_6() { return &___extraRoundDice_6; }
	inline void set_extraRoundDice_6(int32_t value)
	{
		___extraRoundDice_6 = value;
	}

	inline static int32_t get_offset_of_multipleFightCards_7() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___multipleFightCards_7)); }
	inline bool get_multipleFightCards_7() const { return ___multipleFightCards_7; }
	inline bool* get_address_of_multipleFightCards_7() { return &___multipleFightCards_7; }
	inline void set_multipleFightCards_7(bool value)
	{
		___multipleFightCards_7 = value;
	}

	inline static int32_t get_offset_of_ZombiesOnBoardChanged_8() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___ZombiesOnBoardChanged_8)); }
	inline ZombiesOnBoard_t3526012930 * get_ZombiesOnBoardChanged_8() const { return ___ZombiesOnBoardChanged_8; }
	inline ZombiesOnBoard_t3526012930 ** get_address_of_ZombiesOnBoardChanged_8() { return &___ZombiesOnBoardChanged_8; }
	inline void set_ZombiesOnBoardChanged_8(ZombiesOnBoard_t3526012930 * value)
	{
		___ZombiesOnBoardChanged_8 = value;
		Il2CppCodeGenWriteBarrier((&___ZombiesOnBoardChanged_8), value);
	}

	inline static int32_t get_offset_of_ZombiesAreKillableChanged_9() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___ZombiesAreKillableChanged_9)); }
	inline ZombiesAreKillable_t2501657832 * get_ZombiesAreKillableChanged_9() const { return ___ZombiesAreKillableChanged_9; }
	inline ZombiesAreKillable_t2501657832 ** get_address_of_ZombiesAreKillableChanged_9() { return &___ZombiesAreKillableChanged_9; }
	inline void set_ZombiesAreKillableChanged_9(ZombiesAreKillable_t2501657832 * value)
	{
		___ZombiesAreKillableChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___ZombiesAreKillableChanged_9), value);
	}

	inline static int32_t get_offset_of_HeroZombieOnBoardChanged_10() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___HeroZombieOnBoardChanged_10)); }
	inline HeroZombieOnBoard_t1308641688 * get_HeroZombieOnBoardChanged_10() const { return ___HeroZombieOnBoardChanged_10; }
	inline HeroZombieOnBoard_t1308641688 ** get_address_of_HeroZombieOnBoardChanged_10() { return &___HeroZombieOnBoardChanged_10; }
	inline void set_HeroZombieOnBoardChanged_10(HeroZombieOnBoard_t1308641688 * value)
	{
		___HeroZombieOnBoardChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___HeroZombieOnBoardChanged_10), value);
	}
};

struct Zombies_t3905046179_StaticFields
{
public:
	// System.Func`2<Zombie,System.Int32> Zombies::<>f__am$cache0
	Func_2_t3217696673 * ___U3CU3Ef__amU24cache0_11;
	// System.Func`2<Zombie,System.Int32> Zombies::<>f__am$cache1
	Func_2_t3217696673 * ___U3CU3Ef__amU24cache1_12;
	// System.Func`2<Zombie,System.Boolean> Zombies::<>f__am$cache2
	Func_2_t364038885 * ___U3CU3Ef__amU24cache2_13;
	// System.Func`2<Zombie,System.Int32> Zombies::<>f__am$cache3
	Func_2_t3217696673 * ___U3CU3Ef__amU24cache3_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(Zombies_t3905046179_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Func_2_t3217696673 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Func_2_t3217696673 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Func_2_t3217696673 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_12() { return static_cast<int32_t>(offsetof(Zombies_t3905046179_StaticFields, ___U3CU3Ef__amU24cache1_12)); }
	inline Func_2_t3217696673 * get_U3CU3Ef__amU24cache1_12() const { return ___U3CU3Ef__amU24cache1_12; }
	inline Func_2_t3217696673 ** get_address_of_U3CU3Ef__amU24cache1_12() { return &___U3CU3Ef__amU24cache1_12; }
	inline void set_U3CU3Ef__amU24cache1_12(Func_2_t3217696673 * value)
	{
		___U3CU3Ef__amU24cache1_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_13() { return static_cast<int32_t>(offsetof(Zombies_t3905046179_StaticFields, ___U3CU3Ef__amU24cache2_13)); }
	inline Func_2_t364038885 * get_U3CU3Ef__amU24cache2_13() const { return ___U3CU3Ef__amU24cache2_13; }
	inline Func_2_t364038885 ** get_address_of_U3CU3Ef__amU24cache2_13() { return &___U3CU3Ef__amU24cache2_13; }
	inline void set_U3CU3Ef__amU24cache2_13(Func_2_t364038885 * value)
	{
		___U3CU3Ef__amU24cache2_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_14() { return static_cast<int32_t>(offsetof(Zombies_t3905046179_StaticFields, ___U3CU3Ef__amU24cache3_14)); }
	inline Func_2_t3217696673 * get_U3CU3Ef__amU24cache3_14() const { return ___U3CU3Ef__amU24cache3_14; }
	inline Func_2_t3217696673 ** get_address_of_U3CU3Ef__amU24cache3_14() { return &___U3CU3Ef__amU24cache3_14; }
	inline void set_U3CU3Ef__amU24cache3_14(Func_2_t3217696673 * value)
	{
		___U3CU3Ef__amU24cache3_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIES_T3905046179_H
#ifndef GAMESESSION_T4087811243_H
#define GAMESESSION_T4087811243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSession
struct  GameSession_t4087811243  : public RuntimeObject
{
public:
	// Missions GameSession::missionOptions
	Missions_t3398870215 * ___missionOptions_0;
	// Mission GameSession::currentMission
	Mission_t4233471175 * ___currentMission_1;
	// Tiles GameSession::mapTiles
	Tiles_t1457048987 * ___mapTiles_2;
	// Heroes GameSession::heroCharacters
	Heroes_t1064386291 * ___heroCharacters_3;
	// Zombies GameSession::zombies
	Zombies_t3905046179 * ___zombies_4;
	// RadialBasisFunctionNetwork GameSession::rbfSession
	RadialBasisFunctionNetwork_t3778331090 * ___rbfSession_5;
	// System.Double[] GameSession::currentGameDifficulty
	DoubleU5BU5D_t3413330114* ___currentGameDifficulty_6;
	// System.Boolean GameSession::networkCreated
	bool ___networkCreated_7;

public:
	inline static int32_t get_offset_of_missionOptions_0() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___missionOptions_0)); }
	inline Missions_t3398870215 * get_missionOptions_0() const { return ___missionOptions_0; }
	inline Missions_t3398870215 ** get_address_of_missionOptions_0() { return &___missionOptions_0; }
	inline void set_missionOptions_0(Missions_t3398870215 * value)
	{
		___missionOptions_0 = value;
		Il2CppCodeGenWriteBarrier((&___missionOptions_0), value);
	}

	inline static int32_t get_offset_of_currentMission_1() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___currentMission_1)); }
	inline Mission_t4233471175 * get_currentMission_1() const { return ___currentMission_1; }
	inline Mission_t4233471175 ** get_address_of_currentMission_1() { return &___currentMission_1; }
	inline void set_currentMission_1(Mission_t4233471175 * value)
	{
		___currentMission_1 = value;
		Il2CppCodeGenWriteBarrier((&___currentMission_1), value);
	}

	inline static int32_t get_offset_of_mapTiles_2() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___mapTiles_2)); }
	inline Tiles_t1457048987 * get_mapTiles_2() const { return ___mapTiles_2; }
	inline Tiles_t1457048987 ** get_address_of_mapTiles_2() { return &___mapTiles_2; }
	inline void set_mapTiles_2(Tiles_t1457048987 * value)
	{
		___mapTiles_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapTiles_2), value);
	}

	inline static int32_t get_offset_of_heroCharacters_3() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___heroCharacters_3)); }
	inline Heroes_t1064386291 * get_heroCharacters_3() const { return ___heroCharacters_3; }
	inline Heroes_t1064386291 ** get_address_of_heroCharacters_3() { return &___heroCharacters_3; }
	inline void set_heroCharacters_3(Heroes_t1064386291 * value)
	{
		___heroCharacters_3 = value;
		Il2CppCodeGenWriteBarrier((&___heroCharacters_3), value);
	}

	inline static int32_t get_offset_of_zombies_4() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___zombies_4)); }
	inline Zombies_t3905046179 * get_zombies_4() const { return ___zombies_4; }
	inline Zombies_t3905046179 ** get_address_of_zombies_4() { return &___zombies_4; }
	inline void set_zombies_4(Zombies_t3905046179 * value)
	{
		___zombies_4 = value;
		Il2CppCodeGenWriteBarrier((&___zombies_4), value);
	}

	inline static int32_t get_offset_of_rbfSession_5() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___rbfSession_5)); }
	inline RadialBasisFunctionNetwork_t3778331090 * get_rbfSession_5() const { return ___rbfSession_5; }
	inline RadialBasisFunctionNetwork_t3778331090 ** get_address_of_rbfSession_5() { return &___rbfSession_5; }
	inline void set_rbfSession_5(RadialBasisFunctionNetwork_t3778331090 * value)
	{
		___rbfSession_5 = value;
		Il2CppCodeGenWriteBarrier((&___rbfSession_5), value);
	}

	inline static int32_t get_offset_of_currentGameDifficulty_6() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___currentGameDifficulty_6)); }
	inline DoubleU5BU5D_t3413330114* get_currentGameDifficulty_6() const { return ___currentGameDifficulty_6; }
	inline DoubleU5BU5D_t3413330114** get_address_of_currentGameDifficulty_6() { return &___currentGameDifficulty_6; }
	inline void set_currentGameDifficulty_6(DoubleU5BU5D_t3413330114* value)
	{
		___currentGameDifficulty_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentGameDifficulty_6), value);
	}

	inline static int32_t get_offset_of_networkCreated_7() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___networkCreated_7)); }
	inline bool get_networkCreated_7() const { return ___networkCreated_7; }
	inline bool* get_address_of_networkCreated_7() { return &___networkCreated_7; }
	inline void set_networkCreated_7(bool value)
	{
		___networkCreated_7 = value;
	}
};

struct GameSession_t4087811243_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`3<CloudConnectorCore/QueryType,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<System.String>> GameSession::<>f__mg$cache0
	UnityAction_3_t1112121581 * ___U3CU3Ef__mgU24cache0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(GameSession_t4087811243_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline UnityAction_3_t1112121581 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline UnityAction_3_t1112121581 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(UnityAction_3_t1112121581 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESESSION_T4087811243_H
#ifndef SPECIALRULESTYPES_T2275317327_H
#define SPECIALRULESTYPES_T2275317327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mission/SpecialRulesTypes
struct  SpecialRulesTypes_t2275317327  : public RuntimeObject
{
public:
	// System.String Mission/SpecialRulesTypes::Name
	String_t* ___Name_0;
	// System.String Mission/SpecialRulesTypes::SetupText
	String_t* ___SetupText_1;
	// System.String Mission/SpecialRulesTypes::PlayerPrefKey
	String_t* ___PlayerPrefKey_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(SpecialRulesTypes_t2275317327, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_SetupText_1() { return static_cast<int32_t>(offsetof(SpecialRulesTypes_t2275317327, ___SetupText_1)); }
	inline String_t* get_SetupText_1() const { return ___SetupText_1; }
	inline String_t** get_address_of_SetupText_1() { return &___SetupText_1; }
	inline void set_SetupText_1(String_t* value)
	{
		___SetupText_1 = value;
		Il2CppCodeGenWriteBarrier((&___SetupText_1), value);
	}

	inline static int32_t get_offset_of_PlayerPrefKey_2() { return static_cast<int32_t>(offsetof(SpecialRulesTypes_t2275317327, ___PlayerPrefKey_2)); }
	inline String_t* get_PlayerPrefKey_2() const { return ___PlayerPrefKey_2; }
	inline String_t** get_address_of_PlayerPrefKey_2() { return &___PlayerPrefKey_2; }
	inline void set_PlayerPrefKey_2(String_t* value)
	{
		___PlayerPrefKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerPrefKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALRULESTYPES_T2275317327_H
#ifndef U3CPLAYCARDU3EC__ITERATOR0_T1992789720_H
#define U3CPLAYCARDU3EC__ITERATOR0_T1992789720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurn/<PlayCard>c__Iterator0
struct  U3CPlayCardU3Ec__Iterator0_t1992789720  : public RuntimeObject
{
public:
	// UnityEngine.Animator ZombieTurn/<PlayCard>c__Iterator0::<cardAnimator>__0
	Animator_t434523843 * ___U3CcardAnimatorU3E__0_0;
	// ZombieTurn ZombieTurn/<PlayCard>c__Iterator0::$this
	ZombieTurn_t3562480803 * ___U24this_1;
	// System.Object ZombieTurn/<PlayCard>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ZombieTurn/<PlayCard>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 ZombieTurn/<PlayCard>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcardAnimatorU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPlayCardU3Ec__Iterator0_t1992789720, ___U3CcardAnimatorU3E__0_0)); }
	inline Animator_t434523843 * get_U3CcardAnimatorU3E__0_0() const { return ___U3CcardAnimatorU3E__0_0; }
	inline Animator_t434523843 ** get_address_of_U3CcardAnimatorU3E__0_0() { return &___U3CcardAnimatorU3E__0_0; }
	inline void set_U3CcardAnimatorU3E__0_0(Animator_t434523843 * value)
	{
		___U3CcardAnimatorU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcardAnimatorU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CPlayCardU3Ec__Iterator0_t1992789720, ___U24this_1)); }
	inline ZombieTurn_t3562480803 * get_U24this_1() const { return ___U24this_1; }
	inline ZombieTurn_t3562480803 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ZombieTurn_t3562480803 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CPlayCardU3Ec__Iterator0_t1992789720, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CPlayCardU3Ec__Iterator0_t1992789720, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CPlayCardU3Ec__Iterator0_t1992789720, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYCARDU3EC__ITERATOR0_T1992789720_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	StringU5BU5D_t1281789340* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ___EmptyArray_4)); }
	inline StringU5BU5D_t1281789340* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline StringU5BU5D_t1281789340** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(StringU5BU5D_t1281789340* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef U3CFIGHTROLLU3EC__ANONSTOREY1_T3184696250_H
#define U3CFIGHTROLLU3EC__ANONSTOREY1_T3184696250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurn/<FightRoll>c__AnonStorey1
struct  U3CFightRollU3Ec__AnonStorey1_t3184696250  : public RuntimeObject
{
public:
	// System.Int32 ZombieTurn/<FightRoll>c__AnonStorey1::index
	int32_t ___index_0;
	// ZombieTurn ZombieTurn/<FightRoll>c__AnonStorey1::$this
	ZombieTurn_t3562480803 * ___U24this_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CFightRollU3Ec__AnonStorey1_t3184696250, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFightRollU3Ec__AnonStorey1_t3184696250, ___U24this_1)); }
	inline ZombieTurn_t3562480803 * get_U24this_1() const { return ___U24this_1; }
	inline ZombieTurn_t3562480803 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ZombieTurn_t3562480803 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFIGHTROLLU3EC__ANONSTOREY1_T3184696250_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef ANIMATORSTATEINFO_T509032636_H
#define ANIMATORSTATEINFO_T509032636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_t509032636 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTATEINFO_T509032636_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef ENUMERATOR_T1662905041_H
#define ENUMERATOR_T1662905041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Zombie>
struct  Enumerator_t1662905041 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t4068628460 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Zombie_t2596553718 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1662905041, ___l_0)); }
	inline List_1_t4068628460 * get_l_0() const { return ___l_0; }
	inline List_1_t4068628460 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t4068628460 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1662905041, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1662905041, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1662905041, ___current_3)); }
	inline Zombie_t2596553718 * get_current_3() const { return ___current_3; }
	inline Zombie_t2596553718 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Zombie_t2596553718 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1662905041_H
#ifndef ENUMERATOR_T1304005065_H
#define ENUMERATOR_T1304005065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<ZombieCard>
struct  Enumerator_t1304005065 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3709728484 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ZombieCard_t2237653742 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1304005065, ___l_0)); }
	inline List_1_t3709728484 * get_l_0() const { return ___l_0; }
	inline List_1_t3709728484 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3709728484 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1304005065, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1304005065, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1304005065, ___current_3)); }
	inline ZombieCard_t2237653742 * get_current_3() const { return ___current_3; }
	inline ZombieCard_t2237653742 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ZombieCard_t2237653742 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1304005065_H
#ifndef ZOMBIEMOVEMENTTARGET_T4013730920_H
#define ZOMBIEMOVEMENTTARGET_T4013730920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieMovementTarget
struct  ZombieMovementTarget_t4013730920 
{
public:
	// System.Int32 ZombieMovementTarget::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ZombieMovementTarget_t4013730920, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIEMOVEMENTTARGET_T4013730920_H
#ifndef DIRECTION_T337909235_H
#define DIRECTION_T337909235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t337909235 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t337909235, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T337909235_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ZOMBIETYPE_T2798134437_H
#define ZOMBIETYPE_T2798134437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieType
struct  ZombieType_t2798134437 
{
public:
	// System.Int32 ZombieType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ZombieType_t2798134437, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIETYPE_T2798134437_H
#ifndef PLAYDURINGPHASE_T3488456769_H
#define PLAYDURINGPHASE_T3488456769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayDuringPhase
struct  PlayDuringPhase_t3488456769 
{
public:
	// System.Int32 PlayDuringPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayDuringPhase_t3488456769, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYDURINGPHASE_T3488456769_H
#ifndef ZOMBIETURNPHASENAME_T860305160_H
#define ZOMBIETURNPHASENAME_T860305160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurnPhaseName
struct  ZombieTurnPhaseName_t860305160 
{
public:
	// System.Int32 ZombieTurnPhaseName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ZombieTurnPhaseName_t860305160, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIETURNPHASENAME_T860305160_H
#ifndef ZOMBIESPEED_T2101478944_H
#define ZOMBIESPEED_T2101478944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieSpeed
struct  ZombieSpeed_t2101478944 
{
public:
	// System.Int32 ZombieSpeed::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ZombieSpeed_t2101478944, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIESPEED_T2101478944_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef ZOMBIETURNPHASES_T3127322021_H
#define ZOMBIETURNPHASES_T3127322021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurnPhases
struct  ZombieTurnPhases_t3127322021  : public RuntimeObject
{
public:
	// ZombieTurnPhases/TurnPhaseDelegateDeclare ZombieTurnPhases::TurnPhaseDelegate
	TurnPhaseDelegateDeclare_t3595605765 * ___TurnPhaseDelegate_0;
	// ZombieTurn ZombieTurnPhases::zombieTurn
	ZombieTurn_t3562480803 * ___zombieTurn_1;
	// System.Int32 ZombieTurnPhases::MoveZombieIndex
	int32_t ___MoveZombieIndex_2;
	// ZombieTurnPhaseName ZombieTurnPhases::PhaseName
	int32_t ___PhaseName_3;

public:
	inline static int32_t get_offset_of_TurnPhaseDelegate_0() { return static_cast<int32_t>(offsetof(ZombieTurnPhases_t3127322021, ___TurnPhaseDelegate_0)); }
	inline TurnPhaseDelegateDeclare_t3595605765 * get_TurnPhaseDelegate_0() const { return ___TurnPhaseDelegate_0; }
	inline TurnPhaseDelegateDeclare_t3595605765 ** get_address_of_TurnPhaseDelegate_0() { return &___TurnPhaseDelegate_0; }
	inline void set_TurnPhaseDelegate_0(TurnPhaseDelegateDeclare_t3595605765 * value)
	{
		___TurnPhaseDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___TurnPhaseDelegate_0), value);
	}

	inline static int32_t get_offset_of_zombieTurn_1() { return static_cast<int32_t>(offsetof(ZombieTurnPhases_t3127322021, ___zombieTurn_1)); }
	inline ZombieTurn_t3562480803 * get_zombieTurn_1() const { return ___zombieTurn_1; }
	inline ZombieTurn_t3562480803 ** get_address_of_zombieTurn_1() { return &___zombieTurn_1; }
	inline void set_zombieTurn_1(ZombieTurn_t3562480803 * value)
	{
		___zombieTurn_1 = value;
		Il2CppCodeGenWriteBarrier((&___zombieTurn_1), value);
	}

	inline static int32_t get_offset_of_MoveZombieIndex_2() { return static_cast<int32_t>(offsetof(ZombieTurnPhases_t3127322021, ___MoveZombieIndex_2)); }
	inline int32_t get_MoveZombieIndex_2() const { return ___MoveZombieIndex_2; }
	inline int32_t* get_address_of_MoveZombieIndex_2() { return &___MoveZombieIndex_2; }
	inline void set_MoveZombieIndex_2(int32_t value)
	{
		___MoveZombieIndex_2 = value;
	}

	inline static int32_t get_offset_of_PhaseName_3() { return static_cast<int32_t>(offsetof(ZombieTurnPhases_t3127322021, ___PhaseName_3)); }
	inline int32_t get_PhaseName_3() const { return ___PhaseName_3; }
	inline int32_t* get_address_of_PhaseName_3() { return &___PhaseName_3; }
	inline void set_PhaseName_3(int32_t value)
	{
		___PhaseName_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIETURNPHASES_T3127322021_H
#ifndef ZOMBIECARD_T2237653742_H
#define ZOMBIECARD_T2237653742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieCard
struct  ZombieCard_t2237653742  : public RuntimeObject
{
public:
	// System.String ZombieCard::Name
	String_t* ___Name_0;
	// System.String ZombieCard::RemainsInPlayName
	String_t* ___RemainsInPlayName_1;
	// PlayDuringPhase ZombieCard::Phase
	int32_t ___Phase_2;
	// System.String ZombieCard::OriginalText
	String_t* ___OriginalText_3;
	// System.String ZombieCard::FlavorText
	String_t* ___FlavorText_4;
	// System.Boolean ZombieCard::RemainsInPlay
	bool ___RemainsInPlay_5;
	// System.Boolean ZombieCard::tempCard
	bool ___tempCard_6;
	// System.Boolean ZombieCard::SkipCard
	bool ___SkipCard_7;
	// ZombieTurn ZombieCard::ZombieTurnManager
	ZombieTurn_t3562480803 * ___ZombieTurnManager_8;
	// Building ZombieCard::Building
	Building_t1411255995 * ___Building_9;
	// System.String ZombieCard::HeroToEffect
	String_t* ___HeroToEffect_10;
	// System.Int32 ZombieCard::NewZombies
	int32_t ___NewZombies_11;
	// System.Int32 ZombieCard::CardRollValue
	int32_t ___CardRollValue_12;
	// ZombieCard ZombieCard::CardToEffect
	ZombieCard_t2237653742 * ___CardToEffect_13;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_RemainsInPlayName_1() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___RemainsInPlayName_1)); }
	inline String_t* get_RemainsInPlayName_1() const { return ___RemainsInPlayName_1; }
	inline String_t** get_address_of_RemainsInPlayName_1() { return &___RemainsInPlayName_1; }
	inline void set_RemainsInPlayName_1(String_t* value)
	{
		___RemainsInPlayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___RemainsInPlayName_1), value);
	}

	inline static int32_t get_offset_of_Phase_2() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___Phase_2)); }
	inline int32_t get_Phase_2() const { return ___Phase_2; }
	inline int32_t* get_address_of_Phase_2() { return &___Phase_2; }
	inline void set_Phase_2(int32_t value)
	{
		___Phase_2 = value;
	}

	inline static int32_t get_offset_of_OriginalText_3() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___OriginalText_3)); }
	inline String_t* get_OriginalText_3() const { return ___OriginalText_3; }
	inline String_t** get_address_of_OriginalText_3() { return &___OriginalText_3; }
	inline void set_OriginalText_3(String_t* value)
	{
		___OriginalText_3 = value;
		Il2CppCodeGenWriteBarrier((&___OriginalText_3), value);
	}

	inline static int32_t get_offset_of_FlavorText_4() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___FlavorText_4)); }
	inline String_t* get_FlavorText_4() const { return ___FlavorText_4; }
	inline String_t** get_address_of_FlavorText_4() { return &___FlavorText_4; }
	inline void set_FlavorText_4(String_t* value)
	{
		___FlavorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___FlavorText_4), value);
	}

	inline static int32_t get_offset_of_RemainsInPlay_5() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___RemainsInPlay_5)); }
	inline bool get_RemainsInPlay_5() const { return ___RemainsInPlay_5; }
	inline bool* get_address_of_RemainsInPlay_5() { return &___RemainsInPlay_5; }
	inline void set_RemainsInPlay_5(bool value)
	{
		___RemainsInPlay_5 = value;
	}

	inline static int32_t get_offset_of_tempCard_6() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___tempCard_6)); }
	inline bool get_tempCard_6() const { return ___tempCard_6; }
	inline bool* get_address_of_tempCard_6() { return &___tempCard_6; }
	inline void set_tempCard_6(bool value)
	{
		___tempCard_6 = value;
	}

	inline static int32_t get_offset_of_SkipCard_7() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___SkipCard_7)); }
	inline bool get_SkipCard_7() const { return ___SkipCard_7; }
	inline bool* get_address_of_SkipCard_7() { return &___SkipCard_7; }
	inline void set_SkipCard_7(bool value)
	{
		___SkipCard_7 = value;
	}

	inline static int32_t get_offset_of_ZombieTurnManager_8() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___ZombieTurnManager_8)); }
	inline ZombieTurn_t3562480803 * get_ZombieTurnManager_8() const { return ___ZombieTurnManager_8; }
	inline ZombieTurn_t3562480803 ** get_address_of_ZombieTurnManager_8() { return &___ZombieTurnManager_8; }
	inline void set_ZombieTurnManager_8(ZombieTurn_t3562480803 * value)
	{
		___ZombieTurnManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieTurnManager_8), value);
	}

	inline static int32_t get_offset_of_Building_9() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___Building_9)); }
	inline Building_t1411255995 * get_Building_9() const { return ___Building_9; }
	inline Building_t1411255995 ** get_address_of_Building_9() { return &___Building_9; }
	inline void set_Building_9(Building_t1411255995 * value)
	{
		___Building_9 = value;
		Il2CppCodeGenWriteBarrier((&___Building_9), value);
	}

	inline static int32_t get_offset_of_HeroToEffect_10() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___HeroToEffect_10)); }
	inline String_t* get_HeroToEffect_10() const { return ___HeroToEffect_10; }
	inline String_t** get_address_of_HeroToEffect_10() { return &___HeroToEffect_10; }
	inline void set_HeroToEffect_10(String_t* value)
	{
		___HeroToEffect_10 = value;
		Il2CppCodeGenWriteBarrier((&___HeroToEffect_10), value);
	}

	inline static int32_t get_offset_of_NewZombies_11() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___NewZombies_11)); }
	inline int32_t get_NewZombies_11() const { return ___NewZombies_11; }
	inline int32_t* get_address_of_NewZombies_11() { return &___NewZombies_11; }
	inline void set_NewZombies_11(int32_t value)
	{
		___NewZombies_11 = value;
	}

	inline static int32_t get_offset_of_CardRollValue_12() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___CardRollValue_12)); }
	inline int32_t get_CardRollValue_12() const { return ___CardRollValue_12; }
	inline int32_t* get_address_of_CardRollValue_12() { return &___CardRollValue_12; }
	inline void set_CardRollValue_12(int32_t value)
	{
		___CardRollValue_12 = value;
	}

	inline static int32_t get_offset_of_CardToEffect_13() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___CardToEffect_13)); }
	inline ZombieCard_t2237653742 * get_CardToEffect_13() const { return ___CardToEffect_13; }
	inline ZombieCard_t2237653742 ** get_address_of_CardToEffect_13() { return &___CardToEffect_13; }
	inline void set_CardToEffect_13(ZombieCard_t2237653742 * value)
	{
		___CardToEffect_13 = value;
		Il2CppCodeGenWriteBarrier((&___CardToEffect_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIECARD_T2237653742_H
#ifndef ZOMBIE_T2596553718_H
#define ZOMBIE_T2596553718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zombie
struct  Zombie_t2596553718  : public RuntimeObject
{
public:
	// System.String Zombie::name
	String_t* ___name_0;
	// ZombieType Zombie::type
	int32_t ___type_1;
	// ZombieSpeed Zombie::speed
	int32_t ___speed_2;
	// System.Int32 Zombie::health
	int32_t ___health_3;
	// System.Int32 Zombie::hungerRange
	int32_t ___hungerRange_4;
	// System.Boolean Zombie::countTowardSpawnCheck
	bool ___countTowardSpawnCheck_5;
	// ZombieMovementTarget Zombie::movementTarget
	int32_t ___movementTarget_6;
	// ZombieTurn Zombie::zombieTurn
	ZombieTurn_t3562480803 * ___zombieTurn_7;
	// System.Boolean Zombie::ignoreWoundOnFourPlus
	bool ___ignoreWoundOnFourPlus_8;
	// System.Boolean Zombie::canSpawnWithOtherZombie
	bool ___canSpawnWithOtherZombie_9;
	// System.Boolean Zombie::makesZombieHeroOnFivePlus
	bool ___makesZombieHeroOnFivePlus_10;
	// System.Int32 Zombie::zombiesInPool
	int32_t ___zombiesInPool_11;
	// System.Int32 Zombie::zombiesOnBoard
	int32_t ___zombiesOnBoard_12;
	// System.Int32 Zombie::baseFightDice
	int32_t ___baseFightDice_13;
	// System.Int32 Zombie::fightDice
	int32_t ___fightDice_14;
	// System.Int32 Zombie::bonusDice
	int32_t ___bonusDice_15;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___speed_2)); }
	inline int32_t get_speed_2() const { return ___speed_2; }
	inline int32_t* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(int32_t value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_health_3() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___health_3)); }
	inline int32_t get_health_3() const { return ___health_3; }
	inline int32_t* get_address_of_health_3() { return &___health_3; }
	inline void set_health_3(int32_t value)
	{
		___health_3 = value;
	}

	inline static int32_t get_offset_of_hungerRange_4() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___hungerRange_4)); }
	inline int32_t get_hungerRange_4() const { return ___hungerRange_4; }
	inline int32_t* get_address_of_hungerRange_4() { return &___hungerRange_4; }
	inline void set_hungerRange_4(int32_t value)
	{
		___hungerRange_4 = value;
	}

	inline static int32_t get_offset_of_countTowardSpawnCheck_5() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___countTowardSpawnCheck_5)); }
	inline bool get_countTowardSpawnCheck_5() const { return ___countTowardSpawnCheck_5; }
	inline bool* get_address_of_countTowardSpawnCheck_5() { return &___countTowardSpawnCheck_5; }
	inline void set_countTowardSpawnCheck_5(bool value)
	{
		___countTowardSpawnCheck_5 = value;
	}

	inline static int32_t get_offset_of_movementTarget_6() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___movementTarget_6)); }
	inline int32_t get_movementTarget_6() const { return ___movementTarget_6; }
	inline int32_t* get_address_of_movementTarget_6() { return &___movementTarget_6; }
	inline void set_movementTarget_6(int32_t value)
	{
		___movementTarget_6 = value;
	}

	inline static int32_t get_offset_of_zombieTurn_7() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___zombieTurn_7)); }
	inline ZombieTurn_t3562480803 * get_zombieTurn_7() const { return ___zombieTurn_7; }
	inline ZombieTurn_t3562480803 ** get_address_of_zombieTurn_7() { return &___zombieTurn_7; }
	inline void set_zombieTurn_7(ZombieTurn_t3562480803 * value)
	{
		___zombieTurn_7 = value;
		Il2CppCodeGenWriteBarrier((&___zombieTurn_7), value);
	}

	inline static int32_t get_offset_of_ignoreWoundOnFourPlus_8() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___ignoreWoundOnFourPlus_8)); }
	inline bool get_ignoreWoundOnFourPlus_8() const { return ___ignoreWoundOnFourPlus_8; }
	inline bool* get_address_of_ignoreWoundOnFourPlus_8() { return &___ignoreWoundOnFourPlus_8; }
	inline void set_ignoreWoundOnFourPlus_8(bool value)
	{
		___ignoreWoundOnFourPlus_8 = value;
	}

	inline static int32_t get_offset_of_canSpawnWithOtherZombie_9() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___canSpawnWithOtherZombie_9)); }
	inline bool get_canSpawnWithOtherZombie_9() const { return ___canSpawnWithOtherZombie_9; }
	inline bool* get_address_of_canSpawnWithOtherZombie_9() { return &___canSpawnWithOtherZombie_9; }
	inline void set_canSpawnWithOtherZombie_9(bool value)
	{
		___canSpawnWithOtherZombie_9 = value;
	}

	inline static int32_t get_offset_of_makesZombieHeroOnFivePlus_10() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___makesZombieHeroOnFivePlus_10)); }
	inline bool get_makesZombieHeroOnFivePlus_10() const { return ___makesZombieHeroOnFivePlus_10; }
	inline bool* get_address_of_makesZombieHeroOnFivePlus_10() { return &___makesZombieHeroOnFivePlus_10; }
	inline void set_makesZombieHeroOnFivePlus_10(bool value)
	{
		___makesZombieHeroOnFivePlus_10 = value;
	}

	inline static int32_t get_offset_of_zombiesInPool_11() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___zombiesInPool_11)); }
	inline int32_t get_zombiesInPool_11() const { return ___zombiesInPool_11; }
	inline int32_t* get_address_of_zombiesInPool_11() { return &___zombiesInPool_11; }
	inline void set_zombiesInPool_11(int32_t value)
	{
		___zombiesInPool_11 = value;
	}

	inline static int32_t get_offset_of_zombiesOnBoard_12() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___zombiesOnBoard_12)); }
	inline int32_t get_zombiesOnBoard_12() const { return ___zombiesOnBoard_12; }
	inline int32_t* get_address_of_zombiesOnBoard_12() { return &___zombiesOnBoard_12; }
	inline void set_zombiesOnBoard_12(int32_t value)
	{
		___zombiesOnBoard_12 = value;
	}

	inline static int32_t get_offset_of_baseFightDice_13() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___baseFightDice_13)); }
	inline int32_t get_baseFightDice_13() const { return ___baseFightDice_13; }
	inline int32_t* get_address_of_baseFightDice_13() { return &___baseFightDice_13; }
	inline void set_baseFightDice_13(int32_t value)
	{
		___baseFightDice_13 = value;
	}

	inline static int32_t get_offset_of_fightDice_14() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___fightDice_14)); }
	inline int32_t get_fightDice_14() const { return ___fightDice_14; }
	inline int32_t* get_address_of_fightDice_14() { return &___fightDice_14; }
	inline void set_fightDice_14(int32_t value)
	{
		___fightDice_14 = value;
	}

	inline static int32_t get_offset_of_bonusDice_15() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___bonusDice_15)); }
	inline int32_t get_bonusDice_15() const { return ___bonusDice_15; }
	inline int32_t* get_address_of_bonusDice_15() { return &___bonusDice_15; }
	inline void set_bonusDice_15(int32_t value)
	{
		___bonusDice_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIE_T2596553718_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef TURNPHASEDELEGATEDECLARE_T3595605765_H
#define TURNPHASEDELEGATEDECLARE_T3595605765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurnPhases/TurnPhaseDelegateDeclare
struct  TurnPhaseDelegateDeclare_t3595605765  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURNPHASEDELEGATEDECLARE_T3595605765_H
#ifndef ANIMATOR_T434523843_H
#define ANIMATOR_T434523843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t434523843  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T434523843_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef ZOMBIETURN_T3562480803_H
#define ZOMBIETURN_T3562480803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurn
struct  ZombieTurn_t3562480803  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.String> ZombieTurn::setupState
	List_1_t3319525431 * ___setupState_2;
	// ZombieDeck ZombieTurn::decks
	ZombieDeck_t3038342722 * ___decks_3;
	// GameSession ZombieTurn::gameStats
	GameSession_t4087811243 * ___gameStats_4;
	// System.Boolean ZombieTurn::spawn
	bool ___spawn_5;
	// System.Boolean ZombieTurn::zombiesWinOnTie
	bool ___zombiesWinOnTie_6;
	// System.Boolean ZombieTurn::zombieMovesAfterSpawn
	bool ___zombieMovesAfterSpawn_7;
	// System.Boolean ZombieTurn::theHungryOne
	bool ___theHungryOne_8;
	// System.Boolean ZombieTurn::fightCardUsed
	bool ___fightCardUsed_9;
	// System.Int32 ZombieTurn::round
	int32_t ___round_10;
	// System.Int32 ZombieTurn::heroesDiedCount
	int32_t ___heroesDiedCount_11;
	// ZombieCard ZombieTurn::cardBeingPlayed
	ZombieCard_t2237653742 * ___cardBeingPlayed_12;
	// GameData ZombieTurn::gameData
	GameData_t415813024 * ___gameData_13;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ZombieTurn::gameObjectives
	List_1_t2585711361 * ___gameObjectives_14;
	// System.String ZombieTurn::currentSetupState
	String_t* ___currentSetupState_15;
	// System.Collections.Generic.List`1<System.String> ZombieTurn::takenOverBuilding
	List_1_t3319525431 * ___takenOverBuilding_16;
	// System.String ZombieTurn::zombieToFight
	String_t* ___zombieToFight_17;
	// System.Int32 ZombieTurn::numberOfHeroesInGame
	int32_t ___numberOfHeroesInGame_18;
	// System.Collections.Generic.List`1<System.String> ZombieTurn::cardsPlayed
	List_1_t3319525431 * ___cardsPlayed_19;
	// ZombieTurnPhases ZombieTurn::Phases
	ZombieTurnPhases_t3127322021 * ___Phases_20;
	// UnityEngine.GameObject ZombieTurn::SetupPanel
	GameObject_t1113636619 * ___SetupPanel_21;
	// UnityEngine.UI.Text ZombieTurn::SetupText
	Text_t1901882714 * ___SetupText_22;
	// UnityEngine.GameObject ZombieTurn::MainUI
	GameObject_t1113636619 * ___MainUI_23;
	// UnityEngine.GameObject ZombieTurn::Card
	GameObject_t1113636619 * ___Card_24;
	// UnityEngine.UI.Text ZombieTurn::CardTitle
	Text_t1901882714 * ___CardTitle_25;
	// UnityEngine.UI.Text ZombieTurn::CardText
	Text_t1901882714 * ___CardText_26;
	// UnityEngine.UI.Text ZombieTurn::CardFlavor
	Text_t1901882714 * ___CardFlavor_27;
	// UnityEngine.UI.Text ZombieTurn::ZombiesOnBoardText
	Text_t1901882714 * ___ZombiesOnBoardText_28;
	// UnityEngine.UI.Text ZombieTurn::ScenarioName
	Text_t1901882714 * ___ScenarioName_29;
	// UnityEngine.GameObject ZombieTurn::MovePanel
	GameObject_t1113636619 * ___MovePanel_30;
	// UnityEngine.UI.Text ZombieTurn::MoveText
	Text_t1901882714 * ___MoveText_31;
	// UnityEngine.UI.Text ZombieTurn::MoveZombieTypeText
	Text_t1901882714 * ___MoveZombieTypeText_32;
	// UnityEngine.GameObject ZombieTurn::FightPanel
	GameObject_t1113636619 * ___FightPanel_33;
	// UnityEngine.UI.Text ZombieTurn::FightText
	Text_t1901882714 * ___FightText_34;
	// UnityEngine.GameObject ZombieTurn::FightRollResultParent
	GameObject_t1113636619 * ___FightRollResultParent_35;
	// UnityEngine.GameObject ZombieTurn::FightRollResultPrefab
	GameObject_t1113636619 * ___FightRollResultPrefab_36;
	// UnityEngine.GameObject ZombieTurn::SpawnPanel
	GameObject_t1113636619 * ___SpawnPanel_37;
	// UnityEngine.UI.Text ZombieTurn::SpawnText
	Text_t1901882714 * ___SpawnText_38;
	// UnityEngine.GameObject ZombieTurn::RemainsInPlay
	GameObject_t1113636619 * ___RemainsInPlay_39;
	// UnityEngine.GameObject ZombieTurn::RemainsInPlayElement
	GameObject_t1113636619 * ___RemainsInPlayElement_40;
	// UnityEngine.GameObject ZombieTurn::ResultPanel
	GameObject_t1113636619 * ___ResultPanel_41;
	// UnityEngine.UI.Text ZombieTurn::ResultText
	Text_t1901882714 * ___ResultText_42;
	// UnityEngine.UI.Text ZombieTurn::ResultNarrativeText
	Text_t1901882714 * ___ResultNarrativeText_43;
	// UnityEngine.GameObject ZombieTurn::DeadHeroPanel
	GameObject_t1113636619 * ___DeadHeroPanel_44;
	// UnityEngine.GameObject ZombieTurn::DeadZombiePanel
	GameObject_t1113636619 * ___DeadZombiePanel_45;
	// TextAnimation ZombieTurn::TextAnimation
	TextAnimation_t2694049294 * ___TextAnimation_46;
	// GameLog ZombieTurn::GameLog
	GameLog_t2697247111 * ___GameLog_47;
	// PlaceMapImages ZombieTurn::MapImages
	PlaceMapImages_t1484071588 * ___MapImages_48;
	// UnityEngine.GameObject ZombieTurn::ZombieObjectivePanel
	GameObject_t1113636619 * ___ZombieObjectivePanel_49;
	// UnityEngine.GameObject ZombieTurn::HeroObjectivePanel
	GameObject_t1113636619 * ___HeroObjectivePanel_50;
	// UnityEngine.GameObject ZombieTurn::ObjectivePrefab
	GameObject_t1113636619 * ___ObjectivePrefab_51;
	// UnityEngine.GameObject ZombieTurn::NarrativePanel
	GameObject_t1113636619 * ___NarrativePanel_52;
	// UnityEngine.UI.Text ZombieTurn::NarrativeTitle
	Text_t1901882714 * ___NarrativeTitle_53;
	// UnityEngine.UI.Text ZombieTurn::NarrativeText
	Text_t1901882714 * ___NarrativeText_54;
	// UnityEngine.GameObject ZombieTurn::DestroyBuildingPanel
	GameObject_t1113636619 * ___DestroyBuildingPanel_55;
	// UnityEngine.UI.Slider ZombieTurn::DestroyBuildingZombieSlider
	Slider_t3903728902 * ___DestroyBuildingZombieSlider_56;
	// UnityEngine.UI.Dropdown ZombieTurn::DestroyBuildingDropdown
	Dropdown_t2274391225 * ___DestroyBuildingDropdown_57;
	// UnityEngine.UI.Text ZombieTurn::DestroyBuildingDeadZombiesText
	Text_t1901882714 * ___DestroyBuildingDeadZombiesText_58;
	// UnityEngine.GameObject ZombieTurn::ZombiePitPanel
	GameObject_t1113636619 * ___ZombiePitPanel_59;
	// UnityEngine.GameObject ZombieTurn::TutorialPanel
	GameObject_t1113636619 * ___TutorialPanel_60;
	// UnityEngine.UI.Text ZombieTurn::TutorialText
	Text_t1901882714 * ___TutorialText_61;
	// UnityEngine.UI.Dropdown ZombieTurn::DebugCardList
	Dropdown_t2274391225 * ___DebugCardList_62;
	// UnityEngine.Material ZombieTurn::damageMaterial
	Material_t340375123 * ___damageMaterial_63;
	// UnityEngine.AudioSource ZombieTurn::clickAudio
	AudioSource_t3935305588 * ___clickAudio_64;
	// UnityEngine.AudioSource ZombieTurn::audioZombieDead
	AudioSource_t3935305588 * ___audioZombieDead_65;
	// UnityEngine.AudioSource ZombieTurn::audioZombieEvades
	AudioSource_t3935305588 * ___audioZombieEvades_66;
	// UnityEngine.AudioSource ZombieTurn::audioHeroDead
	AudioSource_t3935305588 * ___audioHeroDead_67;
	// System.Boolean ZombieTurn::isDebugLog
	bool ___isDebugLog_68;
	// ViewableUI ZombieTurn::ViewableUI
	ViewableUI_t3820486676 * ___ViewableUI_69;
	// System.Collections.Generic.List`1<ZombieCard> ZombieTurn::<CardsToPlay>k__BackingField
	List_1_t3709728484 * ___U3CCardsToPlayU3Ek__BackingField_70;

public:
	inline static int32_t get_offset_of_setupState_2() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___setupState_2)); }
	inline List_1_t3319525431 * get_setupState_2() const { return ___setupState_2; }
	inline List_1_t3319525431 ** get_address_of_setupState_2() { return &___setupState_2; }
	inline void set_setupState_2(List_1_t3319525431 * value)
	{
		___setupState_2 = value;
		Il2CppCodeGenWriteBarrier((&___setupState_2), value);
	}

	inline static int32_t get_offset_of_decks_3() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___decks_3)); }
	inline ZombieDeck_t3038342722 * get_decks_3() const { return ___decks_3; }
	inline ZombieDeck_t3038342722 ** get_address_of_decks_3() { return &___decks_3; }
	inline void set_decks_3(ZombieDeck_t3038342722 * value)
	{
		___decks_3 = value;
		Il2CppCodeGenWriteBarrier((&___decks_3), value);
	}

	inline static int32_t get_offset_of_gameStats_4() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___gameStats_4)); }
	inline GameSession_t4087811243 * get_gameStats_4() const { return ___gameStats_4; }
	inline GameSession_t4087811243 ** get_address_of_gameStats_4() { return &___gameStats_4; }
	inline void set_gameStats_4(GameSession_t4087811243 * value)
	{
		___gameStats_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameStats_4), value);
	}

	inline static int32_t get_offset_of_spawn_5() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___spawn_5)); }
	inline bool get_spawn_5() const { return ___spawn_5; }
	inline bool* get_address_of_spawn_5() { return &___spawn_5; }
	inline void set_spawn_5(bool value)
	{
		___spawn_5 = value;
	}

	inline static int32_t get_offset_of_zombiesWinOnTie_6() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___zombiesWinOnTie_6)); }
	inline bool get_zombiesWinOnTie_6() const { return ___zombiesWinOnTie_6; }
	inline bool* get_address_of_zombiesWinOnTie_6() { return &___zombiesWinOnTie_6; }
	inline void set_zombiesWinOnTie_6(bool value)
	{
		___zombiesWinOnTie_6 = value;
	}

	inline static int32_t get_offset_of_zombieMovesAfterSpawn_7() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___zombieMovesAfterSpawn_7)); }
	inline bool get_zombieMovesAfterSpawn_7() const { return ___zombieMovesAfterSpawn_7; }
	inline bool* get_address_of_zombieMovesAfterSpawn_7() { return &___zombieMovesAfterSpawn_7; }
	inline void set_zombieMovesAfterSpawn_7(bool value)
	{
		___zombieMovesAfterSpawn_7 = value;
	}

	inline static int32_t get_offset_of_theHungryOne_8() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___theHungryOne_8)); }
	inline bool get_theHungryOne_8() const { return ___theHungryOne_8; }
	inline bool* get_address_of_theHungryOne_8() { return &___theHungryOne_8; }
	inline void set_theHungryOne_8(bool value)
	{
		___theHungryOne_8 = value;
	}

	inline static int32_t get_offset_of_fightCardUsed_9() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___fightCardUsed_9)); }
	inline bool get_fightCardUsed_9() const { return ___fightCardUsed_9; }
	inline bool* get_address_of_fightCardUsed_9() { return &___fightCardUsed_9; }
	inline void set_fightCardUsed_9(bool value)
	{
		___fightCardUsed_9 = value;
	}

	inline static int32_t get_offset_of_round_10() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___round_10)); }
	inline int32_t get_round_10() const { return ___round_10; }
	inline int32_t* get_address_of_round_10() { return &___round_10; }
	inline void set_round_10(int32_t value)
	{
		___round_10 = value;
	}

	inline static int32_t get_offset_of_heroesDiedCount_11() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___heroesDiedCount_11)); }
	inline int32_t get_heroesDiedCount_11() const { return ___heroesDiedCount_11; }
	inline int32_t* get_address_of_heroesDiedCount_11() { return &___heroesDiedCount_11; }
	inline void set_heroesDiedCount_11(int32_t value)
	{
		___heroesDiedCount_11 = value;
	}

	inline static int32_t get_offset_of_cardBeingPlayed_12() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___cardBeingPlayed_12)); }
	inline ZombieCard_t2237653742 * get_cardBeingPlayed_12() const { return ___cardBeingPlayed_12; }
	inline ZombieCard_t2237653742 ** get_address_of_cardBeingPlayed_12() { return &___cardBeingPlayed_12; }
	inline void set_cardBeingPlayed_12(ZombieCard_t2237653742 * value)
	{
		___cardBeingPlayed_12 = value;
		Il2CppCodeGenWriteBarrier((&___cardBeingPlayed_12), value);
	}

	inline static int32_t get_offset_of_gameData_13() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___gameData_13)); }
	inline GameData_t415813024 * get_gameData_13() const { return ___gameData_13; }
	inline GameData_t415813024 ** get_address_of_gameData_13() { return &___gameData_13; }
	inline void set_gameData_13(GameData_t415813024 * value)
	{
		___gameData_13 = value;
		Il2CppCodeGenWriteBarrier((&___gameData_13), value);
	}

	inline static int32_t get_offset_of_gameObjectives_14() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___gameObjectives_14)); }
	inline List_1_t2585711361 * get_gameObjectives_14() const { return ___gameObjectives_14; }
	inline List_1_t2585711361 ** get_address_of_gameObjectives_14() { return &___gameObjectives_14; }
	inline void set_gameObjectives_14(List_1_t2585711361 * value)
	{
		___gameObjectives_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectives_14), value);
	}

	inline static int32_t get_offset_of_currentSetupState_15() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___currentSetupState_15)); }
	inline String_t* get_currentSetupState_15() const { return ___currentSetupState_15; }
	inline String_t** get_address_of_currentSetupState_15() { return &___currentSetupState_15; }
	inline void set_currentSetupState_15(String_t* value)
	{
		___currentSetupState_15 = value;
		Il2CppCodeGenWriteBarrier((&___currentSetupState_15), value);
	}

	inline static int32_t get_offset_of_takenOverBuilding_16() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___takenOverBuilding_16)); }
	inline List_1_t3319525431 * get_takenOverBuilding_16() const { return ___takenOverBuilding_16; }
	inline List_1_t3319525431 ** get_address_of_takenOverBuilding_16() { return &___takenOverBuilding_16; }
	inline void set_takenOverBuilding_16(List_1_t3319525431 * value)
	{
		___takenOverBuilding_16 = value;
		Il2CppCodeGenWriteBarrier((&___takenOverBuilding_16), value);
	}

	inline static int32_t get_offset_of_zombieToFight_17() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___zombieToFight_17)); }
	inline String_t* get_zombieToFight_17() const { return ___zombieToFight_17; }
	inline String_t** get_address_of_zombieToFight_17() { return &___zombieToFight_17; }
	inline void set_zombieToFight_17(String_t* value)
	{
		___zombieToFight_17 = value;
		Il2CppCodeGenWriteBarrier((&___zombieToFight_17), value);
	}

	inline static int32_t get_offset_of_numberOfHeroesInGame_18() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___numberOfHeroesInGame_18)); }
	inline int32_t get_numberOfHeroesInGame_18() const { return ___numberOfHeroesInGame_18; }
	inline int32_t* get_address_of_numberOfHeroesInGame_18() { return &___numberOfHeroesInGame_18; }
	inline void set_numberOfHeroesInGame_18(int32_t value)
	{
		___numberOfHeroesInGame_18 = value;
	}

	inline static int32_t get_offset_of_cardsPlayed_19() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___cardsPlayed_19)); }
	inline List_1_t3319525431 * get_cardsPlayed_19() const { return ___cardsPlayed_19; }
	inline List_1_t3319525431 ** get_address_of_cardsPlayed_19() { return &___cardsPlayed_19; }
	inline void set_cardsPlayed_19(List_1_t3319525431 * value)
	{
		___cardsPlayed_19 = value;
		Il2CppCodeGenWriteBarrier((&___cardsPlayed_19), value);
	}

	inline static int32_t get_offset_of_Phases_20() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___Phases_20)); }
	inline ZombieTurnPhases_t3127322021 * get_Phases_20() const { return ___Phases_20; }
	inline ZombieTurnPhases_t3127322021 ** get_address_of_Phases_20() { return &___Phases_20; }
	inline void set_Phases_20(ZombieTurnPhases_t3127322021 * value)
	{
		___Phases_20 = value;
		Il2CppCodeGenWriteBarrier((&___Phases_20), value);
	}

	inline static int32_t get_offset_of_SetupPanel_21() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___SetupPanel_21)); }
	inline GameObject_t1113636619 * get_SetupPanel_21() const { return ___SetupPanel_21; }
	inline GameObject_t1113636619 ** get_address_of_SetupPanel_21() { return &___SetupPanel_21; }
	inline void set_SetupPanel_21(GameObject_t1113636619 * value)
	{
		___SetupPanel_21 = value;
		Il2CppCodeGenWriteBarrier((&___SetupPanel_21), value);
	}

	inline static int32_t get_offset_of_SetupText_22() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___SetupText_22)); }
	inline Text_t1901882714 * get_SetupText_22() const { return ___SetupText_22; }
	inline Text_t1901882714 ** get_address_of_SetupText_22() { return &___SetupText_22; }
	inline void set_SetupText_22(Text_t1901882714 * value)
	{
		___SetupText_22 = value;
		Il2CppCodeGenWriteBarrier((&___SetupText_22), value);
	}

	inline static int32_t get_offset_of_MainUI_23() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___MainUI_23)); }
	inline GameObject_t1113636619 * get_MainUI_23() const { return ___MainUI_23; }
	inline GameObject_t1113636619 ** get_address_of_MainUI_23() { return &___MainUI_23; }
	inline void set_MainUI_23(GameObject_t1113636619 * value)
	{
		___MainUI_23 = value;
		Il2CppCodeGenWriteBarrier((&___MainUI_23), value);
	}

	inline static int32_t get_offset_of_Card_24() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___Card_24)); }
	inline GameObject_t1113636619 * get_Card_24() const { return ___Card_24; }
	inline GameObject_t1113636619 ** get_address_of_Card_24() { return &___Card_24; }
	inline void set_Card_24(GameObject_t1113636619 * value)
	{
		___Card_24 = value;
		Il2CppCodeGenWriteBarrier((&___Card_24), value);
	}

	inline static int32_t get_offset_of_CardTitle_25() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___CardTitle_25)); }
	inline Text_t1901882714 * get_CardTitle_25() const { return ___CardTitle_25; }
	inline Text_t1901882714 ** get_address_of_CardTitle_25() { return &___CardTitle_25; }
	inline void set_CardTitle_25(Text_t1901882714 * value)
	{
		___CardTitle_25 = value;
		Il2CppCodeGenWriteBarrier((&___CardTitle_25), value);
	}

	inline static int32_t get_offset_of_CardText_26() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___CardText_26)); }
	inline Text_t1901882714 * get_CardText_26() const { return ___CardText_26; }
	inline Text_t1901882714 ** get_address_of_CardText_26() { return &___CardText_26; }
	inline void set_CardText_26(Text_t1901882714 * value)
	{
		___CardText_26 = value;
		Il2CppCodeGenWriteBarrier((&___CardText_26), value);
	}

	inline static int32_t get_offset_of_CardFlavor_27() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___CardFlavor_27)); }
	inline Text_t1901882714 * get_CardFlavor_27() const { return ___CardFlavor_27; }
	inline Text_t1901882714 ** get_address_of_CardFlavor_27() { return &___CardFlavor_27; }
	inline void set_CardFlavor_27(Text_t1901882714 * value)
	{
		___CardFlavor_27 = value;
		Il2CppCodeGenWriteBarrier((&___CardFlavor_27), value);
	}

	inline static int32_t get_offset_of_ZombiesOnBoardText_28() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ZombiesOnBoardText_28)); }
	inline Text_t1901882714 * get_ZombiesOnBoardText_28() const { return ___ZombiesOnBoardText_28; }
	inline Text_t1901882714 ** get_address_of_ZombiesOnBoardText_28() { return &___ZombiesOnBoardText_28; }
	inline void set_ZombiesOnBoardText_28(Text_t1901882714 * value)
	{
		___ZombiesOnBoardText_28 = value;
		Il2CppCodeGenWriteBarrier((&___ZombiesOnBoardText_28), value);
	}

	inline static int32_t get_offset_of_ScenarioName_29() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ScenarioName_29)); }
	inline Text_t1901882714 * get_ScenarioName_29() const { return ___ScenarioName_29; }
	inline Text_t1901882714 ** get_address_of_ScenarioName_29() { return &___ScenarioName_29; }
	inline void set_ScenarioName_29(Text_t1901882714 * value)
	{
		___ScenarioName_29 = value;
		Il2CppCodeGenWriteBarrier((&___ScenarioName_29), value);
	}

	inline static int32_t get_offset_of_MovePanel_30() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___MovePanel_30)); }
	inline GameObject_t1113636619 * get_MovePanel_30() const { return ___MovePanel_30; }
	inline GameObject_t1113636619 ** get_address_of_MovePanel_30() { return &___MovePanel_30; }
	inline void set_MovePanel_30(GameObject_t1113636619 * value)
	{
		___MovePanel_30 = value;
		Il2CppCodeGenWriteBarrier((&___MovePanel_30), value);
	}

	inline static int32_t get_offset_of_MoveText_31() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___MoveText_31)); }
	inline Text_t1901882714 * get_MoveText_31() const { return ___MoveText_31; }
	inline Text_t1901882714 ** get_address_of_MoveText_31() { return &___MoveText_31; }
	inline void set_MoveText_31(Text_t1901882714 * value)
	{
		___MoveText_31 = value;
		Il2CppCodeGenWriteBarrier((&___MoveText_31), value);
	}

	inline static int32_t get_offset_of_MoveZombieTypeText_32() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___MoveZombieTypeText_32)); }
	inline Text_t1901882714 * get_MoveZombieTypeText_32() const { return ___MoveZombieTypeText_32; }
	inline Text_t1901882714 ** get_address_of_MoveZombieTypeText_32() { return &___MoveZombieTypeText_32; }
	inline void set_MoveZombieTypeText_32(Text_t1901882714 * value)
	{
		___MoveZombieTypeText_32 = value;
		Il2CppCodeGenWriteBarrier((&___MoveZombieTypeText_32), value);
	}

	inline static int32_t get_offset_of_FightPanel_33() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___FightPanel_33)); }
	inline GameObject_t1113636619 * get_FightPanel_33() const { return ___FightPanel_33; }
	inline GameObject_t1113636619 ** get_address_of_FightPanel_33() { return &___FightPanel_33; }
	inline void set_FightPanel_33(GameObject_t1113636619 * value)
	{
		___FightPanel_33 = value;
		Il2CppCodeGenWriteBarrier((&___FightPanel_33), value);
	}

	inline static int32_t get_offset_of_FightText_34() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___FightText_34)); }
	inline Text_t1901882714 * get_FightText_34() const { return ___FightText_34; }
	inline Text_t1901882714 ** get_address_of_FightText_34() { return &___FightText_34; }
	inline void set_FightText_34(Text_t1901882714 * value)
	{
		___FightText_34 = value;
		Il2CppCodeGenWriteBarrier((&___FightText_34), value);
	}

	inline static int32_t get_offset_of_FightRollResultParent_35() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___FightRollResultParent_35)); }
	inline GameObject_t1113636619 * get_FightRollResultParent_35() const { return ___FightRollResultParent_35; }
	inline GameObject_t1113636619 ** get_address_of_FightRollResultParent_35() { return &___FightRollResultParent_35; }
	inline void set_FightRollResultParent_35(GameObject_t1113636619 * value)
	{
		___FightRollResultParent_35 = value;
		Il2CppCodeGenWriteBarrier((&___FightRollResultParent_35), value);
	}

	inline static int32_t get_offset_of_FightRollResultPrefab_36() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___FightRollResultPrefab_36)); }
	inline GameObject_t1113636619 * get_FightRollResultPrefab_36() const { return ___FightRollResultPrefab_36; }
	inline GameObject_t1113636619 ** get_address_of_FightRollResultPrefab_36() { return &___FightRollResultPrefab_36; }
	inline void set_FightRollResultPrefab_36(GameObject_t1113636619 * value)
	{
		___FightRollResultPrefab_36 = value;
		Il2CppCodeGenWriteBarrier((&___FightRollResultPrefab_36), value);
	}

	inline static int32_t get_offset_of_SpawnPanel_37() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___SpawnPanel_37)); }
	inline GameObject_t1113636619 * get_SpawnPanel_37() const { return ___SpawnPanel_37; }
	inline GameObject_t1113636619 ** get_address_of_SpawnPanel_37() { return &___SpawnPanel_37; }
	inline void set_SpawnPanel_37(GameObject_t1113636619 * value)
	{
		___SpawnPanel_37 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnPanel_37), value);
	}

	inline static int32_t get_offset_of_SpawnText_38() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___SpawnText_38)); }
	inline Text_t1901882714 * get_SpawnText_38() const { return ___SpawnText_38; }
	inline Text_t1901882714 ** get_address_of_SpawnText_38() { return &___SpawnText_38; }
	inline void set_SpawnText_38(Text_t1901882714 * value)
	{
		___SpawnText_38 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnText_38), value);
	}

	inline static int32_t get_offset_of_RemainsInPlay_39() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___RemainsInPlay_39)); }
	inline GameObject_t1113636619 * get_RemainsInPlay_39() const { return ___RemainsInPlay_39; }
	inline GameObject_t1113636619 ** get_address_of_RemainsInPlay_39() { return &___RemainsInPlay_39; }
	inline void set_RemainsInPlay_39(GameObject_t1113636619 * value)
	{
		___RemainsInPlay_39 = value;
		Il2CppCodeGenWriteBarrier((&___RemainsInPlay_39), value);
	}

	inline static int32_t get_offset_of_RemainsInPlayElement_40() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___RemainsInPlayElement_40)); }
	inline GameObject_t1113636619 * get_RemainsInPlayElement_40() const { return ___RemainsInPlayElement_40; }
	inline GameObject_t1113636619 ** get_address_of_RemainsInPlayElement_40() { return &___RemainsInPlayElement_40; }
	inline void set_RemainsInPlayElement_40(GameObject_t1113636619 * value)
	{
		___RemainsInPlayElement_40 = value;
		Il2CppCodeGenWriteBarrier((&___RemainsInPlayElement_40), value);
	}

	inline static int32_t get_offset_of_ResultPanel_41() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ResultPanel_41)); }
	inline GameObject_t1113636619 * get_ResultPanel_41() const { return ___ResultPanel_41; }
	inline GameObject_t1113636619 ** get_address_of_ResultPanel_41() { return &___ResultPanel_41; }
	inline void set_ResultPanel_41(GameObject_t1113636619 * value)
	{
		___ResultPanel_41 = value;
		Il2CppCodeGenWriteBarrier((&___ResultPanel_41), value);
	}

	inline static int32_t get_offset_of_ResultText_42() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ResultText_42)); }
	inline Text_t1901882714 * get_ResultText_42() const { return ___ResultText_42; }
	inline Text_t1901882714 ** get_address_of_ResultText_42() { return &___ResultText_42; }
	inline void set_ResultText_42(Text_t1901882714 * value)
	{
		___ResultText_42 = value;
		Il2CppCodeGenWriteBarrier((&___ResultText_42), value);
	}

	inline static int32_t get_offset_of_ResultNarrativeText_43() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ResultNarrativeText_43)); }
	inline Text_t1901882714 * get_ResultNarrativeText_43() const { return ___ResultNarrativeText_43; }
	inline Text_t1901882714 ** get_address_of_ResultNarrativeText_43() { return &___ResultNarrativeText_43; }
	inline void set_ResultNarrativeText_43(Text_t1901882714 * value)
	{
		___ResultNarrativeText_43 = value;
		Il2CppCodeGenWriteBarrier((&___ResultNarrativeText_43), value);
	}

	inline static int32_t get_offset_of_DeadHeroPanel_44() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DeadHeroPanel_44)); }
	inline GameObject_t1113636619 * get_DeadHeroPanel_44() const { return ___DeadHeroPanel_44; }
	inline GameObject_t1113636619 ** get_address_of_DeadHeroPanel_44() { return &___DeadHeroPanel_44; }
	inline void set_DeadHeroPanel_44(GameObject_t1113636619 * value)
	{
		___DeadHeroPanel_44 = value;
		Il2CppCodeGenWriteBarrier((&___DeadHeroPanel_44), value);
	}

	inline static int32_t get_offset_of_DeadZombiePanel_45() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DeadZombiePanel_45)); }
	inline GameObject_t1113636619 * get_DeadZombiePanel_45() const { return ___DeadZombiePanel_45; }
	inline GameObject_t1113636619 ** get_address_of_DeadZombiePanel_45() { return &___DeadZombiePanel_45; }
	inline void set_DeadZombiePanel_45(GameObject_t1113636619 * value)
	{
		___DeadZombiePanel_45 = value;
		Il2CppCodeGenWriteBarrier((&___DeadZombiePanel_45), value);
	}

	inline static int32_t get_offset_of_TextAnimation_46() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___TextAnimation_46)); }
	inline TextAnimation_t2694049294 * get_TextAnimation_46() const { return ___TextAnimation_46; }
	inline TextAnimation_t2694049294 ** get_address_of_TextAnimation_46() { return &___TextAnimation_46; }
	inline void set_TextAnimation_46(TextAnimation_t2694049294 * value)
	{
		___TextAnimation_46 = value;
		Il2CppCodeGenWriteBarrier((&___TextAnimation_46), value);
	}

	inline static int32_t get_offset_of_GameLog_47() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___GameLog_47)); }
	inline GameLog_t2697247111 * get_GameLog_47() const { return ___GameLog_47; }
	inline GameLog_t2697247111 ** get_address_of_GameLog_47() { return &___GameLog_47; }
	inline void set_GameLog_47(GameLog_t2697247111 * value)
	{
		___GameLog_47 = value;
		Il2CppCodeGenWriteBarrier((&___GameLog_47), value);
	}

	inline static int32_t get_offset_of_MapImages_48() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___MapImages_48)); }
	inline PlaceMapImages_t1484071588 * get_MapImages_48() const { return ___MapImages_48; }
	inline PlaceMapImages_t1484071588 ** get_address_of_MapImages_48() { return &___MapImages_48; }
	inline void set_MapImages_48(PlaceMapImages_t1484071588 * value)
	{
		___MapImages_48 = value;
		Il2CppCodeGenWriteBarrier((&___MapImages_48), value);
	}

	inline static int32_t get_offset_of_ZombieObjectivePanel_49() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ZombieObjectivePanel_49)); }
	inline GameObject_t1113636619 * get_ZombieObjectivePanel_49() const { return ___ZombieObjectivePanel_49; }
	inline GameObject_t1113636619 ** get_address_of_ZombieObjectivePanel_49() { return &___ZombieObjectivePanel_49; }
	inline void set_ZombieObjectivePanel_49(GameObject_t1113636619 * value)
	{
		___ZombieObjectivePanel_49 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieObjectivePanel_49), value);
	}

	inline static int32_t get_offset_of_HeroObjectivePanel_50() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___HeroObjectivePanel_50)); }
	inline GameObject_t1113636619 * get_HeroObjectivePanel_50() const { return ___HeroObjectivePanel_50; }
	inline GameObject_t1113636619 ** get_address_of_HeroObjectivePanel_50() { return &___HeroObjectivePanel_50; }
	inline void set_HeroObjectivePanel_50(GameObject_t1113636619 * value)
	{
		___HeroObjectivePanel_50 = value;
		Il2CppCodeGenWriteBarrier((&___HeroObjectivePanel_50), value);
	}

	inline static int32_t get_offset_of_ObjectivePrefab_51() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ObjectivePrefab_51)); }
	inline GameObject_t1113636619 * get_ObjectivePrefab_51() const { return ___ObjectivePrefab_51; }
	inline GameObject_t1113636619 ** get_address_of_ObjectivePrefab_51() { return &___ObjectivePrefab_51; }
	inline void set_ObjectivePrefab_51(GameObject_t1113636619 * value)
	{
		___ObjectivePrefab_51 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectivePrefab_51), value);
	}

	inline static int32_t get_offset_of_NarrativePanel_52() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___NarrativePanel_52)); }
	inline GameObject_t1113636619 * get_NarrativePanel_52() const { return ___NarrativePanel_52; }
	inline GameObject_t1113636619 ** get_address_of_NarrativePanel_52() { return &___NarrativePanel_52; }
	inline void set_NarrativePanel_52(GameObject_t1113636619 * value)
	{
		___NarrativePanel_52 = value;
		Il2CppCodeGenWriteBarrier((&___NarrativePanel_52), value);
	}

	inline static int32_t get_offset_of_NarrativeTitle_53() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___NarrativeTitle_53)); }
	inline Text_t1901882714 * get_NarrativeTitle_53() const { return ___NarrativeTitle_53; }
	inline Text_t1901882714 ** get_address_of_NarrativeTitle_53() { return &___NarrativeTitle_53; }
	inline void set_NarrativeTitle_53(Text_t1901882714 * value)
	{
		___NarrativeTitle_53 = value;
		Il2CppCodeGenWriteBarrier((&___NarrativeTitle_53), value);
	}

	inline static int32_t get_offset_of_NarrativeText_54() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___NarrativeText_54)); }
	inline Text_t1901882714 * get_NarrativeText_54() const { return ___NarrativeText_54; }
	inline Text_t1901882714 ** get_address_of_NarrativeText_54() { return &___NarrativeText_54; }
	inline void set_NarrativeText_54(Text_t1901882714 * value)
	{
		___NarrativeText_54 = value;
		Il2CppCodeGenWriteBarrier((&___NarrativeText_54), value);
	}

	inline static int32_t get_offset_of_DestroyBuildingPanel_55() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DestroyBuildingPanel_55)); }
	inline GameObject_t1113636619 * get_DestroyBuildingPanel_55() const { return ___DestroyBuildingPanel_55; }
	inline GameObject_t1113636619 ** get_address_of_DestroyBuildingPanel_55() { return &___DestroyBuildingPanel_55; }
	inline void set_DestroyBuildingPanel_55(GameObject_t1113636619 * value)
	{
		___DestroyBuildingPanel_55 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyBuildingPanel_55), value);
	}

	inline static int32_t get_offset_of_DestroyBuildingZombieSlider_56() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DestroyBuildingZombieSlider_56)); }
	inline Slider_t3903728902 * get_DestroyBuildingZombieSlider_56() const { return ___DestroyBuildingZombieSlider_56; }
	inline Slider_t3903728902 ** get_address_of_DestroyBuildingZombieSlider_56() { return &___DestroyBuildingZombieSlider_56; }
	inline void set_DestroyBuildingZombieSlider_56(Slider_t3903728902 * value)
	{
		___DestroyBuildingZombieSlider_56 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyBuildingZombieSlider_56), value);
	}

	inline static int32_t get_offset_of_DestroyBuildingDropdown_57() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DestroyBuildingDropdown_57)); }
	inline Dropdown_t2274391225 * get_DestroyBuildingDropdown_57() const { return ___DestroyBuildingDropdown_57; }
	inline Dropdown_t2274391225 ** get_address_of_DestroyBuildingDropdown_57() { return &___DestroyBuildingDropdown_57; }
	inline void set_DestroyBuildingDropdown_57(Dropdown_t2274391225 * value)
	{
		___DestroyBuildingDropdown_57 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyBuildingDropdown_57), value);
	}

	inline static int32_t get_offset_of_DestroyBuildingDeadZombiesText_58() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DestroyBuildingDeadZombiesText_58)); }
	inline Text_t1901882714 * get_DestroyBuildingDeadZombiesText_58() const { return ___DestroyBuildingDeadZombiesText_58; }
	inline Text_t1901882714 ** get_address_of_DestroyBuildingDeadZombiesText_58() { return &___DestroyBuildingDeadZombiesText_58; }
	inline void set_DestroyBuildingDeadZombiesText_58(Text_t1901882714 * value)
	{
		___DestroyBuildingDeadZombiesText_58 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyBuildingDeadZombiesText_58), value);
	}

	inline static int32_t get_offset_of_ZombiePitPanel_59() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ZombiePitPanel_59)); }
	inline GameObject_t1113636619 * get_ZombiePitPanel_59() const { return ___ZombiePitPanel_59; }
	inline GameObject_t1113636619 ** get_address_of_ZombiePitPanel_59() { return &___ZombiePitPanel_59; }
	inline void set_ZombiePitPanel_59(GameObject_t1113636619 * value)
	{
		___ZombiePitPanel_59 = value;
		Il2CppCodeGenWriteBarrier((&___ZombiePitPanel_59), value);
	}

	inline static int32_t get_offset_of_TutorialPanel_60() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___TutorialPanel_60)); }
	inline GameObject_t1113636619 * get_TutorialPanel_60() const { return ___TutorialPanel_60; }
	inline GameObject_t1113636619 ** get_address_of_TutorialPanel_60() { return &___TutorialPanel_60; }
	inline void set_TutorialPanel_60(GameObject_t1113636619 * value)
	{
		___TutorialPanel_60 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialPanel_60), value);
	}

	inline static int32_t get_offset_of_TutorialText_61() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___TutorialText_61)); }
	inline Text_t1901882714 * get_TutorialText_61() const { return ___TutorialText_61; }
	inline Text_t1901882714 ** get_address_of_TutorialText_61() { return &___TutorialText_61; }
	inline void set_TutorialText_61(Text_t1901882714 * value)
	{
		___TutorialText_61 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialText_61), value);
	}

	inline static int32_t get_offset_of_DebugCardList_62() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DebugCardList_62)); }
	inline Dropdown_t2274391225 * get_DebugCardList_62() const { return ___DebugCardList_62; }
	inline Dropdown_t2274391225 ** get_address_of_DebugCardList_62() { return &___DebugCardList_62; }
	inline void set_DebugCardList_62(Dropdown_t2274391225 * value)
	{
		___DebugCardList_62 = value;
		Il2CppCodeGenWriteBarrier((&___DebugCardList_62), value);
	}

	inline static int32_t get_offset_of_damageMaterial_63() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___damageMaterial_63)); }
	inline Material_t340375123 * get_damageMaterial_63() const { return ___damageMaterial_63; }
	inline Material_t340375123 ** get_address_of_damageMaterial_63() { return &___damageMaterial_63; }
	inline void set_damageMaterial_63(Material_t340375123 * value)
	{
		___damageMaterial_63 = value;
		Il2CppCodeGenWriteBarrier((&___damageMaterial_63), value);
	}

	inline static int32_t get_offset_of_clickAudio_64() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___clickAudio_64)); }
	inline AudioSource_t3935305588 * get_clickAudio_64() const { return ___clickAudio_64; }
	inline AudioSource_t3935305588 ** get_address_of_clickAudio_64() { return &___clickAudio_64; }
	inline void set_clickAudio_64(AudioSource_t3935305588 * value)
	{
		___clickAudio_64 = value;
		Il2CppCodeGenWriteBarrier((&___clickAudio_64), value);
	}

	inline static int32_t get_offset_of_audioZombieDead_65() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___audioZombieDead_65)); }
	inline AudioSource_t3935305588 * get_audioZombieDead_65() const { return ___audioZombieDead_65; }
	inline AudioSource_t3935305588 ** get_address_of_audioZombieDead_65() { return &___audioZombieDead_65; }
	inline void set_audioZombieDead_65(AudioSource_t3935305588 * value)
	{
		___audioZombieDead_65 = value;
		Il2CppCodeGenWriteBarrier((&___audioZombieDead_65), value);
	}

	inline static int32_t get_offset_of_audioZombieEvades_66() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___audioZombieEvades_66)); }
	inline AudioSource_t3935305588 * get_audioZombieEvades_66() const { return ___audioZombieEvades_66; }
	inline AudioSource_t3935305588 ** get_address_of_audioZombieEvades_66() { return &___audioZombieEvades_66; }
	inline void set_audioZombieEvades_66(AudioSource_t3935305588 * value)
	{
		___audioZombieEvades_66 = value;
		Il2CppCodeGenWriteBarrier((&___audioZombieEvades_66), value);
	}

	inline static int32_t get_offset_of_audioHeroDead_67() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___audioHeroDead_67)); }
	inline AudioSource_t3935305588 * get_audioHeroDead_67() const { return ___audioHeroDead_67; }
	inline AudioSource_t3935305588 ** get_address_of_audioHeroDead_67() { return &___audioHeroDead_67; }
	inline void set_audioHeroDead_67(AudioSource_t3935305588 * value)
	{
		___audioHeroDead_67 = value;
		Il2CppCodeGenWriteBarrier((&___audioHeroDead_67), value);
	}

	inline static int32_t get_offset_of_isDebugLog_68() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___isDebugLog_68)); }
	inline bool get_isDebugLog_68() const { return ___isDebugLog_68; }
	inline bool* get_address_of_isDebugLog_68() { return &___isDebugLog_68; }
	inline void set_isDebugLog_68(bool value)
	{
		___isDebugLog_68 = value;
	}

	inline static int32_t get_offset_of_ViewableUI_69() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ViewableUI_69)); }
	inline ViewableUI_t3820486676 * get_ViewableUI_69() const { return ___ViewableUI_69; }
	inline ViewableUI_t3820486676 ** get_address_of_ViewableUI_69() { return &___ViewableUI_69; }
	inline void set_ViewableUI_69(ViewableUI_t3820486676 * value)
	{
		___ViewableUI_69 = value;
		Il2CppCodeGenWriteBarrier((&___ViewableUI_69), value);
	}

	inline static int32_t get_offset_of_U3CCardsToPlayU3Ek__BackingField_70() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___U3CCardsToPlayU3Ek__BackingField_70)); }
	inline List_1_t3709728484 * get_U3CCardsToPlayU3Ek__BackingField_70() const { return ___U3CCardsToPlayU3Ek__BackingField_70; }
	inline List_1_t3709728484 ** get_address_of_U3CCardsToPlayU3Ek__BackingField_70() { return &___U3CCardsToPlayU3Ek__BackingField_70; }
	inline void set_U3CCardsToPlayU3Ek__BackingField_70(List_1_t3709728484 * value)
	{
		___U3CCardsToPlayU3Ek__BackingField_70 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCardsToPlayU3Ek__BackingField_70), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIETURN_T3562480803_H
#ifndef TEXTANIMATION_T2694049294_H
#define TEXTANIMATION_T2694049294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextAnimation
struct  TextAnimation_t2694049294  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator TextAnimation::textAnimator
	Animator_t434523843 * ___textAnimator_2;

public:
	inline static int32_t get_offset_of_textAnimator_2() { return static_cast<int32_t>(offsetof(TextAnimation_t2694049294, ___textAnimator_2)); }
	inline Animator_t434523843 * get_textAnimator_2() const { return ___textAnimator_2; }
	inline Animator_t434523843 ** get_address_of_textAnimator_2() { return &___textAnimator_2; }
	inline void set_textAnimator_2(Animator_t434523843 * value)
	{
		___textAnimator_2 = value;
		Il2CppCodeGenWriteBarrier((&___textAnimator_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANIMATION_T2694049294_H
#ifndef GAMELOG_T2697247111_H
#define GAMELOG_T2697247111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameLog
struct  GameLog_t2697247111  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GameLog::Contents
	GameObject_t1113636619 * ___Contents_2;
	// UnityEngine.GameObject GameLog::ContentsRender
	GameObject_t1113636619 * ___ContentsRender_3;
	// UnityEngine.GameObject GameLog::TextPrefab
	GameObject_t1113636619 * ___TextPrefab_4;
	// UnityEngine.GameObject GameLog::ImagePrefab
	GameObject_t1113636619 * ___ImagePrefab_5;
	// UnityEngine.Camera GameLog::LogCamera
	Camera_t4157153871 * ___LogCamera_6;
	// UnityEngine.RenderTexture GameLog::logRenderTexture
	RenderTexture_t2108887433 * ___logRenderTexture_7;
	// System.Boolean GameLog::grab
	bool ___grab_8;
	// UnityEngine.GameObject GameLog::dupContent
	GameObject_t1113636619 * ___dupContent_9;
	// System.Int32 GameLog::textObjects
	int32_t ___textObjects_10;
	// System.Int32 GameLog::imageObjects
	int32_t ___imageObjects_11;
	// System.Collections.Generic.List`1<GameSaveData/GameLogEntries> GameLog::GameLogSave
	List_1_t2151963976 * ___GameLogSave_12;
	// System.Single GameLog::imageHeight
	float ___imageHeight_15;

public:
	inline static int32_t get_offset_of_Contents_2() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___Contents_2)); }
	inline GameObject_t1113636619 * get_Contents_2() const { return ___Contents_2; }
	inline GameObject_t1113636619 ** get_address_of_Contents_2() { return &___Contents_2; }
	inline void set_Contents_2(GameObject_t1113636619 * value)
	{
		___Contents_2 = value;
		Il2CppCodeGenWriteBarrier((&___Contents_2), value);
	}

	inline static int32_t get_offset_of_ContentsRender_3() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___ContentsRender_3)); }
	inline GameObject_t1113636619 * get_ContentsRender_3() const { return ___ContentsRender_3; }
	inline GameObject_t1113636619 ** get_address_of_ContentsRender_3() { return &___ContentsRender_3; }
	inline void set_ContentsRender_3(GameObject_t1113636619 * value)
	{
		___ContentsRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___ContentsRender_3), value);
	}

	inline static int32_t get_offset_of_TextPrefab_4() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___TextPrefab_4)); }
	inline GameObject_t1113636619 * get_TextPrefab_4() const { return ___TextPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_TextPrefab_4() { return &___TextPrefab_4; }
	inline void set_TextPrefab_4(GameObject_t1113636619 * value)
	{
		___TextPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextPrefab_4), value);
	}

	inline static int32_t get_offset_of_ImagePrefab_5() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___ImagePrefab_5)); }
	inline GameObject_t1113636619 * get_ImagePrefab_5() const { return ___ImagePrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_ImagePrefab_5() { return &___ImagePrefab_5; }
	inline void set_ImagePrefab_5(GameObject_t1113636619 * value)
	{
		___ImagePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___ImagePrefab_5), value);
	}

	inline static int32_t get_offset_of_LogCamera_6() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___LogCamera_6)); }
	inline Camera_t4157153871 * get_LogCamera_6() const { return ___LogCamera_6; }
	inline Camera_t4157153871 ** get_address_of_LogCamera_6() { return &___LogCamera_6; }
	inline void set_LogCamera_6(Camera_t4157153871 * value)
	{
		___LogCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___LogCamera_6), value);
	}

	inline static int32_t get_offset_of_logRenderTexture_7() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___logRenderTexture_7)); }
	inline RenderTexture_t2108887433 * get_logRenderTexture_7() const { return ___logRenderTexture_7; }
	inline RenderTexture_t2108887433 ** get_address_of_logRenderTexture_7() { return &___logRenderTexture_7; }
	inline void set_logRenderTexture_7(RenderTexture_t2108887433 * value)
	{
		___logRenderTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___logRenderTexture_7), value);
	}

	inline static int32_t get_offset_of_grab_8() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___grab_8)); }
	inline bool get_grab_8() const { return ___grab_8; }
	inline bool* get_address_of_grab_8() { return &___grab_8; }
	inline void set_grab_8(bool value)
	{
		___grab_8 = value;
	}

	inline static int32_t get_offset_of_dupContent_9() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___dupContent_9)); }
	inline GameObject_t1113636619 * get_dupContent_9() const { return ___dupContent_9; }
	inline GameObject_t1113636619 ** get_address_of_dupContent_9() { return &___dupContent_9; }
	inline void set_dupContent_9(GameObject_t1113636619 * value)
	{
		___dupContent_9 = value;
		Il2CppCodeGenWriteBarrier((&___dupContent_9), value);
	}

	inline static int32_t get_offset_of_textObjects_10() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___textObjects_10)); }
	inline int32_t get_textObjects_10() const { return ___textObjects_10; }
	inline int32_t* get_address_of_textObjects_10() { return &___textObjects_10; }
	inline void set_textObjects_10(int32_t value)
	{
		___textObjects_10 = value;
	}

	inline static int32_t get_offset_of_imageObjects_11() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___imageObjects_11)); }
	inline int32_t get_imageObjects_11() const { return ___imageObjects_11; }
	inline int32_t* get_address_of_imageObjects_11() { return &___imageObjects_11; }
	inline void set_imageObjects_11(int32_t value)
	{
		___imageObjects_11 = value;
	}

	inline static int32_t get_offset_of_GameLogSave_12() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___GameLogSave_12)); }
	inline List_1_t2151963976 * get_GameLogSave_12() const { return ___GameLogSave_12; }
	inline List_1_t2151963976 ** get_address_of_GameLogSave_12() { return &___GameLogSave_12; }
	inline void set_GameLogSave_12(List_1_t2151963976 * value)
	{
		___GameLogSave_12 = value;
		Il2CppCodeGenWriteBarrier((&___GameLogSave_12), value);
	}

	inline static int32_t get_offset_of_imageHeight_15() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___imageHeight_15)); }
	inline float get_imageHeight_15() const { return ___imageHeight_15; }
	inline float* get_address_of_imageHeight_15() { return &___imageHeight_15; }
	inline void set_imageHeight_15(float value)
	{
		___imageHeight_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMELOG_T2697247111_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef DROPDOWN_T2274391225_H
#define DROPDOWN_T2274391225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Dropdown
struct  Dropdown_t2274391225  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t3704657025 * ___m_Template_16;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_t1901882714 * ___m_CaptionText_17;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_t2670269651 * ___m_CaptionImage_18;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_t1901882714 * ___m_ItemText_19;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_t2670269651 * ___m_ItemImage_20;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_21;
	// UnityEngine.UI.Dropdown/OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_t1438173104 * ___m_Options_22;
	// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_t4040729994 * ___m_OnValueChanged_23;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_t1113636619 * ___m_Dropdown_24;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_t1113636619 * ___m_Blocker_25;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t2924027637 * ___m_Items_26;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t3520241082 * ___m_AlphaTweenRunner_27;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_28;

public:
	inline static int32_t get_offset_of_m_Template_16() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Template_16)); }
	inline RectTransform_t3704657025 * get_m_Template_16() const { return ___m_Template_16; }
	inline RectTransform_t3704657025 ** get_address_of_m_Template_16() { return &___m_Template_16; }
	inline void set_m_Template_16(RectTransform_t3704657025 * value)
	{
		___m_Template_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Template_16), value);
	}

	inline static int32_t get_offset_of_m_CaptionText_17() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_CaptionText_17)); }
	inline Text_t1901882714 * get_m_CaptionText_17() const { return ___m_CaptionText_17; }
	inline Text_t1901882714 ** get_address_of_m_CaptionText_17() { return &___m_CaptionText_17; }
	inline void set_m_CaptionText_17(Text_t1901882714 * value)
	{
		___m_CaptionText_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionText_17), value);
	}

	inline static int32_t get_offset_of_m_CaptionImage_18() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_CaptionImage_18)); }
	inline Image_t2670269651 * get_m_CaptionImage_18() const { return ___m_CaptionImage_18; }
	inline Image_t2670269651 ** get_address_of_m_CaptionImage_18() { return &___m_CaptionImage_18; }
	inline void set_m_CaptionImage_18(Image_t2670269651 * value)
	{
		___m_CaptionImage_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionImage_18), value);
	}

	inline static int32_t get_offset_of_m_ItemText_19() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_ItemText_19)); }
	inline Text_t1901882714 * get_m_ItemText_19() const { return ___m_ItemText_19; }
	inline Text_t1901882714 ** get_address_of_m_ItemText_19() { return &___m_ItemText_19; }
	inline void set_m_ItemText_19(Text_t1901882714 * value)
	{
		___m_ItemText_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemText_19), value);
	}

	inline static int32_t get_offset_of_m_ItemImage_20() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_ItemImage_20)); }
	inline Image_t2670269651 * get_m_ItemImage_20() const { return ___m_ItemImage_20; }
	inline Image_t2670269651 ** get_address_of_m_ItemImage_20() { return &___m_ItemImage_20; }
	inline void set_m_ItemImage_20(Image_t2670269651 * value)
	{
		___m_ItemImage_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemImage_20), value);
	}

	inline static int32_t get_offset_of_m_Value_21() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Value_21)); }
	inline int32_t get_m_Value_21() const { return ___m_Value_21; }
	inline int32_t* get_address_of_m_Value_21() { return &___m_Value_21; }
	inline void set_m_Value_21(int32_t value)
	{
		___m_Value_21 = value;
	}

	inline static int32_t get_offset_of_m_Options_22() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Options_22)); }
	inline OptionDataList_t1438173104 * get_m_Options_22() const { return ___m_Options_22; }
	inline OptionDataList_t1438173104 ** get_address_of_m_Options_22() { return &___m_Options_22; }
	inline void set_m_Options_22(OptionDataList_t1438173104 * value)
	{
		___m_Options_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Options_22), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_23() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_OnValueChanged_23)); }
	inline DropdownEvent_t4040729994 * get_m_OnValueChanged_23() const { return ___m_OnValueChanged_23; }
	inline DropdownEvent_t4040729994 ** get_address_of_m_OnValueChanged_23() { return &___m_OnValueChanged_23; }
	inline void set_m_OnValueChanged_23(DropdownEvent_t4040729994 * value)
	{
		___m_OnValueChanged_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_23), value);
	}

	inline static int32_t get_offset_of_m_Dropdown_24() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Dropdown_24)); }
	inline GameObject_t1113636619 * get_m_Dropdown_24() const { return ___m_Dropdown_24; }
	inline GameObject_t1113636619 ** get_address_of_m_Dropdown_24() { return &___m_Dropdown_24; }
	inline void set_m_Dropdown_24(GameObject_t1113636619 * value)
	{
		___m_Dropdown_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dropdown_24), value);
	}

	inline static int32_t get_offset_of_m_Blocker_25() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Blocker_25)); }
	inline GameObject_t1113636619 * get_m_Blocker_25() const { return ___m_Blocker_25; }
	inline GameObject_t1113636619 ** get_address_of_m_Blocker_25() { return &___m_Blocker_25; }
	inline void set_m_Blocker_25(GameObject_t1113636619 * value)
	{
		___m_Blocker_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blocker_25), value);
	}

	inline static int32_t get_offset_of_m_Items_26() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Items_26)); }
	inline List_1_t2924027637 * get_m_Items_26() const { return ___m_Items_26; }
	inline List_1_t2924027637 ** get_address_of_m_Items_26() { return &___m_Items_26; }
	inline void set_m_Items_26(List_1_t2924027637 * value)
	{
		___m_Items_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Items_26), value);
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_27() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_AlphaTweenRunner_27)); }
	inline TweenRunner_1_t3520241082 * get_m_AlphaTweenRunner_27() const { return ___m_AlphaTweenRunner_27; }
	inline TweenRunner_1_t3520241082 ** get_address_of_m_AlphaTweenRunner_27() { return &___m_AlphaTweenRunner_27; }
	inline void set_m_AlphaTweenRunner_27(TweenRunner_1_t3520241082 * value)
	{
		___m_AlphaTweenRunner_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlphaTweenRunner_27), value);
	}

	inline static int32_t get_offset_of_validTemplate_28() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___validTemplate_28)); }
	inline bool get_validTemplate_28() const { return ___validTemplate_28; }
	inline bool* get_address_of_validTemplate_28() { return &___validTemplate_28; }
	inline void set_validTemplate_28(bool value)
	{
		___validTemplate_28 = value;
	}
};

struct Dropdown_t2274391225_StaticFields
{
public:
	// UnityEngine.UI.Dropdown/OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t3270282352 * ___s_NoOptionData_29;

public:
	inline static int32_t get_offset_of_s_NoOptionData_29() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225_StaticFields, ___s_NoOptionData_29)); }
	inline OptionData_t3270282352 * get_s_NoOptionData_29() const { return ___s_NoOptionData_29; }
	inline OptionData_t3270282352 ** get_address_of_s_NoOptionData_29() { return &___s_NoOptionData_29; }
	inline void set_s_NoOptionData_29(OptionData_t3270282352 * value)
	{
		___s_NoOptionData_29 = value;
		Il2CppCodeGenWriteBarrier((&___s_NoOptionData_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWN_T2274391225_H
#ifndef SLIDER_T3903728902_H
#define SLIDER_T3903728902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider
struct  Slider_t3903728902  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t3704657025 * ___m_FillRect_16;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t3704657025 * ___m_HandleRect_17;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_18;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_19;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_20;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_21;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_22;
	// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t3180273144 * ___m_OnValueChanged_23;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t2670269651 * ___m_FillImage_24;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_t3600365921 * ___m_FillTransform_25;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t3704657025 * ___m_FillContainerRect_26;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_t3600365921 * ___m_HandleTransform_27;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t3704657025 * ___m_HandleContainerRect_28;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_t2156229523  ___m_Offset_29;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_30;

public:
	inline static int32_t get_offset_of_m_FillRect_16() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillRect_16)); }
	inline RectTransform_t3704657025 * get_m_FillRect_16() const { return ___m_FillRect_16; }
	inline RectTransform_t3704657025 ** get_address_of_m_FillRect_16() { return &___m_FillRect_16; }
	inline void set_m_FillRect_16(RectTransform_t3704657025 * value)
	{
		___m_FillRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillRect_16), value);
	}

	inline static int32_t get_offset_of_m_HandleRect_17() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_HandleRect_17)); }
	inline RectTransform_t3704657025 * get_m_HandleRect_17() const { return ___m_HandleRect_17; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleRect_17() { return &___m_HandleRect_17; }
	inline void set_m_HandleRect_17(RectTransform_t3704657025 * value)
	{
		___m_HandleRect_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_17), value);
	}

	inline static int32_t get_offset_of_m_Direction_18() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Direction_18)); }
	inline int32_t get_m_Direction_18() const { return ___m_Direction_18; }
	inline int32_t* get_address_of_m_Direction_18() { return &___m_Direction_18; }
	inline void set_m_Direction_18(int32_t value)
	{
		___m_Direction_18 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_19() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_MinValue_19)); }
	inline float get_m_MinValue_19() const { return ___m_MinValue_19; }
	inline float* get_address_of_m_MinValue_19() { return &___m_MinValue_19; }
	inline void set_m_MinValue_19(float value)
	{
		___m_MinValue_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_20() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_MaxValue_20)); }
	inline float get_m_MaxValue_20() const { return ___m_MaxValue_20; }
	inline float* get_address_of_m_MaxValue_20() { return &___m_MaxValue_20; }
	inline void set_m_MaxValue_20(float value)
	{
		___m_MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_21() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_WholeNumbers_21)); }
	inline bool get_m_WholeNumbers_21() const { return ___m_WholeNumbers_21; }
	inline bool* get_address_of_m_WholeNumbers_21() { return &___m_WholeNumbers_21; }
	inline void set_m_WholeNumbers_21(bool value)
	{
		___m_WholeNumbers_21 = value;
	}

	inline static int32_t get_offset_of_m_Value_22() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Value_22)); }
	inline float get_m_Value_22() const { return ___m_Value_22; }
	inline float* get_address_of_m_Value_22() { return &___m_Value_22; }
	inline void set_m_Value_22(float value)
	{
		___m_Value_22 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_23() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_OnValueChanged_23)); }
	inline SliderEvent_t3180273144 * get_m_OnValueChanged_23() const { return ___m_OnValueChanged_23; }
	inline SliderEvent_t3180273144 ** get_address_of_m_OnValueChanged_23() { return &___m_OnValueChanged_23; }
	inline void set_m_OnValueChanged_23(SliderEvent_t3180273144 * value)
	{
		___m_OnValueChanged_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_23), value);
	}

	inline static int32_t get_offset_of_m_FillImage_24() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillImage_24)); }
	inline Image_t2670269651 * get_m_FillImage_24() const { return ___m_FillImage_24; }
	inline Image_t2670269651 ** get_address_of_m_FillImage_24() { return &___m_FillImage_24; }
	inline void set_m_FillImage_24(Image_t2670269651 * value)
	{
		___m_FillImage_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillImage_24), value);
	}

	inline static int32_t get_offset_of_m_FillTransform_25() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillTransform_25)); }
	inline Transform_t3600365921 * get_m_FillTransform_25() const { return ___m_FillTransform_25; }
	inline Transform_t3600365921 ** get_address_of_m_FillTransform_25() { return &___m_FillTransform_25; }
	inline void set_m_FillTransform_25(Transform_t3600365921 * value)
	{
		___m_FillTransform_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillTransform_25), value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_26() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillContainerRect_26)); }
	inline RectTransform_t3704657025 * get_m_FillContainerRect_26() const { return ___m_FillContainerRect_26; }
	inline RectTransform_t3704657025 ** get_address_of_m_FillContainerRect_26() { return &___m_FillContainerRect_26; }
	inline void set_m_FillContainerRect_26(RectTransform_t3704657025 * value)
	{
		___m_FillContainerRect_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillContainerRect_26), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_27() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_HandleTransform_27)); }
	inline Transform_t3600365921 * get_m_HandleTransform_27() const { return ___m_HandleTransform_27; }
	inline Transform_t3600365921 ** get_address_of_m_HandleTransform_27() { return &___m_HandleTransform_27; }
	inline void set_m_HandleTransform_27(Transform_t3600365921 * value)
	{
		___m_HandleTransform_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_27), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_28() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_HandleContainerRect_28)); }
	inline RectTransform_t3704657025 * get_m_HandleContainerRect_28() const { return ___m_HandleContainerRect_28; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleContainerRect_28() { return &___m_HandleContainerRect_28; }
	inline void set_m_HandleContainerRect_28(RectTransform_t3704657025 * value)
	{
		___m_HandleContainerRect_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_28), value);
	}

	inline static int32_t get_offset_of_m_Offset_29() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Offset_29)); }
	inline Vector2_t2156229523  get_m_Offset_29() const { return ___m_Offset_29; }
	inline Vector2_t2156229523 * get_address_of_m_Offset_29() { return &___m_Offset_29; }
	inline void set_m_Offset_29(Vector2_t2156229523  value)
	{
		___m_Offset_29 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_30() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Tracker_30)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_30() const { return ___m_Tracker_30; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_30() { return &___m_Tracker_30; }
	inline void set_m_Tracker_30(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDER_T3903728902_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
// System.Int32[]
struct Int32U5BU5D_t385246372  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C"  bool List_1_Contains_m2654125393_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m3709462088_gshared (List_1_t257213610 * __this, RuntimeObject* p0, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurn::ReRollFightDie(System.Int32)
extern "C"  void ZombieTurn_ReRollFightDie_m203485653 (ZombieTurn_t3562480803 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t434523843_m440019408(__this, method) ((  Animator_t434523843 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t509032636  Animator_GetCurrentAnimatorStateInfo_m18694920 (Animator_t434523843 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C"  bool AnimatorStateInfo_IsName_m3393819976 (AnimatorStateInfo_t509032636 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String ZombieCard::GetName()
extern "C"  String_t* ZombieCard_GetName_m1577076967 (ZombieCard_t2237653742 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
#define List_1_Add_m1685793073(__this, p0, method) ((  void (*) (List_1_t3319525431 *, String_t*, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.String ZombieCard::GetCardText()
extern "C"  String_t* ZombieCard_GetCardText_m3227767564 (ZombieCard_t2237653742 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String ZombieCard::GetFlavorText()
extern "C"  String_t* ZombieCard_GetFlavorText_m3816104108 (ZombieCard_t2237653742 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZombieCard::IsActionSkiped()
extern "C"  bool ZombieCard_IsActionSkiped_m1580466293 (ZombieCard_t2237653742 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C"  void Animator_SetTrigger_m2134052629 (Animator_t434523843 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C"  int32_t PlayerPrefs_GetInt_m3797620966 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurnPhases/TurnPhaseDelegateDeclare::.ctor(System.Object,System.IntPtr)
extern "C"  void TurnPhaseDelegateDeclare__ctor_m1784330835 (TurnPhaseDelegateDeclare_t3595605765 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZombieTurnPhases::IsDebugLog()
extern "C"  bool ZombieTurnPhases_IsDebugLog_m2641272074 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// GameSession ZombieTurn::GetGameStats()
extern "C"  GameSession_t4087811243 * ZombieTurn_GetGameStats_m2272398959 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Zombies GameSession::GetZombieTypes()
extern "C"  Zombies_t3905046179 * GameSession_GetZombieTypes_m1922938114 (GameSession_t4087811243 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Zombies::set_IsZombieTurn(System.Boolean)
extern "C"  void Zombies_set_IsZombieTurn_m628466427 (Zombies_t3905046179 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Zombies::ResetFightDice()
extern "C"  void Zombies_ResetFightDice_m3648947672 (Zombies_t3905046179 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Zombies::set_IsZombiesCanBeKilled(System.Boolean)
extern "C"  void Zombies_set_IsZombiesCanBeKilled_m3899987018 (Zombies_t3905046179 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurn::SetZombieWinOnTie(System.Boolean)
extern "C"  void ZombieTurn_SetZombieWinOnTie_m1395098020 (ZombieTurn_t3562480803 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurn::ClearFightRoll()
extern "C"  void ZombieTurn_ClearFightRoll_m862839070 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// ZombieDeck ZombieTurn::GetDecks()
extern "C"  ZombieDeck_t3038342722 * ZombieTurn_GetDecks_m242683250 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieDeck::ResetHandCards()
extern "C"  void ZombieDeck_ResetHandCards_m4203603118 (ZombieDeck_t3038342722 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 ZombieTurn::get_Round()
extern "C"  int32_t ZombieTurn_get_Round_m1885665162 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Mission GameSession::GetCurrentMission()
extern "C"  Mission_t4233471175 * GameSession_GetCurrentMission_m3760507839 (GameSession_t4087811243 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurn::set_Round(System.Int32)
extern "C"  void ZombieTurn_set_Round_m3189061632 (ZombieTurn_t3562480803 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurn::UpdateLogRoundDetails()
extern "C"  void ZombieTurn_UpdateLogRoundDetails_m3311090148 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> ZombieTurn::get_CardsPlayed()
extern "C"  List_1_t3319525431 * ZombieTurn_get_CardsPlayed_m21435556 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurnPhases::PhasePlayStartOfTurnCards()
extern "C"  void ZombieTurnPhases_PhasePlayStartOfTurnCards_m2571742252 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Zombie> Zombies::GetZombieTypes()
extern "C"  List_1_t4068628460 * Zombies_GetZombieTypes_m1957530793 (Zombies_t3905046179 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Zombie>::GetEnumerator()
#define List_1_GetEnumerator_m3732267029(__this, method) ((  Enumerator_t1662905041  (*) (List_1_t4068628460 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<Zombie>::get_Current()
#define Enumerator_get_Current_m3182922466(__this, method) ((  Zombie_t2596553718 * (*) (Enumerator_t1662905041 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Void Zombie::SetZombieTurn()
extern "C"  void Zombie_SetZombieTurn_m3221886388 (Zombie_t2596553718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<Zombie>::MoveNext()
#define Enumerator_MoveNext_m360690339(__this, method) ((  bool (*) (Enumerator_t1662905041 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Zombie>::Dispose()
#define Enumerator_Dispose_m894815359(__this, method) ((  void (*) (Enumerator_t1662905041 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::CheckIfInHand(PlayDuringPhase)
extern "C"  List_1_t3709728484 * ZombieDeck_CheckIfInHand_m2488265367 (ZombieDeck_t3038342722 * __this, int32_t ___cardPhase0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurn::set_CardsToPlay(System.Collections.Generic.List`1<ZombieCard>)
extern "C"  void ZombieTurn_set_CardsToPlay_m2073406087 (ZombieTurn_t3562480803 * __this, List_1_t3709728484 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ZombieCard> ZombieTurn::get_CardsToPlay()
extern "C"  List_1_t3709728484 * ZombieTurn_get_CardsToPlay_m1683485823 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<ZombieCard>::get_Count()
#define List_1_get_Count_m1104284998(__this, method) ((  int32_t (*) (List_1_t3709728484 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<ZombieCard>::GetEnumerator()
#define List_1_GetEnumerator_m162813861(__this, method) ((  Enumerator_t1304005065  (*) (List_1_t3709728484 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<ZombieCard>::get_Current()
#define Enumerator_get_Current_m2411096257(__this, method) ((  ZombieCard_t2237653742 * (*) (Enumerator_t1304005065 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Boolean ZombieTurn::PlayCardBasedOnDifficulty()
extern "C"  bool ZombieTurn_PlayCardBasedOnDifficulty_m580813061 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<ZombieCard>::get_Item(System.Int32)
#define List_1_get_Item_m247343563(__this, p0, method) ((  ZombieCard_t2237653742 * (*) (List_1_t3709728484 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieDeck::DoNotPlayCard(ZombieCard)
extern "C"  void ZombieDeck_DoNotPlayCard_m3876639683 (ZombieDeck_t3038342722 * __this, ZombieCard_t2237653742 * ___card0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurn::set_CardBeingPlayed(ZombieCard)
extern "C"  void ZombieTurn_set_CardBeingPlayed_m480097926 (ZombieTurn_t3562480803 * __this, ZombieCard_t2237653742 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurn::StartPlayCard()
extern "C"  void ZombieTurn_StartPlayCard_m4151681992 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<ZombieCard>::MoveNext()
#define Enumerator_MoveNext_m4044029282(__this, method) ((  bool (*) (Enumerator_t1304005065 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ZombieCard>::Dispose()
#define Enumerator_Dispose_m3650518843(__this, method) ((  void (*) (Enumerator_t1304005065 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.Void ZombieTurnPhases::PhaseDrawNewCards()
extern "C"  void ZombieTurnPhases_PhaseDrawNewCards_m411032853 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::GetHand()
extern "C"  List_1_t3709728484 * ZombieDeck_GetHand_m514179710 (ZombieDeck_t3038342722 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieDeck::DiscardACard(ZombieCard)
extern "C"  void ZombieDeck_DiscardACard_m164680412 (ZombieDeck_t3038342722 * __this, ZombieCard_t2237653742 * ___card0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieDeck::DrawCard()
extern "C"  void ZombieDeck_DrawCard_m1910821610 (ZombieDeck_t3038342722 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurnPhases::PhasePlayImmediatelyCards()
extern "C"  void ZombieTurnPhases_PhasePlayImmediatelyCards_m457453206 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern "C"  String_t* PlayerPrefs_GetString_m389913383 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurnPhases::PhaseCheckIfNewZombiesSpawn()
extern "C"  void ZombieTurnPhases_PhaseCheckIfNewZombiesSpawn_m1652064919 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zombies::GetZombiesInPool()
extern "C"  int32_t Zombies_GetZombiesInPool_m542816421 (Zombies_t3905046179 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<Mission/SpecialRulesTypes>::Contains(!0)
#define List_1_Contains_m915432422(__this, p0, method) ((  bool (*) (List_1_t3747392069 *, SpecialRulesTypes_t2275317327 *, const RuntimeMethod*))List_1_Contains_m2654125393_gshared)(__this, p0, method)
// System.Void ZombieTurn::set_Spawn(System.Boolean)
extern "C"  void ZombieTurn_set_Spawn_m1222009344 (ZombieTurn_t3562480803 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Actions::RollD6()
extern "C"  int32_t Actions_RollD6_m698542841 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zombies::SpawnCheck()
extern "C"  int32_t Zombies_SpawnCheck_m3840534542 (Zombies_t3905046179 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Zombie>::get_Count()
#define List_1_get_Count_m3173494979(__this, method) ((  int32_t (*) (List_1_t4068628460 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void ZombieTurnPhases::PhaseMoveZombies()
extern "C"  void ZombieTurnPhases_PhaseMoveZombies_m3650847429 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<Zombie>::get_Item(System.Int32)
#define List_1_get_Item_m3871794941(__this, p0, method) ((  Zombie_t2596553718 * (*) (List_1_t4068628460 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// Zombie Zombies::GetZombieOfType(ZombieType)
extern "C"  Zombie_t2596553718 * Zombies_GetZombieOfType_m2489544116 (Zombies_t3905046179 * __this, int32_t ___zombieType0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Zombie::GetZombieName()
extern "C"  String_t* Zombie_GetZombieName_m3563533677 (Zombie_t2596553718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String ZombieTurn::GetMovementText(Zombie)
extern "C"  String_t* ZombieTurn_GetMovementText_m4033264826 (ZombieTurn_t3562480803 * __this, Zombie_t2596553718 * ___zombieMoving0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Zombie::SetRandomMovementTarget()
extern "C"  void Zombie_SetRandomMovementTarget_m1696563136 (Zombie_t2596553718 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<Mission/ObjectiveTypes>::Contains(!0)
#define List_1_Contains_m92166693(__this, p0, method) ((  bool (*) (List_1_t4188946901 *, ObjectiveTypes_t2716872159 *, const RuntimeMethod*))List_1_Contains_m2654125393_gshared)(__this, p0, method)
// System.Void UnityEngine.UI.Dropdown::ClearOptions()
extern "C"  void Dropdown_ClearOptions_m4085591601 (Dropdown_t2274391225 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
#define List_1__ctor_m706204246(__this, method) ((  void (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// Tiles GameSession::GetMapTiles()
extern "C"  Tiles_t1457048987 * GameSession_GetMapTiles_m3864703078 (GameSession_t4087811243 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Tiles::GetBuildingToDestroyPriority()
extern "C"  List_1_t3319525431 * Tiles_GetBuildingToDestroyPriority_m3399415659 (Tiles_t1457048987 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m3621602103(__this, p0, method) ((  void (*) (List_1_t3319525431 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m3709462088_gshared)(__this, p0, method)
// System.Void UnityEngine.UI.Dropdown::AddOptions(System.Collections.Generic.List`1<System.String>)
extern "C"  void Dropdown_AddOptions_m2776940360 (Dropdown_t2274391225 * __this, List_1_t3319525431 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurn::SetFightInfoText()
extern "C"  void ZombieTurn_SetFightInfoText_m3143307073 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ZombieTurnPhases/TurnPhaseDelegateDeclare::Invoke()
extern "C"  void TurnPhaseDelegateDeclare_Invoke_m1688307234 (TurnPhaseDelegateDeclare_t3595605765 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZombieTurn::get_Spawn()
extern "C"  bool ZombieTurn_get_Spawn_m2604868717 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ZombieTurn::SpawnZombies()
extern "C"  Int32U5BU5D_t385246372* ZombieTurn_SpawnZombies_m917606607 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m1715369213 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m2971454694 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZombieTurn::IsZombieMovesAfterSpawn()
extern "C"  bool ZombieTurn_IsZombieMovesAfterSpawn_m2315172336 (ZombieTurn_t3562480803 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Zombies::ResetExtraRoundDice()
extern "C"  void Zombies_ResetExtraRoundDice_m1135819780 (Zombies_t3905046179 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TextAnimation::Play(System.String)
extern "C"  void TextAnimation_Play_m3501269388 (TextAnimation_t2694049294 * __this, String_t* ___textToAnimate0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m215368492 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZombieTurn/<FightRoll>c__AnonStorey1::.ctor()
extern "C"  void U3CFightRollU3Ec__AnonStorey1__ctor_m2584342623 (U3CFightRollU3Ec__AnonStorey1_t3184696250 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZombieTurn/<FightRoll>c__AnonStorey1::<>m__0()
extern "C"  void U3CFightRollU3Ec__AnonStorey1_U3CU3Em__0_m2454650522 (U3CFightRollU3Ec__AnonStorey1_t3184696250 * __this, const RuntimeMethod* method)
{
	{
		ZombieTurn_t3562480803 * L_0 = __this->get_U24this_1();
		int32_t L_1 = __this->get_index_0();
		NullCheck(L_0);
		ZombieTurn_ReRollFightDie_m203485653(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZombieTurn/<PlayCard>c__Iterator0::.ctor()
extern "C"  void U3CPlayCardU3Ec__Iterator0__ctor_m187439821 (U3CPlayCardU3Ec__Iterator0_t1992789720 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ZombieTurn/<PlayCard>c__Iterator0::MoveNext()
extern "C"  bool U3CPlayCardU3Ec__Iterator0_MoveNext_m3498982220 (U3CPlayCardU3Ec__Iterator0_t1992789720 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayCardU3Ec__Iterator0_MoveNext_m3498982220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	AnimatorStateInfo_t509032636  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0057;
			}
		}
	}
	{
		goto IL_017f;
	}

IL_0021:
	{
		ZombieTurn_t3562480803 * L_2 = __this->get_U24this_1();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = L_2->get_Card_24();
		NullCheck(L_3);
		Animator_t434523843 * L_4 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_3, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		__this->set_U3CcardAnimatorU3E__0_0(L_4);
		goto IL_0057;
	}

IL_003c:
	{
		__this->set_U24current_2(NULL);
		bool L_5 = __this->get_U24disposing_3();
		if (L_5)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0052:
	{
		goto IL_0181;
	}

IL_0057:
	{
		Animator_t434523843 * L_6 = __this->get_U3CcardAnimatorU3E__0_0();
		NullCheck(L_6);
		AnimatorStateInfo_t509032636  L_7 = Animator_GetCurrentAnimatorStateInfo_m18694920(L_6, 0, /*hidden argument*/NULL);
		V_1 = L_7;
		bool L_8 = AnimatorStateInfo_IsName_m3393819976((AnimatorStateInfo_t509032636 *)(&V_1), _stringLiteral2432531900, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003c;
		}
	}
	{
		ZombieTurn_t3562480803 * L_9 = __this->get_U24this_1();
		NullCheck(L_9);
		bool L_10 = L_9->get_isDebugLog_68();
		if (!L_10)
		{
			goto IL_00a4;
		}
	}
	{
		ZombieTurn_t3562480803 * L_11 = __this->get_U24this_1();
		NullCheck(L_11);
		ZombieCard_t2237653742 * L_12 = L_11->get_cardBeingPlayed_12();
		NullCheck(L_12);
		String_t* L_13 = ZombieCard_GetName_m1577076967(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1495087245, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		ZombieTurn_t3562480803 * L_15 = __this->get_U24this_1();
		NullCheck(L_15);
		List_1_t3319525431 * L_16 = L_15->get_cardsPlayed_19();
		ZombieTurn_t3562480803 * L_17 = __this->get_U24this_1();
		NullCheck(L_17);
		ZombieCard_t2237653742 * L_18 = L_17->get_cardBeingPlayed_12();
		NullCheck(L_18);
		String_t* L_19 = ZombieCard_GetName_m1577076967(L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		List_1_Add_m1685793073(L_16, L_19, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		ZombieTurn_t3562480803 * L_20 = __this->get_U24this_1();
		NullCheck(L_20);
		List_1_t3319525431 * L_21 = L_20->get_cardsPlayed_19();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_21);
		List_1_Add_m1685793073(L_21, L_22, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		ZombieTurn_t3562480803 * L_23 = __this->get_U24this_1();
		NullCheck(L_23);
		ZombieCard_t2237653742 * L_24 = L_23->get_cardBeingPlayed_12();
		NullCheck(L_24);
		VirtActionInvoker0::Invoke(4 /* System.Void ZombieCard::CardSetup() */, L_24);
		ZombieTurn_t3562480803 * L_25 = __this->get_U24this_1();
		NullCheck(L_25);
		Text_t1901882714 * L_26 = L_25->get_CardTitle_25();
		ZombieTurn_t3562480803 * L_27 = __this->get_U24this_1();
		NullCheck(L_27);
		ZombieCard_t2237653742 * L_28 = L_27->get_cardBeingPlayed_12();
		NullCheck(L_28);
		String_t* L_29 = ZombieCard_GetName_m1577076967(L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_26, L_29);
		ZombieTurn_t3562480803 * L_30 = __this->get_U24this_1();
		NullCheck(L_30);
		Text_t1901882714 * L_31 = L_30->get_CardText_26();
		ZombieTurn_t3562480803 * L_32 = __this->get_U24this_1();
		NullCheck(L_32);
		ZombieCard_t2237653742 * L_33 = L_32->get_cardBeingPlayed_12();
		NullCheck(L_33);
		String_t* L_34 = ZombieCard_GetCardText_m3227767564(L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_31, L_34);
		ZombieTurn_t3562480803 * L_35 = __this->get_U24this_1();
		NullCheck(L_35);
		Text_t1901882714 * L_36 = L_35->get_CardFlavor_27();
		ZombieTurn_t3562480803 * L_37 = __this->get_U24this_1();
		NullCheck(L_37);
		ZombieCard_t2237653742 * L_38 = L_37->get_cardBeingPlayed_12();
		NullCheck(L_38);
		String_t* L_39 = ZombieCard_GetFlavorText_m3816104108(L_38, /*hidden argument*/NULL);
		NullCheck(L_36);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_36, L_39);
		ZombieTurn_t3562480803 * L_40 = __this->get_U24this_1();
		NullCheck(L_40);
		ZombieCard_t2237653742 * L_41 = L_40->get_cardBeingPlayed_12();
		NullCheck(L_41);
		bool L_42 = ZombieCard_IsActionSkiped_m1580466293(L_41, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_0178;
		}
	}
	{
		ZombieTurn_t3562480803 * L_43 = __this->get_U24this_1();
		NullCheck(L_43);
		GameObject_t1113636619 * L_44 = L_43->get_Card_24();
		NullCheck(L_44);
		Animator_t434523843 * L_45 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_44, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_45);
		Animator_SetTrigger_m2134052629(L_45, _stringLiteral3905732946, /*hidden argument*/NULL);
	}

IL_0178:
	{
		__this->set_U24PC_4((-1));
	}

IL_017f:
	{
		return (bool)0;
	}

IL_0181:
	{
		return (bool)1;
	}
}
// System.Object ZombieTurn/<PlayCard>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CPlayCardU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3591602327 (U3CPlayCardU3Ec__Iterator0_t1992789720 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object ZombieTurn/<PlayCard>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CPlayCardU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2116636735 (U3CPlayCardU3Ec__Iterator0_t1992789720 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void ZombieTurn/<PlayCard>c__Iterator0::Dispose()
extern "C"  void U3CPlayCardU3Ec__Iterator0_Dispose_m1493986830 (U3CPlayCardU3Ec__Iterator0_t1992789720 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void ZombieTurn/<PlayCard>c__Iterator0::Reset()
extern "C"  void U3CPlayCardU3Ec__Iterator0_Reset_m1814800537 (U3CPlayCardU3Ec__Iterator0_t1992789720 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPlayCardU3Ec__Iterator0_Reset_m1814800537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CPlayCardU3Ec__Iterator0_Reset_m1814800537_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZombieTurnPhases::.ctor()
extern "C"  void ZombieTurnPhases__ctor_m1244733424 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ZombieTurnPhases::IsDebugLog()
extern "C"  bool ZombieTurnPhases_IsDebugLog_m2641272074 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_IsDebugLog_m2641272074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m3797620966(NULL /*static, unused*/, _stringLiteral1478849746, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Void ZombieTurnPhases::SetZombieTurn(ZombieTurn)
extern "C"  void ZombieTurnPhases_SetZombieTurn_m1087060096 (ZombieTurnPhases_t3127322021 * __this, ZombieTurn_t3562480803 * ___zt0, const RuntimeMethod* method)
{
	{
		ZombieTurn_t3562480803 * L_0 = ___zt0;
		__this->set_zombieTurn_1(L_0);
		return;
	}
}
// System.Void ZombieTurnPhases::TurnDelegateStartOfTurn()
extern "C"  void ZombieTurnPhases_TurnDelegateStartOfTurn_m2008126874 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_TurnDelegateStartOfTurn_m2008126874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = (intptr_t)ZombieTurnPhases_PhaseStartOfTurn_m1047464964_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_1 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_1, __this, L_0, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_1);
		return;
	}
}
// System.Void ZombieTurnPhases::SetZombieTurnPhase(ZombieTurnPhaseName)
extern "C"  void ZombieTurnPhases_SetZombieTurnPhase_m1136649717 (ZombieTurnPhases_t3127322021 * __this, int32_t ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_SetZombieTurnPhase_m1136649717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___name0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0037;
			}
			case 1:
			{
				goto IL_004e;
			}
			case 2:
			{
				goto IL_0065;
			}
			case 3:
			{
				goto IL_007c;
			}
			case 4:
			{
				goto IL_0093;
			}
			case 5:
			{
				goto IL_00aa;
			}
			case 6:
			{
				goto IL_00c1;
			}
			case 7:
			{
				goto IL_00d8;
			}
			case 8:
			{
				goto IL_00ef;
			}
			case 9:
			{
				goto IL_0106;
			}
			case 10:
			{
				goto IL_011d;
			}
		}
	}
	{
		goto IL_0134;
	}

IL_0037:
	{
		intptr_t L_1 = (intptr_t)ZombieTurnPhases_PhaseStartOfTurn_m1047464964_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_2 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_2);
		goto IL_014b;
	}

IL_004e:
	{
		intptr_t L_3 = (intptr_t)ZombieTurnPhases_PhasePlayStartOfTurnCards_m2571742252_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_4 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_4, __this, L_3, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_4);
		goto IL_014b;
	}

IL_0065:
	{
		intptr_t L_5 = (intptr_t)ZombieTurnPhases_PhaseDrawNewCards_m411032853_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_6 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_6, __this, L_5, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_6);
		goto IL_014b;
	}

IL_007c:
	{
		intptr_t L_7 = (intptr_t)ZombieTurnPhases_PhasePlayImmediatelyCards_m457453206_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_8 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_8, __this, L_7, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_8);
		goto IL_014b;
	}

IL_0093:
	{
		intptr_t L_9 = (intptr_t)ZombieTurnPhases_PhaseCheckIfNewZombiesSpawn_m1652064919_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_10 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_10, __this, L_9, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_10);
		goto IL_014b;
	}

IL_00aa:
	{
		intptr_t L_11 = (intptr_t)ZombieTurnPhases_PhaseMoveZombies_m3650847429_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_12 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_12, __this, L_11, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_12);
		goto IL_014b;
	}

IL_00c1:
	{
		intptr_t L_13 = (intptr_t)ZombieTurnPhases_PhaseFightHeroes_m3968111430_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_14 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_14, __this, L_13, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_14);
		goto IL_014b;
	}

IL_00d8:
	{
		intptr_t L_15 = (intptr_t)ZombieTurnPhases_PhaseFightResults_m2355902853_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_16 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_16, __this, L_15, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_16);
		goto IL_014b;
	}

IL_00ef:
	{
		intptr_t L_17 = (intptr_t)ZombieTurnPhases_PhaseSpawnNewZombies_m3815212178_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_18 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_18, __this, L_17, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_18);
		goto IL_014b;
	}

IL_0106:
	{
		intptr_t L_19 = (intptr_t)ZombieTurnPhases_PhasePlayEndOfTurnCards_m2517377176_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_20 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_20, __this, L_19, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_20);
		goto IL_014b;
	}

IL_011d:
	{
		intptr_t L_21 = (intptr_t)ZombieTurnPhases_PhaseHeroesTurn_m1774508636_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_22 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_22, __this, L_21, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_22);
		goto IL_014b;
	}

IL_0134:
	{
		intptr_t L_23 = (intptr_t)ZombieTurnPhases_PhaseHeroesTurn_m1774508636_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_24 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_24, __this, L_23, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_24);
		goto IL_014b;
	}

IL_014b:
	{
		return;
	}
}
// System.Void ZombieTurnPhases::PhaseStartOfTurn()
extern "C"  void ZombieTurnPhases_PhaseStartOfTurn_m1047464964 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_PhaseStartOfTurn_m1047464964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3660277191, /*hidden argument*/NULL);
	}

IL_0015:
	{
		__this->set_PhaseName_3(0);
		intptr_t L_1 = (intptr_t)ZombieTurnPhases_PhaseStartOfTurn_m1047464964_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_2 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_2);
		ZombieTurn_t3562480803 * L_3 = __this->get_zombieTurn_1();
		NullCheck(L_3);
		GameSession_t4087811243 * L_4 = ZombieTurn_GetGameStats_m2272398959(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Zombies_t3905046179 * L_5 = GameSession_GetZombieTypes_m1922938114(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Zombies_set_IsZombieTurn_m628466427(L_5, (bool)1, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_6 = __this->get_zombieTurn_1();
		NullCheck(L_6);
		GameSession_t4087811243 * L_7 = ZombieTurn_GetGameStats_m2272398959(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Zombies_t3905046179 * L_8 = GameSession_GetZombieTypes_m1922938114(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Zombies_ResetFightDice_m3648947672(L_8, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_9 = __this->get_zombieTurn_1();
		NullCheck(L_9);
		GameSession_t4087811243 * L_10 = ZombieTurn_GetGameStats_m2272398959(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Zombies_t3905046179 * L_11 = GameSession_GetZombieTypes_m1922938114(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Zombies_set_IsZombiesCanBeKilled_m3899987018(L_11, (bool)1, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_12 = __this->get_zombieTurn_1();
		NullCheck(L_12);
		ZombieTurn_SetZombieWinOnTie_m1395098020(L_12, (bool)1, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_13 = __this->get_zombieTurn_1();
		NullCheck(L_13);
		ZombieTurn_ClearFightRoll_m862839070(L_13, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_14 = __this->get_zombieTurn_1();
		NullCheck(L_14);
		ZombieDeck_t3038342722 * L_15 = ZombieTurn_GetDecks_m242683250(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		ZombieDeck_ResetHandCards_m4203603118(L_15, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_16 = __this->get_zombieTurn_1();
		NullCheck(L_16);
		int32_t L_17 = ZombieTurn_get_Round_m1885665162(L_16, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)-10)))))
		{
			goto IL_00cd;
		}
	}
	{
		ZombieTurn_t3562480803 * L_18 = __this->get_zombieTurn_1();
		ZombieTurn_t3562480803 * L_19 = __this->get_zombieTurn_1();
		NullCheck(L_19);
		GameSession_t4087811243 * L_20 = ZombieTurn_GetGameStats_m2272398959(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Mission_t4233471175 * L_21 = GameSession_GetCurrentMission_m3760507839(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = L_21->get_Rounds_28();
		NullCheck(L_18);
		ZombieTurn_set_Round_m3189061632(L_18, L_22, /*hidden argument*/NULL);
		goto IL_00e0;
	}

IL_00cd:
	{
		ZombieTurn_t3562480803 * L_23 = __this->get_zombieTurn_1();
		ZombieTurn_t3562480803 * L_24 = L_23;
		NullCheck(L_24);
		int32_t L_25 = ZombieTurn_get_Round_m1885665162(L_24, /*hidden argument*/NULL);
		NullCheck(L_24);
		ZombieTurn_set_Round_m3189061632(L_24, ((int32_t)il2cpp_codegen_subtract((int32_t)L_25, (int32_t)1)), /*hidden argument*/NULL);
	}

IL_00e0:
	{
		ZombieTurn_t3562480803 * L_26 = __this->get_zombieTurn_1();
		NullCheck(L_26);
		GameLog_t2697247111 * L_27 = L_26->get_GameLog_47();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_28 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_27, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0101;
		}
	}
	{
		ZombieTurn_t3562480803 * L_29 = __this->get_zombieTurn_1();
		NullCheck(L_29);
		ZombieTurn_UpdateLogRoundDetails_m3311090148(L_29, /*hidden argument*/NULL);
	}

IL_0101:
	{
		ZombieTurn_t3562480803 * L_30 = __this->get_zombieTurn_1();
		NullCheck(L_30);
		List_1_t3319525431 * L_31 = ZombieTurn_get_CardsPlayed_m21435556(L_30, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_32 = __this->get_zombieTurn_1();
		NullCheck(L_32);
		int32_t L_33 = ZombieTurn_get_Round_m1885665162(L_32, /*hidden argument*/NULL);
		int32_t L_34 = L_33;
		RuntimeObject * L_35 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2878338918, L_35, /*hidden argument*/NULL);
		NullCheck(L_31);
		List_1_Add_m1685793073(L_31, L_36, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		ZombieTurnPhases_PhasePlayStartOfTurnCards_m2571742252(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZombieTurnPhases::PhasePlayStartOfTurnCards()
extern "C"  void ZombieTurnPhases_PhasePlayStartOfTurnCards_m2571742252 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_PhasePlayStartOfTurnCards_m2571742252_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Zombie_t2596553718 * V_0 = NULL;
	Enumerator_t1662905041  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ZombieCard_t2237653742 * V_2 = NULL;
	Enumerator_t1304005065  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral4289868831, /*hidden argument*/NULL);
	}

IL_0015:
	{
		__this->set_PhaseName_3(1);
		intptr_t L_1 = (intptr_t)ZombieTurnPhases_PhasePlayStartOfTurnCards_m2571742252_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_2 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_2);
		ZombieTurn_t3562480803 * L_3 = __this->get_zombieTurn_1();
		NullCheck(L_3);
		GameSession_t4087811243 * L_4 = ZombieTurn_GetGameStats_m2272398959(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Zombies_t3905046179 * L_5 = GameSession_GetZombieTypes_m1922938114(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		List_1_t4068628460 * L_6 = Zombies_GetZombieTypes_m1957530793(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Enumerator_t1662905041  L_7 = List_1_GetEnumerator_m3732267029(L_6, /*hidden argument*/List_1_GetEnumerator_m3732267029_RuntimeMethod_var);
		V_1 = L_7;
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005c;
		}

IL_004e:
		{
			Zombie_t2596553718 * L_8 = Enumerator_get_Current_m3182922466((Enumerator_t1662905041 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m3182922466_RuntimeMethod_var);
			V_0 = L_8;
			Zombie_t2596553718 * L_9 = V_0;
			NullCheck(L_9);
			Zombie_SetZombieTurn_m3221886388(L_9, /*hidden argument*/NULL);
		}

IL_005c:
		{
			bool L_10 = Enumerator_MoveNext_m360690339((Enumerator_t1662905041 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m360690339_RuntimeMethod_var);
			if (L_10)
			{
				goto IL_004e;
			}
		}

IL_0068:
		{
			IL2CPP_LEAVE(0x7B, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m894815359((Enumerator_t1662905041 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m894815359_RuntimeMethod_var);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_JUMP_TBL(0x7B, IL_007b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_007b:
	{
		ZombieTurn_t3562480803 * L_11 = __this->get_zombieTurn_1();
		ZombieTurn_t3562480803 * L_12 = __this->get_zombieTurn_1();
		NullCheck(L_12);
		ZombieDeck_t3038342722 * L_13 = ZombieTurn_GetDecks_m242683250(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		List_1_t3709728484 * L_14 = ZombieDeck_CheckIfInHand_m2488265367(L_13, 0, /*hidden argument*/NULL);
		NullCheck(L_11);
		ZombieTurn_set_CardsToPlay_m2073406087(L_11, L_14, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_15 = __this->get_zombieTurn_1();
		NullCheck(L_15);
		List_1_t3709728484 * L_16 = ZombieTurn_get_CardsToPlay_m1683485823(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = List_1_get_Count_m1104284998(L_16, /*hidden argument*/List_1_get_Count_m1104284998_RuntimeMethod_var);
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_01bc;
		}
	}
	{
		ZombieTurn_t3562480803 * L_18 = __this->get_zombieTurn_1();
		NullCheck(L_18);
		List_1_t3709728484 * L_19 = ZombieTurn_get_CardsToPlay_m1683485823(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Enumerator_t1304005065  L_20 = List_1_GetEnumerator_m162813861(L_19, /*hidden argument*/List_1_GetEnumerator_m162813861_RuntimeMethod_var);
		V_3 = L_20;
	}

IL_00be:
	try
	{ // begin try (depth: 1)
		{
			goto IL_019d;
		}

IL_00c3:
		{
			ZombieCard_t2237653742 * L_21 = Enumerator_get_Current_m2411096257((Enumerator_t1304005065 *)(&V_3), /*hidden argument*/Enumerator_get_Current_m2411096257_RuntimeMethod_var);
			V_2 = L_21;
			ZombieTurn_t3562480803 * L_22 = __this->get_zombieTurn_1();
			NullCheck(L_22);
			bool L_23 = ZombieTurn_PlayCardBasedOnDifficulty_m580813061(L_22, /*hidden argument*/NULL);
			if (L_23)
			{
				goto IL_0161;
			}
		}

IL_00db:
		{
			bool L_24 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
			if (!L_24)
			{
				goto IL_0110;
			}
		}

IL_00e6:
		{
			ZombieTurn_t3562480803 * L_25 = __this->get_zombieTurn_1();
			NullCheck(L_25);
			List_1_t3709728484 * L_26 = ZombieTurn_get_CardsToPlay_m1683485823(L_25, /*hidden argument*/NULL);
			NullCheck(L_26);
			ZombieCard_t2237653742 * L_27 = List_1_get_Item_m247343563(L_26, 0, /*hidden argument*/List_1_get_Item_m247343563_RuntimeMethod_var);
			NullCheck(L_27);
			String_t* L_28 = ZombieCard_GetName_m1577076967(L_27, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_29 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2089403394, L_28, _stringLiteral2642543365, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		}

IL_0110:
		{
			ZombieTurn_t3562480803 * L_30 = __this->get_zombieTurn_1();
			NullCheck(L_30);
			List_1_t3319525431 * L_31 = ZombieTurn_get_CardsPlayed_m21435556(L_30, /*hidden argument*/NULL);
			ZombieTurn_t3562480803 * L_32 = __this->get_zombieTurn_1();
			NullCheck(L_32);
			List_1_t3709728484 * L_33 = ZombieTurn_get_CardsToPlay_m1683485823(L_32, /*hidden argument*/NULL);
			NullCheck(L_33);
			ZombieCard_t2237653742 * L_34 = List_1_get_Item_m247343563(L_33, 0, /*hidden argument*/List_1_get_Item_m247343563_RuntimeMethod_var);
			NullCheck(L_34);
			String_t* L_35 = ZombieCard_GetName_m1577076967(L_34, /*hidden argument*/NULL);
			NullCheck(L_31);
			List_1_Add_m1685793073(L_31, L_35, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
			ZombieTurn_t3562480803 * L_36 = __this->get_zombieTurn_1();
			NullCheck(L_36);
			List_1_t3319525431 * L_37 = ZombieTurn_get_CardsPlayed_m21435556(L_36, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_38 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
			NullCheck(L_37);
			List_1_Add_m1685793073(L_37, L_38, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
			ZombieTurn_t3562480803 * L_39 = __this->get_zombieTurn_1();
			NullCheck(L_39);
			ZombieDeck_t3038342722 * L_40 = ZombieTurn_GetDecks_m242683250(L_39, /*hidden argument*/NULL);
			ZombieCard_t2237653742 * L_41 = V_2;
			NullCheck(L_40);
			ZombieDeck_DoNotPlayCard_m3876639683(L_40, L_41, /*hidden argument*/NULL);
			goto IL_019d;
		}

IL_0161:
		{
			bool L_42 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
			if (!L_42)
			{
				goto IL_0181;
			}
		}

IL_016c:
		{
			ZombieCard_t2237653742 * L_43 = V_2;
			NullCheck(L_43);
			String_t* L_44 = ZombieCard_GetName_m1577076967(L_43, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_45 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1920273779, L_44, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		}

IL_0181:
		{
			ZombieTurn_t3562480803 * L_46 = __this->get_zombieTurn_1();
			ZombieCard_t2237653742 * L_47 = V_2;
			NullCheck(L_46);
			ZombieTurn_set_CardBeingPlayed_m480097926(L_46, L_47, /*hidden argument*/NULL);
			ZombieTurn_t3562480803 * L_48 = __this->get_zombieTurn_1();
			NullCheck(L_48);
			ZombieTurn_StartPlayCard_m4151681992(L_48, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x1C2, FINALLY_01ae);
		}

IL_019d:
		{
			bool L_49 = Enumerator_MoveNext_m4044029282((Enumerator_t1304005065 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m4044029282_RuntimeMethod_var);
			if (L_49)
			{
				goto IL_00c3;
			}
		}

IL_01a9:
		{
			IL2CPP_LEAVE(0x1BC, FINALLY_01ae);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01ae;
	}

FINALLY_01ae:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3650518843((Enumerator_t1304005065 *)(&V_3), /*hidden argument*/Enumerator_Dispose_m3650518843_RuntimeMethod_var);
		IL2CPP_END_FINALLY(430)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(430)
	{
		IL2CPP_JUMP_TBL(0x1C2, IL_01c2)
		IL2CPP_JUMP_TBL(0x1BC, IL_01bc)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_01bc:
	{
		ZombieTurnPhases_PhaseDrawNewCards_m411032853(__this, /*hidden argument*/NULL);
	}

IL_01c2:
	{
		return;
	}
}
// System.Void ZombieTurnPhases::PhaseDrawNewCards()
extern "C"  void ZombieTurnPhases_PhaseDrawNewCards_m411032853 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_PhaseDrawNewCards_m411032853_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		bool L_0 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2226740637, /*hidden argument*/NULL);
	}

IL_0015:
	{
		__this->set_PhaseName_3(2);
		V_0 = (bool)0;
		goto IL_004b;
	}

IL_0023:
	{
		ZombieTurn_t3562480803 * L_1 = __this->get_zombieTurn_1();
		NullCheck(L_1);
		ZombieDeck_t3038342722 * L_2 = ZombieTurn_GetDecks_m242683250(L_1, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_3 = __this->get_zombieTurn_1();
		NullCheck(L_3);
		ZombieDeck_t3038342722 * L_4 = ZombieTurn_GetDecks_m242683250(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		List_1_t3709728484 * L_5 = ZombieDeck_GetHand_m514179710(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		ZombieCard_t2237653742 * L_6 = List_1_get_Item_m247343563(L_5, 0, /*hidden argument*/List_1_get_Item_m247343563_RuntimeMethod_var);
		NullCheck(L_2);
		ZombieDeck_DiscardACard_m164680412(L_2, L_6, /*hidden argument*/NULL);
		V_0 = (bool)1;
	}

IL_004b:
	{
		ZombieTurn_t3562480803 * L_7 = __this->get_zombieTurn_1();
		NullCheck(L_7);
		ZombieDeck_t3038342722 * L_8 = ZombieTurn_GetDecks_m242683250(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		List_1_t3709728484 * L_9 = ZombieDeck_GetHand_m514179710(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m1104284998(L_9, /*hidden argument*/List_1_get_Count_m1104284998_RuntimeMethod_var);
		if ((((int32_t)L_10) > ((int32_t)4)))
		{
			goto IL_0023;
		}
	}
	{
		bool L_11 = V_0;
		if (L_11)
		{
			goto IL_00ad;
		}
	}
	{
		ZombieTurn_t3562480803 * L_12 = __this->get_zombieTurn_1();
		NullCheck(L_12);
		ZombieDeck_t3038342722 * L_13 = ZombieTurn_GetDecks_m242683250(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		List_1_t3709728484 * L_14 = ZombieDeck_GetHand_m514179710(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = List_1_get_Count_m1104284998(L_14, /*hidden argument*/List_1_get_Count_m1104284998_RuntimeMethod_var);
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_00ad;
		}
	}
	{
		ZombieTurn_t3562480803 * L_16 = __this->get_zombieTurn_1();
		NullCheck(L_16);
		ZombieDeck_t3038342722 * L_17 = ZombieTurn_GetDecks_m242683250(L_16, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_18 = __this->get_zombieTurn_1();
		NullCheck(L_18);
		ZombieDeck_t3038342722 * L_19 = ZombieTurn_GetDecks_m242683250(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		List_1_t3709728484 * L_20 = ZombieDeck_GetHand_m514179710(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		ZombieCard_t2237653742 * L_21 = List_1_get_Item_m247343563(L_20, 0, /*hidden argument*/List_1_get_Item_m247343563_RuntimeMethod_var);
		NullCheck(L_17);
		ZombieDeck_DiscardACard_m164680412(L_17, L_21, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		V_1 = 0;
		goto IL_00c4;
	}

IL_00b4:
	{
		ZombieTurn_t3562480803 * L_22 = __this->get_zombieTurn_1();
		NullCheck(L_22);
		ZombieDeck_t3038342722 * L_23 = ZombieTurn_GetDecks_m242683250(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		ZombieDeck_DrawCard_m1910821610(L_23, /*hidden argument*/NULL);
	}

IL_00c4:
	{
		ZombieTurn_t3562480803 * L_24 = __this->get_zombieTurn_1();
		NullCheck(L_24);
		ZombieDeck_t3038342722 * L_25 = ZombieTurn_GetDecks_m242683250(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		List_1_t3709728484 * L_26 = ZombieDeck_GetHand_m514179710(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		int32_t L_27 = List_1_get_Count_m1104284998(L_26, /*hidden argument*/List_1_get_Count_m1104284998_RuntimeMethod_var);
		if ((((int32_t)L_27) >= ((int32_t)4)))
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_28 = V_1;
		int32_t L_29 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1));
		V_1 = L_29;
		if ((((int32_t)L_29) < ((int32_t)5)))
		{
			goto IL_00b4;
		}
	}

IL_00ea:
	{
		ZombieTurnPhases_PhasePlayImmediatelyCards_m457453206(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZombieTurnPhases::PhasePlayImmediatelyCards()
extern "C"  void ZombieTurnPhases_PhasePlayImmediatelyCards_m457453206 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_PhasePlayImmediatelyCards_m457453206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ZombieCard_t2237653742 * V_0 = NULL;
	Enumerator_t1304005065  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3747081117, /*hidden argument*/NULL);
	}

IL_0015:
	{
		__this->set_PhaseName_3(3);
		String_t* L_1 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral1014326624, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, _stringLiteral1271060643, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0064;
		}
	}
	{
		ZombieTurn_t3562480803 * L_3 = __this->get_zombieTurn_1();
		NullCheck(L_3);
		Text_t1901882714 * L_4 = L_3->get_TutorialText_61();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteral2778673522);
		ZombieTurn_t3562480803 * L_5 = __this->get_zombieTurn_1();
		NullCheck(L_5);
		GameObject_t1113636619 * L_6 = L_5->get_TutorialPanel_60();
		NullCheck(L_6);
		Animator_t434523843 * L_7 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_6, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_7);
		Animator_SetTrigger_m2134052629(L_7, _stringLiteral3905732946, /*hidden argument*/NULL);
	}

IL_0064:
	{
		intptr_t L_8 = (intptr_t)ZombieTurnPhases_PhasePlayImmediatelyCards_m457453206_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_9 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_9, __this, L_8, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_9);
		ZombieTurn_t3562480803 * L_10 = __this->get_zombieTurn_1();
		ZombieTurn_t3562480803 * L_11 = __this->get_zombieTurn_1();
		NullCheck(L_11);
		ZombieDeck_t3038342722 * L_12 = ZombieTurn_GetDecks_m242683250(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		List_1_t3709728484 * L_13 = ZombieDeck_CheckIfInHand_m2488265367(L_12, 1, /*hidden argument*/NULL);
		NullCheck(L_10);
		ZombieTurn_set_CardsToPlay_m2073406087(L_10, L_13, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_14 = __this->get_zombieTurn_1();
		NullCheck(L_14);
		List_1_t3709728484 * L_15 = ZombieTurn_get_CardsToPlay_m1683485823(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m1104284998(L_15, /*hidden argument*/List_1_get_Count_m1104284998_RuntimeMethod_var);
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_0121;
		}
	}
	{
		ZombieTurn_t3562480803 * L_17 = __this->get_zombieTurn_1();
		NullCheck(L_17);
		List_1_t3709728484 * L_18 = ZombieTurn_get_CardsToPlay_m1683485823(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Enumerator_t1304005065  L_19 = List_1_GetEnumerator_m162813861(L_18, /*hidden argument*/List_1_GetEnumerator_m162813861_RuntimeMethod_var);
		V_1 = L_19;
	}

IL_00b9:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0102;
		}

IL_00be:
		{
			ZombieCard_t2237653742 * L_20 = Enumerator_get_Current_m2411096257((Enumerator_t1304005065 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m2411096257_RuntimeMethod_var);
			V_0 = L_20;
			bool L_21 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_00e6;
			}
		}

IL_00d1:
		{
			ZombieCard_t2237653742 * L_22 = V_0;
			NullCheck(L_22);
			String_t* L_23 = ZombieCard_GetName_m1577076967(L_22, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_24 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2862902950, L_23, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		}

IL_00e6:
		{
			ZombieTurn_t3562480803 * L_25 = __this->get_zombieTurn_1();
			ZombieCard_t2237653742 * L_26 = V_0;
			NullCheck(L_25);
			ZombieTurn_set_CardBeingPlayed_m480097926(L_25, L_26, /*hidden argument*/NULL);
			ZombieTurn_t3562480803 * L_27 = __this->get_zombieTurn_1();
			NullCheck(L_27);
			ZombieTurn_StartPlayCard_m4151681992(L_27, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x127, FINALLY_0113);
		}

IL_0102:
		{
			bool L_28 = Enumerator_MoveNext_m4044029282((Enumerator_t1304005065 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m4044029282_RuntimeMethod_var);
			if (L_28)
			{
				goto IL_00be;
			}
		}

IL_010e:
		{
			IL2CPP_LEAVE(0x121, FINALLY_0113);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0113;
	}

FINALLY_0113:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3650518843((Enumerator_t1304005065 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m3650518843_RuntimeMethod_var);
		IL2CPP_END_FINALLY(275)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(275)
	{
		IL2CPP_JUMP_TBL(0x127, IL_0127)
		IL2CPP_JUMP_TBL(0x121, IL_0121)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0121:
	{
		ZombieTurnPhases_PhaseCheckIfNewZombiesSpawn_m1652064919(__this, /*hidden argument*/NULL);
	}

IL_0127:
	{
		return;
	}
}
// System.Void ZombieTurnPhases::PhaseCheckIfNewZombiesSpawn()
extern "C"  void ZombieTurnPhases_PhaseCheckIfNewZombiesSpawn_m1652064919 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_PhaseCheckIfNewZombiesSpawn_m1652064919_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral557816803, /*hidden argument*/NULL);
	}

IL_0015:
	{
		__this->set_PhaseName_3(4);
		ZombieTurn_t3562480803 * L_1 = __this->get_zombieTurn_1();
		NullCheck(L_1);
		GameSession_t4087811243 * L_2 = ZombieTurn_GetGameStats_m2272398959(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Zombies_t3905046179 * L_3 = GameSession_GetZombieTypes_m1922938114(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = Zombies_GetZombiesInPool_m542816421(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_00d5;
		}
	}
	{
		ZombieTurn_t3562480803 * L_5 = __this->get_zombieTurn_1();
		NullCheck(L_5);
		GameSession_t4087811243 * L_6 = ZombieTurn_GetGameStats_m2272398959(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Mission_t4233471175 * L_7 = GameSession_GetCurrentMission_m3760507839(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		List_1_t3747392069 * L_8 = L_7->get_SpecialRules_32();
		IL2CPP_RUNTIME_CLASS_INIT(Mission_t4233471175_il2cpp_TypeInfo_var);
		SpecialRulesTypes_t2275317327 * L_9 = ((Mission_t4233471175_StaticFields*)il2cpp_codegen_static_fields_for(Mission_t4233471175_il2cpp_TypeInfo_var))->get_ZombieAutoSpawn_20();
		NullCheck(L_8);
		bool L_10 = List_1_Contains_m915432422(L_8, L_9, /*hidden argument*/List_1_Contains_m915432422_RuntimeMethod_var);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		bool L_11 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0070;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2194267415, /*hidden argument*/NULL);
	}

IL_0070:
	{
		ZombieTurn_t3562480803 * L_12 = __this->get_zombieTurn_1();
		NullCheck(L_12);
		ZombieTurn_set_Spawn_m1222009344(L_12, (bool)1, /*hidden argument*/NULL);
		goto IL_00d0;
	}

IL_0081:
	{
		int32_t L_13 = Actions_RollD6_m698542841(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_14 = Actions_RollD6_m698542841(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)L_14));
		bool L_15 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_16 = V_0;
		int32_t L_17 = L_16;
		RuntimeObject * L_18 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2014980447, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		ZombieTurn_t3562480803 * L_20 = __this->get_zombieTurn_1();
		int32_t L_21 = V_0;
		ZombieTurn_t3562480803 * L_22 = __this->get_zombieTurn_1();
		NullCheck(L_22);
		GameSession_t4087811243 * L_23 = ZombieTurn_GetGameStats_m2272398959(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Zombies_t3905046179 * L_24 = GameSession_GetZombieTypes_m1922938114(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_25 = Zombies_SpawnCheck_m3840534542(L_24, /*hidden argument*/NULL);
		NullCheck(L_20);
		ZombieTurn_set_Spawn_m1222009344(L_20, (bool)((((int32_t)L_21) > ((int32_t)L_25))? 1 : 0), /*hidden argument*/NULL);
	}

IL_00d0:
	{
		goto IL_00e1;
	}

IL_00d5:
	{
		ZombieTurn_t3562480803 * L_26 = __this->get_zombieTurn_1();
		NullCheck(L_26);
		ZombieTurn_set_Spawn_m1222009344(L_26, (bool)0, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		ZombieTurn_t3562480803 * L_27 = __this->get_zombieTurn_1();
		NullCheck(L_27);
		GameSession_t4087811243 * L_28 = ZombieTurn_GetGameStats_m2272398959(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Zombies_t3905046179 * L_29 = GameSession_GetZombieTypes_m1922938114(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		List_1_t4068628460 * L_30 = Zombies_GetZombieTypes_m1957530793(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		int32_t L_31 = List_1_get_Count_m3173494979(L_30, /*hidden argument*/List_1_get_Count_m3173494979_RuntimeMethod_var);
		__this->set_MoveZombieIndex_2(L_31);
		ZombieTurnPhases_PhaseMoveZombies_m3650847429(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZombieTurnPhases::PhaseMoveZombies()
extern "C"  void ZombieTurnPhases_PhaseMoveZombies_m3650847429 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_PhaseMoveZombies_m3650847429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ZombieCard_t2237653742 * V_0 = NULL;
	Enumerator_t1304005065  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Zombie_t2596553718 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral66490830, /*hidden argument*/NULL);
	}

IL_0015:
	{
		__this->set_PhaseName_3(5);
		intptr_t L_1 = (intptr_t)ZombieTurnPhases_PhaseMoveZombies_m3650847429_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_2 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_2);
		String_t* L_3 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral1014326624, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral1271060643, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0076;
		}
	}
	{
		ZombieTurn_t3562480803 * L_5 = __this->get_zombieTurn_1();
		NullCheck(L_5);
		Text_t1901882714 * L_6 = L_5->get_TutorialText_61();
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, _stringLiteral1001145992);
		ZombieTurn_t3562480803 * L_7 = __this->get_zombieTurn_1();
		NullCheck(L_7);
		GameObject_t1113636619 * L_8 = L_7->get_TutorialPanel_60();
		NullCheck(L_8);
		Animator_t434523843 * L_9 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_8, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_9);
		Animator_SetTrigger_m2134052629(L_9, _stringLiteral3905732946, /*hidden argument*/NULL);
	}

IL_0076:
	{
		ZombieTurn_t3562480803 * L_10 = __this->get_zombieTurn_1();
		ZombieTurn_t3562480803 * L_11 = __this->get_zombieTurn_1();
		NullCheck(L_11);
		ZombieDeck_t3038342722 * L_12 = ZombieTurn_GetDecks_m242683250(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		List_1_t3709728484 * L_13 = ZombieDeck_CheckIfInHand_m2488265367(L_12, 2, /*hidden argument*/NULL);
		NullCheck(L_10);
		ZombieTurn_set_CardsToPlay_m2073406087(L_10, L_13, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_14 = __this->get_zombieTurn_1();
		NullCheck(L_14);
		List_1_t3709728484 * L_15 = ZombieTurn_get_CardsToPlay_m1683485823(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m1104284998(L_15, /*hidden argument*/List_1_get_Count_m1104284998_RuntimeMethod_var);
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_01c2;
		}
	}
	{
		ZombieTurn_t3562480803 * L_17 = __this->get_zombieTurn_1();
		NullCheck(L_17);
		List_1_t3709728484 * L_18 = ZombieTurn_get_CardsToPlay_m1683485823(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Enumerator_t1304005065  L_19 = List_1_GetEnumerator_m162813861(L_18, /*hidden argument*/List_1_GetEnumerator_m162813861_RuntimeMethod_var);
		V_1 = L_19;
	}

IL_00b9:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01a3;
		}

IL_00be:
		{
			ZombieCard_t2237653742 * L_20 = Enumerator_get_Current_m2411096257((Enumerator_t1304005065 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m2411096257_RuntimeMethod_var);
			V_0 = L_20;
			ZombieTurn_t3562480803 * L_21 = __this->get_zombieTurn_1();
			NullCheck(L_21);
			bool L_22 = ZombieTurn_PlayCardBasedOnDifficulty_m580813061(L_21, /*hidden argument*/NULL);
			if (L_22)
			{
				goto IL_0167;
			}
		}

IL_00d6:
		{
			bool L_23 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_010b;
			}
		}

IL_00e1:
		{
			ZombieTurn_t3562480803 * L_24 = __this->get_zombieTurn_1();
			NullCheck(L_24);
			List_1_t3709728484 * L_25 = ZombieTurn_get_CardsToPlay_m1683485823(L_24, /*hidden argument*/NULL);
			NullCheck(L_25);
			ZombieCard_t2237653742 * L_26 = List_1_get_Item_m247343563(L_25, 0, /*hidden argument*/List_1_get_Item_m247343563_RuntimeMethod_var);
			NullCheck(L_26);
			String_t* L_27 = ZombieCard_GetName_m1577076967(L_26, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_28 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2089403394, L_27, _stringLiteral2642543365, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		}

IL_010b:
		{
			ZombieTurn_t3562480803 * L_29 = __this->get_zombieTurn_1();
			NullCheck(L_29);
			List_1_t3319525431 * L_30 = ZombieTurn_get_CardsPlayed_m21435556(L_29, /*hidden argument*/NULL);
			ZombieTurn_t3562480803 * L_31 = __this->get_zombieTurn_1();
			NullCheck(L_31);
			List_1_t3709728484 * L_32 = ZombieTurn_get_CardsToPlay_m1683485823(L_31, /*hidden argument*/NULL);
			NullCheck(L_32);
			ZombieCard_t2237653742 * L_33 = List_1_get_Item_m247343563(L_32, 0, /*hidden argument*/List_1_get_Item_m247343563_RuntimeMethod_var);
			NullCheck(L_33);
			String_t* L_34 = ZombieCard_GetName_m1577076967(L_33, /*hidden argument*/NULL);
			NullCheck(L_30);
			List_1_Add_m1685793073(L_30, L_34, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
			ZombieTurn_t3562480803 * L_35 = __this->get_zombieTurn_1();
			NullCheck(L_35);
			List_1_t3319525431 * L_36 = ZombieTurn_get_CardsPlayed_m21435556(L_35, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_37 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
			NullCheck(L_36);
			List_1_Add_m1685793073(L_36, L_37, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
			ZombieTurn_t3562480803 * L_38 = __this->get_zombieTurn_1();
			NullCheck(L_38);
			ZombieDeck_t3038342722 * L_39 = ZombieTurn_GetDecks_m242683250(L_38, /*hidden argument*/NULL);
			ZombieTurn_t3562480803 * L_40 = __this->get_zombieTurn_1();
			NullCheck(L_40);
			List_1_t3709728484 * L_41 = ZombieTurn_get_CardsToPlay_m1683485823(L_40, /*hidden argument*/NULL);
			NullCheck(L_41);
			ZombieCard_t2237653742 * L_42 = List_1_get_Item_m247343563(L_41, 0, /*hidden argument*/List_1_get_Item_m247343563_RuntimeMethod_var);
			NullCheck(L_39);
			ZombieDeck_DoNotPlayCard_m3876639683(L_39, L_42, /*hidden argument*/NULL);
		}

IL_0167:
		{
			bool L_43 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
			if (!L_43)
			{
				goto IL_0187;
			}
		}

IL_0172:
		{
			ZombieCard_t2237653742 * L_44 = V_0;
			NullCheck(L_44);
			String_t* L_45 = ZombieCard_GetName_m1577076967(L_44, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_46 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1959744090, L_45, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		}

IL_0187:
		{
			ZombieTurn_t3562480803 * L_47 = __this->get_zombieTurn_1();
			ZombieCard_t2237653742 * L_48 = V_0;
			NullCheck(L_47);
			ZombieTurn_set_CardBeingPlayed_m480097926(L_47, L_48, /*hidden argument*/NULL);
			ZombieTurn_t3562480803 * L_49 = __this->get_zombieTurn_1();
			NullCheck(L_49);
			ZombieTurn_StartPlayCard_m4151681992(L_49, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x292, FINALLY_01b4);
		}

IL_01a3:
		{
			bool L_50 = Enumerator_MoveNext_m4044029282((Enumerator_t1304005065 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m4044029282_RuntimeMethod_var);
			if (L_50)
			{
				goto IL_00be;
			}
		}

IL_01af:
		{
			IL2CPP_LEAVE(0x1C2, FINALLY_01b4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01b4;
	}

FINALLY_01b4:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3650518843((Enumerator_t1304005065 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m3650518843_RuntimeMethod_var);
		IL2CPP_END_FINALLY(436)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(436)
	{
		IL2CPP_JUMP_TBL(0x292, IL_0292)
		IL2CPP_JUMP_TBL(0x1C2, IL_01c2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_01c2:
	{
		ZombieTurn_t3562480803 * L_51 = __this->get_zombieTurn_1();
		NullCheck(L_51);
		Text_t1901882714 * L_52 = L_51->get_MoveText_31();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_52);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_52, L_53);
		ZombieTurn_t3562480803 * L_54 = __this->get_zombieTurn_1();
		NullCheck(L_54);
		GameSession_t4087811243 * L_55 = ZombieTurn_GetGameStats_m2272398959(L_54, /*hidden argument*/NULL);
		NullCheck(L_55);
		Zombies_t3905046179 * L_56 = GameSession_GetZombieTypes_m1922938114(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		List_1_t4068628460 * L_57 = Zombies_GetZombieTypes_m1957530793(L_56, /*hidden argument*/NULL);
		int32_t L_58 = __this->get_MoveZombieIndex_2();
		NullCheck(L_57);
		Zombie_t2596553718 * L_59 = List_1_get_Item_m3871794941(L_57, ((int32_t)il2cpp_codegen_subtract((int32_t)L_58, (int32_t)1)), /*hidden argument*/List_1_get_Item_m3871794941_RuntimeMethod_var);
		V_2 = L_59;
		bool L_60 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0220;
		}
	}
	{
		ZombieTurn_t3562480803 * L_61 = __this->get_zombieTurn_1();
		NullCheck(L_61);
		GameSession_t4087811243 * L_62 = ZombieTurn_GetGameStats_m2272398959(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		Zombies_t3905046179 * L_63 = GameSession_GetZombieTypes_m1922938114(L_62, /*hidden argument*/NULL);
		NullCheck(L_63);
		Zombie_t2596553718 * L_64 = Zombies_GetZombieOfType_m2489544116(L_63, 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
	}

IL_0220:
	{
		ZombieTurn_t3562480803 * L_65 = __this->get_zombieTurn_1();
		NullCheck(L_65);
		Text_t1901882714 * L_66 = L_65->get_MoveZombieTypeText_32();
		Zombie_t2596553718 * L_67 = V_2;
		NullCheck(L_67);
		String_t* L_68 = Zombie_GetZombieName_m3563533677(L_67, /*hidden argument*/NULL);
		NullCheck(L_66);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_66, L_68);
		ZombieTurn_t3562480803 * L_69 = __this->get_zombieTurn_1();
		NullCheck(L_69);
		Text_t1901882714 * L_70 = L_69->get_MoveText_31();
		ZombieTurn_t3562480803 * L_71 = __this->get_zombieTurn_1();
		Zombie_t2596553718 * L_72 = V_2;
		NullCheck(L_71);
		String_t* L_73 = ZombieTurn_GetMovementText_m4033264826(L_71, L_72, /*hidden argument*/NULL);
		NullCheck(L_70);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_70, L_73);
		ZombieTurn_t3562480803 * L_74 = __this->get_zombieTurn_1();
		NullCheck(L_74);
		int32_t L_75 = ZombieTurn_get_Round_m1885665162(L_74, /*hidden argument*/NULL);
		if (((int32_t)((int32_t)L_75%(int32_t)3)))
		{
			goto IL_026a;
		}
	}
	{
		Zombie_t2596553718 * L_76 = V_2;
		NullCheck(L_76);
		Zombie_SetRandomMovementTarget_m1696563136(L_76, /*hidden argument*/NULL);
	}

IL_026a:
	{
		int32_t L_77 = __this->get_MoveZombieIndex_2();
		__this->set_MoveZombieIndex_2(((int32_t)il2cpp_codegen_subtract((int32_t)L_77, (int32_t)1)));
		ZombieTurn_t3562480803 * L_78 = __this->get_zombieTurn_1();
		NullCheck(L_78);
		GameObject_t1113636619 * L_79 = L_78->get_MovePanel_30();
		NullCheck(L_79);
		Animator_t434523843 * L_80 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_79, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_80);
		Animator_SetTrigger_m2134052629(L_80, _stringLiteral3905732946, /*hidden argument*/NULL);
	}

IL_0292:
	{
		return;
	}
}
// System.Void ZombieTurnPhases::PhaseFightHeroes()
extern "C"  void ZombieTurnPhases_PhaseFightHeroes_m3968111430 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_PhaseFightHeroes_m3968111430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3319525431 * V_0 = NULL;
	List_1_t3319525431 * V_1 = NULL;
	AnimatorStateInfo_t509032636  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		bool L_0 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral4237942660, /*hidden argument*/NULL);
	}

IL_0015:
	{
		__this->set_PhaseName_3(6);
		String_t* L_1 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral1014326624, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, _stringLiteral1271060643, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0064;
		}
	}
	{
		ZombieTurn_t3562480803 * L_3 = __this->get_zombieTurn_1();
		NullCheck(L_3);
		Text_t1901882714 * L_4 = L_3->get_TutorialText_61();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteral183330392);
		ZombieTurn_t3562480803 * L_5 = __this->get_zombieTurn_1();
		NullCheck(L_5);
		GameObject_t1113636619 * L_6 = L_5->get_TutorialPanel_60();
		NullCheck(L_6);
		Animator_t434523843 * L_7 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_6, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_7);
		Animator_SetTrigger_m2134052629(L_7, _stringLiteral3905732946, /*hidden argument*/NULL);
	}

IL_0064:
	{
		ZombieTurn_t3562480803 * L_8 = __this->get_zombieTurn_1();
		NullCheck(L_8);
		GameSession_t4087811243 * L_9 = ZombieTurn_GetGameStats_m2272398959(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Mission_t4233471175 * L_10 = GameSession_GetCurrentMission_m3760507839(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		List_1_t4188946901 * L_11 = L_10->get_ZombieObjective_30();
		IL2CPP_RUNTIME_CLASS_INIT(Mission_t4233471175_il2cpp_TypeInfo_var);
		ObjectiveTypes_t2716872159 * L_12 = ((Mission_t4233471175_StaticFields*)il2cpp_codegen_static_fields_for(Mission_t4233471175_il2cpp_TypeInfo_var))->get_Destroy6Buildings_6();
		NullCheck(L_11);
		bool L_13 = List_1_Contains_m92166693(L_11, L_12, /*hidden argument*/List_1_Contains_m92166693_RuntimeMethod_var);
		if (!L_13)
		{
			goto IL_0106;
		}
	}
	{
		ZombieTurn_t3562480803 * L_14 = __this->get_zombieTurn_1();
		NullCheck(L_14);
		Slider_t3903728902 * L_15 = L_14->get_DestroyBuildingZombieSlider_56();
		NullCheck(L_15);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_15, (0.0f));
		ZombieTurn_t3562480803 * L_16 = __this->get_zombieTurn_1();
		NullCheck(L_16);
		Dropdown_t2274391225 * L_17 = L_16->get_DestroyBuildingDropdown_57();
		NullCheck(L_17);
		Dropdown_ClearOptions_m4085591601(L_17, /*hidden argument*/NULL);
		List_1_t3319525431 * L_18 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_18, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		V_1 = L_18;
		List_1_t3319525431 * L_19 = V_1;
		NullCheck(L_19);
		List_1_Add_m1685793073(L_19, _stringLiteral2791739702, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		List_1_t3319525431 * L_20 = V_1;
		V_0 = L_20;
		List_1_t3319525431 * L_21 = V_0;
		ZombieTurn_t3562480803 * L_22 = __this->get_zombieTurn_1();
		NullCheck(L_22);
		GameSession_t4087811243 * L_23 = ZombieTurn_GetGameStats_m2272398959(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Tiles_t1457048987 * L_24 = GameSession_GetMapTiles_m3864703078(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		List_1_t3319525431 * L_25 = Tiles_GetBuildingToDestroyPriority_m3399415659(L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		List_1_AddRange_m3621602103(L_21, L_25, /*hidden argument*/List_1_AddRange_m3621602103_RuntimeMethod_var);
		ZombieTurn_t3562480803 * L_26 = __this->get_zombieTurn_1();
		NullCheck(L_26);
		Dropdown_t2274391225 * L_27 = L_26->get_DestroyBuildingDropdown_57();
		List_1_t3319525431 * L_28 = V_0;
		NullCheck(L_27);
		Dropdown_AddOptions_m2776940360(L_27, L_28, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_29 = __this->get_zombieTurn_1();
		NullCheck(L_29);
		GameObject_t1113636619 * L_30 = L_29->get_DestroyBuildingPanel_55();
		NullCheck(L_30);
		Animator_t434523843 * L_31 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_30, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_31);
		Animator_SetTrigger_m2134052629(L_31, _stringLiteral3905732946, /*hidden argument*/NULL);
	}

IL_0106:
	{
		intptr_t L_32 = (intptr_t)ZombieTurnPhases_PhaseSpawnNewZombies_m3815212178_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_33 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_33, __this, L_32, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_33);
		ZombieTurn_t3562480803 * L_34 = __this->get_zombieTurn_1();
		NullCheck(L_34);
		ZombieTurn_SetFightInfoText_m3143307073(L_34, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_35 = __this->get_zombieTurn_1();
		NullCheck(L_35);
		GameObject_t1113636619 * L_36 = L_35->get_FightPanel_33();
		NullCheck(L_36);
		Animator_t434523843 * L_37 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_36, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_37);
		AnimatorStateInfo_t509032636  L_38 = Animator_GetCurrentAnimatorStateInfo_m18694920(L_37, 0, /*hidden argument*/NULL);
		V_2 = L_38;
		bool L_39 = AnimatorStateInfo_IsName_m3393819976((AnimatorStateInfo_t509032636 *)(&V_2), _stringLiteral2432531900, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0165;
		}
	}
	{
		ZombieTurn_t3562480803 * L_40 = __this->get_zombieTurn_1();
		NullCheck(L_40);
		GameObject_t1113636619 * L_41 = L_40->get_FightPanel_33();
		NullCheck(L_41);
		Animator_t434523843 * L_42 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_41, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_42);
		Animator_SetTrigger_m2134052629(L_42, _stringLiteral3905732946, /*hidden argument*/NULL);
	}

IL_0165:
	{
		return;
	}
}
// System.Void ZombieTurnPhases::PhaseFightResults()
extern "C"  void ZombieTurnPhases_PhaseFightResults_m2355902853 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_PhaseFightResults_m2355902853_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral4161958653, /*hidden argument*/NULL);
	}

IL_0015:
	{
		__this->set_PhaseName_3(7);
		intptr_t L_1 = (intptr_t)ZombieTurnPhases_PhaseSpawnNewZombies_m3815212178_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_2 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_2);
		TurnPhaseDelegateDeclare_t3595605765 * L_3 = __this->get_TurnPhaseDelegate_0();
		NullCheck(L_3);
		TurnPhaseDelegateDeclare_Invoke_m1688307234(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZombieTurnPhases::PhaseSpawnNewZombies()
extern "C"  void ZombieTurnPhases_PhaseSpawnNewZombies_m3815212178 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_PhaseSpawnNewZombies_m3815212178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t385246372* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		bool L_0 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3003607055, /*hidden argument*/NULL);
	}

IL_0015:
	{
		__this->set_PhaseName_3(8);
		intptr_t L_1 = (intptr_t)ZombieTurnPhases_PhasePlayEndOfTurnCards_m2517377176_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_2 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_2);
		ZombieTurn_t3562480803 * L_3 = __this->get_zombieTurn_1();
		NullCheck(L_3);
		bool L_4 = ZombieTurn_get_Spawn_m2604868717(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_010a;
		}
	}
	{
		ZombieTurn_t3562480803 * L_5 = __this->get_zombieTurn_1();
		NullCheck(L_5);
		Int32U5BU5D_t385246372* L_6 = ZombieTurn_SpawnZombies_m917606607(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		ZombieTurn_t3562480803 * L_7 = __this->get_zombieTurn_1();
		NullCheck(L_7);
		Text_t1901882714 * L_8 = L_7->get_SpawnText_38();
		Int32U5BU5D_t385246372* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = 0;
		int32_t L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral1589685702, L_13, _stringLiteral228571493, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_14);
		Int32U5BU5D_t385246372* L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = 1;
		int32_t L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_00bb;
		}
	}
	{
		ZombieTurn_t3562480803 * L_18 = __this->get_zombieTurn_1();
		NullCheck(L_18);
		Text_t1901882714 * L_19 = L_18->get_SpawnText_38();
		Text_t1901882714 * L_20 = L_19;
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_20);
		V_1 = L_21;
		ObjectU5BU5D_t2843939325* L_22 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_23 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_23);
		ObjectU5BU5D_t2843939325* L_24 = L_22;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral1589685702);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)_stringLiteral1589685702);
		ObjectU5BU5D_t2843939325* L_25 = L_24;
		Int32U5BU5D_t385246372* L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = 1;
		int32_t L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		int32_t L_29 = L_28;
		RuntimeObject * L_30 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_30);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_30);
		ObjectU5BU5D_t2843939325* L_31 = L_25;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteral2493621018);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)_stringLiteral2493621018);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2971454694(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		NullCheck(L_20);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_20, L_32);
	}

IL_00bb:
	{
		ZombieTurn_t3562480803 * L_33 = __this->get_zombieTurn_1();
		NullCheck(L_33);
		bool L_34 = ZombieTurn_IsZombieMovesAfterSpawn_m2315172336(L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00eb;
		}
	}
	{
		ZombieTurn_t3562480803 * L_35 = __this->get_zombieTurn_1();
		NullCheck(L_35);
		Text_t1901882714 * L_36 = L_35->get_SpawnText_38();
		Text_t1901882714 * L_37 = L_36;
		NullCheck(L_37);
		String_t* L_38 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_37);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m3937257545(NULL /*static, unused*/, L_38, _stringLiteral448053140, /*hidden argument*/NULL);
		NullCheck(L_37);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_37, L_39);
	}

IL_00eb:
	{
		ZombieTurn_t3562480803 * L_40 = __this->get_zombieTurn_1();
		NullCheck(L_40);
		GameObject_t1113636619 * L_41 = L_40->get_SpawnPanel_37();
		NullCheck(L_41);
		Animator_t434523843 * L_42 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_41, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_42);
		Animator_SetTrigger_m2134052629(L_42, _stringLiteral3905732946, /*hidden argument*/NULL);
		goto IL_0115;
	}

IL_010a:
	{
		TurnPhaseDelegateDeclare_t3595605765 * L_43 = __this->get_TurnPhaseDelegate_0();
		NullCheck(L_43);
		TurnPhaseDelegateDeclare_Invoke_m1688307234(L_43, /*hidden argument*/NULL);
	}

IL_0115:
	{
		return;
	}
}
// System.Void ZombieTurnPhases::PhasePlayEndOfTurnCards()
extern "C"  void ZombieTurnPhases_PhasePlayEndOfTurnCards_m2517377176 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_PhasePlayEndOfTurnCards_m2517377176_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ZombieCard_t2237653742 * V_0 = NULL;
	Enumerator_t1304005065  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3039913822, /*hidden argument*/NULL);
	}

IL_0015:
	{
		__this->set_PhaseName_3(((int32_t)9));
		intptr_t L_1 = (intptr_t)ZombieTurnPhases_PhasePlayEndOfTurnCards_m2517377176_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_2 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_2);
		ZombieTurn_t3562480803 * L_3 = __this->get_zombieTurn_1();
		ZombieTurn_t3562480803 * L_4 = __this->get_zombieTurn_1();
		NullCheck(L_4);
		ZombieDeck_t3038342722 * L_5 = ZombieTurn_GetDecks_m242683250(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		List_1_t3709728484 * L_6 = ZombieDeck_CheckIfInHand_m2488265367(L_5, 5, /*hidden argument*/NULL);
		NullCheck(L_3);
		ZombieTurn_set_CardsToPlay_m2073406087(L_3, L_6, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_7 = __this->get_zombieTurn_1();
		NullCheck(L_7);
		List_1_t3709728484 * L_8 = ZombieTurn_get_CardsToPlay_m1683485823(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m1104284998(L_8, /*hidden argument*/List_1_get_Count_m1104284998_RuntimeMethod_var);
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_015b;
		}
	}
	{
		ZombieTurn_t3562480803 * L_10 = __this->get_zombieTurn_1();
		NullCheck(L_10);
		List_1_t3709728484 * L_11 = ZombieTurn_get_CardsToPlay_m1683485823(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Enumerator_t1304005065  L_12 = List_1_GetEnumerator_m162813861(L_11, /*hidden argument*/List_1_GetEnumerator_m162813861_RuntimeMethod_var);
		V_1 = L_12;
	}

IL_0072:
	try
	{ // begin try (depth: 1)
		{
			goto IL_013c;
		}

IL_0077:
		{
			ZombieCard_t2237653742 * L_13 = Enumerator_get_Current_m2411096257((Enumerator_t1304005065 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m2411096257_RuntimeMethod_var);
			V_0 = L_13;
			ZombieTurn_t3562480803 * L_14 = __this->get_zombieTurn_1();
			NullCheck(L_14);
			bool L_15 = ZombieTurn_PlayCardBasedOnDifficulty_m580813061(L_14, /*hidden argument*/NULL);
			if (L_15)
			{
				goto IL_0100;
			}
		}

IL_008f:
		{
			bool L_16 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
			if (!L_16)
			{
				goto IL_00b4;
			}
		}

IL_009a:
		{
			ZombieCard_t2237653742 * L_17 = V_0;
			NullCheck(L_17);
			String_t* L_18 = ZombieCard_GetName_m1577076967(L_17, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_19 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2089403394, L_18, _stringLiteral2642543365, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		}

IL_00b4:
		{
			ZombieTurn_t3562480803 * L_20 = __this->get_zombieTurn_1();
			NullCheck(L_20);
			List_1_t3319525431 * L_21 = ZombieTurn_get_CardsPlayed_m21435556(L_20, /*hidden argument*/NULL);
			ZombieTurn_t3562480803 * L_22 = __this->get_zombieTurn_1();
			NullCheck(L_22);
			List_1_t3709728484 * L_23 = ZombieTurn_get_CardsToPlay_m1683485823(L_22, /*hidden argument*/NULL);
			NullCheck(L_23);
			ZombieCard_t2237653742 * L_24 = List_1_get_Item_m247343563(L_23, 0, /*hidden argument*/List_1_get_Item_m247343563_RuntimeMethod_var);
			NullCheck(L_24);
			String_t* L_25 = ZombieCard_GetName_m1577076967(L_24, /*hidden argument*/NULL);
			NullCheck(L_21);
			List_1_Add_m1685793073(L_21, L_25, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
			ZombieTurn_t3562480803 * L_26 = __this->get_zombieTurn_1();
			NullCheck(L_26);
			List_1_t3319525431 * L_27 = ZombieTurn_get_CardsPlayed_m21435556(L_26, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_28 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
			NullCheck(L_27);
			List_1_Add_m1685793073(L_27, L_28, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
			ZombieTurn_t3562480803 * L_29 = __this->get_zombieTurn_1();
			NullCheck(L_29);
			ZombieDeck_t3038342722 * L_30 = ZombieTurn_GetDecks_m242683250(L_29, /*hidden argument*/NULL);
			ZombieCard_t2237653742 * L_31 = V_0;
			NullCheck(L_30);
			ZombieDeck_DoNotPlayCard_m3876639683(L_30, L_31, /*hidden argument*/NULL);
		}

IL_0100:
		{
			bool L_32 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
			if (!L_32)
			{
				goto IL_0120;
			}
		}

IL_010b:
		{
			ZombieCard_t2237653742 * L_33 = V_0;
			NullCheck(L_33);
			String_t* L_34 = ZombieCard_GetName_m1577076967(L_33, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_35 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1602784365, L_34, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		}

IL_0120:
		{
			ZombieTurn_t3562480803 * L_36 = __this->get_zombieTurn_1();
			ZombieCard_t2237653742 * L_37 = V_0;
			NullCheck(L_36);
			ZombieTurn_set_CardBeingPlayed_m480097926(L_36, L_37, /*hidden argument*/NULL);
			ZombieTurn_t3562480803 * L_38 = __this->get_zombieTurn_1();
			NullCheck(L_38);
			ZombieTurn_StartPlayCard_m4151681992(L_38, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x178, FINALLY_014d);
		}

IL_013c:
		{
			bool L_39 = Enumerator_MoveNext_m4044029282((Enumerator_t1304005065 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m4044029282_RuntimeMethod_var);
			if (L_39)
			{
				goto IL_0077;
			}
		}

IL_0148:
		{
			IL2CPP_LEAVE(0x15B, FINALLY_014d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_014d;
	}

FINALLY_014d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3650518843((Enumerator_t1304005065 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m3650518843_RuntimeMethod_var);
		IL2CPP_END_FINALLY(333)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(333)
	{
		IL2CPP_JUMP_TBL(0x178, IL_0178)
		IL2CPP_JUMP_TBL(0x15B, IL_015b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_015b:
	{
		intptr_t L_40 = (intptr_t)ZombieTurnPhases_PhaseHeroesTurn_m1774508636_RuntimeMethod_var;
		TurnPhaseDelegateDeclare_t3595605765 * L_41 = (TurnPhaseDelegateDeclare_t3595605765 *)il2cpp_codegen_object_new(TurnPhaseDelegateDeclare_t3595605765_il2cpp_TypeInfo_var);
		TurnPhaseDelegateDeclare__ctor_m1784330835(L_41, __this, L_40, /*hidden argument*/NULL);
		__this->set_TurnPhaseDelegate_0(L_41);
		TurnPhaseDelegateDeclare_t3595605765 * L_42 = __this->get_TurnPhaseDelegate_0();
		NullCheck(L_42);
		TurnPhaseDelegateDeclare_Invoke_m1688307234(L_42, /*hidden argument*/NULL);
	}

IL_0178:
	{
		return;
	}
}
// System.Void ZombieTurnPhases::PhaseHeroesTurn()
extern "C"  void ZombieTurnPhases_PhaseHeroesTurn_m1774508636 (ZombieTurnPhases_t3127322021 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZombieTurnPhases_PhaseHeroesTurn_m1774508636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ZombieTurnPhases_IsDebugLog_m2641272074(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2651087753, /*hidden argument*/NULL);
	}

IL_0015:
	{
		__this->set_PhaseName_3(((int32_t)10));
		ZombieTurn_t3562480803 * L_1 = __this->get_zombieTurn_1();
		NullCheck(L_1);
		GameSession_t4087811243 * L_2 = ZombieTurn_GetGameStats_m2272398959(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Zombies_t3905046179 * L_3 = GameSession_GetZombieTypes_m1922938114(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Zombies_set_IsZombieTurn_m628466427(L_3, (bool)0, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_4 = __this->get_zombieTurn_1();
		NullCheck(L_4);
		GameSession_t4087811243 * L_5 = ZombieTurn_GetGameStats_m2272398959(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Zombies_t3905046179 * L_6 = GameSession_GetZombieTypes_m1922938114(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Zombies_ResetExtraRoundDice_m1135819780(L_6, /*hidden argument*/NULL);
		ZombieTurn_t3562480803 * L_7 = __this->get_zombieTurn_1();
		NullCheck(L_7);
		TextAnimation_t2694049294 * L_8 = L_7->get_TextAnimation_46();
		NullCheck(L_8);
		TextAnimation_Play_m3501269388(L_8, _stringLiteral2381261638, /*hidden argument*/NULL);
		String_t* L_9 = PlayerPrefs_GetString_m389913383(NULL /*static, unused*/, _stringLiteral1014326624, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_9, _stringLiteral1271060643, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		ZombieTurn_t3562480803 * L_11 = __this->get_zombieTurn_1();
		NullCheck(L_11);
		Text_t1901882714 * L_12 = L_11->get_TutorialText_61();
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, _stringLiteral4271152816);
		ZombieTurn_t3562480803 * L_13 = __this->get_zombieTurn_1();
		NullCheck(L_13);
		GameObject_t1113636619 * L_14 = L_13->get_TutorialPanel_60();
		NullCheck(L_14);
		Animator_t434523843 * L_15 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_14, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_15);
		Animator_SetTrigger_m2134052629(L_15, _stringLiteral3905732946, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_TurnPhaseDelegateDeclare_t3595605765 (TurnPhaseDelegateDeclare_t3595605765 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void ZombieTurnPhases/TurnPhaseDelegateDeclare::.ctor(System.Object,System.IntPtr)
extern "C"  void TurnPhaseDelegateDeclare__ctor_m1784330835 (TurnPhaseDelegateDeclare_t3595605765 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void ZombieTurnPhases/TurnPhaseDelegateDeclare::Invoke()
extern "C"  void TurnPhaseDelegateDeclare_Invoke_m1688307234 (TurnPhaseDelegateDeclare_t3595605765 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TurnPhaseDelegateDeclare_Invoke_m1688307234((TurnPhaseDelegateDeclare_t3595605765 *)__this->get_prev_9(), method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
			}
		}
	}
	else
	{
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
}
// System.IAsyncResult ZombieTurnPhases/TurnPhaseDelegateDeclare::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* TurnPhaseDelegateDeclare_BeginInvoke_m55149755 (TurnPhaseDelegateDeclare_t3595605765 * __this, AsyncCallback_t3962456242 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void ZombieTurnPhases/TurnPhaseDelegateDeclare::EndInvoke(System.IAsyncResult)
extern "C"  void TurnPhaseDelegateDeclare_EndInvoke_m2589785319 (TurnPhaseDelegateDeclare_t3595605765 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
