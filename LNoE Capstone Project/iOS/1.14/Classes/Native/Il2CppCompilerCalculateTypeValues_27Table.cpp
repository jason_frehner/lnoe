﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// UnityEngine.Animator
struct Animator_t434523843;
// ZombieTurn
struct ZombieTurn_t3562480803;
// System.Collections.Generic.List`1<ZombieCard>
struct List_1_t3709728484;
// System.Func`2<ZombieCard,System.String>
struct Func_2_t166598193;
// ZombieCard
struct ZombieCard_t2237653742;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// Building
struct Building_t1411255995;
// ZombieTurnPhases/TurnPhaseDelegateDeclare
struct TurnPhaseDelegateDeclare_t3595605765;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Func`2<MapTile,System.Boolean>
struct Func_2_t1480287660;
// System.Func`2<Building,System.Boolean>
struct Func_2_t137751404;
// SpeechRecognizerManager
struct SpeechRecognizerManager_t652028364;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// ZombieDeck
struct ZombieDeck_t3038342722;
// GameSession
struct GameSession_t4087811243;
// GameData
struct GameData_t415813024;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// ZombieTurnPhases
struct ZombieTurnPhases_t3127322021;
// UnityEngine.UI.Text
struct Text_t1901882714;
// TextAnimation
struct TextAnimation_t2694049294;
// GameLog
struct GameLog_t2697247111;
// PlaceMapImages
struct PlaceMapImages_t1484071588;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// ViewableUI
struct ViewableUI_t3820486676;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CGETCARDU3EC__ANONSTOREY2_T3292952327_H
#define U3CGETCARDU3EC__ANONSTOREY2_T3292952327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieDeck/<GetCard>c__AnonStorey2
struct  U3CGetCardU3Ec__AnonStorey2_t3292952327  : public RuntimeObject
{
public:
	// System.String ZombieDeck/<GetCard>c__AnonStorey2::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CGetCardU3Ec__AnonStorey2_t3292952327, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCARDU3EC__ANONSTOREY2_T3292952327_H
#ifndef U3CPLAYCARDU3EC__ITERATOR0_T1992789720_H
#define U3CPLAYCARDU3EC__ITERATOR0_T1992789720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurn/<PlayCard>c__Iterator0
struct  U3CPlayCardU3Ec__Iterator0_t1992789720  : public RuntimeObject
{
public:
	// UnityEngine.Animator ZombieTurn/<PlayCard>c__Iterator0::<cardAnimator>__0
	Animator_t434523843 * ___U3CcardAnimatorU3E__0_0;
	// ZombieTurn ZombieTurn/<PlayCard>c__Iterator0::$this
	ZombieTurn_t3562480803 * ___U24this_1;
	// System.Object ZombieTurn/<PlayCard>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ZombieTurn/<PlayCard>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 ZombieTurn/<PlayCard>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcardAnimatorU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPlayCardU3Ec__Iterator0_t1992789720, ___U3CcardAnimatorU3E__0_0)); }
	inline Animator_t434523843 * get_U3CcardAnimatorU3E__0_0() const { return ___U3CcardAnimatorU3E__0_0; }
	inline Animator_t434523843 ** get_address_of_U3CcardAnimatorU3E__0_0() { return &___U3CcardAnimatorU3E__0_0; }
	inline void set_U3CcardAnimatorU3E__0_0(Animator_t434523843 * value)
	{
		___U3CcardAnimatorU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcardAnimatorU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CPlayCardU3Ec__Iterator0_t1992789720, ___U24this_1)); }
	inline ZombieTurn_t3562480803 * get_U24this_1() const { return ___U24this_1; }
	inline ZombieTurn_t3562480803 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ZombieTurn_t3562480803 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CPlayCardU3Ec__Iterator0_t1992789720, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CPlayCardU3Ec__Iterator0_t1992789720, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CPlayCardU3Ec__Iterator0_t1992789720, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYCARDU3EC__ITERATOR0_T1992789720_H
#ifndef ZOMBIEDECK_T3038342722_H
#define ZOMBIEDECK_T3038342722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieDeck
struct  ZombieDeck_t3038342722  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::deck
	List_1_t3709728484 * ___deck_0;
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::discard
	List_1_t3709728484 * ___discard_1;
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::hand
	List_1_t3709728484 * ___hand_2;
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::handDoNotPlay
	List_1_t3709728484 * ___handDoNotPlay_3;
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::remains
	List_1_t3709728484 * ___remains_4;
	// System.Collections.Generic.List`1<ZombieCard> ZombieDeck::uniqueCards
	List_1_t3709728484 * ___uniqueCards_5;

public:
	inline static int32_t get_offset_of_deck_0() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___deck_0)); }
	inline List_1_t3709728484 * get_deck_0() const { return ___deck_0; }
	inline List_1_t3709728484 ** get_address_of_deck_0() { return &___deck_0; }
	inline void set_deck_0(List_1_t3709728484 * value)
	{
		___deck_0 = value;
		Il2CppCodeGenWriteBarrier((&___deck_0), value);
	}

	inline static int32_t get_offset_of_discard_1() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___discard_1)); }
	inline List_1_t3709728484 * get_discard_1() const { return ___discard_1; }
	inline List_1_t3709728484 ** get_address_of_discard_1() { return &___discard_1; }
	inline void set_discard_1(List_1_t3709728484 * value)
	{
		___discard_1 = value;
		Il2CppCodeGenWriteBarrier((&___discard_1), value);
	}

	inline static int32_t get_offset_of_hand_2() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___hand_2)); }
	inline List_1_t3709728484 * get_hand_2() const { return ___hand_2; }
	inline List_1_t3709728484 ** get_address_of_hand_2() { return &___hand_2; }
	inline void set_hand_2(List_1_t3709728484 * value)
	{
		___hand_2 = value;
		Il2CppCodeGenWriteBarrier((&___hand_2), value);
	}

	inline static int32_t get_offset_of_handDoNotPlay_3() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___handDoNotPlay_3)); }
	inline List_1_t3709728484 * get_handDoNotPlay_3() const { return ___handDoNotPlay_3; }
	inline List_1_t3709728484 ** get_address_of_handDoNotPlay_3() { return &___handDoNotPlay_3; }
	inline void set_handDoNotPlay_3(List_1_t3709728484 * value)
	{
		___handDoNotPlay_3 = value;
		Il2CppCodeGenWriteBarrier((&___handDoNotPlay_3), value);
	}

	inline static int32_t get_offset_of_remains_4() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___remains_4)); }
	inline List_1_t3709728484 * get_remains_4() const { return ___remains_4; }
	inline List_1_t3709728484 ** get_address_of_remains_4() { return &___remains_4; }
	inline void set_remains_4(List_1_t3709728484 * value)
	{
		___remains_4 = value;
		Il2CppCodeGenWriteBarrier((&___remains_4), value);
	}

	inline static int32_t get_offset_of_uniqueCards_5() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722, ___uniqueCards_5)); }
	inline List_1_t3709728484 * get_uniqueCards_5() const { return ___uniqueCards_5; }
	inline List_1_t3709728484 ** get_address_of_uniqueCards_5() { return &___uniqueCards_5; }
	inline void set_uniqueCards_5(List_1_t3709728484 * value)
	{
		___uniqueCards_5 = value;
		Il2CppCodeGenWriteBarrier((&___uniqueCards_5), value);
	}
};

struct ZombieDeck_t3038342722_StaticFields
{
public:
	// System.Func`2<ZombieCard,System.String> ZombieDeck::<>f__am$cache0
	Func_2_t166598193 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(ZombieDeck_t3038342722_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Func_2_t166598193 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Func_2_t166598193 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Func_2_t166598193 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIEDECK_T3038342722_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CADDCARDSTODECKU3EC__ANONSTOREY0_T558456971_H
#define U3CADDCARDSTODECKU3EC__ANONSTOREY0_T558456971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieDeck/<AddCardsToDeck>c__AnonStorey0
struct  U3CAddCardsToDeckU3Ec__AnonStorey0_t558456971  : public RuntimeObject
{
public:
	// ZombieCard ZombieDeck/<AddCardsToDeck>c__AnonStorey0::card
	ZombieCard_t2237653742 * ___card_0;

public:
	inline static int32_t get_offset_of_card_0() { return static_cast<int32_t>(offsetof(U3CAddCardsToDeckU3Ec__AnonStorey0_t558456971, ___card_0)); }
	inline ZombieCard_t2237653742 * get_card_0() const { return ___card_0; }
	inline ZombieCard_t2237653742 ** get_address_of_card_0() { return &___card_0; }
	inline void set_card_0(ZombieCard_t2237653742 * value)
	{
		___card_0 = value;
		Il2CppCodeGenWriteBarrier((&___card_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDCARDSTODECKU3EC__ANONSTOREY0_T558456971_H
#ifndef U3CFIGHTROLLU3EC__ANONSTOREY1_T3184696250_H
#define U3CFIGHTROLLU3EC__ANONSTOREY1_T3184696250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurn/<FightRoll>c__AnonStorey1
struct  U3CFightRollU3Ec__AnonStorey1_t3184696250  : public RuntimeObject
{
public:
	// System.Int32 ZombieTurn/<FightRoll>c__AnonStorey1::index
	int32_t ___index_0;
	// ZombieTurn ZombieTurn/<FightRoll>c__AnonStorey1::$this
	ZombieTurn_t3562480803 * ___U24this_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CFightRollU3Ec__AnonStorey1_t3184696250, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFightRollU3Ec__AnonStorey1_t3184696250, ___U24this_1)); }
	inline ZombieTurn_t3562480803 * get_U24this_1() const { return ___U24this_1; }
	inline ZombieTurn_t3562480803 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ZombieTurn_t3562480803 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFIGHTROLLU3EC__ANONSTOREY1_T3184696250_H
#ifndef U24ARRAYTYPEU3D24_T2467506693_H
#define U24ARRAYTYPEU3D24_T2467506693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t2467506693 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t2467506693__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T2467506693_H
#ifndef U24ARRAYTYPEU3D64_T498138225_H
#define U24ARRAYTYPEU3D64_T498138225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=64
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D64_t498138225 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D64_t498138225__padding[64];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D64_T498138225_H
#ifndef U24ARRAYTYPEU3D56_T1283759776_H
#define U24ARRAYTYPEU3D56_T1283759776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=56
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D56_t1283759776 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D56_t1283759776__padding[56];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D56_T1283759776_H
#ifndef U24ARRAYTYPEU3D32_T3651253610_H
#define U24ARRAYTYPEU3D32_T3651253610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=32
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D32_t3651253610 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D32_t3651253610__padding[32];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D32_T3651253610_H
#ifndef U24ARRAYTYPEU3D16_T3253128244_H
#define U24ARRAYTYPEU3D16_T3253128244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128244 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128244__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128244_H
#ifndef U24ARRAYTYPEU3D208_T3449392908_H
#define U24ARRAYTYPEU3D208_T3449392908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=208
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D208_t3449392908 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D208_t3449392908__padding[208];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D208_T3449392908_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255367  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-A2F1578C0F6612632B975A3AFE7CB9184E9F5590
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2DA2F1578C0F6612632B975A3AFE7CB9184E9F5590_0;
	// <PrivateImplementationDetails>/$ArrayType=208 <PrivateImplementationDetails>::$field-81AFD0604569DA0255357C175BE345F58F17D478
	U24ArrayTypeU3D208_t3449392908  ___U24fieldU2D81AFD0604569DA0255357C175BE345F58F17D478_1;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-380E84549CB845604C318E8E14B73622CC10AF42
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D380E84549CB845604C318E8E14B73622CC10AF42_2;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-963821E4BFFA8EED3A18D7173F2147779057B9B3
	U24ArrayTypeU3D64_t498138225  ___U24fieldU2D963821E4BFFA8EED3A18D7173F2147779057B9B3_3;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-D4EF3A9F65679752698916F2F8DBCD78EE2E41B8
	U24ArrayTypeU3D64_t498138225  ___U24fieldU2DD4EF3A9F65679752698916F2F8DBCD78EE2E41B8_4;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-1CA1C79F41AEC3E78B9270CE6E221118308DFFD5
	U24ArrayTypeU3D64_t498138225  ___U24fieldU2D1CA1C79F41AEC3E78B9270CE6E221118308DFFD5_5;
	// <PrivateImplementationDetails>/$ArrayType=64 <PrivateImplementationDetails>::$field-1DA0256192B60CA2089E8E33FA9F10415D904C9B
	U24ArrayTypeU3D64_t498138225  ___U24fieldU2D1DA0256192B60CA2089E8E33FA9F10415D904C9B_6;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-49610B6D06207419DD075235B5603F1860DDFB14
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D49610B6D06207419DD075235B5603F1860DDFB14_7;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-949FFA4D55D6CAF924550A0169CBEEF3F51C7F2A
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D949FFA4D55D6CAF924550A0169CBEEF3F51C7F2A_8;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-EC16EE82A2343F7D332375CE590AFA11FFC6C5F0
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2DEC16EE82A2343F7D332375CE590AFA11FFC6C5F0_9;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-2223729B78EBEE4B2E434CA9BAF047311413D5C0
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D2223729B78EBEE4B2E434CA9BAF047311413D5C0_10;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-529B72F43D5DB46080436118B23FCEA9D7F66A32
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D529B72F43D5DB46080436118B23FCEA9D7F66A32_11;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-46284D0F43072429162AA2925082FFAC2C0DE31A
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D46284D0F43072429162AA2925082FFAC2C0DE31A_12;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-9EDEF80E922E3463CA046C98E9CFF518CFD1FECA
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D9EDEF80E922E3463CA046C98E9CFF518CFD1FECA_13;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-82091C20834853F435A614D3DCBC5AD6D9FEF27F
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D82091C20834853F435A614D3DCBC5AD6D9FEF27F_14;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-C66129415B3CAF838A8150A682848D3F4ACAAFC1
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DC66129415B3CAF838A8150A682848D3F4ACAAFC1_15;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-6634EA9EA45C5BDEBFF437FC9807140E0BFA58AC
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D6634EA9EA45C5BDEBFF437FC9807140E0BFA58AC_16;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-B2ABE92600B53F2B8A03B9A9E2C734A8BE0C6415
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DB2ABE92600B53F2B8A03B9A9E2C734A8BE0C6415_17;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-A8B6B603D922A9A2949780CB840679B621BF5FFC
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DA8B6B603D922A9A2949780CB840679B621BF5FFC_18;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-F7079845619834F11587B9DB9E90CC2A70A46537
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DF7079845619834F11587B9DB9E90CC2A70A46537_19;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-61070D52649698A08E65825BD36FECC1192D073E
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D61070D52649698A08E65825BD36FECC1192D073E_20;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-5DBF7686821F23413E1AAE8AF7A402B09C9D8146
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D5DBF7686821F23413E1AAE8AF7A402B09C9D8146_21;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-8D2947A8FBC9D80A21D326AA99776795CBA397A6
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D8D2947A8FBC9D80A21D326AA99776795CBA397A6_22;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-7FB6B136550965286B96948AAF7CDC523BDE616B
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D7FB6B136550965286B96948AAF7CDC523BDE616B_23;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-25AC842A810AAB8E814EF9790AE6B3A4D308A6AB
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D25AC842A810AAB8E814EF9790AE6B3A4D308A6AB_24;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-5A349BB4A3D6CA8C50495829F97677BD24485ED5
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D5A349BB4A3D6CA8C50495829F97677BD24485ED5_25;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-5A39C6D3B7176F7D64895BCE1D0626701B464574
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D5A39C6D3B7176F7D64895BCE1D0626701B464574_26;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-C3C69E342B9530497D4B3C71703D4D173F41E821
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DC3C69E342B9530497D4B3C71703D4D173F41E821_27;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-A5F1D7C6F187BBF6A2D1F8DF463AB900CEF4DB13
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DA5F1D7C6F187BBF6A2D1F8DF463AB900CEF4DB13_28;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-7B8E1626915C75E04FD306C808857E4B49CBED19
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D7B8E1626915C75E04FD306C808857E4B49CBED19_29;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-6AD3CA70A847F1E1D25AD94EE69F696A4EF4C1A2
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D6AD3CA70A847F1E1D25AD94EE69F696A4EF4C1A2_30;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-AD198C361B74FABBD2A491412784194700500496
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DAD198C361B74FABBD2A491412784194700500496_31;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-C8172F3DC319ED9F9E4AD940E4E3A172BB870BC6
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DC8172F3DC319ED9F9E4AD940E4E3A172BB870BC6_32;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-C69FBB5DF53AD568630EE6742E3E8AB052C3144F
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DC69FBB5DF53AD568630EE6742E3E8AB052C3144F_33;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-9B2219C436BB44093B17B05D7C37A95D732BDF1A
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D9B2219C436BB44093B17B05D7C37A95D732BDF1A_34;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-52534D8994C76421C9DFA10C8E8A343500B12F38
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D52534D8994C76421C9DFA10C8E8A343500B12F38_35;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-525F126E43DA5737F65A5AD29C20B79DE19010A6
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D525F126E43DA5737F65A5AD29C20B79DE19010A6_36;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-7B5372F481A98563710C701B145AADD910A18C91
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D7B5372F481A98563710C701B145AADD910A18C91_37;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-B7C9C48891656F222702DCE29351381A7D30D3E7
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DB7C9C48891656F222702DCE29351381A7D30D3E7_38;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-D94579B1058818D44D4310948B7C46070767536D
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DD94579B1058818D44D4310948B7C46070767536D_39;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-AF3BA40B1D20EEA4FD9D64C9BAF514D5EF8B2A3C
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DAF3BA40B1D20EEA4FD9D64C9BAF514D5EF8B2A3C_40;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-E8E26DD65BF96015D3F99B815805F6C58C21B23E
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DE8E26DD65BF96015D3F99B815805F6C58C21B23E_41;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-38A81B10112EBC8F090274B9571D818D8F7E3D2C
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D38A81B10112EBC8F090274B9571D818D8F7E3D2C_42;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-1E55BBA47A1186A42E6BBA84D30C72C932A811D3
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D1E55BBA47A1186A42E6BBA84D30C72C932A811D3_43;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-1C4E377322BB013A6F15E832A61BB098615DF0B8
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D1C4E377322BB013A6F15E832A61BB098615DF0B8_44;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-9C4A054E93EF5C04D40B82BEADCFAD5CF4D86DEA
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D9C4A054E93EF5C04D40B82BEADCFAD5CF4D86DEA_45;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-CFA2FBADAA7ADDA055EA831A821AEDB1D14BAB9D
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DCFA2FBADAA7ADDA055EA831A821AEDB1D14BAB9D_46;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-60323ABD2D7C22F62E865DF1F748CF1AF8AD9B7F
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D60323ABD2D7C22F62E865DF1F748CF1AF8AD9B7F_47;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-64DE266A45F5F7D5898FA6A7529673C84D5BA650
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D64DE266A45F5F7D5898FA6A7529673C84D5BA650_48;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-B3A75CC32F78173CF10F472B21A54DAFAAA20F32
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DB3A75CC32F78173CF10F472B21A54DAFAAA20F32_49;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-DA8B2917B2EEA744BF4521F61E17227E77F19519
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DDA8B2917B2EEA744BF4521F61E17227E77F19519_50;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-D1B8C323C9E44C56DDF066DCFA8FD6B89AE29B4D
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DD1B8C323C9E44C56DDF066DCFA8FD6B89AE29B4D_51;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-ADDDB448B434C9ECFE51345E3CC896D2A7FEA1AD
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DADDDB448B434C9ECFE51345E3CC896D2A7FEA1AD_52;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-4FDE5D193D740FA6EDE761A161800A8AFFCEA9BD
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D4FDE5D193D740FA6EDE761A161800A8AFFCEA9BD_53;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-AF9989AA1811EBBCBC515B53CAE9AE32F6A8C504
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DAF9989AA1811EBBCBC515B53CAE9AE32F6A8C504_54;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-85677C568E01B93F32A92ABE48155AD70F976DAD
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D85677C568E01B93F32A92ABE48155AD70F976DAD_55;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-4C1D6C591828D866127110DDE812D542CC9331BC
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D4C1D6C591828D866127110DDE812D542CC9331BC_56;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-73C9519F21426AFF18B3E246AB329D7081D74629
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D73C9519F21426AFF18B3E246AB329D7081D74629_57;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-F01D68EF083172EDC245696B4DFB5B813C4C40D6
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DF01D68EF083172EDC245696B4DFB5B813C4C40D6_58;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-857FADA51A4C3CCFFB1E9A68291CC4C0444975B4
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D857FADA51A4C3CCFFB1E9A68291CC4C0444975B4_59;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-5AD50533400F104F24486A35E159B3A7414E6984
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D5AD50533400F104F24486A35E159B3A7414E6984_60;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-649374558406F6736E729396840BC136C2B5BFD9
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D649374558406F6736E729396840BC136C2B5BFD9_61;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-B7EB9205D5C176BD8A77FBE82679E8CC0D7E5FA8
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DB7EB9205D5C176BD8A77FBE82679E8CC0D7E5FA8_62;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-7190DEAEAB70C823A76B61E3789D9685510816ED
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D7190DEAEAB70C823A76B61E3789D9685510816ED_63;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-7C93AB5D9B0DD4DE43D8459D61FEAB642F5718EB
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D7C93AB5D9B0DD4DE43D8459D61FEAB642F5718EB_64;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-94776C9848170581D079C920A080F505862BA5F5
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D94776C9848170581D079C920A080F505862BA5F5_65;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-B8DD06F686E2962EFFE0C0135249BA1EEBD189F2
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DB8DD06F686E2962EFFE0C0135249BA1EEBD189F2_66;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-27888A1263310BD05ADF3C66E433E62AC22837D4
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D27888A1263310BD05ADF3C66E433E62AC22837D4_67;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-43E7F55B82FD915DDC4D5C8443D0BBB9D27CC671
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D43E7F55B82FD915DDC4D5C8443D0BBB9D27CC671_68;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-567BDB060ABADAA9816DE75D30D09ECBDBAFBB37
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D567BDB060ABADAA9816DE75D30D09ECBDBAFBB37_69;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-3B7119575F4FB85ACD75255CE7CE86293C11DCE7
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D3B7119575F4FB85ACD75255CE7CE86293C11DCE7_70;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-ED9CB0715AFCEA441D9FCF2208BF8FCCC2174D05
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DED9CB0715AFCEA441D9FCF2208BF8FCCC2174D05_71;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-A17570D5B6C2DAAE95E31C43B263CE1D47082979
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DA17570D5B6C2DAAE95E31C43B263CE1D47082979_72;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-96B97D48A5B812040C8C4972167A1D73D48C17C1
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D96B97D48A5B812040C8C4972167A1D73D48C17C1_73;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-3A99CC082D9B62CBA73369CD8566DC6D2D3CE56B
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D3A99CC082D9B62CBA73369CD8566DC6D2D3CE56B_74;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-0BAAEA820015031AACD0305342ACA2F73FE000BF
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D0BAAEA820015031AACD0305342ACA2F73FE000BF_75;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-1FF2C4D3129A713375C286776E2A047579F3B460
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D1FF2C4D3129A713375C286776E2A047579F3B460_76;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-7D6AC3825D861BDCAF6E43C0F58F89B8E92AC302
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D7D6AC3825D861BDCAF6E43C0F58F89B8E92AC302_77;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-28888B3CDD7E5D6AA2A5A4FDDC2CC08247D97BE8
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D28888B3CDD7E5D6AA2A5A4FDDC2CC08247D97BE8_78;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-D7B1D09721FE120EEE473015E875CA4C36093305
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DD7B1D09721FE120EEE473015E875CA4C36093305_79;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-DF301B5B93AC5A743ADF90679C1E31A3E5DF913C
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DDF301B5B93AC5A743ADF90679C1E31A3E5DF913C_80;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-FBBD0BE893F85B13E4036721A87894B24985AB5F
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DFBBD0BE893F85B13E4036721A87894B24985AB5F_81;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-C3F4AC9D069D1C1AAF76D0F721BC71CE766F9C9C
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DC3F4AC9D069D1C1AAF76D0F721BC71CE766F9C9C_82;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-D69752D8BDA7699A11F2988E209F862C81080672
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DD69752D8BDA7699A11F2988E209F862C81080672_83;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-87A054A90E6EE560D4055BFCC8F875CE2A532AA9
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D87A054A90E6EE560D4055BFCC8F875CE2A532AA9_84;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-22DCDD3F71C792BEE79BF79657972F19E8503C28
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D22DCDD3F71C792BEE79BF79657972F19E8503C28_85;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-19801FABF900B8A91B3B7A8C19554CF21938A328
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D19801FABF900B8A91B3B7A8C19554CF21938A328_86;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-D3198CF22F3142E548C0ADEEBCBA1A1DF6C1FA76
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DD3198CF22F3142E548C0ADEEBCBA1A1DF6C1FA76_87;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-626E626D6BCD38FA699B0752E2D91A81B8CFD074
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D626E626D6BCD38FA699B0752E2D91A81B8CFD074_88;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-2841ABEC55FB3862F45BA8E5BD854FECB89B2C0B
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D2841ABEC55FB3862F45BA8E5BD854FECB89B2C0B_89;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-0BDDE6F0882E6AD06D0FECC073EB61DC8779A234
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D0BDDE6F0882E6AD06D0FECC073EB61DC8779A234_90;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-DED837EA58E0B97FD24A53995FCEC1D3A8414EEC
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DDED837EA58E0B97FD24A53995FCEC1D3A8414EEC_91;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-62949AE93481AD89B9E39B9A17AED79A75EF656F
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D62949AE93481AD89B9E39B9A17AED79A75EF656F_92;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-188D091BDAA625C458629AFB438C9CF1110A5749
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D188D091BDAA625C458629AFB438C9CF1110A5749_93;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-E5E7D260D4BD2EF3D7E754EFCDB39BDC6886040C
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DE5E7D260D4BD2EF3D7E754EFCDB39BDC6886040C_94;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-C0F8B663B994BB22A0819A77DC709EB6734A86EF
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DC0F8B663B994BB22A0819A77DC709EB6734A86EF_95;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-472AB78FCC6457EB1051AB776C54D1B5479AD3A2
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D472AB78FCC6457EB1051AB776C54D1B5479AD3A2_96;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-6CFD6E9B2590DEC938E2F8711AFFA7803B7789D3
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D6CFD6E9B2590DEC938E2F8711AFFA7803B7789D3_97;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-9AD10A6B52DCB7C2044C028935E73252BCF32185
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D9AD10A6B52DCB7C2044C028935E73252BCF32185_98;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-D0410F28A9A96BD53F0C8CFD98F9A0D450FA002F
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DD0410F28A9A96BD53F0C8CFD98F9A0D450FA002F_99;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-B8400EAE6B6E484B58EE5FDA5240F2235204F77A
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DB8400EAE6B6E484B58EE5FDA5240F2235204F77A_100;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-7A03E895CDEE6AF52BE3ECF5383175E0F218584C
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D7A03E895CDEE6AF52BE3ECF5383175E0F218584C_101;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-0D7536E292838F2850B78C40CE965EF0FB40CB93
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D0D7536E292838F2850B78C40CE965EF0FB40CB93_102;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-6E84A598EB70DF96202A625C63B66975EED796EE
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D6E84A598EB70DF96202A625C63B66975EED796EE_103;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-11363D33BC05212CEC6843E96E66163F908899C3
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D11363D33BC05212CEC6843E96E66163F908899C3_104;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-614578BC1B89AB094AC64BFBA9BCC07CFE7A5C52
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D614578BC1B89AB094AC64BFBA9BCC07CFE7A5C52_105;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-C4D32E400AA14406F6E11289FC511F0A2BFB765E
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DC4D32E400AA14406F6E11289FC511F0A2BFB765E_106;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-99380A2C88E75E6C9171C58173F9995E7CB0DC96
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D99380A2C88E75E6C9171C58173F9995E7CB0DC96_107;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-D921D1E53ACB96FA59820910A1C6957F797725FB
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DD921D1E53ACB96FA59820910A1C6957F797725FB_108;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-FE8385470E173E2E7C2D9D94D9F81163CA600F0D
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DFE8385470E173E2E7C2D9D94D9F81163CA600F0D_109;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-68845C029EE5C0E5EC8821721F587C33BB9284E8
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D68845C029EE5C0E5EC8821721F587C33BB9284E8_110;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-9AB51121DB726549C24CD8652D05D2279597A826
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D9AB51121DB726549C24CD8652D05D2279597A826_111;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-5A66B903EF5A119B6BADBE48C36C6F935D87D3A8
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D5A66B903EF5A119B6BADBE48C36C6F935D87D3A8_112;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-0AB03169304F60655F2A2014ED877F0B4F8F86E3
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D0AB03169304F60655F2A2014ED877F0B4F8F86E3_113;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-A41A4536581FF8C104E66759035DFB236DFF0994
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DA41A4536581FF8C104E66759035DFB236DFF0994_114;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-87FA64DAA4DA09B4C03E8060CBABE9BF63DFA0E6
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D87FA64DAA4DA09B4C03E8060CBABE9BF63DFA0E6_115;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-75C89CF0447460CFB3ABAACBC205DC204F823B77
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D75C89CF0447460CFB3ABAACBC205DC204F823B77_116;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-20A70D944EED422690C9BA10EF51A59A6A9B603A
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D20A70D944EED422690C9BA10EF51A59A6A9B603A_117;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-970D607246F4725BCA45E7564DBC8E90DF8114B5
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D970D607246F4725BCA45E7564DBC8E90DF8114B5_118;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-714E5457816BBE2A319AAC07D2C587A676A85EF5
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D714E5457816BBE2A319AAC07D2C587A676A85EF5_119;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-BCC954BE08EE00F495F849D2F161752479934142
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DBCC954BE08EE00F495F849D2F161752479934142_120;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-F2CBB55BC65F72A214C8CF9323FBED9861F2F4BC
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DF2CBB55BC65F72A214C8CF9323FBED9861F2F4BC_121;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-1CDE377C407F39C592524FDAFE062059B9165E3F
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D1CDE377C407F39C592524FDAFE062059B9165E3F_122;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-6BECC23C94DC5127540ABE6CE7BE88684C295C5D
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D6BECC23C94DC5127540ABE6CE7BE88684C295C5D_123;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-928F91AB9C19903D2B1009F932437DFCA5CDC584
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D928F91AB9C19903D2B1009F932437DFCA5CDC584_124;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-F83C96E93F812D711747D3CC98683620B651BC5C
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DF83C96E93F812D711747D3CC98683620B651BC5C_125;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-12A155BF457332BF81F6684CCE56CC9C2A3A5076
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D12A155BF457332BF81F6684CCE56CC9C2A3A5076_126;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-0D1AE827D0A9B6FD7B028D3A6670BF59BCA86701
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D0D1AE827D0A9B6FD7B028D3A6670BF59BCA86701_127;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-6E2460C8BE8220FB196B278A5D70BC3556975E55
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D6E2460C8BE8220FB196B278A5D70BC3556975E55_128;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-48322419F79C852FFF1227DCB802B7C8CE2236EB
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D48322419F79C852FFF1227DCB802B7C8CE2236EB_129;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-DE4B828B867A48EAB4617DC73A150B02BD24E72C
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DDE4B828B867A48EAB4617DC73A150B02BD24E72C_130;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-CD6B5BC785A3A5AFE10307374DB684F6132B1C57
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DCD6B5BC785A3A5AFE10307374DB684F6132B1C57_131;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-C420C0CA6FC58CFAADB02A5DA53391092FBB894A
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DC420C0CA6FC58CFAADB02A5DA53391092FBB894A_132;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-E636A7059162508F5B0029ABA2C0826FBC218247
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DE636A7059162508F5B0029ABA2C0826FBC218247_133;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-D6D3C6667F50B055D22DD3D8FAE411326678A71F
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DD6D3C6667F50B055D22DD3D8FAE411326678A71F_134;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-CD7F453E7DAD8B3B8D8AB82971B07921BFC4A3D3
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DCD7F453E7DAD8B3B8D8AB82971B07921BFC4A3D3_135;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-E6570482AA640720A812DC9F4302ACCFF92B93F1
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DE6570482AA640720A812DC9F4302ACCFF92B93F1_136;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-CEAA201847F942506CE4A6D33EB8D87AF00B4AA0
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DCEAA201847F942506CE4A6D33EB8D87AF00B4AA0_137;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-242F9F981726198546AE22426078FC2D0AFDFC7D
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D242F9F981726198546AE22426078FC2D0AFDFC7D_138;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-D178CEED7BBEBC7AA0DA1484363B0ADC369532B1
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DD178CEED7BBEBC7AA0DA1484363B0ADC369532B1_139;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-535985E95CA011ACE58FC3BEE4E4270F4F761478
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D535985E95CA011ACE58FC3BEE4E4270F4F761478_140;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-8BD44E6B764A64B6DE9F115D610775748364EB71
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D8BD44E6B764A64B6DE9F115D610775748364EB71_141;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-74BBA41B92CCB948BD0B17E1D9DE0DA8BEFE2304
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D74BBA41B92CCB948BD0B17E1D9DE0DA8BEFE2304_142;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-52DD1DEF5C43780A82B460C7C23188B55B918057
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D52DD1DEF5C43780A82B460C7C23188B55B918057_143;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-AB8935CA29ED45F3F387F2EB8823E19EC88661DA
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DAB8935CA29ED45F3F387F2EB8823E19EC88661DA_144;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-E2EC72AD9A1FECD786D35F2F342F03AD95B14DD4
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DE2EC72AD9A1FECD786D35F2F342F03AD95B14DD4_145;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-630C3F5D7DBB07B36134EB1AFA096BAA59350F0C
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D630C3F5D7DBB07B36134EB1AFA096BAA59350F0C_146;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-D26254A9468BBBB28AC07899A05D759FD80FE5C5
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DD26254A9468BBBB28AC07899A05D759FD80FE5C5_147;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-8DC2ABCA7A46A411D39035D8806FBA7EB22432FB
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D8DC2ABCA7A46A411D39035D8806FBA7EB22432FB_148;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-B75CE37738CED7D6746D306511B391A9F91684D0
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DB75CE37738CED7D6746D306511B391A9F91684D0_149;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-980BDEDFAACFEB591A358FAFC522C3233E8414D8
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D980BDEDFAACFEB591A358FAFC522C3233E8414D8_150;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-3DFDD69B416E892411930CA44E469FC0EE1A748E
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D3DFDD69B416E892411930CA44E469FC0EE1A748E_151;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-9A7664FF66C09B23E2674031BADDADA42C2E7338
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D9A7664FF66C09B23E2674031BADDADA42C2E7338_152;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-950965F7AE6D3878D95DB10B185EC9969281928B
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D950965F7AE6D3878D95DB10B185EC9969281928B_153;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-E37060512A6142D8BDB50F96F5A31B3F7FD9C811
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DE37060512A6142D8BDB50F96F5A31B3F7FD9C811_154;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-CFD89EB7C35C2C4305E84198CEE1E467C5B685FC
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DCFD89EB7C35C2C4305E84198CEE1E467C5B685FC_155;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-E4692158166F085D6DB6AA88DD688573157DDE59
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DE4692158166F085D6DB6AA88DD688573157DDE59_156;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-E3B39965C126C790271ABFAAC5BAF0CB3544F01C
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DE3B39965C126C790271ABFAAC5BAF0CB3544F01C_157;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-E69D5483E961E76DC16F84DD4D0CF8A1E8AEBFED
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2DE69D5483E961E76DC16F84DD4D0CF8A1E8AEBFED_158;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-263ADBD7E0DAA10E11AAF3AA902BE6511DBEA61F
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D263ADBD7E0DAA10E11AAF3AA902BE6511DBEA61F_159;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-823ECB532404CAC8E6628819EAB525132A3C4CC3
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D823ECB532404CAC8E6628819EAB525132A3C4CC3_160;
	// <PrivateImplementationDetails>/$ArrayType=56 <PrivateImplementationDetails>::$field-66175F96A3A979C0479D04059A97B4F0D1545770
	U24ArrayTypeU3D56_t1283759776  ___U24fieldU2D66175F96A3A979C0479D04059A97B4F0D1545770_161;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-A5E465DCCD6CF445B12C62AFC303C659C6E78B26
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2DA5E465DCCD6CF445B12C62AFC303C659C6E78B26_162;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-B14CD063151CEC59EEB0F8E2557A7CF6B9D833BA
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2DB14CD063151CEC59EEB0F8E2557A7CF6B9D833BA_163;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-62EBB675E18E46D952C37A995181D23A18F84CD4
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D62EBB675E18E46D952C37A995181D23A18F84CD4_164;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-5F214E20357C7CC02D9BB842C0626F580CA6200E
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D5F214E20357C7CC02D9BB842C0626F580CA6200E_165;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-8834238153C27D69F7A59F6EAE1405F77FE45409
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D8834238153C27D69F7A59F6EAE1405F77FE45409_166;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-149F522CB3B9C44EB981E8BC2540D606F1CDCD87
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D149F522CB3B9C44EB981E8BC2540D606F1CDCD87_167;
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-231A0B5CEF9DCFCB9D64B36C1F48EFABC84C7F49
	U24ArrayTypeU3D32_t3651253610  ___U24fieldU2D231A0B5CEF9DCFCB9D64B36C1F48EFABC84C7F49_168;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-E429CCA3F703A39CC5954A6572FEC9086135B34E
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_169;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-8CFA957D76B6E190580D284C12F31AA6E3E2D41C
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D8CFA957D76B6E190580D284C12F31AA6E3E2D41C_170;

public:
	inline static int32_t get_offset_of_U24fieldU2DA2F1578C0F6612632B975A3AFE7CB9184E9F5590_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DA2F1578C0F6612632B975A3AFE7CB9184E9F5590_0)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2DA2F1578C0F6612632B975A3AFE7CB9184E9F5590_0() const { return ___U24fieldU2DA2F1578C0F6612632B975A3AFE7CB9184E9F5590_0; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2DA2F1578C0F6612632B975A3AFE7CB9184E9F5590_0() { return &___U24fieldU2DA2F1578C0F6612632B975A3AFE7CB9184E9F5590_0; }
	inline void set_U24fieldU2DA2F1578C0F6612632B975A3AFE7CB9184E9F5590_0(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2DA2F1578C0F6612632B975A3AFE7CB9184E9F5590_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D81AFD0604569DA0255357C175BE345F58F17D478_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D81AFD0604569DA0255357C175BE345F58F17D478_1)); }
	inline U24ArrayTypeU3D208_t3449392908  get_U24fieldU2D81AFD0604569DA0255357C175BE345F58F17D478_1() const { return ___U24fieldU2D81AFD0604569DA0255357C175BE345F58F17D478_1; }
	inline U24ArrayTypeU3D208_t3449392908 * get_address_of_U24fieldU2D81AFD0604569DA0255357C175BE345F58F17D478_1() { return &___U24fieldU2D81AFD0604569DA0255357C175BE345F58F17D478_1; }
	inline void set_U24fieldU2D81AFD0604569DA0255357C175BE345F58F17D478_1(U24ArrayTypeU3D208_t3449392908  value)
	{
		___U24fieldU2D81AFD0604569DA0255357C175BE345F58F17D478_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D380E84549CB845604C318E8E14B73622CC10AF42_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D380E84549CB845604C318E8E14B73622CC10AF42_2)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D380E84549CB845604C318E8E14B73622CC10AF42_2() const { return ___U24fieldU2D380E84549CB845604C318E8E14B73622CC10AF42_2; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D380E84549CB845604C318E8E14B73622CC10AF42_2() { return &___U24fieldU2D380E84549CB845604C318E8E14B73622CC10AF42_2; }
	inline void set_U24fieldU2D380E84549CB845604C318E8E14B73622CC10AF42_2(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D380E84549CB845604C318E8E14B73622CC10AF42_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D963821E4BFFA8EED3A18D7173F2147779057B9B3_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D963821E4BFFA8EED3A18D7173F2147779057B9B3_3)); }
	inline U24ArrayTypeU3D64_t498138225  get_U24fieldU2D963821E4BFFA8EED3A18D7173F2147779057B9B3_3() const { return ___U24fieldU2D963821E4BFFA8EED3A18D7173F2147779057B9B3_3; }
	inline U24ArrayTypeU3D64_t498138225 * get_address_of_U24fieldU2D963821E4BFFA8EED3A18D7173F2147779057B9B3_3() { return &___U24fieldU2D963821E4BFFA8EED3A18D7173F2147779057B9B3_3; }
	inline void set_U24fieldU2D963821E4BFFA8EED3A18D7173F2147779057B9B3_3(U24ArrayTypeU3D64_t498138225  value)
	{
		___U24fieldU2D963821E4BFFA8EED3A18D7173F2147779057B9B3_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD4EF3A9F65679752698916F2F8DBCD78EE2E41B8_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD4EF3A9F65679752698916F2F8DBCD78EE2E41B8_4)); }
	inline U24ArrayTypeU3D64_t498138225  get_U24fieldU2DD4EF3A9F65679752698916F2F8DBCD78EE2E41B8_4() const { return ___U24fieldU2DD4EF3A9F65679752698916F2F8DBCD78EE2E41B8_4; }
	inline U24ArrayTypeU3D64_t498138225 * get_address_of_U24fieldU2DD4EF3A9F65679752698916F2F8DBCD78EE2E41B8_4() { return &___U24fieldU2DD4EF3A9F65679752698916F2F8DBCD78EE2E41B8_4; }
	inline void set_U24fieldU2DD4EF3A9F65679752698916F2F8DBCD78EE2E41B8_4(U24ArrayTypeU3D64_t498138225  value)
	{
		___U24fieldU2DD4EF3A9F65679752698916F2F8DBCD78EE2E41B8_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1CA1C79F41AEC3E78B9270CE6E221118308DFFD5_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D1CA1C79F41AEC3E78B9270CE6E221118308DFFD5_5)); }
	inline U24ArrayTypeU3D64_t498138225  get_U24fieldU2D1CA1C79F41AEC3E78B9270CE6E221118308DFFD5_5() const { return ___U24fieldU2D1CA1C79F41AEC3E78B9270CE6E221118308DFFD5_5; }
	inline U24ArrayTypeU3D64_t498138225 * get_address_of_U24fieldU2D1CA1C79F41AEC3E78B9270CE6E221118308DFFD5_5() { return &___U24fieldU2D1CA1C79F41AEC3E78B9270CE6E221118308DFFD5_5; }
	inline void set_U24fieldU2D1CA1C79F41AEC3E78B9270CE6E221118308DFFD5_5(U24ArrayTypeU3D64_t498138225  value)
	{
		___U24fieldU2D1CA1C79F41AEC3E78B9270CE6E221118308DFFD5_5 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1DA0256192B60CA2089E8E33FA9F10415D904C9B_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D1DA0256192B60CA2089E8E33FA9F10415D904C9B_6)); }
	inline U24ArrayTypeU3D64_t498138225  get_U24fieldU2D1DA0256192B60CA2089E8E33FA9F10415D904C9B_6() const { return ___U24fieldU2D1DA0256192B60CA2089E8E33FA9F10415D904C9B_6; }
	inline U24ArrayTypeU3D64_t498138225 * get_address_of_U24fieldU2D1DA0256192B60CA2089E8E33FA9F10415D904C9B_6() { return &___U24fieldU2D1DA0256192B60CA2089E8E33FA9F10415D904C9B_6; }
	inline void set_U24fieldU2D1DA0256192B60CA2089E8E33FA9F10415D904C9B_6(U24ArrayTypeU3D64_t498138225  value)
	{
		___U24fieldU2D1DA0256192B60CA2089E8E33FA9F10415D904C9B_6 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D49610B6D06207419DD075235B5603F1860DDFB14_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D49610B6D06207419DD075235B5603F1860DDFB14_7)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D49610B6D06207419DD075235B5603F1860DDFB14_7() const { return ___U24fieldU2D49610B6D06207419DD075235B5603F1860DDFB14_7; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D49610B6D06207419DD075235B5603F1860DDFB14_7() { return &___U24fieldU2D49610B6D06207419DD075235B5603F1860DDFB14_7; }
	inline void set_U24fieldU2D49610B6D06207419DD075235B5603F1860DDFB14_7(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D49610B6D06207419DD075235B5603F1860DDFB14_7 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D949FFA4D55D6CAF924550A0169CBEEF3F51C7F2A_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D949FFA4D55D6CAF924550A0169CBEEF3F51C7F2A_8)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D949FFA4D55D6CAF924550A0169CBEEF3F51C7F2A_8() const { return ___U24fieldU2D949FFA4D55D6CAF924550A0169CBEEF3F51C7F2A_8; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D949FFA4D55D6CAF924550A0169CBEEF3F51C7F2A_8() { return &___U24fieldU2D949FFA4D55D6CAF924550A0169CBEEF3F51C7F2A_8; }
	inline void set_U24fieldU2D949FFA4D55D6CAF924550A0169CBEEF3F51C7F2A_8(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D949FFA4D55D6CAF924550A0169CBEEF3F51C7F2A_8 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DEC16EE82A2343F7D332375CE590AFA11FFC6C5F0_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DEC16EE82A2343F7D332375CE590AFA11FFC6C5F0_9)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2DEC16EE82A2343F7D332375CE590AFA11FFC6C5F0_9() const { return ___U24fieldU2DEC16EE82A2343F7D332375CE590AFA11FFC6C5F0_9; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2DEC16EE82A2343F7D332375CE590AFA11FFC6C5F0_9() { return &___U24fieldU2DEC16EE82A2343F7D332375CE590AFA11FFC6C5F0_9; }
	inline void set_U24fieldU2DEC16EE82A2343F7D332375CE590AFA11FFC6C5F0_9(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2DEC16EE82A2343F7D332375CE590AFA11FFC6C5F0_9 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2223729B78EBEE4B2E434CA9BAF047311413D5C0_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D2223729B78EBEE4B2E434CA9BAF047311413D5C0_10)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D2223729B78EBEE4B2E434CA9BAF047311413D5C0_10() const { return ___U24fieldU2D2223729B78EBEE4B2E434CA9BAF047311413D5C0_10; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D2223729B78EBEE4B2E434CA9BAF047311413D5C0_10() { return &___U24fieldU2D2223729B78EBEE4B2E434CA9BAF047311413D5C0_10; }
	inline void set_U24fieldU2D2223729B78EBEE4B2E434CA9BAF047311413D5C0_10(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D2223729B78EBEE4B2E434CA9BAF047311413D5C0_10 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D529B72F43D5DB46080436118B23FCEA9D7F66A32_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D529B72F43D5DB46080436118B23FCEA9D7F66A32_11)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D529B72F43D5DB46080436118B23FCEA9D7F66A32_11() const { return ___U24fieldU2D529B72F43D5DB46080436118B23FCEA9D7F66A32_11; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D529B72F43D5DB46080436118B23FCEA9D7F66A32_11() { return &___U24fieldU2D529B72F43D5DB46080436118B23FCEA9D7F66A32_11; }
	inline void set_U24fieldU2D529B72F43D5DB46080436118B23FCEA9D7F66A32_11(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D529B72F43D5DB46080436118B23FCEA9D7F66A32_11 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D46284D0F43072429162AA2925082FFAC2C0DE31A_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D46284D0F43072429162AA2925082FFAC2C0DE31A_12)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D46284D0F43072429162AA2925082FFAC2C0DE31A_12() const { return ___U24fieldU2D46284D0F43072429162AA2925082FFAC2C0DE31A_12; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D46284D0F43072429162AA2925082FFAC2C0DE31A_12() { return &___U24fieldU2D46284D0F43072429162AA2925082FFAC2C0DE31A_12; }
	inline void set_U24fieldU2D46284D0F43072429162AA2925082FFAC2C0DE31A_12(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D46284D0F43072429162AA2925082FFAC2C0DE31A_12 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9EDEF80E922E3463CA046C98E9CFF518CFD1FECA_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D9EDEF80E922E3463CA046C98E9CFF518CFD1FECA_13)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D9EDEF80E922E3463CA046C98E9CFF518CFD1FECA_13() const { return ___U24fieldU2D9EDEF80E922E3463CA046C98E9CFF518CFD1FECA_13; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D9EDEF80E922E3463CA046C98E9CFF518CFD1FECA_13() { return &___U24fieldU2D9EDEF80E922E3463CA046C98E9CFF518CFD1FECA_13; }
	inline void set_U24fieldU2D9EDEF80E922E3463CA046C98E9CFF518CFD1FECA_13(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D9EDEF80E922E3463CA046C98E9CFF518CFD1FECA_13 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D82091C20834853F435A614D3DCBC5AD6D9FEF27F_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D82091C20834853F435A614D3DCBC5AD6D9FEF27F_14)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D82091C20834853F435A614D3DCBC5AD6D9FEF27F_14() const { return ___U24fieldU2D82091C20834853F435A614D3DCBC5AD6D9FEF27F_14; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D82091C20834853F435A614D3DCBC5AD6D9FEF27F_14() { return &___U24fieldU2D82091C20834853F435A614D3DCBC5AD6D9FEF27F_14; }
	inline void set_U24fieldU2D82091C20834853F435A614D3DCBC5AD6D9FEF27F_14(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D82091C20834853F435A614D3DCBC5AD6D9FEF27F_14 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC66129415B3CAF838A8150A682848D3F4ACAAFC1_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DC66129415B3CAF838A8150A682848D3F4ACAAFC1_15)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DC66129415B3CAF838A8150A682848D3F4ACAAFC1_15() const { return ___U24fieldU2DC66129415B3CAF838A8150A682848D3F4ACAAFC1_15; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DC66129415B3CAF838A8150A682848D3F4ACAAFC1_15() { return &___U24fieldU2DC66129415B3CAF838A8150A682848D3F4ACAAFC1_15; }
	inline void set_U24fieldU2DC66129415B3CAF838A8150A682848D3F4ACAAFC1_15(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DC66129415B3CAF838A8150A682848D3F4ACAAFC1_15 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6634EA9EA45C5BDEBFF437FC9807140E0BFA58AC_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D6634EA9EA45C5BDEBFF437FC9807140E0BFA58AC_16)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D6634EA9EA45C5BDEBFF437FC9807140E0BFA58AC_16() const { return ___U24fieldU2D6634EA9EA45C5BDEBFF437FC9807140E0BFA58AC_16; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D6634EA9EA45C5BDEBFF437FC9807140E0BFA58AC_16() { return &___U24fieldU2D6634EA9EA45C5BDEBFF437FC9807140E0BFA58AC_16; }
	inline void set_U24fieldU2D6634EA9EA45C5BDEBFF437FC9807140E0BFA58AC_16(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D6634EA9EA45C5BDEBFF437FC9807140E0BFA58AC_16 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB2ABE92600B53F2B8A03B9A9E2C734A8BE0C6415_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DB2ABE92600B53F2B8A03B9A9E2C734A8BE0C6415_17)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DB2ABE92600B53F2B8A03B9A9E2C734A8BE0C6415_17() const { return ___U24fieldU2DB2ABE92600B53F2B8A03B9A9E2C734A8BE0C6415_17; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DB2ABE92600B53F2B8A03B9A9E2C734A8BE0C6415_17() { return &___U24fieldU2DB2ABE92600B53F2B8A03B9A9E2C734A8BE0C6415_17; }
	inline void set_U24fieldU2DB2ABE92600B53F2B8A03B9A9E2C734A8BE0C6415_17(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DB2ABE92600B53F2B8A03B9A9E2C734A8BE0C6415_17 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA8B6B603D922A9A2949780CB840679B621BF5FFC_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DA8B6B603D922A9A2949780CB840679B621BF5FFC_18)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DA8B6B603D922A9A2949780CB840679B621BF5FFC_18() const { return ___U24fieldU2DA8B6B603D922A9A2949780CB840679B621BF5FFC_18; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DA8B6B603D922A9A2949780CB840679B621BF5FFC_18() { return &___U24fieldU2DA8B6B603D922A9A2949780CB840679B621BF5FFC_18; }
	inline void set_U24fieldU2DA8B6B603D922A9A2949780CB840679B621BF5FFC_18(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DA8B6B603D922A9A2949780CB840679B621BF5FFC_18 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF7079845619834F11587B9DB9E90CC2A70A46537_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DF7079845619834F11587B9DB9E90CC2A70A46537_19)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DF7079845619834F11587B9DB9E90CC2A70A46537_19() const { return ___U24fieldU2DF7079845619834F11587B9DB9E90CC2A70A46537_19; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DF7079845619834F11587B9DB9E90CC2A70A46537_19() { return &___U24fieldU2DF7079845619834F11587B9DB9E90CC2A70A46537_19; }
	inline void set_U24fieldU2DF7079845619834F11587B9DB9E90CC2A70A46537_19(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DF7079845619834F11587B9DB9E90CC2A70A46537_19 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D61070D52649698A08E65825BD36FECC1192D073E_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D61070D52649698A08E65825BD36FECC1192D073E_20)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D61070D52649698A08E65825BD36FECC1192D073E_20() const { return ___U24fieldU2D61070D52649698A08E65825BD36FECC1192D073E_20; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D61070D52649698A08E65825BD36FECC1192D073E_20() { return &___U24fieldU2D61070D52649698A08E65825BD36FECC1192D073E_20; }
	inline void set_U24fieldU2D61070D52649698A08E65825BD36FECC1192D073E_20(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D61070D52649698A08E65825BD36FECC1192D073E_20 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5DBF7686821F23413E1AAE8AF7A402B09C9D8146_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D5DBF7686821F23413E1AAE8AF7A402B09C9D8146_21)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D5DBF7686821F23413E1AAE8AF7A402B09C9D8146_21() const { return ___U24fieldU2D5DBF7686821F23413E1AAE8AF7A402B09C9D8146_21; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D5DBF7686821F23413E1AAE8AF7A402B09C9D8146_21() { return &___U24fieldU2D5DBF7686821F23413E1AAE8AF7A402B09C9D8146_21; }
	inline void set_U24fieldU2D5DBF7686821F23413E1AAE8AF7A402B09C9D8146_21(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D5DBF7686821F23413E1AAE8AF7A402B09C9D8146_21 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8D2947A8FBC9D80A21D326AA99776795CBA397A6_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D8D2947A8FBC9D80A21D326AA99776795CBA397A6_22)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D8D2947A8FBC9D80A21D326AA99776795CBA397A6_22() const { return ___U24fieldU2D8D2947A8FBC9D80A21D326AA99776795CBA397A6_22; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D8D2947A8FBC9D80A21D326AA99776795CBA397A6_22() { return &___U24fieldU2D8D2947A8FBC9D80A21D326AA99776795CBA397A6_22; }
	inline void set_U24fieldU2D8D2947A8FBC9D80A21D326AA99776795CBA397A6_22(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D8D2947A8FBC9D80A21D326AA99776795CBA397A6_22 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7FB6B136550965286B96948AAF7CDC523BDE616B_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7FB6B136550965286B96948AAF7CDC523BDE616B_23)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D7FB6B136550965286B96948AAF7CDC523BDE616B_23() const { return ___U24fieldU2D7FB6B136550965286B96948AAF7CDC523BDE616B_23; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D7FB6B136550965286B96948AAF7CDC523BDE616B_23() { return &___U24fieldU2D7FB6B136550965286B96948AAF7CDC523BDE616B_23; }
	inline void set_U24fieldU2D7FB6B136550965286B96948AAF7CDC523BDE616B_23(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D7FB6B136550965286B96948AAF7CDC523BDE616B_23 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D25AC842A810AAB8E814EF9790AE6B3A4D308A6AB_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D25AC842A810AAB8E814EF9790AE6B3A4D308A6AB_24)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D25AC842A810AAB8E814EF9790AE6B3A4D308A6AB_24() const { return ___U24fieldU2D25AC842A810AAB8E814EF9790AE6B3A4D308A6AB_24; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D25AC842A810AAB8E814EF9790AE6B3A4D308A6AB_24() { return &___U24fieldU2D25AC842A810AAB8E814EF9790AE6B3A4D308A6AB_24; }
	inline void set_U24fieldU2D25AC842A810AAB8E814EF9790AE6B3A4D308A6AB_24(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D25AC842A810AAB8E814EF9790AE6B3A4D308A6AB_24 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5A349BB4A3D6CA8C50495829F97677BD24485ED5_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D5A349BB4A3D6CA8C50495829F97677BD24485ED5_25)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D5A349BB4A3D6CA8C50495829F97677BD24485ED5_25() const { return ___U24fieldU2D5A349BB4A3D6CA8C50495829F97677BD24485ED5_25; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D5A349BB4A3D6CA8C50495829F97677BD24485ED5_25() { return &___U24fieldU2D5A349BB4A3D6CA8C50495829F97677BD24485ED5_25; }
	inline void set_U24fieldU2D5A349BB4A3D6CA8C50495829F97677BD24485ED5_25(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D5A349BB4A3D6CA8C50495829F97677BD24485ED5_25 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5A39C6D3B7176F7D64895BCE1D0626701B464574_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D5A39C6D3B7176F7D64895BCE1D0626701B464574_26)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D5A39C6D3B7176F7D64895BCE1D0626701B464574_26() const { return ___U24fieldU2D5A39C6D3B7176F7D64895BCE1D0626701B464574_26; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D5A39C6D3B7176F7D64895BCE1D0626701B464574_26() { return &___U24fieldU2D5A39C6D3B7176F7D64895BCE1D0626701B464574_26; }
	inline void set_U24fieldU2D5A39C6D3B7176F7D64895BCE1D0626701B464574_26(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D5A39C6D3B7176F7D64895BCE1D0626701B464574_26 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC3C69E342B9530497D4B3C71703D4D173F41E821_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DC3C69E342B9530497D4B3C71703D4D173F41E821_27)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DC3C69E342B9530497D4B3C71703D4D173F41E821_27() const { return ___U24fieldU2DC3C69E342B9530497D4B3C71703D4D173F41E821_27; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DC3C69E342B9530497D4B3C71703D4D173F41E821_27() { return &___U24fieldU2DC3C69E342B9530497D4B3C71703D4D173F41E821_27; }
	inline void set_U24fieldU2DC3C69E342B9530497D4B3C71703D4D173F41E821_27(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DC3C69E342B9530497D4B3C71703D4D173F41E821_27 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA5F1D7C6F187BBF6A2D1F8DF463AB900CEF4DB13_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DA5F1D7C6F187BBF6A2D1F8DF463AB900CEF4DB13_28)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DA5F1D7C6F187BBF6A2D1F8DF463AB900CEF4DB13_28() const { return ___U24fieldU2DA5F1D7C6F187BBF6A2D1F8DF463AB900CEF4DB13_28; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DA5F1D7C6F187BBF6A2D1F8DF463AB900CEF4DB13_28() { return &___U24fieldU2DA5F1D7C6F187BBF6A2D1F8DF463AB900CEF4DB13_28; }
	inline void set_U24fieldU2DA5F1D7C6F187BBF6A2D1F8DF463AB900CEF4DB13_28(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DA5F1D7C6F187BBF6A2D1F8DF463AB900CEF4DB13_28 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7B8E1626915C75E04FD306C808857E4B49CBED19_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7B8E1626915C75E04FD306C808857E4B49CBED19_29)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D7B8E1626915C75E04FD306C808857E4B49CBED19_29() const { return ___U24fieldU2D7B8E1626915C75E04FD306C808857E4B49CBED19_29; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D7B8E1626915C75E04FD306C808857E4B49CBED19_29() { return &___U24fieldU2D7B8E1626915C75E04FD306C808857E4B49CBED19_29; }
	inline void set_U24fieldU2D7B8E1626915C75E04FD306C808857E4B49CBED19_29(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D7B8E1626915C75E04FD306C808857E4B49CBED19_29 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6AD3CA70A847F1E1D25AD94EE69F696A4EF4C1A2_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D6AD3CA70A847F1E1D25AD94EE69F696A4EF4C1A2_30)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D6AD3CA70A847F1E1D25AD94EE69F696A4EF4C1A2_30() const { return ___U24fieldU2D6AD3CA70A847F1E1D25AD94EE69F696A4EF4C1A2_30; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D6AD3CA70A847F1E1D25AD94EE69F696A4EF4C1A2_30() { return &___U24fieldU2D6AD3CA70A847F1E1D25AD94EE69F696A4EF4C1A2_30; }
	inline void set_U24fieldU2D6AD3CA70A847F1E1D25AD94EE69F696A4EF4C1A2_30(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D6AD3CA70A847F1E1D25AD94EE69F696A4EF4C1A2_30 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAD198C361B74FABBD2A491412784194700500496_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DAD198C361B74FABBD2A491412784194700500496_31)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DAD198C361B74FABBD2A491412784194700500496_31() const { return ___U24fieldU2DAD198C361B74FABBD2A491412784194700500496_31; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DAD198C361B74FABBD2A491412784194700500496_31() { return &___U24fieldU2DAD198C361B74FABBD2A491412784194700500496_31; }
	inline void set_U24fieldU2DAD198C361B74FABBD2A491412784194700500496_31(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DAD198C361B74FABBD2A491412784194700500496_31 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC8172F3DC319ED9F9E4AD940E4E3A172BB870BC6_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DC8172F3DC319ED9F9E4AD940E4E3A172BB870BC6_32)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DC8172F3DC319ED9F9E4AD940E4E3A172BB870BC6_32() const { return ___U24fieldU2DC8172F3DC319ED9F9E4AD940E4E3A172BB870BC6_32; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DC8172F3DC319ED9F9E4AD940E4E3A172BB870BC6_32() { return &___U24fieldU2DC8172F3DC319ED9F9E4AD940E4E3A172BB870BC6_32; }
	inline void set_U24fieldU2DC8172F3DC319ED9F9E4AD940E4E3A172BB870BC6_32(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DC8172F3DC319ED9F9E4AD940E4E3A172BB870BC6_32 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC69FBB5DF53AD568630EE6742E3E8AB052C3144F_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DC69FBB5DF53AD568630EE6742E3E8AB052C3144F_33)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DC69FBB5DF53AD568630EE6742E3E8AB052C3144F_33() const { return ___U24fieldU2DC69FBB5DF53AD568630EE6742E3E8AB052C3144F_33; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DC69FBB5DF53AD568630EE6742E3E8AB052C3144F_33() { return &___U24fieldU2DC69FBB5DF53AD568630EE6742E3E8AB052C3144F_33; }
	inline void set_U24fieldU2DC69FBB5DF53AD568630EE6742E3E8AB052C3144F_33(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DC69FBB5DF53AD568630EE6742E3E8AB052C3144F_33 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9B2219C436BB44093B17B05D7C37A95D732BDF1A_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D9B2219C436BB44093B17B05D7C37A95D732BDF1A_34)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D9B2219C436BB44093B17B05D7C37A95D732BDF1A_34() const { return ___U24fieldU2D9B2219C436BB44093B17B05D7C37A95D732BDF1A_34; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D9B2219C436BB44093B17B05D7C37A95D732BDF1A_34() { return &___U24fieldU2D9B2219C436BB44093B17B05D7C37A95D732BDF1A_34; }
	inline void set_U24fieldU2D9B2219C436BB44093B17B05D7C37A95D732BDF1A_34(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D9B2219C436BB44093B17B05D7C37A95D732BDF1A_34 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D52534D8994C76421C9DFA10C8E8A343500B12F38_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D52534D8994C76421C9DFA10C8E8A343500B12F38_35)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D52534D8994C76421C9DFA10C8E8A343500B12F38_35() const { return ___U24fieldU2D52534D8994C76421C9DFA10C8E8A343500B12F38_35; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D52534D8994C76421C9DFA10C8E8A343500B12F38_35() { return &___U24fieldU2D52534D8994C76421C9DFA10C8E8A343500B12F38_35; }
	inline void set_U24fieldU2D52534D8994C76421C9DFA10C8E8A343500B12F38_35(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D52534D8994C76421C9DFA10C8E8A343500B12F38_35 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D525F126E43DA5737F65A5AD29C20B79DE19010A6_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D525F126E43DA5737F65A5AD29C20B79DE19010A6_36)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D525F126E43DA5737F65A5AD29C20B79DE19010A6_36() const { return ___U24fieldU2D525F126E43DA5737F65A5AD29C20B79DE19010A6_36; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D525F126E43DA5737F65A5AD29C20B79DE19010A6_36() { return &___U24fieldU2D525F126E43DA5737F65A5AD29C20B79DE19010A6_36; }
	inline void set_U24fieldU2D525F126E43DA5737F65A5AD29C20B79DE19010A6_36(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D525F126E43DA5737F65A5AD29C20B79DE19010A6_36 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7B5372F481A98563710C701B145AADD910A18C91_37() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7B5372F481A98563710C701B145AADD910A18C91_37)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D7B5372F481A98563710C701B145AADD910A18C91_37() const { return ___U24fieldU2D7B5372F481A98563710C701B145AADD910A18C91_37; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D7B5372F481A98563710C701B145AADD910A18C91_37() { return &___U24fieldU2D7B5372F481A98563710C701B145AADD910A18C91_37; }
	inline void set_U24fieldU2D7B5372F481A98563710C701B145AADD910A18C91_37(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D7B5372F481A98563710C701B145AADD910A18C91_37 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB7C9C48891656F222702DCE29351381A7D30D3E7_38() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DB7C9C48891656F222702DCE29351381A7D30D3E7_38)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DB7C9C48891656F222702DCE29351381A7D30D3E7_38() const { return ___U24fieldU2DB7C9C48891656F222702DCE29351381A7D30D3E7_38; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DB7C9C48891656F222702DCE29351381A7D30D3E7_38() { return &___U24fieldU2DB7C9C48891656F222702DCE29351381A7D30D3E7_38; }
	inline void set_U24fieldU2DB7C9C48891656F222702DCE29351381A7D30D3E7_38(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DB7C9C48891656F222702DCE29351381A7D30D3E7_38 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD94579B1058818D44D4310948B7C46070767536D_39() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD94579B1058818D44D4310948B7C46070767536D_39)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DD94579B1058818D44D4310948B7C46070767536D_39() const { return ___U24fieldU2DD94579B1058818D44D4310948B7C46070767536D_39; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DD94579B1058818D44D4310948B7C46070767536D_39() { return &___U24fieldU2DD94579B1058818D44D4310948B7C46070767536D_39; }
	inline void set_U24fieldU2DD94579B1058818D44D4310948B7C46070767536D_39(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DD94579B1058818D44D4310948B7C46070767536D_39 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAF3BA40B1D20EEA4FD9D64C9BAF514D5EF8B2A3C_40() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DAF3BA40B1D20EEA4FD9D64C9BAF514D5EF8B2A3C_40)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DAF3BA40B1D20EEA4FD9D64C9BAF514D5EF8B2A3C_40() const { return ___U24fieldU2DAF3BA40B1D20EEA4FD9D64C9BAF514D5EF8B2A3C_40; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DAF3BA40B1D20EEA4FD9D64C9BAF514D5EF8B2A3C_40() { return &___U24fieldU2DAF3BA40B1D20EEA4FD9D64C9BAF514D5EF8B2A3C_40; }
	inline void set_U24fieldU2DAF3BA40B1D20EEA4FD9D64C9BAF514D5EF8B2A3C_40(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DAF3BA40B1D20EEA4FD9D64C9BAF514D5EF8B2A3C_40 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE8E26DD65BF96015D3F99B815805F6C58C21B23E_41() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DE8E26DD65BF96015D3F99B815805F6C58C21B23E_41)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DE8E26DD65BF96015D3F99B815805F6C58C21B23E_41() const { return ___U24fieldU2DE8E26DD65BF96015D3F99B815805F6C58C21B23E_41; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DE8E26DD65BF96015D3F99B815805F6C58C21B23E_41() { return &___U24fieldU2DE8E26DD65BF96015D3F99B815805F6C58C21B23E_41; }
	inline void set_U24fieldU2DE8E26DD65BF96015D3F99B815805F6C58C21B23E_41(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DE8E26DD65BF96015D3F99B815805F6C58C21B23E_41 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D38A81B10112EBC8F090274B9571D818D8F7E3D2C_42() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D38A81B10112EBC8F090274B9571D818D8F7E3D2C_42)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D38A81B10112EBC8F090274B9571D818D8F7E3D2C_42() const { return ___U24fieldU2D38A81B10112EBC8F090274B9571D818D8F7E3D2C_42; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D38A81B10112EBC8F090274B9571D818D8F7E3D2C_42() { return &___U24fieldU2D38A81B10112EBC8F090274B9571D818D8F7E3D2C_42; }
	inline void set_U24fieldU2D38A81B10112EBC8F090274B9571D818D8F7E3D2C_42(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D38A81B10112EBC8F090274B9571D818D8F7E3D2C_42 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1E55BBA47A1186A42E6BBA84D30C72C932A811D3_43() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D1E55BBA47A1186A42E6BBA84D30C72C932A811D3_43)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D1E55BBA47A1186A42E6BBA84D30C72C932A811D3_43() const { return ___U24fieldU2D1E55BBA47A1186A42E6BBA84D30C72C932A811D3_43; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D1E55BBA47A1186A42E6BBA84D30C72C932A811D3_43() { return &___U24fieldU2D1E55BBA47A1186A42E6BBA84D30C72C932A811D3_43; }
	inline void set_U24fieldU2D1E55BBA47A1186A42E6BBA84D30C72C932A811D3_43(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D1E55BBA47A1186A42E6BBA84D30C72C932A811D3_43 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1C4E377322BB013A6F15E832A61BB098615DF0B8_44() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D1C4E377322BB013A6F15E832A61BB098615DF0B8_44)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D1C4E377322BB013A6F15E832A61BB098615DF0B8_44() const { return ___U24fieldU2D1C4E377322BB013A6F15E832A61BB098615DF0B8_44; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D1C4E377322BB013A6F15E832A61BB098615DF0B8_44() { return &___U24fieldU2D1C4E377322BB013A6F15E832A61BB098615DF0B8_44; }
	inline void set_U24fieldU2D1C4E377322BB013A6F15E832A61BB098615DF0B8_44(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D1C4E377322BB013A6F15E832A61BB098615DF0B8_44 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9C4A054E93EF5C04D40B82BEADCFAD5CF4D86DEA_45() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D9C4A054E93EF5C04D40B82BEADCFAD5CF4D86DEA_45)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D9C4A054E93EF5C04D40B82BEADCFAD5CF4D86DEA_45() const { return ___U24fieldU2D9C4A054E93EF5C04D40B82BEADCFAD5CF4D86DEA_45; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D9C4A054E93EF5C04D40B82BEADCFAD5CF4D86DEA_45() { return &___U24fieldU2D9C4A054E93EF5C04D40B82BEADCFAD5CF4D86DEA_45; }
	inline void set_U24fieldU2D9C4A054E93EF5C04D40B82BEADCFAD5CF4D86DEA_45(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D9C4A054E93EF5C04D40B82BEADCFAD5CF4D86DEA_45 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCFA2FBADAA7ADDA055EA831A821AEDB1D14BAB9D_46() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DCFA2FBADAA7ADDA055EA831A821AEDB1D14BAB9D_46)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DCFA2FBADAA7ADDA055EA831A821AEDB1D14BAB9D_46() const { return ___U24fieldU2DCFA2FBADAA7ADDA055EA831A821AEDB1D14BAB9D_46; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DCFA2FBADAA7ADDA055EA831A821AEDB1D14BAB9D_46() { return &___U24fieldU2DCFA2FBADAA7ADDA055EA831A821AEDB1D14BAB9D_46; }
	inline void set_U24fieldU2DCFA2FBADAA7ADDA055EA831A821AEDB1D14BAB9D_46(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DCFA2FBADAA7ADDA055EA831A821AEDB1D14BAB9D_46 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D60323ABD2D7C22F62E865DF1F748CF1AF8AD9B7F_47() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D60323ABD2D7C22F62E865DF1F748CF1AF8AD9B7F_47)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D60323ABD2D7C22F62E865DF1F748CF1AF8AD9B7F_47() const { return ___U24fieldU2D60323ABD2D7C22F62E865DF1F748CF1AF8AD9B7F_47; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D60323ABD2D7C22F62E865DF1F748CF1AF8AD9B7F_47() { return &___U24fieldU2D60323ABD2D7C22F62E865DF1F748CF1AF8AD9B7F_47; }
	inline void set_U24fieldU2D60323ABD2D7C22F62E865DF1F748CF1AF8AD9B7F_47(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D60323ABD2D7C22F62E865DF1F748CF1AF8AD9B7F_47 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D64DE266A45F5F7D5898FA6A7529673C84D5BA650_48() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D64DE266A45F5F7D5898FA6A7529673C84D5BA650_48)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D64DE266A45F5F7D5898FA6A7529673C84D5BA650_48() const { return ___U24fieldU2D64DE266A45F5F7D5898FA6A7529673C84D5BA650_48; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D64DE266A45F5F7D5898FA6A7529673C84D5BA650_48() { return &___U24fieldU2D64DE266A45F5F7D5898FA6A7529673C84D5BA650_48; }
	inline void set_U24fieldU2D64DE266A45F5F7D5898FA6A7529673C84D5BA650_48(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D64DE266A45F5F7D5898FA6A7529673C84D5BA650_48 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB3A75CC32F78173CF10F472B21A54DAFAAA20F32_49() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DB3A75CC32F78173CF10F472B21A54DAFAAA20F32_49)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DB3A75CC32F78173CF10F472B21A54DAFAAA20F32_49() const { return ___U24fieldU2DB3A75CC32F78173CF10F472B21A54DAFAAA20F32_49; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DB3A75CC32F78173CF10F472B21A54DAFAAA20F32_49() { return &___U24fieldU2DB3A75CC32F78173CF10F472B21A54DAFAAA20F32_49; }
	inline void set_U24fieldU2DB3A75CC32F78173CF10F472B21A54DAFAAA20F32_49(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DB3A75CC32F78173CF10F472B21A54DAFAAA20F32_49 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DDA8B2917B2EEA744BF4521F61E17227E77F19519_50() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DDA8B2917B2EEA744BF4521F61E17227E77F19519_50)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DDA8B2917B2EEA744BF4521F61E17227E77F19519_50() const { return ___U24fieldU2DDA8B2917B2EEA744BF4521F61E17227E77F19519_50; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DDA8B2917B2EEA744BF4521F61E17227E77F19519_50() { return &___U24fieldU2DDA8B2917B2EEA744BF4521F61E17227E77F19519_50; }
	inline void set_U24fieldU2DDA8B2917B2EEA744BF4521F61E17227E77F19519_50(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DDA8B2917B2EEA744BF4521F61E17227E77F19519_50 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD1B8C323C9E44C56DDF066DCFA8FD6B89AE29B4D_51() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD1B8C323C9E44C56DDF066DCFA8FD6B89AE29B4D_51)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DD1B8C323C9E44C56DDF066DCFA8FD6B89AE29B4D_51() const { return ___U24fieldU2DD1B8C323C9E44C56DDF066DCFA8FD6B89AE29B4D_51; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DD1B8C323C9E44C56DDF066DCFA8FD6B89AE29B4D_51() { return &___U24fieldU2DD1B8C323C9E44C56DDF066DCFA8FD6B89AE29B4D_51; }
	inline void set_U24fieldU2DD1B8C323C9E44C56DDF066DCFA8FD6B89AE29B4D_51(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DD1B8C323C9E44C56DDF066DCFA8FD6B89AE29B4D_51 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DADDDB448B434C9ECFE51345E3CC896D2A7FEA1AD_52() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DADDDB448B434C9ECFE51345E3CC896D2A7FEA1AD_52)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DADDDB448B434C9ECFE51345E3CC896D2A7FEA1AD_52() const { return ___U24fieldU2DADDDB448B434C9ECFE51345E3CC896D2A7FEA1AD_52; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DADDDB448B434C9ECFE51345E3CC896D2A7FEA1AD_52() { return &___U24fieldU2DADDDB448B434C9ECFE51345E3CC896D2A7FEA1AD_52; }
	inline void set_U24fieldU2DADDDB448B434C9ECFE51345E3CC896D2A7FEA1AD_52(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DADDDB448B434C9ECFE51345E3CC896D2A7FEA1AD_52 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4FDE5D193D740FA6EDE761A161800A8AFFCEA9BD_53() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D4FDE5D193D740FA6EDE761A161800A8AFFCEA9BD_53)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D4FDE5D193D740FA6EDE761A161800A8AFFCEA9BD_53() const { return ___U24fieldU2D4FDE5D193D740FA6EDE761A161800A8AFFCEA9BD_53; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D4FDE5D193D740FA6EDE761A161800A8AFFCEA9BD_53() { return &___U24fieldU2D4FDE5D193D740FA6EDE761A161800A8AFFCEA9BD_53; }
	inline void set_U24fieldU2D4FDE5D193D740FA6EDE761A161800A8AFFCEA9BD_53(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D4FDE5D193D740FA6EDE761A161800A8AFFCEA9BD_53 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAF9989AA1811EBBCBC515B53CAE9AE32F6A8C504_54() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DAF9989AA1811EBBCBC515B53CAE9AE32F6A8C504_54)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DAF9989AA1811EBBCBC515B53CAE9AE32F6A8C504_54() const { return ___U24fieldU2DAF9989AA1811EBBCBC515B53CAE9AE32F6A8C504_54; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DAF9989AA1811EBBCBC515B53CAE9AE32F6A8C504_54() { return &___U24fieldU2DAF9989AA1811EBBCBC515B53CAE9AE32F6A8C504_54; }
	inline void set_U24fieldU2DAF9989AA1811EBBCBC515B53CAE9AE32F6A8C504_54(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DAF9989AA1811EBBCBC515B53CAE9AE32F6A8C504_54 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D85677C568E01B93F32A92ABE48155AD70F976DAD_55() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D85677C568E01B93F32A92ABE48155AD70F976DAD_55)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D85677C568E01B93F32A92ABE48155AD70F976DAD_55() const { return ___U24fieldU2D85677C568E01B93F32A92ABE48155AD70F976DAD_55; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D85677C568E01B93F32A92ABE48155AD70F976DAD_55() { return &___U24fieldU2D85677C568E01B93F32A92ABE48155AD70F976DAD_55; }
	inline void set_U24fieldU2D85677C568E01B93F32A92ABE48155AD70F976DAD_55(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D85677C568E01B93F32A92ABE48155AD70F976DAD_55 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4C1D6C591828D866127110DDE812D542CC9331BC_56() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D4C1D6C591828D866127110DDE812D542CC9331BC_56)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D4C1D6C591828D866127110DDE812D542CC9331BC_56() const { return ___U24fieldU2D4C1D6C591828D866127110DDE812D542CC9331BC_56; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D4C1D6C591828D866127110DDE812D542CC9331BC_56() { return &___U24fieldU2D4C1D6C591828D866127110DDE812D542CC9331BC_56; }
	inline void set_U24fieldU2D4C1D6C591828D866127110DDE812D542CC9331BC_56(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D4C1D6C591828D866127110DDE812D542CC9331BC_56 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D73C9519F21426AFF18B3E246AB329D7081D74629_57() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D73C9519F21426AFF18B3E246AB329D7081D74629_57)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D73C9519F21426AFF18B3E246AB329D7081D74629_57() const { return ___U24fieldU2D73C9519F21426AFF18B3E246AB329D7081D74629_57; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D73C9519F21426AFF18B3E246AB329D7081D74629_57() { return &___U24fieldU2D73C9519F21426AFF18B3E246AB329D7081D74629_57; }
	inline void set_U24fieldU2D73C9519F21426AFF18B3E246AB329D7081D74629_57(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D73C9519F21426AFF18B3E246AB329D7081D74629_57 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF01D68EF083172EDC245696B4DFB5B813C4C40D6_58() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DF01D68EF083172EDC245696B4DFB5B813C4C40D6_58)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DF01D68EF083172EDC245696B4DFB5B813C4C40D6_58() const { return ___U24fieldU2DF01D68EF083172EDC245696B4DFB5B813C4C40D6_58; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DF01D68EF083172EDC245696B4DFB5B813C4C40D6_58() { return &___U24fieldU2DF01D68EF083172EDC245696B4DFB5B813C4C40D6_58; }
	inline void set_U24fieldU2DF01D68EF083172EDC245696B4DFB5B813C4C40D6_58(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DF01D68EF083172EDC245696B4DFB5B813C4C40D6_58 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D857FADA51A4C3CCFFB1E9A68291CC4C0444975B4_59() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D857FADA51A4C3CCFFB1E9A68291CC4C0444975B4_59)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D857FADA51A4C3CCFFB1E9A68291CC4C0444975B4_59() const { return ___U24fieldU2D857FADA51A4C3CCFFB1E9A68291CC4C0444975B4_59; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D857FADA51A4C3CCFFB1E9A68291CC4C0444975B4_59() { return &___U24fieldU2D857FADA51A4C3CCFFB1E9A68291CC4C0444975B4_59; }
	inline void set_U24fieldU2D857FADA51A4C3CCFFB1E9A68291CC4C0444975B4_59(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D857FADA51A4C3CCFFB1E9A68291CC4C0444975B4_59 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5AD50533400F104F24486A35E159B3A7414E6984_60() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D5AD50533400F104F24486A35E159B3A7414E6984_60)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D5AD50533400F104F24486A35E159B3A7414E6984_60() const { return ___U24fieldU2D5AD50533400F104F24486A35E159B3A7414E6984_60; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D5AD50533400F104F24486A35E159B3A7414E6984_60() { return &___U24fieldU2D5AD50533400F104F24486A35E159B3A7414E6984_60; }
	inline void set_U24fieldU2D5AD50533400F104F24486A35E159B3A7414E6984_60(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D5AD50533400F104F24486A35E159B3A7414E6984_60 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D649374558406F6736E729396840BC136C2B5BFD9_61() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D649374558406F6736E729396840BC136C2B5BFD9_61)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D649374558406F6736E729396840BC136C2B5BFD9_61() const { return ___U24fieldU2D649374558406F6736E729396840BC136C2B5BFD9_61; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D649374558406F6736E729396840BC136C2B5BFD9_61() { return &___U24fieldU2D649374558406F6736E729396840BC136C2B5BFD9_61; }
	inline void set_U24fieldU2D649374558406F6736E729396840BC136C2B5BFD9_61(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D649374558406F6736E729396840BC136C2B5BFD9_61 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB7EB9205D5C176BD8A77FBE82679E8CC0D7E5FA8_62() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DB7EB9205D5C176BD8A77FBE82679E8CC0D7E5FA8_62)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DB7EB9205D5C176BD8A77FBE82679E8CC0D7E5FA8_62() const { return ___U24fieldU2DB7EB9205D5C176BD8A77FBE82679E8CC0D7E5FA8_62; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DB7EB9205D5C176BD8A77FBE82679E8CC0D7E5FA8_62() { return &___U24fieldU2DB7EB9205D5C176BD8A77FBE82679E8CC0D7E5FA8_62; }
	inline void set_U24fieldU2DB7EB9205D5C176BD8A77FBE82679E8CC0D7E5FA8_62(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DB7EB9205D5C176BD8A77FBE82679E8CC0D7E5FA8_62 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7190DEAEAB70C823A76B61E3789D9685510816ED_63() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7190DEAEAB70C823A76B61E3789D9685510816ED_63)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D7190DEAEAB70C823A76B61E3789D9685510816ED_63() const { return ___U24fieldU2D7190DEAEAB70C823A76B61E3789D9685510816ED_63; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D7190DEAEAB70C823A76B61E3789D9685510816ED_63() { return &___U24fieldU2D7190DEAEAB70C823A76B61E3789D9685510816ED_63; }
	inline void set_U24fieldU2D7190DEAEAB70C823A76B61E3789D9685510816ED_63(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D7190DEAEAB70C823A76B61E3789D9685510816ED_63 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7C93AB5D9B0DD4DE43D8459D61FEAB642F5718EB_64() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7C93AB5D9B0DD4DE43D8459D61FEAB642F5718EB_64)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D7C93AB5D9B0DD4DE43D8459D61FEAB642F5718EB_64() const { return ___U24fieldU2D7C93AB5D9B0DD4DE43D8459D61FEAB642F5718EB_64; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D7C93AB5D9B0DD4DE43D8459D61FEAB642F5718EB_64() { return &___U24fieldU2D7C93AB5D9B0DD4DE43D8459D61FEAB642F5718EB_64; }
	inline void set_U24fieldU2D7C93AB5D9B0DD4DE43D8459D61FEAB642F5718EB_64(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D7C93AB5D9B0DD4DE43D8459D61FEAB642F5718EB_64 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D94776C9848170581D079C920A080F505862BA5F5_65() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D94776C9848170581D079C920A080F505862BA5F5_65)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D94776C9848170581D079C920A080F505862BA5F5_65() const { return ___U24fieldU2D94776C9848170581D079C920A080F505862BA5F5_65; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D94776C9848170581D079C920A080F505862BA5F5_65() { return &___U24fieldU2D94776C9848170581D079C920A080F505862BA5F5_65; }
	inline void set_U24fieldU2D94776C9848170581D079C920A080F505862BA5F5_65(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D94776C9848170581D079C920A080F505862BA5F5_65 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB8DD06F686E2962EFFE0C0135249BA1EEBD189F2_66() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DB8DD06F686E2962EFFE0C0135249BA1EEBD189F2_66)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DB8DD06F686E2962EFFE0C0135249BA1EEBD189F2_66() const { return ___U24fieldU2DB8DD06F686E2962EFFE0C0135249BA1EEBD189F2_66; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DB8DD06F686E2962EFFE0C0135249BA1EEBD189F2_66() { return &___U24fieldU2DB8DD06F686E2962EFFE0C0135249BA1EEBD189F2_66; }
	inline void set_U24fieldU2DB8DD06F686E2962EFFE0C0135249BA1EEBD189F2_66(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DB8DD06F686E2962EFFE0C0135249BA1EEBD189F2_66 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D27888A1263310BD05ADF3C66E433E62AC22837D4_67() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D27888A1263310BD05ADF3C66E433E62AC22837D4_67)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D27888A1263310BD05ADF3C66E433E62AC22837D4_67() const { return ___U24fieldU2D27888A1263310BD05ADF3C66E433E62AC22837D4_67; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D27888A1263310BD05ADF3C66E433E62AC22837D4_67() { return &___U24fieldU2D27888A1263310BD05ADF3C66E433E62AC22837D4_67; }
	inline void set_U24fieldU2D27888A1263310BD05ADF3C66E433E62AC22837D4_67(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D27888A1263310BD05ADF3C66E433E62AC22837D4_67 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D43E7F55B82FD915DDC4D5C8443D0BBB9D27CC671_68() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D43E7F55B82FD915DDC4D5C8443D0BBB9D27CC671_68)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D43E7F55B82FD915DDC4D5C8443D0BBB9D27CC671_68() const { return ___U24fieldU2D43E7F55B82FD915DDC4D5C8443D0BBB9D27CC671_68; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D43E7F55B82FD915DDC4D5C8443D0BBB9D27CC671_68() { return &___U24fieldU2D43E7F55B82FD915DDC4D5C8443D0BBB9D27CC671_68; }
	inline void set_U24fieldU2D43E7F55B82FD915DDC4D5C8443D0BBB9D27CC671_68(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D43E7F55B82FD915DDC4D5C8443D0BBB9D27CC671_68 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D567BDB060ABADAA9816DE75D30D09ECBDBAFBB37_69() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D567BDB060ABADAA9816DE75D30D09ECBDBAFBB37_69)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D567BDB060ABADAA9816DE75D30D09ECBDBAFBB37_69() const { return ___U24fieldU2D567BDB060ABADAA9816DE75D30D09ECBDBAFBB37_69; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D567BDB060ABADAA9816DE75D30D09ECBDBAFBB37_69() { return &___U24fieldU2D567BDB060ABADAA9816DE75D30D09ECBDBAFBB37_69; }
	inline void set_U24fieldU2D567BDB060ABADAA9816DE75D30D09ECBDBAFBB37_69(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D567BDB060ABADAA9816DE75D30D09ECBDBAFBB37_69 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3B7119575F4FB85ACD75255CE7CE86293C11DCE7_70() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D3B7119575F4FB85ACD75255CE7CE86293C11DCE7_70)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D3B7119575F4FB85ACD75255CE7CE86293C11DCE7_70() const { return ___U24fieldU2D3B7119575F4FB85ACD75255CE7CE86293C11DCE7_70; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D3B7119575F4FB85ACD75255CE7CE86293C11DCE7_70() { return &___U24fieldU2D3B7119575F4FB85ACD75255CE7CE86293C11DCE7_70; }
	inline void set_U24fieldU2D3B7119575F4FB85ACD75255CE7CE86293C11DCE7_70(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D3B7119575F4FB85ACD75255CE7CE86293C11DCE7_70 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DED9CB0715AFCEA441D9FCF2208BF8FCCC2174D05_71() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DED9CB0715AFCEA441D9FCF2208BF8FCCC2174D05_71)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DED9CB0715AFCEA441D9FCF2208BF8FCCC2174D05_71() const { return ___U24fieldU2DED9CB0715AFCEA441D9FCF2208BF8FCCC2174D05_71; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DED9CB0715AFCEA441D9FCF2208BF8FCCC2174D05_71() { return &___U24fieldU2DED9CB0715AFCEA441D9FCF2208BF8FCCC2174D05_71; }
	inline void set_U24fieldU2DED9CB0715AFCEA441D9FCF2208BF8FCCC2174D05_71(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DED9CB0715AFCEA441D9FCF2208BF8FCCC2174D05_71 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA17570D5B6C2DAAE95E31C43B263CE1D47082979_72() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DA17570D5B6C2DAAE95E31C43B263CE1D47082979_72)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DA17570D5B6C2DAAE95E31C43B263CE1D47082979_72() const { return ___U24fieldU2DA17570D5B6C2DAAE95E31C43B263CE1D47082979_72; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DA17570D5B6C2DAAE95E31C43B263CE1D47082979_72() { return &___U24fieldU2DA17570D5B6C2DAAE95E31C43B263CE1D47082979_72; }
	inline void set_U24fieldU2DA17570D5B6C2DAAE95E31C43B263CE1D47082979_72(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DA17570D5B6C2DAAE95E31C43B263CE1D47082979_72 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D96B97D48A5B812040C8C4972167A1D73D48C17C1_73() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D96B97D48A5B812040C8C4972167A1D73D48C17C1_73)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D96B97D48A5B812040C8C4972167A1D73D48C17C1_73() const { return ___U24fieldU2D96B97D48A5B812040C8C4972167A1D73D48C17C1_73; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D96B97D48A5B812040C8C4972167A1D73D48C17C1_73() { return &___U24fieldU2D96B97D48A5B812040C8C4972167A1D73D48C17C1_73; }
	inline void set_U24fieldU2D96B97D48A5B812040C8C4972167A1D73D48C17C1_73(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D96B97D48A5B812040C8C4972167A1D73D48C17C1_73 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3A99CC082D9B62CBA73369CD8566DC6D2D3CE56B_74() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D3A99CC082D9B62CBA73369CD8566DC6D2D3CE56B_74)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D3A99CC082D9B62CBA73369CD8566DC6D2D3CE56B_74() const { return ___U24fieldU2D3A99CC082D9B62CBA73369CD8566DC6D2D3CE56B_74; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D3A99CC082D9B62CBA73369CD8566DC6D2D3CE56B_74() { return &___U24fieldU2D3A99CC082D9B62CBA73369CD8566DC6D2D3CE56B_74; }
	inline void set_U24fieldU2D3A99CC082D9B62CBA73369CD8566DC6D2D3CE56B_74(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D3A99CC082D9B62CBA73369CD8566DC6D2D3CE56B_74 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D0BAAEA820015031AACD0305342ACA2F73FE000BF_75() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D0BAAEA820015031AACD0305342ACA2F73FE000BF_75)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D0BAAEA820015031AACD0305342ACA2F73FE000BF_75() const { return ___U24fieldU2D0BAAEA820015031AACD0305342ACA2F73FE000BF_75; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D0BAAEA820015031AACD0305342ACA2F73FE000BF_75() { return &___U24fieldU2D0BAAEA820015031AACD0305342ACA2F73FE000BF_75; }
	inline void set_U24fieldU2D0BAAEA820015031AACD0305342ACA2F73FE000BF_75(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D0BAAEA820015031AACD0305342ACA2F73FE000BF_75 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1FF2C4D3129A713375C286776E2A047579F3B460_76() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D1FF2C4D3129A713375C286776E2A047579F3B460_76)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D1FF2C4D3129A713375C286776E2A047579F3B460_76() const { return ___U24fieldU2D1FF2C4D3129A713375C286776E2A047579F3B460_76; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D1FF2C4D3129A713375C286776E2A047579F3B460_76() { return &___U24fieldU2D1FF2C4D3129A713375C286776E2A047579F3B460_76; }
	inline void set_U24fieldU2D1FF2C4D3129A713375C286776E2A047579F3B460_76(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D1FF2C4D3129A713375C286776E2A047579F3B460_76 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7D6AC3825D861BDCAF6E43C0F58F89B8E92AC302_77() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7D6AC3825D861BDCAF6E43C0F58F89B8E92AC302_77)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D7D6AC3825D861BDCAF6E43C0F58F89B8E92AC302_77() const { return ___U24fieldU2D7D6AC3825D861BDCAF6E43C0F58F89B8E92AC302_77; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D7D6AC3825D861BDCAF6E43C0F58F89B8E92AC302_77() { return &___U24fieldU2D7D6AC3825D861BDCAF6E43C0F58F89B8E92AC302_77; }
	inline void set_U24fieldU2D7D6AC3825D861BDCAF6E43C0F58F89B8E92AC302_77(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D7D6AC3825D861BDCAF6E43C0F58F89B8E92AC302_77 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D28888B3CDD7E5D6AA2A5A4FDDC2CC08247D97BE8_78() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D28888B3CDD7E5D6AA2A5A4FDDC2CC08247D97BE8_78)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D28888B3CDD7E5D6AA2A5A4FDDC2CC08247D97BE8_78() const { return ___U24fieldU2D28888B3CDD7E5D6AA2A5A4FDDC2CC08247D97BE8_78; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D28888B3CDD7E5D6AA2A5A4FDDC2CC08247D97BE8_78() { return &___U24fieldU2D28888B3CDD7E5D6AA2A5A4FDDC2CC08247D97BE8_78; }
	inline void set_U24fieldU2D28888B3CDD7E5D6AA2A5A4FDDC2CC08247D97BE8_78(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D28888B3CDD7E5D6AA2A5A4FDDC2CC08247D97BE8_78 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD7B1D09721FE120EEE473015E875CA4C36093305_79() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD7B1D09721FE120EEE473015E875CA4C36093305_79)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DD7B1D09721FE120EEE473015E875CA4C36093305_79() const { return ___U24fieldU2DD7B1D09721FE120EEE473015E875CA4C36093305_79; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DD7B1D09721FE120EEE473015E875CA4C36093305_79() { return &___U24fieldU2DD7B1D09721FE120EEE473015E875CA4C36093305_79; }
	inline void set_U24fieldU2DD7B1D09721FE120EEE473015E875CA4C36093305_79(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DD7B1D09721FE120EEE473015E875CA4C36093305_79 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DDF301B5B93AC5A743ADF90679C1E31A3E5DF913C_80() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DDF301B5B93AC5A743ADF90679C1E31A3E5DF913C_80)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DDF301B5B93AC5A743ADF90679C1E31A3E5DF913C_80() const { return ___U24fieldU2DDF301B5B93AC5A743ADF90679C1E31A3E5DF913C_80; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DDF301B5B93AC5A743ADF90679C1E31A3E5DF913C_80() { return &___U24fieldU2DDF301B5B93AC5A743ADF90679C1E31A3E5DF913C_80; }
	inline void set_U24fieldU2DDF301B5B93AC5A743ADF90679C1E31A3E5DF913C_80(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DDF301B5B93AC5A743ADF90679C1E31A3E5DF913C_80 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFBBD0BE893F85B13E4036721A87894B24985AB5F_81() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DFBBD0BE893F85B13E4036721A87894B24985AB5F_81)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DFBBD0BE893F85B13E4036721A87894B24985AB5F_81() const { return ___U24fieldU2DFBBD0BE893F85B13E4036721A87894B24985AB5F_81; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DFBBD0BE893F85B13E4036721A87894B24985AB5F_81() { return &___U24fieldU2DFBBD0BE893F85B13E4036721A87894B24985AB5F_81; }
	inline void set_U24fieldU2DFBBD0BE893F85B13E4036721A87894B24985AB5F_81(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DFBBD0BE893F85B13E4036721A87894B24985AB5F_81 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC3F4AC9D069D1C1AAF76D0F721BC71CE766F9C9C_82() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DC3F4AC9D069D1C1AAF76D0F721BC71CE766F9C9C_82)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DC3F4AC9D069D1C1AAF76D0F721BC71CE766F9C9C_82() const { return ___U24fieldU2DC3F4AC9D069D1C1AAF76D0F721BC71CE766F9C9C_82; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DC3F4AC9D069D1C1AAF76D0F721BC71CE766F9C9C_82() { return &___U24fieldU2DC3F4AC9D069D1C1AAF76D0F721BC71CE766F9C9C_82; }
	inline void set_U24fieldU2DC3F4AC9D069D1C1AAF76D0F721BC71CE766F9C9C_82(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DC3F4AC9D069D1C1AAF76D0F721BC71CE766F9C9C_82 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD69752D8BDA7699A11F2988E209F862C81080672_83() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD69752D8BDA7699A11F2988E209F862C81080672_83)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DD69752D8BDA7699A11F2988E209F862C81080672_83() const { return ___U24fieldU2DD69752D8BDA7699A11F2988E209F862C81080672_83; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DD69752D8BDA7699A11F2988E209F862C81080672_83() { return &___U24fieldU2DD69752D8BDA7699A11F2988E209F862C81080672_83; }
	inline void set_U24fieldU2DD69752D8BDA7699A11F2988E209F862C81080672_83(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DD69752D8BDA7699A11F2988E209F862C81080672_83 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D87A054A90E6EE560D4055BFCC8F875CE2A532AA9_84() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D87A054A90E6EE560D4055BFCC8F875CE2A532AA9_84)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D87A054A90E6EE560D4055BFCC8F875CE2A532AA9_84() const { return ___U24fieldU2D87A054A90E6EE560D4055BFCC8F875CE2A532AA9_84; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D87A054A90E6EE560D4055BFCC8F875CE2A532AA9_84() { return &___U24fieldU2D87A054A90E6EE560D4055BFCC8F875CE2A532AA9_84; }
	inline void set_U24fieldU2D87A054A90E6EE560D4055BFCC8F875CE2A532AA9_84(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D87A054A90E6EE560D4055BFCC8F875CE2A532AA9_84 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D22DCDD3F71C792BEE79BF79657972F19E8503C28_85() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D22DCDD3F71C792BEE79BF79657972F19E8503C28_85)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D22DCDD3F71C792BEE79BF79657972F19E8503C28_85() const { return ___U24fieldU2D22DCDD3F71C792BEE79BF79657972F19E8503C28_85; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D22DCDD3F71C792BEE79BF79657972F19E8503C28_85() { return &___U24fieldU2D22DCDD3F71C792BEE79BF79657972F19E8503C28_85; }
	inline void set_U24fieldU2D22DCDD3F71C792BEE79BF79657972F19E8503C28_85(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D22DCDD3F71C792BEE79BF79657972F19E8503C28_85 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D19801FABF900B8A91B3B7A8C19554CF21938A328_86() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D19801FABF900B8A91B3B7A8C19554CF21938A328_86)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D19801FABF900B8A91B3B7A8C19554CF21938A328_86() const { return ___U24fieldU2D19801FABF900B8A91B3B7A8C19554CF21938A328_86; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D19801FABF900B8A91B3B7A8C19554CF21938A328_86() { return &___U24fieldU2D19801FABF900B8A91B3B7A8C19554CF21938A328_86; }
	inline void set_U24fieldU2D19801FABF900B8A91B3B7A8C19554CF21938A328_86(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D19801FABF900B8A91B3B7A8C19554CF21938A328_86 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD3198CF22F3142E548C0ADEEBCBA1A1DF6C1FA76_87() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD3198CF22F3142E548C0ADEEBCBA1A1DF6C1FA76_87)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DD3198CF22F3142E548C0ADEEBCBA1A1DF6C1FA76_87() const { return ___U24fieldU2DD3198CF22F3142E548C0ADEEBCBA1A1DF6C1FA76_87; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DD3198CF22F3142E548C0ADEEBCBA1A1DF6C1FA76_87() { return &___U24fieldU2DD3198CF22F3142E548C0ADEEBCBA1A1DF6C1FA76_87; }
	inline void set_U24fieldU2DD3198CF22F3142E548C0ADEEBCBA1A1DF6C1FA76_87(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DD3198CF22F3142E548C0ADEEBCBA1A1DF6C1FA76_87 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D626E626D6BCD38FA699B0752E2D91A81B8CFD074_88() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D626E626D6BCD38FA699B0752E2D91A81B8CFD074_88)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D626E626D6BCD38FA699B0752E2D91A81B8CFD074_88() const { return ___U24fieldU2D626E626D6BCD38FA699B0752E2D91A81B8CFD074_88; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D626E626D6BCD38FA699B0752E2D91A81B8CFD074_88() { return &___U24fieldU2D626E626D6BCD38FA699B0752E2D91A81B8CFD074_88; }
	inline void set_U24fieldU2D626E626D6BCD38FA699B0752E2D91A81B8CFD074_88(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D626E626D6BCD38FA699B0752E2D91A81B8CFD074_88 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2841ABEC55FB3862F45BA8E5BD854FECB89B2C0B_89() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D2841ABEC55FB3862F45BA8E5BD854FECB89B2C0B_89)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D2841ABEC55FB3862F45BA8E5BD854FECB89B2C0B_89() const { return ___U24fieldU2D2841ABEC55FB3862F45BA8E5BD854FECB89B2C0B_89; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D2841ABEC55FB3862F45BA8E5BD854FECB89B2C0B_89() { return &___U24fieldU2D2841ABEC55FB3862F45BA8E5BD854FECB89B2C0B_89; }
	inline void set_U24fieldU2D2841ABEC55FB3862F45BA8E5BD854FECB89B2C0B_89(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D2841ABEC55FB3862F45BA8E5BD854FECB89B2C0B_89 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D0BDDE6F0882E6AD06D0FECC073EB61DC8779A234_90() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D0BDDE6F0882E6AD06D0FECC073EB61DC8779A234_90)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D0BDDE6F0882E6AD06D0FECC073EB61DC8779A234_90() const { return ___U24fieldU2D0BDDE6F0882E6AD06D0FECC073EB61DC8779A234_90; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D0BDDE6F0882E6AD06D0FECC073EB61DC8779A234_90() { return &___U24fieldU2D0BDDE6F0882E6AD06D0FECC073EB61DC8779A234_90; }
	inline void set_U24fieldU2D0BDDE6F0882E6AD06D0FECC073EB61DC8779A234_90(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D0BDDE6F0882E6AD06D0FECC073EB61DC8779A234_90 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DDED837EA58E0B97FD24A53995FCEC1D3A8414EEC_91() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DDED837EA58E0B97FD24A53995FCEC1D3A8414EEC_91)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DDED837EA58E0B97FD24A53995FCEC1D3A8414EEC_91() const { return ___U24fieldU2DDED837EA58E0B97FD24A53995FCEC1D3A8414EEC_91; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DDED837EA58E0B97FD24A53995FCEC1D3A8414EEC_91() { return &___U24fieldU2DDED837EA58E0B97FD24A53995FCEC1D3A8414EEC_91; }
	inline void set_U24fieldU2DDED837EA58E0B97FD24A53995FCEC1D3A8414EEC_91(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DDED837EA58E0B97FD24A53995FCEC1D3A8414EEC_91 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D62949AE93481AD89B9E39B9A17AED79A75EF656F_92() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D62949AE93481AD89B9E39B9A17AED79A75EF656F_92)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D62949AE93481AD89B9E39B9A17AED79A75EF656F_92() const { return ___U24fieldU2D62949AE93481AD89B9E39B9A17AED79A75EF656F_92; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D62949AE93481AD89B9E39B9A17AED79A75EF656F_92() { return &___U24fieldU2D62949AE93481AD89B9E39B9A17AED79A75EF656F_92; }
	inline void set_U24fieldU2D62949AE93481AD89B9E39B9A17AED79A75EF656F_92(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D62949AE93481AD89B9E39B9A17AED79A75EF656F_92 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D188D091BDAA625C458629AFB438C9CF1110A5749_93() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D188D091BDAA625C458629AFB438C9CF1110A5749_93)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D188D091BDAA625C458629AFB438C9CF1110A5749_93() const { return ___U24fieldU2D188D091BDAA625C458629AFB438C9CF1110A5749_93; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D188D091BDAA625C458629AFB438C9CF1110A5749_93() { return &___U24fieldU2D188D091BDAA625C458629AFB438C9CF1110A5749_93; }
	inline void set_U24fieldU2D188D091BDAA625C458629AFB438C9CF1110A5749_93(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D188D091BDAA625C458629AFB438C9CF1110A5749_93 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE5E7D260D4BD2EF3D7E754EFCDB39BDC6886040C_94() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DE5E7D260D4BD2EF3D7E754EFCDB39BDC6886040C_94)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DE5E7D260D4BD2EF3D7E754EFCDB39BDC6886040C_94() const { return ___U24fieldU2DE5E7D260D4BD2EF3D7E754EFCDB39BDC6886040C_94; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DE5E7D260D4BD2EF3D7E754EFCDB39BDC6886040C_94() { return &___U24fieldU2DE5E7D260D4BD2EF3D7E754EFCDB39BDC6886040C_94; }
	inline void set_U24fieldU2DE5E7D260D4BD2EF3D7E754EFCDB39BDC6886040C_94(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DE5E7D260D4BD2EF3D7E754EFCDB39BDC6886040C_94 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC0F8B663B994BB22A0819A77DC709EB6734A86EF_95() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DC0F8B663B994BB22A0819A77DC709EB6734A86EF_95)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DC0F8B663B994BB22A0819A77DC709EB6734A86EF_95() const { return ___U24fieldU2DC0F8B663B994BB22A0819A77DC709EB6734A86EF_95; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DC0F8B663B994BB22A0819A77DC709EB6734A86EF_95() { return &___U24fieldU2DC0F8B663B994BB22A0819A77DC709EB6734A86EF_95; }
	inline void set_U24fieldU2DC0F8B663B994BB22A0819A77DC709EB6734A86EF_95(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DC0F8B663B994BB22A0819A77DC709EB6734A86EF_95 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D472AB78FCC6457EB1051AB776C54D1B5479AD3A2_96() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D472AB78FCC6457EB1051AB776C54D1B5479AD3A2_96)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D472AB78FCC6457EB1051AB776C54D1B5479AD3A2_96() const { return ___U24fieldU2D472AB78FCC6457EB1051AB776C54D1B5479AD3A2_96; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D472AB78FCC6457EB1051AB776C54D1B5479AD3A2_96() { return &___U24fieldU2D472AB78FCC6457EB1051AB776C54D1B5479AD3A2_96; }
	inline void set_U24fieldU2D472AB78FCC6457EB1051AB776C54D1B5479AD3A2_96(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D472AB78FCC6457EB1051AB776C54D1B5479AD3A2_96 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6CFD6E9B2590DEC938E2F8711AFFA7803B7789D3_97() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D6CFD6E9B2590DEC938E2F8711AFFA7803B7789D3_97)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D6CFD6E9B2590DEC938E2F8711AFFA7803B7789D3_97() const { return ___U24fieldU2D6CFD6E9B2590DEC938E2F8711AFFA7803B7789D3_97; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D6CFD6E9B2590DEC938E2F8711AFFA7803B7789D3_97() { return &___U24fieldU2D6CFD6E9B2590DEC938E2F8711AFFA7803B7789D3_97; }
	inline void set_U24fieldU2D6CFD6E9B2590DEC938E2F8711AFFA7803B7789D3_97(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D6CFD6E9B2590DEC938E2F8711AFFA7803B7789D3_97 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9AD10A6B52DCB7C2044C028935E73252BCF32185_98() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D9AD10A6B52DCB7C2044C028935E73252BCF32185_98)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D9AD10A6B52DCB7C2044C028935E73252BCF32185_98() const { return ___U24fieldU2D9AD10A6B52DCB7C2044C028935E73252BCF32185_98; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D9AD10A6B52DCB7C2044C028935E73252BCF32185_98() { return &___U24fieldU2D9AD10A6B52DCB7C2044C028935E73252BCF32185_98; }
	inline void set_U24fieldU2D9AD10A6B52DCB7C2044C028935E73252BCF32185_98(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D9AD10A6B52DCB7C2044C028935E73252BCF32185_98 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD0410F28A9A96BD53F0C8CFD98F9A0D450FA002F_99() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD0410F28A9A96BD53F0C8CFD98F9A0D450FA002F_99)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DD0410F28A9A96BD53F0C8CFD98F9A0D450FA002F_99() const { return ___U24fieldU2DD0410F28A9A96BD53F0C8CFD98F9A0D450FA002F_99; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DD0410F28A9A96BD53F0C8CFD98F9A0D450FA002F_99() { return &___U24fieldU2DD0410F28A9A96BD53F0C8CFD98F9A0D450FA002F_99; }
	inline void set_U24fieldU2DD0410F28A9A96BD53F0C8CFD98F9A0D450FA002F_99(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DD0410F28A9A96BD53F0C8CFD98F9A0D450FA002F_99 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB8400EAE6B6E484B58EE5FDA5240F2235204F77A_100() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DB8400EAE6B6E484B58EE5FDA5240F2235204F77A_100)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DB8400EAE6B6E484B58EE5FDA5240F2235204F77A_100() const { return ___U24fieldU2DB8400EAE6B6E484B58EE5FDA5240F2235204F77A_100; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DB8400EAE6B6E484B58EE5FDA5240F2235204F77A_100() { return &___U24fieldU2DB8400EAE6B6E484B58EE5FDA5240F2235204F77A_100; }
	inline void set_U24fieldU2DB8400EAE6B6E484B58EE5FDA5240F2235204F77A_100(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DB8400EAE6B6E484B58EE5FDA5240F2235204F77A_100 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7A03E895CDEE6AF52BE3ECF5383175E0F218584C_101() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D7A03E895CDEE6AF52BE3ECF5383175E0F218584C_101)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D7A03E895CDEE6AF52BE3ECF5383175E0F218584C_101() const { return ___U24fieldU2D7A03E895CDEE6AF52BE3ECF5383175E0F218584C_101; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D7A03E895CDEE6AF52BE3ECF5383175E0F218584C_101() { return &___U24fieldU2D7A03E895CDEE6AF52BE3ECF5383175E0F218584C_101; }
	inline void set_U24fieldU2D7A03E895CDEE6AF52BE3ECF5383175E0F218584C_101(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D7A03E895CDEE6AF52BE3ECF5383175E0F218584C_101 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D0D7536E292838F2850B78C40CE965EF0FB40CB93_102() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D0D7536E292838F2850B78C40CE965EF0FB40CB93_102)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D0D7536E292838F2850B78C40CE965EF0FB40CB93_102() const { return ___U24fieldU2D0D7536E292838F2850B78C40CE965EF0FB40CB93_102; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D0D7536E292838F2850B78C40CE965EF0FB40CB93_102() { return &___U24fieldU2D0D7536E292838F2850B78C40CE965EF0FB40CB93_102; }
	inline void set_U24fieldU2D0D7536E292838F2850B78C40CE965EF0FB40CB93_102(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D0D7536E292838F2850B78C40CE965EF0FB40CB93_102 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6E84A598EB70DF96202A625C63B66975EED796EE_103() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D6E84A598EB70DF96202A625C63B66975EED796EE_103)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D6E84A598EB70DF96202A625C63B66975EED796EE_103() const { return ___U24fieldU2D6E84A598EB70DF96202A625C63B66975EED796EE_103; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D6E84A598EB70DF96202A625C63B66975EED796EE_103() { return &___U24fieldU2D6E84A598EB70DF96202A625C63B66975EED796EE_103; }
	inline void set_U24fieldU2D6E84A598EB70DF96202A625C63B66975EED796EE_103(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D6E84A598EB70DF96202A625C63B66975EED796EE_103 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D11363D33BC05212CEC6843E96E66163F908899C3_104() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D11363D33BC05212CEC6843E96E66163F908899C3_104)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D11363D33BC05212CEC6843E96E66163F908899C3_104() const { return ___U24fieldU2D11363D33BC05212CEC6843E96E66163F908899C3_104; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D11363D33BC05212CEC6843E96E66163F908899C3_104() { return &___U24fieldU2D11363D33BC05212CEC6843E96E66163F908899C3_104; }
	inline void set_U24fieldU2D11363D33BC05212CEC6843E96E66163F908899C3_104(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D11363D33BC05212CEC6843E96E66163F908899C3_104 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D614578BC1B89AB094AC64BFBA9BCC07CFE7A5C52_105() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D614578BC1B89AB094AC64BFBA9BCC07CFE7A5C52_105)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D614578BC1B89AB094AC64BFBA9BCC07CFE7A5C52_105() const { return ___U24fieldU2D614578BC1B89AB094AC64BFBA9BCC07CFE7A5C52_105; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D614578BC1B89AB094AC64BFBA9BCC07CFE7A5C52_105() { return &___U24fieldU2D614578BC1B89AB094AC64BFBA9BCC07CFE7A5C52_105; }
	inline void set_U24fieldU2D614578BC1B89AB094AC64BFBA9BCC07CFE7A5C52_105(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D614578BC1B89AB094AC64BFBA9BCC07CFE7A5C52_105 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC4D32E400AA14406F6E11289FC511F0A2BFB765E_106() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DC4D32E400AA14406F6E11289FC511F0A2BFB765E_106)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DC4D32E400AA14406F6E11289FC511F0A2BFB765E_106() const { return ___U24fieldU2DC4D32E400AA14406F6E11289FC511F0A2BFB765E_106; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DC4D32E400AA14406F6E11289FC511F0A2BFB765E_106() { return &___U24fieldU2DC4D32E400AA14406F6E11289FC511F0A2BFB765E_106; }
	inline void set_U24fieldU2DC4D32E400AA14406F6E11289FC511F0A2BFB765E_106(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DC4D32E400AA14406F6E11289FC511F0A2BFB765E_106 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D99380A2C88E75E6C9171C58173F9995E7CB0DC96_107() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D99380A2C88E75E6C9171C58173F9995E7CB0DC96_107)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D99380A2C88E75E6C9171C58173F9995E7CB0DC96_107() const { return ___U24fieldU2D99380A2C88E75E6C9171C58173F9995E7CB0DC96_107; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D99380A2C88E75E6C9171C58173F9995E7CB0DC96_107() { return &___U24fieldU2D99380A2C88E75E6C9171C58173F9995E7CB0DC96_107; }
	inline void set_U24fieldU2D99380A2C88E75E6C9171C58173F9995E7CB0DC96_107(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D99380A2C88E75E6C9171C58173F9995E7CB0DC96_107 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD921D1E53ACB96FA59820910A1C6957F797725FB_108() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD921D1E53ACB96FA59820910A1C6957F797725FB_108)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DD921D1E53ACB96FA59820910A1C6957F797725FB_108() const { return ___U24fieldU2DD921D1E53ACB96FA59820910A1C6957F797725FB_108; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DD921D1E53ACB96FA59820910A1C6957F797725FB_108() { return &___U24fieldU2DD921D1E53ACB96FA59820910A1C6957F797725FB_108; }
	inline void set_U24fieldU2DD921D1E53ACB96FA59820910A1C6957F797725FB_108(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DD921D1E53ACB96FA59820910A1C6957F797725FB_108 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFE8385470E173E2E7C2D9D94D9F81163CA600F0D_109() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DFE8385470E173E2E7C2D9D94D9F81163CA600F0D_109)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DFE8385470E173E2E7C2D9D94D9F81163CA600F0D_109() const { return ___U24fieldU2DFE8385470E173E2E7C2D9D94D9F81163CA600F0D_109; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DFE8385470E173E2E7C2D9D94D9F81163CA600F0D_109() { return &___U24fieldU2DFE8385470E173E2E7C2D9D94D9F81163CA600F0D_109; }
	inline void set_U24fieldU2DFE8385470E173E2E7C2D9D94D9F81163CA600F0D_109(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DFE8385470E173E2E7C2D9D94D9F81163CA600F0D_109 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D68845C029EE5C0E5EC8821721F587C33BB9284E8_110() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D68845C029EE5C0E5EC8821721F587C33BB9284E8_110)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D68845C029EE5C0E5EC8821721F587C33BB9284E8_110() const { return ___U24fieldU2D68845C029EE5C0E5EC8821721F587C33BB9284E8_110; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D68845C029EE5C0E5EC8821721F587C33BB9284E8_110() { return &___U24fieldU2D68845C029EE5C0E5EC8821721F587C33BB9284E8_110; }
	inline void set_U24fieldU2D68845C029EE5C0E5EC8821721F587C33BB9284E8_110(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D68845C029EE5C0E5EC8821721F587C33BB9284E8_110 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9AB51121DB726549C24CD8652D05D2279597A826_111() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D9AB51121DB726549C24CD8652D05D2279597A826_111)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D9AB51121DB726549C24CD8652D05D2279597A826_111() const { return ___U24fieldU2D9AB51121DB726549C24CD8652D05D2279597A826_111; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D9AB51121DB726549C24CD8652D05D2279597A826_111() { return &___U24fieldU2D9AB51121DB726549C24CD8652D05D2279597A826_111; }
	inline void set_U24fieldU2D9AB51121DB726549C24CD8652D05D2279597A826_111(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D9AB51121DB726549C24CD8652D05D2279597A826_111 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5A66B903EF5A119B6BADBE48C36C6F935D87D3A8_112() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D5A66B903EF5A119B6BADBE48C36C6F935D87D3A8_112)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D5A66B903EF5A119B6BADBE48C36C6F935D87D3A8_112() const { return ___U24fieldU2D5A66B903EF5A119B6BADBE48C36C6F935D87D3A8_112; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D5A66B903EF5A119B6BADBE48C36C6F935D87D3A8_112() { return &___U24fieldU2D5A66B903EF5A119B6BADBE48C36C6F935D87D3A8_112; }
	inline void set_U24fieldU2D5A66B903EF5A119B6BADBE48C36C6F935D87D3A8_112(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D5A66B903EF5A119B6BADBE48C36C6F935D87D3A8_112 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D0AB03169304F60655F2A2014ED877F0B4F8F86E3_113() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D0AB03169304F60655F2A2014ED877F0B4F8F86E3_113)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D0AB03169304F60655F2A2014ED877F0B4F8F86E3_113() const { return ___U24fieldU2D0AB03169304F60655F2A2014ED877F0B4F8F86E3_113; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D0AB03169304F60655F2A2014ED877F0B4F8F86E3_113() { return &___U24fieldU2D0AB03169304F60655F2A2014ED877F0B4F8F86E3_113; }
	inline void set_U24fieldU2D0AB03169304F60655F2A2014ED877F0B4F8F86E3_113(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D0AB03169304F60655F2A2014ED877F0B4F8F86E3_113 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA41A4536581FF8C104E66759035DFB236DFF0994_114() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DA41A4536581FF8C104E66759035DFB236DFF0994_114)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DA41A4536581FF8C104E66759035DFB236DFF0994_114() const { return ___U24fieldU2DA41A4536581FF8C104E66759035DFB236DFF0994_114; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DA41A4536581FF8C104E66759035DFB236DFF0994_114() { return &___U24fieldU2DA41A4536581FF8C104E66759035DFB236DFF0994_114; }
	inline void set_U24fieldU2DA41A4536581FF8C104E66759035DFB236DFF0994_114(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DA41A4536581FF8C104E66759035DFB236DFF0994_114 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D87FA64DAA4DA09B4C03E8060CBABE9BF63DFA0E6_115() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D87FA64DAA4DA09B4C03E8060CBABE9BF63DFA0E6_115)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D87FA64DAA4DA09B4C03E8060CBABE9BF63DFA0E6_115() const { return ___U24fieldU2D87FA64DAA4DA09B4C03E8060CBABE9BF63DFA0E6_115; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D87FA64DAA4DA09B4C03E8060CBABE9BF63DFA0E6_115() { return &___U24fieldU2D87FA64DAA4DA09B4C03E8060CBABE9BF63DFA0E6_115; }
	inline void set_U24fieldU2D87FA64DAA4DA09B4C03E8060CBABE9BF63DFA0E6_115(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D87FA64DAA4DA09B4C03E8060CBABE9BF63DFA0E6_115 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D75C89CF0447460CFB3ABAACBC205DC204F823B77_116() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D75C89CF0447460CFB3ABAACBC205DC204F823B77_116)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D75C89CF0447460CFB3ABAACBC205DC204F823B77_116() const { return ___U24fieldU2D75C89CF0447460CFB3ABAACBC205DC204F823B77_116; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D75C89CF0447460CFB3ABAACBC205DC204F823B77_116() { return &___U24fieldU2D75C89CF0447460CFB3ABAACBC205DC204F823B77_116; }
	inline void set_U24fieldU2D75C89CF0447460CFB3ABAACBC205DC204F823B77_116(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D75C89CF0447460CFB3ABAACBC205DC204F823B77_116 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D20A70D944EED422690C9BA10EF51A59A6A9B603A_117() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D20A70D944EED422690C9BA10EF51A59A6A9B603A_117)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D20A70D944EED422690C9BA10EF51A59A6A9B603A_117() const { return ___U24fieldU2D20A70D944EED422690C9BA10EF51A59A6A9B603A_117; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D20A70D944EED422690C9BA10EF51A59A6A9B603A_117() { return &___U24fieldU2D20A70D944EED422690C9BA10EF51A59A6A9B603A_117; }
	inline void set_U24fieldU2D20A70D944EED422690C9BA10EF51A59A6A9B603A_117(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D20A70D944EED422690C9BA10EF51A59A6A9B603A_117 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D970D607246F4725BCA45E7564DBC8E90DF8114B5_118() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D970D607246F4725BCA45E7564DBC8E90DF8114B5_118)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D970D607246F4725BCA45E7564DBC8E90DF8114B5_118() const { return ___U24fieldU2D970D607246F4725BCA45E7564DBC8E90DF8114B5_118; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D970D607246F4725BCA45E7564DBC8E90DF8114B5_118() { return &___U24fieldU2D970D607246F4725BCA45E7564DBC8E90DF8114B5_118; }
	inline void set_U24fieldU2D970D607246F4725BCA45E7564DBC8E90DF8114B5_118(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D970D607246F4725BCA45E7564DBC8E90DF8114B5_118 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D714E5457816BBE2A319AAC07D2C587A676A85EF5_119() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D714E5457816BBE2A319AAC07D2C587A676A85EF5_119)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D714E5457816BBE2A319AAC07D2C587A676A85EF5_119() const { return ___U24fieldU2D714E5457816BBE2A319AAC07D2C587A676A85EF5_119; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D714E5457816BBE2A319AAC07D2C587A676A85EF5_119() { return &___U24fieldU2D714E5457816BBE2A319AAC07D2C587A676A85EF5_119; }
	inline void set_U24fieldU2D714E5457816BBE2A319AAC07D2C587A676A85EF5_119(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D714E5457816BBE2A319AAC07D2C587A676A85EF5_119 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DBCC954BE08EE00F495F849D2F161752479934142_120() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DBCC954BE08EE00F495F849D2F161752479934142_120)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DBCC954BE08EE00F495F849D2F161752479934142_120() const { return ___U24fieldU2DBCC954BE08EE00F495F849D2F161752479934142_120; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DBCC954BE08EE00F495F849D2F161752479934142_120() { return &___U24fieldU2DBCC954BE08EE00F495F849D2F161752479934142_120; }
	inline void set_U24fieldU2DBCC954BE08EE00F495F849D2F161752479934142_120(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DBCC954BE08EE00F495F849D2F161752479934142_120 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF2CBB55BC65F72A214C8CF9323FBED9861F2F4BC_121() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DF2CBB55BC65F72A214C8CF9323FBED9861F2F4BC_121)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DF2CBB55BC65F72A214C8CF9323FBED9861F2F4BC_121() const { return ___U24fieldU2DF2CBB55BC65F72A214C8CF9323FBED9861F2F4BC_121; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DF2CBB55BC65F72A214C8CF9323FBED9861F2F4BC_121() { return &___U24fieldU2DF2CBB55BC65F72A214C8CF9323FBED9861F2F4BC_121; }
	inline void set_U24fieldU2DF2CBB55BC65F72A214C8CF9323FBED9861F2F4BC_121(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DF2CBB55BC65F72A214C8CF9323FBED9861F2F4BC_121 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1CDE377C407F39C592524FDAFE062059B9165E3F_122() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D1CDE377C407F39C592524FDAFE062059B9165E3F_122)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D1CDE377C407F39C592524FDAFE062059B9165E3F_122() const { return ___U24fieldU2D1CDE377C407F39C592524FDAFE062059B9165E3F_122; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D1CDE377C407F39C592524FDAFE062059B9165E3F_122() { return &___U24fieldU2D1CDE377C407F39C592524FDAFE062059B9165E3F_122; }
	inline void set_U24fieldU2D1CDE377C407F39C592524FDAFE062059B9165E3F_122(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D1CDE377C407F39C592524FDAFE062059B9165E3F_122 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6BECC23C94DC5127540ABE6CE7BE88684C295C5D_123() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D6BECC23C94DC5127540ABE6CE7BE88684C295C5D_123)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D6BECC23C94DC5127540ABE6CE7BE88684C295C5D_123() const { return ___U24fieldU2D6BECC23C94DC5127540ABE6CE7BE88684C295C5D_123; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D6BECC23C94DC5127540ABE6CE7BE88684C295C5D_123() { return &___U24fieldU2D6BECC23C94DC5127540ABE6CE7BE88684C295C5D_123; }
	inline void set_U24fieldU2D6BECC23C94DC5127540ABE6CE7BE88684C295C5D_123(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D6BECC23C94DC5127540ABE6CE7BE88684C295C5D_123 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D928F91AB9C19903D2B1009F932437DFCA5CDC584_124() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D928F91AB9C19903D2B1009F932437DFCA5CDC584_124)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D928F91AB9C19903D2B1009F932437DFCA5CDC584_124() const { return ___U24fieldU2D928F91AB9C19903D2B1009F932437DFCA5CDC584_124; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D928F91AB9C19903D2B1009F932437DFCA5CDC584_124() { return &___U24fieldU2D928F91AB9C19903D2B1009F932437DFCA5CDC584_124; }
	inline void set_U24fieldU2D928F91AB9C19903D2B1009F932437DFCA5CDC584_124(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D928F91AB9C19903D2B1009F932437DFCA5CDC584_124 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF83C96E93F812D711747D3CC98683620B651BC5C_125() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DF83C96E93F812D711747D3CC98683620B651BC5C_125)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DF83C96E93F812D711747D3CC98683620B651BC5C_125() const { return ___U24fieldU2DF83C96E93F812D711747D3CC98683620B651BC5C_125; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DF83C96E93F812D711747D3CC98683620B651BC5C_125() { return &___U24fieldU2DF83C96E93F812D711747D3CC98683620B651BC5C_125; }
	inline void set_U24fieldU2DF83C96E93F812D711747D3CC98683620B651BC5C_125(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DF83C96E93F812D711747D3CC98683620B651BC5C_125 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D12A155BF457332BF81F6684CCE56CC9C2A3A5076_126() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D12A155BF457332BF81F6684CCE56CC9C2A3A5076_126)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D12A155BF457332BF81F6684CCE56CC9C2A3A5076_126() const { return ___U24fieldU2D12A155BF457332BF81F6684CCE56CC9C2A3A5076_126; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D12A155BF457332BF81F6684CCE56CC9C2A3A5076_126() { return &___U24fieldU2D12A155BF457332BF81F6684CCE56CC9C2A3A5076_126; }
	inline void set_U24fieldU2D12A155BF457332BF81F6684CCE56CC9C2A3A5076_126(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D12A155BF457332BF81F6684CCE56CC9C2A3A5076_126 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D0D1AE827D0A9B6FD7B028D3A6670BF59BCA86701_127() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D0D1AE827D0A9B6FD7B028D3A6670BF59BCA86701_127)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D0D1AE827D0A9B6FD7B028D3A6670BF59BCA86701_127() const { return ___U24fieldU2D0D1AE827D0A9B6FD7B028D3A6670BF59BCA86701_127; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D0D1AE827D0A9B6FD7B028D3A6670BF59BCA86701_127() { return &___U24fieldU2D0D1AE827D0A9B6FD7B028D3A6670BF59BCA86701_127; }
	inline void set_U24fieldU2D0D1AE827D0A9B6FD7B028D3A6670BF59BCA86701_127(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D0D1AE827D0A9B6FD7B028D3A6670BF59BCA86701_127 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6E2460C8BE8220FB196B278A5D70BC3556975E55_128() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D6E2460C8BE8220FB196B278A5D70BC3556975E55_128)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D6E2460C8BE8220FB196B278A5D70BC3556975E55_128() const { return ___U24fieldU2D6E2460C8BE8220FB196B278A5D70BC3556975E55_128; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D6E2460C8BE8220FB196B278A5D70BC3556975E55_128() { return &___U24fieldU2D6E2460C8BE8220FB196B278A5D70BC3556975E55_128; }
	inline void set_U24fieldU2D6E2460C8BE8220FB196B278A5D70BC3556975E55_128(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D6E2460C8BE8220FB196B278A5D70BC3556975E55_128 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D48322419F79C852FFF1227DCB802B7C8CE2236EB_129() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D48322419F79C852FFF1227DCB802B7C8CE2236EB_129)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D48322419F79C852FFF1227DCB802B7C8CE2236EB_129() const { return ___U24fieldU2D48322419F79C852FFF1227DCB802B7C8CE2236EB_129; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D48322419F79C852FFF1227DCB802B7C8CE2236EB_129() { return &___U24fieldU2D48322419F79C852FFF1227DCB802B7C8CE2236EB_129; }
	inline void set_U24fieldU2D48322419F79C852FFF1227DCB802B7C8CE2236EB_129(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D48322419F79C852FFF1227DCB802B7C8CE2236EB_129 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DDE4B828B867A48EAB4617DC73A150B02BD24E72C_130() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DDE4B828B867A48EAB4617DC73A150B02BD24E72C_130)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DDE4B828B867A48EAB4617DC73A150B02BD24E72C_130() const { return ___U24fieldU2DDE4B828B867A48EAB4617DC73A150B02BD24E72C_130; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DDE4B828B867A48EAB4617DC73A150B02BD24E72C_130() { return &___U24fieldU2DDE4B828B867A48EAB4617DC73A150B02BD24E72C_130; }
	inline void set_U24fieldU2DDE4B828B867A48EAB4617DC73A150B02BD24E72C_130(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DDE4B828B867A48EAB4617DC73A150B02BD24E72C_130 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCD6B5BC785A3A5AFE10307374DB684F6132B1C57_131() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DCD6B5BC785A3A5AFE10307374DB684F6132B1C57_131)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DCD6B5BC785A3A5AFE10307374DB684F6132B1C57_131() const { return ___U24fieldU2DCD6B5BC785A3A5AFE10307374DB684F6132B1C57_131; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DCD6B5BC785A3A5AFE10307374DB684F6132B1C57_131() { return &___U24fieldU2DCD6B5BC785A3A5AFE10307374DB684F6132B1C57_131; }
	inline void set_U24fieldU2DCD6B5BC785A3A5AFE10307374DB684F6132B1C57_131(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DCD6B5BC785A3A5AFE10307374DB684F6132B1C57_131 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC420C0CA6FC58CFAADB02A5DA53391092FBB894A_132() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DC420C0CA6FC58CFAADB02A5DA53391092FBB894A_132)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DC420C0CA6FC58CFAADB02A5DA53391092FBB894A_132() const { return ___U24fieldU2DC420C0CA6FC58CFAADB02A5DA53391092FBB894A_132; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DC420C0CA6FC58CFAADB02A5DA53391092FBB894A_132() { return &___U24fieldU2DC420C0CA6FC58CFAADB02A5DA53391092FBB894A_132; }
	inline void set_U24fieldU2DC420C0CA6FC58CFAADB02A5DA53391092FBB894A_132(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DC420C0CA6FC58CFAADB02A5DA53391092FBB894A_132 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE636A7059162508F5B0029ABA2C0826FBC218247_133() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DE636A7059162508F5B0029ABA2C0826FBC218247_133)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DE636A7059162508F5B0029ABA2C0826FBC218247_133() const { return ___U24fieldU2DE636A7059162508F5B0029ABA2C0826FBC218247_133; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DE636A7059162508F5B0029ABA2C0826FBC218247_133() { return &___U24fieldU2DE636A7059162508F5B0029ABA2C0826FBC218247_133; }
	inline void set_U24fieldU2DE636A7059162508F5B0029ABA2C0826FBC218247_133(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DE636A7059162508F5B0029ABA2C0826FBC218247_133 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD6D3C6667F50B055D22DD3D8FAE411326678A71F_134() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD6D3C6667F50B055D22DD3D8FAE411326678A71F_134)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DD6D3C6667F50B055D22DD3D8FAE411326678A71F_134() const { return ___U24fieldU2DD6D3C6667F50B055D22DD3D8FAE411326678A71F_134; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DD6D3C6667F50B055D22DD3D8FAE411326678A71F_134() { return &___U24fieldU2DD6D3C6667F50B055D22DD3D8FAE411326678A71F_134; }
	inline void set_U24fieldU2DD6D3C6667F50B055D22DD3D8FAE411326678A71F_134(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DD6D3C6667F50B055D22DD3D8FAE411326678A71F_134 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCD7F453E7DAD8B3B8D8AB82971B07921BFC4A3D3_135() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DCD7F453E7DAD8B3B8D8AB82971B07921BFC4A3D3_135)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DCD7F453E7DAD8B3B8D8AB82971B07921BFC4A3D3_135() const { return ___U24fieldU2DCD7F453E7DAD8B3B8D8AB82971B07921BFC4A3D3_135; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DCD7F453E7DAD8B3B8D8AB82971B07921BFC4A3D3_135() { return &___U24fieldU2DCD7F453E7DAD8B3B8D8AB82971B07921BFC4A3D3_135; }
	inline void set_U24fieldU2DCD7F453E7DAD8B3B8D8AB82971B07921BFC4A3D3_135(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DCD7F453E7DAD8B3B8D8AB82971B07921BFC4A3D3_135 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE6570482AA640720A812DC9F4302ACCFF92B93F1_136() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DE6570482AA640720A812DC9F4302ACCFF92B93F1_136)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DE6570482AA640720A812DC9F4302ACCFF92B93F1_136() const { return ___U24fieldU2DE6570482AA640720A812DC9F4302ACCFF92B93F1_136; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DE6570482AA640720A812DC9F4302ACCFF92B93F1_136() { return &___U24fieldU2DE6570482AA640720A812DC9F4302ACCFF92B93F1_136; }
	inline void set_U24fieldU2DE6570482AA640720A812DC9F4302ACCFF92B93F1_136(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DE6570482AA640720A812DC9F4302ACCFF92B93F1_136 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCEAA201847F942506CE4A6D33EB8D87AF00B4AA0_137() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DCEAA201847F942506CE4A6D33EB8D87AF00B4AA0_137)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DCEAA201847F942506CE4A6D33EB8D87AF00B4AA0_137() const { return ___U24fieldU2DCEAA201847F942506CE4A6D33EB8D87AF00B4AA0_137; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DCEAA201847F942506CE4A6D33EB8D87AF00B4AA0_137() { return &___U24fieldU2DCEAA201847F942506CE4A6D33EB8D87AF00B4AA0_137; }
	inline void set_U24fieldU2DCEAA201847F942506CE4A6D33EB8D87AF00B4AA0_137(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DCEAA201847F942506CE4A6D33EB8D87AF00B4AA0_137 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D242F9F981726198546AE22426078FC2D0AFDFC7D_138() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D242F9F981726198546AE22426078FC2D0AFDFC7D_138)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D242F9F981726198546AE22426078FC2D0AFDFC7D_138() const { return ___U24fieldU2D242F9F981726198546AE22426078FC2D0AFDFC7D_138; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D242F9F981726198546AE22426078FC2D0AFDFC7D_138() { return &___U24fieldU2D242F9F981726198546AE22426078FC2D0AFDFC7D_138; }
	inline void set_U24fieldU2D242F9F981726198546AE22426078FC2D0AFDFC7D_138(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D242F9F981726198546AE22426078FC2D0AFDFC7D_138 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD178CEED7BBEBC7AA0DA1484363B0ADC369532B1_139() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD178CEED7BBEBC7AA0DA1484363B0ADC369532B1_139)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DD178CEED7BBEBC7AA0DA1484363B0ADC369532B1_139() const { return ___U24fieldU2DD178CEED7BBEBC7AA0DA1484363B0ADC369532B1_139; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DD178CEED7BBEBC7AA0DA1484363B0ADC369532B1_139() { return &___U24fieldU2DD178CEED7BBEBC7AA0DA1484363B0ADC369532B1_139; }
	inline void set_U24fieldU2DD178CEED7BBEBC7AA0DA1484363B0ADC369532B1_139(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DD178CEED7BBEBC7AA0DA1484363B0ADC369532B1_139 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D535985E95CA011ACE58FC3BEE4E4270F4F761478_140() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D535985E95CA011ACE58FC3BEE4E4270F4F761478_140)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D535985E95CA011ACE58FC3BEE4E4270F4F761478_140() const { return ___U24fieldU2D535985E95CA011ACE58FC3BEE4E4270F4F761478_140; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D535985E95CA011ACE58FC3BEE4E4270F4F761478_140() { return &___U24fieldU2D535985E95CA011ACE58FC3BEE4E4270F4F761478_140; }
	inline void set_U24fieldU2D535985E95CA011ACE58FC3BEE4E4270F4F761478_140(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D535985E95CA011ACE58FC3BEE4E4270F4F761478_140 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8BD44E6B764A64B6DE9F115D610775748364EB71_141() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D8BD44E6B764A64B6DE9F115D610775748364EB71_141)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D8BD44E6B764A64B6DE9F115D610775748364EB71_141() const { return ___U24fieldU2D8BD44E6B764A64B6DE9F115D610775748364EB71_141; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D8BD44E6B764A64B6DE9F115D610775748364EB71_141() { return &___U24fieldU2D8BD44E6B764A64B6DE9F115D610775748364EB71_141; }
	inline void set_U24fieldU2D8BD44E6B764A64B6DE9F115D610775748364EB71_141(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D8BD44E6B764A64B6DE9F115D610775748364EB71_141 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D74BBA41B92CCB948BD0B17E1D9DE0DA8BEFE2304_142() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D74BBA41B92CCB948BD0B17E1D9DE0DA8BEFE2304_142)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D74BBA41B92CCB948BD0B17E1D9DE0DA8BEFE2304_142() const { return ___U24fieldU2D74BBA41B92CCB948BD0B17E1D9DE0DA8BEFE2304_142; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D74BBA41B92CCB948BD0B17E1D9DE0DA8BEFE2304_142() { return &___U24fieldU2D74BBA41B92CCB948BD0B17E1D9DE0DA8BEFE2304_142; }
	inline void set_U24fieldU2D74BBA41B92CCB948BD0B17E1D9DE0DA8BEFE2304_142(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D74BBA41B92CCB948BD0B17E1D9DE0DA8BEFE2304_142 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D52DD1DEF5C43780A82B460C7C23188B55B918057_143() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D52DD1DEF5C43780A82B460C7C23188B55B918057_143)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D52DD1DEF5C43780A82B460C7C23188B55B918057_143() const { return ___U24fieldU2D52DD1DEF5C43780A82B460C7C23188B55B918057_143; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D52DD1DEF5C43780A82B460C7C23188B55B918057_143() { return &___U24fieldU2D52DD1DEF5C43780A82B460C7C23188B55B918057_143; }
	inline void set_U24fieldU2D52DD1DEF5C43780A82B460C7C23188B55B918057_143(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D52DD1DEF5C43780A82B460C7C23188B55B918057_143 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAB8935CA29ED45F3F387F2EB8823E19EC88661DA_144() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DAB8935CA29ED45F3F387F2EB8823E19EC88661DA_144)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DAB8935CA29ED45F3F387F2EB8823E19EC88661DA_144() const { return ___U24fieldU2DAB8935CA29ED45F3F387F2EB8823E19EC88661DA_144; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DAB8935CA29ED45F3F387F2EB8823E19EC88661DA_144() { return &___U24fieldU2DAB8935CA29ED45F3F387F2EB8823E19EC88661DA_144; }
	inline void set_U24fieldU2DAB8935CA29ED45F3F387F2EB8823E19EC88661DA_144(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DAB8935CA29ED45F3F387F2EB8823E19EC88661DA_144 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE2EC72AD9A1FECD786D35F2F342F03AD95B14DD4_145() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DE2EC72AD9A1FECD786D35F2F342F03AD95B14DD4_145)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DE2EC72AD9A1FECD786D35F2F342F03AD95B14DD4_145() const { return ___U24fieldU2DE2EC72AD9A1FECD786D35F2F342F03AD95B14DD4_145; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DE2EC72AD9A1FECD786D35F2F342F03AD95B14DD4_145() { return &___U24fieldU2DE2EC72AD9A1FECD786D35F2F342F03AD95B14DD4_145; }
	inline void set_U24fieldU2DE2EC72AD9A1FECD786D35F2F342F03AD95B14DD4_145(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DE2EC72AD9A1FECD786D35F2F342F03AD95B14DD4_145 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D630C3F5D7DBB07B36134EB1AFA096BAA59350F0C_146() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D630C3F5D7DBB07B36134EB1AFA096BAA59350F0C_146)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D630C3F5D7DBB07B36134EB1AFA096BAA59350F0C_146() const { return ___U24fieldU2D630C3F5D7DBB07B36134EB1AFA096BAA59350F0C_146; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D630C3F5D7DBB07B36134EB1AFA096BAA59350F0C_146() { return &___U24fieldU2D630C3F5D7DBB07B36134EB1AFA096BAA59350F0C_146; }
	inline void set_U24fieldU2D630C3F5D7DBB07B36134EB1AFA096BAA59350F0C_146(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D630C3F5D7DBB07B36134EB1AFA096BAA59350F0C_146 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD26254A9468BBBB28AC07899A05D759FD80FE5C5_147() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD26254A9468BBBB28AC07899A05D759FD80FE5C5_147)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DD26254A9468BBBB28AC07899A05D759FD80FE5C5_147() const { return ___U24fieldU2DD26254A9468BBBB28AC07899A05D759FD80FE5C5_147; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DD26254A9468BBBB28AC07899A05D759FD80FE5C5_147() { return &___U24fieldU2DD26254A9468BBBB28AC07899A05D759FD80FE5C5_147; }
	inline void set_U24fieldU2DD26254A9468BBBB28AC07899A05D759FD80FE5C5_147(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DD26254A9468BBBB28AC07899A05D759FD80FE5C5_147 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8DC2ABCA7A46A411D39035D8806FBA7EB22432FB_148() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D8DC2ABCA7A46A411D39035D8806FBA7EB22432FB_148)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D8DC2ABCA7A46A411D39035D8806FBA7EB22432FB_148() const { return ___U24fieldU2D8DC2ABCA7A46A411D39035D8806FBA7EB22432FB_148; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D8DC2ABCA7A46A411D39035D8806FBA7EB22432FB_148() { return &___U24fieldU2D8DC2ABCA7A46A411D39035D8806FBA7EB22432FB_148; }
	inline void set_U24fieldU2D8DC2ABCA7A46A411D39035D8806FBA7EB22432FB_148(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D8DC2ABCA7A46A411D39035D8806FBA7EB22432FB_148 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB75CE37738CED7D6746D306511B391A9F91684D0_149() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DB75CE37738CED7D6746D306511B391A9F91684D0_149)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DB75CE37738CED7D6746D306511B391A9F91684D0_149() const { return ___U24fieldU2DB75CE37738CED7D6746D306511B391A9F91684D0_149; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DB75CE37738CED7D6746D306511B391A9F91684D0_149() { return &___U24fieldU2DB75CE37738CED7D6746D306511B391A9F91684D0_149; }
	inline void set_U24fieldU2DB75CE37738CED7D6746D306511B391A9F91684D0_149(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DB75CE37738CED7D6746D306511B391A9F91684D0_149 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D980BDEDFAACFEB591A358FAFC522C3233E8414D8_150() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D980BDEDFAACFEB591A358FAFC522C3233E8414D8_150)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D980BDEDFAACFEB591A358FAFC522C3233E8414D8_150() const { return ___U24fieldU2D980BDEDFAACFEB591A358FAFC522C3233E8414D8_150; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D980BDEDFAACFEB591A358FAFC522C3233E8414D8_150() { return &___U24fieldU2D980BDEDFAACFEB591A358FAFC522C3233E8414D8_150; }
	inline void set_U24fieldU2D980BDEDFAACFEB591A358FAFC522C3233E8414D8_150(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D980BDEDFAACFEB591A358FAFC522C3233E8414D8_150 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3DFDD69B416E892411930CA44E469FC0EE1A748E_151() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D3DFDD69B416E892411930CA44E469FC0EE1A748E_151)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D3DFDD69B416E892411930CA44E469FC0EE1A748E_151() const { return ___U24fieldU2D3DFDD69B416E892411930CA44E469FC0EE1A748E_151; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D3DFDD69B416E892411930CA44E469FC0EE1A748E_151() { return &___U24fieldU2D3DFDD69B416E892411930CA44E469FC0EE1A748E_151; }
	inline void set_U24fieldU2D3DFDD69B416E892411930CA44E469FC0EE1A748E_151(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D3DFDD69B416E892411930CA44E469FC0EE1A748E_151 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9A7664FF66C09B23E2674031BADDADA42C2E7338_152() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D9A7664FF66C09B23E2674031BADDADA42C2E7338_152)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D9A7664FF66C09B23E2674031BADDADA42C2E7338_152() const { return ___U24fieldU2D9A7664FF66C09B23E2674031BADDADA42C2E7338_152; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D9A7664FF66C09B23E2674031BADDADA42C2E7338_152() { return &___U24fieldU2D9A7664FF66C09B23E2674031BADDADA42C2E7338_152; }
	inline void set_U24fieldU2D9A7664FF66C09B23E2674031BADDADA42C2E7338_152(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D9A7664FF66C09B23E2674031BADDADA42C2E7338_152 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D950965F7AE6D3878D95DB10B185EC9969281928B_153() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D950965F7AE6D3878D95DB10B185EC9969281928B_153)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D950965F7AE6D3878D95DB10B185EC9969281928B_153() const { return ___U24fieldU2D950965F7AE6D3878D95DB10B185EC9969281928B_153; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D950965F7AE6D3878D95DB10B185EC9969281928B_153() { return &___U24fieldU2D950965F7AE6D3878D95DB10B185EC9969281928B_153; }
	inline void set_U24fieldU2D950965F7AE6D3878D95DB10B185EC9969281928B_153(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D950965F7AE6D3878D95DB10B185EC9969281928B_153 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE37060512A6142D8BDB50F96F5A31B3F7FD9C811_154() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DE37060512A6142D8BDB50F96F5A31B3F7FD9C811_154)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DE37060512A6142D8BDB50F96F5A31B3F7FD9C811_154() const { return ___U24fieldU2DE37060512A6142D8BDB50F96F5A31B3F7FD9C811_154; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DE37060512A6142D8BDB50F96F5A31B3F7FD9C811_154() { return &___U24fieldU2DE37060512A6142D8BDB50F96F5A31B3F7FD9C811_154; }
	inline void set_U24fieldU2DE37060512A6142D8BDB50F96F5A31B3F7FD9C811_154(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DE37060512A6142D8BDB50F96F5A31B3F7FD9C811_154 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCFD89EB7C35C2C4305E84198CEE1E467C5B685FC_155() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DCFD89EB7C35C2C4305E84198CEE1E467C5B685FC_155)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DCFD89EB7C35C2C4305E84198CEE1E467C5B685FC_155() const { return ___U24fieldU2DCFD89EB7C35C2C4305E84198CEE1E467C5B685FC_155; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DCFD89EB7C35C2C4305E84198CEE1E467C5B685FC_155() { return &___U24fieldU2DCFD89EB7C35C2C4305E84198CEE1E467C5B685FC_155; }
	inline void set_U24fieldU2DCFD89EB7C35C2C4305E84198CEE1E467C5B685FC_155(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DCFD89EB7C35C2C4305E84198CEE1E467C5B685FC_155 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE4692158166F085D6DB6AA88DD688573157DDE59_156() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DE4692158166F085D6DB6AA88DD688573157DDE59_156)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DE4692158166F085D6DB6AA88DD688573157DDE59_156() const { return ___U24fieldU2DE4692158166F085D6DB6AA88DD688573157DDE59_156; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DE4692158166F085D6DB6AA88DD688573157DDE59_156() { return &___U24fieldU2DE4692158166F085D6DB6AA88DD688573157DDE59_156; }
	inline void set_U24fieldU2DE4692158166F085D6DB6AA88DD688573157DDE59_156(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DE4692158166F085D6DB6AA88DD688573157DDE59_156 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE3B39965C126C790271ABFAAC5BAF0CB3544F01C_157() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DE3B39965C126C790271ABFAAC5BAF0CB3544F01C_157)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DE3B39965C126C790271ABFAAC5BAF0CB3544F01C_157() const { return ___U24fieldU2DE3B39965C126C790271ABFAAC5BAF0CB3544F01C_157; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DE3B39965C126C790271ABFAAC5BAF0CB3544F01C_157() { return &___U24fieldU2DE3B39965C126C790271ABFAAC5BAF0CB3544F01C_157; }
	inline void set_U24fieldU2DE3B39965C126C790271ABFAAC5BAF0CB3544F01C_157(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DE3B39965C126C790271ABFAAC5BAF0CB3544F01C_157 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE69D5483E961E76DC16F84DD4D0CF8A1E8AEBFED_158() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DE69D5483E961E76DC16F84DD4D0CF8A1E8AEBFED_158)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2DE69D5483E961E76DC16F84DD4D0CF8A1E8AEBFED_158() const { return ___U24fieldU2DE69D5483E961E76DC16F84DD4D0CF8A1E8AEBFED_158; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2DE69D5483E961E76DC16F84DD4D0CF8A1E8AEBFED_158() { return &___U24fieldU2DE69D5483E961E76DC16F84DD4D0CF8A1E8AEBFED_158; }
	inline void set_U24fieldU2DE69D5483E961E76DC16F84DD4D0CF8A1E8AEBFED_158(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2DE69D5483E961E76DC16F84DD4D0CF8A1E8AEBFED_158 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D263ADBD7E0DAA10E11AAF3AA902BE6511DBEA61F_159() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D263ADBD7E0DAA10E11AAF3AA902BE6511DBEA61F_159)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D263ADBD7E0DAA10E11AAF3AA902BE6511DBEA61F_159() const { return ___U24fieldU2D263ADBD7E0DAA10E11AAF3AA902BE6511DBEA61F_159; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D263ADBD7E0DAA10E11AAF3AA902BE6511DBEA61F_159() { return &___U24fieldU2D263ADBD7E0DAA10E11AAF3AA902BE6511DBEA61F_159; }
	inline void set_U24fieldU2D263ADBD7E0DAA10E11AAF3AA902BE6511DBEA61F_159(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D263ADBD7E0DAA10E11AAF3AA902BE6511DBEA61F_159 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D823ECB532404CAC8E6628819EAB525132A3C4CC3_160() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D823ECB532404CAC8E6628819EAB525132A3C4CC3_160)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D823ECB532404CAC8E6628819EAB525132A3C4CC3_160() const { return ___U24fieldU2D823ECB532404CAC8E6628819EAB525132A3C4CC3_160; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D823ECB532404CAC8E6628819EAB525132A3C4CC3_160() { return &___U24fieldU2D823ECB532404CAC8E6628819EAB525132A3C4CC3_160; }
	inline void set_U24fieldU2D823ECB532404CAC8E6628819EAB525132A3C4CC3_160(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D823ECB532404CAC8E6628819EAB525132A3C4CC3_160 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D66175F96A3A979C0479D04059A97B4F0D1545770_161() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D66175F96A3A979C0479D04059A97B4F0D1545770_161)); }
	inline U24ArrayTypeU3D56_t1283759776  get_U24fieldU2D66175F96A3A979C0479D04059A97B4F0D1545770_161() const { return ___U24fieldU2D66175F96A3A979C0479D04059A97B4F0D1545770_161; }
	inline U24ArrayTypeU3D56_t1283759776 * get_address_of_U24fieldU2D66175F96A3A979C0479D04059A97B4F0D1545770_161() { return &___U24fieldU2D66175F96A3A979C0479D04059A97B4F0D1545770_161; }
	inline void set_U24fieldU2D66175F96A3A979C0479D04059A97B4F0D1545770_161(U24ArrayTypeU3D56_t1283759776  value)
	{
		___U24fieldU2D66175F96A3A979C0479D04059A97B4F0D1545770_161 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA5E465DCCD6CF445B12C62AFC303C659C6E78B26_162() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DA5E465DCCD6CF445B12C62AFC303C659C6E78B26_162)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2DA5E465DCCD6CF445B12C62AFC303C659C6E78B26_162() const { return ___U24fieldU2DA5E465DCCD6CF445B12C62AFC303C659C6E78B26_162; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2DA5E465DCCD6CF445B12C62AFC303C659C6E78B26_162() { return &___U24fieldU2DA5E465DCCD6CF445B12C62AFC303C659C6E78B26_162; }
	inline void set_U24fieldU2DA5E465DCCD6CF445B12C62AFC303C659C6E78B26_162(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2DA5E465DCCD6CF445B12C62AFC303C659C6E78B26_162 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB14CD063151CEC59EEB0F8E2557A7CF6B9D833BA_163() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DB14CD063151CEC59EEB0F8E2557A7CF6B9D833BA_163)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2DB14CD063151CEC59EEB0F8E2557A7CF6B9D833BA_163() const { return ___U24fieldU2DB14CD063151CEC59EEB0F8E2557A7CF6B9D833BA_163; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2DB14CD063151CEC59EEB0F8E2557A7CF6B9D833BA_163() { return &___U24fieldU2DB14CD063151CEC59EEB0F8E2557A7CF6B9D833BA_163; }
	inline void set_U24fieldU2DB14CD063151CEC59EEB0F8E2557A7CF6B9D833BA_163(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2DB14CD063151CEC59EEB0F8E2557A7CF6B9D833BA_163 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D62EBB675E18E46D952C37A995181D23A18F84CD4_164() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D62EBB675E18E46D952C37A995181D23A18F84CD4_164)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D62EBB675E18E46D952C37A995181D23A18F84CD4_164() const { return ___U24fieldU2D62EBB675E18E46D952C37A995181D23A18F84CD4_164; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D62EBB675E18E46D952C37A995181D23A18F84CD4_164() { return &___U24fieldU2D62EBB675E18E46D952C37A995181D23A18F84CD4_164; }
	inline void set_U24fieldU2D62EBB675E18E46D952C37A995181D23A18F84CD4_164(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D62EBB675E18E46D952C37A995181D23A18F84CD4_164 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5F214E20357C7CC02D9BB842C0626F580CA6200E_165() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D5F214E20357C7CC02D9BB842C0626F580CA6200E_165)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D5F214E20357C7CC02D9BB842C0626F580CA6200E_165() const { return ___U24fieldU2D5F214E20357C7CC02D9BB842C0626F580CA6200E_165; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D5F214E20357C7CC02D9BB842C0626F580CA6200E_165() { return &___U24fieldU2D5F214E20357C7CC02D9BB842C0626F580CA6200E_165; }
	inline void set_U24fieldU2D5F214E20357C7CC02D9BB842C0626F580CA6200E_165(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D5F214E20357C7CC02D9BB842C0626F580CA6200E_165 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8834238153C27D69F7A59F6EAE1405F77FE45409_166() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D8834238153C27D69F7A59F6EAE1405F77FE45409_166)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D8834238153C27D69F7A59F6EAE1405F77FE45409_166() const { return ___U24fieldU2D8834238153C27D69F7A59F6EAE1405F77FE45409_166; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D8834238153C27D69F7A59F6EAE1405F77FE45409_166() { return &___U24fieldU2D8834238153C27D69F7A59F6EAE1405F77FE45409_166; }
	inline void set_U24fieldU2D8834238153C27D69F7A59F6EAE1405F77FE45409_166(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D8834238153C27D69F7A59F6EAE1405F77FE45409_166 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D149F522CB3B9C44EB981E8BC2540D606F1CDCD87_167() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D149F522CB3B9C44EB981E8BC2540D606F1CDCD87_167)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D149F522CB3B9C44EB981E8BC2540D606F1CDCD87_167() const { return ___U24fieldU2D149F522CB3B9C44EB981E8BC2540D606F1CDCD87_167; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D149F522CB3B9C44EB981E8BC2540D606F1CDCD87_167() { return &___U24fieldU2D149F522CB3B9C44EB981E8BC2540D606F1CDCD87_167; }
	inline void set_U24fieldU2D149F522CB3B9C44EB981E8BC2540D606F1CDCD87_167(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D149F522CB3B9C44EB981E8BC2540D606F1CDCD87_167 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D231A0B5CEF9DCFCB9D64B36C1F48EFABC84C7F49_168() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D231A0B5CEF9DCFCB9D64B36C1F48EFABC84C7F49_168)); }
	inline U24ArrayTypeU3D32_t3651253610  get_U24fieldU2D231A0B5CEF9DCFCB9D64B36C1F48EFABC84C7F49_168() const { return ___U24fieldU2D231A0B5CEF9DCFCB9D64B36C1F48EFABC84C7F49_168; }
	inline U24ArrayTypeU3D32_t3651253610 * get_address_of_U24fieldU2D231A0B5CEF9DCFCB9D64B36C1F48EFABC84C7F49_168() { return &___U24fieldU2D231A0B5CEF9DCFCB9D64B36C1F48EFABC84C7F49_168; }
	inline void set_U24fieldU2D231A0B5CEF9DCFCB9D64B36C1F48EFABC84C7F49_168(U24ArrayTypeU3D32_t3651253610  value)
	{
		___U24fieldU2D231A0B5CEF9DCFCB9D64B36C1F48EFABC84C7F49_168 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_169() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_169)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_169() const { return ___U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_169; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_169() { return &___U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_169; }
	inline void set_U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_169(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_169 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8CFA957D76B6E190580D284C12F31AA6E3E2D41C_170() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D8CFA957D76B6E190580D284C12F31AA6E3E2D41C_170)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D8CFA957D76B6E190580D284C12F31AA6E3E2D41C_170() const { return ___U24fieldU2D8CFA957D76B6E190580D284C12F31AA6E3E2D41C_170; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D8CFA957D76B6E190580D284C12F31AA6E3E2D41C_170() { return &___U24fieldU2D8CFA957D76B6E190580D284C12F31AA6E3E2D41C_170; }
	inline void set_U24fieldU2D8CFA957D76B6E190580D284C12F31AA6E3E2D41C_170(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D8CFA957D76B6E190580D284C12F31AA6E3E2D41C_170 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef PLAYDURINGPHASE_T3488456769_H
#define PLAYDURINGPHASE_T3488456769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayDuringPhase
struct  PlayDuringPhase_t3488456769 
{
public:
	// System.Int32 PlayDuringPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayDuringPhase_t3488456769, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYDURINGPHASE_T3488456769_H
#ifndef ZOMBIETURNPHASENAME_T860305160_H
#define ZOMBIETURNPHASENAME_T860305160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurnPhaseName
struct  ZombieTurnPhaseName_t860305160 
{
public:
	// System.Int32 ZombieTurnPhaseName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ZombieTurnPhaseName_t860305160, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIETURNPHASENAME_T860305160_H
#ifndef U3CCHECKIFINHANDU3EC__ANONSTOREY1_T2015387922_H
#define U3CCHECKIFINHANDU3EC__ANONSTOREY1_T2015387922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieDeck/<CheckIfInHand>c__AnonStorey1
struct  U3CCheckIfInHandU3Ec__AnonStorey1_t2015387922  : public RuntimeObject
{
public:
	// PlayDuringPhase ZombieDeck/<CheckIfInHand>c__AnonStorey1::cardPhase
	int32_t ___cardPhase_0;

public:
	inline static int32_t get_offset_of_cardPhase_0() { return static_cast<int32_t>(offsetof(U3CCheckIfInHandU3Ec__AnonStorey1_t2015387922, ___cardPhase_0)); }
	inline int32_t get_cardPhase_0() const { return ___cardPhase_0; }
	inline int32_t* get_address_of_cardPhase_0() { return &___cardPhase_0; }
	inline void set_cardPhase_0(int32_t value)
	{
		___cardPhase_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKIFINHANDU3EC__ANONSTOREY1_T2015387922_H
#ifndef ZOMBIECARD_T2237653742_H
#define ZOMBIECARD_T2237653742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieCard
struct  ZombieCard_t2237653742  : public RuntimeObject
{
public:
	// System.String ZombieCard::Name
	String_t* ___Name_0;
	// System.String ZombieCard::RemainsInPlayName
	String_t* ___RemainsInPlayName_1;
	// PlayDuringPhase ZombieCard::Phase
	int32_t ___Phase_2;
	// System.String ZombieCard::OriginalText
	String_t* ___OriginalText_3;
	// System.String ZombieCard::FlavorText
	String_t* ___FlavorText_4;
	// System.Boolean ZombieCard::RemainsInPlay
	bool ___RemainsInPlay_5;
	// System.Boolean ZombieCard::tempCard
	bool ___tempCard_6;
	// System.Boolean ZombieCard::SkipCard
	bool ___SkipCard_7;
	// ZombieTurn ZombieCard::ZombieTurnManager
	ZombieTurn_t3562480803 * ___ZombieTurnManager_8;
	// Building ZombieCard::Building
	Building_t1411255995 * ___Building_9;
	// System.String ZombieCard::HeroToEffect
	String_t* ___HeroToEffect_10;
	// System.Int32 ZombieCard::NewZombies
	int32_t ___NewZombies_11;
	// System.Int32 ZombieCard::CardRollValue
	int32_t ___CardRollValue_12;
	// ZombieCard ZombieCard::CardToEffect
	ZombieCard_t2237653742 * ___CardToEffect_13;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_RemainsInPlayName_1() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___RemainsInPlayName_1)); }
	inline String_t* get_RemainsInPlayName_1() const { return ___RemainsInPlayName_1; }
	inline String_t** get_address_of_RemainsInPlayName_1() { return &___RemainsInPlayName_1; }
	inline void set_RemainsInPlayName_1(String_t* value)
	{
		___RemainsInPlayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___RemainsInPlayName_1), value);
	}

	inline static int32_t get_offset_of_Phase_2() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___Phase_2)); }
	inline int32_t get_Phase_2() const { return ___Phase_2; }
	inline int32_t* get_address_of_Phase_2() { return &___Phase_2; }
	inline void set_Phase_2(int32_t value)
	{
		___Phase_2 = value;
	}

	inline static int32_t get_offset_of_OriginalText_3() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___OriginalText_3)); }
	inline String_t* get_OriginalText_3() const { return ___OriginalText_3; }
	inline String_t** get_address_of_OriginalText_3() { return &___OriginalText_3; }
	inline void set_OriginalText_3(String_t* value)
	{
		___OriginalText_3 = value;
		Il2CppCodeGenWriteBarrier((&___OriginalText_3), value);
	}

	inline static int32_t get_offset_of_FlavorText_4() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___FlavorText_4)); }
	inline String_t* get_FlavorText_4() const { return ___FlavorText_4; }
	inline String_t** get_address_of_FlavorText_4() { return &___FlavorText_4; }
	inline void set_FlavorText_4(String_t* value)
	{
		___FlavorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___FlavorText_4), value);
	}

	inline static int32_t get_offset_of_RemainsInPlay_5() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___RemainsInPlay_5)); }
	inline bool get_RemainsInPlay_5() const { return ___RemainsInPlay_5; }
	inline bool* get_address_of_RemainsInPlay_5() { return &___RemainsInPlay_5; }
	inline void set_RemainsInPlay_5(bool value)
	{
		___RemainsInPlay_5 = value;
	}

	inline static int32_t get_offset_of_tempCard_6() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___tempCard_6)); }
	inline bool get_tempCard_6() const { return ___tempCard_6; }
	inline bool* get_address_of_tempCard_6() { return &___tempCard_6; }
	inline void set_tempCard_6(bool value)
	{
		___tempCard_6 = value;
	}

	inline static int32_t get_offset_of_SkipCard_7() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___SkipCard_7)); }
	inline bool get_SkipCard_7() const { return ___SkipCard_7; }
	inline bool* get_address_of_SkipCard_7() { return &___SkipCard_7; }
	inline void set_SkipCard_7(bool value)
	{
		___SkipCard_7 = value;
	}

	inline static int32_t get_offset_of_ZombieTurnManager_8() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___ZombieTurnManager_8)); }
	inline ZombieTurn_t3562480803 * get_ZombieTurnManager_8() const { return ___ZombieTurnManager_8; }
	inline ZombieTurn_t3562480803 ** get_address_of_ZombieTurnManager_8() { return &___ZombieTurnManager_8; }
	inline void set_ZombieTurnManager_8(ZombieTurn_t3562480803 * value)
	{
		___ZombieTurnManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieTurnManager_8), value);
	}

	inline static int32_t get_offset_of_Building_9() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___Building_9)); }
	inline Building_t1411255995 * get_Building_9() const { return ___Building_9; }
	inline Building_t1411255995 ** get_address_of_Building_9() { return &___Building_9; }
	inline void set_Building_9(Building_t1411255995 * value)
	{
		___Building_9 = value;
		Il2CppCodeGenWriteBarrier((&___Building_9), value);
	}

	inline static int32_t get_offset_of_HeroToEffect_10() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___HeroToEffect_10)); }
	inline String_t* get_HeroToEffect_10() const { return ___HeroToEffect_10; }
	inline String_t** get_address_of_HeroToEffect_10() { return &___HeroToEffect_10; }
	inline void set_HeroToEffect_10(String_t* value)
	{
		___HeroToEffect_10 = value;
		Il2CppCodeGenWriteBarrier((&___HeroToEffect_10), value);
	}

	inline static int32_t get_offset_of_NewZombies_11() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___NewZombies_11)); }
	inline int32_t get_NewZombies_11() const { return ___NewZombies_11; }
	inline int32_t* get_address_of_NewZombies_11() { return &___NewZombies_11; }
	inline void set_NewZombies_11(int32_t value)
	{
		___NewZombies_11 = value;
	}

	inline static int32_t get_offset_of_CardRollValue_12() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___CardRollValue_12)); }
	inline int32_t get_CardRollValue_12() const { return ___CardRollValue_12; }
	inline int32_t* get_address_of_CardRollValue_12() { return &___CardRollValue_12; }
	inline void set_CardRollValue_12(int32_t value)
	{
		___CardRollValue_12 = value;
	}

	inline static int32_t get_offset_of_CardToEffect_13() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___CardToEffect_13)); }
	inline ZombieCard_t2237653742 * get_CardToEffect_13() const { return ___CardToEffect_13; }
	inline ZombieCard_t2237653742 ** get_address_of_CardToEffect_13() { return &___CardToEffect_13; }
	inline void set_CardToEffect_13(ZombieCard_t2237653742 * value)
	{
		___CardToEffect_13 = value;
		Il2CppCodeGenWriteBarrier((&___CardToEffect_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIECARD_T2237653742_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ZOMBIETURNPHASES_T3127322021_H
#define ZOMBIETURNPHASES_T3127322021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurnPhases
struct  ZombieTurnPhases_t3127322021  : public RuntimeObject
{
public:
	// ZombieTurnPhases/TurnPhaseDelegateDeclare ZombieTurnPhases::TurnPhaseDelegate
	TurnPhaseDelegateDeclare_t3595605765 * ___TurnPhaseDelegate_0;
	// ZombieTurn ZombieTurnPhases::zombieTurn
	ZombieTurn_t3562480803 * ___zombieTurn_1;
	// System.Int32 ZombieTurnPhases::MoveZombieIndex
	int32_t ___MoveZombieIndex_2;
	// ZombieTurnPhaseName ZombieTurnPhases::PhaseName
	int32_t ___PhaseName_3;

public:
	inline static int32_t get_offset_of_TurnPhaseDelegate_0() { return static_cast<int32_t>(offsetof(ZombieTurnPhases_t3127322021, ___TurnPhaseDelegate_0)); }
	inline TurnPhaseDelegateDeclare_t3595605765 * get_TurnPhaseDelegate_0() const { return ___TurnPhaseDelegate_0; }
	inline TurnPhaseDelegateDeclare_t3595605765 ** get_address_of_TurnPhaseDelegate_0() { return &___TurnPhaseDelegate_0; }
	inline void set_TurnPhaseDelegate_0(TurnPhaseDelegateDeclare_t3595605765 * value)
	{
		___TurnPhaseDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___TurnPhaseDelegate_0), value);
	}

	inline static int32_t get_offset_of_zombieTurn_1() { return static_cast<int32_t>(offsetof(ZombieTurnPhases_t3127322021, ___zombieTurn_1)); }
	inline ZombieTurn_t3562480803 * get_zombieTurn_1() const { return ___zombieTurn_1; }
	inline ZombieTurn_t3562480803 ** get_address_of_zombieTurn_1() { return &___zombieTurn_1; }
	inline void set_zombieTurn_1(ZombieTurn_t3562480803 * value)
	{
		___zombieTurn_1 = value;
		Il2CppCodeGenWriteBarrier((&___zombieTurn_1), value);
	}

	inline static int32_t get_offset_of_MoveZombieIndex_2() { return static_cast<int32_t>(offsetof(ZombieTurnPhases_t3127322021, ___MoveZombieIndex_2)); }
	inline int32_t get_MoveZombieIndex_2() const { return ___MoveZombieIndex_2; }
	inline int32_t* get_address_of_MoveZombieIndex_2() { return &___MoveZombieIndex_2; }
	inline void set_MoveZombieIndex_2(int32_t value)
	{
		___MoveZombieIndex_2 = value;
	}

	inline static int32_t get_offset_of_PhaseName_3() { return static_cast<int32_t>(offsetof(ZombieTurnPhases_t3127322021, ___PhaseName_3)); }
	inline int32_t get_PhaseName_3() const { return ___PhaseName_3; }
	inline int32_t* get_address_of_PhaseName_3() { return &___PhaseName_3; }
	inline void set_PhaseName_3(int32_t value)
	{
		___PhaseName_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIETURNPHASES_T3127322021_H
#ifndef TURNPHASEDELEGATEDECLARE_T3595605765_H
#define TURNPHASEDELEGATEDECLARE_T3595605765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurnPhases/TurnPhaseDelegateDeclare
struct  TurnPhaseDelegateDeclare_t3595605765  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURNPHASEDELEGATEDECLARE_T3595605765_H
#ifndef HOPELESS_T2016799881_H
#define HOPELESS_T2016799881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Hopeless
struct  Hopeless_t2016799881  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOPELESS_T2016799881_H
#ifndef TOOLSOFTHEGRAVE_T2598357773_H
#define TOOLSOFTHEGRAVE_T2598357773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToolsOfTheGrave
struct  ToolsOfTheGrave_t2598357773  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLSOFTHEGRAVE_T2598357773_H
#ifndef TIDEOFTHEDEAD_T2906667500_H
#define TIDEOFTHEDEAD_T2906667500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TideOfTheDead
struct  TideOfTheDead_t2906667500  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIDEOFTHEDEAD_T2906667500_H
#ifndef DEADLYSURPRISE_T2801286068_H
#define DEADLYSURPRISE_T2801286068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeadlySurprise
struct  DeadlySurprise_t2801286068  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEADLYSURPRISE_T2801286068_H
#ifndef NOITCANTBE_T3821047303_H
#define NOITCANTBE_T3821047303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NoItCantBe
struct  NoItCantBe_t3821047303  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOITCANTBE_T3821047303_H
#ifndef UNSTOPPABLE_T2769904673_H
#define UNSTOPPABLE_T2769904673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unstoppable
struct  Unstoppable_t2769904673  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSTOPPABLE_T2769904673_H
#ifndef LEGIONSOFTHEDEAD_T3250160808_H
#define LEGIONSOFTHEDEAD_T3250160808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LegionsOfTheDead
struct  LegionsOfTheDead_t3250160808  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEGIONSOFTHEDEAD_T3250160808_H
#ifndef TOWNSECRETS_T1576979025_H
#define TOWNSECRETS_T1576979025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TownSecrets
struct  TownSecrets_t1576979025  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWNSECRETS_T1576979025_H
#ifndef DANCEOFTHEDEAD_T3960488959_H
#define DANCEOFTHEDEAD_T3960488959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DanceOfTheDead
struct  DanceOfTheDead_t3960488959  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DANCEOFTHEDEAD_T3960488959_H
#ifndef FEELSNOPAIN_T340595724_H
#define FEELSNOPAIN_T340595724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FeelsNoPain
struct  FeelsNoPain_t340595724  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEELSNOPAIN_T340595724_H
#ifndef THESMELLOFBRAINS_T1861641393_H
#define THESMELLOFBRAINS_T1861641393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheSmellOfBrains
struct  TheSmellOfBrains_t1861641393  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THESMELLOFBRAINS_T1861641393_H
#ifndef DIVIDEANDCONQUER_T4022414097_H
#define DIVIDEANDCONQUER_T4022414097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DivideAndConquer
struct  DivideAndConquer_t4022414097  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIVIDEANDCONQUER_T4022414097_H
#ifndef ANGRYDEAD_T1938627711_H
#define ANGRYDEAD_T1938627711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AngryDead
struct  AngryDead_t1938627711  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANGRYDEAD_T1938627711_H
#ifndef CLOSINGIN_T1878830773_H
#define CLOSINGIN_T1878830773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClosingIn
struct  ClosingIn_t1878830773  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSINGIN_T1878830773_H
#ifndef FALLINGDARKNESS_T3315734010_H
#define FALLINGDARKNESS_T3315734010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FallingDarkness
struct  FallingDarkness_t3315734010  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FALLINGDARKNESS_T3315734010_H
#ifndef TRAPPED_T3561378555_H
#define TRAPPED_T3561378555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Trapped
struct  Trapped_t3561378555  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAPPED_T3561378555_H
#ifndef HYSTERIA_T3350862265_H
#define HYSTERIA_T3350862265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Hysteria
struct  Hysteria_t3350862265  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HYSTERIA_T3350862265_H
#ifndef ANXIOUSTOFEED_T3134021366_H
#define ANXIOUSTOFEED_T3134021366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnxiousToFeed
struct  AnxiousToFeed_t3134021366  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANXIOUSTOFEED_T3134021366_H
#ifndef THEHUNGRYONE_T928842377_H
#define THEHUNGRYONE_T928842377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheHungryOne
struct  TheHungryOne_t928842377  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THEHUNGRYONE_T928842377_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TWISTEDANKLE_T4165690136_H
#define TWISTEDANKLE_T4165690136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwistedAnkle
struct  TwistedAnkle_t4165690136  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWISTEDANKLE_T4165690136_H
#ifndef RISENFROMTHEGRAVE_T127519008_H
#define RISENFROMTHEGRAVE_T127519008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RisenFromTheGrave
struct  RisenFromTheGrave_t127519008  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RISENFROMTHEGRAVE_T127519008_H
#ifndef HUNGRYDEAD_T869998133_H
#define HUNGRYDEAD_T869998133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HungryDead
struct  HungryDead_t869998133  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUNGRYDEAD_T869998133_H
#ifndef FIGHTFORSURVIVAL_T1379125000_H
#define FIGHTFORSURVIVAL_T1379125000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FightForSurvival
struct  FightForSurvival_t1379125000  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIGHTFORSURVIVAL_T1379125000_H
#ifndef NOWHERETORUN_T2169937835_H
#define NOWHERETORUN_T2169937835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NowhereToRun
struct  NowhereToRun_t2169937835  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOWHERETORUN_T2169937835_H
#ifndef LIVINGNIGHTMARE_T2875092037_H
#define LIVINGNIGHTMARE_T2875092037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LivingNightmare
struct  LivingNightmare_t2875092037  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIVINGNIGHTMARE_T2875092037_H
#ifndef THISCANTBEHAPPENING_T2204035598_H
#define THISCANTBEHAPPENING_T2204035598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThisCantBeHappening
struct  ThisCantBeHappening_t2204035598  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THISCANTBEHAPPENING_T2204035598_H
#ifndef TEENANGST_T2416046438_H
#define TEENANGST_T2416046438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TeenAngst
struct  TeenAngst_t2416046438  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEENANGST_T2416046438_H
#ifndef OVERCONFIDENCE_T2812648673_H
#define OVERCONFIDENCE_T2812648673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Overconfidence
struct  Overconfidence_t2812648673  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERCONFIDENCE_T2812648673_H
#ifndef MYGODHESAZOMBIE_T2959971631_H
#define MYGODHESAZOMBIE_T2959971631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyGodHesAZombie
struct  MyGodHesAZombie_t2959971631  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYGODHESAZOMBIE_T2959971631_H
#ifndef IVEGOTTOGETTOTHE_T3595394397_H
#define IVEGOTTOGETTOTHE_T3595394397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IveGotToGetToThe
struct  IveGotToGetToThe_t3595394397  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IVEGOTTOGETTOTHE_T3595394397_H
#ifndef IDONTTRUSTEM_T2604071939_H
#define IDONTTRUSTEM_T2604071939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IDontTrustEm
struct  IDontTrustEm_t2604071939  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDONTTRUSTEM_T2604071939_H
#ifndef HAUNTEDBYTHEPAST_T1055848434_H
#define HAUNTEDBYTHEPAST_T1055848434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HauntedByThePast
struct  HauntedByThePast_t1055848434  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HAUNTEDBYTHEPAST_T1055848434_H
#ifndef TRIP_T609988995_H
#define TRIP_T609988995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Trip
struct  Trip_t609988995  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIP_T609988995_H
#ifndef LOSSOFFAITH_T1416337014_H
#define LOSSOFFAITH_T1416337014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LossOfFaith
struct  LossOfFaith_t1416337014  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOSSOFFAITH_T1416337014_H
#ifndef ZOMBIESURGE_T4069364120_H
#define ZOMBIESURGE_T4069364120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieSurge
struct  ZombieSurge_t4069364120  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIESURGE_T4069364120_H
#ifndef RESILIENT_T2897756277_H
#define RESILIENT_T2897756277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Resilient
struct  Resilient_t2897756277  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESILIENT_T2897756277_H
#ifndef UNDEADHATETHELIVING_T3645703618_H
#define UNDEADHATETHELIVING_T3645703618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UndeadHateTheLiving
struct  UndeadHateTheLiving_t3645703618  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNDEADHATETHELIVING_T3645703618_H
#ifndef BRAAINS_T332229191_H
#define BRAAINS_T332229191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Braains
struct  Braains_t332229191  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRAAINS_T332229191_H
#ifndef UUUURRRGGGHH_T1205182777_H
#define UUUURRRGGGHH_T1205182777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Uuuurrrggghh
struct  Uuuurrrggghh_t1205182777  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UUUURRRGGGHH_T1205182777_H
#ifndef SHAMBLE_T2552159791_H
#define SHAMBLE_T2552159791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shamble
struct  Shamble_t2552159791  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAMBLE_T2552159791_H
#ifndef SURPRISEATTACK_T2653573337_H
#define SURPRISEATTACK_T2653573337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SurpriseAttack
struct  SurpriseAttack_t2653573337  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURPRISEATTACK_T2653573337_H
#ifndef OHTHEHORROR_T2613725065_H
#define OHTHEHORROR_T2613725065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OhTheHorror
struct  OhTheHorror_t2613725065  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OHTHEHORROR_T2613725065_H
#ifndef NEWSPAWNINGPIT_T552318903_H
#define NEWSPAWNINGPIT_T552318903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewSpawningPit
struct  NewSpawningPit_t552318903  : public ZombieCard_t2237653742
{
public:

public:
};

struct NewSpawningPit_t552318903_StaticFields
{
public:
	// System.Func`2<MapTile,System.Boolean> NewSpawningPit::<>f__am$cache0
	Func_2_t1480287660 * ___U3CU3Ef__amU24cache0_14;
	// System.Func`2<Building,System.Boolean> NewSpawningPit::<>f__am$cache1
	Func_2_t137751404 * ___U3CU3Ef__amU24cache1_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(NewSpawningPit_t552318903_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Func_2_t1480287660 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Func_2_t1480287660 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Func_2_t1480287660 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_15() { return static_cast<int32_t>(offsetof(NewSpawningPit_t552318903_StaticFields, ___U3CU3Ef__amU24cache1_15)); }
	inline Func_2_t137751404 * get_U3CU3Ef__amU24cache1_15() const { return ___U3CU3Ef__amU24cache1_15; }
	inline Func_2_t137751404 ** get_address_of_U3CU3Ef__amU24cache1_15() { return &___U3CU3Ef__amU24cache1_15; }
	inline void set_U3CU3Ef__amU24cache1_15(Func_2_t137751404 * value)
	{
		___U3CU3Ef__amU24cache1_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWSPAWNINGPIT_T552318903_H
#ifndef MYGODTHEYVETAKENTHE_T4142636702_H
#define MYGODTHEYVETAKENTHE_T4142636702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyGodTheyveTakenThe
struct  MyGodTheyveTakenThe_t4142636702  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYGODTHEYVETAKENTHE_T4142636702_H
#ifndef ATOWNOVERRUN_T175921820_H
#define ATOWNOVERRUN_T175921820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ATownOverrun
struct  ATownOverrun_t175921820  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATOWNOVERRUN_T175921820_H
#ifndef HEAVYRAIN_T332432098_H
#define HEAVYRAIN_T332432098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeavyRain
struct  HeavyRain_t332432098  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEAVYRAIN_T332432098_H
#ifndef THERESTOOMANY_T2689988620_H
#define THERESTOOMANY_T2689988620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheresTooMany
struct  TheresTooMany_t2689988620  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THERESTOOMANY_T2689988620_H
#ifndef NIGHTTHATNEVERENDS_T930728108_H
#define NIGHTTHATNEVERENDS_T930728108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NightThatNeverEnds
struct  NightThatNeverEnds_t930728108  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NIGHTTHATNEVERENDS_T930728108_H
#ifndef LOCKEDDOOR_T3547742430_H
#define LOCKEDDOOR_T3547742430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LockedDoor
struct  LockedDoor_t3547742430  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCKEDDOOR_T3547742430_H
#ifndef THERESNOTIMELEAVEIT_T1342240655_H
#define THERESNOTIMELEAVEIT_T1342240655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheresNoTimeLeaveIt
struct  TheresNoTimeLeaveIt_t1342240655  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THERESNOTIMELEAVEIT_T1342240655_H
#ifndef THELINEISDEAD_T2968485054_H
#define THELINEISDEAD_T2968485054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheLineIsDead
struct  TheLineIsDead_t2968485054  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THELINEISDEAD_T2968485054_H
#ifndef KNOCKEDAWAY_T1831705761_H
#define KNOCKEDAWAY_T1831705761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KnockedAway
struct  KnockedAway_t1831705761  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNOCKEDAWAY_T1831705761_H
#ifndef ITSSTUCK_T414080583_H
#define ITSSTUCK_T414080583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItsStuck
struct  ItsStuck_t414080583  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITSSTUCK_T414080583_H
#ifndef DESPAIR_T771065786_H
#define DESPAIR_T771065786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Despair
struct  Despair_t771065786  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESPAIR_T771065786_H
#ifndef CATFIGHT_T1469214242_H
#define CATFIGHT_T1469214242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Catfight
struct  Catfight_t1469214242  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATFIGHT_T1469214242_H
#ifndef LETSSPLITUP_T2094917924_H
#define LETSSPLITUP_T2094917924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LetsSplitUp
struct  LetsSplitUp_t2094917924  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETSSPLITUP_T2094917924_H
#ifndef THEYREEVERYWHERE_T2749774660_H
#define THEYREEVERYWHERE_T2749774660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheyreEverywhere
struct  TheyreEverywhere_t2749774660  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THEYREEVERYWHERE_T2749774660_H
#ifndef BITTEN_T2205689511_H
#define BITTEN_T2205689511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitten
struct  Bitten_t2205689511  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITTEN_T2205689511_H
#ifndef SCRATCHANDCLAW_T3789112499_H
#define SCRATCHANDCLAW_T3789112499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScratchAndClaw
struct  ScratchAndClaw_t3789112499  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRATCHANDCLAW_T3789112499_H
#ifndef OVERWHELMED_T420465551_H
#define OVERWHELMED_T420465551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Overwhelmed
struct  Overwhelmed_t420465551  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERWHELMED_T420465551_H
#ifndef FIGHTINGINSTINCTS_T2852332482_H
#define FIGHTINGINSTINCTS_T2852332482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FightingInstincts
struct  FightingInstincts_t2852332482  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIGHTINGINSTINCTS_T2852332482_H
#ifndef CAUGHTOFFGUARD_T3812377631_H
#define CAUGHTOFFGUARD_T3812377631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CaughtOffGuard
struct  CaughtOffGuard_t3812377631  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAUGHTOFFGUARD_T3812377631_H
#ifndef ROTTENBODIES_T748955989_H
#define ROTTENBODIES_T748955989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RottenBodies
struct  RottenBodies_t748955989  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTTENBODIES_T748955989_H
#ifndef DRAGGINGMEAT_T3424770081_H
#define DRAGGINGMEAT_T3424770081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DraggingMeat
struct  DraggingMeat_t3424770081  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGGINGMEAT_T3424770081_H
#ifndef NOWHERETOHIDE_T3704763644_H
#define NOWHERETOHIDE_T3704763644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NowhereToHide
struct  NowhereToHide_t3704763644  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOWHERETOHIDE_T3704763644_H
#ifndef IFELLKINDASTRANGE_T3376890855_H
#define IFELLKINDASTRANGE_T3376890855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IFellKindaStrange
struct  IFellKindaStrange_t3376890855  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IFELLKINDASTRANGE_T3376890855_H
#ifndef BICKERING_T3580212373_H
#define BICKERING_T3580212373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bickering
struct  Bickering_t3580212373  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BICKERING_T3580212373_H
#ifndef THISCOULDBEOURLASTNIGHTONEARTH_T3976126266_H
#define THISCOULDBEOURLASTNIGHTONEARTH_T3976126266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThisCouldBeOurLastNightOnEarth
struct  ThisCouldBeOurLastNightOnEarth_t3976126266  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THISCOULDBEOURLASTNIGHTONEARTH_T3976126266_H
#ifndef THEYRECOMINGFROMTHE_T2799902499_H
#define THEYRECOMINGFROMTHE_T2799902499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TheyreComingFromThe
struct  TheyreComingFromThe_t2799902499  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THEYRECOMINGFROMTHE_T2799902499_H
#ifndef UNNECESSARYSELFSACRIFICE_T890063265_H
#define UNNECESSARYSELFSACRIFICE_T890063265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnnecessarySelfSacrifice
struct  UnnecessarySelfSacrifice_t890063265  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNNECESSARYSELFSACRIFICE_T890063265_H
#ifndef LONER_T2606872010_H
#define LONER_T2606872010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Loner
struct  Loner_t2606872010  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONER_T2606872010_H
#ifndef DESPERATEFORFLESH_T2132404054_H
#define DESPERATEFORFLESH_T2132404054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DesperateForFlesh
struct  DesperateForFlesh_t2132404054  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESPERATEFORFLESH_T2132404054_H
#ifndef GROWINGHUNGER_T1321750346_H
#define GROWINGHUNGER_T1321750346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GrowingHunger
struct  GrowingHunger_t1321750346  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROWINGHUNGER_T1321750346_H
#ifndef RELENTLESSADVANCE_T2095454104_H
#define RELENTLESSADVANCE_T2095454104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RelentlessAdvance
struct  RelentlessAdvance_t2095454104  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELENTLESSADVANCE_T2095454104_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SPEECHRECOGNIZERDEMOSCENEMANAGER_T231664079_H
#define SPEECHRECOGNIZERDEMOSCENEMANAGER_T231664079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpeechRecognizerDemoSceneManager
struct  SpeechRecognizerDemoSceneManager_t231664079  : public MonoBehaviour_t3962482529
{
public:
	// SpeechRecognizerManager SpeechRecognizerDemoSceneManager::_speechManager
	SpeechRecognizerManager_t652028364 * ____speechManager_2;
	// System.Boolean SpeechRecognizerDemoSceneManager::_isListening
	bool ____isListening_3;
	// System.String SpeechRecognizerDemoSceneManager::_message
	String_t* ____message_4;

public:
	inline static int32_t get_offset_of__speechManager_2() { return static_cast<int32_t>(offsetof(SpeechRecognizerDemoSceneManager_t231664079, ____speechManager_2)); }
	inline SpeechRecognizerManager_t652028364 * get__speechManager_2() const { return ____speechManager_2; }
	inline SpeechRecognizerManager_t652028364 ** get_address_of__speechManager_2() { return &____speechManager_2; }
	inline void set__speechManager_2(SpeechRecognizerManager_t652028364 * value)
	{
		____speechManager_2 = value;
		Il2CppCodeGenWriteBarrier((&____speechManager_2), value);
	}

	inline static int32_t get_offset_of__isListening_3() { return static_cast<int32_t>(offsetof(SpeechRecognizerDemoSceneManager_t231664079, ____isListening_3)); }
	inline bool get__isListening_3() const { return ____isListening_3; }
	inline bool* get_address_of__isListening_3() { return &____isListening_3; }
	inline void set__isListening_3(bool value)
	{
		____isListening_3 = value;
	}

	inline static int32_t get_offset_of__message_4() { return static_cast<int32_t>(offsetof(SpeechRecognizerDemoSceneManager_t231664079, ____message_4)); }
	inline String_t* get__message_4() const { return ____message_4; }
	inline String_t** get_address_of__message_4() { return &____message_4; }
	inline void set__message_4(String_t* value)
	{
		____message_4 = value;
		Il2CppCodeGenWriteBarrier((&____message_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEECHRECOGNIZERDEMOSCENEMANAGER_T231664079_H
#ifndef VUFORIAMONOBEHAVIOUR_T1150221792_H
#define VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t1150221792  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifndef ZOMBIEDEADSCREEN_T2482992935_H
#define ZOMBIEDEADSCREEN_T2482992935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieDeadScreen
struct  ZombieDeadScreen_t2482992935  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ZombieDeadScreen::Option1
	GameObject_t1113636619 * ___Option1_2;
	// UnityEngine.GameObject ZombieDeadScreen::Option2
	GameObject_t1113636619 * ___Option2_3;
	// UnityEngine.GameObject ZombieDeadScreen::Option3
	GameObject_t1113636619 * ___Option3_4;
	// UnityEngine.GameObject ZombieDeadScreen::Option4
	GameObject_t1113636619 * ___Option4_5;
	// UnityEngine.GameObject ZombieDeadScreen::Option5
	GameObject_t1113636619 * ___Option5_6;
	// UnityEngine.GameObject ZombieDeadScreen::ZombieDeadPanel
	GameObject_t1113636619 * ___ZombieDeadPanel_7;
	// ZombieTurn ZombieDeadScreen::zombieTurn
	ZombieTurn_t3562480803 * ___zombieTurn_8;

public:
	inline static int32_t get_offset_of_Option1_2() { return static_cast<int32_t>(offsetof(ZombieDeadScreen_t2482992935, ___Option1_2)); }
	inline GameObject_t1113636619 * get_Option1_2() const { return ___Option1_2; }
	inline GameObject_t1113636619 ** get_address_of_Option1_2() { return &___Option1_2; }
	inline void set_Option1_2(GameObject_t1113636619 * value)
	{
		___Option1_2 = value;
		Il2CppCodeGenWriteBarrier((&___Option1_2), value);
	}

	inline static int32_t get_offset_of_Option2_3() { return static_cast<int32_t>(offsetof(ZombieDeadScreen_t2482992935, ___Option2_3)); }
	inline GameObject_t1113636619 * get_Option2_3() const { return ___Option2_3; }
	inline GameObject_t1113636619 ** get_address_of_Option2_3() { return &___Option2_3; }
	inline void set_Option2_3(GameObject_t1113636619 * value)
	{
		___Option2_3 = value;
		Il2CppCodeGenWriteBarrier((&___Option2_3), value);
	}

	inline static int32_t get_offset_of_Option3_4() { return static_cast<int32_t>(offsetof(ZombieDeadScreen_t2482992935, ___Option3_4)); }
	inline GameObject_t1113636619 * get_Option3_4() const { return ___Option3_4; }
	inline GameObject_t1113636619 ** get_address_of_Option3_4() { return &___Option3_4; }
	inline void set_Option3_4(GameObject_t1113636619 * value)
	{
		___Option3_4 = value;
		Il2CppCodeGenWriteBarrier((&___Option3_4), value);
	}

	inline static int32_t get_offset_of_Option4_5() { return static_cast<int32_t>(offsetof(ZombieDeadScreen_t2482992935, ___Option4_5)); }
	inline GameObject_t1113636619 * get_Option4_5() const { return ___Option4_5; }
	inline GameObject_t1113636619 ** get_address_of_Option4_5() { return &___Option4_5; }
	inline void set_Option4_5(GameObject_t1113636619 * value)
	{
		___Option4_5 = value;
		Il2CppCodeGenWriteBarrier((&___Option4_5), value);
	}

	inline static int32_t get_offset_of_Option5_6() { return static_cast<int32_t>(offsetof(ZombieDeadScreen_t2482992935, ___Option5_6)); }
	inline GameObject_t1113636619 * get_Option5_6() const { return ___Option5_6; }
	inline GameObject_t1113636619 ** get_address_of_Option5_6() { return &___Option5_6; }
	inline void set_Option5_6(GameObject_t1113636619 * value)
	{
		___Option5_6 = value;
		Il2CppCodeGenWriteBarrier((&___Option5_6), value);
	}

	inline static int32_t get_offset_of_ZombieDeadPanel_7() { return static_cast<int32_t>(offsetof(ZombieDeadScreen_t2482992935, ___ZombieDeadPanel_7)); }
	inline GameObject_t1113636619 * get_ZombieDeadPanel_7() const { return ___ZombieDeadPanel_7; }
	inline GameObject_t1113636619 ** get_address_of_ZombieDeadPanel_7() { return &___ZombieDeadPanel_7; }
	inline void set_ZombieDeadPanel_7(GameObject_t1113636619 * value)
	{
		___ZombieDeadPanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieDeadPanel_7), value);
	}

	inline static int32_t get_offset_of_zombieTurn_8() { return static_cast<int32_t>(offsetof(ZombieDeadScreen_t2482992935, ___zombieTurn_8)); }
	inline ZombieTurn_t3562480803 * get_zombieTurn_8() const { return ___zombieTurn_8; }
	inline ZombieTurn_t3562480803 ** get_address_of_zombieTurn_8() { return &___zombieTurn_8; }
	inline void set_zombieTurn_8(ZombieTurn_t3562480803 * value)
	{
		___zombieTurn_8 = value;
		Il2CppCodeGenWriteBarrier((&___zombieTurn_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIEDEADSCREEN_T2482992935_H
#ifndef ZOMBIETURN_T3562480803_H
#define ZOMBIETURN_T3562480803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurn
struct  ZombieTurn_t3562480803  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.String> ZombieTurn::setupState
	List_1_t3319525431 * ___setupState_2;
	// ZombieDeck ZombieTurn::decks
	ZombieDeck_t3038342722 * ___decks_3;
	// GameSession ZombieTurn::gameStats
	GameSession_t4087811243 * ___gameStats_4;
	// System.Boolean ZombieTurn::spawn
	bool ___spawn_5;
	// System.Boolean ZombieTurn::zombiesWinOnTie
	bool ___zombiesWinOnTie_6;
	// System.Boolean ZombieTurn::zombieMovesAfterSpawn
	bool ___zombieMovesAfterSpawn_7;
	// System.Boolean ZombieTurn::theHungryOne
	bool ___theHungryOne_8;
	// System.Boolean ZombieTurn::fightCardUsed
	bool ___fightCardUsed_9;
	// System.Int32 ZombieTurn::round
	int32_t ___round_10;
	// System.Int32 ZombieTurn::heroesDiedCount
	int32_t ___heroesDiedCount_11;
	// ZombieCard ZombieTurn::cardBeingPlayed
	ZombieCard_t2237653742 * ___cardBeingPlayed_12;
	// GameData ZombieTurn::gameData
	GameData_t415813024 * ___gameData_13;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ZombieTurn::gameObjectives
	List_1_t2585711361 * ___gameObjectives_14;
	// System.String ZombieTurn::currentSetupState
	String_t* ___currentSetupState_15;
	// System.Collections.Generic.List`1<System.String> ZombieTurn::takenOverBuilding
	List_1_t3319525431 * ___takenOverBuilding_16;
	// System.String ZombieTurn::zombieToFight
	String_t* ___zombieToFight_17;
	// System.Int32 ZombieTurn::numberOfHeroesInGame
	int32_t ___numberOfHeroesInGame_18;
	// System.Collections.Generic.List`1<System.String> ZombieTurn::cardsPlayed
	List_1_t3319525431 * ___cardsPlayed_19;
	// ZombieTurnPhases ZombieTurn::Phases
	ZombieTurnPhases_t3127322021 * ___Phases_20;
	// UnityEngine.GameObject ZombieTurn::SetupPanel
	GameObject_t1113636619 * ___SetupPanel_21;
	// UnityEngine.UI.Text ZombieTurn::SetupText
	Text_t1901882714 * ___SetupText_22;
	// UnityEngine.GameObject ZombieTurn::MainUI
	GameObject_t1113636619 * ___MainUI_23;
	// UnityEngine.GameObject ZombieTurn::Card
	GameObject_t1113636619 * ___Card_24;
	// UnityEngine.UI.Text ZombieTurn::CardTitle
	Text_t1901882714 * ___CardTitle_25;
	// UnityEngine.UI.Text ZombieTurn::CardText
	Text_t1901882714 * ___CardText_26;
	// UnityEngine.UI.Text ZombieTurn::CardFlavor
	Text_t1901882714 * ___CardFlavor_27;
	// UnityEngine.UI.Text ZombieTurn::ZombiesOnBoardText
	Text_t1901882714 * ___ZombiesOnBoardText_28;
	// UnityEngine.UI.Text ZombieTurn::ScenarioName
	Text_t1901882714 * ___ScenarioName_29;
	// UnityEngine.GameObject ZombieTurn::MovePanel
	GameObject_t1113636619 * ___MovePanel_30;
	// UnityEngine.UI.Text ZombieTurn::MoveText
	Text_t1901882714 * ___MoveText_31;
	// UnityEngine.UI.Text ZombieTurn::MoveZombieTypeText
	Text_t1901882714 * ___MoveZombieTypeText_32;
	// UnityEngine.GameObject ZombieTurn::FightPanel
	GameObject_t1113636619 * ___FightPanel_33;
	// UnityEngine.UI.Text ZombieTurn::FightText
	Text_t1901882714 * ___FightText_34;
	// UnityEngine.GameObject ZombieTurn::FightRollResultParent
	GameObject_t1113636619 * ___FightRollResultParent_35;
	// UnityEngine.GameObject ZombieTurn::FightRollResultPrefab
	GameObject_t1113636619 * ___FightRollResultPrefab_36;
	// UnityEngine.GameObject ZombieTurn::SpawnPanel
	GameObject_t1113636619 * ___SpawnPanel_37;
	// UnityEngine.UI.Text ZombieTurn::SpawnText
	Text_t1901882714 * ___SpawnText_38;
	// UnityEngine.GameObject ZombieTurn::RemainsInPlay
	GameObject_t1113636619 * ___RemainsInPlay_39;
	// UnityEngine.GameObject ZombieTurn::RemainsInPlayElement
	GameObject_t1113636619 * ___RemainsInPlayElement_40;
	// UnityEngine.GameObject ZombieTurn::ResultPanel
	GameObject_t1113636619 * ___ResultPanel_41;
	// UnityEngine.UI.Text ZombieTurn::ResultText
	Text_t1901882714 * ___ResultText_42;
	// UnityEngine.UI.Text ZombieTurn::ResultNarrativeText
	Text_t1901882714 * ___ResultNarrativeText_43;
	// UnityEngine.GameObject ZombieTurn::DeadHeroPanel
	GameObject_t1113636619 * ___DeadHeroPanel_44;
	// UnityEngine.GameObject ZombieTurn::DeadZombiePanel
	GameObject_t1113636619 * ___DeadZombiePanel_45;
	// TextAnimation ZombieTurn::TextAnimation
	TextAnimation_t2694049294 * ___TextAnimation_46;
	// GameLog ZombieTurn::GameLog
	GameLog_t2697247111 * ___GameLog_47;
	// PlaceMapImages ZombieTurn::MapImages
	PlaceMapImages_t1484071588 * ___MapImages_48;
	// UnityEngine.GameObject ZombieTurn::ZombieObjectivePanel
	GameObject_t1113636619 * ___ZombieObjectivePanel_49;
	// UnityEngine.GameObject ZombieTurn::HeroObjectivePanel
	GameObject_t1113636619 * ___HeroObjectivePanel_50;
	// UnityEngine.GameObject ZombieTurn::ObjectivePrefab
	GameObject_t1113636619 * ___ObjectivePrefab_51;
	// UnityEngine.GameObject ZombieTurn::NarrativePanel
	GameObject_t1113636619 * ___NarrativePanel_52;
	// UnityEngine.UI.Text ZombieTurn::NarrativeTitle
	Text_t1901882714 * ___NarrativeTitle_53;
	// UnityEngine.UI.Text ZombieTurn::NarrativeText
	Text_t1901882714 * ___NarrativeText_54;
	// UnityEngine.GameObject ZombieTurn::DestroyBuildingPanel
	GameObject_t1113636619 * ___DestroyBuildingPanel_55;
	// UnityEngine.UI.Slider ZombieTurn::DestroyBuildingZombieSlider
	Slider_t3903728902 * ___DestroyBuildingZombieSlider_56;
	// UnityEngine.UI.Dropdown ZombieTurn::DestroyBuildingDropdown
	Dropdown_t2274391225 * ___DestroyBuildingDropdown_57;
	// UnityEngine.UI.Text ZombieTurn::DestroyBuildingDeadZombiesText
	Text_t1901882714 * ___DestroyBuildingDeadZombiesText_58;
	// UnityEngine.GameObject ZombieTurn::ZombiePitPanel
	GameObject_t1113636619 * ___ZombiePitPanel_59;
	// UnityEngine.GameObject ZombieTurn::TutorialPanel
	GameObject_t1113636619 * ___TutorialPanel_60;
	// UnityEngine.UI.Text ZombieTurn::TutorialText
	Text_t1901882714 * ___TutorialText_61;
	// UnityEngine.UI.Dropdown ZombieTurn::DebugCardList
	Dropdown_t2274391225 * ___DebugCardList_62;
	// UnityEngine.Material ZombieTurn::damageMaterial
	Material_t340375123 * ___damageMaterial_63;
	// UnityEngine.AudioSource ZombieTurn::clickAudio
	AudioSource_t3935305588 * ___clickAudio_64;
	// UnityEngine.AudioSource ZombieTurn::audioZombieDead
	AudioSource_t3935305588 * ___audioZombieDead_65;
	// UnityEngine.AudioSource ZombieTurn::audioZombieEvades
	AudioSource_t3935305588 * ___audioZombieEvades_66;
	// UnityEngine.AudioSource ZombieTurn::audioHeroDead
	AudioSource_t3935305588 * ___audioHeroDead_67;
	// System.Boolean ZombieTurn::isDebugLog
	bool ___isDebugLog_68;
	// ViewableUI ZombieTurn::ViewableUI
	ViewableUI_t3820486676 * ___ViewableUI_69;
	// System.Collections.Generic.List`1<ZombieCard> ZombieTurn::<CardsToPlay>k__BackingField
	List_1_t3709728484 * ___U3CCardsToPlayU3Ek__BackingField_70;

public:
	inline static int32_t get_offset_of_setupState_2() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___setupState_2)); }
	inline List_1_t3319525431 * get_setupState_2() const { return ___setupState_2; }
	inline List_1_t3319525431 ** get_address_of_setupState_2() { return &___setupState_2; }
	inline void set_setupState_2(List_1_t3319525431 * value)
	{
		___setupState_2 = value;
		Il2CppCodeGenWriteBarrier((&___setupState_2), value);
	}

	inline static int32_t get_offset_of_decks_3() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___decks_3)); }
	inline ZombieDeck_t3038342722 * get_decks_3() const { return ___decks_3; }
	inline ZombieDeck_t3038342722 ** get_address_of_decks_3() { return &___decks_3; }
	inline void set_decks_3(ZombieDeck_t3038342722 * value)
	{
		___decks_3 = value;
		Il2CppCodeGenWriteBarrier((&___decks_3), value);
	}

	inline static int32_t get_offset_of_gameStats_4() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___gameStats_4)); }
	inline GameSession_t4087811243 * get_gameStats_4() const { return ___gameStats_4; }
	inline GameSession_t4087811243 ** get_address_of_gameStats_4() { return &___gameStats_4; }
	inline void set_gameStats_4(GameSession_t4087811243 * value)
	{
		___gameStats_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameStats_4), value);
	}

	inline static int32_t get_offset_of_spawn_5() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___spawn_5)); }
	inline bool get_spawn_5() const { return ___spawn_5; }
	inline bool* get_address_of_spawn_5() { return &___spawn_5; }
	inline void set_spawn_5(bool value)
	{
		___spawn_5 = value;
	}

	inline static int32_t get_offset_of_zombiesWinOnTie_6() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___zombiesWinOnTie_6)); }
	inline bool get_zombiesWinOnTie_6() const { return ___zombiesWinOnTie_6; }
	inline bool* get_address_of_zombiesWinOnTie_6() { return &___zombiesWinOnTie_6; }
	inline void set_zombiesWinOnTie_6(bool value)
	{
		___zombiesWinOnTie_6 = value;
	}

	inline static int32_t get_offset_of_zombieMovesAfterSpawn_7() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___zombieMovesAfterSpawn_7)); }
	inline bool get_zombieMovesAfterSpawn_7() const { return ___zombieMovesAfterSpawn_7; }
	inline bool* get_address_of_zombieMovesAfterSpawn_7() { return &___zombieMovesAfterSpawn_7; }
	inline void set_zombieMovesAfterSpawn_7(bool value)
	{
		___zombieMovesAfterSpawn_7 = value;
	}

	inline static int32_t get_offset_of_theHungryOne_8() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___theHungryOne_8)); }
	inline bool get_theHungryOne_8() const { return ___theHungryOne_8; }
	inline bool* get_address_of_theHungryOne_8() { return &___theHungryOne_8; }
	inline void set_theHungryOne_8(bool value)
	{
		___theHungryOne_8 = value;
	}

	inline static int32_t get_offset_of_fightCardUsed_9() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___fightCardUsed_9)); }
	inline bool get_fightCardUsed_9() const { return ___fightCardUsed_9; }
	inline bool* get_address_of_fightCardUsed_9() { return &___fightCardUsed_9; }
	inline void set_fightCardUsed_9(bool value)
	{
		___fightCardUsed_9 = value;
	}

	inline static int32_t get_offset_of_round_10() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___round_10)); }
	inline int32_t get_round_10() const { return ___round_10; }
	inline int32_t* get_address_of_round_10() { return &___round_10; }
	inline void set_round_10(int32_t value)
	{
		___round_10 = value;
	}

	inline static int32_t get_offset_of_heroesDiedCount_11() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___heroesDiedCount_11)); }
	inline int32_t get_heroesDiedCount_11() const { return ___heroesDiedCount_11; }
	inline int32_t* get_address_of_heroesDiedCount_11() { return &___heroesDiedCount_11; }
	inline void set_heroesDiedCount_11(int32_t value)
	{
		___heroesDiedCount_11 = value;
	}

	inline static int32_t get_offset_of_cardBeingPlayed_12() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___cardBeingPlayed_12)); }
	inline ZombieCard_t2237653742 * get_cardBeingPlayed_12() const { return ___cardBeingPlayed_12; }
	inline ZombieCard_t2237653742 ** get_address_of_cardBeingPlayed_12() { return &___cardBeingPlayed_12; }
	inline void set_cardBeingPlayed_12(ZombieCard_t2237653742 * value)
	{
		___cardBeingPlayed_12 = value;
		Il2CppCodeGenWriteBarrier((&___cardBeingPlayed_12), value);
	}

	inline static int32_t get_offset_of_gameData_13() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___gameData_13)); }
	inline GameData_t415813024 * get_gameData_13() const { return ___gameData_13; }
	inline GameData_t415813024 ** get_address_of_gameData_13() { return &___gameData_13; }
	inline void set_gameData_13(GameData_t415813024 * value)
	{
		___gameData_13 = value;
		Il2CppCodeGenWriteBarrier((&___gameData_13), value);
	}

	inline static int32_t get_offset_of_gameObjectives_14() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___gameObjectives_14)); }
	inline List_1_t2585711361 * get_gameObjectives_14() const { return ___gameObjectives_14; }
	inline List_1_t2585711361 ** get_address_of_gameObjectives_14() { return &___gameObjectives_14; }
	inline void set_gameObjectives_14(List_1_t2585711361 * value)
	{
		___gameObjectives_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectives_14), value);
	}

	inline static int32_t get_offset_of_currentSetupState_15() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___currentSetupState_15)); }
	inline String_t* get_currentSetupState_15() const { return ___currentSetupState_15; }
	inline String_t** get_address_of_currentSetupState_15() { return &___currentSetupState_15; }
	inline void set_currentSetupState_15(String_t* value)
	{
		___currentSetupState_15 = value;
		Il2CppCodeGenWriteBarrier((&___currentSetupState_15), value);
	}

	inline static int32_t get_offset_of_takenOverBuilding_16() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___takenOverBuilding_16)); }
	inline List_1_t3319525431 * get_takenOverBuilding_16() const { return ___takenOverBuilding_16; }
	inline List_1_t3319525431 ** get_address_of_takenOverBuilding_16() { return &___takenOverBuilding_16; }
	inline void set_takenOverBuilding_16(List_1_t3319525431 * value)
	{
		___takenOverBuilding_16 = value;
		Il2CppCodeGenWriteBarrier((&___takenOverBuilding_16), value);
	}

	inline static int32_t get_offset_of_zombieToFight_17() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___zombieToFight_17)); }
	inline String_t* get_zombieToFight_17() const { return ___zombieToFight_17; }
	inline String_t** get_address_of_zombieToFight_17() { return &___zombieToFight_17; }
	inline void set_zombieToFight_17(String_t* value)
	{
		___zombieToFight_17 = value;
		Il2CppCodeGenWriteBarrier((&___zombieToFight_17), value);
	}

	inline static int32_t get_offset_of_numberOfHeroesInGame_18() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___numberOfHeroesInGame_18)); }
	inline int32_t get_numberOfHeroesInGame_18() const { return ___numberOfHeroesInGame_18; }
	inline int32_t* get_address_of_numberOfHeroesInGame_18() { return &___numberOfHeroesInGame_18; }
	inline void set_numberOfHeroesInGame_18(int32_t value)
	{
		___numberOfHeroesInGame_18 = value;
	}

	inline static int32_t get_offset_of_cardsPlayed_19() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___cardsPlayed_19)); }
	inline List_1_t3319525431 * get_cardsPlayed_19() const { return ___cardsPlayed_19; }
	inline List_1_t3319525431 ** get_address_of_cardsPlayed_19() { return &___cardsPlayed_19; }
	inline void set_cardsPlayed_19(List_1_t3319525431 * value)
	{
		___cardsPlayed_19 = value;
		Il2CppCodeGenWriteBarrier((&___cardsPlayed_19), value);
	}

	inline static int32_t get_offset_of_Phases_20() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___Phases_20)); }
	inline ZombieTurnPhases_t3127322021 * get_Phases_20() const { return ___Phases_20; }
	inline ZombieTurnPhases_t3127322021 ** get_address_of_Phases_20() { return &___Phases_20; }
	inline void set_Phases_20(ZombieTurnPhases_t3127322021 * value)
	{
		___Phases_20 = value;
		Il2CppCodeGenWriteBarrier((&___Phases_20), value);
	}

	inline static int32_t get_offset_of_SetupPanel_21() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___SetupPanel_21)); }
	inline GameObject_t1113636619 * get_SetupPanel_21() const { return ___SetupPanel_21; }
	inline GameObject_t1113636619 ** get_address_of_SetupPanel_21() { return &___SetupPanel_21; }
	inline void set_SetupPanel_21(GameObject_t1113636619 * value)
	{
		___SetupPanel_21 = value;
		Il2CppCodeGenWriteBarrier((&___SetupPanel_21), value);
	}

	inline static int32_t get_offset_of_SetupText_22() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___SetupText_22)); }
	inline Text_t1901882714 * get_SetupText_22() const { return ___SetupText_22; }
	inline Text_t1901882714 ** get_address_of_SetupText_22() { return &___SetupText_22; }
	inline void set_SetupText_22(Text_t1901882714 * value)
	{
		___SetupText_22 = value;
		Il2CppCodeGenWriteBarrier((&___SetupText_22), value);
	}

	inline static int32_t get_offset_of_MainUI_23() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___MainUI_23)); }
	inline GameObject_t1113636619 * get_MainUI_23() const { return ___MainUI_23; }
	inline GameObject_t1113636619 ** get_address_of_MainUI_23() { return &___MainUI_23; }
	inline void set_MainUI_23(GameObject_t1113636619 * value)
	{
		___MainUI_23 = value;
		Il2CppCodeGenWriteBarrier((&___MainUI_23), value);
	}

	inline static int32_t get_offset_of_Card_24() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___Card_24)); }
	inline GameObject_t1113636619 * get_Card_24() const { return ___Card_24; }
	inline GameObject_t1113636619 ** get_address_of_Card_24() { return &___Card_24; }
	inline void set_Card_24(GameObject_t1113636619 * value)
	{
		___Card_24 = value;
		Il2CppCodeGenWriteBarrier((&___Card_24), value);
	}

	inline static int32_t get_offset_of_CardTitle_25() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___CardTitle_25)); }
	inline Text_t1901882714 * get_CardTitle_25() const { return ___CardTitle_25; }
	inline Text_t1901882714 ** get_address_of_CardTitle_25() { return &___CardTitle_25; }
	inline void set_CardTitle_25(Text_t1901882714 * value)
	{
		___CardTitle_25 = value;
		Il2CppCodeGenWriteBarrier((&___CardTitle_25), value);
	}

	inline static int32_t get_offset_of_CardText_26() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___CardText_26)); }
	inline Text_t1901882714 * get_CardText_26() const { return ___CardText_26; }
	inline Text_t1901882714 ** get_address_of_CardText_26() { return &___CardText_26; }
	inline void set_CardText_26(Text_t1901882714 * value)
	{
		___CardText_26 = value;
		Il2CppCodeGenWriteBarrier((&___CardText_26), value);
	}

	inline static int32_t get_offset_of_CardFlavor_27() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___CardFlavor_27)); }
	inline Text_t1901882714 * get_CardFlavor_27() const { return ___CardFlavor_27; }
	inline Text_t1901882714 ** get_address_of_CardFlavor_27() { return &___CardFlavor_27; }
	inline void set_CardFlavor_27(Text_t1901882714 * value)
	{
		___CardFlavor_27 = value;
		Il2CppCodeGenWriteBarrier((&___CardFlavor_27), value);
	}

	inline static int32_t get_offset_of_ZombiesOnBoardText_28() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ZombiesOnBoardText_28)); }
	inline Text_t1901882714 * get_ZombiesOnBoardText_28() const { return ___ZombiesOnBoardText_28; }
	inline Text_t1901882714 ** get_address_of_ZombiesOnBoardText_28() { return &___ZombiesOnBoardText_28; }
	inline void set_ZombiesOnBoardText_28(Text_t1901882714 * value)
	{
		___ZombiesOnBoardText_28 = value;
		Il2CppCodeGenWriteBarrier((&___ZombiesOnBoardText_28), value);
	}

	inline static int32_t get_offset_of_ScenarioName_29() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ScenarioName_29)); }
	inline Text_t1901882714 * get_ScenarioName_29() const { return ___ScenarioName_29; }
	inline Text_t1901882714 ** get_address_of_ScenarioName_29() { return &___ScenarioName_29; }
	inline void set_ScenarioName_29(Text_t1901882714 * value)
	{
		___ScenarioName_29 = value;
		Il2CppCodeGenWriteBarrier((&___ScenarioName_29), value);
	}

	inline static int32_t get_offset_of_MovePanel_30() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___MovePanel_30)); }
	inline GameObject_t1113636619 * get_MovePanel_30() const { return ___MovePanel_30; }
	inline GameObject_t1113636619 ** get_address_of_MovePanel_30() { return &___MovePanel_30; }
	inline void set_MovePanel_30(GameObject_t1113636619 * value)
	{
		___MovePanel_30 = value;
		Il2CppCodeGenWriteBarrier((&___MovePanel_30), value);
	}

	inline static int32_t get_offset_of_MoveText_31() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___MoveText_31)); }
	inline Text_t1901882714 * get_MoveText_31() const { return ___MoveText_31; }
	inline Text_t1901882714 ** get_address_of_MoveText_31() { return &___MoveText_31; }
	inline void set_MoveText_31(Text_t1901882714 * value)
	{
		___MoveText_31 = value;
		Il2CppCodeGenWriteBarrier((&___MoveText_31), value);
	}

	inline static int32_t get_offset_of_MoveZombieTypeText_32() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___MoveZombieTypeText_32)); }
	inline Text_t1901882714 * get_MoveZombieTypeText_32() const { return ___MoveZombieTypeText_32; }
	inline Text_t1901882714 ** get_address_of_MoveZombieTypeText_32() { return &___MoveZombieTypeText_32; }
	inline void set_MoveZombieTypeText_32(Text_t1901882714 * value)
	{
		___MoveZombieTypeText_32 = value;
		Il2CppCodeGenWriteBarrier((&___MoveZombieTypeText_32), value);
	}

	inline static int32_t get_offset_of_FightPanel_33() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___FightPanel_33)); }
	inline GameObject_t1113636619 * get_FightPanel_33() const { return ___FightPanel_33; }
	inline GameObject_t1113636619 ** get_address_of_FightPanel_33() { return &___FightPanel_33; }
	inline void set_FightPanel_33(GameObject_t1113636619 * value)
	{
		___FightPanel_33 = value;
		Il2CppCodeGenWriteBarrier((&___FightPanel_33), value);
	}

	inline static int32_t get_offset_of_FightText_34() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___FightText_34)); }
	inline Text_t1901882714 * get_FightText_34() const { return ___FightText_34; }
	inline Text_t1901882714 ** get_address_of_FightText_34() { return &___FightText_34; }
	inline void set_FightText_34(Text_t1901882714 * value)
	{
		___FightText_34 = value;
		Il2CppCodeGenWriteBarrier((&___FightText_34), value);
	}

	inline static int32_t get_offset_of_FightRollResultParent_35() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___FightRollResultParent_35)); }
	inline GameObject_t1113636619 * get_FightRollResultParent_35() const { return ___FightRollResultParent_35; }
	inline GameObject_t1113636619 ** get_address_of_FightRollResultParent_35() { return &___FightRollResultParent_35; }
	inline void set_FightRollResultParent_35(GameObject_t1113636619 * value)
	{
		___FightRollResultParent_35 = value;
		Il2CppCodeGenWriteBarrier((&___FightRollResultParent_35), value);
	}

	inline static int32_t get_offset_of_FightRollResultPrefab_36() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___FightRollResultPrefab_36)); }
	inline GameObject_t1113636619 * get_FightRollResultPrefab_36() const { return ___FightRollResultPrefab_36; }
	inline GameObject_t1113636619 ** get_address_of_FightRollResultPrefab_36() { return &___FightRollResultPrefab_36; }
	inline void set_FightRollResultPrefab_36(GameObject_t1113636619 * value)
	{
		___FightRollResultPrefab_36 = value;
		Il2CppCodeGenWriteBarrier((&___FightRollResultPrefab_36), value);
	}

	inline static int32_t get_offset_of_SpawnPanel_37() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___SpawnPanel_37)); }
	inline GameObject_t1113636619 * get_SpawnPanel_37() const { return ___SpawnPanel_37; }
	inline GameObject_t1113636619 ** get_address_of_SpawnPanel_37() { return &___SpawnPanel_37; }
	inline void set_SpawnPanel_37(GameObject_t1113636619 * value)
	{
		___SpawnPanel_37 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnPanel_37), value);
	}

	inline static int32_t get_offset_of_SpawnText_38() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___SpawnText_38)); }
	inline Text_t1901882714 * get_SpawnText_38() const { return ___SpawnText_38; }
	inline Text_t1901882714 ** get_address_of_SpawnText_38() { return &___SpawnText_38; }
	inline void set_SpawnText_38(Text_t1901882714 * value)
	{
		___SpawnText_38 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnText_38), value);
	}

	inline static int32_t get_offset_of_RemainsInPlay_39() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___RemainsInPlay_39)); }
	inline GameObject_t1113636619 * get_RemainsInPlay_39() const { return ___RemainsInPlay_39; }
	inline GameObject_t1113636619 ** get_address_of_RemainsInPlay_39() { return &___RemainsInPlay_39; }
	inline void set_RemainsInPlay_39(GameObject_t1113636619 * value)
	{
		___RemainsInPlay_39 = value;
		Il2CppCodeGenWriteBarrier((&___RemainsInPlay_39), value);
	}

	inline static int32_t get_offset_of_RemainsInPlayElement_40() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___RemainsInPlayElement_40)); }
	inline GameObject_t1113636619 * get_RemainsInPlayElement_40() const { return ___RemainsInPlayElement_40; }
	inline GameObject_t1113636619 ** get_address_of_RemainsInPlayElement_40() { return &___RemainsInPlayElement_40; }
	inline void set_RemainsInPlayElement_40(GameObject_t1113636619 * value)
	{
		___RemainsInPlayElement_40 = value;
		Il2CppCodeGenWriteBarrier((&___RemainsInPlayElement_40), value);
	}

	inline static int32_t get_offset_of_ResultPanel_41() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ResultPanel_41)); }
	inline GameObject_t1113636619 * get_ResultPanel_41() const { return ___ResultPanel_41; }
	inline GameObject_t1113636619 ** get_address_of_ResultPanel_41() { return &___ResultPanel_41; }
	inline void set_ResultPanel_41(GameObject_t1113636619 * value)
	{
		___ResultPanel_41 = value;
		Il2CppCodeGenWriteBarrier((&___ResultPanel_41), value);
	}

	inline static int32_t get_offset_of_ResultText_42() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ResultText_42)); }
	inline Text_t1901882714 * get_ResultText_42() const { return ___ResultText_42; }
	inline Text_t1901882714 ** get_address_of_ResultText_42() { return &___ResultText_42; }
	inline void set_ResultText_42(Text_t1901882714 * value)
	{
		___ResultText_42 = value;
		Il2CppCodeGenWriteBarrier((&___ResultText_42), value);
	}

	inline static int32_t get_offset_of_ResultNarrativeText_43() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ResultNarrativeText_43)); }
	inline Text_t1901882714 * get_ResultNarrativeText_43() const { return ___ResultNarrativeText_43; }
	inline Text_t1901882714 ** get_address_of_ResultNarrativeText_43() { return &___ResultNarrativeText_43; }
	inline void set_ResultNarrativeText_43(Text_t1901882714 * value)
	{
		___ResultNarrativeText_43 = value;
		Il2CppCodeGenWriteBarrier((&___ResultNarrativeText_43), value);
	}

	inline static int32_t get_offset_of_DeadHeroPanel_44() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DeadHeroPanel_44)); }
	inline GameObject_t1113636619 * get_DeadHeroPanel_44() const { return ___DeadHeroPanel_44; }
	inline GameObject_t1113636619 ** get_address_of_DeadHeroPanel_44() { return &___DeadHeroPanel_44; }
	inline void set_DeadHeroPanel_44(GameObject_t1113636619 * value)
	{
		___DeadHeroPanel_44 = value;
		Il2CppCodeGenWriteBarrier((&___DeadHeroPanel_44), value);
	}

	inline static int32_t get_offset_of_DeadZombiePanel_45() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DeadZombiePanel_45)); }
	inline GameObject_t1113636619 * get_DeadZombiePanel_45() const { return ___DeadZombiePanel_45; }
	inline GameObject_t1113636619 ** get_address_of_DeadZombiePanel_45() { return &___DeadZombiePanel_45; }
	inline void set_DeadZombiePanel_45(GameObject_t1113636619 * value)
	{
		___DeadZombiePanel_45 = value;
		Il2CppCodeGenWriteBarrier((&___DeadZombiePanel_45), value);
	}

	inline static int32_t get_offset_of_TextAnimation_46() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___TextAnimation_46)); }
	inline TextAnimation_t2694049294 * get_TextAnimation_46() const { return ___TextAnimation_46; }
	inline TextAnimation_t2694049294 ** get_address_of_TextAnimation_46() { return &___TextAnimation_46; }
	inline void set_TextAnimation_46(TextAnimation_t2694049294 * value)
	{
		___TextAnimation_46 = value;
		Il2CppCodeGenWriteBarrier((&___TextAnimation_46), value);
	}

	inline static int32_t get_offset_of_GameLog_47() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___GameLog_47)); }
	inline GameLog_t2697247111 * get_GameLog_47() const { return ___GameLog_47; }
	inline GameLog_t2697247111 ** get_address_of_GameLog_47() { return &___GameLog_47; }
	inline void set_GameLog_47(GameLog_t2697247111 * value)
	{
		___GameLog_47 = value;
		Il2CppCodeGenWriteBarrier((&___GameLog_47), value);
	}

	inline static int32_t get_offset_of_MapImages_48() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___MapImages_48)); }
	inline PlaceMapImages_t1484071588 * get_MapImages_48() const { return ___MapImages_48; }
	inline PlaceMapImages_t1484071588 ** get_address_of_MapImages_48() { return &___MapImages_48; }
	inline void set_MapImages_48(PlaceMapImages_t1484071588 * value)
	{
		___MapImages_48 = value;
		Il2CppCodeGenWriteBarrier((&___MapImages_48), value);
	}

	inline static int32_t get_offset_of_ZombieObjectivePanel_49() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ZombieObjectivePanel_49)); }
	inline GameObject_t1113636619 * get_ZombieObjectivePanel_49() const { return ___ZombieObjectivePanel_49; }
	inline GameObject_t1113636619 ** get_address_of_ZombieObjectivePanel_49() { return &___ZombieObjectivePanel_49; }
	inline void set_ZombieObjectivePanel_49(GameObject_t1113636619 * value)
	{
		___ZombieObjectivePanel_49 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieObjectivePanel_49), value);
	}

	inline static int32_t get_offset_of_HeroObjectivePanel_50() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___HeroObjectivePanel_50)); }
	inline GameObject_t1113636619 * get_HeroObjectivePanel_50() const { return ___HeroObjectivePanel_50; }
	inline GameObject_t1113636619 ** get_address_of_HeroObjectivePanel_50() { return &___HeroObjectivePanel_50; }
	inline void set_HeroObjectivePanel_50(GameObject_t1113636619 * value)
	{
		___HeroObjectivePanel_50 = value;
		Il2CppCodeGenWriteBarrier((&___HeroObjectivePanel_50), value);
	}

	inline static int32_t get_offset_of_ObjectivePrefab_51() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ObjectivePrefab_51)); }
	inline GameObject_t1113636619 * get_ObjectivePrefab_51() const { return ___ObjectivePrefab_51; }
	inline GameObject_t1113636619 ** get_address_of_ObjectivePrefab_51() { return &___ObjectivePrefab_51; }
	inline void set_ObjectivePrefab_51(GameObject_t1113636619 * value)
	{
		___ObjectivePrefab_51 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectivePrefab_51), value);
	}

	inline static int32_t get_offset_of_NarrativePanel_52() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___NarrativePanel_52)); }
	inline GameObject_t1113636619 * get_NarrativePanel_52() const { return ___NarrativePanel_52; }
	inline GameObject_t1113636619 ** get_address_of_NarrativePanel_52() { return &___NarrativePanel_52; }
	inline void set_NarrativePanel_52(GameObject_t1113636619 * value)
	{
		___NarrativePanel_52 = value;
		Il2CppCodeGenWriteBarrier((&___NarrativePanel_52), value);
	}

	inline static int32_t get_offset_of_NarrativeTitle_53() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___NarrativeTitle_53)); }
	inline Text_t1901882714 * get_NarrativeTitle_53() const { return ___NarrativeTitle_53; }
	inline Text_t1901882714 ** get_address_of_NarrativeTitle_53() { return &___NarrativeTitle_53; }
	inline void set_NarrativeTitle_53(Text_t1901882714 * value)
	{
		___NarrativeTitle_53 = value;
		Il2CppCodeGenWriteBarrier((&___NarrativeTitle_53), value);
	}

	inline static int32_t get_offset_of_NarrativeText_54() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___NarrativeText_54)); }
	inline Text_t1901882714 * get_NarrativeText_54() const { return ___NarrativeText_54; }
	inline Text_t1901882714 ** get_address_of_NarrativeText_54() { return &___NarrativeText_54; }
	inline void set_NarrativeText_54(Text_t1901882714 * value)
	{
		___NarrativeText_54 = value;
		Il2CppCodeGenWriteBarrier((&___NarrativeText_54), value);
	}

	inline static int32_t get_offset_of_DestroyBuildingPanel_55() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DestroyBuildingPanel_55)); }
	inline GameObject_t1113636619 * get_DestroyBuildingPanel_55() const { return ___DestroyBuildingPanel_55; }
	inline GameObject_t1113636619 ** get_address_of_DestroyBuildingPanel_55() { return &___DestroyBuildingPanel_55; }
	inline void set_DestroyBuildingPanel_55(GameObject_t1113636619 * value)
	{
		___DestroyBuildingPanel_55 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyBuildingPanel_55), value);
	}

	inline static int32_t get_offset_of_DestroyBuildingZombieSlider_56() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DestroyBuildingZombieSlider_56)); }
	inline Slider_t3903728902 * get_DestroyBuildingZombieSlider_56() const { return ___DestroyBuildingZombieSlider_56; }
	inline Slider_t3903728902 ** get_address_of_DestroyBuildingZombieSlider_56() { return &___DestroyBuildingZombieSlider_56; }
	inline void set_DestroyBuildingZombieSlider_56(Slider_t3903728902 * value)
	{
		___DestroyBuildingZombieSlider_56 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyBuildingZombieSlider_56), value);
	}

	inline static int32_t get_offset_of_DestroyBuildingDropdown_57() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DestroyBuildingDropdown_57)); }
	inline Dropdown_t2274391225 * get_DestroyBuildingDropdown_57() const { return ___DestroyBuildingDropdown_57; }
	inline Dropdown_t2274391225 ** get_address_of_DestroyBuildingDropdown_57() { return &___DestroyBuildingDropdown_57; }
	inline void set_DestroyBuildingDropdown_57(Dropdown_t2274391225 * value)
	{
		___DestroyBuildingDropdown_57 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyBuildingDropdown_57), value);
	}

	inline static int32_t get_offset_of_DestroyBuildingDeadZombiesText_58() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DestroyBuildingDeadZombiesText_58)); }
	inline Text_t1901882714 * get_DestroyBuildingDeadZombiesText_58() const { return ___DestroyBuildingDeadZombiesText_58; }
	inline Text_t1901882714 ** get_address_of_DestroyBuildingDeadZombiesText_58() { return &___DestroyBuildingDeadZombiesText_58; }
	inline void set_DestroyBuildingDeadZombiesText_58(Text_t1901882714 * value)
	{
		___DestroyBuildingDeadZombiesText_58 = value;
		Il2CppCodeGenWriteBarrier((&___DestroyBuildingDeadZombiesText_58), value);
	}

	inline static int32_t get_offset_of_ZombiePitPanel_59() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ZombiePitPanel_59)); }
	inline GameObject_t1113636619 * get_ZombiePitPanel_59() const { return ___ZombiePitPanel_59; }
	inline GameObject_t1113636619 ** get_address_of_ZombiePitPanel_59() { return &___ZombiePitPanel_59; }
	inline void set_ZombiePitPanel_59(GameObject_t1113636619 * value)
	{
		___ZombiePitPanel_59 = value;
		Il2CppCodeGenWriteBarrier((&___ZombiePitPanel_59), value);
	}

	inline static int32_t get_offset_of_TutorialPanel_60() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___TutorialPanel_60)); }
	inline GameObject_t1113636619 * get_TutorialPanel_60() const { return ___TutorialPanel_60; }
	inline GameObject_t1113636619 ** get_address_of_TutorialPanel_60() { return &___TutorialPanel_60; }
	inline void set_TutorialPanel_60(GameObject_t1113636619 * value)
	{
		___TutorialPanel_60 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialPanel_60), value);
	}

	inline static int32_t get_offset_of_TutorialText_61() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___TutorialText_61)); }
	inline Text_t1901882714 * get_TutorialText_61() const { return ___TutorialText_61; }
	inline Text_t1901882714 ** get_address_of_TutorialText_61() { return &___TutorialText_61; }
	inline void set_TutorialText_61(Text_t1901882714 * value)
	{
		___TutorialText_61 = value;
		Il2CppCodeGenWriteBarrier((&___TutorialText_61), value);
	}

	inline static int32_t get_offset_of_DebugCardList_62() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___DebugCardList_62)); }
	inline Dropdown_t2274391225 * get_DebugCardList_62() const { return ___DebugCardList_62; }
	inline Dropdown_t2274391225 ** get_address_of_DebugCardList_62() { return &___DebugCardList_62; }
	inline void set_DebugCardList_62(Dropdown_t2274391225 * value)
	{
		___DebugCardList_62 = value;
		Il2CppCodeGenWriteBarrier((&___DebugCardList_62), value);
	}

	inline static int32_t get_offset_of_damageMaterial_63() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___damageMaterial_63)); }
	inline Material_t340375123 * get_damageMaterial_63() const { return ___damageMaterial_63; }
	inline Material_t340375123 ** get_address_of_damageMaterial_63() { return &___damageMaterial_63; }
	inline void set_damageMaterial_63(Material_t340375123 * value)
	{
		___damageMaterial_63 = value;
		Il2CppCodeGenWriteBarrier((&___damageMaterial_63), value);
	}

	inline static int32_t get_offset_of_clickAudio_64() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___clickAudio_64)); }
	inline AudioSource_t3935305588 * get_clickAudio_64() const { return ___clickAudio_64; }
	inline AudioSource_t3935305588 ** get_address_of_clickAudio_64() { return &___clickAudio_64; }
	inline void set_clickAudio_64(AudioSource_t3935305588 * value)
	{
		___clickAudio_64 = value;
		Il2CppCodeGenWriteBarrier((&___clickAudio_64), value);
	}

	inline static int32_t get_offset_of_audioZombieDead_65() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___audioZombieDead_65)); }
	inline AudioSource_t3935305588 * get_audioZombieDead_65() const { return ___audioZombieDead_65; }
	inline AudioSource_t3935305588 ** get_address_of_audioZombieDead_65() { return &___audioZombieDead_65; }
	inline void set_audioZombieDead_65(AudioSource_t3935305588 * value)
	{
		___audioZombieDead_65 = value;
		Il2CppCodeGenWriteBarrier((&___audioZombieDead_65), value);
	}

	inline static int32_t get_offset_of_audioZombieEvades_66() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___audioZombieEvades_66)); }
	inline AudioSource_t3935305588 * get_audioZombieEvades_66() const { return ___audioZombieEvades_66; }
	inline AudioSource_t3935305588 ** get_address_of_audioZombieEvades_66() { return &___audioZombieEvades_66; }
	inline void set_audioZombieEvades_66(AudioSource_t3935305588 * value)
	{
		___audioZombieEvades_66 = value;
		Il2CppCodeGenWriteBarrier((&___audioZombieEvades_66), value);
	}

	inline static int32_t get_offset_of_audioHeroDead_67() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___audioHeroDead_67)); }
	inline AudioSource_t3935305588 * get_audioHeroDead_67() const { return ___audioHeroDead_67; }
	inline AudioSource_t3935305588 ** get_address_of_audioHeroDead_67() { return &___audioHeroDead_67; }
	inline void set_audioHeroDead_67(AudioSource_t3935305588 * value)
	{
		___audioHeroDead_67 = value;
		Il2CppCodeGenWriteBarrier((&___audioHeroDead_67), value);
	}

	inline static int32_t get_offset_of_isDebugLog_68() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___isDebugLog_68)); }
	inline bool get_isDebugLog_68() const { return ___isDebugLog_68; }
	inline bool* get_address_of_isDebugLog_68() { return &___isDebugLog_68; }
	inline void set_isDebugLog_68(bool value)
	{
		___isDebugLog_68 = value;
	}

	inline static int32_t get_offset_of_ViewableUI_69() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___ViewableUI_69)); }
	inline ViewableUI_t3820486676 * get_ViewableUI_69() const { return ___ViewableUI_69; }
	inline ViewableUI_t3820486676 ** get_address_of_ViewableUI_69() { return &___ViewableUI_69; }
	inline void set_ViewableUI_69(ViewableUI_t3820486676 * value)
	{
		___ViewableUI_69 = value;
		Il2CppCodeGenWriteBarrier((&___ViewableUI_69), value);
	}

	inline static int32_t get_offset_of_U3CCardsToPlayU3Ek__BackingField_70() { return static_cast<int32_t>(offsetof(ZombieTurn_t3562480803, ___U3CCardsToPlayU3Ek__BackingField_70)); }
	inline List_1_t3709728484 * get_U3CCardsToPlayU3Ek__BackingField_70() const { return ___U3CCardsToPlayU3Ek__BackingField_70; }
	inline List_1_t3709728484 ** get_address_of_U3CCardsToPlayU3Ek__BackingField_70() { return &___U3CCardsToPlayU3Ek__BackingField_70; }
	inline void set_U3CCardsToPlayU3Ek__BackingField_70(List_1_t3709728484 * value)
	{
		___U3CCardsToPlayU3Ek__BackingField_70 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCardsToPlayU3Ek__BackingField_70), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIETURN_T3562480803_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#define DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t1588957063  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_2;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_2() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___mTrackableBehaviour_2)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_2() const { return ___mTrackableBehaviour_2; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_2() { return &___mTrackableBehaviour_2; }
	inline void set_mTrackableBehaviour_2(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t3109936861  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.String DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_2;
	// System.Boolean DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_3;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::bodyStyle
	GUIStyle_t3956901511 * ___bodyStyle_5;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::headerStyle
	GUIStyle_t3956901511 * ___headerStyle_6;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::footerStyle
	GUIStyle_t3956901511 * ___footerStyle_7;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::bodyTexture
	Texture2D_t3840446185 * ___bodyTexture_8;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::headerTexture
	Texture2D_t3840446185 * ___headerTexture_9;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::footerTexture
	Texture2D_t3840446185 * ___footerTexture_10;

public:
	inline static int32_t get_offset_of_mErrorText_2() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorText_2)); }
	inline String_t* get_mErrorText_2() const { return ___mErrorText_2; }
	inline String_t** get_address_of_mErrorText_2() { return &___mErrorText_2; }
	inline void set_mErrorText_2(String_t* value)
	{
		___mErrorText_2 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_2), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_3() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorOccurred_3)); }
	inline bool get_mErrorOccurred_3() const { return ___mErrorOccurred_3; }
	inline bool* get_address_of_mErrorOccurred_3() { return &___mErrorOccurred_3; }
	inline void set_mErrorOccurred_3(bool value)
	{
		___mErrorOccurred_3 = value;
	}

	inline static int32_t get_offset_of_bodyStyle_5() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyStyle_5)); }
	inline GUIStyle_t3956901511 * get_bodyStyle_5() const { return ___bodyStyle_5; }
	inline GUIStyle_t3956901511 ** get_address_of_bodyStyle_5() { return &___bodyStyle_5; }
	inline void set_bodyStyle_5(GUIStyle_t3956901511 * value)
	{
		___bodyStyle_5 = value;
		Il2CppCodeGenWriteBarrier((&___bodyStyle_5), value);
	}

	inline static int32_t get_offset_of_headerStyle_6() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerStyle_6)); }
	inline GUIStyle_t3956901511 * get_headerStyle_6() const { return ___headerStyle_6; }
	inline GUIStyle_t3956901511 ** get_address_of_headerStyle_6() { return &___headerStyle_6; }
	inline void set_headerStyle_6(GUIStyle_t3956901511 * value)
	{
		___headerStyle_6 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_6), value);
	}

	inline static int32_t get_offset_of_footerStyle_7() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerStyle_7)); }
	inline GUIStyle_t3956901511 * get_footerStyle_7() const { return ___footerStyle_7; }
	inline GUIStyle_t3956901511 ** get_address_of_footerStyle_7() { return &___footerStyle_7; }
	inline void set_footerStyle_7(GUIStyle_t3956901511 * value)
	{
		___footerStyle_7 = value;
		Il2CppCodeGenWriteBarrier((&___footerStyle_7), value);
	}

	inline static int32_t get_offset_of_bodyTexture_8() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyTexture_8)); }
	inline Texture2D_t3840446185 * get_bodyTexture_8() const { return ___bodyTexture_8; }
	inline Texture2D_t3840446185 ** get_address_of_bodyTexture_8() { return &___bodyTexture_8; }
	inline void set_bodyTexture_8(Texture2D_t3840446185 * value)
	{
		___bodyTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTexture_8), value);
	}

	inline static int32_t get_offset_of_headerTexture_9() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerTexture_9)); }
	inline Texture2D_t3840446185 * get_headerTexture_9() const { return ___headerTexture_9; }
	inline Texture2D_t3840446185 ** get_address_of_headerTexture_9() { return &___headerTexture_9; }
	inline void set_headerTexture_9(Texture2D_t3840446185 * value)
	{
		___headerTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___headerTexture_9), value);
	}

	inline static int32_t get_offset_of_footerTexture_10() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerTexture_10)); }
	inline Texture2D_t3840446185 * get_footerTexture_10() const { return ___footerTexture_10; }
	inline Texture2D_t3840446185 ** get_address_of_footerTexture_10() { return &___footerTexture_10; }
	inline void set_footerTexture_10(Texture2D_t3840446185 * value)
	{
		___footerTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___footerTexture_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (RelentlessAdvance_t2095454104), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (HeavyRain_t332432098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (ATownOverrun_t175921820), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (MyGodTheyveTakenThe_t4142636702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (NewSpawningPit_t552318903), -1, sizeof(NewSpawningPit_t552318903_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2704[2] = 
{
	NewSpawningPit_t552318903_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
	NewSpawningPit_t552318903_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (OhTheHorror_t2613725065), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (SurpriseAttack_t2653573337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (Shamble_t2552159791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (Uuuurrrggghh_t1205182777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (Braains_t332229191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (UndeadHateTheLiving_t3645703618), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (TheresTooMany_t2689988620), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (Resilient_t2897756277), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (LossOfFaith_t1416337014), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (Trip_t609988995), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (HauntedByThePast_t1055848434), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (IDontTrustEm_t2604071939), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (IveGotToGetToThe_t3595394397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (MyGodHesAZombie_t2959971631), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (Overconfidence_t2812648673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (TeenAngst_t2416046438), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (ThisCantBeHappening_t2204035598), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (LivingNightmare_t2875092037), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (LockedDoor_t3547742430), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (NightThatNeverEnds_t930728108), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (RottenBodies_t748955989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (TheresNoTimeLeaveIt_t1342240655), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (IFellKindaStrange_t3376890855), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (Bickering_t3580212373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (ThisCouldBeOurLastNightOnEarth_t3976126266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (UnnecessarySelfSacrifice_t890063265), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (Loner_t2606872010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (DesperateForFlesh_t2132404054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (GrowingHunger_t1321750346), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (NowhereToHide_t3704763644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (TheyreComingFromThe_t2799902499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (DraggingMeat_t3424770081), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (CaughtOffGuard_t3812377631), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (FightingInstincts_t2852332482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (Overwhelmed_t420465551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (ScratchAndClaw_t3789112499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (Bitten_t2205689511), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (TheyreEverywhere_t2749774660), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (LetsSplitUp_t2094917924), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (Catfight_t1469214242), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (Despair_t771065786), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (ItsStuck_t414080583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (KnockedAway_t1831705761), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (TheLineIsDead_t2968485054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (NowhereToRun_t2169937835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (ZombieSurge_t4069364120), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (HungryDead_t869998133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (TwistedAnkle_t4165690136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (Hopeless_t2016799881), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (FightForSurvival_t1379125000), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (ToolsOfTheGrave_t2598357773), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (TideOfTheDead_t2906667500), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (DeadlySurprise_t2801286068), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (NoItCantBe_t3821047303), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (Unstoppable_t2769904673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (LegionsOfTheDead_t3250160808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (TownSecrets_t1576979025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (DanceOfTheDead_t3960488959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (FeelsNoPain_t340595724), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (RisenFromTheGrave_t127519008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (TheSmellOfBrains_t1861641393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (DivideAndConquer_t4022414097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (AngryDead_t1938627711), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (ClosingIn_t1878830773), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (FallingDarkness_t3315734010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (Trapped_t3561378555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (Hysteria_t3350862265), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (AnxiousToFeed_t3134021366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (TheHungryOne_t928842377), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (ZombieDeadScreen_t2482992935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[7] = 
{
	ZombieDeadScreen_t2482992935::get_offset_of_Option1_2(),
	ZombieDeadScreen_t2482992935::get_offset_of_Option2_3(),
	ZombieDeadScreen_t2482992935::get_offset_of_Option3_4(),
	ZombieDeadScreen_t2482992935::get_offset_of_Option4_5(),
	ZombieDeadScreen_t2482992935::get_offset_of_Option5_6(),
	ZombieDeadScreen_t2482992935::get_offset_of_ZombieDeadPanel_7(),
	ZombieDeadScreen_t2482992935::get_offset_of_zombieTurn_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (ZombieDeck_t3038342722), -1, sizeof(ZombieDeck_t3038342722_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2775[7] = 
{
	ZombieDeck_t3038342722::get_offset_of_deck_0(),
	ZombieDeck_t3038342722::get_offset_of_discard_1(),
	ZombieDeck_t3038342722::get_offset_of_hand_2(),
	ZombieDeck_t3038342722::get_offset_of_handDoNotPlay_3(),
	ZombieDeck_t3038342722::get_offset_of_remains_4(),
	ZombieDeck_t3038342722::get_offset_of_uniqueCards_5(),
	ZombieDeck_t3038342722_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (U3CAddCardsToDeckU3Ec__AnonStorey0_t558456971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[1] = 
{
	U3CAddCardsToDeckU3Ec__AnonStorey0_t558456971::get_offset_of_card_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (U3CCheckIfInHandU3Ec__AnonStorey1_t2015387922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[1] = 
{
	U3CCheckIfInHandU3Ec__AnonStorey1_t2015387922::get_offset_of_cardPhase_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (U3CGetCardU3Ec__AnonStorey2_t3292952327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[1] = 
{
	U3CGetCardU3Ec__AnonStorey2_t3292952327::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (ZombieTurn_t3562480803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2779[69] = 
{
	ZombieTurn_t3562480803::get_offset_of_setupState_2(),
	ZombieTurn_t3562480803::get_offset_of_decks_3(),
	ZombieTurn_t3562480803::get_offset_of_gameStats_4(),
	ZombieTurn_t3562480803::get_offset_of_spawn_5(),
	ZombieTurn_t3562480803::get_offset_of_zombiesWinOnTie_6(),
	ZombieTurn_t3562480803::get_offset_of_zombieMovesAfterSpawn_7(),
	ZombieTurn_t3562480803::get_offset_of_theHungryOne_8(),
	ZombieTurn_t3562480803::get_offset_of_fightCardUsed_9(),
	ZombieTurn_t3562480803::get_offset_of_round_10(),
	ZombieTurn_t3562480803::get_offset_of_heroesDiedCount_11(),
	ZombieTurn_t3562480803::get_offset_of_cardBeingPlayed_12(),
	ZombieTurn_t3562480803::get_offset_of_gameData_13(),
	ZombieTurn_t3562480803::get_offset_of_gameObjectives_14(),
	ZombieTurn_t3562480803::get_offset_of_currentSetupState_15(),
	ZombieTurn_t3562480803::get_offset_of_takenOverBuilding_16(),
	ZombieTurn_t3562480803::get_offset_of_zombieToFight_17(),
	ZombieTurn_t3562480803::get_offset_of_numberOfHeroesInGame_18(),
	ZombieTurn_t3562480803::get_offset_of_cardsPlayed_19(),
	ZombieTurn_t3562480803::get_offset_of_Phases_20(),
	ZombieTurn_t3562480803::get_offset_of_SetupPanel_21(),
	ZombieTurn_t3562480803::get_offset_of_SetupText_22(),
	ZombieTurn_t3562480803::get_offset_of_MainUI_23(),
	ZombieTurn_t3562480803::get_offset_of_Card_24(),
	ZombieTurn_t3562480803::get_offset_of_CardTitle_25(),
	ZombieTurn_t3562480803::get_offset_of_CardText_26(),
	ZombieTurn_t3562480803::get_offset_of_CardFlavor_27(),
	ZombieTurn_t3562480803::get_offset_of_ZombiesOnBoardText_28(),
	ZombieTurn_t3562480803::get_offset_of_ScenarioName_29(),
	ZombieTurn_t3562480803::get_offset_of_MovePanel_30(),
	ZombieTurn_t3562480803::get_offset_of_MoveText_31(),
	ZombieTurn_t3562480803::get_offset_of_MoveZombieTypeText_32(),
	ZombieTurn_t3562480803::get_offset_of_FightPanel_33(),
	ZombieTurn_t3562480803::get_offset_of_FightText_34(),
	ZombieTurn_t3562480803::get_offset_of_FightRollResultParent_35(),
	ZombieTurn_t3562480803::get_offset_of_FightRollResultPrefab_36(),
	ZombieTurn_t3562480803::get_offset_of_SpawnPanel_37(),
	ZombieTurn_t3562480803::get_offset_of_SpawnText_38(),
	ZombieTurn_t3562480803::get_offset_of_RemainsInPlay_39(),
	ZombieTurn_t3562480803::get_offset_of_RemainsInPlayElement_40(),
	ZombieTurn_t3562480803::get_offset_of_ResultPanel_41(),
	ZombieTurn_t3562480803::get_offset_of_ResultText_42(),
	ZombieTurn_t3562480803::get_offset_of_ResultNarrativeText_43(),
	ZombieTurn_t3562480803::get_offset_of_DeadHeroPanel_44(),
	ZombieTurn_t3562480803::get_offset_of_DeadZombiePanel_45(),
	ZombieTurn_t3562480803::get_offset_of_TextAnimation_46(),
	ZombieTurn_t3562480803::get_offset_of_GameLog_47(),
	ZombieTurn_t3562480803::get_offset_of_MapImages_48(),
	ZombieTurn_t3562480803::get_offset_of_ZombieObjectivePanel_49(),
	ZombieTurn_t3562480803::get_offset_of_HeroObjectivePanel_50(),
	ZombieTurn_t3562480803::get_offset_of_ObjectivePrefab_51(),
	ZombieTurn_t3562480803::get_offset_of_NarrativePanel_52(),
	ZombieTurn_t3562480803::get_offset_of_NarrativeTitle_53(),
	ZombieTurn_t3562480803::get_offset_of_NarrativeText_54(),
	ZombieTurn_t3562480803::get_offset_of_DestroyBuildingPanel_55(),
	ZombieTurn_t3562480803::get_offset_of_DestroyBuildingZombieSlider_56(),
	ZombieTurn_t3562480803::get_offset_of_DestroyBuildingDropdown_57(),
	ZombieTurn_t3562480803::get_offset_of_DestroyBuildingDeadZombiesText_58(),
	ZombieTurn_t3562480803::get_offset_of_ZombiePitPanel_59(),
	ZombieTurn_t3562480803::get_offset_of_TutorialPanel_60(),
	ZombieTurn_t3562480803::get_offset_of_TutorialText_61(),
	ZombieTurn_t3562480803::get_offset_of_DebugCardList_62(),
	ZombieTurn_t3562480803::get_offset_of_damageMaterial_63(),
	ZombieTurn_t3562480803::get_offset_of_clickAudio_64(),
	ZombieTurn_t3562480803::get_offset_of_audioZombieDead_65(),
	ZombieTurn_t3562480803::get_offset_of_audioZombieEvades_66(),
	ZombieTurn_t3562480803::get_offset_of_audioHeroDead_67(),
	ZombieTurn_t3562480803::get_offset_of_isDebugLog_68(),
	ZombieTurn_t3562480803::get_offset_of_ViewableUI_69(),
	ZombieTurn_t3562480803::get_offset_of_U3CCardsToPlayU3Ek__BackingField_70(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (U3CPlayCardU3Ec__Iterator0_t1992789720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[5] = 
{
	U3CPlayCardU3Ec__Iterator0_t1992789720::get_offset_of_U3CcardAnimatorU3E__0_0(),
	U3CPlayCardU3Ec__Iterator0_t1992789720::get_offset_of_U24this_1(),
	U3CPlayCardU3Ec__Iterator0_t1992789720::get_offset_of_U24current_2(),
	U3CPlayCardU3Ec__Iterator0_t1992789720::get_offset_of_U24disposing_3(),
	U3CPlayCardU3Ec__Iterator0_t1992789720::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (U3CFightRollU3Ec__AnonStorey1_t3184696250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[2] = 
{
	U3CFightRollU3Ec__AnonStorey1_t3184696250::get_offset_of_index_0(),
	U3CFightRollU3Ec__AnonStorey1_t3184696250::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (ZombieTurnPhaseName_t860305160)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2782[12] = 
{
	ZombieTurnPhaseName_t860305160::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (ZombieTurnPhases_t3127322021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2783[4] = 
{
	ZombieTurnPhases_t3127322021::get_offset_of_TurnPhaseDelegate_0(),
	ZombieTurnPhases_t3127322021::get_offset_of_zombieTurn_1(),
	ZombieTurnPhases_t3127322021::get_offset_of_MoveZombieIndex_2(),
	ZombieTurnPhases_t3127322021::get_offset_of_PhaseName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (TurnPhaseDelegateDeclare_t3595605765), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (SpeechRecognizerDemoSceneManager_t231664079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2785[3] = 
{
	SpeechRecognizerDemoSceneManager_t231664079::get_offset_of__speechManager_2(),
	SpeechRecognizerDemoSceneManager_t231664079::get_offset_of__isListening_3(),
	SpeechRecognizerDemoSceneManager_t231664079::get_offset_of__message_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (DefaultInitializationErrorHandler_t3109936861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2786[9] = 
{
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorText_2(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorOccurred_3(),
	0,
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyStyle_5(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerStyle_6(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerStyle_7(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyTexture_8(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerTexture_9(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerTexture_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (DefaultTrackableEventHandler_t1588957063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2787[1] = 
{
	DefaultTrackableEventHandler_t1588957063::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255367), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2788[171] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DA2F1578C0F6612632B975A3AFE7CB9184E9F5590_0(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D81AFD0604569DA0255357C175BE345F58F17D478_1(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D380E84549CB845604C318E8E14B73622CC10AF42_2(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D963821E4BFFA8EED3A18D7173F2147779057B9B3_3(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD4EF3A9F65679752698916F2F8DBCD78EE2E41B8_4(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D1CA1C79F41AEC3E78B9270CE6E221118308DFFD5_5(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D1DA0256192B60CA2089E8E33FA9F10415D904C9B_6(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D49610B6D06207419DD075235B5603F1860DDFB14_7(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D949FFA4D55D6CAF924550A0169CBEEF3F51C7F2A_8(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DEC16EE82A2343F7D332375CE590AFA11FFC6C5F0_9(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D2223729B78EBEE4B2E434CA9BAF047311413D5C0_10(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D529B72F43D5DB46080436118B23FCEA9D7F66A32_11(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D46284D0F43072429162AA2925082FFAC2C0DE31A_12(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D9EDEF80E922E3463CA046C98E9CFF518CFD1FECA_13(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D82091C20834853F435A614D3DCBC5AD6D9FEF27F_14(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DC66129415B3CAF838A8150A682848D3F4ACAAFC1_15(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D6634EA9EA45C5BDEBFF437FC9807140E0BFA58AC_16(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DB2ABE92600B53F2B8A03B9A9E2C734A8BE0C6415_17(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DA8B6B603D922A9A2949780CB840679B621BF5FFC_18(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DF7079845619834F11587B9DB9E90CC2A70A46537_19(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D61070D52649698A08E65825BD36FECC1192D073E_20(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D5DBF7686821F23413E1AAE8AF7A402B09C9D8146_21(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D8D2947A8FBC9D80A21D326AA99776795CBA397A6_22(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D7FB6B136550965286B96948AAF7CDC523BDE616B_23(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D25AC842A810AAB8E814EF9790AE6B3A4D308A6AB_24(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D5A349BB4A3D6CA8C50495829F97677BD24485ED5_25(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D5A39C6D3B7176F7D64895BCE1D0626701B464574_26(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DC3C69E342B9530497D4B3C71703D4D173F41E821_27(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DA5F1D7C6F187BBF6A2D1F8DF463AB900CEF4DB13_28(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D7B8E1626915C75E04FD306C808857E4B49CBED19_29(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D6AD3CA70A847F1E1D25AD94EE69F696A4EF4C1A2_30(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DAD198C361B74FABBD2A491412784194700500496_31(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DC8172F3DC319ED9F9E4AD940E4E3A172BB870BC6_32(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DC69FBB5DF53AD568630EE6742E3E8AB052C3144F_33(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D9B2219C436BB44093B17B05D7C37A95D732BDF1A_34(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D52534D8994C76421C9DFA10C8E8A343500B12F38_35(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D525F126E43DA5737F65A5AD29C20B79DE19010A6_36(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D7B5372F481A98563710C701B145AADD910A18C91_37(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DB7C9C48891656F222702DCE29351381A7D30D3E7_38(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD94579B1058818D44D4310948B7C46070767536D_39(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DAF3BA40B1D20EEA4FD9D64C9BAF514D5EF8B2A3C_40(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DE8E26DD65BF96015D3F99B815805F6C58C21B23E_41(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D38A81B10112EBC8F090274B9571D818D8F7E3D2C_42(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D1E55BBA47A1186A42E6BBA84D30C72C932A811D3_43(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D1C4E377322BB013A6F15E832A61BB098615DF0B8_44(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D9C4A054E93EF5C04D40B82BEADCFAD5CF4D86DEA_45(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DCFA2FBADAA7ADDA055EA831A821AEDB1D14BAB9D_46(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D60323ABD2D7C22F62E865DF1F748CF1AF8AD9B7F_47(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D64DE266A45F5F7D5898FA6A7529673C84D5BA650_48(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DB3A75CC32F78173CF10F472B21A54DAFAAA20F32_49(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DDA8B2917B2EEA744BF4521F61E17227E77F19519_50(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD1B8C323C9E44C56DDF066DCFA8FD6B89AE29B4D_51(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DADDDB448B434C9ECFE51345E3CC896D2A7FEA1AD_52(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D4FDE5D193D740FA6EDE761A161800A8AFFCEA9BD_53(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DAF9989AA1811EBBCBC515B53CAE9AE32F6A8C504_54(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D85677C568E01B93F32A92ABE48155AD70F976DAD_55(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D4C1D6C591828D866127110DDE812D542CC9331BC_56(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D73C9519F21426AFF18B3E246AB329D7081D74629_57(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DF01D68EF083172EDC245696B4DFB5B813C4C40D6_58(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D857FADA51A4C3CCFFB1E9A68291CC4C0444975B4_59(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D5AD50533400F104F24486A35E159B3A7414E6984_60(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D649374558406F6736E729396840BC136C2B5BFD9_61(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DB7EB9205D5C176BD8A77FBE82679E8CC0D7E5FA8_62(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D7190DEAEAB70C823A76B61E3789D9685510816ED_63(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D7C93AB5D9B0DD4DE43D8459D61FEAB642F5718EB_64(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D94776C9848170581D079C920A080F505862BA5F5_65(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DB8DD06F686E2962EFFE0C0135249BA1EEBD189F2_66(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D27888A1263310BD05ADF3C66E433E62AC22837D4_67(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D43E7F55B82FD915DDC4D5C8443D0BBB9D27CC671_68(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D567BDB060ABADAA9816DE75D30D09ECBDBAFBB37_69(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D3B7119575F4FB85ACD75255CE7CE86293C11DCE7_70(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DED9CB0715AFCEA441D9FCF2208BF8FCCC2174D05_71(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DA17570D5B6C2DAAE95E31C43B263CE1D47082979_72(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D96B97D48A5B812040C8C4972167A1D73D48C17C1_73(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D3A99CC082D9B62CBA73369CD8566DC6D2D3CE56B_74(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D0BAAEA820015031AACD0305342ACA2F73FE000BF_75(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D1FF2C4D3129A713375C286776E2A047579F3B460_76(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D7D6AC3825D861BDCAF6E43C0F58F89B8E92AC302_77(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D28888B3CDD7E5D6AA2A5A4FDDC2CC08247D97BE8_78(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD7B1D09721FE120EEE473015E875CA4C36093305_79(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DDF301B5B93AC5A743ADF90679C1E31A3E5DF913C_80(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DFBBD0BE893F85B13E4036721A87894B24985AB5F_81(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DC3F4AC9D069D1C1AAF76D0F721BC71CE766F9C9C_82(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD69752D8BDA7699A11F2988E209F862C81080672_83(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D87A054A90E6EE560D4055BFCC8F875CE2A532AA9_84(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D22DCDD3F71C792BEE79BF79657972F19E8503C28_85(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D19801FABF900B8A91B3B7A8C19554CF21938A328_86(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD3198CF22F3142E548C0ADEEBCBA1A1DF6C1FA76_87(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D626E626D6BCD38FA699B0752E2D91A81B8CFD074_88(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D2841ABEC55FB3862F45BA8E5BD854FECB89B2C0B_89(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D0BDDE6F0882E6AD06D0FECC073EB61DC8779A234_90(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DDED837EA58E0B97FD24A53995FCEC1D3A8414EEC_91(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D62949AE93481AD89B9E39B9A17AED79A75EF656F_92(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D188D091BDAA625C458629AFB438C9CF1110A5749_93(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DE5E7D260D4BD2EF3D7E754EFCDB39BDC6886040C_94(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DC0F8B663B994BB22A0819A77DC709EB6734A86EF_95(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D472AB78FCC6457EB1051AB776C54D1B5479AD3A2_96(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D6CFD6E9B2590DEC938E2F8711AFFA7803B7789D3_97(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D9AD10A6B52DCB7C2044C028935E73252BCF32185_98(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD0410F28A9A96BD53F0C8CFD98F9A0D450FA002F_99(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DB8400EAE6B6E484B58EE5FDA5240F2235204F77A_100(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D7A03E895CDEE6AF52BE3ECF5383175E0F218584C_101(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D0D7536E292838F2850B78C40CE965EF0FB40CB93_102(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D6E84A598EB70DF96202A625C63B66975EED796EE_103(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D11363D33BC05212CEC6843E96E66163F908899C3_104(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D614578BC1B89AB094AC64BFBA9BCC07CFE7A5C52_105(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DC4D32E400AA14406F6E11289FC511F0A2BFB765E_106(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D99380A2C88E75E6C9171C58173F9995E7CB0DC96_107(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD921D1E53ACB96FA59820910A1C6957F797725FB_108(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DFE8385470E173E2E7C2D9D94D9F81163CA600F0D_109(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D68845C029EE5C0E5EC8821721F587C33BB9284E8_110(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D9AB51121DB726549C24CD8652D05D2279597A826_111(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D5A66B903EF5A119B6BADBE48C36C6F935D87D3A8_112(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D0AB03169304F60655F2A2014ED877F0B4F8F86E3_113(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DA41A4536581FF8C104E66759035DFB236DFF0994_114(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D87FA64DAA4DA09B4C03E8060CBABE9BF63DFA0E6_115(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D75C89CF0447460CFB3ABAACBC205DC204F823B77_116(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D20A70D944EED422690C9BA10EF51A59A6A9B603A_117(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D970D607246F4725BCA45E7564DBC8E90DF8114B5_118(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D714E5457816BBE2A319AAC07D2C587A676A85EF5_119(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DBCC954BE08EE00F495F849D2F161752479934142_120(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DF2CBB55BC65F72A214C8CF9323FBED9861F2F4BC_121(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D1CDE377C407F39C592524FDAFE062059B9165E3F_122(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D6BECC23C94DC5127540ABE6CE7BE88684C295C5D_123(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D928F91AB9C19903D2B1009F932437DFCA5CDC584_124(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DF83C96E93F812D711747D3CC98683620B651BC5C_125(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D12A155BF457332BF81F6684CCE56CC9C2A3A5076_126(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D0D1AE827D0A9B6FD7B028D3A6670BF59BCA86701_127(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D6E2460C8BE8220FB196B278A5D70BC3556975E55_128(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D48322419F79C852FFF1227DCB802B7C8CE2236EB_129(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DDE4B828B867A48EAB4617DC73A150B02BD24E72C_130(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DCD6B5BC785A3A5AFE10307374DB684F6132B1C57_131(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DC420C0CA6FC58CFAADB02A5DA53391092FBB894A_132(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DE636A7059162508F5B0029ABA2C0826FBC218247_133(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD6D3C6667F50B055D22DD3D8FAE411326678A71F_134(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DCD7F453E7DAD8B3B8D8AB82971B07921BFC4A3D3_135(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DE6570482AA640720A812DC9F4302ACCFF92B93F1_136(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DCEAA201847F942506CE4A6D33EB8D87AF00B4AA0_137(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D242F9F981726198546AE22426078FC2D0AFDFC7D_138(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD178CEED7BBEBC7AA0DA1484363B0ADC369532B1_139(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D535985E95CA011ACE58FC3BEE4E4270F4F761478_140(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D8BD44E6B764A64B6DE9F115D610775748364EB71_141(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D74BBA41B92CCB948BD0B17E1D9DE0DA8BEFE2304_142(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D52DD1DEF5C43780A82B460C7C23188B55B918057_143(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DAB8935CA29ED45F3F387F2EB8823E19EC88661DA_144(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DE2EC72AD9A1FECD786D35F2F342F03AD95B14DD4_145(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D630C3F5D7DBB07B36134EB1AFA096BAA59350F0C_146(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DD26254A9468BBBB28AC07899A05D759FD80FE5C5_147(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D8DC2ABCA7A46A411D39035D8806FBA7EB22432FB_148(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DB75CE37738CED7D6746D306511B391A9F91684D0_149(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D980BDEDFAACFEB591A358FAFC522C3233E8414D8_150(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D3DFDD69B416E892411930CA44E469FC0EE1A748E_151(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D9A7664FF66C09B23E2674031BADDADA42C2E7338_152(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D950965F7AE6D3878D95DB10B185EC9969281928B_153(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DE37060512A6142D8BDB50F96F5A31B3F7FD9C811_154(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DCFD89EB7C35C2C4305E84198CEE1E467C5B685FC_155(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DE4692158166F085D6DB6AA88DD688573157DDE59_156(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DE3B39965C126C790271ABFAAC5BAF0CB3544F01C_157(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DE69D5483E961E76DC16F84DD4D0CF8A1E8AEBFED_158(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D263ADBD7E0DAA10E11AAF3AA902BE6511DBEA61F_159(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D823ECB532404CAC8E6628819EAB525132A3C4CC3_160(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D66175F96A3A979C0479D04059A97B4F0D1545770_161(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DA5E465DCCD6CF445B12C62AFC303C659C6E78B26_162(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DB14CD063151CEC59EEB0F8E2557A7CF6B9D833BA_163(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D62EBB675E18E46D952C37A995181D23A18F84CD4_164(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D5F214E20357C7CC02D9BB842C0626F580CA6200E_165(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D8834238153C27D69F7A59F6EAE1405F77FE45409_166(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D149F522CB3B9C44EB981E8BC2540D606F1CDCD87_167(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D231A0B5CEF9DCFCB9D64B36C1F48EFABC84C7F49_168(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_169(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D8CFA957D76B6E190580D284C12F31AA6E3E2D41C_170(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (U24ArrayTypeU3D16_t3253128244)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t3253128244 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (U24ArrayTypeU3D208_t3449392908)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D208_t3449392908 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (U24ArrayTypeU3D24_t2467506693)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t2467506693 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (U24ArrayTypeU3D64_t498138225)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D64_t498138225 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (U24ArrayTypeU3D56_t1283759776)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D56_t1283759776 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (U24ArrayTypeU3D32_t3651253610)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D32_t3651253610 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
