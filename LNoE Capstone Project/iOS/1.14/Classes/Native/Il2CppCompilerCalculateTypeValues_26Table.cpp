﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.List`1<Zombie>
struct List_1_t4068628460;
// Zombies/ZombiesOnBoard
struct ZombiesOnBoard_t3526012930;
// Zombies/ZombiesAreKillable
struct ZombiesAreKillable_t2501657832;
// Zombies/HeroZombieOnBoard
struct HeroZombieOnBoard_t1308641688;
// System.Func`2<Zombie,System.Int32>
struct Func_2_t3217696673;
// System.Func`2<Zombie,System.Boolean>
struct Func_2_t364038885;
// System.String
struct String_t;
// Mission/ObjectiveTypes
struct ObjectiveTypes_t2716872159;
// Mission/SpecialRulesTypes
struct SpecialRulesTypes_t2275317327;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<Mission/ObjectiveTypes>
struct List_1_t4188946901;
// System.Collections.Generic.List`1<Mission/ScenarioSearchItemTypes>
struct List_1_t3444701783;
// System.Collections.Generic.List`1<Mission/SpecialRulesTypes>
struct List_1_t3747392069;
// Mission
struct Mission_t4233471175;
// System.Collections.Generic.List`1<Mission>
struct List_1_t1410578621;
// System.Predicate`1<Mission>
struct Predicate_1_t763798003;
// System.Double[]
struct DoubleU5BU5D_t3413330114;
// System.Collections.Generic.List`1<RadialBasisFunctionNetwork/RBFNode>
struct List_1_t384684280;
// System.Collections.Generic.List`1<Building>
struct List_1_t2883330737;
// System.Func`2<Building,System.Boolean>
struct Func_2_t137751404;
// System.Func`2<Building,System.Int32>
struct Func_2_t2991409192;
// System.Collections.Generic.List`1<MapTile>
struct List_1_t680461681;
// System.Comparison`1<Building>
struct Comparison_1_t1186187174;
// System.Func`2<MapTile,System.Int32>
struct Func_2_t38978152;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// ZombieTurn
struct ZombieTurn_t3562480803;
// Building
struct Building_t1411255995;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// SpeechRecognizerManager
struct SpeechRecognizerManager_t652028364;
// LoadScene
struct LoadScene_t3470671713;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3521020193;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.WebCamTexture
struct WebCamTexture_t1514609158;
// GameLog
struct GameLog_t2697247111;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t32045322;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4131667876;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t911335936;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ZOMBIES_T3905046179_H
#define ZOMBIES_T3905046179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zombies
struct  Zombies_t3905046179  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zombie> Zombies::zombieTypes
	List_1_t4068628460 * ___zombieTypes_0;
	// System.Int32 Zombies::zombiesKilledCount
	int32_t ___zombiesKilledCount_1;
	// System.Boolean Zombies::zombieTurn
	bool ___zombieTurn_2;
	// System.Boolean Zombies::zombiesCanCanBeKilled
	bool ___zombiesCanCanBeKilled_3;
	// System.Int32 Zombies::tempFightDice
	int32_t ___tempFightDice_4;
	// System.Int32 Zombies::bonusFightDice
	int32_t ___bonusFightDice_5;
	// System.Int32 Zombies::extraRoundDice
	int32_t ___extraRoundDice_6;
	// System.Boolean Zombies::multipleFightCards
	bool ___multipleFightCards_7;
	// Zombies/ZombiesOnBoard Zombies::ZombiesOnBoardChanged
	ZombiesOnBoard_t3526012930 * ___ZombiesOnBoardChanged_8;
	// Zombies/ZombiesAreKillable Zombies::ZombiesAreKillableChanged
	ZombiesAreKillable_t2501657832 * ___ZombiesAreKillableChanged_9;
	// Zombies/HeroZombieOnBoard Zombies::HeroZombieOnBoardChanged
	HeroZombieOnBoard_t1308641688 * ___HeroZombieOnBoardChanged_10;

public:
	inline static int32_t get_offset_of_zombieTypes_0() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___zombieTypes_0)); }
	inline List_1_t4068628460 * get_zombieTypes_0() const { return ___zombieTypes_0; }
	inline List_1_t4068628460 ** get_address_of_zombieTypes_0() { return &___zombieTypes_0; }
	inline void set_zombieTypes_0(List_1_t4068628460 * value)
	{
		___zombieTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___zombieTypes_0), value);
	}

	inline static int32_t get_offset_of_zombiesKilledCount_1() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___zombiesKilledCount_1)); }
	inline int32_t get_zombiesKilledCount_1() const { return ___zombiesKilledCount_1; }
	inline int32_t* get_address_of_zombiesKilledCount_1() { return &___zombiesKilledCount_1; }
	inline void set_zombiesKilledCount_1(int32_t value)
	{
		___zombiesKilledCount_1 = value;
	}

	inline static int32_t get_offset_of_zombieTurn_2() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___zombieTurn_2)); }
	inline bool get_zombieTurn_2() const { return ___zombieTurn_2; }
	inline bool* get_address_of_zombieTurn_2() { return &___zombieTurn_2; }
	inline void set_zombieTurn_2(bool value)
	{
		___zombieTurn_2 = value;
	}

	inline static int32_t get_offset_of_zombiesCanCanBeKilled_3() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___zombiesCanCanBeKilled_3)); }
	inline bool get_zombiesCanCanBeKilled_3() const { return ___zombiesCanCanBeKilled_3; }
	inline bool* get_address_of_zombiesCanCanBeKilled_3() { return &___zombiesCanCanBeKilled_3; }
	inline void set_zombiesCanCanBeKilled_3(bool value)
	{
		___zombiesCanCanBeKilled_3 = value;
	}

	inline static int32_t get_offset_of_tempFightDice_4() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___tempFightDice_4)); }
	inline int32_t get_tempFightDice_4() const { return ___tempFightDice_4; }
	inline int32_t* get_address_of_tempFightDice_4() { return &___tempFightDice_4; }
	inline void set_tempFightDice_4(int32_t value)
	{
		___tempFightDice_4 = value;
	}

	inline static int32_t get_offset_of_bonusFightDice_5() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___bonusFightDice_5)); }
	inline int32_t get_bonusFightDice_5() const { return ___bonusFightDice_5; }
	inline int32_t* get_address_of_bonusFightDice_5() { return &___bonusFightDice_5; }
	inline void set_bonusFightDice_5(int32_t value)
	{
		___bonusFightDice_5 = value;
	}

	inline static int32_t get_offset_of_extraRoundDice_6() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___extraRoundDice_6)); }
	inline int32_t get_extraRoundDice_6() const { return ___extraRoundDice_6; }
	inline int32_t* get_address_of_extraRoundDice_6() { return &___extraRoundDice_6; }
	inline void set_extraRoundDice_6(int32_t value)
	{
		___extraRoundDice_6 = value;
	}

	inline static int32_t get_offset_of_multipleFightCards_7() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___multipleFightCards_7)); }
	inline bool get_multipleFightCards_7() const { return ___multipleFightCards_7; }
	inline bool* get_address_of_multipleFightCards_7() { return &___multipleFightCards_7; }
	inline void set_multipleFightCards_7(bool value)
	{
		___multipleFightCards_7 = value;
	}

	inline static int32_t get_offset_of_ZombiesOnBoardChanged_8() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___ZombiesOnBoardChanged_8)); }
	inline ZombiesOnBoard_t3526012930 * get_ZombiesOnBoardChanged_8() const { return ___ZombiesOnBoardChanged_8; }
	inline ZombiesOnBoard_t3526012930 ** get_address_of_ZombiesOnBoardChanged_8() { return &___ZombiesOnBoardChanged_8; }
	inline void set_ZombiesOnBoardChanged_8(ZombiesOnBoard_t3526012930 * value)
	{
		___ZombiesOnBoardChanged_8 = value;
		Il2CppCodeGenWriteBarrier((&___ZombiesOnBoardChanged_8), value);
	}

	inline static int32_t get_offset_of_ZombiesAreKillableChanged_9() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___ZombiesAreKillableChanged_9)); }
	inline ZombiesAreKillable_t2501657832 * get_ZombiesAreKillableChanged_9() const { return ___ZombiesAreKillableChanged_9; }
	inline ZombiesAreKillable_t2501657832 ** get_address_of_ZombiesAreKillableChanged_9() { return &___ZombiesAreKillableChanged_9; }
	inline void set_ZombiesAreKillableChanged_9(ZombiesAreKillable_t2501657832 * value)
	{
		___ZombiesAreKillableChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___ZombiesAreKillableChanged_9), value);
	}

	inline static int32_t get_offset_of_HeroZombieOnBoardChanged_10() { return static_cast<int32_t>(offsetof(Zombies_t3905046179, ___HeroZombieOnBoardChanged_10)); }
	inline HeroZombieOnBoard_t1308641688 * get_HeroZombieOnBoardChanged_10() const { return ___HeroZombieOnBoardChanged_10; }
	inline HeroZombieOnBoard_t1308641688 ** get_address_of_HeroZombieOnBoardChanged_10() { return &___HeroZombieOnBoardChanged_10; }
	inline void set_HeroZombieOnBoardChanged_10(HeroZombieOnBoard_t1308641688 * value)
	{
		___HeroZombieOnBoardChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___HeroZombieOnBoardChanged_10), value);
	}
};

struct Zombies_t3905046179_StaticFields
{
public:
	// System.Func`2<Zombie,System.Int32> Zombies::<>f__am$cache0
	Func_2_t3217696673 * ___U3CU3Ef__amU24cache0_11;
	// System.Func`2<Zombie,System.Int32> Zombies::<>f__am$cache1
	Func_2_t3217696673 * ___U3CU3Ef__amU24cache1_12;
	// System.Func`2<Zombie,System.Boolean> Zombies::<>f__am$cache2
	Func_2_t364038885 * ___U3CU3Ef__amU24cache2_13;
	// System.Func`2<Zombie,System.Int32> Zombies::<>f__am$cache3
	Func_2_t3217696673 * ___U3CU3Ef__amU24cache3_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(Zombies_t3905046179_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Func_2_t3217696673 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Func_2_t3217696673 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Func_2_t3217696673 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_12() { return static_cast<int32_t>(offsetof(Zombies_t3905046179_StaticFields, ___U3CU3Ef__amU24cache1_12)); }
	inline Func_2_t3217696673 * get_U3CU3Ef__amU24cache1_12() const { return ___U3CU3Ef__amU24cache1_12; }
	inline Func_2_t3217696673 ** get_address_of_U3CU3Ef__amU24cache1_12() { return &___U3CU3Ef__amU24cache1_12; }
	inline void set_U3CU3Ef__amU24cache1_12(Func_2_t3217696673 * value)
	{
		___U3CU3Ef__amU24cache1_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_13() { return static_cast<int32_t>(offsetof(Zombies_t3905046179_StaticFields, ___U3CU3Ef__amU24cache2_13)); }
	inline Func_2_t364038885 * get_U3CU3Ef__amU24cache2_13() const { return ___U3CU3Ef__amU24cache2_13; }
	inline Func_2_t364038885 ** get_address_of_U3CU3Ef__amU24cache2_13() { return &___U3CU3Ef__amU24cache2_13; }
	inline void set_U3CU3Ef__amU24cache2_13(Func_2_t364038885 * value)
	{
		___U3CU3Ef__amU24cache2_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_14() { return static_cast<int32_t>(offsetof(Zombies_t3905046179_StaticFields, ___U3CU3Ef__amU24cache3_14)); }
	inline Func_2_t3217696673 * get_U3CU3Ef__amU24cache3_14() const { return ___U3CU3Ef__amU24cache3_14; }
	inline Func_2_t3217696673 ** get_address_of_U3CU3Ef__amU24cache3_14() { return &___U3CU3Ef__amU24cache3_14; }
	inline void set_U3CU3Ef__amU24cache3_14(Func_2_t3217696673 * value)
	{
		___U3CU3Ef__amU24cache3_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIES_T3905046179_H
#ifndef NORMALIZATION_T2844905348_H
#define NORMALIZATION_T2844905348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Normalization
struct  Normalization_t2844905348  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALIZATION_T2844905348_H
#ifndef SPECIALRULESTYPES_T2275317327_H
#define SPECIALRULESTYPES_T2275317327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mission/SpecialRulesTypes
struct  SpecialRulesTypes_t2275317327  : public RuntimeObject
{
public:
	// System.String Mission/SpecialRulesTypes::Name
	String_t* ___Name_0;
	// System.String Mission/SpecialRulesTypes::SetupText
	String_t* ___SetupText_1;
	// System.String Mission/SpecialRulesTypes::PlayerPrefKey
	String_t* ___PlayerPrefKey_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(SpecialRulesTypes_t2275317327, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_SetupText_1() { return static_cast<int32_t>(offsetof(SpecialRulesTypes_t2275317327, ___SetupText_1)); }
	inline String_t* get_SetupText_1() const { return ___SetupText_1; }
	inline String_t** get_address_of_SetupText_1() { return &___SetupText_1; }
	inline void set_SetupText_1(String_t* value)
	{
		___SetupText_1 = value;
		Il2CppCodeGenWriteBarrier((&___SetupText_1), value);
	}

	inline static int32_t get_offset_of_PlayerPrefKey_2() { return static_cast<int32_t>(offsetof(SpecialRulesTypes_t2275317327, ___PlayerPrefKey_2)); }
	inline String_t* get_PlayerPrefKey_2() const { return ___PlayerPrefKey_2; }
	inline String_t** get_address_of_PlayerPrefKey_2() { return &___PlayerPrefKey_2; }
	inline void set_PlayerPrefKey_2(String_t* value)
	{
		___PlayerPrefKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___PlayerPrefKey_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALRULESTYPES_T2275317327_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef OBJECTIVETYPES_T2716872159_H
#define OBJECTIVETYPES_T2716872159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mission/ObjectiveTypes
struct  ObjectiveTypes_t2716872159  : public RuntimeObject
{
public:
	// System.String Mission/ObjectiveTypes::Name
	String_t* ___Name_0;
	// System.Int32 Mission/ObjectiveTypes::Value
	int32_t ___Value_1;
	// System.Boolean Mission/ObjectiveTypes::Button
	bool ___Button_2;
	// System.String Mission/ObjectiveTypes::Audio
	String_t* ___Audio_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ObjectiveTypes_t2716872159, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(ObjectiveTypes_t2716872159, ___Value_1)); }
	inline int32_t get_Value_1() const { return ___Value_1; }
	inline int32_t* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(int32_t value)
	{
		___Value_1 = value;
	}

	inline static int32_t get_offset_of_Button_2() { return static_cast<int32_t>(offsetof(ObjectiveTypes_t2716872159, ___Button_2)); }
	inline bool get_Button_2() const { return ___Button_2; }
	inline bool* get_address_of_Button_2() { return &___Button_2; }
	inline void set_Button_2(bool value)
	{
		___Button_2 = value;
	}

	inline static int32_t get_offset_of_Audio_3() { return static_cast<int32_t>(offsetof(ObjectiveTypes_t2716872159, ___Audio_3)); }
	inline String_t* get_Audio_3() const { return ___Audio_3; }
	inline String_t** get_address_of_Audio_3() { return &___Audio_3; }
	inline void set_Audio_3(String_t* value)
	{
		___Audio_3 = value;
		Il2CppCodeGenWriteBarrier((&___Audio_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVETYPES_T2716872159_H
#ifndef MISSION_T4233471175_H
#define MISSION_T4233471175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mission
struct  Mission_t4233471175  : public RuntimeObject
{
public:
	// Mission/ObjectiveTypes Mission::BurnDownManor
	ObjectiveTypes_t2716872159 * ___BurnDownManor_0;
	// Mission/ObjectiveTypes Mission::Destroy3ZombiePits
	ObjectiveTypes_t2716872159 * ___Destroy3ZombiePits_1;
	// Mission/ObjectiveTypes Mission::EscapeTruckButton
	ObjectiveTypes_t2716872159 * ___EscapeTruckButton_2;
	// Mission/ObjectiveTypes Mission::Kill7PlagueCarriersButton
	ObjectiveTypes_t2716872159 * ___Kill7PlagueCarriersButton_3;
	// Mission/ObjectiveTypes Mission::Kill15Zombies
	ObjectiveTypes_t2716872159 * ___Kill15Zombies_4;
	// Mission/ObjectiveTypes Mission::Save4TownfolkButton
	ObjectiveTypes_t2716872159 * ___Save4TownfolkButton_5;
	// Mission/ObjectiveTypes Mission::Kill2Heroes
	ObjectiveTypes_t2716872159 * ___Kill2Heroes_7;
	// Mission/ObjectiveTypes Mission::Kill4Heroes
	ObjectiveTypes_t2716872159 * ___Kill4Heroes_8;
	// Mission/ObjectiveTypes Mission::ZombiesInHouse9
	ObjectiveTypes_t2716872159 * ___ZombiesInHouse9_9;
	// Mission/ObjectiveTypes Mission::HuntForSurvivors
	ObjectiveTypes_t2716872159 * ___HuntForSurvivors_10;
	// Mission/ObjectiveTypes Mission::FindZombieSkull
	ObjectiveTypes_t2716872159 * ___FindZombieSkull_11;
	// Mission/ObjectiveTypes Mission::GetSkullToDoctor
	ObjectiveTypes_t2716872159 * ___GetSkullToDoctor_12;
	// Mission/ObjectiveTypes Mission::KillDoctorZombie
	ObjectiveTypes_t2716872159 * ___KillDoctorZombie_13;
	// System.String Mission::Name
	String_t* ___Name_23;
	// System.String Mission::Text
	String_t* ___Text_24;
	// System.Collections.Generic.List`1<System.String> Mission::StartNarrativeText
	List_1_t3319525431 * ___StartNarrativeText_25;
	// System.Collections.Generic.List`1<System.String> Mission::NarrativeText
	List_1_t3319525431 * ___NarrativeText_26;
	// System.Collections.Generic.List`1<System.String> Mission::EndNarrativeText
	List_1_t3319525431 * ___EndNarrativeText_27;
	// System.Int32 Mission::Rounds
	int32_t ___Rounds_28;
	// System.Collections.Generic.List`1<Mission/ObjectiveTypes> Mission::HeroObjective
	List_1_t4188946901 * ___HeroObjective_29;
	// System.Collections.Generic.List`1<Mission/ObjectiveTypes> Mission::ZombieObjective
	List_1_t4188946901 * ___ZombieObjective_30;
	// System.Collections.Generic.List`1<Mission/ScenarioSearchItemTypes> Mission::ScenarioSearchItems
	List_1_t3444701783 * ___ScenarioSearchItems_31;
	// System.Collections.Generic.List`1<Mission/SpecialRulesTypes> Mission::SpecialRules
	List_1_t3747392069 * ___SpecialRules_32;
	// System.String Mission::MissionMovementTarget
	String_t* ___MissionMovementTarget_33;
	// System.String Mission::CenterTile
	String_t* ___CenterTile_34;
	// System.Boolean Mission::AllHeroStartLocationChanged
	bool ___AllHeroStartLocationChanged_35;
	// System.Boolean Mission::CenterHeroStartLocationChanged
	bool ___CenterHeroStartLocationChanged_36;
	// System.String Mission::HeroStartLocation
	String_t* ___HeroStartLocation_37;
	// System.String Mission::AudioWin
	String_t* ___AudioWin_38;
	// System.String Mission::AudioLose
	String_t* ___AudioLose_39;

public:
	inline static int32_t get_offset_of_BurnDownManor_0() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___BurnDownManor_0)); }
	inline ObjectiveTypes_t2716872159 * get_BurnDownManor_0() const { return ___BurnDownManor_0; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_BurnDownManor_0() { return &___BurnDownManor_0; }
	inline void set_BurnDownManor_0(ObjectiveTypes_t2716872159 * value)
	{
		___BurnDownManor_0 = value;
		Il2CppCodeGenWriteBarrier((&___BurnDownManor_0), value);
	}

	inline static int32_t get_offset_of_Destroy3ZombiePits_1() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Destroy3ZombiePits_1)); }
	inline ObjectiveTypes_t2716872159 * get_Destroy3ZombiePits_1() const { return ___Destroy3ZombiePits_1; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Destroy3ZombiePits_1() { return &___Destroy3ZombiePits_1; }
	inline void set_Destroy3ZombiePits_1(ObjectiveTypes_t2716872159 * value)
	{
		___Destroy3ZombiePits_1 = value;
		Il2CppCodeGenWriteBarrier((&___Destroy3ZombiePits_1), value);
	}

	inline static int32_t get_offset_of_EscapeTruckButton_2() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___EscapeTruckButton_2)); }
	inline ObjectiveTypes_t2716872159 * get_EscapeTruckButton_2() const { return ___EscapeTruckButton_2; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_EscapeTruckButton_2() { return &___EscapeTruckButton_2; }
	inline void set_EscapeTruckButton_2(ObjectiveTypes_t2716872159 * value)
	{
		___EscapeTruckButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___EscapeTruckButton_2), value);
	}

	inline static int32_t get_offset_of_Kill7PlagueCarriersButton_3() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Kill7PlagueCarriersButton_3)); }
	inline ObjectiveTypes_t2716872159 * get_Kill7PlagueCarriersButton_3() const { return ___Kill7PlagueCarriersButton_3; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Kill7PlagueCarriersButton_3() { return &___Kill7PlagueCarriersButton_3; }
	inline void set_Kill7PlagueCarriersButton_3(ObjectiveTypes_t2716872159 * value)
	{
		___Kill7PlagueCarriersButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___Kill7PlagueCarriersButton_3), value);
	}

	inline static int32_t get_offset_of_Kill15Zombies_4() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Kill15Zombies_4)); }
	inline ObjectiveTypes_t2716872159 * get_Kill15Zombies_4() const { return ___Kill15Zombies_4; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Kill15Zombies_4() { return &___Kill15Zombies_4; }
	inline void set_Kill15Zombies_4(ObjectiveTypes_t2716872159 * value)
	{
		___Kill15Zombies_4 = value;
		Il2CppCodeGenWriteBarrier((&___Kill15Zombies_4), value);
	}

	inline static int32_t get_offset_of_Save4TownfolkButton_5() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Save4TownfolkButton_5)); }
	inline ObjectiveTypes_t2716872159 * get_Save4TownfolkButton_5() const { return ___Save4TownfolkButton_5; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Save4TownfolkButton_5() { return &___Save4TownfolkButton_5; }
	inline void set_Save4TownfolkButton_5(ObjectiveTypes_t2716872159 * value)
	{
		___Save4TownfolkButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___Save4TownfolkButton_5), value);
	}

	inline static int32_t get_offset_of_Kill2Heroes_7() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Kill2Heroes_7)); }
	inline ObjectiveTypes_t2716872159 * get_Kill2Heroes_7() const { return ___Kill2Heroes_7; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Kill2Heroes_7() { return &___Kill2Heroes_7; }
	inline void set_Kill2Heroes_7(ObjectiveTypes_t2716872159 * value)
	{
		___Kill2Heroes_7 = value;
		Il2CppCodeGenWriteBarrier((&___Kill2Heroes_7), value);
	}

	inline static int32_t get_offset_of_Kill4Heroes_8() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Kill4Heroes_8)); }
	inline ObjectiveTypes_t2716872159 * get_Kill4Heroes_8() const { return ___Kill4Heroes_8; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Kill4Heroes_8() { return &___Kill4Heroes_8; }
	inline void set_Kill4Heroes_8(ObjectiveTypes_t2716872159 * value)
	{
		___Kill4Heroes_8 = value;
		Il2CppCodeGenWriteBarrier((&___Kill4Heroes_8), value);
	}

	inline static int32_t get_offset_of_ZombiesInHouse9_9() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___ZombiesInHouse9_9)); }
	inline ObjectiveTypes_t2716872159 * get_ZombiesInHouse9_9() const { return ___ZombiesInHouse9_9; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_ZombiesInHouse9_9() { return &___ZombiesInHouse9_9; }
	inline void set_ZombiesInHouse9_9(ObjectiveTypes_t2716872159 * value)
	{
		___ZombiesInHouse9_9 = value;
		Il2CppCodeGenWriteBarrier((&___ZombiesInHouse9_9), value);
	}

	inline static int32_t get_offset_of_HuntForSurvivors_10() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___HuntForSurvivors_10)); }
	inline ObjectiveTypes_t2716872159 * get_HuntForSurvivors_10() const { return ___HuntForSurvivors_10; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_HuntForSurvivors_10() { return &___HuntForSurvivors_10; }
	inline void set_HuntForSurvivors_10(ObjectiveTypes_t2716872159 * value)
	{
		___HuntForSurvivors_10 = value;
		Il2CppCodeGenWriteBarrier((&___HuntForSurvivors_10), value);
	}

	inline static int32_t get_offset_of_FindZombieSkull_11() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___FindZombieSkull_11)); }
	inline ObjectiveTypes_t2716872159 * get_FindZombieSkull_11() const { return ___FindZombieSkull_11; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_FindZombieSkull_11() { return &___FindZombieSkull_11; }
	inline void set_FindZombieSkull_11(ObjectiveTypes_t2716872159 * value)
	{
		___FindZombieSkull_11 = value;
		Il2CppCodeGenWriteBarrier((&___FindZombieSkull_11), value);
	}

	inline static int32_t get_offset_of_GetSkullToDoctor_12() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___GetSkullToDoctor_12)); }
	inline ObjectiveTypes_t2716872159 * get_GetSkullToDoctor_12() const { return ___GetSkullToDoctor_12; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_GetSkullToDoctor_12() { return &___GetSkullToDoctor_12; }
	inline void set_GetSkullToDoctor_12(ObjectiveTypes_t2716872159 * value)
	{
		___GetSkullToDoctor_12 = value;
		Il2CppCodeGenWriteBarrier((&___GetSkullToDoctor_12), value);
	}

	inline static int32_t get_offset_of_KillDoctorZombie_13() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___KillDoctorZombie_13)); }
	inline ObjectiveTypes_t2716872159 * get_KillDoctorZombie_13() const { return ___KillDoctorZombie_13; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_KillDoctorZombie_13() { return &___KillDoctorZombie_13; }
	inline void set_KillDoctorZombie_13(ObjectiveTypes_t2716872159 * value)
	{
		___KillDoctorZombie_13 = value;
		Il2CppCodeGenWriteBarrier((&___KillDoctorZombie_13), value);
	}

	inline static int32_t get_offset_of_Name_23() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Name_23)); }
	inline String_t* get_Name_23() const { return ___Name_23; }
	inline String_t** get_address_of_Name_23() { return &___Name_23; }
	inline void set_Name_23(String_t* value)
	{
		___Name_23 = value;
		Il2CppCodeGenWriteBarrier((&___Name_23), value);
	}

	inline static int32_t get_offset_of_Text_24() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Text_24)); }
	inline String_t* get_Text_24() const { return ___Text_24; }
	inline String_t** get_address_of_Text_24() { return &___Text_24; }
	inline void set_Text_24(String_t* value)
	{
		___Text_24 = value;
		Il2CppCodeGenWriteBarrier((&___Text_24), value);
	}

	inline static int32_t get_offset_of_StartNarrativeText_25() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___StartNarrativeText_25)); }
	inline List_1_t3319525431 * get_StartNarrativeText_25() const { return ___StartNarrativeText_25; }
	inline List_1_t3319525431 ** get_address_of_StartNarrativeText_25() { return &___StartNarrativeText_25; }
	inline void set_StartNarrativeText_25(List_1_t3319525431 * value)
	{
		___StartNarrativeText_25 = value;
		Il2CppCodeGenWriteBarrier((&___StartNarrativeText_25), value);
	}

	inline static int32_t get_offset_of_NarrativeText_26() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___NarrativeText_26)); }
	inline List_1_t3319525431 * get_NarrativeText_26() const { return ___NarrativeText_26; }
	inline List_1_t3319525431 ** get_address_of_NarrativeText_26() { return &___NarrativeText_26; }
	inline void set_NarrativeText_26(List_1_t3319525431 * value)
	{
		___NarrativeText_26 = value;
		Il2CppCodeGenWriteBarrier((&___NarrativeText_26), value);
	}

	inline static int32_t get_offset_of_EndNarrativeText_27() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___EndNarrativeText_27)); }
	inline List_1_t3319525431 * get_EndNarrativeText_27() const { return ___EndNarrativeText_27; }
	inline List_1_t3319525431 ** get_address_of_EndNarrativeText_27() { return &___EndNarrativeText_27; }
	inline void set_EndNarrativeText_27(List_1_t3319525431 * value)
	{
		___EndNarrativeText_27 = value;
		Il2CppCodeGenWriteBarrier((&___EndNarrativeText_27), value);
	}

	inline static int32_t get_offset_of_Rounds_28() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___Rounds_28)); }
	inline int32_t get_Rounds_28() const { return ___Rounds_28; }
	inline int32_t* get_address_of_Rounds_28() { return &___Rounds_28; }
	inline void set_Rounds_28(int32_t value)
	{
		___Rounds_28 = value;
	}

	inline static int32_t get_offset_of_HeroObjective_29() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___HeroObjective_29)); }
	inline List_1_t4188946901 * get_HeroObjective_29() const { return ___HeroObjective_29; }
	inline List_1_t4188946901 ** get_address_of_HeroObjective_29() { return &___HeroObjective_29; }
	inline void set_HeroObjective_29(List_1_t4188946901 * value)
	{
		___HeroObjective_29 = value;
		Il2CppCodeGenWriteBarrier((&___HeroObjective_29), value);
	}

	inline static int32_t get_offset_of_ZombieObjective_30() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___ZombieObjective_30)); }
	inline List_1_t4188946901 * get_ZombieObjective_30() const { return ___ZombieObjective_30; }
	inline List_1_t4188946901 ** get_address_of_ZombieObjective_30() { return &___ZombieObjective_30; }
	inline void set_ZombieObjective_30(List_1_t4188946901 * value)
	{
		___ZombieObjective_30 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieObjective_30), value);
	}

	inline static int32_t get_offset_of_ScenarioSearchItems_31() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___ScenarioSearchItems_31)); }
	inline List_1_t3444701783 * get_ScenarioSearchItems_31() const { return ___ScenarioSearchItems_31; }
	inline List_1_t3444701783 ** get_address_of_ScenarioSearchItems_31() { return &___ScenarioSearchItems_31; }
	inline void set_ScenarioSearchItems_31(List_1_t3444701783 * value)
	{
		___ScenarioSearchItems_31 = value;
		Il2CppCodeGenWriteBarrier((&___ScenarioSearchItems_31), value);
	}

	inline static int32_t get_offset_of_SpecialRules_32() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___SpecialRules_32)); }
	inline List_1_t3747392069 * get_SpecialRules_32() const { return ___SpecialRules_32; }
	inline List_1_t3747392069 ** get_address_of_SpecialRules_32() { return &___SpecialRules_32; }
	inline void set_SpecialRules_32(List_1_t3747392069 * value)
	{
		___SpecialRules_32 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialRules_32), value);
	}

	inline static int32_t get_offset_of_MissionMovementTarget_33() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___MissionMovementTarget_33)); }
	inline String_t* get_MissionMovementTarget_33() const { return ___MissionMovementTarget_33; }
	inline String_t** get_address_of_MissionMovementTarget_33() { return &___MissionMovementTarget_33; }
	inline void set_MissionMovementTarget_33(String_t* value)
	{
		___MissionMovementTarget_33 = value;
		Il2CppCodeGenWriteBarrier((&___MissionMovementTarget_33), value);
	}

	inline static int32_t get_offset_of_CenterTile_34() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___CenterTile_34)); }
	inline String_t* get_CenterTile_34() const { return ___CenterTile_34; }
	inline String_t** get_address_of_CenterTile_34() { return &___CenterTile_34; }
	inline void set_CenterTile_34(String_t* value)
	{
		___CenterTile_34 = value;
		Il2CppCodeGenWriteBarrier((&___CenterTile_34), value);
	}

	inline static int32_t get_offset_of_AllHeroStartLocationChanged_35() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___AllHeroStartLocationChanged_35)); }
	inline bool get_AllHeroStartLocationChanged_35() const { return ___AllHeroStartLocationChanged_35; }
	inline bool* get_address_of_AllHeroStartLocationChanged_35() { return &___AllHeroStartLocationChanged_35; }
	inline void set_AllHeroStartLocationChanged_35(bool value)
	{
		___AllHeroStartLocationChanged_35 = value;
	}

	inline static int32_t get_offset_of_CenterHeroStartLocationChanged_36() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___CenterHeroStartLocationChanged_36)); }
	inline bool get_CenterHeroStartLocationChanged_36() const { return ___CenterHeroStartLocationChanged_36; }
	inline bool* get_address_of_CenterHeroStartLocationChanged_36() { return &___CenterHeroStartLocationChanged_36; }
	inline void set_CenterHeroStartLocationChanged_36(bool value)
	{
		___CenterHeroStartLocationChanged_36 = value;
	}

	inline static int32_t get_offset_of_HeroStartLocation_37() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___HeroStartLocation_37)); }
	inline String_t* get_HeroStartLocation_37() const { return ___HeroStartLocation_37; }
	inline String_t** get_address_of_HeroStartLocation_37() { return &___HeroStartLocation_37; }
	inline void set_HeroStartLocation_37(String_t* value)
	{
		___HeroStartLocation_37 = value;
		Il2CppCodeGenWriteBarrier((&___HeroStartLocation_37), value);
	}

	inline static int32_t get_offset_of_AudioWin_38() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___AudioWin_38)); }
	inline String_t* get_AudioWin_38() const { return ___AudioWin_38; }
	inline String_t** get_address_of_AudioWin_38() { return &___AudioWin_38; }
	inline void set_AudioWin_38(String_t* value)
	{
		___AudioWin_38 = value;
		Il2CppCodeGenWriteBarrier((&___AudioWin_38), value);
	}

	inline static int32_t get_offset_of_AudioLose_39() { return static_cast<int32_t>(offsetof(Mission_t4233471175, ___AudioLose_39)); }
	inline String_t* get_AudioLose_39() const { return ___AudioLose_39; }
	inline String_t** get_address_of_AudioLose_39() { return &___AudioLose_39; }
	inline void set_AudioLose_39(String_t* value)
	{
		___AudioLose_39 = value;
		Il2CppCodeGenWriteBarrier((&___AudioLose_39), value);
	}
};

struct Mission_t4233471175_StaticFields
{
public:
	// Mission/ObjectiveTypes Mission::Destroy6Buildings
	ObjectiveTypes_t2716872159 * ___Destroy6Buildings_6;
	// Mission/SpecialRulesTypes Mission::FreeSearchMarkers
	SpecialRulesTypes_t2275317327 * ___FreeSearchMarkers_14;
	// Mission/SpecialRulesTypes Mission::HeroesReplenish
	SpecialRulesTypes_t2275317327 * ___HeroesReplenish_15;
	// Mission/SpecialRulesTypes Mission::HeroStartingCards1
	SpecialRulesTypes_t2275317327 * ___HeroStartingCards1_16;
	// Mission/SpecialRulesTypes Mission::HeroStartingCards2
	SpecialRulesTypes_t2275317327 * ___HeroStartingCards2_17;
	// Mission/SpecialRulesTypes Mission::PlagueCarriers
	SpecialRulesTypes_t2275317327 * ___PlagueCarriers_18;
	// Mission/SpecialRulesTypes Mission::WellStockedBuilding
	SpecialRulesTypes_t2275317327 * ___WellStockedBuilding_19;
	// Mission/SpecialRulesTypes Mission::ZombieAutoSpawn
	SpecialRulesTypes_t2275317327 * ___ZombieAutoSpawn_20;
	// Mission/SpecialRulesTypes Mission::ZombieGraveDead
	SpecialRulesTypes_t2275317327 * ___ZombieGraveDead_21;
	// Mission/SpecialRulesTypes Mission::ZombieHorde21
	SpecialRulesTypes_t2275317327 * ___ZombieHorde21_22;

public:
	inline static int32_t get_offset_of_Destroy6Buildings_6() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___Destroy6Buildings_6)); }
	inline ObjectiveTypes_t2716872159 * get_Destroy6Buildings_6() const { return ___Destroy6Buildings_6; }
	inline ObjectiveTypes_t2716872159 ** get_address_of_Destroy6Buildings_6() { return &___Destroy6Buildings_6; }
	inline void set_Destroy6Buildings_6(ObjectiveTypes_t2716872159 * value)
	{
		___Destroy6Buildings_6 = value;
		Il2CppCodeGenWriteBarrier((&___Destroy6Buildings_6), value);
	}

	inline static int32_t get_offset_of_FreeSearchMarkers_14() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___FreeSearchMarkers_14)); }
	inline SpecialRulesTypes_t2275317327 * get_FreeSearchMarkers_14() const { return ___FreeSearchMarkers_14; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_FreeSearchMarkers_14() { return &___FreeSearchMarkers_14; }
	inline void set_FreeSearchMarkers_14(SpecialRulesTypes_t2275317327 * value)
	{
		___FreeSearchMarkers_14 = value;
		Il2CppCodeGenWriteBarrier((&___FreeSearchMarkers_14), value);
	}

	inline static int32_t get_offset_of_HeroesReplenish_15() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___HeroesReplenish_15)); }
	inline SpecialRulesTypes_t2275317327 * get_HeroesReplenish_15() const { return ___HeroesReplenish_15; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_HeroesReplenish_15() { return &___HeroesReplenish_15; }
	inline void set_HeroesReplenish_15(SpecialRulesTypes_t2275317327 * value)
	{
		___HeroesReplenish_15 = value;
		Il2CppCodeGenWriteBarrier((&___HeroesReplenish_15), value);
	}

	inline static int32_t get_offset_of_HeroStartingCards1_16() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___HeroStartingCards1_16)); }
	inline SpecialRulesTypes_t2275317327 * get_HeroStartingCards1_16() const { return ___HeroStartingCards1_16; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_HeroStartingCards1_16() { return &___HeroStartingCards1_16; }
	inline void set_HeroStartingCards1_16(SpecialRulesTypes_t2275317327 * value)
	{
		___HeroStartingCards1_16 = value;
		Il2CppCodeGenWriteBarrier((&___HeroStartingCards1_16), value);
	}

	inline static int32_t get_offset_of_HeroStartingCards2_17() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___HeroStartingCards2_17)); }
	inline SpecialRulesTypes_t2275317327 * get_HeroStartingCards2_17() const { return ___HeroStartingCards2_17; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_HeroStartingCards2_17() { return &___HeroStartingCards2_17; }
	inline void set_HeroStartingCards2_17(SpecialRulesTypes_t2275317327 * value)
	{
		___HeroStartingCards2_17 = value;
		Il2CppCodeGenWriteBarrier((&___HeroStartingCards2_17), value);
	}

	inline static int32_t get_offset_of_PlagueCarriers_18() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___PlagueCarriers_18)); }
	inline SpecialRulesTypes_t2275317327 * get_PlagueCarriers_18() const { return ___PlagueCarriers_18; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_PlagueCarriers_18() { return &___PlagueCarriers_18; }
	inline void set_PlagueCarriers_18(SpecialRulesTypes_t2275317327 * value)
	{
		___PlagueCarriers_18 = value;
		Il2CppCodeGenWriteBarrier((&___PlagueCarriers_18), value);
	}

	inline static int32_t get_offset_of_WellStockedBuilding_19() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___WellStockedBuilding_19)); }
	inline SpecialRulesTypes_t2275317327 * get_WellStockedBuilding_19() const { return ___WellStockedBuilding_19; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_WellStockedBuilding_19() { return &___WellStockedBuilding_19; }
	inline void set_WellStockedBuilding_19(SpecialRulesTypes_t2275317327 * value)
	{
		___WellStockedBuilding_19 = value;
		Il2CppCodeGenWriteBarrier((&___WellStockedBuilding_19), value);
	}

	inline static int32_t get_offset_of_ZombieAutoSpawn_20() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___ZombieAutoSpawn_20)); }
	inline SpecialRulesTypes_t2275317327 * get_ZombieAutoSpawn_20() const { return ___ZombieAutoSpawn_20; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_ZombieAutoSpawn_20() { return &___ZombieAutoSpawn_20; }
	inline void set_ZombieAutoSpawn_20(SpecialRulesTypes_t2275317327 * value)
	{
		___ZombieAutoSpawn_20 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieAutoSpawn_20), value);
	}

	inline static int32_t get_offset_of_ZombieGraveDead_21() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___ZombieGraveDead_21)); }
	inline SpecialRulesTypes_t2275317327 * get_ZombieGraveDead_21() const { return ___ZombieGraveDead_21; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_ZombieGraveDead_21() { return &___ZombieGraveDead_21; }
	inline void set_ZombieGraveDead_21(SpecialRulesTypes_t2275317327 * value)
	{
		___ZombieGraveDead_21 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieGraveDead_21), value);
	}

	inline static int32_t get_offset_of_ZombieHorde21_22() { return static_cast<int32_t>(offsetof(Mission_t4233471175_StaticFields, ___ZombieHorde21_22)); }
	inline SpecialRulesTypes_t2275317327 * get_ZombieHorde21_22() const { return ___ZombieHorde21_22; }
	inline SpecialRulesTypes_t2275317327 ** get_address_of_ZombieHorde21_22() { return &___ZombieHorde21_22; }
	inline void set_ZombieHorde21_22(SpecialRulesTypes_t2275317327 * value)
	{
		___ZombieHorde21_22 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieHorde21_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSION_T4233471175_H
#ifndef MISSIONS_T3398870215_H
#define MISSIONS_T3398870215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Missions
struct  Missions_t3398870215  : public RuntimeObject
{
public:
	// Mission Missions::activeMission
	Mission_t4233471175 * ___activeMission_0;
	// System.Collections.Generic.List`1<Mission> Missions::coreMissions
	List_1_t1410578621 * ___coreMissions_1;
	// System.Collections.Generic.List`1<Mission> Missions::growingHungerMissions
	List_1_t1410578621 * ___growingHungerMissions_2;
	// System.Collections.Generic.List`1<Mission> Missions::heroPackOneMissions
	List_1_t1410578621 * ___heroPackOneMissions_3;
	// System.Collections.Generic.List`1<Mission> Missions::timberPeakMissions
	List_1_t1410578621 * ___timberPeakMissions_4;
	// System.Collections.Generic.List`1<Mission> Missions::bloodInTheForestMissions
	List_1_t1410578621 * ___bloodInTheForestMissions_5;
	// System.Collections.Generic.List`1<Mission> Missions::survivalOfTheFittestMissions
	List_1_t1410578621 * ___survivalOfTheFittestMissions_6;
	// System.Collections.Generic.List`1<Mission> Missions::graveDeadMissions
	List_1_t1410578621 * ___graveDeadMissions_7;
	// System.Collections.Generic.List`1<Mission> Missions::supplementMissions
	List_1_t1410578621 * ___supplementMissions_8;
	// System.Collections.Generic.List`1<Mission> Missions::webMissions
	List_1_t1410578621 * ___webMissions_9;
	// System.Collections.Generic.List`1<Mission> Missions::customMissions
	List_1_t1410578621 * ___customMissions_10;

public:
	inline static int32_t get_offset_of_activeMission_0() { return static_cast<int32_t>(offsetof(Missions_t3398870215, ___activeMission_0)); }
	inline Mission_t4233471175 * get_activeMission_0() const { return ___activeMission_0; }
	inline Mission_t4233471175 ** get_address_of_activeMission_0() { return &___activeMission_0; }
	inline void set_activeMission_0(Mission_t4233471175 * value)
	{
		___activeMission_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeMission_0), value);
	}

	inline static int32_t get_offset_of_coreMissions_1() { return static_cast<int32_t>(offsetof(Missions_t3398870215, ___coreMissions_1)); }
	inline List_1_t1410578621 * get_coreMissions_1() const { return ___coreMissions_1; }
	inline List_1_t1410578621 ** get_address_of_coreMissions_1() { return &___coreMissions_1; }
	inline void set_coreMissions_1(List_1_t1410578621 * value)
	{
		___coreMissions_1 = value;
		Il2CppCodeGenWriteBarrier((&___coreMissions_1), value);
	}

	inline static int32_t get_offset_of_growingHungerMissions_2() { return static_cast<int32_t>(offsetof(Missions_t3398870215, ___growingHungerMissions_2)); }
	inline List_1_t1410578621 * get_growingHungerMissions_2() const { return ___growingHungerMissions_2; }
	inline List_1_t1410578621 ** get_address_of_growingHungerMissions_2() { return &___growingHungerMissions_2; }
	inline void set_growingHungerMissions_2(List_1_t1410578621 * value)
	{
		___growingHungerMissions_2 = value;
		Il2CppCodeGenWriteBarrier((&___growingHungerMissions_2), value);
	}

	inline static int32_t get_offset_of_heroPackOneMissions_3() { return static_cast<int32_t>(offsetof(Missions_t3398870215, ___heroPackOneMissions_3)); }
	inline List_1_t1410578621 * get_heroPackOneMissions_3() const { return ___heroPackOneMissions_3; }
	inline List_1_t1410578621 ** get_address_of_heroPackOneMissions_3() { return &___heroPackOneMissions_3; }
	inline void set_heroPackOneMissions_3(List_1_t1410578621 * value)
	{
		___heroPackOneMissions_3 = value;
		Il2CppCodeGenWriteBarrier((&___heroPackOneMissions_3), value);
	}

	inline static int32_t get_offset_of_timberPeakMissions_4() { return static_cast<int32_t>(offsetof(Missions_t3398870215, ___timberPeakMissions_4)); }
	inline List_1_t1410578621 * get_timberPeakMissions_4() const { return ___timberPeakMissions_4; }
	inline List_1_t1410578621 ** get_address_of_timberPeakMissions_4() { return &___timberPeakMissions_4; }
	inline void set_timberPeakMissions_4(List_1_t1410578621 * value)
	{
		___timberPeakMissions_4 = value;
		Il2CppCodeGenWriteBarrier((&___timberPeakMissions_4), value);
	}

	inline static int32_t get_offset_of_bloodInTheForestMissions_5() { return static_cast<int32_t>(offsetof(Missions_t3398870215, ___bloodInTheForestMissions_5)); }
	inline List_1_t1410578621 * get_bloodInTheForestMissions_5() const { return ___bloodInTheForestMissions_5; }
	inline List_1_t1410578621 ** get_address_of_bloodInTheForestMissions_5() { return &___bloodInTheForestMissions_5; }
	inline void set_bloodInTheForestMissions_5(List_1_t1410578621 * value)
	{
		___bloodInTheForestMissions_5 = value;
		Il2CppCodeGenWriteBarrier((&___bloodInTheForestMissions_5), value);
	}

	inline static int32_t get_offset_of_survivalOfTheFittestMissions_6() { return static_cast<int32_t>(offsetof(Missions_t3398870215, ___survivalOfTheFittestMissions_6)); }
	inline List_1_t1410578621 * get_survivalOfTheFittestMissions_6() const { return ___survivalOfTheFittestMissions_6; }
	inline List_1_t1410578621 ** get_address_of_survivalOfTheFittestMissions_6() { return &___survivalOfTheFittestMissions_6; }
	inline void set_survivalOfTheFittestMissions_6(List_1_t1410578621 * value)
	{
		___survivalOfTheFittestMissions_6 = value;
		Il2CppCodeGenWriteBarrier((&___survivalOfTheFittestMissions_6), value);
	}

	inline static int32_t get_offset_of_graveDeadMissions_7() { return static_cast<int32_t>(offsetof(Missions_t3398870215, ___graveDeadMissions_7)); }
	inline List_1_t1410578621 * get_graveDeadMissions_7() const { return ___graveDeadMissions_7; }
	inline List_1_t1410578621 ** get_address_of_graveDeadMissions_7() { return &___graveDeadMissions_7; }
	inline void set_graveDeadMissions_7(List_1_t1410578621 * value)
	{
		___graveDeadMissions_7 = value;
		Il2CppCodeGenWriteBarrier((&___graveDeadMissions_7), value);
	}

	inline static int32_t get_offset_of_supplementMissions_8() { return static_cast<int32_t>(offsetof(Missions_t3398870215, ___supplementMissions_8)); }
	inline List_1_t1410578621 * get_supplementMissions_8() const { return ___supplementMissions_8; }
	inline List_1_t1410578621 ** get_address_of_supplementMissions_8() { return &___supplementMissions_8; }
	inline void set_supplementMissions_8(List_1_t1410578621 * value)
	{
		___supplementMissions_8 = value;
		Il2CppCodeGenWriteBarrier((&___supplementMissions_8), value);
	}

	inline static int32_t get_offset_of_webMissions_9() { return static_cast<int32_t>(offsetof(Missions_t3398870215, ___webMissions_9)); }
	inline List_1_t1410578621 * get_webMissions_9() const { return ___webMissions_9; }
	inline List_1_t1410578621 ** get_address_of_webMissions_9() { return &___webMissions_9; }
	inline void set_webMissions_9(List_1_t1410578621 * value)
	{
		___webMissions_9 = value;
		Il2CppCodeGenWriteBarrier((&___webMissions_9), value);
	}

	inline static int32_t get_offset_of_customMissions_10() { return static_cast<int32_t>(offsetof(Missions_t3398870215, ___customMissions_10)); }
	inline List_1_t1410578621 * get_customMissions_10() const { return ___customMissions_10; }
	inline List_1_t1410578621 ** get_address_of_customMissions_10() { return &___customMissions_10; }
	inline void set_customMissions_10(List_1_t1410578621 * value)
	{
		___customMissions_10 = value;
		Il2CppCodeGenWriteBarrier((&___customMissions_10), value);
	}
};

struct Missions_t3398870215_StaticFields
{
public:
	// System.Predicate`1<Mission> Missions::<>f__am$cache0
	Predicate_1_t763798003 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(Missions_t3398870215_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Predicate_1_t763798003 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Predicate_1_t763798003 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Predicate_1_t763798003 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSIONS_T3398870215_H
#ifndef U3CGETZOMBIEU3EC__ANONSTOREY1_T4035454636_H
#define U3CGETZOMBIEU3EC__ANONSTOREY1_T4035454636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zombies/<GetZombie>c__AnonStorey1
struct  U3CGetZombieU3Ec__AnonStorey1_t4035454636  : public RuntimeObject
{
public:
	// System.String Zombies/<GetZombie>c__AnonStorey1::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CGetZombieU3Ec__AnonStorey1_t4035454636, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETZOMBIEU3EC__ANONSTOREY1_T4035454636_H
#ifndef CLUSTERPOINT_T3639481456_H
#define CLUSTERPOINT_T3639481456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadialBasisFunctionNetwork/ClusterPoint
struct  ClusterPoint_t3639481456  : public RuntimeObject
{
public:
	// System.Double[] RadialBasisFunctionNetwork/ClusterPoint::Value
	DoubleU5BU5D_t3413330114* ___Value_0;
	// System.Int32 RadialBasisFunctionNetwork/ClusterPoint::AssignedCluster
	int32_t ___AssignedCluster_1;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(ClusterPoint_t3639481456, ___Value_0)); }
	inline DoubleU5BU5D_t3413330114* get_Value_0() const { return ___Value_0; }
	inline DoubleU5BU5D_t3413330114** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(DoubleU5BU5D_t3413330114* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_AssignedCluster_1() { return static_cast<int32_t>(offsetof(ClusterPoint_t3639481456, ___AssignedCluster_1)); }
	inline int32_t get_AssignedCluster_1() const { return ___AssignedCluster_1; }
	inline int32_t* get_address_of_AssignedCluster_1() { return &___AssignedCluster_1; }
	inline void set_AssignedCluster_1(int32_t value)
	{
		___AssignedCluster_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLUSTERPOINT_T3639481456_H
#ifndef CLUSTERCENTER_T787658505_H
#define CLUSTERCENTER_T787658505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadialBasisFunctionNetwork/ClusterCenter
struct  ClusterCenter_t787658505  : public RuntimeObject
{
public:
	// System.Int32 RadialBasisFunctionNetwork/ClusterCenter::ClusterId
	int32_t ___ClusterId_0;
	// System.Double[] RadialBasisFunctionNetwork/ClusterCenter::Value
	DoubleU5BU5D_t3413330114* ___Value_1;
	// System.Double RadialBasisFunctionNetwork/ClusterCenter::Width
	double ___Width_2;

public:
	inline static int32_t get_offset_of_ClusterId_0() { return static_cast<int32_t>(offsetof(ClusterCenter_t787658505, ___ClusterId_0)); }
	inline int32_t get_ClusterId_0() const { return ___ClusterId_0; }
	inline int32_t* get_address_of_ClusterId_0() { return &___ClusterId_0; }
	inline void set_ClusterId_0(int32_t value)
	{
		___ClusterId_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(ClusterCenter_t787658505, ___Value_1)); }
	inline DoubleU5BU5D_t3413330114* get_Value_1() const { return ___Value_1; }
	inline DoubleU5BU5D_t3413330114** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(DoubleU5BU5D_t3413330114* value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}

	inline static int32_t get_offset_of_Width_2() { return static_cast<int32_t>(offsetof(ClusterCenter_t787658505, ___Width_2)); }
	inline double get_Width_2() const { return ___Width_2; }
	inline double* get_address_of_Width_2() { return &___Width_2; }
	inline void set_Width_2(double value)
	{
		___Width_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLUSTERCENTER_T787658505_H
#ifndef RBFNODE_T3207576834_H
#define RBFNODE_T3207576834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadialBasisFunctionNetwork/RBFNode
struct  RBFNode_t3207576834  : public RuntimeObject
{
public:
	// System.Double[] RadialBasisFunctionNetwork/RBFNode::Centers
	DoubleU5BU5D_t3413330114* ___Centers_0;
	// System.Double[] RadialBasisFunctionNetwork/RBFNode::ToOutputWeights
	DoubleU5BU5D_t3413330114* ___ToOutputWeights_1;
	// System.Double RadialBasisFunctionNetwork/RBFNode::FunctionOut
	double ___FunctionOut_2;
	// System.Double RadialBasisFunctionNetwork/RBFNode::Width
	double ___Width_3;

public:
	inline static int32_t get_offset_of_Centers_0() { return static_cast<int32_t>(offsetof(RBFNode_t3207576834, ___Centers_0)); }
	inline DoubleU5BU5D_t3413330114* get_Centers_0() const { return ___Centers_0; }
	inline DoubleU5BU5D_t3413330114** get_address_of_Centers_0() { return &___Centers_0; }
	inline void set_Centers_0(DoubleU5BU5D_t3413330114* value)
	{
		___Centers_0 = value;
		Il2CppCodeGenWriteBarrier((&___Centers_0), value);
	}

	inline static int32_t get_offset_of_ToOutputWeights_1() { return static_cast<int32_t>(offsetof(RBFNode_t3207576834, ___ToOutputWeights_1)); }
	inline DoubleU5BU5D_t3413330114* get_ToOutputWeights_1() const { return ___ToOutputWeights_1; }
	inline DoubleU5BU5D_t3413330114** get_address_of_ToOutputWeights_1() { return &___ToOutputWeights_1; }
	inline void set_ToOutputWeights_1(DoubleU5BU5D_t3413330114* value)
	{
		___ToOutputWeights_1 = value;
		Il2CppCodeGenWriteBarrier((&___ToOutputWeights_1), value);
	}

	inline static int32_t get_offset_of_FunctionOut_2() { return static_cast<int32_t>(offsetof(RBFNode_t3207576834, ___FunctionOut_2)); }
	inline double get_FunctionOut_2() const { return ___FunctionOut_2; }
	inline double* get_address_of_FunctionOut_2() { return &___FunctionOut_2; }
	inline void set_FunctionOut_2(double value)
	{
		___FunctionOut_2 = value;
	}

	inline static int32_t get_offset_of_Width_3() { return static_cast<int32_t>(offsetof(RBFNode_t3207576834, ___Width_3)); }
	inline double get_Width_3() const { return ___Width_3; }
	inline double* get_address_of_Width_3() { return &___Width_3; }
	inline void set_Width_3(double value)
	{
		___Width_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RBFNODE_T3207576834_H
#ifndef U3CSOFTMAXU3EC__ANONSTOREY0_T2479826977_H
#define U3CSOFTMAXU3EC__ANONSTOREY0_T2479826977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadialBasisFunctionNetwork/<Softmax>c__AnonStorey0
struct  U3CSoftmaxU3Ec__AnonStorey0_t2479826977  : public RuntimeObject
{
public:
	// System.Double RadialBasisFunctionNetwork/<Softmax>c__AnonStorey0::max
	double ___max_0;

public:
	inline static int32_t get_offset_of_max_0() { return static_cast<int32_t>(offsetof(U3CSoftmaxU3Ec__AnonStorey0_t2479826977, ___max_0)); }
	inline double get_max_0() const { return ___max_0; }
	inline double* get_address_of_max_0() { return &___max_0; }
	inline void set_max_0(double value)
	{
		___max_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSOFTMAXU3EC__ANONSTOREY0_T2479826977_H
#ifndef GAUSSIANFUNCTION_T1720587675_H
#define GAUSSIANFUNCTION_T1720587675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GaussianFunction
struct  GaussianFunction_t1720587675  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANFUNCTION_T1720587675_H
#ifndef ROUNDDATA_T2030032500_H
#define ROUNDDATA_T2030032500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoundData
struct  RoundData_t2030032500  : public RuntimeObject
{
public:
	// System.String RoundData::heroes
	String_t* ___heroes_0;
	// System.Int32 RoundData::roundsLeft
	int32_t ___roundsLeft_1;
	// System.Int32 RoundData::zombiesDead
	int32_t ___zombiesDead_2;
	// System.Int32 RoundData::zombiesOnBoard
	int32_t ___zombiesOnBoard_3;
	// System.Int32 RoundData::heroesDead
	int32_t ___heroesDead_4;
	// System.Int32 RoundData::buildingsTakenOver
	int32_t ___buildingsTakenOver_5;
	// System.Int32 RoundData::buildingsLightsOut
	int32_t ___buildingsLightsOut_6;
	// System.Int32 RoundData::spawningPits
	int32_t ___spawningPits_7;

public:
	inline static int32_t get_offset_of_heroes_0() { return static_cast<int32_t>(offsetof(RoundData_t2030032500, ___heroes_0)); }
	inline String_t* get_heroes_0() const { return ___heroes_0; }
	inline String_t** get_address_of_heroes_0() { return &___heroes_0; }
	inline void set_heroes_0(String_t* value)
	{
		___heroes_0 = value;
		Il2CppCodeGenWriteBarrier((&___heroes_0), value);
	}

	inline static int32_t get_offset_of_roundsLeft_1() { return static_cast<int32_t>(offsetof(RoundData_t2030032500, ___roundsLeft_1)); }
	inline int32_t get_roundsLeft_1() const { return ___roundsLeft_1; }
	inline int32_t* get_address_of_roundsLeft_1() { return &___roundsLeft_1; }
	inline void set_roundsLeft_1(int32_t value)
	{
		___roundsLeft_1 = value;
	}

	inline static int32_t get_offset_of_zombiesDead_2() { return static_cast<int32_t>(offsetof(RoundData_t2030032500, ___zombiesDead_2)); }
	inline int32_t get_zombiesDead_2() const { return ___zombiesDead_2; }
	inline int32_t* get_address_of_zombiesDead_2() { return &___zombiesDead_2; }
	inline void set_zombiesDead_2(int32_t value)
	{
		___zombiesDead_2 = value;
	}

	inline static int32_t get_offset_of_zombiesOnBoard_3() { return static_cast<int32_t>(offsetof(RoundData_t2030032500, ___zombiesOnBoard_3)); }
	inline int32_t get_zombiesOnBoard_3() const { return ___zombiesOnBoard_3; }
	inline int32_t* get_address_of_zombiesOnBoard_3() { return &___zombiesOnBoard_3; }
	inline void set_zombiesOnBoard_3(int32_t value)
	{
		___zombiesOnBoard_3 = value;
	}

	inline static int32_t get_offset_of_heroesDead_4() { return static_cast<int32_t>(offsetof(RoundData_t2030032500, ___heroesDead_4)); }
	inline int32_t get_heroesDead_4() const { return ___heroesDead_4; }
	inline int32_t* get_address_of_heroesDead_4() { return &___heroesDead_4; }
	inline void set_heroesDead_4(int32_t value)
	{
		___heroesDead_4 = value;
	}

	inline static int32_t get_offset_of_buildingsTakenOver_5() { return static_cast<int32_t>(offsetof(RoundData_t2030032500, ___buildingsTakenOver_5)); }
	inline int32_t get_buildingsTakenOver_5() const { return ___buildingsTakenOver_5; }
	inline int32_t* get_address_of_buildingsTakenOver_5() { return &___buildingsTakenOver_5; }
	inline void set_buildingsTakenOver_5(int32_t value)
	{
		___buildingsTakenOver_5 = value;
	}

	inline static int32_t get_offset_of_buildingsLightsOut_6() { return static_cast<int32_t>(offsetof(RoundData_t2030032500, ___buildingsLightsOut_6)); }
	inline int32_t get_buildingsLightsOut_6() const { return ___buildingsLightsOut_6; }
	inline int32_t* get_address_of_buildingsLightsOut_6() { return &___buildingsLightsOut_6; }
	inline void set_buildingsLightsOut_6(int32_t value)
	{
		___buildingsLightsOut_6 = value;
	}

	inline static int32_t get_offset_of_spawningPits_7() { return static_cast<int32_t>(offsetof(RoundData_t2030032500, ___spawningPits_7)); }
	inline int32_t get_spawningPits_7() const { return ___spawningPits_7; }
	inline int32_t* get_address_of_spawningPits_7() { return &___spawningPits_7; }
	inline void set_spawningPits_7(int32_t value)
	{
		___spawningPits_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUNDDATA_T2030032500_H
#ifndef RADIALBASISFUNCTIONNETWORK_T3778331090_H
#define RADIALBASISFUNCTIONNETWORK_T3778331090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadialBasisFunctionNetwork
struct  RadialBasisFunctionNetwork_t3778331090  : public RuntimeObject
{
public:
	// System.Int32 RadialBasisFunctionNetwork::numInputs
	int32_t ___numInputs_0;
	// System.Int32 RadialBasisFunctionNetwork::numRadialBasisFunctions
	int32_t ___numRadialBasisFunctions_1;
	// System.Int32 RadialBasisFunctionNetwork::numOutputs
	int32_t ___numOutputs_2;
	// System.Double[] RadialBasisFunctionNetwork::inputs
	DoubleU5BU5D_t3413330114* ___inputs_3;
	// System.Collections.Generic.List`1<RadialBasisFunctionNetwork/RBFNode> RadialBasisFunctionNetwork::rbfNodes
	List_1_t384684280 * ___rbfNodes_4;
	// System.Double[] RadialBasisFunctionNetwork::outputBiases
	DoubleU5BU5D_t3413330114* ___outputBiases_5;
	// System.Double[] RadialBasisFunctionNetwork::outputs
	DoubleU5BU5D_t3413330114* ___outputs_6;

public:
	inline static int32_t get_offset_of_numInputs_0() { return static_cast<int32_t>(offsetof(RadialBasisFunctionNetwork_t3778331090, ___numInputs_0)); }
	inline int32_t get_numInputs_0() const { return ___numInputs_0; }
	inline int32_t* get_address_of_numInputs_0() { return &___numInputs_0; }
	inline void set_numInputs_0(int32_t value)
	{
		___numInputs_0 = value;
	}

	inline static int32_t get_offset_of_numRadialBasisFunctions_1() { return static_cast<int32_t>(offsetof(RadialBasisFunctionNetwork_t3778331090, ___numRadialBasisFunctions_1)); }
	inline int32_t get_numRadialBasisFunctions_1() const { return ___numRadialBasisFunctions_1; }
	inline int32_t* get_address_of_numRadialBasisFunctions_1() { return &___numRadialBasisFunctions_1; }
	inline void set_numRadialBasisFunctions_1(int32_t value)
	{
		___numRadialBasisFunctions_1 = value;
	}

	inline static int32_t get_offset_of_numOutputs_2() { return static_cast<int32_t>(offsetof(RadialBasisFunctionNetwork_t3778331090, ___numOutputs_2)); }
	inline int32_t get_numOutputs_2() const { return ___numOutputs_2; }
	inline int32_t* get_address_of_numOutputs_2() { return &___numOutputs_2; }
	inline void set_numOutputs_2(int32_t value)
	{
		___numOutputs_2 = value;
	}

	inline static int32_t get_offset_of_inputs_3() { return static_cast<int32_t>(offsetof(RadialBasisFunctionNetwork_t3778331090, ___inputs_3)); }
	inline DoubleU5BU5D_t3413330114* get_inputs_3() const { return ___inputs_3; }
	inline DoubleU5BU5D_t3413330114** get_address_of_inputs_3() { return &___inputs_3; }
	inline void set_inputs_3(DoubleU5BU5D_t3413330114* value)
	{
		___inputs_3 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_3), value);
	}

	inline static int32_t get_offset_of_rbfNodes_4() { return static_cast<int32_t>(offsetof(RadialBasisFunctionNetwork_t3778331090, ___rbfNodes_4)); }
	inline List_1_t384684280 * get_rbfNodes_4() const { return ___rbfNodes_4; }
	inline List_1_t384684280 ** get_address_of_rbfNodes_4() { return &___rbfNodes_4; }
	inline void set_rbfNodes_4(List_1_t384684280 * value)
	{
		___rbfNodes_4 = value;
		Il2CppCodeGenWriteBarrier((&___rbfNodes_4), value);
	}

	inline static int32_t get_offset_of_outputBiases_5() { return static_cast<int32_t>(offsetof(RadialBasisFunctionNetwork_t3778331090, ___outputBiases_5)); }
	inline DoubleU5BU5D_t3413330114* get_outputBiases_5() const { return ___outputBiases_5; }
	inline DoubleU5BU5D_t3413330114** get_address_of_outputBiases_5() { return &___outputBiases_5; }
	inline void set_outputBiases_5(DoubleU5BU5D_t3413330114* value)
	{
		___outputBiases_5 = value;
		Il2CppCodeGenWriteBarrier((&___outputBiases_5), value);
	}

	inline static int32_t get_offset_of_outputs_6() { return static_cast<int32_t>(offsetof(RadialBasisFunctionNetwork_t3778331090, ___outputs_6)); }
	inline DoubleU5BU5D_t3413330114* get_outputs_6() const { return ___outputs_6; }
	inline DoubleU5BU5D_t3413330114** get_address_of_outputs_6() { return &___outputs_6; }
	inline void set_outputs_6(DoubleU5BU5D_t3413330114* value)
	{
		___outputs_6 = value;
		Il2CppCodeGenWriteBarrier((&___outputs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALBASISFUNCTIONNETWORK_T3778331090_H
#ifndef AUDIOMIXERKEYS_T1124916762_H
#define AUDIOMIXERKEYS_T1124916762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strings/AudioMixerKeys
struct  AudioMixerKeys_t1124916762  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMIXERKEYS_T1124916762_H
#ifndef STRINGS_T3430161606_H
#define STRINGS_T3430161606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strings
struct  Strings_t3430161606  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGS_T3430161606_H
#ifndef TUTORIAL_T2275885037_H
#define TUTORIAL_T2275885037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tutorial
struct  Tutorial_t2275885037  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIAL_T2275885037_H
#ifndef MAPTILE_T3503354235_H
#define MAPTILE_T3503354235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapTile
struct  MapTile_t3503354235  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Building> MapTile::BuildingsOnTile
	List_1_t2883330737 * ___BuildingsOnTile_0;
	// System.String MapTile::Name
	String_t* ___Name_1;

public:
	inline static int32_t get_offset_of_BuildingsOnTile_0() { return static_cast<int32_t>(offsetof(MapTile_t3503354235, ___BuildingsOnTile_0)); }
	inline List_1_t2883330737 * get_BuildingsOnTile_0() const { return ___BuildingsOnTile_0; }
	inline List_1_t2883330737 ** get_address_of_BuildingsOnTile_0() { return &___BuildingsOnTile_0; }
	inline void set_BuildingsOnTile_0(List_1_t2883330737 * value)
	{
		___BuildingsOnTile_0 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingsOnTile_0), value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(MapTile_t3503354235, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}
};

struct MapTile_t3503354235_StaticFields
{
public:
	// System.Func`2<Building,System.Boolean> MapTile::<>f__am$cache0
	Func_2_t137751404 * ___U3CU3Ef__amU24cache0_2;
	// System.Func`2<Building,System.Int32> MapTile::<>f__am$cache1
	Func_2_t2991409192 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(MapTile_t3503354235_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t137751404 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t137751404 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t137751404 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(MapTile_t3503354235_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Func_2_t2991409192 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Func_2_t2991409192 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Func_2_t2991409192 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPTILE_T3503354235_H
#ifndef TILES_T1457048987_H
#define TILES_T1457048987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tiles
struct  Tiles_t1457048987  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<MapTile> Tiles::coreMapTiles
	List_1_t680461681 * ___coreMapTiles_0;
	// System.Collections.Generic.List`1<MapTile> Tiles::growingHungerMapTiles
	List_1_t680461681 * ___growingHungerMapTiles_1;
	// System.Collections.Generic.List`1<MapTile> Tiles::fourRandomTilesForGame
	List_1_t680461681 * ___fourRandomTilesForGame_2;

public:
	inline static int32_t get_offset_of_coreMapTiles_0() { return static_cast<int32_t>(offsetof(Tiles_t1457048987, ___coreMapTiles_0)); }
	inline List_1_t680461681 * get_coreMapTiles_0() const { return ___coreMapTiles_0; }
	inline List_1_t680461681 ** get_address_of_coreMapTiles_0() { return &___coreMapTiles_0; }
	inline void set_coreMapTiles_0(List_1_t680461681 * value)
	{
		___coreMapTiles_0 = value;
		Il2CppCodeGenWriteBarrier((&___coreMapTiles_0), value);
	}

	inline static int32_t get_offset_of_growingHungerMapTiles_1() { return static_cast<int32_t>(offsetof(Tiles_t1457048987, ___growingHungerMapTiles_1)); }
	inline List_1_t680461681 * get_growingHungerMapTiles_1() const { return ___growingHungerMapTiles_1; }
	inline List_1_t680461681 ** get_address_of_growingHungerMapTiles_1() { return &___growingHungerMapTiles_1; }
	inline void set_growingHungerMapTiles_1(List_1_t680461681 * value)
	{
		___growingHungerMapTiles_1 = value;
		Il2CppCodeGenWriteBarrier((&___growingHungerMapTiles_1), value);
	}

	inline static int32_t get_offset_of_fourRandomTilesForGame_2() { return static_cast<int32_t>(offsetof(Tiles_t1457048987, ___fourRandomTilesForGame_2)); }
	inline List_1_t680461681 * get_fourRandomTilesForGame_2() const { return ___fourRandomTilesForGame_2; }
	inline List_1_t680461681 ** get_address_of_fourRandomTilesForGame_2() { return &___fourRandomTilesForGame_2; }
	inline void set_fourRandomTilesForGame_2(List_1_t680461681 * value)
	{
		___fourRandomTilesForGame_2 = value;
		Il2CppCodeGenWriteBarrier((&___fourRandomTilesForGame_2), value);
	}
};

struct Tiles_t1457048987_StaticFields
{
public:
	// System.Comparison`1<Building> Tiles::<>f__mg$cache0
	Comparison_1_t1186187174 * ___U3CU3Ef__mgU24cache0_3;
	// System.Func`2<MapTile,System.Int32> Tiles::<>f__am$cache0
	Func_2_t38978152 * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<MapTile,System.Int32> Tiles::<>f__am$cache1
	Func_2_t38978152 * ___U3CU3Ef__amU24cache1_5;
	// System.Func`2<MapTile,System.Int32> Tiles::<>f__am$cache2
	Func_2_t38978152 * ___U3CU3Ef__amU24cache2_6;
	// System.Func`2<Building,System.Boolean> Tiles::<>f__am$cache3
	Func_2_t137751404 * ___U3CU3Ef__amU24cache3_7;
	// System.Func`2<Building,System.Boolean> Tiles::<>f__am$cache4
	Func_2_t137751404 * ___U3CU3Ef__amU24cache4_8;
	// System.Func`2<Building,System.Boolean> Tiles::<>f__am$cache5
	Func_2_t137751404 * ___U3CU3Ef__amU24cache5_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline Comparison_1_t1186187174 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline Comparison_1_t1186187174 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(Comparison_1_t1186187174 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_2_t38978152 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_2_t38978152 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_2_t38978152 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Func_2_t38978152 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Func_2_t38978152 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Func_2_t38978152 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline Func_2_t38978152 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline Func_2_t38978152 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(Func_2_t38978152 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline Func_2_t137751404 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline Func_2_t137751404 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(Func_2_t137751404 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline Func_2_t137751404 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline Func_2_t137751404 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(Func_2_t137751404 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_9() { return static_cast<int32_t>(offsetof(Tiles_t1457048987_StaticFields, ___U3CU3Ef__amU24cache5_9)); }
	inline Func_2_t137751404 * get_U3CU3Ef__amU24cache5_9() const { return ___U3CU3Ef__amU24cache5_9; }
	inline Func_2_t137751404 ** get_address_of_U3CU3Ef__amU24cache5_9() { return &___U3CU3Ef__amU24cache5_9; }
	inline void set_U3CU3Ef__amU24cache5_9(Func_2_t137751404 * value)
	{
		___U3CU3Ef__amU24cache5_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILES_T1457048987_H
#ifndef SCENENAMES_T4116278453_H
#define SCENENAMES_T4116278453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strings/SceneNames
struct  SceneNames_t4116278453  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENENAMES_T4116278453_H
#ifndef MATERIALPROPERTIES_T270969053_H
#define MATERIALPROPERTIES_T270969053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strings/MaterialProperties
struct  MaterialProperties_t270969053  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALPROPERTIES_T270969053_H
#ifndef SPRITES_T2880089301_H
#define SPRITES_T2880089301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strings/Sprites
struct  Sprites_t2880089301  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITES_T2880089301_H
#ifndef PLAYERPREFKEYS_T1747914545_H
#define PLAYERPREFKEYS_T1747914545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strings/PlayerPrefKeys
struct  PlayerPrefKeys_t1747914545  : public RuntimeObject
{
public:

public:
};

struct PlayerPrefKeys_t1747914545_StaticFields
{
public:
	// System.String[] Strings/PlayerPrefKeys::Expansions
	StringU5BU5D_t1281789340* ___Expansions_17;
	// System.String[] Strings/PlayerPrefKeys::SpecialRules
	StringU5BU5D_t1281789340* ___SpecialRules_30;

public:
	inline static int32_t get_offset_of_Expansions_17() { return static_cast<int32_t>(offsetof(PlayerPrefKeys_t1747914545_StaticFields, ___Expansions_17)); }
	inline StringU5BU5D_t1281789340* get_Expansions_17() const { return ___Expansions_17; }
	inline StringU5BU5D_t1281789340** get_address_of_Expansions_17() { return &___Expansions_17; }
	inline void set_Expansions_17(StringU5BU5D_t1281789340* value)
	{
		___Expansions_17 = value;
		Il2CppCodeGenWriteBarrier((&___Expansions_17), value);
	}

	inline static int32_t get_offset_of_SpecialRules_30() { return static_cast<int32_t>(offsetof(PlayerPrefKeys_t1747914545_StaticFields, ___SpecialRules_30)); }
	inline StringU5BU5D_t1281789340* get_SpecialRules_30() const { return ___SpecialRules_30; }
	inline StringU5BU5D_t1281789340** get_address_of_SpecialRules_30() { return &___SpecialRules_30; }
	inline void set_SpecialRules_30(StringU5BU5D_t1281789340* value)
	{
		___SpecialRules_30 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialRules_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPREFKEYS_T1747914545_H
#ifndef HERONAMES_T922592968_H
#define HERONAMES_T922592968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strings/HeroNames
struct  HeroNames_t922592968  : public RuntimeObject
{
public:

public:
};

struct HeroNames_t922592968_StaticFields
{
public:
	// System.String[] Strings/HeroNames::AllHeroes
	StringU5BU5D_t1281789340* ___AllHeroes_16;

public:
	inline static int32_t get_offset_of_AllHeroes_16() { return static_cast<int32_t>(offsetof(HeroNames_t922592968_StaticFields, ___AllHeroes_16)); }
	inline StringU5BU5D_t1281789340* get_AllHeroes_16() const { return ___AllHeroes_16; }
	inline StringU5BU5D_t1281789340** get_address_of_AllHeroes_16() { return &___AllHeroes_16; }
	inline void set_AllHeroes_16(StringU5BU5D_t1281789340* value)
	{
		___AllHeroes_16 = value;
		Il2CppCodeGenWriteBarrier((&___AllHeroes_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HERONAMES_T922592968_H
#ifndef BOARDPOSITIONS_T2766534255_H
#define BOARDPOSITIONS_T2766534255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strings/GameObjectNames/BoardPositions
struct  BoardPositions_t2766534255  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOARDPOSITIONS_T2766534255_H
#ifndef GAMEOBJECTNAMES_T1011435887_H
#define GAMEOBJECTNAMES_T1011435887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strings/GameObjectNames
struct  GameObjectNames_t1011435887  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTNAMES_T1011435887_H
#ifndef CAMPAIGNSAVE_T1186366797_H
#define CAMPAIGNSAVE_T1186366797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strings/CampaignSave
struct  CampaignSave_t1186366797  : public RuntimeObject
{
public:

public:
};

struct CampaignSave_t1186366797_StaticFields
{
public:
	// System.String[] Strings/CampaignSave::AllSaveFiles
	StringU5BU5D_t1281789340* ___AllSaveFiles_4;
	// System.String Strings/CampaignSave::Folder
	String_t* ___Folder_5;

public:
	inline static int32_t get_offset_of_AllSaveFiles_4() { return static_cast<int32_t>(offsetof(CampaignSave_t1186366797_StaticFields, ___AllSaveFiles_4)); }
	inline StringU5BU5D_t1281789340* get_AllSaveFiles_4() const { return ___AllSaveFiles_4; }
	inline StringU5BU5D_t1281789340** get_address_of_AllSaveFiles_4() { return &___AllSaveFiles_4; }
	inline void set_AllSaveFiles_4(StringU5BU5D_t1281789340* value)
	{
		___AllSaveFiles_4 = value;
		Il2CppCodeGenWriteBarrier((&___AllSaveFiles_4), value);
	}

	inline static int32_t get_offset_of_Folder_5() { return static_cast<int32_t>(offsetof(CampaignSave_t1186366797_StaticFields, ___Folder_5)); }
	inline String_t* get_Folder_5() const { return ___Folder_5; }
	inline String_t** get_address_of_Folder_5() { return &___Folder_5; }
	inline void set_Folder_5(String_t* value)
	{
		___Folder_5 = value;
		Il2CppCodeGenWriteBarrier((&___Folder_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMPAIGNSAVE_T1186366797_H
#ifndef MATERIALS_T3845095469_H
#define MATERIALS_T3845095469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Strings/Materials
struct  Materials_t3845095469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALS_T3845095469_H
#ifndef STOCKUP_T3356519569_H
#define STOCKUP_T3356519569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StockUp
struct  StockUp_t3356519569  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOCKUP_T3356519569_H
#ifndef ESCAPEINTHEPLANE_T937804966_H
#define ESCAPEINTHEPLANE_T937804966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EscapeInThePlane
struct  EscapeInThePlane_t937804966  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESCAPEINTHEPLANE_T937804966_H
#ifndef HOLDTHELINE_T1394975525_H
#define HOLDTHELINE_T1394975525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoldTheLine
struct  HoldTheLine_t1394975525  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLDTHELINE_T1394975525_H
#ifndef LOSTINTHEWOODS_T1105034517_H
#define LOSTINTHEWOODS_T1105034517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LostInTheWoods
struct  LostInTheWoods_t1105034517  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOSTINTHEWOODS_T1105034517_H
#ifndef SALVAGEMISSION_T1351220260_H
#define SALVAGEMISSION_T1351220260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SalvageMission
struct  SalvageMission_t1351220260  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SALVAGEMISSION_T1351220260_H
#ifndef REVENGEOFTHEDEAD_T2620849876_H
#define REVENGEOFTHEDEAD_T2620849876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RevengeOfTheDead
struct  RevengeOfTheDead_t2620849876  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVENGEOFTHEDEAD_T2620849876_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef CAMPAIGNMISSION1_T881585865_H
#define CAMPAIGNMISSION1_T881585865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CampaignMission1
struct  CampaignMission1_t881585865  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMPAIGNMISSION1_T881585865_H
#ifndef CAMPAIGNMISSION2_T3220238025_H
#define CAMPAIGNMISSION2_T3220238025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CampaignMission2
struct  CampaignMission2_t3220238025  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMPAIGNMISSION2_T3220238025_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ZOMBIEPILLAGE_T886770412_H
#define ZOMBIEPILLAGE_T886770412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombiePillage
struct  ZombiePillage_t886770412  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIEPILLAGE_T886770412_H
#ifndef AIRSTRIKE_T1314356644_H
#define AIRSTRIKE_T1314356644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Airstrike
struct  Airstrike_t1314356644  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AIRSTRIKE_T1314356644_H
#ifndef LEARNTOSURVIVE_T2546588854_H
#define LEARNTOSURVIVE_T2546588854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LearnToSurvive
struct  LearnToSurvive_t2546588854  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEARNTOSURVIVE_T2546588854_H
#ifndef MOUNTAINOFTHEDEAD_T2984684957_H
#define MOUNTAINOFTHEDEAD_T2984684957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MountainOfTheDead
struct  MountainOfTheDead_t2984684957  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUNTAINOFTHEDEAD_T2984684957_H
#ifndef RADIOFORHELP_T586490126_H
#define RADIOFORHELP_T586490126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadioForHelp
struct  RadioForHelp_t586490126  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIOFORHELP_T586490126_H
#ifndef HARDWAREHOSPITALPOLICE_T1260422496_H
#define HARDWAREHOSPITALPOLICE_T1260422496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HardwareHospitalPolice
struct  HardwareHospitalPolice_t1260422496  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HARDWAREHOSPITALPOLICE_T1260422496_H
#ifndef LUMBEROFFICESAW_T4060998708_H
#define LUMBEROFFICESAW_T4060998708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LumberOfficeSaw
struct  LumberOfficeSaw_t4060998708  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMBEROFFICESAW_T4060998708_H
#ifndef DINERPOWERSTATION_T520786256_H
#define DINERPOWERSTATION_T520786256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DinerPowerStation
struct  DinerPowerStation_t520786256  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DINERPOWERSTATION_T520786256_H
#ifndef BOOKGYMHIGHSCHOOL_T4263581806_H
#define BOOKGYMHIGHSCHOOL_T4263581806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BookGymHighSchool
struct  BookGymHighSchool_t4263581806  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOKGYMHIGHSCHOOL_T4263581806_H
#ifndef REFINERYTRAIN_T2111168844_H
#define REFINERYTRAIN_T2111168844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RefineryTrain
struct  RefineryTrain_t2111168844  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFINERYTRAIN_T2111168844_H
#ifndef BOWLINGTAVERN_T1342164777_H
#define BOWLINGTAVERN_T1342164777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BowlingTavern
struct  BowlingTavern_t1342164777  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOWLINGTAVERN_T1342164777_H
#ifndef DRUGPOSTOFFICESUPERMARKET_T367081682_H
#define DRUGPOSTOFFICESUPERMARKET_T367081682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrugPostOfficeSupermarket
struct  DrugPostOfficeSupermarket_t367081682  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRUGPOSTOFFICESUPERMARKET_T367081682_H
#ifndef ANTIQUEFACTORYLIBRARY_T896367873_H
#define ANTIQUEFACTORYLIBRARY_T896367873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AntiqueFactoryLibrary
struct  AntiqueFactoryLibrary_t896367873  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANTIQUEFACTORYLIBRARY_T896367873_H
#ifndef SCHOOLGYM_T119813715_H
#define SCHOOLGYM_T119813715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SchoolGym
struct  SchoolGym_t119813715  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHOOLGYM_T119813715_H
#ifndef HANGARHOSPITALDINER_T1147268456_H
#define HANGARHOSPITALDINER_T1147268456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HangarHospitalDiner
struct  HangarHospitalDiner_t1147268456  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANGARHOSPITALDINER_T1147268456_H
#ifndef GENERALGUNPLANT_T3644479267_H
#define GENERALGUNPLANT_T3644479267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GeneralGunPlant
struct  GeneralGunPlant_t3644479267  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALGUNPLANT_T3644479267_H
#ifndef CHURCHGRAVEYARDPOLICE_T3650776148_H
#define CHURCHGRAVEYARDPOLICE_T3650776148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChurchGraveyardPolice
struct  ChurchGraveyardPolice_t3650776148  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHURCHGRAVEYARDPOLICE_T3650776148_H
#ifndef BARNFARMHOUSE_T415986967_H
#define BARNFARMHOUSE_T415986967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarnFarmhouse
struct  BarnFarmhouse_t415986967  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARNFARMHOUSE_T415986967_H
#ifndef BANKJUNKYARDGAS_T3888625882_H
#define BANKJUNKYARDGAS_T3888625882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BankJunkyardGas
struct  BankJunkyardGas_t3888625882  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANKJUNKYARDGAS_T3888625882_H
#ifndef OLDCABIN_T4142939193_H
#define OLDCABIN_T4142939193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OldCabin
struct  OldCabin_t4142939193  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OLDCABIN_T4142939193_H
#ifndef TOWERRELAY_T1797386082_H
#define TOWERRELAY_T1797386082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerRelay
struct  TowerRelay_t1797386082  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERRELAY_T1797386082_H
#ifndef THRASHERMILL_T3449986375_H
#define THRASHERMILL_T3449986375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThrasherMill
struct  ThrasherMill_t3449986375  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THRASHERMILL_T3449986375_H
#ifndef PLAGUECARRIERS_T2860829270_H
#define PLAGUECARRIERS_T2860829270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlagueCarriers
struct  PlagueCarriers_t2860829270  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAGUECARRIERS_T2860829270_H
#ifndef BLOWUPTHETOWN_T1662253320_H
#define BLOWUPTHETOWN_T1662253320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlowUpTheTown
struct  BlowUpTheTown_t1662253320  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOWUPTHETOWN_T1662253320_H
#ifndef SUPPLYRUN_T1899586975_H
#define SUPPLYRUN_T1899586975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SupplyRun
struct  SupplyRun_t1899586975  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPLYRUN_T1899586975_H
#ifndef SEARCHFORTHETRUTH_T1265244687_H
#define SEARCHFORTHETRUTH_T1265244687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SearchForTheTruth
struct  SearchForTheTruth_t1265244687  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHFORTHETRUTH_T1265244687_H
#ifndef RESCUEMISSION_T3975397388_H
#define RESCUEMISSION_T3975397388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RescueMission
struct  RescueMission_t3975397388  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESCUEMISSION_T3975397388_H
#ifndef HUNKERDOWN_T2613713738_H
#define HUNKERDOWN_T2613713738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HunkerDown
struct  HunkerDown_t2613713738  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUNKERDOWN_T2613713738_H
#ifndef HUNTFORSURVIVORS_T2859470428_H
#define HUNTFORSURVIVORS_T2859470428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HuntForSurvivors
struct  HuntForSurvivors_t2859470428  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUNTFORSURVIVORS_T2859470428_H
#ifndef ZOMBIEAPOCALYPSE_T1518918614_H
#define ZOMBIEAPOCALYPSE_T1518918614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieApocalypse
struct  ZombieApocalypse_t1518918614  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIEAPOCALYPSE_T1518918614_H
#ifndef RANGERSTATION_T488444359_H
#define RANGERSTATION_T488444359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RangerStation
struct  RangerStation_t488444359  : public MapTile_t3503354235
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGERSTATION_T488444359_H
#ifndef CAMPAIGNMISSION3_T1263922889_H
#define CAMPAIGNMISSION3_T1263922889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CampaignMission3
struct  CampaignMission3_t1263922889  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMPAIGNMISSION3_T1263922889_H
#ifndef SAVETHETOWNSFOLK_T4109486956_H
#define SAVETHETOWNSFOLK_T4109486956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SaveTheTownsFolk
struct  SaveTheTownsFolk_t4109486956  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVETHETOWNSFOLK_T4109486956_H
#ifndef ESCAPEINTHETRUCK_T1670568109_H
#define ESCAPEINTHETRUCK_T1670568109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EscapeInTheTruck
struct  EscapeInTheTruck_t1670568109  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESCAPEINTHETRUCK_T1670568109_H
#ifndef DIEZOMBIESDIE_T3864401308_H
#define DIEZOMBIESDIE_T3864401308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DieZombiesDie
struct  DieZombiesDie_t3864401308  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIEZOMBIESDIE_T3864401308_H
#ifndef DEFENDTHEMANORHOUSE_T4176879821_H
#define DEFENDTHEMANORHOUSE_T4176879821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefendTheManorHouse
struct  DefendTheManorHouse_t4176879821  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFENDTHEMANORHOUSE_T4176879821_H
#ifndef BURNEMOUT_T307227638_H
#define BURNEMOUT_T307227638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BurnEmOut
struct  BurnEmOut_t307227638  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BURNEMOUT_T307227638_H
#ifndef TUTORIALMISSION_T2826207972_H
#define TUTORIALMISSION_T2826207972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialMission
struct  TutorialMission_t2826207972  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALMISSION_T2826207972_H
#ifndef BURNITTOTHEGROUND_T648940805_H
#define BURNITTOTHEGROUND_T648940805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BurnItToTheGround
struct  BurnItToTheGround_t648940805  : public Mission_t4233471175
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BURNITTOTHEGROUND_T648940805_H
#ifndef ZOMBIESPEED_T2101478944_H
#define ZOMBIESPEED_T2101478944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieSpeed
struct  ZombieSpeed_t2101478944 
{
public:
	// System.Int32 ZombieSpeed::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ZombieSpeed_t2101478944, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIESPEED_T2101478944_H
#ifndef ZOMBIEMOVEMENTTARGET_T4013730920_H
#define ZOMBIEMOVEMENTTARGET_T4013730920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieMovementTarget
struct  ZombieMovementTarget_t4013730920 
{
public:
	// System.Int32 ZombieMovementTarget::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ZombieMovementTarget_t4013730920, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIEMOVEMENTTARGET_T4013730920_H
#ifndef ZOMBIETYPE_T2798134437_H
#define ZOMBIETYPE_T2798134437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieType
struct  ZombieType_t2798134437 
{
public:
	// System.Int32 ZombieType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ZombieType_t2798134437, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIETYPE_T2798134437_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef PLAYDURINGPHASE_T3488456769_H
#define PLAYDURINGPHASE_T3488456769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayDuringPhase
struct  PlayDuringPhase_t3488456769 
{
public:
	// System.Int32 PlayDuringPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayDuringPhase_t3488456769, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYDURINGPHASE_T3488456769_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COLORCHANNEL_T1119199196_H
#define COLORCHANNEL_T1119199196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Building/ColorChannel
struct  ColorChannel_t1119199196 
{
public:
	// System.Int32 Building/ColorChannel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorChannel_t1119199196, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCHANNEL_T1119199196_H
#ifndef SCENARIOSEARCHITEMTYPES_T1972627041_H
#define SCENARIOSEARCHITEMTYPES_T1972627041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mission/ScenarioSearchItemTypes
struct  ScenarioSearchItemTypes_t1972627041 
{
public:
	// System.Int32 Mission/ScenarioSearchItemTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScenarioSearchItemTypes_t1972627041, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENARIOSEARCHITEMTYPES_T1972627041_H
#ifndef NFCMODE_T1366099354_H
#define NFCMODE_T1366099354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NFC/NFCMode
struct  NFCMode_t1366099354 
{
public:
	// System.Int32 NFC/NFCMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NFCMode_t1366099354, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NFCMODE_T1366099354_H
#ifndef BUILDING_T1411255995_H
#define BUILDING_T1411255995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Building
struct  Building_t1411255995  : public RuntimeObject
{
public:
	// System.String Building::name
	String_t* ___name_0;
	// System.Collections.Generic.List`1<System.Int32> Building::rollRange
	List_1_t128053199 * ___rollRange_1;
	// System.String Building::pickUpItem
	String_t* ___pickUpItem_2;
	// System.Int32 Building::spaces
	int32_t ___spaces_3;
	// System.Int32 Building::numDoors
	int32_t ___numDoors_4;
	// System.Int32 Building::numLockedDoors
	int32_t ___numLockedDoors_5;
	// Building/ColorChannel Building::matColorCode
	int32_t ___matColorCode_6;
	// System.Boolean Building::<HasSpawningPit>k__BackingField
	bool ___U3CHasSpawningPitU3Ek__BackingField_7;
	// System.Boolean Building::<IsTakenOver>k__BackingField
	bool ___U3CIsTakenOverU3Ek__BackingField_8;
	// System.Boolean Building::<HasLightsOut>k__BackingField
	bool ___U3CHasLightsOutU3Ek__BackingField_9;
	// System.Boolean Building::<IsDestroyed>k__BackingField
	bool ___U3CIsDestroyedU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Building_t1411255995, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_rollRange_1() { return static_cast<int32_t>(offsetof(Building_t1411255995, ___rollRange_1)); }
	inline List_1_t128053199 * get_rollRange_1() const { return ___rollRange_1; }
	inline List_1_t128053199 ** get_address_of_rollRange_1() { return &___rollRange_1; }
	inline void set_rollRange_1(List_1_t128053199 * value)
	{
		___rollRange_1 = value;
		Il2CppCodeGenWriteBarrier((&___rollRange_1), value);
	}

	inline static int32_t get_offset_of_pickUpItem_2() { return static_cast<int32_t>(offsetof(Building_t1411255995, ___pickUpItem_2)); }
	inline String_t* get_pickUpItem_2() const { return ___pickUpItem_2; }
	inline String_t** get_address_of_pickUpItem_2() { return &___pickUpItem_2; }
	inline void set_pickUpItem_2(String_t* value)
	{
		___pickUpItem_2 = value;
		Il2CppCodeGenWriteBarrier((&___pickUpItem_2), value);
	}

	inline static int32_t get_offset_of_spaces_3() { return static_cast<int32_t>(offsetof(Building_t1411255995, ___spaces_3)); }
	inline int32_t get_spaces_3() const { return ___spaces_3; }
	inline int32_t* get_address_of_spaces_3() { return &___spaces_3; }
	inline void set_spaces_3(int32_t value)
	{
		___spaces_3 = value;
	}

	inline static int32_t get_offset_of_numDoors_4() { return static_cast<int32_t>(offsetof(Building_t1411255995, ___numDoors_4)); }
	inline int32_t get_numDoors_4() const { return ___numDoors_4; }
	inline int32_t* get_address_of_numDoors_4() { return &___numDoors_4; }
	inline void set_numDoors_4(int32_t value)
	{
		___numDoors_4 = value;
	}

	inline static int32_t get_offset_of_numLockedDoors_5() { return static_cast<int32_t>(offsetof(Building_t1411255995, ___numLockedDoors_5)); }
	inline int32_t get_numLockedDoors_5() const { return ___numLockedDoors_5; }
	inline int32_t* get_address_of_numLockedDoors_5() { return &___numLockedDoors_5; }
	inline void set_numLockedDoors_5(int32_t value)
	{
		___numLockedDoors_5 = value;
	}

	inline static int32_t get_offset_of_matColorCode_6() { return static_cast<int32_t>(offsetof(Building_t1411255995, ___matColorCode_6)); }
	inline int32_t get_matColorCode_6() const { return ___matColorCode_6; }
	inline int32_t* get_address_of_matColorCode_6() { return &___matColorCode_6; }
	inline void set_matColorCode_6(int32_t value)
	{
		___matColorCode_6 = value;
	}

	inline static int32_t get_offset_of_U3CHasSpawningPitU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Building_t1411255995, ___U3CHasSpawningPitU3Ek__BackingField_7)); }
	inline bool get_U3CHasSpawningPitU3Ek__BackingField_7() const { return ___U3CHasSpawningPitU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CHasSpawningPitU3Ek__BackingField_7() { return &___U3CHasSpawningPitU3Ek__BackingField_7; }
	inline void set_U3CHasSpawningPitU3Ek__BackingField_7(bool value)
	{
		___U3CHasSpawningPitU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsTakenOverU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Building_t1411255995, ___U3CIsTakenOverU3Ek__BackingField_8)); }
	inline bool get_U3CIsTakenOverU3Ek__BackingField_8() const { return ___U3CIsTakenOverU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsTakenOverU3Ek__BackingField_8() { return &___U3CIsTakenOverU3Ek__BackingField_8; }
	inline void set_U3CIsTakenOverU3Ek__BackingField_8(bool value)
	{
		___U3CIsTakenOverU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CHasLightsOutU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Building_t1411255995, ___U3CHasLightsOutU3Ek__BackingField_9)); }
	inline bool get_U3CHasLightsOutU3Ek__BackingField_9() const { return ___U3CHasLightsOutU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CHasLightsOutU3Ek__BackingField_9() { return &___U3CHasLightsOutU3Ek__BackingField_9; }
	inline void set_U3CHasLightsOutU3Ek__BackingField_9(bool value)
	{
		___U3CHasLightsOutU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CIsDestroyedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Building_t1411255995, ___U3CIsDestroyedU3Ek__BackingField_10)); }
	inline bool get_U3CIsDestroyedU3Ek__BackingField_10() const { return ___U3CIsDestroyedU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsDestroyedU3Ek__BackingField_10() { return &___U3CIsDestroyedU3Ek__BackingField_10; }
	inline void set_U3CIsDestroyedU3Ek__BackingField_10(bool value)
	{
		___U3CIsDestroyedU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDING_T1411255995_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef U3CGETZOMBIEOFTYPEU3EC__ANONSTOREY0_T1243602918_H
#define U3CGETZOMBIEOFTYPEU3EC__ANONSTOREY0_T1243602918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zombies/<GetZombieOfType>c__AnonStorey0
struct  U3CGetZombieOfTypeU3Ec__AnonStorey0_t1243602918  : public RuntimeObject
{
public:
	// ZombieType Zombies/<GetZombieOfType>c__AnonStorey0::zombieType
	int32_t ___zombieType_0;

public:
	inline static int32_t get_offset_of_zombieType_0() { return static_cast<int32_t>(offsetof(U3CGetZombieOfTypeU3Ec__AnonStorey0_t1243602918, ___zombieType_0)); }
	inline int32_t get_zombieType_0() const { return ___zombieType_0; }
	inline int32_t* get_address_of_zombieType_0() { return &___zombieType_0; }
	inline void set_zombieType_0(int32_t value)
	{
		___zombieType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETZOMBIEOFTYPEU3EC__ANONSTOREY0_T1243602918_H
#ifndef ZOMBIE_T2596553718_H
#define ZOMBIE_T2596553718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zombie
struct  Zombie_t2596553718  : public RuntimeObject
{
public:
	// System.String Zombie::name
	String_t* ___name_0;
	// ZombieType Zombie::type
	int32_t ___type_1;
	// ZombieSpeed Zombie::speed
	int32_t ___speed_2;
	// System.Int32 Zombie::health
	int32_t ___health_3;
	// System.Int32 Zombie::hungerRange
	int32_t ___hungerRange_4;
	// System.Boolean Zombie::countTowardSpawnCheck
	bool ___countTowardSpawnCheck_5;
	// ZombieMovementTarget Zombie::movementTarget
	int32_t ___movementTarget_6;
	// ZombieTurn Zombie::zombieTurn
	ZombieTurn_t3562480803 * ___zombieTurn_7;
	// System.Boolean Zombie::ignoreWoundOnFourPlus
	bool ___ignoreWoundOnFourPlus_8;
	// System.Boolean Zombie::canSpawnWithOtherZombie
	bool ___canSpawnWithOtherZombie_9;
	// System.Boolean Zombie::makesZombieHeroOnFivePlus
	bool ___makesZombieHeroOnFivePlus_10;
	// System.Int32 Zombie::zombiesInPool
	int32_t ___zombiesInPool_11;
	// System.Int32 Zombie::zombiesOnBoard
	int32_t ___zombiesOnBoard_12;
	// System.Int32 Zombie::baseFightDice
	int32_t ___baseFightDice_13;
	// System.Int32 Zombie::fightDice
	int32_t ___fightDice_14;
	// System.Int32 Zombie::bonusDice
	int32_t ___bonusDice_15;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___speed_2)); }
	inline int32_t get_speed_2() const { return ___speed_2; }
	inline int32_t* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(int32_t value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_health_3() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___health_3)); }
	inline int32_t get_health_3() const { return ___health_3; }
	inline int32_t* get_address_of_health_3() { return &___health_3; }
	inline void set_health_3(int32_t value)
	{
		___health_3 = value;
	}

	inline static int32_t get_offset_of_hungerRange_4() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___hungerRange_4)); }
	inline int32_t get_hungerRange_4() const { return ___hungerRange_4; }
	inline int32_t* get_address_of_hungerRange_4() { return &___hungerRange_4; }
	inline void set_hungerRange_4(int32_t value)
	{
		___hungerRange_4 = value;
	}

	inline static int32_t get_offset_of_countTowardSpawnCheck_5() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___countTowardSpawnCheck_5)); }
	inline bool get_countTowardSpawnCheck_5() const { return ___countTowardSpawnCheck_5; }
	inline bool* get_address_of_countTowardSpawnCheck_5() { return &___countTowardSpawnCheck_5; }
	inline void set_countTowardSpawnCheck_5(bool value)
	{
		___countTowardSpawnCheck_5 = value;
	}

	inline static int32_t get_offset_of_movementTarget_6() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___movementTarget_6)); }
	inline int32_t get_movementTarget_6() const { return ___movementTarget_6; }
	inline int32_t* get_address_of_movementTarget_6() { return &___movementTarget_6; }
	inline void set_movementTarget_6(int32_t value)
	{
		___movementTarget_6 = value;
	}

	inline static int32_t get_offset_of_zombieTurn_7() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___zombieTurn_7)); }
	inline ZombieTurn_t3562480803 * get_zombieTurn_7() const { return ___zombieTurn_7; }
	inline ZombieTurn_t3562480803 ** get_address_of_zombieTurn_7() { return &___zombieTurn_7; }
	inline void set_zombieTurn_7(ZombieTurn_t3562480803 * value)
	{
		___zombieTurn_7 = value;
		Il2CppCodeGenWriteBarrier((&___zombieTurn_7), value);
	}

	inline static int32_t get_offset_of_ignoreWoundOnFourPlus_8() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___ignoreWoundOnFourPlus_8)); }
	inline bool get_ignoreWoundOnFourPlus_8() const { return ___ignoreWoundOnFourPlus_8; }
	inline bool* get_address_of_ignoreWoundOnFourPlus_8() { return &___ignoreWoundOnFourPlus_8; }
	inline void set_ignoreWoundOnFourPlus_8(bool value)
	{
		___ignoreWoundOnFourPlus_8 = value;
	}

	inline static int32_t get_offset_of_canSpawnWithOtherZombie_9() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___canSpawnWithOtherZombie_9)); }
	inline bool get_canSpawnWithOtherZombie_9() const { return ___canSpawnWithOtherZombie_9; }
	inline bool* get_address_of_canSpawnWithOtherZombie_9() { return &___canSpawnWithOtherZombie_9; }
	inline void set_canSpawnWithOtherZombie_9(bool value)
	{
		___canSpawnWithOtherZombie_9 = value;
	}

	inline static int32_t get_offset_of_makesZombieHeroOnFivePlus_10() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___makesZombieHeroOnFivePlus_10)); }
	inline bool get_makesZombieHeroOnFivePlus_10() const { return ___makesZombieHeroOnFivePlus_10; }
	inline bool* get_address_of_makesZombieHeroOnFivePlus_10() { return &___makesZombieHeroOnFivePlus_10; }
	inline void set_makesZombieHeroOnFivePlus_10(bool value)
	{
		___makesZombieHeroOnFivePlus_10 = value;
	}

	inline static int32_t get_offset_of_zombiesInPool_11() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___zombiesInPool_11)); }
	inline int32_t get_zombiesInPool_11() const { return ___zombiesInPool_11; }
	inline int32_t* get_address_of_zombiesInPool_11() { return &___zombiesInPool_11; }
	inline void set_zombiesInPool_11(int32_t value)
	{
		___zombiesInPool_11 = value;
	}

	inline static int32_t get_offset_of_zombiesOnBoard_12() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___zombiesOnBoard_12)); }
	inline int32_t get_zombiesOnBoard_12() const { return ___zombiesOnBoard_12; }
	inline int32_t* get_address_of_zombiesOnBoard_12() { return &___zombiesOnBoard_12; }
	inline void set_zombiesOnBoard_12(int32_t value)
	{
		___zombiesOnBoard_12 = value;
	}

	inline static int32_t get_offset_of_baseFightDice_13() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___baseFightDice_13)); }
	inline int32_t get_baseFightDice_13() const { return ___baseFightDice_13; }
	inline int32_t* get_address_of_baseFightDice_13() { return &___baseFightDice_13; }
	inline void set_baseFightDice_13(int32_t value)
	{
		___baseFightDice_13 = value;
	}

	inline static int32_t get_offset_of_fightDice_14() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___fightDice_14)); }
	inline int32_t get_fightDice_14() const { return ___fightDice_14; }
	inline int32_t* get_address_of_fightDice_14() { return &___fightDice_14; }
	inline void set_fightDice_14(int32_t value)
	{
		___fightDice_14 = value;
	}

	inline static int32_t get_offset_of_bonusDice_15() { return static_cast<int32_t>(offsetof(Zombie_t2596553718, ___bonusDice_15)); }
	inline int32_t get_bonusDice_15() const { return ___bonusDice_15; }
	inline int32_t* get_address_of_bonusDice_15() { return &___bonusDice_15; }
	inline void set_bonusDice_15(int32_t value)
	{
		___bonusDice_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIE_T2596553718_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef ZOMBIECARD_T2237653742_H
#define ZOMBIECARD_T2237653742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieCard
struct  ZombieCard_t2237653742  : public RuntimeObject
{
public:
	// System.String ZombieCard::Name
	String_t* ___Name_0;
	// System.String ZombieCard::RemainsInPlayName
	String_t* ___RemainsInPlayName_1;
	// PlayDuringPhase ZombieCard::Phase
	int32_t ___Phase_2;
	// System.String ZombieCard::OriginalText
	String_t* ___OriginalText_3;
	// System.String ZombieCard::FlavorText
	String_t* ___FlavorText_4;
	// System.Boolean ZombieCard::RemainsInPlay
	bool ___RemainsInPlay_5;
	// System.Boolean ZombieCard::tempCard
	bool ___tempCard_6;
	// System.Boolean ZombieCard::SkipCard
	bool ___SkipCard_7;
	// ZombieTurn ZombieCard::ZombieTurnManager
	ZombieTurn_t3562480803 * ___ZombieTurnManager_8;
	// Building ZombieCard::Building
	Building_t1411255995 * ___Building_9;
	// System.String ZombieCard::HeroToEffect
	String_t* ___HeroToEffect_10;
	// System.Int32 ZombieCard::NewZombies
	int32_t ___NewZombies_11;
	// System.Int32 ZombieCard::CardRollValue
	int32_t ___CardRollValue_12;
	// ZombieCard ZombieCard::CardToEffect
	ZombieCard_t2237653742 * ___CardToEffect_13;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_RemainsInPlayName_1() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___RemainsInPlayName_1)); }
	inline String_t* get_RemainsInPlayName_1() const { return ___RemainsInPlayName_1; }
	inline String_t** get_address_of_RemainsInPlayName_1() { return &___RemainsInPlayName_1; }
	inline void set_RemainsInPlayName_1(String_t* value)
	{
		___RemainsInPlayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___RemainsInPlayName_1), value);
	}

	inline static int32_t get_offset_of_Phase_2() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___Phase_2)); }
	inline int32_t get_Phase_2() const { return ___Phase_2; }
	inline int32_t* get_address_of_Phase_2() { return &___Phase_2; }
	inline void set_Phase_2(int32_t value)
	{
		___Phase_2 = value;
	}

	inline static int32_t get_offset_of_OriginalText_3() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___OriginalText_3)); }
	inline String_t* get_OriginalText_3() const { return ___OriginalText_3; }
	inline String_t** get_address_of_OriginalText_3() { return &___OriginalText_3; }
	inline void set_OriginalText_3(String_t* value)
	{
		___OriginalText_3 = value;
		Il2CppCodeGenWriteBarrier((&___OriginalText_3), value);
	}

	inline static int32_t get_offset_of_FlavorText_4() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___FlavorText_4)); }
	inline String_t* get_FlavorText_4() const { return ___FlavorText_4; }
	inline String_t** get_address_of_FlavorText_4() { return &___FlavorText_4; }
	inline void set_FlavorText_4(String_t* value)
	{
		___FlavorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___FlavorText_4), value);
	}

	inline static int32_t get_offset_of_RemainsInPlay_5() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___RemainsInPlay_5)); }
	inline bool get_RemainsInPlay_5() const { return ___RemainsInPlay_5; }
	inline bool* get_address_of_RemainsInPlay_5() { return &___RemainsInPlay_5; }
	inline void set_RemainsInPlay_5(bool value)
	{
		___RemainsInPlay_5 = value;
	}

	inline static int32_t get_offset_of_tempCard_6() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___tempCard_6)); }
	inline bool get_tempCard_6() const { return ___tempCard_6; }
	inline bool* get_address_of_tempCard_6() { return &___tempCard_6; }
	inline void set_tempCard_6(bool value)
	{
		___tempCard_6 = value;
	}

	inline static int32_t get_offset_of_SkipCard_7() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___SkipCard_7)); }
	inline bool get_SkipCard_7() const { return ___SkipCard_7; }
	inline bool* get_address_of_SkipCard_7() { return &___SkipCard_7; }
	inline void set_SkipCard_7(bool value)
	{
		___SkipCard_7 = value;
	}

	inline static int32_t get_offset_of_ZombieTurnManager_8() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___ZombieTurnManager_8)); }
	inline ZombieTurn_t3562480803 * get_ZombieTurnManager_8() const { return ___ZombieTurnManager_8; }
	inline ZombieTurn_t3562480803 ** get_address_of_ZombieTurnManager_8() { return &___ZombieTurnManager_8; }
	inline void set_ZombieTurnManager_8(ZombieTurn_t3562480803 * value)
	{
		___ZombieTurnManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieTurnManager_8), value);
	}

	inline static int32_t get_offset_of_Building_9() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___Building_9)); }
	inline Building_t1411255995 * get_Building_9() const { return ___Building_9; }
	inline Building_t1411255995 ** get_address_of_Building_9() { return &___Building_9; }
	inline void set_Building_9(Building_t1411255995 * value)
	{
		___Building_9 = value;
		Il2CppCodeGenWriteBarrier((&___Building_9), value);
	}

	inline static int32_t get_offset_of_HeroToEffect_10() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___HeroToEffect_10)); }
	inline String_t* get_HeroToEffect_10() const { return ___HeroToEffect_10; }
	inline String_t** get_address_of_HeroToEffect_10() { return &___HeroToEffect_10; }
	inline void set_HeroToEffect_10(String_t* value)
	{
		___HeroToEffect_10 = value;
		Il2CppCodeGenWriteBarrier((&___HeroToEffect_10), value);
	}

	inline static int32_t get_offset_of_NewZombies_11() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___NewZombies_11)); }
	inline int32_t get_NewZombies_11() const { return ___NewZombies_11; }
	inline int32_t* get_address_of_NewZombies_11() { return &___NewZombies_11; }
	inline void set_NewZombies_11(int32_t value)
	{
		___NewZombies_11 = value;
	}

	inline static int32_t get_offset_of_CardRollValue_12() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___CardRollValue_12)); }
	inline int32_t get_CardRollValue_12() const { return ___CardRollValue_12; }
	inline int32_t* get_address_of_CardRollValue_12() { return &___CardRollValue_12; }
	inline void set_CardRollValue_12(int32_t value)
	{
		___CardRollValue_12 = value;
	}

	inline static int32_t get_offset_of_CardToEffect_13() { return static_cast<int32_t>(offsetof(ZombieCard_t2237653742, ___CardToEffect_13)); }
	inline ZombieCard_t2237653742 * get_CardToEffect_13() const { return ___CardToEffect_13; }
	inline ZombieCard_t2237653742 ** get_address_of_CardToEffect_13() { return &___CardToEffect_13; }
	inline void set_CardToEffect_13(ZombieCard_t2237653742 * value)
	{
		___CardToEffect_13 = value;
		Il2CppCodeGenWriteBarrier((&___CardToEffect_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIECARD_T2237653742_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef CORNERED_T3235490063_H
#define CORNERED_T3235490063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cornered
struct  Cornered_t3235490063  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNERED_T3235490063_H
#ifndef LIGHTSOUT_T1738659613_H
#define LIGHTSOUT_T1738659613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightsOut
struct  LightsOut_t1738659613  : public ZombieCard_t2237653742
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTSOUT_T1738659613_H
#ifndef HEROZOMBIEONBOARD_T1308641688_H
#define HEROZOMBIEONBOARD_T1308641688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zombies/HeroZombieOnBoard
struct  HeroZombieOnBoard_t1308641688  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROZOMBIEONBOARD_T1308641688_H
#ifndef ZOMBIESAREKILLABLE_T2501657832_H
#define ZOMBIESAREKILLABLE_T2501657832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zombies/ZombiesAreKillable
struct  ZombiesAreKillable_t2501657832  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIESAREKILLABLE_T2501657832_H
#ifndef ZOMBIESONBOARD_T3526012930_H
#define ZOMBIESONBOARD_T3526012930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zombies/ZombiesOnBoard
struct  ZombiesOnBoard_t3526012930  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIESONBOARD_T3526012930_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef VOICECOMMAND_T286642266_H
#define VOICECOMMAND_T286642266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoiceCommand
struct  VoiceCommand_t286642266  : public MonoBehaviour_t3962482529
{
public:
	// SpeechRecognizerManager VoiceCommand::voiceManager
	SpeechRecognizerManager_t652028364 * ___voiceManager_2;
	// System.Boolean VoiceCommand::isListening
	bool ___isListening_3;
	// System.String VoiceCommand::message
	String_t* ___message_4;
	// LoadScene VoiceCommand::loadScene
	LoadScene_t3470671713 * ___loadScene_5;
	// ZombieTurn VoiceCommand::zombieTurn
	ZombieTurn_t3562480803 * ___zombieTurn_6;
	// UnityEngine.Animator VoiceCommand::fight
	Animator_t434523843 * ___fight_7;
	// UnityEngine.Animator VoiceCommand::remains
	Animator_t434523843 * ___remains_8;
	// UnityEngine.UI.Text VoiceCommand::text
	Text_t1901882714 * ___text_9;
	// UnityEngine.GameObject VoiceCommand::voicePanel
	GameObject_t1113636619 * ___voicePanel_10;
	// UnityEngine.Audio.AudioMixer VoiceCommand::Mixer
	AudioMixer_t3521020193 * ___Mixer_11;

public:
	inline static int32_t get_offset_of_voiceManager_2() { return static_cast<int32_t>(offsetof(VoiceCommand_t286642266, ___voiceManager_2)); }
	inline SpeechRecognizerManager_t652028364 * get_voiceManager_2() const { return ___voiceManager_2; }
	inline SpeechRecognizerManager_t652028364 ** get_address_of_voiceManager_2() { return &___voiceManager_2; }
	inline void set_voiceManager_2(SpeechRecognizerManager_t652028364 * value)
	{
		___voiceManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___voiceManager_2), value);
	}

	inline static int32_t get_offset_of_isListening_3() { return static_cast<int32_t>(offsetof(VoiceCommand_t286642266, ___isListening_3)); }
	inline bool get_isListening_3() const { return ___isListening_3; }
	inline bool* get_address_of_isListening_3() { return &___isListening_3; }
	inline void set_isListening_3(bool value)
	{
		___isListening_3 = value;
	}

	inline static int32_t get_offset_of_message_4() { return static_cast<int32_t>(offsetof(VoiceCommand_t286642266, ___message_4)); }
	inline String_t* get_message_4() const { return ___message_4; }
	inline String_t** get_address_of_message_4() { return &___message_4; }
	inline void set_message_4(String_t* value)
	{
		___message_4 = value;
		Il2CppCodeGenWriteBarrier((&___message_4), value);
	}

	inline static int32_t get_offset_of_loadScene_5() { return static_cast<int32_t>(offsetof(VoiceCommand_t286642266, ___loadScene_5)); }
	inline LoadScene_t3470671713 * get_loadScene_5() const { return ___loadScene_5; }
	inline LoadScene_t3470671713 ** get_address_of_loadScene_5() { return &___loadScene_5; }
	inline void set_loadScene_5(LoadScene_t3470671713 * value)
	{
		___loadScene_5 = value;
		Il2CppCodeGenWriteBarrier((&___loadScene_5), value);
	}

	inline static int32_t get_offset_of_zombieTurn_6() { return static_cast<int32_t>(offsetof(VoiceCommand_t286642266, ___zombieTurn_6)); }
	inline ZombieTurn_t3562480803 * get_zombieTurn_6() const { return ___zombieTurn_6; }
	inline ZombieTurn_t3562480803 ** get_address_of_zombieTurn_6() { return &___zombieTurn_6; }
	inline void set_zombieTurn_6(ZombieTurn_t3562480803 * value)
	{
		___zombieTurn_6 = value;
		Il2CppCodeGenWriteBarrier((&___zombieTurn_6), value);
	}

	inline static int32_t get_offset_of_fight_7() { return static_cast<int32_t>(offsetof(VoiceCommand_t286642266, ___fight_7)); }
	inline Animator_t434523843 * get_fight_7() const { return ___fight_7; }
	inline Animator_t434523843 ** get_address_of_fight_7() { return &___fight_7; }
	inline void set_fight_7(Animator_t434523843 * value)
	{
		___fight_7 = value;
		Il2CppCodeGenWriteBarrier((&___fight_7), value);
	}

	inline static int32_t get_offset_of_remains_8() { return static_cast<int32_t>(offsetof(VoiceCommand_t286642266, ___remains_8)); }
	inline Animator_t434523843 * get_remains_8() const { return ___remains_8; }
	inline Animator_t434523843 ** get_address_of_remains_8() { return &___remains_8; }
	inline void set_remains_8(Animator_t434523843 * value)
	{
		___remains_8 = value;
		Il2CppCodeGenWriteBarrier((&___remains_8), value);
	}

	inline static int32_t get_offset_of_text_9() { return static_cast<int32_t>(offsetof(VoiceCommand_t286642266, ___text_9)); }
	inline Text_t1901882714 * get_text_9() const { return ___text_9; }
	inline Text_t1901882714 ** get_address_of_text_9() { return &___text_9; }
	inline void set_text_9(Text_t1901882714 * value)
	{
		___text_9 = value;
		Il2CppCodeGenWriteBarrier((&___text_9), value);
	}

	inline static int32_t get_offset_of_voicePanel_10() { return static_cast<int32_t>(offsetof(VoiceCommand_t286642266, ___voicePanel_10)); }
	inline GameObject_t1113636619 * get_voicePanel_10() const { return ___voicePanel_10; }
	inline GameObject_t1113636619 ** get_address_of_voicePanel_10() { return &___voicePanel_10; }
	inline void set_voicePanel_10(GameObject_t1113636619 * value)
	{
		___voicePanel_10 = value;
		Il2CppCodeGenWriteBarrier((&___voicePanel_10), value);
	}

	inline static int32_t get_offset_of_Mixer_11() { return static_cast<int32_t>(offsetof(VoiceCommand_t286642266, ___Mixer_11)); }
	inline AudioMixer_t3521020193 * get_Mixer_11() const { return ___Mixer_11; }
	inline AudioMixer_t3521020193 ** get_address_of_Mixer_11() { return &___Mixer_11; }
	inline void set_Mixer_11(AudioMixer_t3521020193 * value)
	{
		___Mixer_11 = value;
		Il2CppCodeGenWriteBarrier((&___Mixer_11), value);
	}
};

struct VoiceCommand_t286642266_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> VoiceCommand::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_12() { return static_cast<int32_t>(offsetof(VoiceCommand_t286642266_StaticFields, ___U3CU3Ef__switchU24map1_12)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_12() const { return ___U3CU3Ef__switchU24map1_12; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_12() { return &___U3CU3Ef__switchU24map1_12; }
	inline void set_U3CU3Ef__switchU24map1_12(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOICECOMMAND_T286642266_H
#ifndef OBJECTIVE_T1269591627_H
#define OBJECTIVE_T1269591627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Objective
struct  Objective_t1269591627  : public MonoBehaviour_t3962482529
{
public:
	// System.String Objective::Name
	String_t* ___Name_2;
	// System.Int32 Objective::CurrentProgress
	int32_t ___CurrentProgress_3;
	// System.Int32 Objective::Goal
	int32_t ___Goal_4;
	// ZombieTurn Objective::zombieTurnManager
	ZombieTurn_t3562480803 * ___zombieTurnManager_5;
	// UnityEngine.UI.Text Objective::ObjectiveTitle
	Text_t1901882714 * ___ObjectiveTitle_6;
	// UnityEngine.UI.Text Objective::CurrentProgressText
	Text_t1901882714 * ___CurrentProgressText_7;
	// UnityEngine.UI.Image Objective::ProgressBar
	Image_t2670269651 * ___ProgressBar_8;
	// System.Boolean Objective::IsHeroGoal
	bool ___IsHeroGoal_9;
	// System.String Objective::AudioName
	String_t* ___AudioName_10;
	// System.Boolean Objective::isDebugLog
	bool ___isDebugLog_11;

public:
	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(Objective_t1269591627, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}

	inline static int32_t get_offset_of_CurrentProgress_3() { return static_cast<int32_t>(offsetof(Objective_t1269591627, ___CurrentProgress_3)); }
	inline int32_t get_CurrentProgress_3() const { return ___CurrentProgress_3; }
	inline int32_t* get_address_of_CurrentProgress_3() { return &___CurrentProgress_3; }
	inline void set_CurrentProgress_3(int32_t value)
	{
		___CurrentProgress_3 = value;
	}

	inline static int32_t get_offset_of_Goal_4() { return static_cast<int32_t>(offsetof(Objective_t1269591627, ___Goal_4)); }
	inline int32_t get_Goal_4() const { return ___Goal_4; }
	inline int32_t* get_address_of_Goal_4() { return &___Goal_4; }
	inline void set_Goal_4(int32_t value)
	{
		___Goal_4 = value;
	}

	inline static int32_t get_offset_of_zombieTurnManager_5() { return static_cast<int32_t>(offsetof(Objective_t1269591627, ___zombieTurnManager_5)); }
	inline ZombieTurn_t3562480803 * get_zombieTurnManager_5() const { return ___zombieTurnManager_5; }
	inline ZombieTurn_t3562480803 ** get_address_of_zombieTurnManager_5() { return &___zombieTurnManager_5; }
	inline void set_zombieTurnManager_5(ZombieTurn_t3562480803 * value)
	{
		___zombieTurnManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___zombieTurnManager_5), value);
	}

	inline static int32_t get_offset_of_ObjectiveTitle_6() { return static_cast<int32_t>(offsetof(Objective_t1269591627, ___ObjectiveTitle_6)); }
	inline Text_t1901882714 * get_ObjectiveTitle_6() const { return ___ObjectiveTitle_6; }
	inline Text_t1901882714 ** get_address_of_ObjectiveTitle_6() { return &___ObjectiveTitle_6; }
	inline void set_ObjectiveTitle_6(Text_t1901882714 * value)
	{
		___ObjectiveTitle_6 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectiveTitle_6), value);
	}

	inline static int32_t get_offset_of_CurrentProgressText_7() { return static_cast<int32_t>(offsetof(Objective_t1269591627, ___CurrentProgressText_7)); }
	inline Text_t1901882714 * get_CurrentProgressText_7() const { return ___CurrentProgressText_7; }
	inline Text_t1901882714 ** get_address_of_CurrentProgressText_7() { return &___CurrentProgressText_7; }
	inline void set_CurrentProgressText_7(Text_t1901882714 * value)
	{
		___CurrentProgressText_7 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentProgressText_7), value);
	}

	inline static int32_t get_offset_of_ProgressBar_8() { return static_cast<int32_t>(offsetof(Objective_t1269591627, ___ProgressBar_8)); }
	inline Image_t2670269651 * get_ProgressBar_8() const { return ___ProgressBar_8; }
	inline Image_t2670269651 ** get_address_of_ProgressBar_8() { return &___ProgressBar_8; }
	inline void set_ProgressBar_8(Image_t2670269651 * value)
	{
		___ProgressBar_8 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressBar_8), value);
	}

	inline static int32_t get_offset_of_IsHeroGoal_9() { return static_cast<int32_t>(offsetof(Objective_t1269591627, ___IsHeroGoal_9)); }
	inline bool get_IsHeroGoal_9() const { return ___IsHeroGoal_9; }
	inline bool* get_address_of_IsHeroGoal_9() { return &___IsHeroGoal_9; }
	inline void set_IsHeroGoal_9(bool value)
	{
		___IsHeroGoal_9 = value;
	}

	inline static int32_t get_offset_of_AudioName_10() { return static_cast<int32_t>(offsetof(Objective_t1269591627, ___AudioName_10)); }
	inline String_t* get_AudioName_10() const { return ___AudioName_10; }
	inline String_t** get_address_of_AudioName_10() { return &___AudioName_10; }
	inline void set_AudioName_10(String_t* value)
	{
		___AudioName_10 = value;
		Il2CppCodeGenWriteBarrier((&___AudioName_10), value);
	}

	inline static int32_t get_offset_of_isDebugLog_11() { return static_cast<int32_t>(offsetof(Objective_t1269591627, ___isDebugLog_11)); }
	inline bool get_isDebugLog_11() const { return ___isDebugLog_11; }
	inline bool* get_address_of_isDebugLog_11() { return &___isDebugLog_11; }
	inline void set_isDebugLog_11(bool value)
	{
		___isDebugLog_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVE_T1269591627_H
#ifndef NFCHIDEUI_T996897874_H
#define NFCHIDEUI_T996897874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NFCHideUI
struct  NFCHideUI_t996897874  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] NFCHideUI::UIToHide
	GameObjectU5BU5D_t3328599146* ___UIToHide_2;

public:
	inline static int32_t get_offset_of_UIToHide_2() { return static_cast<int32_t>(offsetof(NFCHideUI_t996897874, ___UIToHide_2)); }
	inline GameObjectU5BU5D_t3328599146* get_UIToHide_2() const { return ___UIToHide_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_UIToHide_2() { return &___UIToHide_2; }
	inline void set_UIToHide_2(GameObjectU5BU5D_t3328599146* value)
	{
		___UIToHide_2 = value;
		Il2CppCodeGenWriteBarrier((&___UIToHide_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NFCHIDEUI_T996897874_H
#ifndef SINGLETON_T995483262_H
#define SINGLETON_T995483262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton
struct  Singleton_t995483262  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Singleton_t995483262_StaticFields
{
public:
	// Singleton Singleton::<Instance>k__BackingField
	Singleton_t995483262 * ___U3CInstanceU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Singleton_t995483262_StaticFields, ___U3CInstanceU3Ek__BackingField_2)); }
	inline Singleton_t995483262 * get_U3CInstanceU3Ek__BackingField_2() const { return ___U3CInstanceU3Ek__BackingField_2; }
	inline Singleton_t995483262 ** get_address_of_U3CInstanceU3Ek__BackingField_2() { return &___U3CInstanceU3Ek__BackingField_2; }
	inline void set_U3CInstanceU3Ek__BackingField_2(Singleton_t995483262 * value)
	{
		___U3CInstanceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_T995483262_H
#ifndef SNAPPHOTO_T154476977_H
#define SNAPPHOTO_T154476977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SnapPhoto
struct  SnapPhoto_t154476977  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.WebCamTexture SnapPhoto::cameraTexture
	WebCamTexture_t1514609158 * ___cameraTexture_2;
	// UnityEngine.WebCamTexture SnapPhoto::frontCam
	WebCamTexture_t1514609158 * ___frontCam_3;
	// UnityEngine.WebCamTexture SnapPhoto::backCam
	WebCamTexture_t1514609158 * ___backCam_4;
	// UnityEngine.GameObject SnapPhoto::CameraViewPlane
	GameObject_t1113636619 * ___CameraViewPlane_5;
	// GameLog SnapPhoto::GameLog
	GameLog_t2697247111 * ___GameLog_6;
	// System.Boolean SnapPhoto::cameraNativeSizeSet
	bool ___cameraNativeSizeSet_7;

public:
	inline static int32_t get_offset_of_cameraTexture_2() { return static_cast<int32_t>(offsetof(SnapPhoto_t154476977, ___cameraTexture_2)); }
	inline WebCamTexture_t1514609158 * get_cameraTexture_2() const { return ___cameraTexture_2; }
	inline WebCamTexture_t1514609158 ** get_address_of_cameraTexture_2() { return &___cameraTexture_2; }
	inline void set_cameraTexture_2(WebCamTexture_t1514609158 * value)
	{
		___cameraTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTexture_2), value);
	}

	inline static int32_t get_offset_of_frontCam_3() { return static_cast<int32_t>(offsetof(SnapPhoto_t154476977, ___frontCam_3)); }
	inline WebCamTexture_t1514609158 * get_frontCam_3() const { return ___frontCam_3; }
	inline WebCamTexture_t1514609158 ** get_address_of_frontCam_3() { return &___frontCam_3; }
	inline void set_frontCam_3(WebCamTexture_t1514609158 * value)
	{
		___frontCam_3 = value;
		Il2CppCodeGenWriteBarrier((&___frontCam_3), value);
	}

	inline static int32_t get_offset_of_backCam_4() { return static_cast<int32_t>(offsetof(SnapPhoto_t154476977, ___backCam_4)); }
	inline WebCamTexture_t1514609158 * get_backCam_4() const { return ___backCam_4; }
	inline WebCamTexture_t1514609158 ** get_address_of_backCam_4() { return &___backCam_4; }
	inline void set_backCam_4(WebCamTexture_t1514609158 * value)
	{
		___backCam_4 = value;
		Il2CppCodeGenWriteBarrier((&___backCam_4), value);
	}

	inline static int32_t get_offset_of_CameraViewPlane_5() { return static_cast<int32_t>(offsetof(SnapPhoto_t154476977, ___CameraViewPlane_5)); }
	inline GameObject_t1113636619 * get_CameraViewPlane_5() const { return ___CameraViewPlane_5; }
	inline GameObject_t1113636619 ** get_address_of_CameraViewPlane_5() { return &___CameraViewPlane_5; }
	inline void set_CameraViewPlane_5(GameObject_t1113636619 * value)
	{
		___CameraViewPlane_5 = value;
		Il2CppCodeGenWriteBarrier((&___CameraViewPlane_5), value);
	}

	inline static int32_t get_offset_of_GameLog_6() { return static_cast<int32_t>(offsetof(SnapPhoto_t154476977, ___GameLog_6)); }
	inline GameLog_t2697247111 * get_GameLog_6() const { return ___GameLog_6; }
	inline GameLog_t2697247111 ** get_address_of_GameLog_6() { return &___GameLog_6; }
	inline void set_GameLog_6(GameLog_t2697247111 * value)
	{
		___GameLog_6 = value;
		Il2CppCodeGenWriteBarrier((&___GameLog_6), value);
	}

	inline static int32_t get_offset_of_cameraNativeSizeSet_7() { return static_cast<int32_t>(offsetof(SnapPhoto_t154476977, ___cameraNativeSizeSet_7)); }
	inline bool get_cameraNativeSizeSet_7() const { return ___cameraNativeSizeSet_7; }
	inline bool* get_address_of_cameraNativeSizeSet_7() { return &___cameraNativeSizeSet_7; }
	inline void set_cameraNativeSizeSet_7(bool value)
	{
		___cameraNativeSizeSet_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNAPPHOTO_T154476977_H
#ifndef NFC_T118729767_H
#define NFC_T118729767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NFC
struct  NFC_t118729767  : public MonoBehaviour_t3962482529
{
public:
	// NFC/NFCMode NFC::Mode
	int32_t ___Mode_2;
	// UnityEngine.UI.Text NFC::ScanText
	Text_t1901882714 * ___ScanText_3;
	// UnityEngine.UI.Text NFC::OutputText
	Text_t1901882714 * ___OutputText_4;
	// UnityEngine.UI.InputField NFC::InputText
	InputField_t3762917431 * ___InputText_5;
	// UnityEngine.GameObject[] NFC::AfterScanButtons
	GameObjectU5BU5D_t3328599146* ___AfterScanButtons_6;
	// System.Boolean NFC::tagFound
	bool ___tagFound_7;
	// UnityEngine.AndroidJavaClass NFC::nfcPluginClass
	AndroidJavaClass_t32045322 * ___nfcPluginClass_8;
	// UnityEngine.AndroidJavaObject NFC::currentActivity
	AndroidJavaObject_t4131667876 * ___currentActivity_9;
	// UnityEngine.AndroidJavaObject NFC::pendingIntent
	AndroidJavaObject_t4131667876 * ___pendingIntent_10;
	// UnityEngine.AndroidJavaObject NFC::currentIntent
	AndroidJavaObject_t4131667876 * ___currentIntent_11;
	// System.String NFC::currentAction
	String_t* ___currentAction_12;
	// LoadScene NFC::loadScene
	LoadScene_t3470671713 * ___loadScene_13;
	// System.String NFC::payload
	String_t* ___payload_14;
	// System.String NFC::sceneName
	String_t* ___sceneName_18;
	// System.Boolean NFC::isDebugLog
	bool ___isDebugLog_19;

public:
	inline static int32_t get_offset_of_Mode_2() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___Mode_2)); }
	inline int32_t get_Mode_2() const { return ___Mode_2; }
	inline int32_t* get_address_of_Mode_2() { return &___Mode_2; }
	inline void set_Mode_2(int32_t value)
	{
		___Mode_2 = value;
	}

	inline static int32_t get_offset_of_ScanText_3() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___ScanText_3)); }
	inline Text_t1901882714 * get_ScanText_3() const { return ___ScanText_3; }
	inline Text_t1901882714 ** get_address_of_ScanText_3() { return &___ScanText_3; }
	inline void set_ScanText_3(Text_t1901882714 * value)
	{
		___ScanText_3 = value;
		Il2CppCodeGenWriteBarrier((&___ScanText_3), value);
	}

	inline static int32_t get_offset_of_OutputText_4() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___OutputText_4)); }
	inline Text_t1901882714 * get_OutputText_4() const { return ___OutputText_4; }
	inline Text_t1901882714 ** get_address_of_OutputText_4() { return &___OutputText_4; }
	inline void set_OutputText_4(Text_t1901882714 * value)
	{
		___OutputText_4 = value;
		Il2CppCodeGenWriteBarrier((&___OutputText_4), value);
	}

	inline static int32_t get_offset_of_InputText_5() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___InputText_5)); }
	inline InputField_t3762917431 * get_InputText_5() const { return ___InputText_5; }
	inline InputField_t3762917431 ** get_address_of_InputText_5() { return &___InputText_5; }
	inline void set_InputText_5(InputField_t3762917431 * value)
	{
		___InputText_5 = value;
		Il2CppCodeGenWriteBarrier((&___InputText_5), value);
	}

	inline static int32_t get_offset_of_AfterScanButtons_6() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___AfterScanButtons_6)); }
	inline GameObjectU5BU5D_t3328599146* get_AfterScanButtons_6() const { return ___AfterScanButtons_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_AfterScanButtons_6() { return &___AfterScanButtons_6; }
	inline void set_AfterScanButtons_6(GameObjectU5BU5D_t3328599146* value)
	{
		___AfterScanButtons_6 = value;
		Il2CppCodeGenWriteBarrier((&___AfterScanButtons_6), value);
	}

	inline static int32_t get_offset_of_tagFound_7() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___tagFound_7)); }
	inline bool get_tagFound_7() const { return ___tagFound_7; }
	inline bool* get_address_of_tagFound_7() { return &___tagFound_7; }
	inline void set_tagFound_7(bool value)
	{
		___tagFound_7 = value;
	}

	inline static int32_t get_offset_of_nfcPluginClass_8() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___nfcPluginClass_8)); }
	inline AndroidJavaClass_t32045322 * get_nfcPluginClass_8() const { return ___nfcPluginClass_8; }
	inline AndroidJavaClass_t32045322 ** get_address_of_nfcPluginClass_8() { return &___nfcPluginClass_8; }
	inline void set_nfcPluginClass_8(AndroidJavaClass_t32045322 * value)
	{
		___nfcPluginClass_8 = value;
		Il2CppCodeGenWriteBarrier((&___nfcPluginClass_8), value);
	}

	inline static int32_t get_offset_of_currentActivity_9() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___currentActivity_9)); }
	inline AndroidJavaObject_t4131667876 * get_currentActivity_9() const { return ___currentActivity_9; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_currentActivity_9() { return &___currentActivity_9; }
	inline void set_currentActivity_9(AndroidJavaObject_t4131667876 * value)
	{
		___currentActivity_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentActivity_9), value);
	}

	inline static int32_t get_offset_of_pendingIntent_10() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___pendingIntent_10)); }
	inline AndroidJavaObject_t4131667876 * get_pendingIntent_10() const { return ___pendingIntent_10; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_pendingIntent_10() { return &___pendingIntent_10; }
	inline void set_pendingIntent_10(AndroidJavaObject_t4131667876 * value)
	{
		___pendingIntent_10 = value;
		Il2CppCodeGenWriteBarrier((&___pendingIntent_10), value);
	}

	inline static int32_t get_offset_of_currentIntent_11() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___currentIntent_11)); }
	inline AndroidJavaObject_t4131667876 * get_currentIntent_11() const { return ___currentIntent_11; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_currentIntent_11() { return &___currentIntent_11; }
	inline void set_currentIntent_11(AndroidJavaObject_t4131667876 * value)
	{
		___currentIntent_11 = value;
		Il2CppCodeGenWriteBarrier((&___currentIntent_11), value);
	}

	inline static int32_t get_offset_of_currentAction_12() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___currentAction_12)); }
	inline String_t* get_currentAction_12() const { return ___currentAction_12; }
	inline String_t** get_address_of_currentAction_12() { return &___currentAction_12; }
	inline void set_currentAction_12(String_t* value)
	{
		___currentAction_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentAction_12), value);
	}

	inline static int32_t get_offset_of_loadScene_13() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___loadScene_13)); }
	inline LoadScene_t3470671713 * get_loadScene_13() const { return ___loadScene_13; }
	inline LoadScene_t3470671713 ** get_address_of_loadScene_13() { return &___loadScene_13; }
	inline void set_loadScene_13(LoadScene_t3470671713 * value)
	{
		___loadScene_13 = value;
		Il2CppCodeGenWriteBarrier((&___loadScene_13), value);
	}

	inline static int32_t get_offset_of_payload_14() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___payload_14)); }
	inline String_t* get_payload_14() const { return ___payload_14; }
	inline String_t** get_address_of_payload_14() { return &___payload_14; }
	inline void set_payload_14(String_t* value)
	{
		___payload_14 = value;
		Il2CppCodeGenWriteBarrier((&___payload_14), value);
	}

	inline static int32_t get_offset_of_sceneName_18() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___sceneName_18)); }
	inline String_t* get_sceneName_18() const { return ___sceneName_18; }
	inline String_t** get_address_of_sceneName_18() { return &___sceneName_18; }
	inline void set_sceneName_18(String_t* value)
	{
		___sceneName_18 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_18), value);
	}

	inline static int32_t get_offset_of_isDebugLog_19() { return static_cast<int32_t>(offsetof(NFC_t118729767, ___isDebugLog_19)); }
	inline bool get_isDebugLog_19() const { return ___isDebugLog_19; }
	inline bool* get_address_of_isDebugLog_19() { return &___isDebugLog_19; }
	inline void set_isDebugLog_19(bool value)
	{
		___isDebugLog_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NFC_T118729767_H
#ifndef TEXTANIMATION_T2694049294_H
#define TEXTANIMATION_T2694049294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextAnimation
struct  TextAnimation_t2694049294  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator TextAnimation::textAnimator
	Animator_t434523843 * ___textAnimator_2;

public:
	inline static int32_t get_offset_of_textAnimator_2() { return static_cast<int32_t>(offsetof(TextAnimation_t2694049294, ___textAnimator_2)); }
	inline Animator_t434523843 * get_textAnimator_2() const { return ___textAnimator_2; }
	inline Animator_t434523843 ** get_address_of_textAnimator_2() { return &___textAnimator_2; }
	inline void set_textAnimator_2(Animator_t434523843 * value)
	{
		___textAnimator_2 = value;
		Il2CppCodeGenWriteBarrier((&___textAnimator_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANIMATION_T2694049294_H
#ifndef VIEWABLEUI_T3820486676_H
#define VIEWABLEUI_T3820486676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViewableUI
struct  ViewableUI_t3820486676  : public MonoBehaviour_t3962482529
{
public:
	// ZombieTurn ViewableUI::ZombieTurn
	ZombieTurn_t3562480803 * ___ZombieTurn_2;
	// UnityEngine.GameObject ViewableUI::FightHeroKilledButton
	GameObject_t1113636619 * ___FightHeroKilledButton_3;
	// UnityEngine.GameObject ViewableUI::FightZombieKilledButton
	GameObject_t1113636619 * ___FightZombieKilledButton_4;
	// UnityEngine.GameObject ViewableUI::StartFightButton
	GameObject_t1113636619 * ___StartFightButton_5;
	// UnityEngine.GameObject ViewableUI::FightZombieHero
	GameObject_t1113636619 * ___FightZombieHero_6;
	// UnityEngine.GameObject ViewableUI::KillZombieButton
	GameObject_t1113636619 * ___KillZombieButton_7;
	// UnityEngine.GameObject ViewableUI::DebugDrawButton
	GameObject_t1113636619 * ___DebugDrawButton_8;
	// UnityEngine.GameObject ViewableUI::DebugCardDropdown
	GameObject_t1113636619 * ___DebugCardDropdown_9;
	// UnityEngine.GameObject ViewableUI::DebugToolsButton
	GameObject_t1113636619 * ___DebugToolsButton_10;
	// System.Boolean ViewableUI::isZombiesOnBoard
	bool ___isZombiesOnBoard_11;
	// System.Boolean ViewableUI::isZombiesKillable
	bool ___isZombiesKillable_12;
	// System.Boolean ViewableUI::isZombieHeroOnBoard
	bool ___isZombieHeroOnBoard_13;
	// System.Boolean ViewableUI::isDebugLog
	bool ___isDebugLog_14;

public:
	inline static int32_t get_offset_of_ZombieTurn_2() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___ZombieTurn_2)); }
	inline ZombieTurn_t3562480803 * get_ZombieTurn_2() const { return ___ZombieTurn_2; }
	inline ZombieTurn_t3562480803 ** get_address_of_ZombieTurn_2() { return &___ZombieTurn_2; }
	inline void set_ZombieTurn_2(ZombieTurn_t3562480803 * value)
	{
		___ZombieTurn_2 = value;
		Il2CppCodeGenWriteBarrier((&___ZombieTurn_2), value);
	}

	inline static int32_t get_offset_of_FightHeroKilledButton_3() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___FightHeroKilledButton_3)); }
	inline GameObject_t1113636619 * get_FightHeroKilledButton_3() const { return ___FightHeroKilledButton_3; }
	inline GameObject_t1113636619 ** get_address_of_FightHeroKilledButton_3() { return &___FightHeroKilledButton_3; }
	inline void set_FightHeroKilledButton_3(GameObject_t1113636619 * value)
	{
		___FightHeroKilledButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___FightHeroKilledButton_3), value);
	}

	inline static int32_t get_offset_of_FightZombieKilledButton_4() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___FightZombieKilledButton_4)); }
	inline GameObject_t1113636619 * get_FightZombieKilledButton_4() const { return ___FightZombieKilledButton_4; }
	inline GameObject_t1113636619 ** get_address_of_FightZombieKilledButton_4() { return &___FightZombieKilledButton_4; }
	inline void set_FightZombieKilledButton_4(GameObject_t1113636619 * value)
	{
		___FightZombieKilledButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___FightZombieKilledButton_4), value);
	}

	inline static int32_t get_offset_of_StartFightButton_5() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___StartFightButton_5)); }
	inline GameObject_t1113636619 * get_StartFightButton_5() const { return ___StartFightButton_5; }
	inline GameObject_t1113636619 ** get_address_of_StartFightButton_5() { return &___StartFightButton_5; }
	inline void set_StartFightButton_5(GameObject_t1113636619 * value)
	{
		___StartFightButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___StartFightButton_5), value);
	}

	inline static int32_t get_offset_of_FightZombieHero_6() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___FightZombieHero_6)); }
	inline GameObject_t1113636619 * get_FightZombieHero_6() const { return ___FightZombieHero_6; }
	inline GameObject_t1113636619 ** get_address_of_FightZombieHero_6() { return &___FightZombieHero_6; }
	inline void set_FightZombieHero_6(GameObject_t1113636619 * value)
	{
		___FightZombieHero_6 = value;
		Il2CppCodeGenWriteBarrier((&___FightZombieHero_6), value);
	}

	inline static int32_t get_offset_of_KillZombieButton_7() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___KillZombieButton_7)); }
	inline GameObject_t1113636619 * get_KillZombieButton_7() const { return ___KillZombieButton_7; }
	inline GameObject_t1113636619 ** get_address_of_KillZombieButton_7() { return &___KillZombieButton_7; }
	inline void set_KillZombieButton_7(GameObject_t1113636619 * value)
	{
		___KillZombieButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___KillZombieButton_7), value);
	}

	inline static int32_t get_offset_of_DebugDrawButton_8() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___DebugDrawButton_8)); }
	inline GameObject_t1113636619 * get_DebugDrawButton_8() const { return ___DebugDrawButton_8; }
	inline GameObject_t1113636619 ** get_address_of_DebugDrawButton_8() { return &___DebugDrawButton_8; }
	inline void set_DebugDrawButton_8(GameObject_t1113636619 * value)
	{
		___DebugDrawButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___DebugDrawButton_8), value);
	}

	inline static int32_t get_offset_of_DebugCardDropdown_9() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___DebugCardDropdown_9)); }
	inline GameObject_t1113636619 * get_DebugCardDropdown_9() const { return ___DebugCardDropdown_9; }
	inline GameObject_t1113636619 ** get_address_of_DebugCardDropdown_9() { return &___DebugCardDropdown_9; }
	inline void set_DebugCardDropdown_9(GameObject_t1113636619 * value)
	{
		___DebugCardDropdown_9 = value;
		Il2CppCodeGenWriteBarrier((&___DebugCardDropdown_9), value);
	}

	inline static int32_t get_offset_of_DebugToolsButton_10() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___DebugToolsButton_10)); }
	inline GameObject_t1113636619 * get_DebugToolsButton_10() const { return ___DebugToolsButton_10; }
	inline GameObject_t1113636619 ** get_address_of_DebugToolsButton_10() { return &___DebugToolsButton_10; }
	inline void set_DebugToolsButton_10(GameObject_t1113636619 * value)
	{
		___DebugToolsButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___DebugToolsButton_10), value);
	}

	inline static int32_t get_offset_of_isZombiesOnBoard_11() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___isZombiesOnBoard_11)); }
	inline bool get_isZombiesOnBoard_11() const { return ___isZombiesOnBoard_11; }
	inline bool* get_address_of_isZombiesOnBoard_11() { return &___isZombiesOnBoard_11; }
	inline void set_isZombiesOnBoard_11(bool value)
	{
		___isZombiesOnBoard_11 = value;
	}

	inline static int32_t get_offset_of_isZombiesKillable_12() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___isZombiesKillable_12)); }
	inline bool get_isZombiesKillable_12() const { return ___isZombiesKillable_12; }
	inline bool* get_address_of_isZombiesKillable_12() { return &___isZombiesKillable_12; }
	inline void set_isZombiesKillable_12(bool value)
	{
		___isZombiesKillable_12 = value;
	}

	inline static int32_t get_offset_of_isZombieHeroOnBoard_13() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___isZombieHeroOnBoard_13)); }
	inline bool get_isZombieHeroOnBoard_13() const { return ___isZombieHeroOnBoard_13; }
	inline bool* get_address_of_isZombieHeroOnBoard_13() { return &___isZombieHeroOnBoard_13; }
	inline void set_isZombieHeroOnBoard_13(bool value)
	{
		___isZombieHeroOnBoard_13 = value;
	}

	inline static int32_t get_offset_of_isDebugLog_14() { return static_cast<int32_t>(offsetof(ViewableUI_t3820486676, ___isDebugLog_14)); }
	inline bool get_isDebugLog_14() const { return ___isDebugLog_14; }
	inline bool* get_address_of_isDebugLog_14() { return &___isDebugLog_14; }
	inline void set_isDebugLog_14(bool value)
	{
		___isDebugLog_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWABLEUI_T3820486676_H
#ifndef PLACEMAPIMAGES_T1484071588_H
#define PLACEMAPIMAGES_T1484071588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaceMapImages
struct  PlaceMapImages_t1484071588  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite[] PlaceMapImages::MapImages
	SpriteU5BU5D_t2581906349* ___MapImages_2;
	// UnityEngine.SpriteRenderer[] PlaceMapImages::SpriteRenderers
	SpriteRendererU5BU5D_t911335936* ___SpriteRenderers_3;
	// System.Boolean PlaceMapImages::isDebugLog
	bool ___isDebugLog_4;

public:
	inline static int32_t get_offset_of_MapImages_2() { return static_cast<int32_t>(offsetof(PlaceMapImages_t1484071588, ___MapImages_2)); }
	inline SpriteU5BU5D_t2581906349* get_MapImages_2() const { return ___MapImages_2; }
	inline SpriteU5BU5D_t2581906349** get_address_of_MapImages_2() { return &___MapImages_2; }
	inline void set_MapImages_2(SpriteU5BU5D_t2581906349* value)
	{
		___MapImages_2 = value;
		Il2CppCodeGenWriteBarrier((&___MapImages_2), value);
	}

	inline static int32_t get_offset_of_SpriteRenderers_3() { return static_cast<int32_t>(offsetof(PlaceMapImages_t1484071588, ___SpriteRenderers_3)); }
	inline SpriteRendererU5BU5D_t911335936* get_SpriteRenderers_3() const { return ___SpriteRenderers_3; }
	inline SpriteRendererU5BU5D_t911335936** get_address_of_SpriteRenderers_3() { return &___SpriteRenderers_3; }
	inline void set_SpriteRenderers_3(SpriteRendererU5BU5D_t911335936* value)
	{
		___SpriteRenderers_3 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteRenderers_3), value);
	}

	inline static int32_t get_offset_of_isDebugLog_4() { return static_cast<int32_t>(offsetof(PlaceMapImages_t1484071588, ___isDebugLog_4)); }
	inline bool get_isDebugLog_4() const { return ___isDebugLog_4; }
	inline bool* get_address_of_isDebugLog_4() { return &___isDebugLog_4; }
	inline void set_isDebugLog_4(bool value)
	{
		___isDebugLog_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMAPIMAGES_T1484071588_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (Building_t1411255995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[11] = 
{
	Building_t1411255995::get_offset_of_name_0(),
	Building_t1411255995::get_offset_of_rollRange_1(),
	Building_t1411255995::get_offset_of_pickUpItem_2(),
	Building_t1411255995::get_offset_of_spaces_3(),
	Building_t1411255995::get_offset_of_numDoors_4(),
	Building_t1411255995::get_offset_of_numLockedDoors_5(),
	Building_t1411255995::get_offset_of_matColorCode_6(),
	Building_t1411255995::get_offset_of_U3CHasSpawningPitU3Ek__BackingField_7(),
	Building_t1411255995::get_offset_of_U3CIsTakenOverU3Ek__BackingField_8(),
	Building_t1411255995::get_offset_of_U3CHasLightsOutU3Ek__BackingField_9(),
	Building_t1411255995::get_offset_of_U3CIsDestroyedU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (ColorChannel_t1119199196)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2601[4] = 
{
	ColorChannel_t1119199196::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (MapTile_t3503354235), -1, sizeof(MapTile_t3503354235_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2602[4] = 
{
	MapTile_t3503354235::get_offset_of_BuildingsOnTile_0(),
	MapTile_t3503354235::get_offset_of_Name_1(),
	MapTile_t3503354235_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	MapTile_t3503354235_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (Tiles_t1457048987), -1, sizeof(Tiles_t1457048987_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2603[10] = 
{
	Tiles_t1457048987::get_offset_of_coreMapTiles_0(),
	Tiles_t1457048987::get_offset_of_growingHungerMapTiles_1(),
	Tiles_t1457048987::get_offset_of_fourRandomTilesForGame_2(),
	Tiles_t1457048987_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	Tiles_t1457048987_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	Tiles_t1457048987_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	Tiles_t1457048987_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	Tiles_t1457048987_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	Tiles_t1457048987_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
	Tiles_t1457048987_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (BankJunkyardGas_t3888625882), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (BarnFarmhouse_t415986967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (ChurchGraveyardPolice_t3650776148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (GeneralGunPlant_t3644479267), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (HangarHospitalDiner_t1147268456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (SchoolGym_t119813715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (AntiqueFactoryLibrary_t896367873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (DrugPostOfficeSupermarket_t367081682), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (BowlingTavern_t1342164777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (RefineryTrain_t2111168844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (BookGymHighSchool_t4263581806), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (DinerPowerStation_t520786256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (LumberOfficeSaw_t4060998708), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (HardwareHospitalPolice_t1260422496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (ThrasherMill_t3449986375), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (OldCabin_t4142939193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (TowerRelay_t1797386082), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (RangerStation_t488444359), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (Missions_t3398870215), -1, sizeof(Missions_t3398870215_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2622[12] = 
{
	Missions_t3398870215::get_offset_of_activeMission_0(),
	Missions_t3398870215::get_offset_of_coreMissions_1(),
	Missions_t3398870215::get_offset_of_growingHungerMissions_2(),
	Missions_t3398870215::get_offset_of_heroPackOneMissions_3(),
	Missions_t3398870215::get_offset_of_timberPeakMissions_4(),
	Missions_t3398870215::get_offset_of_bloodInTheForestMissions_5(),
	Missions_t3398870215::get_offset_of_survivalOfTheFittestMissions_6(),
	Missions_t3398870215::get_offset_of_graveDeadMissions_7(),
	Missions_t3398870215::get_offset_of_supplementMissions_8(),
	Missions_t3398870215::get_offset_of_webMissions_9(),
	Missions_t3398870215::get_offset_of_customMissions_10(),
	Missions_t3398870215_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (Mission_t4233471175), -1, sizeof(Mission_t4233471175_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2623[40] = 
{
	Mission_t4233471175::get_offset_of_BurnDownManor_0(),
	Mission_t4233471175::get_offset_of_Destroy3ZombiePits_1(),
	Mission_t4233471175::get_offset_of_EscapeTruckButton_2(),
	Mission_t4233471175::get_offset_of_Kill7PlagueCarriersButton_3(),
	Mission_t4233471175::get_offset_of_Kill15Zombies_4(),
	Mission_t4233471175::get_offset_of_Save4TownfolkButton_5(),
	Mission_t4233471175_StaticFields::get_offset_of_Destroy6Buildings_6(),
	Mission_t4233471175::get_offset_of_Kill2Heroes_7(),
	Mission_t4233471175::get_offset_of_Kill4Heroes_8(),
	Mission_t4233471175::get_offset_of_ZombiesInHouse9_9(),
	Mission_t4233471175::get_offset_of_HuntForSurvivors_10(),
	Mission_t4233471175::get_offset_of_FindZombieSkull_11(),
	Mission_t4233471175::get_offset_of_GetSkullToDoctor_12(),
	Mission_t4233471175::get_offset_of_KillDoctorZombie_13(),
	Mission_t4233471175_StaticFields::get_offset_of_FreeSearchMarkers_14(),
	Mission_t4233471175_StaticFields::get_offset_of_HeroesReplenish_15(),
	Mission_t4233471175_StaticFields::get_offset_of_HeroStartingCards1_16(),
	Mission_t4233471175_StaticFields::get_offset_of_HeroStartingCards2_17(),
	Mission_t4233471175_StaticFields::get_offset_of_PlagueCarriers_18(),
	Mission_t4233471175_StaticFields::get_offset_of_WellStockedBuilding_19(),
	Mission_t4233471175_StaticFields::get_offset_of_ZombieAutoSpawn_20(),
	Mission_t4233471175_StaticFields::get_offset_of_ZombieGraveDead_21(),
	Mission_t4233471175_StaticFields::get_offset_of_ZombieHorde21_22(),
	Mission_t4233471175::get_offset_of_Name_23(),
	Mission_t4233471175::get_offset_of_Text_24(),
	Mission_t4233471175::get_offset_of_StartNarrativeText_25(),
	Mission_t4233471175::get_offset_of_NarrativeText_26(),
	Mission_t4233471175::get_offset_of_EndNarrativeText_27(),
	Mission_t4233471175::get_offset_of_Rounds_28(),
	Mission_t4233471175::get_offset_of_HeroObjective_29(),
	Mission_t4233471175::get_offset_of_ZombieObjective_30(),
	Mission_t4233471175::get_offset_of_ScenarioSearchItems_31(),
	Mission_t4233471175::get_offset_of_SpecialRules_32(),
	Mission_t4233471175::get_offset_of_MissionMovementTarget_33(),
	Mission_t4233471175::get_offset_of_CenterTile_34(),
	Mission_t4233471175::get_offset_of_AllHeroStartLocationChanged_35(),
	Mission_t4233471175::get_offset_of_CenterHeroStartLocationChanged_36(),
	Mission_t4233471175::get_offset_of_HeroStartLocation_37(),
	Mission_t4233471175::get_offset_of_AudioWin_38(),
	Mission_t4233471175::get_offset_of_AudioLose_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (ObjectiveTypes_t2716872159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[4] = 
{
	ObjectiveTypes_t2716872159::get_offset_of_Name_0(),
	ObjectiveTypes_t2716872159::get_offset_of_Value_1(),
	ObjectiveTypes_t2716872159::get_offset_of_Button_2(),
	ObjectiveTypes_t2716872159::get_offset_of_Audio_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (ScenarioSearchItemTypes_t1972627041)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2625[6] = 
{
	ScenarioSearchItemTypes_t1972627041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (SpecialRulesTypes_t2275317327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[3] = 
{
	SpecialRulesTypes_t2275317327::get_offset_of_Name_0(),
	SpecialRulesTypes_t2275317327::get_offset_of_SetupText_1(),
	SpecialRulesTypes_t2275317327::get_offset_of_PlayerPrefKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (TutorialMission_t2826207972), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (BurnEmOut_t307227638), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (DefendTheManorHouse_t4176879821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (DieZombiesDie_t3864401308), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (EscapeInTheTruck_t1670568109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (SaveTheTownsFolk_t4109486956), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (BurnItToTheGround_t648940805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (PlagueCarriers_t2860829270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (ZombieApocalypse_t1518918614), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (HuntForSurvivors_t2859470428), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (HunkerDown_t2613713738), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (RescueMission_t3975397388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (SearchForTheTruth_t1265244687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (SupplyRun_t1899586975), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (BlowUpTheTown_t1662253320), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (LearnToSurvive_t2546588854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (MountainOfTheDead_t2984684957), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (RadioForHelp_t586490126), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (Airstrike_t1314356644), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (EscapeInThePlane_t937804966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (HoldTheLine_t1394975525), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (LostInTheWoods_t1105034517), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (SalvageMission_t1351220260), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (StockUp_t3356519569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (RevengeOfTheDead_t2620849876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (ZombiePillage_t886770412), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (CampaignMission1_t881585865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (CampaignMission2_t3220238025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (CampaignMission3_t1263922889), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (NFC_t118729767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[18] = 
{
	NFC_t118729767::get_offset_of_Mode_2(),
	NFC_t118729767::get_offset_of_ScanText_3(),
	NFC_t118729767::get_offset_of_OutputText_4(),
	NFC_t118729767::get_offset_of_InputText_5(),
	NFC_t118729767::get_offset_of_AfterScanButtons_6(),
	NFC_t118729767::get_offset_of_tagFound_7(),
	NFC_t118729767::get_offset_of_nfcPluginClass_8(),
	NFC_t118729767::get_offset_of_currentActivity_9(),
	NFC_t118729767::get_offset_of_pendingIntent_10(),
	NFC_t118729767::get_offset_of_currentIntent_11(),
	NFC_t118729767::get_offset_of_currentAction_12(),
	NFC_t118729767::get_offset_of_loadScene_13(),
	NFC_t118729767::get_offset_of_payload_14(),
	0,
	0,
	0,
	NFC_t118729767::get_offset_of_sceneName_18(),
	NFC_t118729767::get_offset_of_isDebugLog_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (NFCMode_t1366099354)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2657[3] = 
{
	NFCMode_t1366099354::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (NFCHideUI_t996897874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[1] = 
{
	NFCHideUI_t996897874::get_offset_of_UIToHide_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (Normalization_t2844905348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (Objective_t1269591627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[10] = 
{
	Objective_t1269591627::get_offset_of_Name_2(),
	Objective_t1269591627::get_offset_of_CurrentProgress_3(),
	Objective_t1269591627::get_offset_of_Goal_4(),
	Objective_t1269591627::get_offset_of_zombieTurnManager_5(),
	Objective_t1269591627::get_offset_of_ObjectiveTitle_6(),
	Objective_t1269591627::get_offset_of_CurrentProgressText_7(),
	Objective_t1269591627::get_offset_of_ProgressBar_8(),
	Objective_t1269591627::get_offset_of_IsHeroGoal_9(),
	Objective_t1269591627::get_offset_of_AudioName_10(),
	Objective_t1269591627::get_offset_of_isDebugLog_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (PlaceMapImages_t1484071588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[3] = 
{
	PlaceMapImages_t1484071588::get_offset_of_MapImages_2(),
	PlaceMapImages_t1484071588::get_offset_of_SpriteRenderers_3(),
	PlaceMapImages_t1484071588::get_offset_of_isDebugLog_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (RadialBasisFunctionNetwork_t3778331090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[7] = 
{
	RadialBasisFunctionNetwork_t3778331090::get_offset_of_numInputs_0(),
	RadialBasisFunctionNetwork_t3778331090::get_offset_of_numRadialBasisFunctions_1(),
	RadialBasisFunctionNetwork_t3778331090::get_offset_of_numOutputs_2(),
	RadialBasisFunctionNetwork_t3778331090::get_offset_of_inputs_3(),
	RadialBasisFunctionNetwork_t3778331090::get_offset_of_rbfNodes_4(),
	RadialBasisFunctionNetwork_t3778331090::get_offset_of_outputBiases_5(),
	RadialBasisFunctionNetwork_t3778331090::get_offset_of_outputs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (ClusterPoint_t3639481456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[2] = 
{
	ClusterPoint_t3639481456::get_offset_of_Value_0(),
	ClusterPoint_t3639481456::get_offset_of_AssignedCluster_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (ClusterCenter_t787658505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[3] = 
{
	ClusterCenter_t787658505::get_offset_of_ClusterId_0(),
	ClusterCenter_t787658505::get_offset_of_Value_1(),
	ClusterCenter_t787658505::get_offset_of_Width_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (RBFNode_t3207576834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[4] = 
{
	RBFNode_t3207576834::get_offset_of_Centers_0(),
	RBFNode_t3207576834::get_offset_of_ToOutputWeights_1(),
	RBFNode_t3207576834::get_offset_of_FunctionOut_2(),
	RBFNode_t3207576834::get_offset_of_Width_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (U3CSoftmaxU3Ec__AnonStorey0_t2479826977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[1] = 
{
	U3CSoftmaxU3Ec__AnonStorey0_t2479826977::get_offset_of_max_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (GaussianFunction_t1720587675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (RoundData_t2030032500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[8] = 
{
	RoundData_t2030032500::get_offset_of_heroes_0(),
	RoundData_t2030032500::get_offset_of_roundsLeft_1(),
	RoundData_t2030032500::get_offset_of_zombiesDead_2(),
	RoundData_t2030032500::get_offset_of_zombiesOnBoard_3(),
	RoundData_t2030032500::get_offset_of_heroesDead_4(),
	RoundData_t2030032500::get_offset_of_buildingsTakenOver_5(),
	RoundData_t2030032500::get_offset_of_buildingsLightsOut_6(),
	RoundData_t2030032500::get_offset_of_spawningPits_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (Singleton_t995483262), -1, sizeof(Singleton_t995483262_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2669[1] = 
{
	Singleton_t995483262_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (SnapPhoto_t154476977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[6] = 
{
	SnapPhoto_t154476977::get_offset_of_cameraTexture_2(),
	SnapPhoto_t154476977::get_offset_of_frontCam_3(),
	SnapPhoto_t154476977::get_offset_of_backCam_4(),
	SnapPhoto_t154476977::get_offset_of_CameraViewPlane_5(),
	SnapPhoto_t154476977::get_offset_of_GameLog_6(),
	SnapPhoto_t154476977::get_offset_of_cameraNativeSizeSet_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (Strings_t3430161606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (AudioMixerKeys_t1124916762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (CampaignSave_t1186366797), -1, sizeof(CampaignSave_t1186366797_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2673[7] = 
{
	0,
	0,
	0,
	0,
	CampaignSave_t1186366797_StaticFields::get_offset_of_AllSaveFiles_4(),
	CampaignSave_t1186366797_StaticFields::get_offset_of_Folder_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (GameObjectNames_t1011435887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (BoardPositions_t2766534255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (HeroNames_t922592968), -1, sizeof(HeroNames_t922592968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2676[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	HeroNames_t922592968_StaticFields::get_offset_of_AllHeroes_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (PlayerPrefKeys_t1747914545), -1, sizeof(PlayerPrefKeys_t1747914545_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2677[34] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	PlayerPrefKeys_t1747914545_StaticFields::get_offset_of_Expansions_17(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	PlayerPrefKeys_t1747914545_StaticFields::get_offset_of_SpecialRules_30(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (Materials_t3845095469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (MaterialProperties_t270969053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (SceneNames_t4116278453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (Sprites_t2880089301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (TextAnimation_t2694049294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[1] = 
{
	TextAnimation_t2694049294::get_offset_of_textAnimator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (Tutorial_t2275885037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (ViewableUI_t3820486676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[13] = 
{
	ViewableUI_t3820486676::get_offset_of_ZombieTurn_2(),
	ViewableUI_t3820486676::get_offset_of_FightHeroKilledButton_3(),
	ViewableUI_t3820486676::get_offset_of_FightZombieKilledButton_4(),
	ViewableUI_t3820486676::get_offset_of_StartFightButton_5(),
	ViewableUI_t3820486676::get_offset_of_FightZombieHero_6(),
	ViewableUI_t3820486676::get_offset_of_KillZombieButton_7(),
	ViewableUI_t3820486676::get_offset_of_DebugDrawButton_8(),
	ViewableUI_t3820486676::get_offset_of_DebugCardDropdown_9(),
	ViewableUI_t3820486676::get_offset_of_DebugToolsButton_10(),
	ViewableUI_t3820486676::get_offset_of_isZombiesOnBoard_11(),
	ViewableUI_t3820486676::get_offset_of_isZombiesKillable_12(),
	ViewableUI_t3820486676::get_offset_of_isZombieHeroOnBoard_13(),
	ViewableUI_t3820486676::get_offset_of_isDebugLog_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (VoiceCommand_t286642266), -1, sizeof(VoiceCommand_t286642266_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2685[11] = 
{
	VoiceCommand_t286642266::get_offset_of_voiceManager_2(),
	VoiceCommand_t286642266::get_offset_of_isListening_3(),
	VoiceCommand_t286642266::get_offset_of_message_4(),
	VoiceCommand_t286642266::get_offset_of_loadScene_5(),
	VoiceCommand_t286642266::get_offset_of_zombieTurn_6(),
	VoiceCommand_t286642266::get_offset_of_fight_7(),
	VoiceCommand_t286642266::get_offset_of_remains_8(),
	VoiceCommand_t286642266::get_offset_of_text_9(),
	VoiceCommand_t286642266::get_offset_of_voicePanel_10(),
	VoiceCommand_t286642266::get_offset_of_Mixer_11(),
	VoiceCommand_t286642266_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (Zombies_t3905046179), -1, sizeof(Zombies_t3905046179_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2686[15] = 
{
	Zombies_t3905046179::get_offset_of_zombieTypes_0(),
	Zombies_t3905046179::get_offset_of_zombiesKilledCount_1(),
	Zombies_t3905046179::get_offset_of_zombieTurn_2(),
	Zombies_t3905046179::get_offset_of_zombiesCanCanBeKilled_3(),
	Zombies_t3905046179::get_offset_of_tempFightDice_4(),
	Zombies_t3905046179::get_offset_of_bonusFightDice_5(),
	Zombies_t3905046179::get_offset_of_extraRoundDice_6(),
	Zombies_t3905046179::get_offset_of_multipleFightCards_7(),
	Zombies_t3905046179::get_offset_of_ZombiesOnBoardChanged_8(),
	Zombies_t3905046179::get_offset_of_ZombiesAreKillableChanged_9(),
	Zombies_t3905046179::get_offset_of_HeroZombieOnBoardChanged_10(),
	Zombies_t3905046179_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
	Zombies_t3905046179_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_12(),
	Zombies_t3905046179_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_13(),
	Zombies_t3905046179_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (ZombiesOnBoard_t3526012930), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (ZombiesAreKillable_t2501657832), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (HeroZombieOnBoard_t1308641688), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (U3CGetZombieOfTypeU3Ec__AnonStorey0_t1243602918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[1] = 
{
	U3CGetZombieOfTypeU3Ec__AnonStorey0_t1243602918::get_offset_of_zombieType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (U3CGetZombieU3Ec__AnonStorey1_t4035454636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[1] = 
{
	U3CGetZombieU3Ec__AnonStorey1_t4035454636::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (Zombie_t2596553718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[16] = 
{
	Zombie_t2596553718::get_offset_of_name_0(),
	Zombie_t2596553718::get_offset_of_type_1(),
	Zombie_t2596553718::get_offset_of_speed_2(),
	Zombie_t2596553718::get_offset_of_health_3(),
	Zombie_t2596553718::get_offset_of_hungerRange_4(),
	Zombie_t2596553718::get_offset_of_countTowardSpawnCheck_5(),
	Zombie_t2596553718::get_offset_of_movementTarget_6(),
	Zombie_t2596553718::get_offset_of_zombieTurn_7(),
	Zombie_t2596553718::get_offset_of_ignoreWoundOnFourPlus_8(),
	Zombie_t2596553718::get_offset_of_canSpawnWithOtherZombie_9(),
	Zombie_t2596553718::get_offset_of_makesZombieHeroOnFivePlus_10(),
	Zombie_t2596553718::get_offset_of_zombiesInPool_11(),
	Zombie_t2596553718::get_offset_of_zombiesOnBoard_12(),
	Zombie_t2596553718::get_offset_of_baseFightDice_13(),
	Zombie_t2596553718::get_offset_of_fightDice_14(),
	Zombie_t2596553718::get_offset_of_bonusDice_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (ZombieType_t2798134437)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2693[7] = 
{
	ZombieType_t2798134437::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (ZombieSpeed_t2101478944)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2694[5] = 
{
	ZombieSpeed_t2101478944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (ZombieMovementTarget_t4013730920)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2695[6] = 
{
	ZombieMovementTarget_t4013730920::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (PlayDuringPhase_t3488456769)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2696[10] = 
{
	PlayDuringPhase_t3488456769::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (ZombieCard_t2237653742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[14] = 
{
	ZombieCard_t2237653742::get_offset_of_Name_0(),
	ZombieCard_t2237653742::get_offset_of_RemainsInPlayName_1(),
	ZombieCard_t2237653742::get_offset_of_Phase_2(),
	ZombieCard_t2237653742::get_offset_of_OriginalText_3(),
	ZombieCard_t2237653742::get_offset_of_FlavorText_4(),
	ZombieCard_t2237653742::get_offset_of_RemainsInPlay_5(),
	ZombieCard_t2237653742::get_offset_of_tempCard_6(),
	ZombieCard_t2237653742::get_offset_of_SkipCard_7(),
	ZombieCard_t2237653742::get_offset_of_ZombieTurnManager_8(),
	ZombieCard_t2237653742::get_offset_of_Building_9(),
	ZombieCard_t2237653742::get_offset_of_HeroToEffect_10(),
	ZombieCard_t2237653742::get_offset_of_NewZombies_11(),
	ZombieCard_t2237653742::get_offset_of_CardRollValue_12(),
	ZombieCard_t2237653742::get_offset_of_CardToEffect_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (Cornered_t3235490063), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (LightsOut_t1738659613), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
