﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Random
struct Random_t108471755;
// System.Double[]
struct DoubleU5BU5D_t3413330114;
// System.Double[][]
struct DoubleU5BU5DU5BU5D_t1159594487;
// HeroCard
struct HeroCard_t3183847341;
// System.Collections.Generic.List`1<HeroCard>
struct List_1_t360954787;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Collections.Generic.List`1<Hero>
struct List_1_t3733427512;
// System.Func`3<System.String,Hero,System.String>
struct Func_3_t2253617649;
// Missions
struct Missions_t3398870215;
// Mission
struct Mission_t4233471175;
// Tiles
struct Tiles_t1457048987;
// Heroes
struct Heroes_t1064386291;
// Zombies
struct Zombies_t3905046179;
// RadialBasisFunctionNetwork
struct RadialBasisFunctionNetwork_t3778331090;
// UnityEngine.Events.UnityAction`3<CloudConnectorCore/QueryType,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<System.String>>
struct UnityAction_3_t1112121581;
// System.String
struct String_t;
// Heroes/<HeroesInTheGame>c__AnonStorey1
struct U3CHeroesInTheGameU3Ec__AnonStorey1_t809524509;
// Hero
struct Hero_t2261352770;
// Heroes/<RandomHeroWithKeyword>c__AnonStorey3
struct U3CRandomHeroWithKeywordU3Ec__AnonStorey3_t371716733;
// System.Collections.Generic.List`1<RoundData>
struct List_1_t3502107242;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// NeuralNetwork
struct NeuralNetwork_t3692603454;
// FeedForwardProgram
struct FeedForwardProgram_t2707572273;
// System.Collections.Generic.List`1<HeroCard/HeroCardTypes>
struct List_1_t2987524082;
// CampaignLoadScene
struct CampaignLoadScene_t453142025;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Void
struct Void_t1185182177;
// System.String[][]
struct StringU5BU5DU5BU5D_t2611993717;
// System.Collections.Generic.List`1<Mission>
struct List_1_t1410578621;
// System.Collections.Generic.List`1<Hero/KeywordType>
struct List_1_t401849560;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.List`1<GameSaveData/RemainsInPlayCardDetails>
struct List_1_t2442161526;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<GameSaveData/GameLogEntries>
struct List_1_t2151963976;
// GameData
struct GameData_t415813024;
// UnityEngine.Material
struct Material_t340375123;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3521020193;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// LoadScene
struct LoadScene_t3470671713;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// System.Func`3<System.String,System.Double,System.String>
struct Func_3_t220556092;
// UnityEngine.UI.Text
struct Text_t1901882714;
// ZombieTurn
struct ZombieTurn_t3562480803;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// CampaignManager
struct CampaignManager_t2990572144;
// UnityEngine.UI.Button
struct Button_t4055032469;
// ZombieCard
struct ZombieCard_t2237653742;
// BasicHeroCards
struct BasicHeroCards_t2570609810;
// AdvancedHeroCards
struct AdvancedHeroCards_t368746982;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef NEURALNETWORK_T3692603454_H
#define NEURALNETWORK_T3692603454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NeuralNetwork
struct  NeuralNetwork_t3692603454  : public RuntimeObject
{
public:
	// System.Int32 NeuralNetwork::numInput
	int32_t ___numInput_1;
	// System.Double[] NeuralNetwork::inputs
	DoubleU5BU5D_t3413330114* ___inputs_2;
	// System.Double[][] NeuralNetwork::inputToHiddenWeights
	DoubleU5BU5DU5BU5D_t1159594487* ___inputToHiddenWeights_3;
	// System.Double[][] NeuralNetwork::inputToHiddenPrevWeightsDelta
	DoubleU5BU5DU5BU5D_t1159594487* ___inputToHiddenPrevWeightsDelta_4;
	// System.Int32 NeuralNetwork::numHidden
	int32_t ___numHidden_5;
	// System.Double[] NeuralNetwork::hiddenBiases
	DoubleU5BU5D_t3413330114* ___hiddenBiases_6;
	// System.Double[][] NeuralNetwork::hiddenToOutputWeights
	DoubleU5BU5DU5BU5D_t1159594487* ___hiddenToOutputWeights_7;
	// System.Double[] NeuralNetwork::hiddenOutputs
	DoubleU5BU5D_t3413330114* ___hiddenOutputs_8;
	// System.Double[] NeuralNetwork::hiddenGradients
	DoubleU5BU5D_t3413330114* ___hiddenGradients_9;
	// System.Double[] NeuralNetwork::hiddenPrevBiasesDelta
	DoubleU5BU5D_t3413330114* ___hiddenPrevBiasesDelta_10;
	// System.Double[][] NeuralNetwork::hiddenToOutputPrevWeightDelta
	DoubleU5BU5DU5BU5D_t1159594487* ___hiddenToOutputPrevWeightDelta_11;
	// System.Int32 NeuralNetwork::numOutput
	int32_t ___numOutput_12;
	// System.Double[] NeuralNetwork::outputs
	DoubleU5BU5D_t3413330114* ___outputs_13;
	// System.Double[] NeuralNetwork::outputBiases
	DoubleU5BU5D_t3413330114* ___outputBiases_14;
	// System.Double[] NeuralNetwork::outputGradients
	DoubleU5BU5D_t3413330114* ___outputGradients_15;
	// System.Double[] NeuralNetwork::outputPrevBiasesDelta
	DoubleU5BU5D_t3413330114* ___outputPrevBiasesDelta_16;

public:
	inline static int32_t get_offset_of_numInput_1() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___numInput_1)); }
	inline int32_t get_numInput_1() const { return ___numInput_1; }
	inline int32_t* get_address_of_numInput_1() { return &___numInput_1; }
	inline void set_numInput_1(int32_t value)
	{
		___numInput_1 = value;
	}

	inline static int32_t get_offset_of_inputs_2() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___inputs_2)); }
	inline DoubleU5BU5D_t3413330114* get_inputs_2() const { return ___inputs_2; }
	inline DoubleU5BU5D_t3413330114** get_address_of_inputs_2() { return &___inputs_2; }
	inline void set_inputs_2(DoubleU5BU5D_t3413330114* value)
	{
		___inputs_2 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_2), value);
	}

	inline static int32_t get_offset_of_inputToHiddenWeights_3() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___inputToHiddenWeights_3)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_inputToHiddenWeights_3() const { return ___inputToHiddenWeights_3; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_inputToHiddenWeights_3() { return &___inputToHiddenWeights_3; }
	inline void set_inputToHiddenWeights_3(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___inputToHiddenWeights_3 = value;
		Il2CppCodeGenWriteBarrier((&___inputToHiddenWeights_3), value);
	}

	inline static int32_t get_offset_of_inputToHiddenPrevWeightsDelta_4() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___inputToHiddenPrevWeightsDelta_4)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_inputToHiddenPrevWeightsDelta_4() const { return ___inputToHiddenPrevWeightsDelta_4; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_inputToHiddenPrevWeightsDelta_4() { return &___inputToHiddenPrevWeightsDelta_4; }
	inline void set_inputToHiddenPrevWeightsDelta_4(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___inputToHiddenPrevWeightsDelta_4 = value;
		Il2CppCodeGenWriteBarrier((&___inputToHiddenPrevWeightsDelta_4), value);
	}

	inline static int32_t get_offset_of_numHidden_5() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___numHidden_5)); }
	inline int32_t get_numHidden_5() const { return ___numHidden_5; }
	inline int32_t* get_address_of_numHidden_5() { return &___numHidden_5; }
	inline void set_numHidden_5(int32_t value)
	{
		___numHidden_5 = value;
	}

	inline static int32_t get_offset_of_hiddenBiases_6() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___hiddenBiases_6)); }
	inline DoubleU5BU5D_t3413330114* get_hiddenBiases_6() const { return ___hiddenBiases_6; }
	inline DoubleU5BU5D_t3413330114** get_address_of_hiddenBiases_6() { return &___hiddenBiases_6; }
	inline void set_hiddenBiases_6(DoubleU5BU5D_t3413330114* value)
	{
		___hiddenBiases_6 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenBiases_6), value);
	}

	inline static int32_t get_offset_of_hiddenToOutputWeights_7() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___hiddenToOutputWeights_7)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_hiddenToOutputWeights_7() const { return ___hiddenToOutputWeights_7; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_hiddenToOutputWeights_7() { return &___hiddenToOutputWeights_7; }
	inline void set_hiddenToOutputWeights_7(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___hiddenToOutputWeights_7 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenToOutputWeights_7), value);
	}

	inline static int32_t get_offset_of_hiddenOutputs_8() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___hiddenOutputs_8)); }
	inline DoubleU5BU5D_t3413330114* get_hiddenOutputs_8() const { return ___hiddenOutputs_8; }
	inline DoubleU5BU5D_t3413330114** get_address_of_hiddenOutputs_8() { return &___hiddenOutputs_8; }
	inline void set_hiddenOutputs_8(DoubleU5BU5D_t3413330114* value)
	{
		___hiddenOutputs_8 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenOutputs_8), value);
	}

	inline static int32_t get_offset_of_hiddenGradients_9() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___hiddenGradients_9)); }
	inline DoubleU5BU5D_t3413330114* get_hiddenGradients_9() const { return ___hiddenGradients_9; }
	inline DoubleU5BU5D_t3413330114** get_address_of_hiddenGradients_9() { return &___hiddenGradients_9; }
	inline void set_hiddenGradients_9(DoubleU5BU5D_t3413330114* value)
	{
		___hiddenGradients_9 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenGradients_9), value);
	}

	inline static int32_t get_offset_of_hiddenPrevBiasesDelta_10() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___hiddenPrevBiasesDelta_10)); }
	inline DoubleU5BU5D_t3413330114* get_hiddenPrevBiasesDelta_10() const { return ___hiddenPrevBiasesDelta_10; }
	inline DoubleU5BU5D_t3413330114** get_address_of_hiddenPrevBiasesDelta_10() { return &___hiddenPrevBiasesDelta_10; }
	inline void set_hiddenPrevBiasesDelta_10(DoubleU5BU5D_t3413330114* value)
	{
		___hiddenPrevBiasesDelta_10 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenPrevBiasesDelta_10), value);
	}

	inline static int32_t get_offset_of_hiddenToOutputPrevWeightDelta_11() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___hiddenToOutputPrevWeightDelta_11)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_hiddenToOutputPrevWeightDelta_11() const { return ___hiddenToOutputPrevWeightDelta_11; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_hiddenToOutputPrevWeightDelta_11() { return &___hiddenToOutputPrevWeightDelta_11; }
	inline void set_hiddenToOutputPrevWeightDelta_11(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___hiddenToOutputPrevWeightDelta_11 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenToOutputPrevWeightDelta_11), value);
	}

	inline static int32_t get_offset_of_numOutput_12() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___numOutput_12)); }
	inline int32_t get_numOutput_12() const { return ___numOutput_12; }
	inline int32_t* get_address_of_numOutput_12() { return &___numOutput_12; }
	inline void set_numOutput_12(int32_t value)
	{
		___numOutput_12 = value;
	}

	inline static int32_t get_offset_of_outputs_13() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___outputs_13)); }
	inline DoubleU5BU5D_t3413330114* get_outputs_13() const { return ___outputs_13; }
	inline DoubleU5BU5D_t3413330114** get_address_of_outputs_13() { return &___outputs_13; }
	inline void set_outputs_13(DoubleU5BU5D_t3413330114* value)
	{
		___outputs_13 = value;
		Il2CppCodeGenWriteBarrier((&___outputs_13), value);
	}

	inline static int32_t get_offset_of_outputBiases_14() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___outputBiases_14)); }
	inline DoubleU5BU5D_t3413330114* get_outputBiases_14() const { return ___outputBiases_14; }
	inline DoubleU5BU5D_t3413330114** get_address_of_outputBiases_14() { return &___outputBiases_14; }
	inline void set_outputBiases_14(DoubleU5BU5D_t3413330114* value)
	{
		___outputBiases_14 = value;
		Il2CppCodeGenWriteBarrier((&___outputBiases_14), value);
	}

	inline static int32_t get_offset_of_outputGradients_15() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___outputGradients_15)); }
	inline DoubleU5BU5D_t3413330114* get_outputGradients_15() const { return ___outputGradients_15; }
	inline DoubleU5BU5D_t3413330114** get_address_of_outputGradients_15() { return &___outputGradients_15; }
	inline void set_outputGradients_15(DoubleU5BU5D_t3413330114* value)
	{
		___outputGradients_15 = value;
		Il2CppCodeGenWriteBarrier((&___outputGradients_15), value);
	}

	inline static int32_t get_offset_of_outputPrevBiasesDelta_16() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454, ___outputPrevBiasesDelta_16)); }
	inline DoubleU5BU5D_t3413330114* get_outputPrevBiasesDelta_16() const { return ___outputPrevBiasesDelta_16; }
	inline DoubleU5BU5D_t3413330114** get_address_of_outputPrevBiasesDelta_16() { return &___outputPrevBiasesDelta_16; }
	inline void set_outputPrevBiasesDelta_16(DoubleU5BU5D_t3413330114* value)
	{
		___outputPrevBiasesDelta_16 = value;
		Il2CppCodeGenWriteBarrier((&___outputPrevBiasesDelta_16), value);
	}
};

struct NeuralNetwork_t3692603454_StaticFields
{
public:
	// System.Random NeuralNetwork::rnd
	Random_t108471755 * ___rnd_0;

public:
	inline static int32_t get_offset_of_rnd_0() { return static_cast<int32_t>(offsetof(NeuralNetwork_t3692603454_StaticFields, ___rnd_0)); }
	inline Random_t108471755 * get_rnd_0() const { return ___rnd_0; }
	inline Random_t108471755 ** get_address_of_rnd_0() { return &___rnd_0; }
	inline void set_rnd_0(Random_t108471755 * value)
	{
		___rnd_0 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEURALNETWORK_T3692603454_H
#ifndef ADVANCEDHEROCARDS_T368746982_H
#define ADVANCEDHEROCARDS_T368746982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdvancedHeroCards
struct  AdvancedHeroCards_t368746982  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<HeroCard> AdvancedHeroCards::AdvancedCards
	List_1_t360954787 * ___AdvancedCards_10;

public:
	inline static int32_t get_offset_of_AdvancedCards_10() { return static_cast<int32_t>(offsetof(AdvancedHeroCards_t368746982, ___AdvancedCards_10)); }
	inline List_1_t360954787 * get_AdvancedCards_10() const { return ___AdvancedCards_10; }
	inline List_1_t360954787 ** get_address_of_AdvancedCards_10() { return &___AdvancedCards_10; }
	inline void set_AdvancedCards_10(List_1_t360954787 * value)
	{
		___AdvancedCards_10 = value;
		Il2CppCodeGenWriteBarrier((&___AdvancedCards_10), value);
	}
};

struct AdvancedHeroCards_t368746982_StaticFields
{
public:
	// HeroCard AdvancedHeroCards::BlockedWindows
	HeroCard_t3183847341 * ___BlockedWindows_0;
	// HeroCard AdvancedHeroCards::Dynamite
	HeroCard_t3183847341 * ___Dynamite_1;
	// HeroCard AdvancedHeroCards::EscapeThroughTheWindows
	HeroCard_t3183847341 * ___EscapeThroughTheWindows_2;
	// HeroCard AdvancedHeroCards::FireExtinguisher
	HeroCard_t3183847341 * ___FireExtinguisher_3;
	// HeroCard AdvancedHeroCards::Gasoline
	HeroCard_t3183847341 * ___Gasoline_4;
	// HeroCard AdvancedHeroCards::HeroicResolve
	HeroCard_t3183847341 * ___HeroicResolve_5;
	// HeroCard AdvancedHeroCards::HerosRage
	HeroCard_t3183847341 * ___HerosRage_6;
	// HeroCard AdvancedHeroCards::JustWhatINeeded
	HeroCard_t3183847341 * ___JustWhatINeeded_7;
	// HeroCard AdvancedHeroCards::Lighter
	HeroCard_t3183847341 * ___Lighter_8;
	// HeroCard AdvancedHeroCards::OldBetsy
	HeroCard_t3183847341 * ___OldBetsy_9;

public:
	inline static int32_t get_offset_of_BlockedWindows_0() { return static_cast<int32_t>(offsetof(AdvancedHeroCards_t368746982_StaticFields, ___BlockedWindows_0)); }
	inline HeroCard_t3183847341 * get_BlockedWindows_0() const { return ___BlockedWindows_0; }
	inline HeroCard_t3183847341 ** get_address_of_BlockedWindows_0() { return &___BlockedWindows_0; }
	inline void set_BlockedWindows_0(HeroCard_t3183847341 * value)
	{
		___BlockedWindows_0 = value;
		Il2CppCodeGenWriteBarrier((&___BlockedWindows_0), value);
	}

	inline static int32_t get_offset_of_Dynamite_1() { return static_cast<int32_t>(offsetof(AdvancedHeroCards_t368746982_StaticFields, ___Dynamite_1)); }
	inline HeroCard_t3183847341 * get_Dynamite_1() const { return ___Dynamite_1; }
	inline HeroCard_t3183847341 ** get_address_of_Dynamite_1() { return &___Dynamite_1; }
	inline void set_Dynamite_1(HeroCard_t3183847341 * value)
	{
		___Dynamite_1 = value;
		Il2CppCodeGenWriteBarrier((&___Dynamite_1), value);
	}

	inline static int32_t get_offset_of_EscapeThroughTheWindows_2() { return static_cast<int32_t>(offsetof(AdvancedHeroCards_t368746982_StaticFields, ___EscapeThroughTheWindows_2)); }
	inline HeroCard_t3183847341 * get_EscapeThroughTheWindows_2() const { return ___EscapeThroughTheWindows_2; }
	inline HeroCard_t3183847341 ** get_address_of_EscapeThroughTheWindows_2() { return &___EscapeThroughTheWindows_2; }
	inline void set_EscapeThroughTheWindows_2(HeroCard_t3183847341 * value)
	{
		___EscapeThroughTheWindows_2 = value;
		Il2CppCodeGenWriteBarrier((&___EscapeThroughTheWindows_2), value);
	}

	inline static int32_t get_offset_of_FireExtinguisher_3() { return static_cast<int32_t>(offsetof(AdvancedHeroCards_t368746982_StaticFields, ___FireExtinguisher_3)); }
	inline HeroCard_t3183847341 * get_FireExtinguisher_3() const { return ___FireExtinguisher_3; }
	inline HeroCard_t3183847341 ** get_address_of_FireExtinguisher_3() { return &___FireExtinguisher_3; }
	inline void set_FireExtinguisher_3(HeroCard_t3183847341 * value)
	{
		___FireExtinguisher_3 = value;
		Il2CppCodeGenWriteBarrier((&___FireExtinguisher_3), value);
	}

	inline static int32_t get_offset_of_Gasoline_4() { return static_cast<int32_t>(offsetof(AdvancedHeroCards_t368746982_StaticFields, ___Gasoline_4)); }
	inline HeroCard_t3183847341 * get_Gasoline_4() const { return ___Gasoline_4; }
	inline HeroCard_t3183847341 ** get_address_of_Gasoline_4() { return &___Gasoline_4; }
	inline void set_Gasoline_4(HeroCard_t3183847341 * value)
	{
		___Gasoline_4 = value;
		Il2CppCodeGenWriteBarrier((&___Gasoline_4), value);
	}

	inline static int32_t get_offset_of_HeroicResolve_5() { return static_cast<int32_t>(offsetof(AdvancedHeroCards_t368746982_StaticFields, ___HeroicResolve_5)); }
	inline HeroCard_t3183847341 * get_HeroicResolve_5() const { return ___HeroicResolve_5; }
	inline HeroCard_t3183847341 ** get_address_of_HeroicResolve_5() { return &___HeroicResolve_5; }
	inline void set_HeroicResolve_5(HeroCard_t3183847341 * value)
	{
		___HeroicResolve_5 = value;
		Il2CppCodeGenWriteBarrier((&___HeroicResolve_5), value);
	}

	inline static int32_t get_offset_of_HerosRage_6() { return static_cast<int32_t>(offsetof(AdvancedHeroCards_t368746982_StaticFields, ___HerosRage_6)); }
	inline HeroCard_t3183847341 * get_HerosRage_6() const { return ___HerosRage_6; }
	inline HeroCard_t3183847341 ** get_address_of_HerosRage_6() { return &___HerosRage_6; }
	inline void set_HerosRage_6(HeroCard_t3183847341 * value)
	{
		___HerosRage_6 = value;
		Il2CppCodeGenWriteBarrier((&___HerosRage_6), value);
	}

	inline static int32_t get_offset_of_JustWhatINeeded_7() { return static_cast<int32_t>(offsetof(AdvancedHeroCards_t368746982_StaticFields, ___JustWhatINeeded_7)); }
	inline HeroCard_t3183847341 * get_JustWhatINeeded_7() const { return ___JustWhatINeeded_7; }
	inline HeroCard_t3183847341 ** get_address_of_JustWhatINeeded_7() { return &___JustWhatINeeded_7; }
	inline void set_JustWhatINeeded_7(HeroCard_t3183847341 * value)
	{
		___JustWhatINeeded_7 = value;
		Il2CppCodeGenWriteBarrier((&___JustWhatINeeded_7), value);
	}

	inline static int32_t get_offset_of_Lighter_8() { return static_cast<int32_t>(offsetof(AdvancedHeroCards_t368746982_StaticFields, ___Lighter_8)); }
	inline HeroCard_t3183847341 * get_Lighter_8() const { return ___Lighter_8; }
	inline HeroCard_t3183847341 ** get_address_of_Lighter_8() { return &___Lighter_8; }
	inline void set_Lighter_8(HeroCard_t3183847341 * value)
	{
		___Lighter_8 = value;
		Il2CppCodeGenWriteBarrier((&___Lighter_8), value);
	}

	inline static int32_t get_offset_of_OldBetsy_9() { return static_cast<int32_t>(offsetof(AdvancedHeroCards_t368746982_StaticFields, ___OldBetsy_9)); }
	inline HeroCard_t3183847341 * get_OldBetsy_9() const { return ___OldBetsy_9; }
	inline HeroCard_t3183847341 ** get_address_of_OldBetsy_9() { return &___OldBetsy_9; }
	inline void set_OldBetsy_9(HeroCard_t3183847341 * value)
	{
		___OldBetsy_9 = value;
		Il2CppCodeGenWriteBarrier((&___OldBetsy_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDHEROCARDS_T368746982_H
#ifndef HEROES_T1064386291_H
#define HEROES_T1064386291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Heroes
struct  Heroes_t1064386291  : public RuntimeObject
{
public:
	// System.String[] Heroes::heroTargetPriority
	StringU5BU5D_t1281789340* ___heroTargetPriority_0;
	// System.Collections.Generic.List`1<Hero> Heroes::coreHeroes
	List_1_t3733427512 * ___coreHeroes_1;
	// System.Collections.Generic.List`1<Hero> Heroes::growingHungerHeroes
	List_1_t3733427512 * ___growingHungerHeroes_2;
	// System.Collections.Generic.List`1<Hero> Heroes::heroPackOneHeroes
	List_1_t3733427512 * ___heroPackOneHeroes_3;
	// System.Collections.Generic.List`1<Hero> Heroes::timberPeakHeroes
	List_1_t3733427512 * ___timberPeakHeroes_4;
	// System.Collections.Generic.List`1<Hero> Heroes::bloodInTheForestHeroes
	List_1_t3733427512 * ___bloodInTheForestHeroes_5;
	// System.Collections.Generic.List`1<Hero> Heroes::tenthAnniversaryEditionHeroes
	List_1_t3733427512 * ___tenthAnniversaryEditionHeroes_6;
	// System.Collections.Generic.List`1<Hero> Heroes::heroPackTwoHeroes
	List_1_t3733427512 * ___heroPackTwoHeroes_7;
	// System.Collections.Generic.List`1<Hero> Heroes::selectableHeroes
	List_1_t3733427512 * ___selectableHeroes_8;
	// System.Collections.Generic.List`1<Hero> Heroes::GameHeroes
	List_1_t3733427512 * ___GameHeroes_9;

public:
	inline static int32_t get_offset_of_heroTargetPriority_0() { return static_cast<int32_t>(offsetof(Heroes_t1064386291, ___heroTargetPriority_0)); }
	inline StringU5BU5D_t1281789340* get_heroTargetPriority_0() const { return ___heroTargetPriority_0; }
	inline StringU5BU5D_t1281789340** get_address_of_heroTargetPriority_0() { return &___heroTargetPriority_0; }
	inline void set_heroTargetPriority_0(StringU5BU5D_t1281789340* value)
	{
		___heroTargetPriority_0 = value;
		Il2CppCodeGenWriteBarrier((&___heroTargetPriority_0), value);
	}

	inline static int32_t get_offset_of_coreHeroes_1() { return static_cast<int32_t>(offsetof(Heroes_t1064386291, ___coreHeroes_1)); }
	inline List_1_t3733427512 * get_coreHeroes_1() const { return ___coreHeroes_1; }
	inline List_1_t3733427512 ** get_address_of_coreHeroes_1() { return &___coreHeroes_1; }
	inline void set_coreHeroes_1(List_1_t3733427512 * value)
	{
		___coreHeroes_1 = value;
		Il2CppCodeGenWriteBarrier((&___coreHeroes_1), value);
	}

	inline static int32_t get_offset_of_growingHungerHeroes_2() { return static_cast<int32_t>(offsetof(Heroes_t1064386291, ___growingHungerHeroes_2)); }
	inline List_1_t3733427512 * get_growingHungerHeroes_2() const { return ___growingHungerHeroes_2; }
	inline List_1_t3733427512 ** get_address_of_growingHungerHeroes_2() { return &___growingHungerHeroes_2; }
	inline void set_growingHungerHeroes_2(List_1_t3733427512 * value)
	{
		___growingHungerHeroes_2 = value;
		Il2CppCodeGenWriteBarrier((&___growingHungerHeroes_2), value);
	}

	inline static int32_t get_offset_of_heroPackOneHeroes_3() { return static_cast<int32_t>(offsetof(Heroes_t1064386291, ___heroPackOneHeroes_3)); }
	inline List_1_t3733427512 * get_heroPackOneHeroes_3() const { return ___heroPackOneHeroes_3; }
	inline List_1_t3733427512 ** get_address_of_heroPackOneHeroes_3() { return &___heroPackOneHeroes_3; }
	inline void set_heroPackOneHeroes_3(List_1_t3733427512 * value)
	{
		___heroPackOneHeroes_3 = value;
		Il2CppCodeGenWriteBarrier((&___heroPackOneHeroes_3), value);
	}

	inline static int32_t get_offset_of_timberPeakHeroes_4() { return static_cast<int32_t>(offsetof(Heroes_t1064386291, ___timberPeakHeroes_4)); }
	inline List_1_t3733427512 * get_timberPeakHeroes_4() const { return ___timberPeakHeroes_4; }
	inline List_1_t3733427512 ** get_address_of_timberPeakHeroes_4() { return &___timberPeakHeroes_4; }
	inline void set_timberPeakHeroes_4(List_1_t3733427512 * value)
	{
		___timberPeakHeroes_4 = value;
		Il2CppCodeGenWriteBarrier((&___timberPeakHeroes_4), value);
	}

	inline static int32_t get_offset_of_bloodInTheForestHeroes_5() { return static_cast<int32_t>(offsetof(Heroes_t1064386291, ___bloodInTheForestHeroes_5)); }
	inline List_1_t3733427512 * get_bloodInTheForestHeroes_5() const { return ___bloodInTheForestHeroes_5; }
	inline List_1_t3733427512 ** get_address_of_bloodInTheForestHeroes_5() { return &___bloodInTheForestHeroes_5; }
	inline void set_bloodInTheForestHeroes_5(List_1_t3733427512 * value)
	{
		___bloodInTheForestHeroes_5 = value;
		Il2CppCodeGenWriteBarrier((&___bloodInTheForestHeroes_5), value);
	}

	inline static int32_t get_offset_of_tenthAnniversaryEditionHeroes_6() { return static_cast<int32_t>(offsetof(Heroes_t1064386291, ___tenthAnniversaryEditionHeroes_6)); }
	inline List_1_t3733427512 * get_tenthAnniversaryEditionHeroes_6() const { return ___tenthAnniversaryEditionHeroes_6; }
	inline List_1_t3733427512 ** get_address_of_tenthAnniversaryEditionHeroes_6() { return &___tenthAnniversaryEditionHeroes_6; }
	inline void set_tenthAnniversaryEditionHeroes_6(List_1_t3733427512 * value)
	{
		___tenthAnniversaryEditionHeroes_6 = value;
		Il2CppCodeGenWriteBarrier((&___tenthAnniversaryEditionHeroes_6), value);
	}

	inline static int32_t get_offset_of_heroPackTwoHeroes_7() { return static_cast<int32_t>(offsetof(Heroes_t1064386291, ___heroPackTwoHeroes_7)); }
	inline List_1_t3733427512 * get_heroPackTwoHeroes_7() const { return ___heroPackTwoHeroes_7; }
	inline List_1_t3733427512 ** get_address_of_heroPackTwoHeroes_7() { return &___heroPackTwoHeroes_7; }
	inline void set_heroPackTwoHeroes_7(List_1_t3733427512 * value)
	{
		___heroPackTwoHeroes_7 = value;
		Il2CppCodeGenWriteBarrier((&___heroPackTwoHeroes_7), value);
	}

	inline static int32_t get_offset_of_selectableHeroes_8() { return static_cast<int32_t>(offsetof(Heroes_t1064386291, ___selectableHeroes_8)); }
	inline List_1_t3733427512 * get_selectableHeroes_8() const { return ___selectableHeroes_8; }
	inline List_1_t3733427512 ** get_address_of_selectableHeroes_8() { return &___selectableHeroes_8; }
	inline void set_selectableHeroes_8(List_1_t3733427512 * value)
	{
		___selectableHeroes_8 = value;
		Il2CppCodeGenWriteBarrier((&___selectableHeroes_8), value);
	}

	inline static int32_t get_offset_of_GameHeroes_9() { return static_cast<int32_t>(offsetof(Heroes_t1064386291, ___GameHeroes_9)); }
	inline List_1_t3733427512 * get_GameHeroes_9() const { return ___GameHeroes_9; }
	inline List_1_t3733427512 ** get_address_of_GameHeroes_9() { return &___GameHeroes_9; }
	inline void set_GameHeroes_9(List_1_t3733427512 * value)
	{
		___GameHeroes_9 = value;
		Il2CppCodeGenWriteBarrier((&___GameHeroes_9), value);
	}
};

struct Heroes_t1064386291_StaticFields
{
public:
	// System.Func`3<System.String,Hero,System.String> Heroes::<>f__am$cache0
	Func_3_t2253617649 * ___U3CU3Ef__amU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(Heroes_t1064386291_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Func_3_t2253617649 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Func_3_t2253617649 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Func_3_t2253617649 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROES_T1064386291_H
#ifndef U3CHEROESINTHEGAMEU3EC__ANONSTOREY1_T809524509_H
#define U3CHEROESINTHEGAMEU3EC__ANONSTOREY1_T809524509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Heroes/<HeroesInTheGame>c__AnonStorey1
struct  U3CHeroesInTheGameU3Ec__AnonStorey1_t809524509  : public RuntimeObject
{
public:
	// System.String[] Heroes/<HeroesInTheGame>c__AnonStorey1::ppHeroKey
	StringU5BU5D_t1281789340* ___ppHeroKey_0;

public:
	inline static int32_t get_offset_of_ppHeroKey_0() { return static_cast<int32_t>(offsetof(U3CHeroesInTheGameU3Ec__AnonStorey1_t809524509, ___ppHeroKey_0)); }
	inline StringU5BU5D_t1281789340* get_ppHeroKey_0() const { return ___ppHeroKey_0; }
	inline StringU5BU5D_t1281789340** get_address_of_ppHeroKey_0() { return &___ppHeroKey_0; }
	inline void set_ppHeroKey_0(StringU5BU5D_t1281789340* value)
	{
		___ppHeroKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___ppHeroKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHEROESINTHEGAMEU3EC__ANONSTOREY1_T809524509_H
#ifndef GAMESESSION_T4087811243_H
#define GAMESESSION_T4087811243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSession
struct  GameSession_t4087811243  : public RuntimeObject
{
public:
	// Missions GameSession::missionOptions
	Missions_t3398870215 * ___missionOptions_0;
	// Mission GameSession::currentMission
	Mission_t4233471175 * ___currentMission_1;
	// Tiles GameSession::mapTiles
	Tiles_t1457048987 * ___mapTiles_2;
	// Heroes GameSession::heroCharacters
	Heroes_t1064386291 * ___heroCharacters_3;
	// Zombies GameSession::zombies
	Zombies_t3905046179 * ___zombies_4;
	// RadialBasisFunctionNetwork GameSession::rbfSession
	RadialBasisFunctionNetwork_t3778331090 * ___rbfSession_5;
	// System.Double[] GameSession::currentGameDifficulty
	DoubleU5BU5D_t3413330114* ___currentGameDifficulty_6;
	// System.Boolean GameSession::networkCreated
	bool ___networkCreated_7;

public:
	inline static int32_t get_offset_of_missionOptions_0() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___missionOptions_0)); }
	inline Missions_t3398870215 * get_missionOptions_0() const { return ___missionOptions_0; }
	inline Missions_t3398870215 ** get_address_of_missionOptions_0() { return &___missionOptions_0; }
	inline void set_missionOptions_0(Missions_t3398870215 * value)
	{
		___missionOptions_0 = value;
		Il2CppCodeGenWriteBarrier((&___missionOptions_0), value);
	}

	inline static int32_t get_offset_of_currentMission_1() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___currentMission_1)); }
	inline Mission_t4233471175 * get_currentMission_1() const { return ___currentMission_1; }
	inline Mission_t4233471175 ** get_address_of_currentMission_1() { return &___currentMission_1; }
	inline void set_currentMission_1(Mission_t4233471175 * value)
	{
		___currentMission_1 = value;
		Il2CppCodeGenWriteBarrier((&___currentMission_1), value);
	}

	inline static int32_t get_offset_of_mapTiles_2() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___mapTiles_2)); }
	inline Tiles_t1457048987 * get_mapTiles_2() const { return ___mapTiles_2; }
	inline Tiles_t1457048987 ** get_address_of_mapTiles_2() { return &___mapTiles_2; }
	inline void set_mapTiles_2(Tiles_t1457048987 * value)
	{
		___mapTiles_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapTiles_2), value);
	}

	inline static int32_t get_offset_of_heroCharacters_3() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___heroCharacters_3)); }
	inline Heroes_t1064386291 * get_heroCharacters_3() const { return ___heroCharacters_3; }
	inline Heroes_t1064386291 ** get_address_of_heroCharacters_3() { return &___heroCharacters_3; }
	inline void set_heroCharacters_3(Heroes_t1064386291 * value)
	{
		___heroCharacters_3 = value;
		Il2CppCodeGenWriteBarrier((&___heroCharacters_3), value);
	}

	inline static int32_t get_offset_of_zombies_4() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___zombies_4)); }
	inline Zombies_t3905046179 * get_zombies_4() const { return ___zombies_4; }
	inline Zombies_t3905046179 ** get_address_of_zombies_4() { return &___zombies_4; }
	inline void set_zombies_4(Zombies_t3905046179 * value)
	{
		___zombies_4 = value;
		Il2CppCodeGenWriteBarrier((&___zombies_4), value);
	}

	inline static int32_t get_offset_of_rbfSession_5() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___rbfSession_5)); }
	inline RadialBasisFunctionNetwork_t3778331090 * get_rbfSession_5() const { return ___rbfSession_5; }
	inline RadialBasisFunctionNetwork_t3778331090 ** get_address_of_rbfSession_5() { return &___rbfSession_5; }
	inline void set_rbfSession_5(RadialBasisFunctionNetwork_t3778331090 * value)
	{
		___rbfSession_5 = value;
		Il2CppCodeGenWriteBarrier((&___rbfSession_5), value);
	}

	inline static int32_t get_offset_of_currentGameDifficulty_6() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___currentGameDifficulty_6)); }
	inline DoubleU5BU5D_t3413330114* get_currentGameDifficulty_6() const { return ___currentGameDifficulty_6; }
	inline DoubleU5BU5D_t3413330114** get_address_of_currentGameDifficulty_6() { return &___currentGameDifficulty_6; }
	inline void set_currentGameDifficulty_6(DoubleU5BU5D_t3413330114* value)
	{
		___currentGameDifficulty_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentGameDifficulty_6), value);
	}

	inline static int32_t get_offset_of_networkCreated_7() { return static_cast<int32_t>(offsetof(GameSession_t4087811243, ___networkCreated_7)); }
	inline bool get_networkCreated_7() const { return ___networkCreated_7; }
	inline bool* get_address_of_networkCreated_7() { return &___networkCreated_7; }
	inline void set_networkCreated_7(bool value)
	{
		___networkCreated_7 = value;
	}
};

struct GameSession_t4087811243_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`3<CloudConnectorCore/QueryType,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<System.String>> GameSession::<>f__mg$cache0
	UnityAction_3_t1112121581 * ___U3CU3Ef__mgU24cache0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(GameSession_t4087811243_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline UnityAction_3_t1112121581 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline UnityAction_3_t1112121581 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(UnityAction_3_t1112121581 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESESSION_T4087811243_H
#ifndef U3CHEROESINTHEGAMEU3EC__ANONSTOREY0_T809590045_H
#define U3CHEROESINTHEGAMEU3EC__ANONSTOREY0_T809590045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Heroes/<HeroesInTheGame>c__AnonStorey0
struct  U3CHeroesInTheGameU3Ec__AnonStorey0_t809590045  : public RuntimeObject
{
public:
	// System.String Heroes/<HeroesInTheGame>c__AnonStorey0::deadHero
	String_t* ___deadHero_0;

public:
	inline static int32_t get_offset_of_deadHero_0() { return static_cast<int32_t>(offsetof(U3CHeroesInTheGameU3Ec__AnonStorey0_t809590045, ___deadHero_0)); }
	inline String_t* get_deadHero_0() const { return ___deadHero_0; }
	inline String_t** get_address_of_deadHero_0() { return &___deadHero_0; }
	inline void set_deadHero_0(String_t* value)
	{
		___deadHero_0 = value;
		Il2CppCodeGenWriteBarrier((&___deadHero_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHEROESINTHEGAMEU3EC__ANONSTOREY0_T809590045_H
#ifndef U3CHEROESINTHEGAMEU3EC__ANONSTOREY2_T809721117_H
#define U3CHEROESINTHEGAMEU3EC__ANONSTOREY2_T809721117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Heroes/<HeroesInTheGame>c__AnonStorey2
struct  U3CHeroesInTheGameU3Ec__AnonStorey2_t809721117  : public RuntimeObject
{
public:
	// System.Int32 Heroes/<HeroesInTheGame>c__AnonStorey2::i
	int32_t ___i_0;
	// Heroes/<HeroesInTheGame>c__AnonStorey1 Heroes/<HeroesInTheGame>c__AnonStorey2::<>f__ref$1
	U3CHeroesInTheGameU3Ec__AnonStorey1_t809524509 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(U3CHeroesInTheGameU3Ec__AnonStorey2_t809721117, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CHeroesInTheGameU3Ec__AnonStorey2_t809721117, ___U3CU3Ef__refU241_1)); }
	inline U3CHeroesInTheGameU3Ec__AnonStorey1_t809524509 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CHeroesInTheGameU3Ec__AnonStorey1_t809524509 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CHeroesInTheGameU3Ec__AnonStorey1_t809524509 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHEROESINTHEGAMEU3EC__ANONSTOREY2_T809721117_H
#ifndef REMAINSINPLAYCARDDETAILS_T970086784_H
#define REMAINSINPLAYCARDDETAILS_T970086784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSaveData/RemainsInPlayCardDetails
struct  RemainsInPlayCardDetails_t970086784  : public RuntimeObject
{
public:
	// System.String GameSaveData/RemainsInPlayCardDetails::Name
	String_t* ___Name_0;
	// System.String GameSaveData/RemainsInPlayCardDetails::RemainsInPlayName
	String_t* ___RemainsInPlayName_1;
	// System.String GameSaveData/RemainsInPlayCardDetails::CardText
	String_t* ___CardText_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(RemainsInPlayCardDetails_t970086784, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_RemainsInPlayName_1() { return static_cast<int32_t>(offsetof(RemainsInPlayCardDetails_t970086784, ___RemainsInPlayName_1)); }
	inline String_t* get_RemainsInPlayName_1() const { return ___RemainsInPlayName_1; }
	inline String_t** get_address_of_RemainsInPlayName_1() { return &___RemainsInPlayName_1; }
	inline void set_RemainsInPlayName_1(String_t* value)
	{
		___RemainsInPlayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___RemainsInPlayName_1), value);
	}

	inline static int32_t get_offset_of_CardText_2() { return static_cast<int32_t>(offsetof(RemainsInPlayCardDetails_t970086784, ___CardText_2)); }
	inline String_t* get_CardText_2() const { return ___CardText_2; }
	inline String_t** get_address_of_CardText_2() { return &___CardText_2; }
	inline void set_CardText_2(String_t* value)
	{
		___CardText_2 = value;
		Il2CppCodeGenWriteBarrier((&___CardText_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMAINSINPLAYCARDDETAILS_T970086784_H
#ifndef U3CRANDOMHEROWITHKEYWORDU3EC__ANONSTOREY4_T3092705917_H
#define U3CRANDOMHEROWITHKEYWORDU3EC__ANONSTOREY4_T3092705917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Heroes/<RandomHeroWithKeyword>c__AnonStorey4
struct  U3CRandomHeroWithKeywordU3Ec__AnonStorey4_t3092705917  : public RuntimeObject
{
public:
	// Hero Heroes/<RandomHeroWithKeyword>c__AnonStorey4::hero
	Hero_t2261352770 * ___hero_0;
	// Heroes/<RandomHeroWithKeyword>c__AnonStorey3 Heroes/<RandomHeroWithKeyword>c__AnonStorey4::<>f__ref$3
	U3CRandomHeroWithKeywordU3Ec__AnonStorey3_t371716733 * ___U3CU3Ef__refU243_1;

public:
	inline static int32_t get_offset_of_hero_0() { return static_cast<int32_t>(offsetof(U3CRandomHeroWithKeywordU3Ec__AnonStorey4_t3092705917, ___hero_0)); }
	inline Hero_t2261352770 * get_hero_0() const { return ___hero_0; }
	inline Hero_t2261352770 ** get_address_of_hero_0() { return &___hero_0; }
	inline void set_hero_0(Hero_t2261352770 * value)
	{
		___hero_0 = value;
		Il2CppCodeGenWriteBarrier((&___hero_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_1() { return static_cast<int32_t>(offsetof(U3CRandomHeroWithKeywordU3Ec__AnonStorey4_t3092705917, ___U3CU3Ef__refU243_1)); }
	inline U3CRandomHeroWithKeywordU3Ec__AnonStorey3_t371716733 * get_U3CU3Ef__refU243_1() const { return ___U3CU3Ef__refU243_1; }
	inline U3CRandomHeroWithKeywordU3Ec__AnonStorey3_t371716733 ** get_address_of_U3CU3Ef__refU243_1() { return &___U3CU3Ef__refU243_1; }
	inline void set_U3CU3Ef__refU243_1(U3CRandomHeroWithKeywordU3Ec__AnonStorey3_t371716733 * value)
	{
		___U3CU3Ef__refU243_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU243_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRANDOMHEROWITHKEYWORDU3EC__ANONSTOREY4_T3092705917_H
#ifndef GAMEDATA_T415813024_H
#define GAMEDATA_T415813024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameData
struct  GameData_t415813024  : public RuntimeObject
{
public:
	// System.String GameData::gameSessionName
	String_t* ___gameSessionName_0;
	// System.String GameData::mission
	String_t* ___mission_1;
	// System.String GameData::buildings
	String_t* ___buildings_2;
	// System.Collections.Generic.List`1<RoundData> GameData::roundData
	List_1_t3502107242 * ___roundData_3;
	// System.String GameData::<GameWon>k__BackingField
	String_t* ___U3CGameWonU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_gameSessionName_0() { return static_cast<int32_t>(offsetof(GameData_t415813024, ___gameSessionName_0)); }
	inline String_t* get_gameSessionName_0() const { return ___gameSessionName_0; }
	inline String_t** get_address_of_gameSessionName_0() { return &___gameSessionName_0; }
	inline void set_gameSessionName_0(String_t* value)
	{
		___gameSessionName_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameSessionName_0), value);
	}

	inline static int32_t get_offset_of_mission_1() { return static_cast<int32_t>(offsetof(GameData_t415813024, ___mission_1)); }
	inline String_t* get_mission_1() const { return ___mission_1; }
	inline String_t** get_address_of_mission_1() { return &___mission_1; }
	inline void set_mission_1(String_t* value)
	{
		___mission_1 = value;
		Il2CppCodeGenWriteBarrier((&___mission_1), value);
	}

	inline static int32_t get_offset_of_buildings_2() { return static_cast<int32_t>(offsetof(GameData_t415813024, ___buildings_2)); }
	inline String_t* get_buildings_2() const { return ___buildings_2; }
	inline String_t** get_address_of_buildings_2() { return &___buildings_2; }
	inline void set_buildings_2(String_t* value)
	{
		___buildings_2 = value;
		Il2CppCodeGenWriteBarrier((&___buildings_2), value);
	}

	inline static int32_t get_offset_of_roundData_3() { return static_cast<int32_t>(offsetof(GameData_t415813024, ___roundData_3)); }
	inline List_1_t3502107242 * get_roundData_3() const { return ___roundData_3; }
	inline List_1_t3502107242 ** get_address_of_roundData_3() { return &___roundData_3; }
	inline void set_roundData_3(List_1_t3502107242 * value)
	{
		___roundData_3 = value;
		Il2CppCodeGenWriteBarrier((&___roundData_3), value);
	}

	inline static int32_t get_offset_of_U3CGameWonU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GameData_t415813024, ___U3CGameWonU3Ek__BackingField_4)); }
	inline String_t* get_U3CGameWonU3Ek__BackingField_4() const { return ___U3CGameWonU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CGameWonU3Ek__BackingField_4() { return &___U3CGameWonU3Ek__BackingField_4; }
	inline void set_U3CGameWonU3Ek__BackingField_4(String_t* value)
	{
		___U3CGameWonU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameWonU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEDATA_T415813024_H
#ifndef FILEIO_T1459473183_H
#define FILEIO_T1459473183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FileIo
struct  FileIo_t1459473183  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEIO_T1459473183_H
#ifndef U3CLOADINGSCENEU3EC__ITERATOR0_T983482384_H
#define U3CLOADINGSCENEU3EC__ITERATOR0_T983482384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadScene/<LoadingScene>c__Iterator0
struct  U3CLoadingSceneU3Ec__Iterator0_t983482384  : public RuntimeObject
{
public:
	// System.String LoadScene/<LoadingScene>c__Iterator0::scene
	String_t* ___scene_0;
	// UnityEngine.AsyncOperation LoadScene/<LoadingScene>c__Iterator0::<async>__0
	AsyncOperation_t1445031843 * ___U3CasyncU3E__0_1;
	// System.Object LoadScene/<LoadingScene>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean LoadScene/<LoadingScene>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 LoadScene/<LoadingScene>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_scene_0() { return static_cast<int32_t>(offsetof(U3CLoadingSceneU3Ec__Iterator0_t983482384, ___scene_0)); }
	inline String_t* get_scene_0() const { return ___scene_0; }
	inline String_t** get_address_of_scene_0() { return &___scene_0; }
	inline void set_scene_0(String_t* value)
	{
		___scene_0 = value;
		Il2CppCodeGenWriteBarrier((&___scene_0), value);
	}

	inline static int32_t get_offset_of_U3CasyncU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadingSceneU3Ec__Iterator0_t983482384, ___U3CasyncU3E__0_1)); }
	inline AsyncOperation_t1445031843 * get_U3CasyncU3E__0_1() const { return ___U3CasyncU3E__0_1; }
	inline AsyncOperation_t1445031843 ** get_address_of_U3CasyncU3E__0_1() { return &___U3CasyncU3E__0_1; }
	inline void set_U3CasyncU3E__0_1(AsyncOperation_t1445031843 * value)
	{
		___U3CasyncU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CasyncU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadingSceneU3Ec__Iterator0_t983482384, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadingSceneU3Ec__Iterator0_t983482384, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadingSceneU3Ec__Iterator0_t983482384, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINGSCENEU3EC__ITERATOR0_T983482384_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1232402489_H
#define U3CSTARTU3EC__ITERATOR0_T1232402489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FeedForwardProgram/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1232402489  : public RuntimeObject
{
public:
	// NeuralNetwork FeedForwardProgram/<Start>c__Iterator0::<nn>__0
	NeuralNetwork_t3692603454 * ___U3CnnU3E__0_0;
	// System.Double[] FeedForwardProgram/<Start>c__Iterator0::<outputValues>__0
	DoubleU5BU5D_t3413330114* ___U3CoutputValuesU3E__0_1;
	// FeedForwardProgram FeedForwardProgram/<Start>c__Iterator0::$this
	FeedForwardProgram_t2707572273 * ___U24this_2;
	// System.Object FeedForwardProgram/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean FeedForwardProgram/<Start>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 FeedForwardProgram/<Start>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CnnU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1232402489, ___U3CnnU3E__0_0)); }
	inline NeuralNetwork_t3692603454 * get_U3CnnU3E__0_0() const { return ___U3CnnU3E__0_0; }
	inline NeuralNetwork_t3692603454 ** get_address_of_U3CnnU3E__0_0() { return &___U3CnnU3E__0_0; }
	inline void set_U3CnnU3E__0_0(NeuralNetwork_t3692603454 * value)
	{
		___U3CnnU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnnU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CoutputValuesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1232402489, ___U3CoutputValuesU3E__0_1)); }
	inline DoubleU5BU5D_t3413330114* get_U3CoutputValuesU3E__0_1() const { return ___U3CoutputValuesU3E__0_1; }
	inline DoubleU5BU5D_t3413330114** get_address_of_U3CoutputValuesU3E__0_1() { return &___U3CoutputValuesU3E__0_1; }
	inline void set_U3CoutputValuesU3E__0_1(DoubleU5BU5D_t3413330114* value)
	{
		___U3CoutputValuesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoutputValuesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1232402489, ___U24this_2)); }
	inline FeedForwardProgram_t2707572273 * get_U24this_2() const { return ___U24this_2; }
	inline FeedForwardProgram_t2707572273 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(FeedForwardProgram_t2707572273 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1232402489, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1232402489, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1232402489, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1232402489_H
#ifndef HEROCARD_T3183847341_H
#define HEROCARD_T3183847341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroCard
struct  HeroCard_t3183847341  : public RuntimeObject
{
public:
	// System.String HeroCard::ReferenceName
	String_t* ___ReferenceName_0;
	// System.String HeroCard::Name
	String_t* ___Name_1;
	// System.Collections.Generic.List`1<HeroCard/HeroCardTypes> HeroCard::CardTypes
	List_1_t2987524082 * ___CardTypes_2;
	// System.String HeroCard::Errata
	String_t* ___Errata_3;

public:
	inline static int32_t get_offset_of_ReferenceName_0() { return static_cast<int32_t>(offsetof(HeroCard_t3183847341, ___ReferenceName_0)); }
	inline String_t* get_ReferenceName_0() const { return ___ReferenceName_0; }
	inline String_t** get_address_of_ReferenceName_0() { return &___ReferenceName_0; }
	inline void set_ReferenceName_0(String_t* value)
	{
		___ReferenceName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ReferenceName_0), value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(HeroCard_t3183847341, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_CardTypes_2() { return static_cast<int32_t>(offsetof(HeroCard_t3183847341, ___CardTypes_2)); }
	inline List_1_t2987524082 * get_CardTypes_2() const { return ___CardTypes_2; }
	inline List_1_t2987524082 ** get_address_of_CardTypes_2() { return &___CardTypes_2; }
	inline void set_CardTypes_2(List_1_t2987524082 * value)
	{
		___CardTypes_2 = value;
		Il2CppCodeGenWriteBarrier((&___CardTypes_2), value);
	}

	inline static int32_t get_offset_of_Errata_3() { return static_cast<int32_t>(offsetof(HeroCard_t3183847341, ___Errata_3)); }
	inline String_t* get_Errata_3() const { return ___Errata_3; }
	inline String_t** get_address_of_Errata_3() { return &___Errata_3; }
	inline void set_Errata_3(String_t* value)
	{
		___Errata_3 = value;
		Il2CppCodeGenWriteBarrier((&___Errata_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROCARD_T3183847341_H
#ifndef U3CSOFTMAXU3EC__ANONSTOREY0_T2838892455_H
#define U3CSOFTMAXU3EC__ANONSTOREY0_T2838892455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NeuralNetwork/<Softmax>c__AnonStorey0
struct  U3CSoftmaxU3Ec__AnonStorey0_t2838892455  : public RuntimeObject
{
public:
	// System.Double NeuralNetwork/<Softmax>c__AnonStorey0::max
	double ___max_0;

public:
	inline static int32_t get_offset_of_max_0() { return static_cast<int32_t>(offsetof(U3CSoftmaxU3Ec__AnonStorey0_t2838892455, ___max_0)); }
	inline double get_max_0() const { return ___max_0; }
	inline double* get_address_of_max_0() { return &___max_0; }
	inline void set_max_0(double value)
	{
		___max_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSOFTMAXU3EC__ANONSTOREY0_T2838892455_H
#ifndef BASICHEROCARDS_T2570609810_H
#define BASICHEROCARDS_T2570609810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BasicHeroCards
struct  BasicHeroCards_t2570609810  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<HeroCard> BasicHeroCards::BasicCards
	List_1_t360954787 * ___BasicCards_25;

public:
	inline static int32_t get_offset_of_BasicCards_25() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810, ___BasicCards_25)); }
	inline List_1_t360954787 * get_BasicCards_25() const { return ___BasicCards_25; }
	inline List_1_t360954787 ** get_address_of_BasicCards_25() { return &___BasicCards_25; }
	inline void set_BasicCards_25(List_1_t360954787 * value)
	{
		___BasicCards_25 = value;
		Il2CppCodeGenWriteBarrier((&___BasicCards_25), value);
	}
};

struct BasicHeroCards_t2570609810_StaticFields
{
public:
	// HeroCard BasicHeroCards::Ammo
	HeroCard_t3183847341 * ___Ammo_0;
	// HeroCard BasicHeroCards::AtLast
	HeroCard_t3183847341 * ___AtLast_1;
	// HeroCard BasicHeroCards::BaseballBat
	HeroCard_t3183847341 * ___BaseballBat_2;
	// HeroCard BasicHeroCards::Chainsaw
	HeroCard_t3183847341 * ___Chainsaw_3;
	// HeroCard BasicHeroCards::Crowbar
	HeroCard_t3183847341 * ___Crowbar_4;
	// HeroCard BasicHeroCards::DeputyTaylor
	HeroCard_t3183847341 * ___DeputyTaylor_5;
	// HeroCard BasicHeroCards::DocBrody
	HeroCard_t3183847341 * ___DocBrody_6;
	// HeroCard BasicHeroCards::Faith
	HeroCard_t3183847341 * ___Faith_7;
	// HeroCard BasicHeroCards::FarmerSty
	HeroCard_t3183847341 * ___FarmerSty_8;
	// HeroCard BasicHeroCards::FireAxe
	HeroCard_t3183847341 * ___FireAxe_9;
	// HeroCard BasicHeroCards::FirstAid
	HeroCard_t3183847341 * ___FirstAid_10;
	// HeroCard BasicHeroCards::GetBackYouDevils
	HeroCard_t3183847341 * ___GetBackYouDevils_11;
	// HeroCard BasicHeroCards::Jeb
	HeroCard_t3183847341 * ___Jeb_12;
	// HeroCard BasicHeroCards::JustAScratch
	HeroCard_t3183847341 * ___JustAScratch_13;
	// HeroCard BasicHeroCards::Keys
	HeroCard_t3183847341 * ___Keys_14;
	// HeroCard BasicHeroCards::MeatCleaver
	HeroCard_t3183847341 * ___MeatCleaver_15;
	// HeroCard BasicHeroCards::MrHyde
	HeroCard_t3183847341 * ___MrHyde_16;
	// HeroCard BasicHeroCards::Pitchfork
	HeroCard_t3183847341 * ___Pitchfork_17;
	// HeroCard BasicHeroCards::PrincipalGomez
	HeroCard_t3183847341 * ___PrincipalGomez_18;
	// HeroCard BasicHeroCards::PumpShotgun
	HeroCard_t3183847341 * ___PumpShotgun_19;
	// HeroCard BasicHeroCards::Recovery
	HeroCard_t3183847341 * ___Recovery_20;
	// HeroCard BasicHeroCards::Revolver
	HeroCard_t3183847341 * ___Revolver_21;
	// HeroCard BasicHeroCards::SignalFlare
	HeroCard_t3183847341 * ___SignalFlare_22;
	// HeroCard BasicHeroCards::Torch
	HeroCard_t3183847341 * ___Torch_23;
	// HeroCard BasicHeroCards::WeldingTorch
	HeroCard_t3183847341 * ___WeldingTorch_24;

public:
	inline static int32_t get_offset_of_Ammo_0() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___Ammo_0)); }
	inline HeroCard_t3183847341 * get_Ammo_0() const { return ___Ammo_0; }
	inline HeroCard_t3183847341 ** get_address_of_Ammo_0() { return &___Ammo_0; }
	inline void set_Ammo_0(HeroCard_t3183847341 * value)
	{
		___Ammo_0 = value;
		Il2CppCodeGenWriteBarrier((&___Ammo_0), value);
	}

	inline static int32_t get_offset_of_AtLast_1() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___AtLast_1)); }
	inline HeroCard_t3183847341 * get_AtLast_1() const { return ___AtLast_1; }
	inline HeroCard_t3183847341 ** get_address_of_AtLast_1() { return &___AtLast_1; }
	inline void set_AtLast_1(HeroCard_t3183847341 * value)
	{
		___AtLast_1 = value;
		Il2CppCodeGenWriteBarrier((&___AtLast_1), value);
	}

	inline static int32_t get_offset_of_BaseballBat_2() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___BaseballBat_2)); }
	inline HeroCard_t3183847341 * get_BaseballBat_2() const { return ___BaseballBat_2; }
	inline HeroCard_t3183847341 ** get_address_of_BaseballBat_2() { return &___BaseballBat_2; }
	inline void set_BaseballBat_2(HeroCard_t3183847341 * value)
	{
		___BaseballBat_2 = value;
		Il2CppCodeGenWriteBarrier((&___BaseballBat_2), value);
	}

	inline static int32_t get_offset_of_Chainsaw_3() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___Chainsaw_3)); }
	inline HeroCard_t3183847341 * get_Chainsaw_3() const { return ___Chainsaw_3; }
	inline HeroCard_t3183847341 ** get_address_of_Chainsaw_3() { return &___Chainsaw_3; }
	inline void set_Chainsaw_3(HeroCard_t3183847341 * value)
	{
		___Chainsaw_3 = value;
		Il2CppCodeGenWriteBarrier((&___Chainsaw_3), value);
	}

	inline static int32_t get_offset_of_Crowbar_4() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___Crowbar_4)); }
	inline HeroCard_t3183847341 * get_Crowbar_4() const { return ___Crowbar_4; }
	inline HeroCard_t3183847341 ** get_address_of_Crowbar_4() { return &___Crowbar_4; }
	inline void set_Crowbar_4(HeroCard_t3183847341 * value)
	{
		___Crowbar_4 = value;
		Il2CppCodeGenWriteBarrier((&___Crowbar_4), value);
	}

	inline static int32_t get_offset_of_DeputyTaylor_5() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___DeputyTaylor_5)); }
	inline HeroCard_t3183847341 * get_DeputyTaylor_5() const { return ___DeputyTaylor_5; }
	inline HeroCard_t3183847341 ** get_address_of_DeputyTaylor_5() { return &___DeputyTaylor_5; }
	inline void set_DeputyTaylor_5(HeroCard_t3183847341 * value)
	{
		___DeputyTaylor_5 = value;
		Il2CppCodeGenWriteBarrier((&___DeputyTaylor_5), value);
	}

	inline static int32_t get_offset_of_DocBrody_6() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___DocBrody_6)); }
	inline HeroCard_t3183847341 * get_DocBrody_6() const { return ___DocBrody_6; }
	inline HeroCard_t3183847341 ** get_address_of_DocBrody_6() { return &___DocBrody_6; }
	inline void set_DocBrody_6(HeroCard_t3183847341 * value)
	{
		___DocBrody_6 = value;
		Il2CppCodeGenWriteBarrier((&___DocBrody_6), value);
	}

	inline static int32_t get_offset_of_Faith_7() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___Faith_7)); }
	inline HeroCard_t3183847341 * get_Faith_7() const { return ___Faith_7; }
	inline HeroCard_t3183847341 ** get_address_of_Faith_7() { return &___Faith_7; }
	inline void set_Faith_7(HeroCard_t3183847341 * value)
	{
		___Faith_7 = value;
		Il2CppCodeGenWriteBarrier((&___Faith_7), value);
	}

	inline static int32_t get_offset_of_FarmerSty_8() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___FarmerSty_8)); }
	inline HeroCard_t3183847341 * get_FarmerSty_8() const { return ___FarmerSty_8; }
	inline HeroCard_t3183847341 ** get_address_of_FarmerSty_8() { return &___FarmerSty_8; }
	inline void set_FarmerSty_8(HeroCard_t3183847341 * value)
	{
		___FarmerSty_8 = value;
		Il2CppCodeGenWriteBarrier((&___FarmerSty_8), value);
	}

	inline static int32_t get_offset_of_FireAxe_9() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___FireAxe_9)); }
	inline HeroCard_t3183847341 * get_FireAxe_9() const { return ___FireAxe_9; }
	inline HeroCard_t3183847341 ** get_address_of_FireAxe_9() { return &___FireAxe_9; }
	inline void set_FireAxe_9(HeroCard_t3183847341 * value)
	{
		___FireAxe_9 = value;
		Il2CppCodeGenWriteBarrier((&___FireAxe_9), value);
	}

	inline static int32_t get_offset_of_FirstAid_10() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___FirstAid_10)); }
	inline HeroCard_t3183847341 * get_FirstAid_10() const { return ___FirstAid_10; }
	inline HeroCard_t3183847341 ** get_address_of_FirstAid_10() { return &___FirstAid_10; }
	inline void set_FirstAid_10(HeroCard_t3183847341 * value)
	{
		___FirstAid_10 = value;
		Il2CppCodeGenWriteBarrier((&___FirstAid_10), value);
	}

	inline static int32_t get_offset_of_GetBackYouDevils_11() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___GetBackYouDevils_11)); }
	inline HeroCard_t3183847341 * get_GetBackYouDevils_11() const { return ___GetBackYouDevils_11; }
	inline HeroCard_t3183847341 ** get_address_of_GetBackYouDevils_11() { return &___GetBackYouDevils_11; }
	inline void set_GetBackYouDevils_11(HeroCard_t3183847341 * value)
	{
		___GetBackYouDevils_11 = value;
		Il2CppCodeGenWriteBarrier((&___GetBackYouDevils_11), value);
	}

	inline static int32_t get_offset_of_Jeb_12() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___Jeb_12)); }
	inline HeroCard_t3183847341 * get_Jeb_12() const { return ___Jeb_12; }
	inline HeroCard_t3183847341 ** get_address_of_Jeb_12() { return &___Jeb_12; }
	inline void set_Jeb_12(HeroCard_t3183847341 * value)
	{
		___Jeb_12 = value;
		Il2CppCodeGenWriteBarrier((&___Jeb_12), value);
	}

	inline static int32_t get_offset_of_JustAScratch_13() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___JustAScratch_13)); }
	inline HeroCard_t3183847341 * get_JustAScratch_13() const { return ___JustAScratch_13; }
	inline HeroCard_t3183847341 ** get_address_of_JustAScratch_13() { return &___JustAScratch_13; }
	inline void set_JustAScratch_13(HeroCard_t3183847341 * value)
	{
		___JustAScratch_13 = value;
		Il2CppCodeGenWriteBarrier((&___JustAScratch_13), value);
	}

	inline static int32_t get_offset_of_Keys_14() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___Keys_14)); }
	inline HeroCard_t3183847341 * get_Keys_14() const { return ___Keys_14; }
	inline HeroCard_t3183847341 ** get_address_of_Keys_14() { return &___Keys_14; }
	inline void set_Keys_14(HeroCard_t3183847341 * value)
	{
		___Keys_14 = value;
		Il2CppCodeGenWriteBarrier((&___Keys_14), value);
	}

	inline static int32_t get_offset_of_MeatCleaver_15() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___MeatCleaver_15)); }
	inline HeroCard_t3183847341 * get_MeatCleaver_15() const { return ___MeatCleaver_15; }
	inline HeroCard_t3183847341 ** get_address_of_MeatCleaver_15() { return &___MeatCleaver_15; }
	inline void set_MeatCleaver_15(HeroCard_t3183847341 * value)
	{
		___MeatCleaver_15 = value;
		Il2CppCodeGenWriteBarrier((&___MeatCleaver_15), value);
	}

	inline static int32_t get_offset_of_MrHyde_16() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___MrHyde_16)); }
	inline HeroCard_t3183847341 * get_MrHyde_16() const { return ___MrHyde_16; }
	inline HeroCard_t3183847341 ** get_address_of_MrHyde_16() { return &___MrHyde_16; }
	inline void set_MrHyde_16(HeroCard_t3183847341 * value)
	{
		___MrHyde_16 = value;
		Il2CppCodeGenWriteBarrier((&___MrHyde_16), value);
	}

	inline static int32_t get_offset_of_Pitchfork_17() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___Pitchfork_17)); }
	inline HeroCard_t3183847341 * get_Pitchfork_17() const { return ___Pitchfork_17; }
	inline HeroCard_t3183847341 ** get_address_of_Pitchfork_17() { return &___Pitchfork_17; }
	inline void set_Pitchfork_17(HeroCard_t3183847341 * value)
	{
		___Pitchfork_17 = value;
		Il2CppCodeGenWriteBarrier((&___Pitchfork_17), value);
	}

	inline static int32_t get_offset_of_PrincipalGomez_18() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___PrincipalGomez_18)); }
	inline HeroCard_t3183847341 * get_PrincipalGomez_18() const { return ___PrincipalGomez_18; }
	inline HeroCard_t3183847341 ** get_address_of_PrincipalGomez_18() { return &___PrincipalGomez_18; }
	inline void set_PrincipalGomez_18(HeroCard_t3183847341 * value)
	{
		___PrincipalGomez_18 = value;
		Il2CppCodeGenWriteBarrier((&___PrincipalGomez_18), value);
	}

	inline static int32_t get_offset_of_PumpShotgun_19() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___PumpShotgun_19)); }
	inline HeroCard_t3183847341 * get_PumpShotgun_19() const { return ___PumpShotgun_19; }
	inline HeroCard_t3183847341 ** get_address_of_PumpShotgun_19() { return &___PumpShotgun_19; }
	inline void set_PumpShotgun_19(HeroCard_t3183847341 * value)
	{
		___PumpShotgun_19 = value;
		Il2CppCodeGenWriteBarrier((&___PumpShotgun_19), value);
	}

	inline static int32_t get_offset_of_Recovery_20() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___Recovery_20)); }
	inline HeroCard_t3183847341 * get_Recovery_20() const { return ___Recovery_20; }
	inline HeroCard_t3183847341 ** get_address_of_Recovery_20() { return &___Recovery_20; }
	inline void set_Recovery_20(HeroCard_t3183847341 * value)
	{
		___Recovery_20 = value;
		Il2CppCodeGenWriteBarrier((&___Recovery_20), value);
	}

	inline static int32_t get_offset_of_Revolver_21() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___Revolver_21)); }
	inline HeroCard_t3183847341 * get_Revolver_21() const { return ___Revolver_21; }
	inline HeroCard_t3183847341 ** get_address_of_Revolver_21() { return &___Revolver_21; }
	inline void set_Revolver_21(HeroCard_t3183847341 * value)
	{
		___Revolver_21 = value;
		Il2CppCodeGenWriteBarrier((&___Revolver_21), value);
	}

	inline static int32_t get_offset_of_SignalFlare_22() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___SignalFlare_22)); }
	inline HeroCard_t3183847341 * get_SignalFlare_22() const { return ___SignalFlare_22; }
	inline HeroCard_t3183847341 ** get_address_of_SignalFlare_22() { return &___SignalFlare_22; }
	inline void set_SignalFlare_22(HeroCard_t3183847341 * value)
	{
		___SignalFlare_22 = value;
		Il2CppCodeGenWriteBarrier((&___SignalFlare_22), value);
	}

	inline static int32_t get_offset_of_Torch_23() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___Torch_23)); }
	inline HeroCard_t3183847341 * get_Torch_23() const { return ___Torch_23; }
	inline HeroCard_t3183847341 ** get_address_of_Torch_23() { return &___Torch_23; }
	inline void set_Torch_23(HeroCard_t3183847341 * value)
	{
		___Torch_23 = value;
		Il2CppCodeGenWriteBarrier((&___Torch_23), value);
	}

	inline static int32_t get_offset_of_WeldingTorch_24() { return static_cast<int32_t>(offsetof(BasicHeroCards_t2570609810_StaticFields, ___WeldingTorch_24)); }
	inline HeroCard_t3183847341 * get_WeldingTorch_24() const { return ___WeldingTorch_24; }
	inline HeroCard_t3183847341 ** get_address_of_WeldingTorch_24() { return &___WeldingTorch_24; }
	inline void set_WeldingTorch_24(HeroCard_t3183847341 * value)
	{
		___WeldingTorch_24 = value;
		Il2CppCodeGenWriteBarrier((&___WeldingTorch_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICHEROCARDS_T2570609810_H
#ifndef U3CUISAVEFILEEXISTU3EC__ANONSTOREY0_T3461180196_H
#define U3CUISAVEFILEEXISTU3EC__ANONSTOREY0_T3461180196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CampaignLoadScene/<UISaveFileExist>c__AnonStorey0
struct  U3CUISaveFileExistU3Ec__AnonStorey0_t3461180196  : public RuntimeObject
{
public:
	// System.String CampaignLoadScene/<UISaveFileExist>c__AnonStorey0::saveName
	String_t* ___saveName_0;
	// CampaignLoadScene CampaignLoadScene/<UISaveFileExist>c__AnonStorey0::$this
	CampaignLoadScene_t453142025 * ___U24this_1;

public:
	inline static int32_t get_offset_of_saveName_0() { return static_cast<int32_t>(offsetof(U3CUISaveFileExistU3Ec__AnonStorey0_t3461180196, ___saveName_0)); }
	inline String_t* get_saveName_0() const { return ___saveName_0; }
	inline String_t** get_address_of_saveName_0() { return &___saveName_0; }
	inline void set_saveName_0(String_t* value)
	{
		___saveName_0 = value;
		Il2CppCodeGenWriteBarrier((&___saveName_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CUISaveFileExistU3Ec__AnonStorey0_t3461180196, ___U24this_1)); }
	inline CampaignLoadScene_t453142025 * get_U24this_1() const { return ___U24this_1; }
	inline CampaignLoadScene_t453142025 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CampaignLoadScene_t453142025 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUISAVEFILEEXISTU3EC__ANONSTOREY0_T3461180196_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef GSFUJSONHELPER_T2039638717_H
#define GSFUJSONHELPER_T2039638717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GSFUJsonHelper
struct  GSFUJsonHelper_t2039638717  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSFUJSONHELPER_T2039638717_H
#ifndef ACTIONS_T3679456191_H
#define ACTIONS_T3679456191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Actions
struct  Actions_t3679456191  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONS_T3679456191_H
#ifndef U3CUISAVEFILENOTEXISTU3EC__ANONSTOREY1_T2060043828_H
#define U3CUISAVEFILENOTEXISTU3EC__ANONSTOREY1_T2060043828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CampaignLoadScene/<UISaveFileNotExist>c__AnonStorey1
struct  U3CUISaveFileNotExistU3Ec__AnonStorey1_t2060043828  : public RuntimeObject
{
public:
	// System.String CampaignLoadScene/<UISaveFileNotExist>c__AnonStorey1::saveName
	String_t* ___saveName_0;
	// CampaignLoadScene CampaignLoadScene/<UISaveFileNotExist>c__AnonStorey1::$this
	CampaignLoadScene_t453142025 * ___U24this_1;

public:
	inline static int32_t get_offset_of_saveName_0() { return static_cast<int32_t>(offsetof(U3CUISaveFileNotExistU3Ec__AnonStorey1_t2060043828, ___saveName_0)); }
	inline String_t* get_saveName_0() const { return ___saveName_0; }
	inline String_t** get_address_of_saveName_0() { return &___saveName_0; }
	inline void set_saveName_0(String_t* value)
	{
		___saveName_0 = value;
		Il2CppCodeGenWriteBarrier((&___saveName_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CUISaveFileNotExistU3Ec__AnonStorey1_t2060043828, ___U24this_1)); }
	inline CampaignLoadScene_t453142025 * get_U24this_1() const { return ___U24this_1; }
	inline CampaignLoadScene_t453142025 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CampaignLoadScene_t453142025 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUISAVEFILENOTEXISTU3EC__ANONSTOREY1_T2060043828_H
#ifndef AUTOSAVE_T3127297172_H
#define AUTOSAVE_T3127297172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoSave
struct  AutoSave_t3127297172  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOSAVE_T3127297172_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef PERCEPTRON_T1002209887_H
#define PERCEPTRON_T1002209887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Perceptron
struct  Perceptron_t1002209887  : public RuntimeObject
{
public:
	// System.Int32 Perceptron::numInput
	int32_t ___numInput_0;
	// System.Double[] Perceptron::inputs
	DoubleU5BU5D_t3413330114* ___inputs_1;
	// System.Double[] Perceptron::weights
	DoubleU5BU5D_t3413330114* ___weights_2;
	// System.Double Perceptron::bias
	double ___bias_3;
	// System.Random Perceptron::rnd
	Random_t108471755 * ___rnd_4;

public:
	inline static int32_t get_offset_of_numInput_0() { return static_cast<int32_t>(offsetof(Perceptron_t1002209887, ___numInput_0)); }
	inline int32_t get_numInput_0() const { return ___numInput_0; }
	inline int32_t* get_address_of_numInput_0() { return &___numInput_0; }
	inline void set_numInput_0(int32_t value)
	{
		___numInput_0 = value;
	}

	inline static int32_t get_offset_of_inputs_1() { return static_cast<int32_t>(offsetof(Perceptron_t1002209887, ___inputs_1)); }
	inline DoubleU5BU5D_t3413330114* get_inputs_1() const { return ___inputs_1; }
	inline DoubleU5BU5D_t3413330114** get_address_of_inputs_1() { return &___inputs_1; }
	inline void set_inputs_1(DoubleU5BU5D_t3413330114* value)
	{
		___inputs_1 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_1), value);
	}

	inline static int32_t get_offset_of_weights_2() { return static_cast<int32_t>(offsetof(Perceptron_t1002209887, ___weights_2)); }
	inline DoubleU5BU5D_t3413330114* get_weights_2() const { return ___weights_2; }
	inline DoubleU5BU5D_t3413330114** get_address_of_weights_2() { return &___weights_2; }
	inline void set_weights_2(DoubleU5BU5D_t3413330114* value)
	{
		___weights_2 = value;
		Il2CppCodeGenWriteBarrier((&___weights_2), value);
	}

	inline static int32_t get_offset_of_bias_3() { return static_cast<int32_t>(offsetof(Perceptron_t1002209887, ___bias_3)); }
	inline double get_bias_3() const { return ___bias_3; }
	inline double* get_address_of_bias_3() { return &___bias_3; }
	inline void set_bias_3(double value)
	{
		___bias_3 = value;
	}

	inline static int32_t get_offset_of_rnd_4() { return static_cast<int32_t>(offsetof(Perceptron_t1002209887, ___rnd_4)); }
	inline Random_t108471755 * get_rnd_4() const { return ___rnd_4; }
	inline Random_t108471755 ** get_address_of_rnd_4() { return &___rnd_4; }
	inline void set_rnd_4(Random_t108471755 * value)
	{
		___rnd_4 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERCEPTRON_T1002209887_H
#ifndef U3CGAUSSNORMALU3EC__ANONSTOREY0_T3607787937_H
#define U3CGAUSSNORMALU3EC__ANONSTOREY0_T3607787937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Normalize/<GaussNormal>c__AnonStorey0
struct  U3CGaussNormalU3Ec__AnonStorey0_t3607787937  : public RuntimeObject
{
public:
	// System.Int32 Normalize/<GaussNormal>c__AnonStorey0::column
	int32_t ___column_0;
	// System.Double Normalize/<GaussNormal>c__AnonStorey0::mean
	double ___mean_1;

public:
	inline static int32_t get_offset_of_column_0() { return static_cast<int32_t>(offsetof(U3CGaussNormalU3Ec__AnonStorey0_t3607787937, ___column_0)); }
	inline int32_t get_column_0() const { return ___column_0; }
	inline int32_t* get_address_of_column_0() { return &___column_0; }
	inline void set_column_0(int32_t value)
	{
		___column_0 = value;
	}

	inline static int32_t get_offset_of_mean_1() { return static_cast<int32_t>(offsetof(U3CGaussNormalU3Ec__AnonStorey0_t3607787937, ___mean_1)); }
	inline double get_mean_1() const { return ___mean_1; }
	inline double* get_address_of_mean_1() { return &___mean_1; }
	inline void set_mean_1(double value)
	{
		___mean_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGAUSSNORMALU3EC__ANONSTOREY0_T3607787937_H
#ifndef UNITYEVENT_3_T1959629666_H
#define UNITYEVENT_3_T1959629666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<CloudConnectorCore/QueryType,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<System.String>>
struct  UnityEvent_3_t1959629666  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1959629666, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1959629666_H
#ifndef UNITYEVENT_1_T2729110193_H
#define UNITYEVENT_1_T2729110193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2729110193  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2729110193, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2729110193_H
#ifndef PLAYERINFO_T3792932304_H
#define PLAYERINFO_T3792932304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GSFU_Demo_Utils/PlayerInfo
struct  PlayerInfo_t3792932304 
{
public:
	// System.String GSFU_Demo_Utils/PlayerInfo::name
	String_t* ___name_0;
	// System.Int32 GSFU_Demo_Utils/PlayerInfo::level
	int32_t ___level_1;
	// System.Single GSFU_Demo_Utils/PlayerInfo::health
	float ___health_2;
	// System.String GSFU_Demo_Utils/PlayerInfo::role
	String_t* ___role_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(PlayerInfo_t3792932304, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(PlayerInfo_t3792932304, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}

	inline static int32_t get_offset_of_health_2() { return static_cast<int32_t>(offsetof(PlayerInfo_t3792932304, ___health_2)); }
	inline float get_health_2() const { return ___health_2; }
	inline float* get_address_of_health_2() { return &___health_2; }
	inline void set_health_2(float value)
	{
		___health_2 = value;
	}

	inline static int32_t get_offset_of_role_3() { return static_cast<int32_t>(offsetof(PlayerInfo_t3792932304, ___role_3)); }
	inline String_t* get_role_3() const { return ___role_3; }
	inline String_t** get_address_of_role_3() { return &___role_3; }
	inline void set_role_3(String_t* value)
	{
		___role_3 = value;
		Il2CppCodeGenWriteBarrier((&___role_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GSFU_Demo_Utils/PlayerInfo
struct PlayerInfo_t3792932304_marshaled_pinvoke
{
	char* ___name_0;
	int32_t ___level_1;
	float ___health_2;
	char* ___role_3;
};
// Native definition for COM marshalling of GSFU_Demo_Utils/PlayerInfo
struct PlayerInfo_t3792932304_marshaled_com
{
	Il2CppChar* ___name_0;
	int32_t ___level_1;
	float ___health_2;
	Il2CppChar* ___role_3;
};
#endif // PLAYERINFO_T3792932304_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef GAMELOG_T3659206319_H
#define GAMELOG_T3659206319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GsfuUtilsLnoe/GameLog
struct  GameLog_t3659206319 
{
public:
	// System.String GsfuUtilsLnoe/GameLog::GameSessionName
	String_t* ___GameSessionName_0;
	// System.String GsfuUtilsLnoe/GameLog::Mission
	String_t* ___Mission_1;
	// System.String GsfuUtilsLnoe/GameLog::Buildings
	String_t* ___Buildings_2;
	// System.String GsfuUtilsLnoe/GameLog::Heroes
	String_t* ___Heroes_3;
	// System.Int32 GsfuUtilsLnoe/GameLog::RoundsLeft
	int32_t ___RoundsLeft_4;
	// System.Int32 GsfuUtilsLnoe/GameLog::ZombiesDead
	int32_t ___ZombiesDead_5;
	// System.Int32 GsfuUtilsLnoe/GameLog::ZombiesOnBoard
	int32_t ___ZombiesOnBoard_6;
	// System.Int32 GsfuUtilsLnoe/GameLog::HeroesDead
	int32_t ___HeroesDead_7;
	// System.Int32 GsfuUtilsLnoe/GameLog::BuildingsTakenOver
	int32_t ___BuildingsTakenOver_8;
	// System.Int32 GsfuUtilsLnoe/GameLog::BuildingsLightsOut
	int32_t ___BuildingsLightsOut_9;
	// System.Int32 GsfuUtilsLnoe/GameLog::SpawningPits
	int32_t ___SpawningPits_10;
	// System.String GsfuUtilsLnoe/GameLog::GameWon
	String_t* ___GameWon_11;

public:
	inline static int32_t get_offset_of_GameSessionName_0() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___GameSessionName_0)); }
	inline String_t* get_GameSessionName_0() const { return ___GameSessionName_0; }
	inline String_t** get_address_of_GameSessionName_0() { return &___GameSessionName_0; }
	inline void set_GameSessionName_0(String_t* value)
	{
		___GameSessionName_0 = value;
		Il2CppCodeGenWriteBarrier((&___GameSessionName_0), value);
	}

	inline static int32_t get_offset_of_Mission_1() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___Mission_1)); }
	inline String_t* get_Mission_1() const { return ___Mission_1; }
	inline String_t** get_address_of_Mission_1() { return &___Mission_1; }
	inline void set_Mission_1(String_t* value)
	{
		___Mission_1 = value;
		Il2CppCodeGenWriteBarrier((&___Mission_1), value);
	}

	inline static int32_t get_offset_of_Buildings_2() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___Buildings_2)); }
	inline String_t* get_Buildings_2() const { return ___Buildings_2; }
	inline String_t** get_address_of_Buildings_2() { return &___Buildings_2; }
	inline void set_Buildings_2(String_t* value)
	{
		___Buildings_2 = value;
		Il2CppCodeGenWriteBarrier((&___Buildings_2), value);
	}

	inline static int32_t get_offset_of_Heroes_3() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___Heroes_3)); }
	inline String_t* get_Heroes_3() const { return ___Heroes_3; }
	inline String_t** get_address_of_Heroes_3() { return &___Heroes_3; }
	inline void set_Heroes_3(String_t* value)
	{
		___Heroes_3 = value;
		Il2CppCodeGenWriteBarrier((&___Heroes_3), value);
	}

	inline static int32_t get_offset_of_RoundsLeft_4() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___RoundsLeft_4)); }
	inline int32_t get_RoundsLeft_4() const { return ___RoundsLeft_4; }
	inline int32_t* get_address_of_RoundsLeft_4() { return &___RoundsLeft_4; }
	inline void set_RoundsLeft_4(int32_t value)
	{
		___RoundsLeft_4 = value;
	}

	inline static int32_t get_offset_of_ZombiesDead_5() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___ZombiesDead_5)); }
	inline int32_t get_ZombiesDead_5() const { return ___ZombiesDead_5; }
	inline int32_t* get_address_of_ZombiesDead_5() { return &___ZombiesDead_5; }
	inline void set_ZombiesDead_5(int32_t value)
	{
		___ZombiesDead_5 = value;
	}

	inline static int32_t get_offset_of_ZombiesOnBoard_6() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___ZombiesOnBoard_6)); }
	inline int32_t get_ZombiesOnBoard_6() const { return ___ZombiesOnBoard_6; }
	inline int32_t* get_address_of_ZombiesOnBoard_6() { return &___ZombiesOnBoard_6; }
	inline void set_ZombiesOnBoard_6(int32_t value)
	{
		___ZombiesOnBoard_6 = value;
	}

	inline static int32_t get_offset_of_HeroesDead_7() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___HeroesDead_7)); }
	inline int32_t get_HeroesDead_7() const { return ___HeroesDead_7; }
	inline int32_t* get_address_of_HeroesDead_7() { return &___HeroesDead_7; }
	inline void set_HeroesDead_7(int32_t value)
	{
		___HeroesDead_7 = value;
	}

	inline static int32_t get_offset_of_BuildingsTakenOver_8() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___BuildingsTakenOver_8)); }
	inline int32_t get_BuildingsTakenOver_8() const { return ___BuildingsTakenOver_8; }
	inline int32_t* get_address_of_BuildingsTakenOver_8() { return &___BuildingsTakenOver_8; }
	inline void set_BuildingsTakenOver_8(int32_t value)
	{
		___BuildingsTakenOver_8 = value;
	}

	inline static int32_t get_offset_of_BuildingsLightsOut_9() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___BuildingsLightsOut_9)); }
	inline int32_t get_BuildingsLightsOut_9() const { return ___BuildingsLightsOut_9; }
	inline int32_t* get_address_of_BuildingsLightsOut_9() { return &___BuildingsLightsOut_9; }
	inline void set_BuildingsLightsOut_9(int32_t value)
	{
		___BuildingsLightsOut_9 = value;
	}

	inline static int32_t get_offset_of_SpawningPits_10() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___SpawningPits_10)); }
	inline int32_t get_SpawningPits_10() const { return ___SpawningPits_10; }
	inline int32_t* get_address_of_SpawningPits_10() { return &___SpawningPits_10; }
	inline void set_SpawningPits_10(int32_t value)
	{
		___SpawningPits_10 = value;
	}

	inline static int32_t get_offset_of_GameWon_11() { return static_cast<int32_t>(offsetof(GameLog_t3659206319, ___GameWon_11)); }
	inline String_t* get_GameWon_11() const { return ___GameWon_11; }
	inline String_t** get_address_of_GameWon_11() { return &___GameWon_11; }
	inline void set_GameWon_11(String_t* value)
	{
		___GameWon_11 = value;
		Il2CppCodeGenWriteBarrier((&___GameWon_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GsfuUtilsLnoe/GameLog
struct GameLog_t3659206319_marshaled_pinvoke
{
	char* ___GameSessionName_0;
	char* ___Mission_1;
	char* ___Buildings_2;
	char* ___Heroes_3;
	int32_t ___RoundsLeft_4;
	int32_t ___ZombiesDead_5;
	int32_t ___ZombiesOnBoard_6;
	int32_t ___HeroesDead_7;
	int32_t ___BuildingsTakenOver_8;
	int32_t ___BuildingsLightsOut_9;
	int32_t ___SpawningPits_10;
	char* ___GameWon_11;
};
// Native definition for COM marshalling of GsfuUtilsLnoe/GameLog
struct GameLog_t3659206319_marshaled_com
{
	Il2CppChar* ___GameSessionName_0;
	Il2CppChar* ___Mission_1;
	Il2CppChar* ___Buildings_2;
	Il2CppChar* ___Heroes_3;
	int32_t ___RoundsLeft_4;
	int32_t ___ZombiesDead_5;
	int32_t ___ZombiesOnBoard_6;
	int32_t ___HeroesDead_7;
	int32_t ___BuildingsTakenOver_8;
	int32_t ___BuildingsLightsOut_9;
	int32_t ___SpawningPits_10;
	Il2CppChar* ___GameWon_11;
};
#endif // GAMELOG_T3659206319_H
#ifndef NETWORKSETTINGS_T4022558215_H
#define NETWORKSETTINGS_T4022558215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GsfuUtilsLnoe/NetworkSettings
struct  NetworkSettings_t4022558215 
{
public:
	// System.String GsfuUtilsLnoe/NetworkSettings::NumInputs
	String_t* ___NumInputs_0;
	// System.String GsfuUtilsLnoe/NetworkSettings::NumRbfNodes
	String_t* ___NumRbfNodes_1;
	// System.String GsfuUtilsLnoe/NetworkSettings::NumOutputs
	String_t* ___NumOutputs_2;
	// System.String GsfuUtilsLnoe/NetworkSettings::Centers
	String_t* ___Centers_3;
	// System.String GsfuUtilsLnoe/NetworkSettings::Width
	String_t* ___Width_4;
	// System.String GsfuUtilsLnoe/NetworkSettings::Weights
	String_t* ___Weights_5;
	// System.String GsfuUtilsLnoe/NetworkSettings::Bias
	String_t* ___Bias_6;
	// System.String GsfuUtilsLnoe/NetworkSettings::InputRangeLow
	String_t* ___InputRangeLow_7;
	// System.String GsfuUtilsLnoe/NetworkSettings::InputRangeHigh
	String_t* ___InputRangeHigh_8;

public:
	inline static int32_t get_offset_of_NumInputs_0() { return static_cast<int32_t>(offsetof(NetworkSettings_t4022558215, ___NumInputs_0)); }
	inline String_t* get_NumInputs_0() const { return ___NumInputs_0; }
	inline String_t** get_address_of_NumInputs_0() { return &___NumInputs_0; }
	inline void set_NumInputs_0(String_t* value)
	{
		___NumInputs_0 = value;
		Il2CppCodeGenWriteBarrier((&___NumInputs_0), value);
	}

	inline static int32_t get_offset_of_NumRbfNodes_1() { return static_cast<int32_t>(offsetof(NetworkSettings_t4022558215, ___NumRbfNodes_1)); }
	inline String_t* get_NumRbfNodes_1() const { return ___NumRbfNodes_1; }
	inline String_t** get_address_of_NumRbfNodes_1() { return &___NumRbfNodes_1; }
	inline void set_NumRbfNodes_1(String_t* value)
	{
		___NumRbfNodes_1 = value;
		Il2CppCodeGenWriteBarrier((&___NumRbfNodes_1), value);
	}

	inline static int32_t get_offset_of_NumOutputs_2() { return static_cast<int32_t>(offsetof(NetworkSettings_t4022558215, ___NumOutputs_2)); }
	inline String_t* get_NumOutputs_2() const { return ___NumOutputs_2; }
	inline String_t** get_address_of_NumOutputs_2() { return &___NumOutputs_2; }
	inline void set_NumOutputs_2(String_t* value)
	{
		___NumOutputs_2 = value;
		Il2CppCodeGenWriteBarrier((&___NumOutputs_2), value);
	}

	inline static int32_t get_offset_of_Centers_3() { return static_cast<int32_t>(offsetof(NetworkSettings_t4022558215, ___Centers_3)); }
	inline String_t* get_Centers_3() const { return ___Centers_3; }
	inline String_t** get_address_of_Centers_3() { return &___Centers_3; }
	inline void set_Centers_3(String_t* value)
	{
		___Centers_3 = value;
		Il2CppCodeGenWriteBarrier((&___Centers_3), value);
	}

	inline static int32_t get_offset_of_Width_4() { return static_cast<int32_t>(offsetof(NetworkSettings_t4022558215, ___Width_4)); }
	inline String_t* get_Width_4() const { return ___Width_4; }
	inline String_t** get_address_of_Width_4() { return &___Width_4; }
	inline void set_Width_4(String_t* value)
	{
		___Width_4 = value;
		Il2CppCodeGenWriteBarrier((&___Width_4), value);
	}

	inline static int32_t get_offset_of_Weights_5() { return static_cast<int32_t>(offsetof(NetworkSettings_t4022558215, ___Weights_5)); }
	inline String_t* get_Weights_5() const { return ___Weights_5; }
	inline String_t** get_address_of_Weights_5() { return &___Weights_5; }
	inline void set_Weights_5(String_t* value)
	{
		___Weights_5 = value;
		Il2CppCodeGenWriteBarrier((&___Weights_5), value);
	}

	inline static int32_t get_offset_of_Bias_6() { return static_cast<int32_t>(offsetof(NetworkSettings_t4022558215, ___Bias_6)); }
	inline String_t* get_Bias_6() const { return ___Bias_6; }
	inline String_t** get_address_of_Bias_6() { return &___Bias_6; }
	inline void set_Bias_6(String_t* value)
	{
		___Bias_6 = value;
		Il2CppCodeGenWriteBarrier((&___Bias_6), value);
	}

	inline static int32_t get_offset_of_InputRangeLow_7() { return static_cast<int32_t>(offsetof(NetworkSettings_t4022558215, ___InputRangeLow_7)); }
	inline String_t* get_InputRangeLow_7() const { return ___InputRangeLow_7; }
	inline String_t** get_address_of_InputRangeLow_7() { return &___InputRangeLow_7; }
	inline void set_InputRangeLow_7(String_t* value)
	{
		___InputRangeLow_7 = value;
		Il2CppCodeGenWriteBarrier((&___InputRangeLow_7), value);
	}

	inline static int32_t get_offset_of_InputRangeHigh_8() { return static_cast<int32_t>(offsetof(NetworkSettings_t4022558215, ___InputRangeHigh_8)); }
	inline String_t* get_InputRangeHigh_8() const { return ___InputRangeHigh_8; }
	inline String_t** get_address_of_InputRangeHigh_8() { return &___InputRangeHigh_8; }
	inline void set_InputRangeHigh_8(String_t* value)
	{
		___InputRangeHigh_8 = value;
		Il2CppCodeGenWriteBarrier((&___InputRangeHigh_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GsfuUtilsLnoe/NetworkSettings
struct NetworkSettings_t4022558215_marshaled_pinvoke
{
	char* ___NumInputs_0;
	char* ___NumRbfNodes_1;
	char* ___NumOutputs_2;
	char* ___Centers_3;
	char* ___Width_4;
	char* ___Weights_5;
	char* ___Bias_6;
	char* ___InputRangeLow_7;
	char* ___InputRangeHigh_8;
};
// Native definition for COM marshalling of GsfuUtilsLnoe/NetworkSettings
struct NetworkSettings_t4022558215_marshaled_com
{
	Il2CppChar* ___NumInputs_0;
	Il2CppChar* ___NumRbfNodes_1;
	Il2CppChar* ___NumOutputs_2;
	Il2CppChar* ___Centers_3;
	Il2CppChar* ___Width_4;
	Il2CppChar* ___Weights_5;
	Il2CppChar* ___Bias_6;
	Il2CppChar* ___InputRangeLow_7;
	Il2CppChar* ___InputRangeHigh_8;
};
#endif // NETWORKSETTINGS_T4022558215_H
#ifndef CAMPAIGNSTATE_T3637050575_H
#define CAMPAIGNSTATE_T3637050575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CampaignState
struct  CampaignState_t3637050575 
{
public:
	// System.Int32 CampaignState::MissionIndex
	int32_t ___MissionIndex_0;
	// System.Collections.Generic.List`1<System.String> CampaignState::HeroesDied
	List_1_t3319525431 * ___HeroesDied_1;

public:
	inline static int32_t get_offset_of_MissionIndex_0() { return static_cast<int32_t>(offsetof(CampaignState_t3637050575, ___MissionIndex_0)); }
	inline int32_t get_MissionIndex_0() const { return ___MissionIndex_0; }
	inline int32_t* get_address_of_MissionIndex_0() { return &___MissionIndex_0; }
	inline void set_MissionIndex_0(int32_t value)
	{
		___MissionIndex_0 = value;
	}

	inline static int32_t get_offset_of_HeroesDied_1() { return static_cast<int32_t>(offsetof(CampaignState_t3637050575, ___HeroesDied_1)); }
	inline List_1_t3319525431 * get_HeroesDied_1() const { return ___HeroesDied_1; }
	inline List_1_t3319525431 ** get_address_of_HeroesDied_1() { return &___HeroesDied_1; }
	inline void set_HeroesDied_1(List_1_t3319525431 * value)
	{
		___HeroesDied_1 = value;
		Il2CppCodeGenWriteBarrier((&___HeroesDied_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CampaignState
struct CampaignState_t3637050575_marshaled_pinvoke
{
	int32_t ___MissionIndex_0;
	List_1_t3319525431 * ___HeroesDied_1;
};
// Native definition for COM marshalling of CampaignState
struct CampaignState_t3637050575_marshaled_com
{
	int32_t ___MissionIndex_0;
	List_1_t3319525431 * ___HeroesDied_1;
};
#endif // CAMPAIGNSTATE_T3637050575_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef GENDERTYPE_T1790903313_H
#define GENDERTYPE_T1790903313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Hero/GenderType
struct  GenderType_t1790903313 
{
public:
	// System.Int32 Hero/GenderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GenderType_t1790903313, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENDERTYPE_T1790903313_H
#ifndef ZOMBIETURNPHASENAME_T860305160_H
#define ZOMBIETURNPHASENAME_T860305160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTurnPhaseName
struct  ZombieTurnPhaseName_t860305160 
{
public:
	// System.Int32 ZombieTurnPhaseName::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ZombieTurnPhaseName_t860305160, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIETURNPHASENAME_T860305160_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef KEYWORDTYPE_T3224742114_H
#define KEYWORDTYPE_T3224742114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Hero/KeywordType
struct  KeywordType_t3224742114 
{
public:
	// System.Int32 Hero/KeywordType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeywordType_t3224742114, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYWORDTYPE_T3224742114_H
#ifndef CALLBACKEVENTRAW_T2706945269_H
#define CALLBACKEVENTRAW_T2706945269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudConnectorCore/CallBackEventRaw
struct  CallBackEventRaw_t2706945269  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKEVENTRAW_T2706945269_H
#ifndef GSFUUTILSLNOE_T3057049897_H
#define GSFUUTILSLNOE_T3057049897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GsfuUtilsLnoe
struct  GsfuUtilsLnoe_t3057049897  : public RuntimeObject
{
public:

public:
};

struct GsfuUtilsLnoe_t3057049897_StaticFields
{
public:
	// System.String[][] GsfuUtilsLnoe::TrainData
	StringU5BU5DU5BU5D_t2611993717* ___TrainData_0;
	// System.Int32 GsfuUtilsLnoe::NetworkSettingsLength
	int32_t ___NetworkSettingsLength_1;
	// GsfuUtilsLnoe/GameLog GsfuUtilsLnoe::gameLog
	GameLog_t3659206319  ___gameLog_2;

public:
	inline static int32_t get_offset_of_TrainData_0() { return static_cast<int32_t>(offsetof(GsfuUtilsLnoe_t3057049897_StaticFields, ___TrainData_0)); }
	inline StringU5BU5DU5BU5D_t2611993717* get_TrainData_0() const { return ___TrainData_0; }
	inline StringU5BU5DU5BU5D_t2611993717** get_address_of_TrainData_0() { return &___TrainData_0; }
	inline void set_TrainData_0(StringU5BU5DU5BU5D_t2611993717* value)
	{
		___TrainData_0 = value;
		Il2CppCodeGenWriteBarrier((&___TrainData_0), value);
	}

	inline static int32_t get_offset_of_NetworkSettingsLength_1() { return static_cast<int32_t>(offsetof(GsfuUtilsLnoe_t3057049897_StaticFields, ___NetworkSettingsLength_1)); }
	inline int32_t get_NetworkSettingsLength_1() const { return ___NetworkSettingsLength_1; }
	inline int32_t* get_address_of_NetworkSettingsLength_1() { return &___NetworkSettingsLength_1; }
	inline void set_NetworkSettingsLength_1(int32_t value)
	{
		___NetworkSettingsLength_1 = value;
	}

	inline static int32_t get_offset_of_gameLog_2() { return static_cast<int32_t>(offsetof(GsfuUtilsLnoe_t3057049897_StaticFields, ___gameLog_2)); }
	inline GameLog_t3659206319  get_gameLog_2() const { return ___gameLog_2; }
	inline GameLog_t3659206319 * get_address_of_gameLog_2() { return &___gameLog_2; }
	inline void set_gameLog_2(GameLog_t3659206319  value)
	{
		___gameLog_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSFUUTILSLNOE_T3057049897_H
#ifndef TYPE_T2881192505_H
#define TYPE_T2881192505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSaveData/GameLogEntries/Type
struct  Type_t2881192505 
{
public:
	// System.Int32 GameSaveData/GameLogEntries/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t2881192505, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T2881192505_H
#ifndef HEROCARDTYPES_T1515449340_H
#define HEROCARDTYPES_T1515449340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroCard/HeroCardTypes
struct  HeroCardTypes_t1515449340 
{
public:
	// System.Int32 HeroCard/HeroCardTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HeroCardTypes_t1515449340, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROCARDTYPES_T1515449340_H
#ifndef CALLBACKEVENTPROCESSED_T4150531253_H
#define CALLBACKEVENTPROCESSED_T4150531253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloudConnectorCore/CallBackEventProcessed
struct  CallBackEventProcessed_t4150531253  : public UnityEvent_3_t1959629666
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKEVENTPROCESSED_T4150531253_H
#ifndef GSFU_DEMO_UTILS_T2030822237_H
#define GSFU_DEMO_UTILS_T2030822237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GSFU_Demo_Utils
struct  GSFU_Demo_Utils_t2030822237  : public RuntimeObject
{
public:

public:
};

struct GSFU_Demo_Utils_t2030822237_StaticFields
{
public:
	// GSFU_Demo_Utils/PlayerInfo GSFU_Demo_Utils::player
	PlayerInfo_t3792932304  ___player_0;
	// System.String GSFU_Demo_Utils::tableName
	String_t* ___tableName_1;

public:
	inline static int32_t get_offset_of_player_0() { return static_cast<int32_t>(offsetof(GSFU_Demo_Utils_t2030822237_StaticFields, ___player_0)); }
	inline PlayerInfo_t3792932304  get_player_0() const { return ___player_0; }
	inline PlayerInfo_t3792932304 * get_address_of_player_0() { return &___player_0; }
	inline void set_player_0(PlayerInfo_t3792932304  value)
	{
		___player_0 = value;
	}

	inline static int32_t get_offset_of_tableName_1() { return static_cast<int32_t>(offsetof(GSFU_Demo_Utils_t2030822237_StaticFields, ___tableName_1)); }
	inline String_t* get_tableName_1() const { return ___tableName_1; }
	inline String_t** get_address_of_tableName_1() { return &___tableName_1; }
	inline void set_tableName_1(String_t* value)
	{
		___tableName_1 = value;
		Il2CppCodeGenWriteBarrier((&___tableName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSFU_DEMO_UTILS_T2030822237_H
#ifndef CAMPAIGNMANAGER_T2990572144_H
#define CAMPAIGNMANAGER_T2990572144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CampaignManager
struct  CampaignManager_t2990572144  : public RuntimeObject
{
public:
	// System.String CampaignManager::zombieCampaignObjective
	String_t* ___zombieCampaignObjective_2;
	// System.Collections.Generic.List`1<System.String> CampaignManager::heroCampaignItems
	List_1_t3319525431 * ___heroCampaignItems_3;

public:
	inline static int32_t get_offset_of_zombieCampaignObjective_2() { return static_cast<int32_t>(offsetof(CampaignManager_t2990572144, ___zombieCampaignObjective_2)); }
	inline String_t* get_zombieCampaignObjective_2() const { return ___zombieCampaignObjective_2; }
	inline String_t** get_address_of_zombieCampaignObjective_2() { return &___zombieCampaignObjective_2; }
	inline void set_zombieCampaignObjective_2(String_t* value)
	{
		___zombieCampaignObjective_2 = value;
		Il2CppCodeGenWriteBarrier((&___zombieCampaignObjective_2), value);
	}

	inline static int32_t get_offset_of_heroCampaignItems_3() { return static_cast<int32_t>(offsetof(CampaignManager_t2990572144, ___heroCampaignItems_3)); }
	inline List_1_t3319525431 * get_heroCampaignItems_3() const { return ___heroCampaignItems_3; }
	inline List_1_t3319525431 ** get_address_of_heroCampaignItems_3() { return &___heroCampaignItems_3; }
	inline void set_heroCampaignItems_3(List_1_t3319525431 * value)
	{
		___heroCampaignItems_3 = value;
		Il2CppCodeGenWriteBarrier((&___heroCampaignItems_3), value);
	}
};

struct CampaignManager_t2990572144_StaticFields
{
public:
	// System.Collections.Generic.List`1<Mission> CampaignManager::CampaignMissions
	List_1_t1410578621 * ___CampaignMissions_0;
	// System.Collections.Generic.List`1<System.String> CampaignManager::heroesDiedInCampaign
	List_1_t3319525431 * ___heroesDiedInCampaign_1;
	// System.Int32 CampaignManager::currentMissionIndex
	int32_t ___currentMissionIndex_5;
	// System.Int32 CampaignManager::heroDeathCount
	int32_t ___heroDeathCount_6;
	// CampaignState CampaignManager::campaignState
	CampaignState_t3637050575  ___campaignState_7;
	// System.String CampaignManager::lastCampaignFile
	String_t* ___lastCampaignFile_8;
	// System.Boolean CampaignManager::campaignOver
	bool ___campaignOver_9;
	// System.Boolean CampaignManager::heroesWon
	bool ___heroesWon_10;

public:
	inline static int32_t get_offset_of_CampaignMissions_0() { return static_cast<int32_t>(offsetof(CampaignManager_t2990572144_StaticFields, ___CampaignMissions_0)); }
	inline List_1_t1410578621 * get_CampaignMissions_0() const { return ___CampaignMissions_0; }
	inline List_1_t1410578621 ** get_address_of_CampaignMissions_0() { return &___CampaignMissions_0; }
	inline void set_CampaignMissions_0(List_1_t1410578621 * value)
	{
		___CampaignMissions_0 = value;
		Il2CppCodeGenWriteBarrier((&___CampaignMissions_0), value);
	}

	inline static int32_t get_offset_of_heroesDiedInCampaign_1() { return static_cast<int32_t>(offsetof(CampaignManager_t2990572144_StaticFields, ___heroesDiedInCampaign_1)); }
	inline List_1_t3319525431 * get_heroesDiedInCampaign_1() const { return ___heroesDiedInCampaign_1; }
	inline List_1_t3319525431 ** get_address_of_heroesDiedInCampaign_1() { return &___heroesDiedInCampaign_1; }
	inline void set_heroesDiedInCampaign_1(List_1_t3319525431 * value)
	{
		___heroesDiedInCampaign_1 = value;
		Il2CppCodeGenWriteBarrier((&___heroesDiedInCampaign_1), value);
	}

	inline static int32_t get_offset_of_currentMissionIndex_5() { return static_cast<int32_t>(offsetof(CampaignManager_t2990572144_StaticFields, ___currentMissionIndex_5)); }
	inline int32_t get_currentMissionIndex_5() const { return ___currentMissionIndex_5; }
	inline int32_t* get_address_of_currentMissionIndex_5() { return &___currentMissionIndex_5; }
	inline void set_currentMissionIndex_5(int32_t value)
	{
		___currentMissionIndex_5 = value;
	}

	inline static int32_t get_offset_of_heroDeathCount_6() { return static_cast<int32_t>(offsetof(CampaignManager_t2990572144_StaticFields, ___heroDeathCount_6)); }
	inline int32_t get_heroDeathCount_6() const { return ___heroDeathCount_6; }
	inline int32_t* get_address_of_heroDeathCount_6() { return &___heroDeathCount_6; }
	inline void set_heroDeathCount_6(int32_t value)
	{
		___heroDeathCount_6 = value;
	}

	inline static int32_t get_offset_of_campaignState_7() { return static_cast<int32_t>(offsetof(CampaignManager_t2990572144_StaticFields, ___campaignState_7)); }
	inline CampaignState_t3637050575  get_campaignState_7() const { return ___campaignState_7; }
	inline CampaignState_t3637050575 * get_address_of_campaignState_7() { return &___campaignState_7; }
	inline void set_campaignState_7(CampaignState_t3637050575  value)
	{
		___campaignState_7 = value;
	}

	inline static int32_t get_offset_of_lastCampaignFile_8() { return static_cast<int32_t>(offsetof(CampaignManager_t2990572144_StaticFields, ___lastCampaignFile_8)); }
	inline String_t* get_lastCampaignFile_8() const { return ___lastCampaignFile_8; }
	inline String_t** get_address_of_lastCampaignFile_8() { return &___lastCampaignFile_8; }
	inline void set_lastCampaignFile_8(String_t* value)
	{
		___lastCampaignFile_8 = value;
		Il2CppCodeGenWriteBarrier((&___lastCampaignFile_8), value);
	}

	inline static int32_t get_offset_of_campaignOver_9() { return static_cast<int32_t>(offsetof(CampaignManager_t2990572144_StaticFields, ___campaignOver_9)); }
	inline bool get_campaignOver_9() const { return ___campaignOver_9; }
	inline bool* get_address_of_campaignOver_9() { return &___campaignOver_9; }
	inline void set_campaignOver_9(bool value)
	{
		___campaignOver_9 = value;
	}

	inline static int32_t get_offset_of_heroesWon_10() { return static_cast<int32_t>(offsetof(CampaignManager_t2990572144_StaticFields, ___heroesWon_10)); }
	inline bool get_heroesWon_10() const { return ___heroesWon_10; }
	inline bool* get_address_of_heroesWon_10() { return &___heroesWon_10; }
	inline void set_heroesWon_10(bool value)
	{
		___heroesWon_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMPAIGNMANAGER_T2990572144_H
#ifndef HERO_T2261352770_H
#define HERO_T2261352770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Hero
struct  Hero_t2261352770  : public RuntimeObject
{
public:
	// System.String Hero::Name
	String_t* ___Name_0;
	// System.String Hero::Title
	String_t* ___Title_1;
	// System.Collections.Generic.List`1<Hero/KeywordType> Hero::Keyword
	List_1_t401849560 * ___Keyword_2;
	// Hero/GenderType Hero::Gender
	int32_t ___Gender_3;
	// System.String Hero::StartLocation
	String_t* ___StartLocation_4;
	// System.String Hero::FlavorText
	String_t* ___FlavorText_5;
	// System.Int32 Hero::Health
	int32_t ___Health_6;
	// System.String Hero::StartItem
	String_t* ___StartItem_7;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Hero_t2261352770, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Title_1() { return static_cast<int32_t>(offsetof(Hero_t2261352770, ___Title_1)); }
	inline String_t* get_Title_1() const { return ___Title_1; }
	inline String_t** get_address_of_Title_1() { return &___Title_1; }
	inline void set_Title_1(String_t* value)
	{
		___Title_1 = value;
		Il2CppCodeGenWriteBarrier((&___Title_1), value);
	}

	inline static int32_t get_offset_of_Keyword_2() { return static_cast<int32_t>(offsetof(Hero_t2261352770, ___Keyword_2)); }
	inline List_1_t401849560 * get_Keyword_2() const { return ___Keyword_2; }
	inline List_1_t401849560 ** get_address_of_Keyword_2() { return &___Keyword_2; }
	inline void set_Keyword_2(List_1_t401849560 * value)
	{
		___Keyword_2 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_2), value);
	}

	inline static int32_t get_offset_of_Gender_3() { return static_cast<int32_t>(offsetof(Hero_t2261352770, ___Gender_3)); }
	inline int32_t get_Gender_3() const { return ___Gender_3; }
	inline int32_t* get_address_of_Gender_3() { return &___Gender_3; }
	inline void set_Gender_3(int32_t value)
	{
		___Gender_3 = value;
	}

	inline static int32_t get_offset_of_StartLocation_4() { return static_cast<int32_t>(offsetof(Hero_t2261352770, ___StartLocation_4)); }
	inline String_t* get_StartLocation_4() const { return ___StartLocation_4; }
	inline String_t** get_address_of_StartLocation_4() { return &___StartLocation_4; }
	inline void set_StartLocation_4(String_t* value)
	{
		___StartLocation_4 = value;
		Il2CppCodeGenWriteBarrier((&___StartLocation_4), value);
	}

	inline static int32_t get_offset_of_FlavorText_5() { return static_cast<int32_t>(offsetof(Hero_t2261352770, ___FlavorText_5)); }
	inline String_t* get_FlavorText_5() const { return ___FlavorText_5; }
	inline String_t** get_address_of_FlavorText_5() { return &___FlavorText_5; }
	inline void set_FlavorText_5(String_t* value)
	{
		___FlavorText_5 = value;
		Il2CppCodeGenWriteBarrier((&___FlavorText_5), value);
	}

	inline static int32_t get_offset_of_Health_6() { return static_cast<int32_t>(offsetof(Hero_t2261352770, ___Health_6)); }
	inline int32_t get_Health_6() const { return ___Health_6; }
	inline int32_t* get_address_of_Health_6() { return &___Health_6; }
	inline void set_Health_6(int32_t value)
	{
		___Health_6 = value;
	}

	inline static int32_t get_offset_of_StartItem_7() { return static_cast<int32_t>(offsetof(Hero_t2261352770, ___StartItem_7)); }
	inline String_t* get_StartItem_7() const { return ___StartItem_7; }
	inline String_t** get_address_of_StartItem_7() { return &___StartItem_7; }
	inline void set_StartItem_7(String_t* value)
	{
		___StartItem_7 = value;
		Il2CppCodeGenWriteBarrier((&___StartItem_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HERO_T2261352770_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMELOGENTRIES_T679889234_H
#define GAMELOGENTRIES_T679889234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSaveData/GameLogEntries
struct  GameLogEntries_t679889234  : public RuntimeObject
{
public:
	// GameSaveData/GameLogEntries/Type GameSaveData/GameLogEntries::LogType
	int32_t ___LogType_0;
	// System.String GameSaveData/GameLogEntries::LogText
	String_t* ___LogText_1;
	// System.Byte[] GameSaveData/GameLogEntries::LogTexture
	ByteU5BU5D_t4116647657* ___LogTexture_2;
	// UnityEngine.TextureFormat GameSaveData/GameLogEntries::LogTextureFormat
	int32_t ___LogTextureFormat_3;
	// System.Int32 GameSaveData/GameLogEntries::TextureWidth
	int32_t ___TextureWidth_4;
	// System.Int32 GameSaveData/GameLogEntries::TextureHeight
	int32_t ___TextureHeight_5;

public:
	inline static int32_t get_offset_of_LogType_0() { return static_cast<int32_t>(offsetof(GameLogEntries_t679889234, ___LogType_0)); }
	inline int32_t get_LogType_0() const { return ___LogType_0; }
	inline int32_t* get_address_of_LogType_0() { return &___LogType_0; }
	inline void set_LogType_0(int32_t value)
	{
		___LogType_0 = value;
	}

	inline static int32_t get_offset_of_LogText_1() { return static_cast<int32_t>(offsetof(GameLogEntries_t679889234, ___LogText_1)); }
	inline String_t* get_LogText_1() const { return ___LogText_1; }
	inline String_t** get_address_of_LogText_1() { return &___LogText_1; }
	inline void set_LogText_1(String_t* value)
	{
		___LogText_1 = value;
		Il2CppCodeGenWriteBarrier((&___LogText_1), value);
	}

	inline static int32_t get_offset_of_LogTexture_2() { return static_cast<int32_t>(offsetof(GameLogEntries_t679889234, ___LogTexture_2)); }
	inline ByteU5BU5D_t4116647657* get_LogTexture_2() const { return ___LogTexture_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_LogTexture_2() { return &___LogTexture_2; }
	inline void set_LogTexture_2(ByteU5BU5D_t4116647657* value)
	{
		___LogTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___LogTexture_2), value);
	}

	inline static int32_t get_offset_of_LogTextureFormat_3() { return static_cast<int32_t>(offsetof(GameLogEntries_t679889234, ___LogTextureFormat_3)); }
	inline int32_t get_LogTextureFormat_3() const { return ___LogTextureFormat_3; }
	inline int32_t* get_address_of_LogTextureFormat_3() { return &___LogTextureFormat_3; }
	inline void set_LogTextureFormat_3(int32_t value)
	{
		___LogTextureFormat_3 = value;
	}

	inline static int32_t get_offset_of_TextureWidth_4() { return static_cast<int32_t>(offsetof(GameLogEntries_t679889234, ___TextureWidth_4)); }
	inline int32_t get_TextureWidth_4() const { return ___TextureWidth_4; }
	inline int32_t* get_address_of_TextureWidth_4() { return &___TextureWidth_4; }
	inline void set_TextureWidth_4(int32_t value)
	{
		___TextureWidth_4 = value;
	}

	inline static int32_t get_offset_of_TextureHeight_5() { return static_cast<int32_t>(offsetof(GameLogEntries_t679889234, ___TextureHeight_5)); }
	inline int32_t get_TextureHeight_5() const { return ___TextureHeight_5; }
	inline int32_t* get_address_of_TextureHeight_5() { return &___TextureHeight_5; }
	inline void set_TextureHeight_5(int32_t value)
	{
		___TextureHeight_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMELOGENTRIES_T679889234_H
#ifndef U3CRANDOMHEROWITHKEYWORDU3EC__ANONSTOREY3_T371716733_H
#define U3CRANDOMHEROWITHKEYWORDU3EC__ANONSTOREY3_T371716733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Heroes/<RandomHeroWithKeyword>c__AnonStorey3
struct  U3CRandomHeroWithKeywordU3Ec__AnonStorey3_t371716733  : public RuntimeObject
{
public:
	// Hero/KeywordType Heroes/<RandomHeroWithKeyword>c__AnonStorey3::keyword
	int32_t ___keyword_0;

public:
	inline static int32_t get_offset_of_keyword_0() { return static_cast<int32_t>(offsetof(U3CRandomHeroWithKeywordU3Ec__AnonStorey3_t371716733, ___keyword_0)); }
	inline int32_t get_keyword_0() const { return ___keyword_0; }
	inline int32_t* get_address_of_keyword_0() { return &___keyword_0; }
	inline void set_keyword_0(int32_t value)
	{
		___keyword_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRANDOMHEROWITHKEYWORDU3EC__ANONSTOREY3_T371716733_H
#ifndef GAMESAVEDATA_T3577483891_H
#define GAMESAVEDATA_T3577483891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSaveData
struct  GameSaveData_t3577483891  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> GameSaveData::HandDeck
	List_1_t3319525431 * ___HandDeck_0;
	// System.Collections.Generic.List`1<System.String> GameSaveData::DiscardDeck
	List_1_t3319525431 * ___DiscardDeck_1;
	// System.Collections.Generic.List`1<GameSaveData/RemainsInPlayCardDetails> GameSaveData::RemainsDeck
	List_1_t2442161526 * ___RemainsDeck_2;
	// System.Collections.Generic.List`1<System.Int32> GameSaveData::ObjectivesStates
	List_1_t128053199 * ___ObjectivesStates_3;
	// System.Int32 GameSaveData::ZombiesOnBoard
	int32_t ___ZombiesOnBoard_4;
	// System.Boolean GameSaveData::Spawn
	bool ___Spawn_5;
	// System.Boolean GameSaveData::ZombiesWinOnTie
	bool ___ZombiesWinOnTie_6;
	// System.Boolean GameSaveData::ZombieMovesAfterSpawn
	bool ___ZombieMovesAfterSpawn_7;
	// System.Boolean GameSaveData::TheHungryOne
	bool ___TheHungryOne_8;
	// System.Boolean GameSaveData::FightCardUsed
	bool ___FightCardUsed_9;
	// System.Int32 GameSaveData::Round
	int32_t ___Round_10;
	// System.Int32 GameSaveData::HeroesDiedCount
	int32_t ___HeroesDiedCount_11;
	// System.Int32 GameSaveData::NumberOfHeroesInGame
	int32_t ___NumberOfHeroesInGame_12;
	// System.Collections.Generic.List`1<GameSaveData/GameLogEntries> GameSaveData::GameLog
	List_1_t2151963976 * ___GameLog_13;
	// System.Collections.Generic.List`1<System.String> GameSaveData::CardsPlayed
	List_1_t3319525431 * ___CardsPlayed_14;
	// GameData GameSaveData::GameData
	GameData_t415813024 * ___GameData_15;
	// ZombieTurnPhaseName GameSaveData::PhaseName
	int32_t ___PhaseName_16;
	// System.Boolean GameSaveData::ZombieCanBeKilled
	bool ___ZombieCanBeKilled_17;

public:
	inline static int32_t get_offset_of_HandDeck_0() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___HandDeck_0)); }
	inline List_1_t3319525431 * get_HandDeck_0() const { return ___HandDeck_0; }
	inline List_1_t3319525431 ** get_address_of_HandDeck_0() { return &___HandDeck_0; }
	inline void set_HandDeck_0(List_1_t3319525431 * value)
	{
		___HandDeck_0 = value;
		Il2CppCodeGenWriteBarrier((&___HandDeck_0), value);
	}

	inline static int32_t get_offset_of_DiscardDeck_1() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___DiscardDeck_1)); }
	inline List_1_t3319525431 * get_DiscardDeck_1() const { return ___DiscardDeck_1; }
	inline List_1_t3319525431 ** get_address_of_DiscardDeck_1() { return &___DiscardDeck_1; }
	inline void set_DiscardDeck_1(List_1_t3319525431 * value)
	{
		___DiscardDeck_1 = value;
		Il2CppCodeGenWriteBarrier((&___DiscardDeck_1), value);
	}

	inline static int32_t get_offset_of_RemainsDeck_2() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___RemainsDeck_2)); }
	inline List_1_t2442161526 * get_RemainsDeck_2() const { return ___RemainsDeck_2; }
	inline List_1_t2442161526 ** get_address_of_RemainsDeck_2() { return &___RemainsDeck_2; }
	inline void set_RemainsDeck_2(List_1_t2442161526 * value)
	{
		___RemainsDeck_2 = value;
		Il2CppCodeGenWriteBarrier((&___RemainsDeck_2), value);
	}

	inline static int32_t get_offset_of_ObjectivesStates_3() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___ObjectivesStates_3)); }
	inline List_1_t128053199 * get_ObjectivesStates_3() const { return ___ObjectivesStates_3; }
	inline List_1_t128053199 ** get_address_of_ObjectivesStates_3() { return &___ObjectivesStates_3; }
	inline void set_ObjectivesStates_3(List_1_t128053199 * value)
	{
		___ObjectivesStates_3 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectivesStates_3), value);
	}

	inline static int32_t get_offset_of_ZombiesOnBoard_4() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___ZombiesOnBoard_4)); }
	inline int32_t get_ZombiesOnBoard_4() const { return ___ZombiesOnBoard_4; }
	inline int32_t* get_address_of_ZombiesOnBoard_4() { return &___ZombiesOnBoard_4; }
	inline void set_ZombiesOnBoard_4(int32_t value)
	{
		___ZombiesOnBoard_4 = value;
	}

	inline static int32_t get_offset_of_Spawn_5() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___Spawn_5)); }
	inline bool get_Spawn_5() const { return ___Spawn_5; }
	inline bool* get_address_of_Spawn_5() { return &___Spawn_5; }
	inline void set_Spawn_5(bool value)
	{
		___Spawn_5 = value;
	}

	inline static int32_t get_offset_of_ZombiesWinOnTie_6() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___ZombiesWinOnTie_6)); }
	inline bool get_ZombiesWinOnTie_6() const { return ___ZombiesWinOnTie_6; }
	inline bool* get_address_of_ZombiesWinOnTie_6() { return &___ZombiesWinOnTie_6; }
	inline void set_ZombiesWinOnTie_6(bool value)
	{
		___ZombiesWinOnTie_6 = value;
	}

	inline static int32_t get_offset_of_ZombieMovesAfterSpawn_7() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___ZombieMovesAfterSpawn_7)); }
	inline bool get_ZombieMovesAfterSpawn_7() const { return ___ZombieMovesAfterSpawn_7; }
	inline bool* get_address_of_ZombieMovesAfterSpawn_7() { return &___ZombieMovesAfterSpawn_7; }
	inline void set_ZombieMovesAfterSpawn_7(bool value)
	{
		___ZombieMovesAfterSpawn_7 = value;
	}

	inline static int32_t get_offset_of_TheHungryOne_8() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___TheHungryOne_8)); }
	inline bool get_TheHungryOne_8() const { return ___TheHungryOne_8; }
	inline bool* get_address_of_TheHungryOne_8() { return &___TheHungryOne_8; }
	inline void set_TheHungryOne_8(bool value)
	{
		___TheHungryOne_8 = value;
	}

	inline static int32_t get_offset_of_FightCardUsed_9() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___FightCardUsed_9)); }
	inline bool get_FightCardUsed_9() const { return ___FightCardUsed_9; }
	inline bool* get_address_of_FightCardUsed_9() { return &___FightCardUsed_9; }
	inline void set_FightCardUsed_9(bool value)
	{
		___FightCardUsed_9 = value;
	}

	inline static int32_t get_offset_of_Round_10() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___Round_10)); }
	inline int32_t get_Round_10() const { return ___Round_10; }
	inline int32_t* get_address_of_Round_10() { return &___Round_10; }
	inline void set_Round_10(int32_t value)
	{
		___Round_10 = value;
	}

	inline static int32_t get_offset_of_HeroesDiedCount_11() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___HeroesDiedCount_11)); }
	inline int32_t get_HeroesDiedCount_11() const { return ___HeroesDiedCount_11; }
	inline int32_t* get_address_of_HeroesDiedCount_11() { return &___HeroesDiedCount_11; }
	inline void set_HeroesDiedCount_11(int32_t value)
	{
		___HeroesDiedCount_11 = value;
	}

	inline static int32_t get_offset_of_NumberOfHeroesInGame_12() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___NumberOfHeroesInGame_12)); }
	inline int32_t get_NumberOfHeroesInGame_12() const { return ___NumberOfHeroesInGame_12; }
	inline int32_t* get_address_of_NumberOfHeroesInGame_12() { return &___NumberOfHeroesInGame_12; }
	inline void set_NumberOfHeroesInGame_12(int32_t value)
	{
		___NumberOfHeroesInGame_12 = value;
	}

	inline static int32_t get_offset_of_GameLog_13() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___GameLog_13)); }
	inline List_1_t2151963976 * get_GameLog_13() const { return ___GameLog_13; }
	inline List_1_t2151963976 ** get_address_of_GameLog_13() { return &___GameLog_13; }
	inline void set_GameLog_13(List_1_t2151963976 * value)
	{
		___GameLog_13 = value;
		Il2CppCodeGenWriteBarrier((&___GameLog_13), value);
	}

	inline static int32_t get_offset_of_CardsPlayed_14() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___CardsPlayed_14)); }
	inline List_1_t3319525431 * get_CardsPlayed_14() const { return ___CardsPlayed_14; }
	inline List_1_t3319525431 ** get_address_of_CardsPlayed_14() { return &___CardsPlayed_14; }
	inline void set_CardsPlayed_14(List_1_t3319525431 * value)
	{
		___CardsPlayed_14 = value;
		Il2CppCodeGenWriteBarrier((&___CardsPlayed_14), value);
	}

	inline static int32_t get_offset_of_GameData_15() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___GameData_15)); }
	inline GameData_t415813024 * get_GameData_15() const { return ___GameData_15; }
	inline GameData_t415813024 ** get_address_of_GameData_15() { return &___GameData_15; }
	inline void set_GameData_15(GameData_t415813024 * value)
	{
		___GameData_15 = value;
		Il2CppCodeGenWriteBarrier((&___GameData_15), value);
	}

	inline static int32_t get_offset_of_PhaseName_16() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___PhaseName_16)); }
	inline int32_t get_PhaseName_16() const { return ___PhaseName_16; }
	inline int32_t* get_address_of_PhaseName_16() { return &___PhaseName_16; }
	inline void set_PhaseName_16(int32_t value)
	{
		___PhaseName_16 = value;
	}

	inline static int32_t get_offset_of_ZombieCanBeKilled_17() { return static_cast<int32_t>(offsetof(GameSaveData_t3577483891, ___ZombieCanBeKilled_17)); }
	inline bool get_ZombieCanBeKilled_17() const { return ___ZombieCanBeKilled_17; }
	inline bool* get_address_of_ZombieCanBeKilled_17() { return &___ZombieCanBeKilled_17; }
	inline void set_ZombieCanBeKilled_17(bool value)
	{
		___ZombieCanBeKilled_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESAVEDATA_T3577483891_H
#ifndef SISTEROPHELIA_T2122755205_H
#define SISTEROPHELIA_T2122755205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SisterOphelia
struct  SisterOphelia_t2122755205  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SISTEROPHELIA_T2122755205_H
#ifndef AGENTCARTER_T1285216719_H
#define AGENTCARTER_T1285216719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AgentCarter
struct  AgentCarter_t1285216719  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGENTCARTER_T1285216719_H
#ifndef DEPUTYTAYLOR_T3698009909_H
#define DEPUTYTAYLOR_T3698009909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeputyTaylor
struct  DeputyTaylor_t3698009909  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPUTYTAYLOR_T3698009909_H
#ifndef SALLYSURVIVOR_T524194474_H
#define SALLYSURVIVOR_T524194474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SallySurvivor
struct  SallySurvivor_t524194474  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SALLYSURVIVOR_T524194474_H
#ifndef NIKKI_T639850332_H
#define NIKKI_T639850332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Nikki
struct  Nikki_t639850332  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NIKKI_T639850332_H
#ifndef SHERIFFANDERSONSURVIVOR_T2620316193_H
#define SHERIFFANDERSONSURVIVOR_T2620316193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SheriffAndersonSurvivor
struct  SheriffAndersonSurvivor_t2620316193  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHERIFFANDERSONSURVIVOR_T2620316193_H
#ifndef DOCBRODY_T2824236715_H
#define DOCBRODY_T2824236715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DocBrody
struct  DocBrody_t2824236715  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCBRODY_T2824236715_H
#ifndef DRYAMATO_T1382649438_H
#define DRYAMATO_T1382649438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrYamato
struct  DrYamato_t1382649438  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRYAMATO_T1382649438_H
#ifndef MRHYDE_T3080480473_H
#define MRHYDE_T3080480473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MrHyde
struct  MrHyde_t3080480473  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRHYDE_T3080480473_H
#ifndef ANGELA_T1405538042_H
#define ANGELA_T1405538042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Angela
struct  Angela_t1405538042  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANGELA_T1405538042_H
#ifndef BEAR_T582637133_H
#define BEAR_T582637133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bear
struct  Bear_t582637133  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEAR_T582637133_H
#ifndef JAKECARTWRIGHTSURVIVOR_T432754528_H
#define JAKECARTWRIGHTSURVIVOR_T432754528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JakeCartwrightSurvivor
struct  JakeCartwrightSurvivor_t432754528  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAKECARTWRIGHTSURVIVOR_T432754528_H
#ifndef MARIA_T2214739604_H
#define MARIA_T2214739604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Maria
struct  Maria_t2214739604  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARIA_T2214739604_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef JEB_T3107703536_H
#define JEB_T3107703536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jeb
struct  Jeb_t3107703536  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JEB_T3107703536_H
#ifndef EDBAKER_T1130674762_H
#define EDBAKER_T1130674762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EdBaker
struct  EdBaker_t1130674762  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDBAKER_T1130674762_H
#ifndef JENNY_T1784592017_H
#define JENNY_T1784592017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jenny
struct  Jenny_t1784592017  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JENNY_T1784592017_H
#ifndef VICTOR_T190053894_H
#define VICTOR_T190053894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Victor
struct  Victor_t190053894  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VICTOR_T190053894_H
#ifndef BECKY_T970128288_H
#define BECKY_T970128288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Becky
struct  Becky_t970128288  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BECKY_T970128288_H
#ifndef ALICE_T2236182153_H
#define ALICE_T2236182153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Alice
struct  Alice_t2236182153  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALICE_T2236182153_H
#ifndef FATHERJOSEPH_T936646166_H
#define FATHERJOSEPH_T936646166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FatherJoseph
struct  FatherJoseph_t936646166  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FATHERJOSEPH_T936646166_H
#ifndef JAKECARTWRIGHT_T2726891737_H
#define JAKECARTWRIGHT_T2726891737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JakeCartwright
struct  JakeCartwright_t2726891737  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAKECARTWRIGHT_T2726891737_H
#ifndef JOHNNY_T2600401634_H
#define JOHNNY_T2600401634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Johnny
struct  Johnny_t2600401634  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOHNNY_T2600401634_H
#ifndef SALLY_T1012780404_H
#define SALLY_T1012780404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sally
struct  Sally_t1012780404  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SALLY_T1012780404_H
#ifndef SHERIFFANDERSON_T3506115239_H
#define SHERIFFANDERSON_T3506115239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SheriffAnderson
struct  SheriffAnderson_t3506115239  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHERIFFANDERSON_T3506115239_H
#ifndef BILLY_T1030081379_H
#define BILLY_T1030081379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Billy
struct  Billy_t1030081379  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLY_T1030081379_H
#ifndef KENNY_T1784591794_H
#define KENNY_T1784591794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kenny
struct  Kenny_t1784591794  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KENNY_T1784591794_H
#ifndef SAM_T2348450800_H
#define SAM_T2348450800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sam
struct  Sam_t2348450800  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAM_T2348450800_H
#ifndef RACHELLE_T2663978534_H
#define RACHELLE_T2663978534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rachelle
struct  Rachelle_t2663978534  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RACHELLE_T2663978534_H
#ifndef JADE_T2989863974_H
#define JADE_T2989863974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jade
struct  Jade_t2989863974  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JADE_T2989863974_H
#ifndef STACY_T2609714299_H
#define STACY_T2609714299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stacy
struct  Stacy_t2609714299  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACY_T2609714299_H
#ifndef AMANDA_T1400640352_H
#define AMANDA_T1400640352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amanda
struct  Amanda_t1400640352  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMANDA_T1400640352_H
#ifndef MRGODDARD_T854555955_H
#define MRGODDARD_T854555955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MrGoddard
struct  MrGoddard_t854555955  : public Hero_t2261352770
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MRGODDARD_T854555955_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef BUTTONDAMAGEOVERLAY_T2640942607_H
#define BUTTONDAMAGEOVERLAY_T2640942607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonDamageOverlay
struct  ButtonDamageOverlay_t2640942607  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ButtonDamageOverlay::Intensity
	float ___Intensity_2;
	// UnityEngine.Material ButtonDamageOverlay::damageMaterial
	Material_t340375123 * ___damageMaterial_3;

public:
	inline static int32_t get_offset_of_Intensity_2() { return static_cast<int32_t>(offsetof(ButtonDamageOverlay_t2640942607, ___Intensity_2)); }
	inline float get_Intensity_2() const { return ___Intensity_2; }
	inline float* get_address_of_Intensity_2() { return &___Intensity_2; }
	inline void set_Intensity_2(float value)
	{
		___Intensity_2 = value;
	}

	inline static int32_t get_offset_of_damageMaterial_3() { return static_cast<int32_t>(offsetof(ButtonDamageOverlay_t2640942607, ___damageMaterial_3)); }
	inline Material_t340375123 * get_damageMaterial_3() const { return ___damageMaterial_3; }
	inline Material_t340375123 ** get_address_of_damageMaterial_3() { return &___damageMaterial_3; }
	inline void set_damageMaterial_3(Material_t340375123 * value)
	{
		___damageMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___damageMaterial_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONDAMAGEOVERLAY_T2640942607_H
#ifndef AUTOSAVECONTINUE_T69711145_H
#define AUTOSAVECONTINUE_T69711145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoSaveContinue
struct  AutoSaveContinue_t69711145  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOSAVECONTINUE_T69711145_H
#ifndef ARTRACKABLEEVENTHANDLER_T2777929902_H
#define ARTRACKABLEEVENTHANDLER_T2777929902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARTrackableEventHandler
struct  ARTrackableEventHandler_t2777929902  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.TrackableBehaviour ARTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_2;
	// UnityEngine.GameObject ARTrackableEventHandler::CardDetails
	GameObject_t1113636619 * ___CardDetails_3;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_2() { return static_cast<int32_t>(offsetof(ARTrackableEventHandler_t2777929902, ___mTrackableBehaviour_2)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_2() const { return ___mTrackableBehaviour_2; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_2() { return &___mTrackableBehaviour_2; }
	inline void set_mTrackableBehaviour_2(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_2), value);
	}

	inline static int32_t get_offset_of_CardDetails_3() { return static_cast<int32_t>(offsetof(ARTrackableEventHandler_t2777929902, ___CardDetails_3)); }
	inline GameObject_t1113636619 * get_CardDetails_3() const { return ___CardDetails_3; }
	inline GameObject_t1113636619 ** get_address_of_CardDetails_3() { return &___CardDetails_3; }
	inline void set_CardDetails_3(GameObject_t1113636619 * value)
	{
		___CardDetails_3 = value;
		Il2CppCodeGenWriteBarrier((&___CardDetails_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKABLEEVENTHANDLER_T2777929902_H
#ifndef EXPANSIONMANAGER_T4180898005_H
#define EXPANSIONMANAGER_T4180898005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExpansionManager
struct  ExpansionManager_t4180898005  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPANSIONMANAGER_T4180898005_H
#ifndef ADJUSTAUDIO_T1754716221_H
#define ADJUSTAUDIO_T1754716221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdjustAudio
struct  AdjustAudio_t1754716221  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Audio.AudioMixer AdjustAudio::Mixer
	AudioMixer_t3521020193 * ___Mixer_2;
	// UnityEngine.UI.Slider AdjustAudio::SliderMusic
	Slider_t3903728902 * ___SliderMusic_3;
	// UnityEngine.UI.Slider AdjustAudio::SliderSFX
	Slider_t3903728902 * ___SliderSFX_4;
	// UnityEngine.UI.Image AdjustAudio::AudioToggleButtonImage
	Image_t2670269651 * ___AudioToggleButtonImage_5;
	// UnityEngine.Sprite AdjustAudio::TurnAudioOffSprite
	Sprite_t280657092 * ___TurnAudioOffSprite_6;
	// UnityEngine.Sprite AdjustAudio::TurnAudioOnSprite
	Sprite_t280657092 * ___TurnAudioOnSprite_7;
	// UnityEngine.AudioSource AdjustAudio::clickAudio
	AudioSource_t3935305588 * ___clickAudio_8;

public:
	inline static int32_t get_offset_of_Mixer_2() { return static_cast<int32_t>(offsetof(AdjustAudio_t1754716221, ___Mixer_2)); }
	inline AudioMixer_t3521020193 * get_Mixer_2() const { return ___Mixer_2; }
	inline AudioMixer_t3521020193 ** get_address_of_Mixer_2() { return &___Mixer_2; }
	inline void set_Mixer_2(AudioMixer_t3521020193 * value)
	{
		___Mixer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Mixer_2), value);
	}

	inline static int32_t get_offset_of_SliderMusic_3() { return static_cast<int32_t>(offsetof(AdjustAudio_t1754716221, ___SliderMusic_3)); }
	inline Slider_t3903728902 * get_SliderMusic_3() const { return ___SliderMusic_3; }
	inline Slider_t3903728902 ** get_address_of_SliderMusic_3() { return &___SliderMusic_3; }
	inline void set_SliderMusic_3(Slider_t3903728902 * value)
	{
		___SliderMusic_3 = value;
		Il2CppCodeGenWriteBarrier((&___SliderMusic_3), value);
	}

	inline static int32_t get_offset_of_SliderSFX_4() { return static_cast<int32_t>(offsetof(AdjustAudio_t1754716221, ___SliderSFX_4)); }
	inline Slider_t3903728902 * get_SliderSFX_4() const { return ___SliderSFX_4; }
	inline Slider_t3903728902 ** get_address_of_SliderSFX_4() { return &___SliderSFX_4; }
	inline void set_SliderSFX_4(Slider_t3903728902 * value)
	{
		___SliderSFX_4 = value;
		Il2CppCodeGenWriteBarrier((&___SliderSFX_4), value);
	}

	inline static int32_t get_offset_of_AudioToggleButtonImage_5() { return static_cast<int32_t>(offsetof(AdjustAudio_t1754716221, ___AudioToggleButtonImage_5)); }
	inline Image_t2670269651 * get_AudioToggleButtonImage_5() const { return ___AudioToggleButtonImage_5; }
	inline Image_t2670269651 ** get_address_of_AudioToggleButtonImage_5() { return &___AudioToggleButtonImage_5; }
	inline void set_AudioToggleButtonImage_5(Image_t2670269651 * value)
	{
		___AudioToggleButtonImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___AudioToggleButtonImage_5), value);
	}

	inline static int32_t get_offset_of_TurnAudioOffSprite_6() { return static_cast<int32_t>(offsetof(AdjustAudio_t1754716221, ___TurnAudioOffSprite_6)); }
	inline Sprite_t280657092 * get_TurnAudioOffSprite_6() const { return ___TurnAudioOffSprite_6; }
	inline Sprite_t280657092 ** get_address_of_TurnAudioOffSprite_6() { return &___TurnAudioOffSprite_6; }
	inline void set_TurnAudioOffSprite_6(Sprite_t280657092 * value)
	{
		___TurnAudioOffSprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___TurnAudioOffSprite_6), value);
	}

	inline static int32_t get_offset_of_TurnAudioOnSprite_7() { return static_cast<int32_t>(offsetof(AdjustAudio_t1754716221, ___TurnAudioOnSprite_7)); }
	inline Sprite_t280657092 * get_TurnAudioOnSprite_7() const { return ___TurnAudioOnSprite_7; }
	inline Sprite_t280657092 ** get_address_of_TurnAudioOnSprite_7() { return &___TurnAudioOnSprite_7; }
	inline void set_TurnAudioOnSprite_7(Sprite_t280657092 * value)
	{
		___TurnAudioOnSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___TurnAudioOnSprite_7), value);
	}

	inline static int32_t get_offset_of_clickAudio_8() { return static_cast<int32_t>(offsetof(AdjustAudio_t1754716221, ___clickAudio_8)); }
	inline AudioSource_t3935305588 * get_clickAudio_8() const { return ___clickAudio_8; }
	inline AudioSource_t3935305588 ** get_address_of_clickAudio_8() { return &___clickAudio_8; }
	inline void set_clickAudio_8(AudioSource_t3935305588 * value)
	{
		___clickAudio_8 = value;
		Il2CppCodeGenWriteBarrier((&___clickAudio_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADJUSTAUDIO_T1754716221_H
#ifndef GSFU_DEMO_RUNTIME_T606846259_H
#define GSFU_DEMO_RUNTIME_T606846259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GSFU_Demo_Runtime
struct  GSFU_Demo_Runtime_t606846259  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct GSFU_Demo_Runtime_t606846259_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`3<CloudConnectorCore/QueryType,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<System.String>> GSFU_Demo_Runtime::<>f__mg$cache0
	UnityAction_3_t1112121581 * ___U3CU3Ef__mgU24cache0_2;
	// UnityEngine.Events.UnityAction`3<CloudConnectorCore/QueryType,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<System.String>> GSFU_Demo_Runtime::<>f__mg$cache1
	UnityAction_3_t1112121581 * ___U3CU3Ef__mgU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(GSFU_Demo_Runtime_t606846259_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline UnityAction_3_t1112121581 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline UnityAction_3_t1112121581 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(UnityAction_3_t1112121581 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_3() { return static_cast<int32_t>(offsetof(GSFU_Demo_Runtime_t606846259_StaticFields, ___U3CU3Ef__mgU24cache1_3)); }
	inline UnityAction_3_t1112121581 * get_U3CU3Ef__mgU24cache1_3() const { return ___U3CU3Ef__mgU24cache1_3; }
	inline UnityAction_3_t1112121581 ** get_address_of_U3CU3Ef__mgU24cache1_3() { return &___U3CU3Ef__mgU24cache1_3; }
	inline void set_U3CU3Ef__mgU24cache1_3(UnityAction_3_t1112121581 * value)
	{
		___U3CU3Ef__mgU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GSFU_DEMO_RUNTIME_T606846259_H
#ifndef GAMELOG_T2697247111_H
#define GAMELOG_T2697247111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameLog
struct  GameLog_t2697247111  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GameLog::Contents
	GameObject_t1113636619 * ___Contents_2;
	// UnityEngine.GameObject GameLog::ContentsRender
	GameObject_t1113636619 * ___ContentsRender_3;
	// UnityEngine.GameObject GameLog::TextPrefab
	GameObject_t1113636619 * ___TextPrefab_4;
	// UnityEngine.GameObject GameLog::ImagePrefab
	GameObject_t1113636619 * ___ImagePrefab_5;
	// UnityEngine.Camera GameLog::LogCamera
	Camera_t4157153871 * ___LogCamera_6;
	// UnityEngine.RenderTexture GameLog::logRenderTexture
	RenderTexture_t2108887433 * ___logRenderTexture_7;
	// System.Boolean GameLog::grab
	bool ___grab_8;
	// UnityEngine.GameObject GameLog::dupContent
	GameObject_t1113636619 * ___dupContent_9;
	// System.Int32 GameLog::textObjects
	int32_t ___textObjects_10;
	// System.Int32 GameLog::imageObjects
	int32_t ___imageObjects_11;
	// System.Collections.Generic.List`1<GameSaveData/GameLogEntries> GameLog::GameLogSave
	List_1_t2151963976 * ___GameLogSave_12;
	// System.Single GameLog::imageHeight
	float ___imageHeight_15;

public:
	inline static int32_t get_offset_of_Contents_2() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___Contents_2)); }
	inline GameObject_t1113636619 * get_Contents_2() const { return ___Contents_2; }
	inline GameObject_t1113636619 ** get_address_of_Contents_2() { return &___Contents_2; }
	inline void set_Contents_2(GameObject_t1113636619 * value)
	{
		___Contents_2 = value;
		Il2CppCodeGenWriteBarrier((&___Contents_2), value);
	}

	inline static int32_t get_offset_of_ContentsRender_3() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___ContentsRender_3)); }
	inline GameObject_t1113636619 * get_ContentsRender_3() const { return ___ContentsRender_3; }
	inline GameObject_t1113636619 ** get_address_of_ContentsRender_3() { return &___ContentsRender_3; }
	inline void set_ContentsRender_3(GameObject_t1113636619 * value)
	{
		___ContentsRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___ContentsRender_3), value);
	}

	inline static int32_t get_offset_of_TextPrefab_4() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___TextPrefab_4)); }
	inline GameObject_t1113636619 * get_TextPrefab_4() const { return ___TextPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_TextPrefab_4() { return &___TextPrefab_4; }
	inline void set_TextPrefab_4(GameObject_t1113636619 * value)
	{
		___TextPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextPrefab_4), value);
	}

	inline static int32_t get_offset_of_ImagePrefab_5() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___ImagePrefab_5)); }
	inline GameObject_t1113636619 * get_ImagePrefab_5() const { return ___ImagePrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_ImagePrefab_5() { return &___ImagePrefab_5; }
	inline void set_ImagePrefab_5(GameObject_t1113636619 * value)
	{
		___ImagePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___ImagePrefab_5), value);
	}

	inline static int32_t get_offset_of_LogCamera_6() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___LogCamera_6)); }
	inline Camera_t4157153871 * get_LogCamera_6() const { return ___LogCamera_6; }
	inline Camera_t4157153871 ** get_address_of_LogCamera_6() { return &___LogCamera_6; }
	inline void set_LogCamera_6(Camera_t4157153871 * value)
	{
		___LogCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___LogCamera_6), value);
	}

	inline static int32_t get_offset_of_logRenderTexture_7() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___logRenderTexture_7)); }
	inline RenderTexture_t2108887433 * get_logRenderTexture_7() const { return ___logRenderTexture_7; }
	inline RenderTexture_t2108887433 ** get_address_of_logRenderTexture_7() { return &___logRenderTexture_7; }
	inline void set_logRenderTexture_7(RenderTexture_t2108887433 * value)
	{
		___logRenderTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___logRenderTexture_7), value);
	}

	inline static int32_t get_offset_of_grab_8() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___grab_8)); }
	inline bool get_grab_8() const { return ___grab_8; }
	inline bool* get_address_of_grab_8() { return &___grab_8; }
	inline void set_grab_8(bool value)
	{
		___grab_8 = value;
	}

	inline static int32_t get_offset_of_dupContent_9() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___dupContent_9)); }
	inline GameObject_t1113636619 * get_dupContent_9() const { return ___dupContent_9; }
	inline GameObject_t1113636619 ** get_address_of_dupContent_9() { return &___dupContent_9; }
	inline void set_dupContent_9(GameObject_t1113636619 * value)
	{
		___dupContent_9 = value;
		Il2CppCodeGenWriteBarrier((&___dupContent_9), value);
	}

	inline static int32_t get_offset_of_textObjects_10() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___textObjects_10)); }
	inline int32_t get_textObjects_10() const { return ___textObjects_10; }
	inline int32_t* get_address_of_textObjects_10() { return &___textObjects_10; }
	inline void set_textObjects_10(int32_t value)
	{
		___textObjects_10 = value;
	}

	inline static int32_t get_offset_of_imageObjects_11() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___imageObjects_11)); }
	inline int32_t get_imageObjects_11() const { return ___imageObjects_11; }
	inline int32_t* get_address_of_imageObjects_11() { return &___imageObjects_11; }
	inline void set_imageObjects_11(int32_t value)
	{
		___imageObjects_11 = value;
	}

	inline static int32_t get_offset_of_GameLogSave_12() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___GameLogSave_12)); }
	inline List_1_t2151963976 * get_GameLogSave_12() const { return ___GameLogSave_12; }
	inline List_1_t2151963976 ** get_address_of_GameLogSave_12() { return &___GameLogSave_12; }
	inline void set_GameLogSave_12(List_1_t2151963976 * value)
	{
		___GameLogSave_12 = value;
		Il2CppCodeGenWriteBarrier((&___GameLogSave_12), value);
	}

	inline static int32_t get_offset_of_imageHeight_15() { return static_cast<int32_t>(offsetof(GameLog_t2697247111, ___imageHeight_15)); }
	inline float get_imageHeight_15() const { return ___imageHeight_15; }
	inline float* get_address_of_imageHeight_15() { return &___imageHeight_15; }
	inline void set_imageHeight_15(float value)
	{
		___imageHeight_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMELOG_T2697247111_H
#ifndef GAMESETTINGS_T2345380323_H
#define GAMESETTINGS_T2345380323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSettings
struct  GameSettings_t2345380323  : public MonoBehaviour_t3962482529
{
public:
	// LoadScene GameSettings::LoadScene
	LoadScene_t3470671713 * ___LoadScene_2;
	// UnityEngine.GameObject GameSettings::CoreHeroes
	GameObject_t1113636619 * ___CoreHeroes_3;
	// UnityEngine.GameObject GameSettings::GHHeroes
	GameObject_t1113636619 * ___GHHeroes_4;
	// UnityEngine.GameObject GameSettings::GHSpecial
	GameObject_t1113636619 * ___GHSpecial_5;
	// UnityEngine.GameObject GameSettings::HP1Heroes
	GameObject_t1113636619 * ___HP1Heroes_6;
	// UnityEngine.GameObject GameSettings::TPHeroes
	GameObject_t1113636619 * ___TPHeroes_7;
	// UnityEngine.GameObject GameSettings::BFHeroes
	GameObject_t1113636619 * ___BFHeroes_8;
	// UnityEngine.GameObject GameSettings::AEHeroes
	GameObject_t1113636619 * ___AEHeroes_9;
	// UnityEngine.GameObject GameSettings::HP2Heroes
	GameObject_t1113636619 * ___HP2Heroes_10;
	// System.Collections.Generic.List`1<System.String> GameSettings::expansionsWithHeroes
	List_1_t3319525431 * ___expansionsWithHeroes_11;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameSettings::heroObjects
	List_1_t2585711361 * ___heroObjects_12;
	// UnityEngine.RectTransform GameSettings::HeroContent
	RectTransform_t3704657025 * ___HeroContent_13;
	// System.Int32 GameSettings::heroContentHeight
	int32_t ___heroContentHeight_14;
	// System.Int32 GameSettings::numPlayers
	int32_t ___numPlayers_15;
	// System.String GameSettings::GameType
	String_t* ___GameType_16;
	// System.String GameSettings::scenario
	String_t* ___scenario_17;
	// System.Collections.Generic.List`1<System.String> GameSettings::coreHeroOptions
	List_1_t3319525431 * ___coreHeroOptions_18;
	// System.Collections.Generic.List`1<System.String> GameSettings::selectedHeroOptions
	List_1_t3319525431 * ___selectedHeroOptions_19;
	// System.Boolean GameSettings::isWellStockedBuildings
	bool ___isWellStockedBuildings_20;
	// System.Boolean GameSettings::isHeroesReplenish
	bool ___isHeroesReplenish_21;
	// System.Boolean GameSettings::isHeroStartingCards1
	bool ___isHeroStartingCards1_22;
	// System.Boolean GameSettings::isHeroStartingCards2
	bool ___isHeroStartingCards2_23;
	// System.Boolean GameSettings::isFreeSearchMarkers
	bool ___isFreeSearchMarkers_24;
	// System.Boolean GameSettings::isGraveDeadZombiesInGame
	bool ___isGraveDeadZombiesInGame_25;
	// System.Boolean GameSettings::isZombieAutoSpawn
	bool ___isZombieAutoSpawn_26;
	// System.Boolean GameSettings::isZombieHorde21
	bool ___isZombieHorde21_27;
	// UnityEngine.AudioSource GameSettings::clickAudio
	AudioSource_t3935305588 * ___clickAudio_28;
	// UnityEngine.UI.Dropdown GameSettings::ScenarioList
	Dropdown_t2274391225 * ___ScenarioList_29;
	// System.Int32 GameSettings::SelectedHeroes
	int32_t ___SelectedHeroes_30;
	// UnityEngine.GameObject GameSettings::SpecialRules
	GameObject_t1113636619 * ___SpecialRules_31;

public:
	inline static int32_t get_offset_of_LoadScene_2() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___LoadScene_2)); }
	inline LoadScene_t3470671713 * get_LoadScene_2() const { return ___LoadScene_2; }
	inline LoadScene_t3470671713 ** get_address_of_LoadScene_2() { return &___LoadScene_2; }
	inline void set_LoadScene_2(LoadScene_t3470671713 * value)
	{
		___LoadScene_2 = value;
		Il2CppCodeGenWriteBarrier((&___LoadScene_2), value);
	}

	inline static int32_t get_offset_of_CoreHeroes_3() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___CoreHeroes_3)); }
	inline GameObject_t1113636619 * get_CoreHeroes_3() const { return ___CoreHeroes_3; }
	inline GameObject_t1113636619 ** get_address_of_CoreHeroes_3() { return &___CoreHeroes_3; }
	inline void set_CoreHeroes_3(GameObject_t1113636619 * value)
	{
		___CoreHeroes_3 = value;
		Il2CppCodeGenWriteBarrier((&___CoreHeroes_3), value);
	}

	inline static int32_t get_offset_of_GHHeroes_4() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___GHHeroes_4)); }
	inline GameObject_t1113636619 * get_GHHeroes_4() const { return ___GHHeroes_4; }
	inline GameObject_t1113636619 ** get_address_of_GHHeroes_4() { return &___GHHeroes_4; }
	inline void set_GHHeroes_4(GameObject_t1113636619 * value)
	{
		___GHHeroes_4 = value;
		Il2CppCodeGenWriteBarrier((&___GHHeroes_4), value);
	}

	inline static int32_t get_offset_of_GHSpecial_5() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___GHSpecial_5)); }
	inline GameObject_t1113636619 * get_GHSpecial_5() const { return ___GHSpecial_5; }
	inline GameObject_t1113636619 ** get_address_of_GHSpecial_5() { return &___GHSpecial_5; }
	inline void set_GHSpecial_5(GameObject_t1113636619 * value)
	{
		___GHSpecial_5 = value;
		Il2CppCodeGenWriteBarrier((&___GHSpecial_5), value);
	}

	inline static int32_t get_offset_of_HP1Heroes_6() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___HP1Heroes_6)); }
	inline GameObject_t1113636619 * get_HP1Heroes_6() const { return ___HP1Heroes_6; }
	inline GameObject_t1113636619 ** get_address_of_HP1Heroes_6() { return &___HP1Heroes_6; }
	inline void set_HP1Heroes_6(GameObject_t1113636619 * value)
	{
		___HP1Heroes_6 = value;
		Il2CppCodeGenWriteBarrier((&___HP1Heroes_6), value);
	}

	inline static int32_t get_offset_of_TPHeroes_7() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___TPHeroes_7)); }
	inline GameObject_t1113636619 * get_TPHeroes_7() const { return ___TPHeroes_7; }
	inline GameObject_t1113636619 ** get_address_of_TPHeroes_7() { return &___TPHeroes_7; }
	inline void set_TPHeroes_7(GameObject_t1113636619 * value)
	{
		___TPHeroes_7 = value;
		Il2CppCodeGenWriteBarrier((&___TPHeroes_7), value);
	}

	inline static int32_t get_offset_of_BFHeroes_8() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___BFHeroes_8)); }
	inline GameObject_t1113636619 * get_BFHeroes_8() const { return ___BFHeroes_8; }
	inline GameObject_t1113636619 ** get_address_of_BFHeroes_8() { return &___BFHeroes_8; }
	inline void set_BFHeroes_8(GameObject_t1113636619 * value)
	{
		___BFHeroes_8 = value;
		Il2CppCodeGenWriteBarrier((&___BFHeroes_8), value);
	}

	inline static int32_t get_offset_of_AEHeroes_9() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___AEHeroes_9)); }
	inline GameObject_t1113636619 * get_AEHeroes_9() const { return ___AEHeroes_9; }
	inline GameObject_t1113636619 ** get_address_of_AEHeroes_9() { return &___AEHeroes_9; }
	inline void set_AEHeroes_9(GameObject_t1113636619 * value)
	{
		___AEHeroes_9 = value;
		Il2CppCodeGenWriteBarrier((&___AEHeroes_9), value);
	}

	inline static int32_t get_offset_of_HP2Heroes_10() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___HP2Heroes_10)); }
	inline GameObject_t1113636619 * get_HP2Heroes_10() const { return ___HP2Heroes_10; }
	inline GameObject_t1113636619 ** get_address_of_HP2Heroes_10() { return &___HP2Heroes_10; }
	inline void set_HP2Heroes_10(GameObject_t1113636619 * value)
	{
		___HP2Heroes_10 = value;
		Il2CppCodeGenWriteBarrier((&___HP2Heroes_10), value);
	}

	inline static int32_t get_offset_of_expansionsWithHeroes_11() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___expansionsWithHeroes_11)); }
	inline List_1_t3319525431 * get_expansionsWithHeroes_11() const { return ___expansionsWithHeroes_11; }
	inline List_1_t3319525431 ** get_address_of_expansionsWithHeroes_11() { return &___expansionsWithHeroes_11; }
	inline void set_expansionsWithHeroes_11(List_1_t3319525431 * value)
	{
		___expansionsWithHeroes_11 = value;
		Il2CppCodeGenWriteBarrier((&___expansionsWithHeroes_11), value);
	}

	inline static int32_t get_offset_of_heroObjects_12() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___heroObjects_12)); }
	inline List_1_t2585711361 * get_heroObjects_12() const { return ___heroObjects_12; }
	inline List_1_t2585711361 ** get_address_of_heroObjects_12() { return &___heroObjects_12; }
	inline void set_heroObjects_12(List_1_t2585711361 * value)
	{
		___heroObjects_12 = value;
		Il2CppCodeGenWriteBarrier((&___heroObjects_12), value);
	}

	inline static int32_t get_offset_of_HeroContent_13() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___HeroContent_13)); }
	inline RectTransform_t3704657025 * get_HeroContent_13() const { return ___HeroContent_13; }
	inline RectTransform_t3704657025 ** get_address_of_HeroContent_13() { return &___HeroContent_13; }
	inline void set_HeroContent_13(RectTransform_t3704657025 * value)
	{
		___HeroContent_13 = value;
		Il2CppCodeGenWriteBarrier((&___HeroContent_13), value);
	}

	inline static int32_t get_offset_of_heroContentHeight_14() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___heroContentHeight_14)); }
	inline int32_t get_heroContentHeight_14() const { return ___heroContentHeight_14; }
	inline int32_t* get_address_of_heroContentHeight_14() { return &___heroContentHeight_14; }
	inline void set_heroContentHeight_14(int32_t value)
	{
		___heroContentHeight_14 = value;
	}

	inline static int32_t get_offset_of_numPlayers_15() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___numPlayers_15)); }
	inline int32_t get_numPlayers_15() const { return ___numPlayers_15; }
	inline int32_t* get_address_of_numPlayers_15() { return &___numPlayers_15; }
	inline void set_numPlayers_15(int32_t value)
	{
		___numPlayers_15 = value;
	}

	inline static int32_t get_offset_of_GameType_16() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___GameType_16)); }
	inline String_t* get_GameType_16() const { return ___GameType_16; }
	inline String_t** get_address_of_GameType_16() { return &___GameType_16; }
	inline void set_GameType_16(String_t* value)
	{
		___GameType_16 = value;
		Il2CppCodeGenWriteBarrier((&___GameType_16), value);
	}

	inline static int32_t get_offset_of_scenario_17() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___scenario_17)); }
	inline String_t* get_scenario_17() const { return ___scenario_17; }
	inline String_t** get_address_of_scenario_17() { return &___scenario_17; }
	inline void set_scenario_17(String_t* value)
	{
		___scenario_17 = value;
		Il2CppCodeGenWriteBarrier((&___scenario_17), value);
	}

	inline static int32_t get_offset_of_coreHeroOptions_18() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___coreHeroOptions_18)); }
	inline List_1_t3319525431 * get_coreHeroOptions_18() const { return ___coreHeroOptions_18; }
	inline List_1_t3319525431 ** get_address_of_coreHeroOptions_18() { return &___coreHeroOptions_18; }
	inline void set_coreHeroOptions_18(List_1_t3319525431 * value)
	{
		___coreHeroOptions_18 = value;
		Il2CppCodeGenWriteBarrier((&___coreHeroOptions_18), value);
	}

	inline static int32_t get_offset_of_selectedHeroOptions_19() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___selectedHeroOptions_19)); }
	inline List_1_t3319525431 * get_selectedHeroOptions_19() const { return ___selectedHeroOptions_19; }
	inline List_1_t3319525431 ** get_address_of_selectedHeroOptions_19() { return &___selectedHeroOptions_19; }
	inline void set_selectedHeroOptions_19(List_1_t3319525431 * value)
	{
		___selectedHeroOptions_19 = value;
		Il2CppCodeGenWriteBarrier((&___selectedHeroOptions_19), value);
	}

	inline static int32_t get_offset_of_isWellStockedBuildings_20() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___isWellStockedBuildings_20)); }
	inline bool get_isWellStockedBuildings_20() const { return ___isWellStockedBuildings_20; }
	inline bool* get_address_of_isWellStockedBuildings_20() { return &___isWellStockedBuildings_20; }
	inline void set_isWellStockedBuildings_20(bool value)
	{
		___isWellStockedBuildings_20 = value;
	}

	inline static int32_t get_offset_of_isHeroesReplenish_21() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___isHeroesReplenish_21)); }
	inline bool get_isHeroesReplenish_21() const { return ___isHeroesReplenish_21; }
	inline bool* get_address_of_isHeroesReplenish_21() { return &___isHeroesReplenish_21; }
	inline void set_isHeroesReplenish_21(bool value)
	{
		___isHeroesReplenish_21 = value;
	}

	inline static int32_t get_offset_of_isHeroStartingCards1_22() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___isHeroStartingCards1_22)); }
	inline bool get_isHeroStartingCards1_22() const { return ___isHeroStartingCards1_22; }
	inline bool* get_address_of_isHeroStartingCards1_22() { return &___isHeroStartingCards1_22; }
	inline void set_isHeroStartingCards1_22(bool value)
	{
		___isHeroStartingCards1_22 = value;
	}

	inline static int32_t get_offset_of_isHeroStartingCards2_23() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___isHeroStartingCards2_23)); }
	inline bool get_isHeroStartingCards2_23() const { return ___isHeroStartingCards2_23; }
	inline bool* get_address_of_isHeroStartingCards2_23() { return &___isHeroStartingCards2_23; }
	inline void set_isHeroStartingCards2_23(bool value)
	{
		___isHeroStartingCards2_23 = value;
	}

	inline static int32_t get_offset_of_isFreeSearchMarkers_24() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___isFreeSearchMarkers_24)); }
	inline bool get_isFreeSearchMarkers_24() const { return ___isFreeSearchMarkers_24; }
	inline bool* get_address_of_isFreeSearchMarkers_24() { return &___isFreeSearchMarkers_24; }
	inline void set_isFreeSearchMarkers_24(bool value)
	{
		___isFreeSearchMarkers_24 = value;
	}

	inline static int32_t get_offset_of_isGraveDeadZombiesInGame_25() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___isGraveDeadZombiesInGame_25)); }
	inline bool get_isGraveDeadZombiesInGame_25() const { return ___isGraveDeadZombiesInGame_25; }
	inline bool* get_address_of_isGraveDeadZombiesInGame_25() { return &___isGraveDeadZombiesInGame_25; }
	inline void set_isGraveDeadZombiesInGame_25(bool value)
	{
		___isGraveDeadZombiesInGame_25 = value;
	}

	inline static int32_t get_offset_of_isZombieAutoSpawn_26() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___isZombieAutoSpawn_26)); }
	inline bool get_isZombieAutoSpawn_26() const { return ___isZombieAutoSpawn_26; }
	inline bool* get_address_of_isZombieAutoSpawn_26() { return &___isZombieAutoSpawn_26; }
	inline void set_isZombieAutoSpawn_26(bool value)
	{
		___isZombieAutoSpawn_26 = value;
	}

	inline static int32_t get_offset_of_isZombieHorde21_27() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___isZombieHorde21_27)); }
	inline bool get_isZombieHorde21_27() const { return ___isZombieHorde21_27; }
	inline bool* get_address_of_isZombieHorde21_27() { return &___isZombieHorde21_27; }
	inline void set_isZombieHorde21_27(bool value)
	{
		___isZombieHorde21_27 = value;
	}

	inline static int32_t get_offset_of_clickAudio_28() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___clickAudio_28)); }
	inline AudioSource_t3935305588 * get_clickAudio_28() const { return ___clickAudio_28; }
	inline AudioSource_t3935305588 ** get_address_of_clickAudio_28() { return &___clickAudio_28; }
	inline void set_clickAudio_28(AudioSource_t3935305588 * value)
	{
		___clickAudio_28 = value;
		Il2CppCodeGenWriteBarrier((&___clickAudio_28), value);
	}

	inline static int32_t get_offset_of_ScenarioList_29() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___ScenarioList_29)); }
	inline Dropdown_t2274391225 * get_ScenarioList_29() const { return ___ScenarioList_29; }
	inline Dropdown_t2274391225 ** get_address_of_ScenarioList_29() { return &___ScenarioList_29; }
	inline void set_ScenarioList_29(Dropdown_t2274391225 * value)
	{
		___ScenarioList_29 = value;
		Il2CppCodeGenWriteBarrier((&___ScenarioList_29), value);
	}

	inline static int32_t get_offset_of_SelectedHeroes_30() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___SelectedHeroes_30)); }
	inline int32_t get_SelectedHeroes_30() const { return ___SelectedHeroes_30; }
	inline int32_t* get_address_of_SelectedHeroes_30() { return &___SelectedHeroes_30; }
	inline void set_SelectedHeroes_30(int32_t value)
	{
		___SelectedHeroes_30 = value;
	}

	inline static int32_t get_offset_of_SpecialRules_31() { return static_cast<int32_t>(offsetof(GameSettings_t2345380323, ___SpecialRules_31)); }
	inline GameObject_t1113636619 * get_SpecialRules_31() const { return ___SpecialRules_31; }
	inline GameObject_t1113636619 ** get_address_of_SpecialRules_31() { return &___SpecialRules_31; }
	inline void set_SpecialRules_31(GameObject_t1113636619 * value)
	{
		___SpecialRules_31 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialRules_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESETTINGS_T2345380323_H
#ifndef XORTEST_T823650901_H
#define XORTEST_T823650901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// xorTest
struct  xorTest_t823650901  : public MonoBehaviour_t3962482529
{
public:
	// System.Double[][] xorTest::xorData
	DoubleU5BU5DU5BU5D_t1159594487* ___xorData_5;
	// System.Double[][] xorTest::trainData
	DoubleU5BU5DU5BU5D_t1159594487* ___trainData_9;
	// System.Double[][] xorTest::testData
	DoubleU5BU5DU5BU5D_t1159594487* ___testData_10;

public:
	inline static int32_t get_offset_of_xorData_5() { return static_cast<int32_t>(offsetof(xorTest_t823650901, ___xorData_5)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_xorData_5() const { return ___xorData_5; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_xorData_5() { return &___xorData_5; }
	inline void set_xorData_5(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___xorData_5 = value;
		Il2CppCodeGenWriteBarrier((&___xorData_5), value);
	}

	inline static int32_t get_offset_of_trainData_9() { return static_cast<int32_t>(offsetof(xorTest_t823650901, ___trainData_9)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_trainData_9() const { return ___trainData_9; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_trainData_9() { return &___trainData_9; }
	inline void set_trainData_9(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___trainData_9 = value;
		Il2CppCodeGenWriteBarrier((&___trainData_9), value);
	}

	inline static int32_t get_offset_of_testData_10() { return static_cast<int32_t>(offsetof(xorTest_t823650901, ___testData_10)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_testData_10() const { return ___testData_10; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_testData_10() { return &___testData_10; }
	inline void set_testData_10(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___testData_10 = value;
		Il2CppCodeGenWriteBarrier((&___testData_10), value);
	}
};

struct xorTest_t823650901_StaticFields
{
public:
	// System.Func`3<System.String,System.Double,System.String> xorTest::<>f__am$cache0
	Func_3_t220556092 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(xorTest_t823650901_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Func_3_t220556092 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Func_3_t220556092 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Func_3_t220556092 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XORTEST_T823650901_H
#ifndef HERODEADSCREEN_T3673982372_H
#define HERODEADSCREEN_T3673982372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroDeadScreen
struct  HeroDeadScreen_t3673982372  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject HeroDeadScreen::Option1
	GameObject_t1113636619 * ___Option1_2;
	// UnityEngine.GameObject HeroDeadScreen::Option2
	GameObject_t1113636619 * ___Option2_3;
	// UnityEngine.GameObject HeroDeadScreen::Option3
	GameObject_t1113636619 * ___Option3_4;
	// UnityEngine.GameObject HeroDeadScreen::Option4
	GameObject_t1113636619 * ___Option4_5;
	// UnityEngine.UI.Text HeroDeadScreen::TextInstructions
	Text_t1901882714 * ___TextInstructions_6;
	// UnityEngine.GameObject HeroDeadScreen::WhatHeroPanel
	GameObject_t1113636619 * ___WhatHeroPanel_7;
	// UnityEngine.GameObject HeroDeadScreen::LastHeroPanel
	GameObject_t1113636619 * ___LastHeroPanel_8;
	// UnityEngine.GameObject HeroDeadScreen::ContinuePanel
	GameObject_t1113636619 * ___ContinuePanel_9;
	// ZombieTurn HeroDeadScreen::zombieTurn
	ZombieTurn_t3562480803 * ___zombieTurn_10;
	// System.Collections.Generic.List`1<Hero> HeroDeadScreen::heroesInGame
	List_1_t3733427512 * ___heroesInGame_11;
	// System.String HeroDeadScreen::deadHero
	String_t* ___deadHero_12;
	// UnityEngine.AudioSource HeroDeadScreen::clickAudio
	AudioSource_t3935305588 * ___clickAudio_13;

public:
	inline static int32_t get_offset_of_Option1_2() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___Option1_2)); }
	inline GameObject_t1113636619 * get_Option1_2() const { return ___Option1_2; }
	inline GameObject_t1113636619 ** get_address_of_Option1_2() { return &___Option1_2; }
	inline void set_Option1_2(GameObject_t1113636619 * value)
	{
		___Option1_2 = value;
		Il2CppCodeGenWriteBarrier((&___Option1_2), value);
	}

	inline static int32_t get_offset_of_Option2_3() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___Option2_3)); }
	inline GameObject_t1113636619 * get_Option2_3() const { return ___Option2_3; }
	inline GameObject_t1113636619 ** get_address_of_Option2_3() { return &___Option2_3; }
	inline void set_Option2_3(GameObject_t1113636619 * value)
	{
		___Option2_3 = value;
		Il2CppCodeGenWriteBarrier((&___Option2_3), value);
	}

	inline static int32_t get_offset_of_Option3_4() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___Option3_4)); }
	inline GameObject_t1113636619 * get_Option3_4() const { return ___Option3_4; }
	inline GameObject_t1113636619 ** get_address_of_Option3_4() { return &___Option3_4; }
	inline void set_Option3_4(GameObject_t1113636619 * value)
	{
		___Option3_4 = value;
		Il2CppCodeGenWriteBarrier((&___Option3_4), value);
	}

	inline static int32_t get_offset_of_Option4_5() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___Option4_5)); }
	inline GameObject_t1113636619 * get_Option4_5() const { return ___Option4_5; }
	inline GameObject_t1113636619 ** get_address_of_Option4_5() { return &___Option4_5; }
	inline void set_Option4_5(GameObject_t1113636619 * value)
	{
		___Option4_5 = value;
		Il2CppCodeGenWriteBarrier((&___Option4_5), value);
	}

	inline static int32_t get_offset_of_TextInstructions_6() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___TextInstructions_6)); }
	inline Text_t1901882714 * get_TextInstructions_6() const { return ___TextInstructions_6; }
	inline Text_t1901882714 ** get_address_of_TextInstructions_6() { return &___TextInstructions_6; }
	inline void set_TextInstructions_6(Text_t1901882714 * value)
	{
		___TextInstructions_6 = value;
		Il2CppCodeGenWriteBarrier((&___TextInstructions_6), value);
	}

	inline static int32_t get_offset_of_WhatHeroPanel_7() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___WhatHeroPanel_7)); }
	inline GameObject_t1113636619 * get_WhatHeroPanel_7() const { return ___WhatHeroPanel_7; }
	inline GameObject_t1113636619 ** get_address_of_WhatHeroPanel_7() { return &___WhatHeroPanel_7; }
	inline void set_WhatHeroPanel_7(GameObject_t1113636619 * value)
	{
		___WhatHeroPanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___WhatHeroPanel_7), value);
	}

	inline static int32_t get_offset_of_LastHeroPanel_8() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___LastHeroPanel_8)); }
	inline GameObject_t1113636619 * get_LastHeroPanel_8() const { return ___LastHeroPanel_8; }
	inline GameObject_t1113636619 ** get_address_of_LastHeroPanel_8() { return &___LastHeroPanel_8; }
	inline void set_LastHeroPanel_8(GameObject_t1113636619 * value)
	{
		___LastHeroPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___LastHeroPanel_8), value);
	}

	inline static int32_t get_offset_of_ContinuePanel_9() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___ContinuePanel_9)); }
	inline GameObject_t1113636619 * get_ContinuePanel_9() const { return ___ContinuePanel_9; }
	inline GameObject_t1113636619 ** get_address_of_ContinuePanel_9() { return &___ContinuePanel_9; }
	inline void set_ContinuePanel_9(GameObject_t1113636619 * value)
	{
		___ContinuePanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___ContinuePanel_9), value);
	}

	inline static int32_t get_offset_of_zombieTurn_10() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___zombieTurn_10)); }
	inline ZombieTurn_t3562480803 * get_zombieTurn_10() const { return ___zombieTurn_10; }
	inline ZombieTurn_t3562480803 ** get_address_of_zombieTurn_10() { return &___zombieTurn_10; }
	inline void set_zombieTurn_10(ZombieTurn_t3562480803 * value)
	{
		___zombieTurn_10 = value;
		Il2CppCodeGenWriteBarrier((&___zombieTurn_10), value);
	}

	inline static int32_t get_offset_of_heroesInGame_11() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___heroesInGame_11)); }
	inline List_1_t3733427512 * get_heroesInGame_11() const { return ___heroesInGame_11; }
	inline List_1_t3733427512 ** get_address_of_heroesInGame_11() { return &___heroesInGame_11; }
	inline void set_heroesInGame_11(List_1_t3733427512 * value)
	{
		___heroesInGame_11 = value;
		Il2CppCodeGenWriteBarrier((&___heroesInGame_11), value);
	}

	inline static int32_t get_offset_of_deadHero_12() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___deadHero_12)); }
	inline String_t* get_deadHero_12() const { return ___deadHero_12; }
	inline String_t** get_address_of_deadHero_12() { return &___deadHero_12; }
	inline void set_deadHero_12(String_t* value)
	{
		___deadHero_12 = value;
		Il2CppCodeGenWriteBarrier((&___deadHero_12), value);
	}

	inline static int32_t get_offset_of_clickAudio_13() { return static_cast<int32_t>(offsetof(HeroDeadScreen_t3673982372, ___clickAudio_13)); }
	inline AudioSource_t3935305588 * get_clickAudio_13() const { return ___clickAudio_13; }
	inline AudioSource_t3935305588 ** get_address_of_clickAudio_13() { return &___clickAudio_13; }
	inline void set_clickAudio_13(AudioSource_t3935305588 * value)
	{
		___clickAudio_13 = value;
		Il2CppCodeGenWriteBarrier((&___clickAudio_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HERODEADSCREEN_T3673982372_H
#ifndef ARCONTROL_T2334390846_H
#define ARCONTROL_T2334390846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARControl
struct  ARControl_t2334390846  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ARControl::GameCamera
	GameObject_t1113636619 * ___GameCamera_2;
	// UnityEngine.GameObject ARControl::ARCamera
	GameObject_t1113636619 * ___ARCamera_3;
	// UnityEngine.GameObject ARControl::LogCamera
	GameObject_t1113636619 * ___LogCamera_4;
	// UnityEngine.GameObject[] ARControl::ARHideObjects
	GameObjectU5BU5D_t3328599146* ___ARHideObjects_5;
	// System.Boolean ARControl::isARon
	bool ___isARon_6;
	// System.Boolean ARControl::AutoStart
	bool ___AutoStart_7;

public:
	inline static int32_t get_offset_of_GameCamera_2() { return static_cast<int32_t>(offsetof(ARControl_t2334390846, ___GameCamera_2)); }
	inline GameObject_t1113636619 * get_GameCamera_2() const { return ___GameCamera_2; }
	inline GameObject_t1113636619 ** get_address_of_GameCamera_2() { return &___GameCamera_2; }
	inline void set_GameCamera_2(GameObject_t1113636619 * value)
	{
		___GameCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___GameCamera_2), value);
	}

	inline static int32_t get_offset_of_ARCamera_3() { return static_cast<int32_t>(offsetof(ARControl_t2334390846, ___ARCamera_3)); }
	inline GameObject_t1113636619 * get_ARCamera_3() const { return ___ARCamera_3; }
	inline GameObject_t1113636619 ** get_address_of_ARCamera_3() { return &___ARCamera_3; }
	inline void set_ARCamera_3(GameObject_t1113636619 * value)
	{
		___ARCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___ARCamera_3), value);
	}

	inline static int32_t get_offset_of_LogCamera_4() { return static_cast<int32_t>(offsetof(ARControl_t2334390846, ___LogCamera_4)); }
	inline GameObject_t1113636619 * get_LogCamera_4() const { return ___LogCamera_4; }
	inline GameObject_t1113636619 ** get_address_of_LogCamera_4() { return &___LogCamera_4; }
	inline void set_LogCamera_4(GameObject_t1113636619 * value)
	{
		___LogCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___LogCamera_4), value);
	}

	inline static int32_t get_offset_of_ARHideObjects_5() { return static_cast<int32_t>(offsetof(ARControl_t2334390846, ___ARHideObjects_5)); }
	inline GameObjectU5BU5D_t3328599146* get_ARHideObjects_5() const { return ___ARHideObjects_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_ARHideObjects_5() { return &___ARHideObjects_5; }
	inline void set_ARHideObjects_5(GameObjectU5BU5D_t3328599146* value)
	{
		___ARHideObjects_5 = value;
		Il2CppCodeGenWriteBarrier((&___ARHideObjects_5), value);
	}

	inline static int32_t get_offset_of_isARon_6() { return static_cast<int32_t>(offsetof(ARControl_t2334390846, ___isARon_6)); }
	inline bool get_isARon_6() const { return ___isARon_6; }
	inline bool* get_address_of_isARon_6() { return &___isARon_6; }
	inline void set_isARon_6(bool value)
	{
		___isARon_6 = value;
	}

	inline static int32_t get_offset_of_AutoStart_7() { return static_cast<int32_t>(offsetof(ARControl_t2334390846, ___AutoStart_7)); }
	inline bool get_AutoStart_7() const { return ___AutoStart_7; }
	inline bool* get_address_of_AutoStart_7() { return &___AutoStart_7; }
	inline void set_AutoStart_7(bool value)
	{
		___AutoStart_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROL_T2334390846_H
#ifndef TRAINING_T3060993675_H
#define TRAINING_T3060993675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Training
struct  Training_t3060993675  : public MonoBehaviour_t3962482529
{
public:
	// System.Double[][] Training::allDataIris
	DoubleU5BU5DU5BU5D_t1159594487* ___allDataIris_5;
	// System.Double[][] Training::trainData
	DoubleU5BU5DU5BU5D_t1159594487* ___trainData_9;
	// System.Double[][] Training::testData
	DoubleU5BU5DU5BU5D_t1159594487* ___testData_10;

public:
	inline static int32_t get_offset_of_allDataIris_5() { return static_cast<int32_t>(offsetof(Training_t3060993675, ___allDataIris_5)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_allDataIris_5() const { return ___allDataIris_5; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_allDataIris_5() { return &___allDataIris_5; }
	inline void set_allDataIris_5(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___allDataIris_5 = value;
		Il2CppCodeGenWriteBarrier((&___allDataIris_5), value);
	}

	inline static int32_t get_offset_of_trainData_9() { return static_cast<int32_t>(offsetof(Training_t3060993675, ___trainData_9)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_trainData_9() const { return ___trainData_9; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_trainData_9() { return &___trainData_9; }
	inline void set_trainData_9(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___trainData_9 = value;
		Il2CppCodeGenWriteBarrier((&___trainData_9), value);
	}

	inline static int32_t get_offset_of_testData_10() { return static_cast<int32_t>(offsetof(Training_t3060993675, ___testData_10)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_testData_10() const { return ___testData_10; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_testData_10() { return &___testData_10; }
	inline void set_testData_10(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___testData_10 = value;
		Il2CppCodeGenWriteBarrier((&___testData_10), value);
	}
};

struct Training_t3060993675_StaticFields
{
public:
	// System.Func`3<System.String,System.Double,System.String> Training::<>f__am$cache0
	Func_3_t220556092 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(Training_t3060993675_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Func_3_t220556092 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Func_3_t220556092 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Func_3_t220556092 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAINING_T3060993675_H
#ifndef CAMPAIGNSTATUS_T3636067551_H
#define CAMPAIGNSTATUS_T3636067551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CampaignStatus
struct  CampaignStatus_t3636067551  : public MonoBehaviour_t3962482529
{
public:
	// CampaignManager CampaignStatus::CampaignManager
	CampaignManager_t2990572144 * ___CampaignManager_2;
	// LoadScene CampaignStatus::loadScene
	LoadScene_t3470671713 * ___loadScene_3;

public:
	inline static int32_t get_offset_of_CampaignManager_2() { return static_cast<int32_t>(offsetof(CampaignStatus_t3636067551, ___CampaignManager_2)); }
	inline CampaignManager_t2990572144 * get_CampaignManager_2() const { return ___CampaignManager_2; }
	inline CampaignManager_t2990572144 ** get_address_of_CampaignManager_2() { return &___CampaignManager_2; }
	inline void set_CampaignManager_2(CampaignManager_t2990572144 * value)
	{
		___CampaignManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___CampaignManager_2), value);
	}

	inline static int32_t get_offset_of_loadScene_3() { return static_cast<int32_t>(offsetof(CampaignStatus_t3636067551, ___loadScene_3)); }
	inline LoadScene_t3470671713 * get_loadScene_3() const { return ___loadScene_3; }
	inline LoadScene_t3470671713 ** get_address_of_loadScene_3() { return &___loadScene_3; }
	inline void set_loadScene_3(LoadScene_t3470671713 * value)
	{
		___loadScene_3 = value;
		Il2CppCodeGenWriteBarrier((&___loadScene_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMPAIGNSTATUS_T3636067551_H
#ifndef RBFTEST_T1216387495_H
#define RBFTEST_T1216387495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// rbfTest
struct  rbfTest_t1216387495  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 rbfTest::numInputs
	int32_t ___numInputs_2;
	// System.Int32 rbfTest::numRBFs
	int32_t ___numRBFs_3;
	// System.Int32 rbfTest::numOutputs
	int32_t ___numOutputs_4;
	// System.Double[][] rbfTest::allDataIris
	DoubleU5BU5DU5BU5D_t1159594487* ___allDataIris_5;
	// System.Double[][] rbfTest::normalizedIrisdata
	DoubleU5BU5DU5BU5D_t1159594487* ___normalizedIrisdata_6;
	// System.Double[][] rbfTest::trainData
	DoubleU5BU5DU5BU5D_t1159594487* ___trainData_7;
	// System.Double[][] rbfTest::testData
	DoubleU5BU5DU5BU5D_t1159594487* ___testData_8;
	// RadialBasisFunctionNetwork rbfTest::rbfn
	RadialBasisFunctionNetwork_t3778331090 * ___rbfn_9;
	// RadialBasisFunctionNetwork rbfTest::rbfnTwo
	RadialBasisFunctionNetwork_t3778331090 * ___rbfnTwo_10;

public:
	inline static int32_t get_offset_of_numInputs_2() { return static_cast<int32_t>(offsetof(rbfTest_t1216387495, ___numInputs_2)); }
	inline int32_t get_numInputs_2() const { return ___numInputs_2; }
	inline int32_t* get_address_of_numInputs_2() { return &___numInputs_2; }
	inline void set_numInputs_2(int32_t value)
	{
		___numInputs_2 = value;
	}

	inline static int32_t get_offset_of_numRBFs_3() { return static_cast<int32_t>(offsetof(rbfTest_t1216387495, ___numRBFs_3)); }
	inline int32_t get_numRBFs_3() const { return ___numRBFs_3; }
	inline int32_t* get_address_of_numRBFs_3() { return &___numRBFs_3; }
	inline void set_numRBFs_3(int32_t value)
	{
		___numRBFs_3 = value;
	}

	inline static int32_t get_offset_of_numOutputs_4() { return static_cast<int32_t>(offsetof(rbfTest_t1216387495, ___numOutputs_4)); }
	inline int32_t get_numOutputs_4() const { return ___numOutputs_4; }
	inline int32_t* get_address_of_numOutputs_4() { return &___numOutputs_4; }
	inline void set_numOutputs_4(int32_t value)
	{
		___numOutputs_4 = value;
	}

	inline static int32_t get_offset_of_allDataIris_5() { return static_cast<int32_t>(offsetof(rbfTest_t1216387495, ___allDataIris_5)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_allDataIris_5() const { return ___allDataIris_5; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_allDataIris_5() { return &___allDataIris_5; }
	inline void set_allDataIris_5(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___allDataIris_5 = value;
		Il2CppCodeGenWriteBarrier((&___allDataIris_5), value);
	}

	inline static int32_t get_offset_of_normalizedIrisdata_6() { return static_cast<int32_t>(offsetof(rbfTest_t1216387495, ___normalizedIrisdata_6)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_normalizedIrisdata_6() const { return ___normalizedIrisdata_6; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_normalizedIrisdata_6() { return &___normalizedIrisdata_6; }
	inline void set_normalizedIrisdata_6(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___normalizedIrisdata_6 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedIrisdata_6), value);
	}

	inline static int32_t get_offset_of_trainData_7() { return static_cast<int32_t>(offsetof(rbfTest_t1216387495, ___trainData_7)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_trainData_7() const { return ___trainData_7; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_trainData_7() { return &___trainData_7; }
	inline void set_trainData_7(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___trainData_7 = value;
		Il2CppCodeGenWriteBarrier((&___trainData_7), value);
	}

	inline static int32_t get_offset_of_testData_8() { return static_cast<int32_t>(offsetof(rbfTest_t1216387495, ___testData_8)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_testData_8() const { return ___testData_8; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_testData_8() { return &___testData_8; }
	inline void set_testData_8(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___testData_8 = value;
		Il2CppCodeGenWriteBarrier((&___testData_8), value);
	}

	inline static int32_t get_offset_of_rbfn_9() { return static_cast<int32_t>(offsetof(rbfTest_t1216387495, ___rbfn_9)); }
	inline RadialBasisFunctionNetwork_t3778331090 * get_rbfn_9() const { return ___rbfn_9; }
	inline RadialBasisFunctionNetwork_t3778331090 ** get_address_of_rbfn_9() { return &___rbfn_9; }
	inline void set_rbfn_9(RadialBasisFunctionNetwork_t3778331090 * value)
	{
		___rbfn_9 = value;
		Il2CppCodeGenWriteBarrier((&___rbfn_9), value);
	}

	inline static int32_t get_offset_of_rbfnTwo_10() { return static_cast<int32_t>(offsetof(rbfTest_t1216387495, ___rbfnTwo_10)); }
	inline RadialBasisFunctionNetwork_t3778331090 * get_rbfnTwo_10() const { return ___rbfnTwo_10; }
	inline RadialBasisFunctionNetwork_t3778331090 ** get_address_of_rbfnTwo_10() { return &___rbfnTwo_10; }
	inline void set_rbfnTwo_10(RadialBasisFunctionNetwork_t3778331090 * value)
	{
		___rbfnTwo_10 = value;
		Il2CppCodeGenWriteBarrier((&___rbfnTwo_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RBFTEST_T1216387495_H
#ifndef BUTTONOVERLAYPOSITION_T1621774379_H
#define BUTTONOVERLAYPOSITION_T1621774379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonOverlayPosition
struct  ButtonOverlayPosition_t1621774379  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean ButtonOverlayPosition::RightAngles
	bool ___RightAngles_2;

public:
	inline static int32_t get_offset_of_RightAngles_2() { return static_cast<int32_t>(offsetof(ButtonOverlayPosition_t1621774379, ___RightAngles_2)); }
	inline bool get_RightAngles_2() const { return ___RightAngles_2; }
	inline bool* get_address_of_RightAngles_2() { return &___RightAngles_2; }
	inline void set_RightAngles_2(bool value)
	{
		___RightAngles_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONOVERLAYPOSITION_T1621774379_H
#ifndef PERCEPTRONS_T1682201663_H
#define PERCEPTRONS_T1682201663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Perceptrons
struct  Perceptrons_t1682201663  : public MonoBehaviour_t3962482529
{
public:
	// System.Double[][] Perceptrons::trainData
	DoubleU5BU5DU5BU5D_t1159594487* ___trainData_2;

public:
	inline static int32_t get_offset_of_trainData_2() { return static_cast<int32_t>(offsetof(Perceptrons_t1682201663, ___trainData_2)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_trainData_2() const { return ___trainData_2; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_trainData_2() { return &___trainData_2; }
	inline void set_trainData_2(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___trainData_2 = value;
		Il2CppCodeGenWriteBarrier((&___trainData_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERCEPTRONS_T1682201663_H
#ifndef CAMPAIGNLOADSCENE_T453142025_H
#define CAMPAIGNLOADSCENE_T453142025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CampaignLoadScene
struct  CampaignLoadScene_t453142025  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CampaignLoadScene::SaveSlot1
	GameObject_t1113636619 * ___SaveSlot1_2;
	// UnityEngine.GameObject CampaignLoadScene::SaveSlot2
	GameObject_t1113636619 * ___SaveSlot2_3;
	// UnityEngine.GameObject CampaignLoadScene::SaveSlot3
	GameObject_t1113636619 * ___SaveSlot3_4;
	// UnityEngine.GameObject CampaignLoadScene::SaveSlot4
	GameObject_t1113636619 * ___SaveSlot4_5;
	// UnityEngine.GameObject[] CampaignLoadScene::saveSlots
	GameObjectU5BU5D_t3328599146* ___saveSlots_6;
	// LoadScene CampaignLoadScene::loadScene
	LoadScene_t3470671713 * ___loadScene_7;
	// UnityEngine.AudioSource CampaignLoadScene::clickAudio
	AudioSource_t3935305588 * ___clickAudio_8;

public:
	inline static int32_t get_offset_of_SaveSlot1_2() { return static_cast<int32_t>(offsetof(CampaignLoadScene_t453142025, ___SaveSlot1_2)); }
	inline GameObject_t1113636619 * get_SaveSlot1_2() const { return ___SaveSlot1_2; }
	inline GameObject_t1113636619 ** get_address_of_SaveSlot1_2() { return &___SaveSlot1_2; }
	inline void set_SaveSlot1_2(GameObject_t1113636619 * value)
	{
		___SaveSlot1_2 = value;
		Il2CppCodeGenWriteBarrier((&___SaveSlot1_2), value);
	}

	inline static int32_t get_offset_of_SaveSlot2_3() { return static_cast<int32_t>(offsetof(CampaignLoadScene_t453142025, ___SaveSlot2_3)); }
	inline GameObject_t1113636619 * get_SaveSlot2_3() const { return ___SaveSlot2_3; }
	inline GameObject_t1113636619 ** get_address_of_SaveSlot2_3() { return &___SaveSlot2_3; }
	inline void set_SaveSlot2_3(GameObject_t1113636619 * value)
	{
		___SaveSlot2_3 = value;
		Il2CppCodeGenWriteBarrier((&___SaveSlot2_3), value);
	}

	inline static int32_t get_offset_of_SaveSlot3_4() { return static_cast<int32_t>(offsetof(CampaignLoadScene_t453142025, ___SaveSlot3_4)); }
	inline GameObject_t1113636619 * get_SaveSlot3_4() const { return ___SaveSlot3_4; }
	inline GameObject_t1113636619 ** get_address_of_SaveSlot3_4() { return &___SaveSlot3_4; }
	inline void set_SaveSlot3_4(GameObject_t1113636619 * value)
	{
		___SaveSlot3_4 = value;
		Il2CppCodeGenWriteBarrier((&___SaveSlot3_4), value);
	}

	inline static int32_t get_offset_of_SaveSlot4_5() { return static_cast<int32_t>(offsetof(CampaignLoadScene_t453142025, ___SaveSlot4_5)); }
	inline GameObject_t1113636619 * get_SaveSlot4_5() const { return ___SaveSlot4_5; }
	inline GameObject_t1113636619 ** get_address_of_SaveSlot4_5() { return &___SaveSlot4_5; }
	inline void set_SaveSlot4_5(GameObject_t1113636619 * value)
	{
		___SaveSlot4_5 = value;
		Il2CppCodeGenWriteBarrier((&___SaveSlot4_5), value);
	}

	inline static int32_t get_offset_of_saveSlots_6() { return static_cast<int32_t>(offsetof(CampaignLoadScene_t453142025, ___saveSlots_6)); }
	inline GameObjectU5BU5D_t3328599146* get_saveSlots_6() const { return ___saveSlots_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_saveSlots_6() { return &___saveSlots_6; }
	inline void set_saveSlots_6(GameObjectU5BU5D_t3328599146* value)
	{
		___saveSlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___saveSlots_6), value);
	}

	inline static int32_t get_offset_of_loadScene_7() { return static_cast<int32_t>(offsetof(CampaignLoadScene_t453142025, ___loadScene_7)); }
	inline LoadScene_t3470671713 * get_loadScene_7() const { return ___loadScene_7; }
	inline LoadScene_t3470671713 ** get_address_of_loadScene_7() { return &___loadScene_7; }
	inline void set_loadScene_7(LoadScene_t3470671713 * value)
	{
		___loadScene_7 = value;
		Il2CppCodeGenWriteBarrier((&___loadScene_7), value);
	}

	inline static int32_t get_offset_of_clickAudio_8() { return static_cast<int32_t>(offsetof(CampaignLoadScene_t453142025, ___clickAudio_8)); }
	inline AudioSource_t3935305588 * get_clickAudio_8() const { return ___clickAudio_8; }
	inline AudioSource_t3935305588 ** get_address_of_clickAudio_8() { return &___clickAudio_8; }
	inline void set_clickAudio_8(AudioSource_t3935305588 * value)
	{
		___clickAudio_8 = value;
		Il2CppCodeGenWriteBarrier((&___clickAudio_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMPAIGNLOADSCENE_T453142025_H
#ifndef NORMALIZE_T1352299554_H
#define NORMALIZE_T1352299554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Normalize
struct  Normalize_t1352299554  : public MonoBehaviour_t3962482529
{
public:
	// System.String[] Normalize::sourceData
	StringU5BU5D_t1281789340* ___sourceData_2;
	// System.String[] Normalize::encodedData
	StringU5BU5D_t1281789340* ___encodedData_3;
	// System.Double[][] Normalize::numericData
	DoubleU5BU5DU5BU5D_t1159594487* ___numericData_4;

public:
	inline static int32_t get_offset_of_sourceData_2() { return static_cast<int32_t>(offsetof(Normalize_t1352299554, ___sourceData_2)); }
	inline StringU5BU5D_t1281789340* get_sourceData_2() const { return ___sourceData_2; }
	inline StringU5BU5D_t1281789340** get_address_of_sourceData_2() { return &___sourceData_2; }
	inline void set_sourceData_2(StringU5BU5D_t1281789340* value)
	{
		___sourceData_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceData_2), value);
	}

	inline static int32_t get_offset_of_encodedData_3() { return static_cast<int32_t>(offsetof(Normalize_t1352299554, ___encodedData_3)); }
	inline StringU5BU5D_t1281789340* get_encodedData_3() const { return ___encodedData_3; }
	inline StringU5BU5D_t1281789340** get_address_of_encodedData_3() { return &___encodedData_3; }
	inline void set_encodedData_3(StringU5BU5D_t1281789340* value)
	{
		___encodedData_3 = value;
		Il2CppCodeGenWriteBarrier((&___encodedData_3), value);
	}

	inline static int32_t get_offset_of_numericData_4() { return static_cast<int32_t>(offsetof(Normalize_t1352299554, ___numericData_4)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_numericData_4() const { return ___numericData_4; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_numericData_4() { return &___numericData_4; }
	inline void set_numericData_4(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___numericData_4 = value;
		Il2CppCodeGenWriteBarrier((&___numericData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALIZE_T1352299554_H
#ifndef CUSTOMIMAGEEFFECT_T473996489_H
#define CUSTOMIMAGEEFFECT_T473996489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomImageEffect
struct  CustomImageEffect_t473996489  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material CustomImageEffect::EffectMaterial
	Material_t340375123 * ___EffectMaterial_2;

public:
	inline static int32_t get_offset_of_EffectMaterial_2() { return static_cast<int32_t>(offsetof(CustomImageEffect_t473996489, ___EffectMaterial_2)); }
	inline Material_t340375123 * get_EffectMaterial_2() const { return ___EffectMaterial_2; }
	inline Material_t340375123 ** get_address_of_EffectMaterial_2() { return &___EffectMaterial_2; }
	inline void set_EffectMaterial_2(Material_t340375123 * value)
	{
		___EffectMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___EffectMaterial_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIMAGEEFFECT_T473996489_H
#ifndef FEEDFORWARDPROGRAM_T2707572273_H
#define FEEDFORWARDPROGRAM_T2707572273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FeedForwardProgram
struct  FeedForwardProgram_t2707572273  : public MonoBehaviour_t3962482529
{
public:
	// System.Double[] FeedForwardProgram::weights
	DoubleU5BU5D_t3413330114* ___weights_5;
	// System.Double[] FeedForwardProgram::inputValues
	DoubleU5BU5D_t3413330114* ___inputValues_6;

public:
	inline static int32_t get_offset_of_weights_5() { return static_cast<int32_t>(offsetof(FeedForwardProgram_t2707572273, ___weights_5)); }
	inline DoubleU5BU5D_t3413330114* get_weights_5() const { return ___weights_5; }
	inline DoubleU5BU5D_t3413330114** get_address_of_weights_5() { return &___weights_5; }
	inline void set_weights_5(DoubleU5BU5D_t3413330114* value)
	{
		___weights_5 = value;
		Il2CppCodeGenWriteBarrier((&___weights_5), value);
	}

	inline static int32_t get_offset_of_inputValues_6() { return static_cast<int32_t>(offsetof(FeedForwardProgram_t2707572273, ___inputValues_6)); }
	inline DoubleU5BU5D_t3413330114* get_inputValues_6() const { return ___inputValues_6; }
	inline DoubleU5BU5D_t3413330114** get_address_of_inputValues_6() { return &___inputValues_6; }
	inline void set_inputValues_6(DoubleU5BU5D_t3413330114* value)
	{
		___inputValues_6 = value;
		Il2CppCodeGenWriteBarrier((&___inputValues_6), value);
	}
};

struct FeedForwardProgram_t2707572273_StaticFields
{
public:
	// System.Func`3<System.String,System.Double,System.String> FeedForwardProgram::<>f__am$cache0
	Func_3_t220556092 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(FeedForwardProgram_t2707572273_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_3_t220556092 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_3_t220556092 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_3_t220556092 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEEDFORWARDPROGRAM_T2707572273_H
#ifndef BACKPROP_T3634268158_H
#define BACKPROP_T3634268158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackProp
struct  BackProp_t3634268158  : public MonoBehaviour_t3962482529
{
public:
	// System.Double[] BackProp::weights
	DoubleU5BU5D_t3413330114* ___weights_5;
	// System.Double[] BackProp::inputValues
	DoubleU5BU5D_t3413330114* ___inputValues_6;
	// System.Double[] BackProp::targetValues
	DoubleU5BU5D_t3413330114* ___targetValues_7;

public:
	inline static int32_t get_offset_of_weights_5() { return static_cast<int32_t>(offsetof(BackProp_t3634268158, ___weights_5)); }
	inline DoubleU5BU5D_t3413330114* get_weights_5() const { return ___weights_5; }
	inline DoubleU5BU5D_t3413330114** get_address_of_weights_5() { return &___weights_5; }
	inline void set_weights_5(DoubleU5BU5D_t3413330114* value)
	{
		___weights_5 = value;
		Il2CppCodeGenWriteBarrier((&___weights_5), value);
	}

	inline static int32_t get_offset_of_inputValues_6() { return static_cast<int32_t>(offsetof(BackProp_t3634268158, ___inputValues_6)); }
	inline DoubleU5BU5D_t3413330114* get_inputValues_6() const { return ___inputValues_6; }
	inline DoubleU5BU5D_t3413330114** get_address_of_inputValues_6() { return &___inputValues_6; }
	inline void set_inputValues_6(DoubleU5BU5D_t3413330114* value)
	{
		___inputValues_6 = value;
		Il2CppCodeGenWriteBarrier((&___inputValues_6), value);
	}

	inline static int32_t get_offset_of_targetValues_7() { return static_cast<int32_t>(offsetof(BackProp_t3634268158, ___targetValues_7)); }
	inline DoubleU5BU5D_t3413330114* get_targetValues_7() const { return ___targetValues_7; }
	inline DoubleU5BU5D_t3413330114** get_address_of_targetValues_7() { return &___targetValues_7; }
	inline void set_targetValues_7(DoubleU5BU5D_t3413330114* value)
	{
		___targetValues_7 = value;
		Il2CppCodeGenWriteBarrier((&___targetValues_7), value);
	}
};

struct BackProp_t3634268158_StaticFields
{
public:
	// System.Func`3<System.String,System.Double,System.String> BackProp::<>f__am$cache0
	Func_3_t220556092 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(BackProp_t3634268158_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Func_3_t220556092 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Func_3_t220556092 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Func_3_t220556092 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKPROP_T3634268158_H
#ifndef DESTROYPARENT_T190572050_H
#define DESTROYPARENT_T190572050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyParent
struct  DestroyParent_t190572050  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button DestroyParent::Button
	Button_t4055032469 * ___Button_2;
	// ZombieCard DestroyParent::Card
	ZombieCard_t2237653742 * ___Card_3;
	// UnityEngine.AudioSource DestroyParent::clickAudio
	AudioSource_t3935305588 * ___clickAudio_4;

public:
	inline static int32_t get_offset_of_Button_2() { return static_cast<int32_t>(offsetof(DestroyParent_t190572050, ___Button_2)); }
	inline Button_t4055032469 * get_Button_2() const { return ___Button_2; }
	inline Button_t4055032469 ** get_address_of_Button_2() { return &___Button_2; }
	inline void set_Button_2(Button_t4055032469 * value)
	{
		___Button_2 = value;
		Il2CppCodeGenWriteBarrier((&___Button_2), value);
	}

	inline static int32_t get_offset_of_Card_3() { return static_cast<int32_t>(offsetof(DestroyParent_t190572050, ___Card_3)); }
	inline ZombieCard_t2237653742 * get_Card_3() const { return ___Card_3; }
	inline ZombieCard_t2237653742 ** get_address_of_Card_3() { return &___Card_3; }
	inline void set_Card_3(ZombieCard_t2237653742 * value)
	{
		___Card_3 = value;
		Il2CppCodeGenWriteBarrier((&___Card_3), value);
	}

	inline static int32_t get_offset_of_clickAudio_4() { return static_cast<int32_t>(offsetof(DestroyParent_t190572050, ___clickAudio_4)); }
	inline AudioSource_t3935305588 * get_clickAudio_4() const { return ___clickAudio_4; }
	inline AudioSource_t3935305588 ** get_address_of_clickAudio_4() { return &___clickAudio_4; }
	inline void set_clickAudio_4(AudioSource_t3935305588 * value)
	{
		___clickAudio_4 = value;
		Il2CppCodeGenWriteBarrier((&___clickAudio_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYPARENT_T190572050_H
#ifndef LOADSCENE_T3470671713_H
#define LOADSCENE_T3470671713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadScene
struct  LoadScene_t3470671713  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LoadScene::LoadingScreen
	GameObject_t1113636619 * ___LoadingScreen_2;
	// UnityEngine.AudioSource LoadScene::clickAudio
	AudioSource_t3935305588 * ___clickAudio_3;

public:
	inline static int32_t get_offset_of_LoadingScreen_2() { return static_cast<int32_t>(offsetof(LoadScene_t3470671713, ___LoadingScreen_2)); }
	inline GameObject_t1113636619 * get_LoadingScreen_2() const { return ___LoadingScreen_2; }
	inline GameObject_t1113636619 ** get_address_of_LoadingScreen_2() { return &___LoadingScreen_2; }
	inline void set_LoadingScreen_2(GameObject_t1113636619 * value)
	{
		___LoadingScreen_2 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingScreen_2), value);
	}

	inline static int32_t get_offset_of_clickAudio_3() { return static_cast<int32_t>(offsetof(LoadScene_t3470671713, ___clickAudio_3)); }
	inline AudioSource_t3935305588 * get_clickAudio_3() const { return ___clickAudio_3; }
	inline AudioSource_t3935305588 ** get_address_of_clickAudio_3() { return &___clickAudio_3; }
	inline void set_clickAudio_3(AudioSource_t3935305588 * value)
	{
		___clickAudio_3 = value;
		Il2CppCodeGenWriteBarrier((&___clickAudio_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENE_T3470671713_H
#ifndef IOSHIDE_T3784820258_H
#define IOSHIDE_T3784820258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iOSHide
struct  iOSHide_t3784820258  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] iOSHide::AppleHideObjects
	GameObjectU5BU5D_t3328599146* ___AppleHideObjects_2;

public:
	inline static int32_t get_offset_of_AppleHideObjects_2() { return static_cast<int32_t>(offsetof(iOSHide_t3784820258, ___AppleHideObjects_2)); }
	inline GameObjectU5BU5D_t3328599146* get_AppleHideObjects_2() const { return ___AppleHideObjects_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_AppleHideObjects_2() { return &___AppleHideObjects_2; }
	inline void set_AppleHideObjects_2(GameObjectU5BU5D_t3328599146* value)
	{
		___AppleHideObjects_2 = value;
		Il2CppCodeGenWriteBarrier((&___AppleHideObjects_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSHIDE_T3784820258_H
#ifndef DOWNLOADNETWORKDATA_T3077572408_H
#define DOWNLOADNETWORKDATA_T3077572408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DownloadNetworkData
struct  DownloadNetworkData_t3077572408  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct DownloadNetworkData_t3077572408_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`3<CloudConnectorCore/QueryType,System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<System.String>> DownloadNetworkData::<>f__mg$cache0
	UnityAction_3_t1112121581 * ___U3CU3Ef__mgU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(DownloadNetworkData_t3077572408_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline UnityAction_3_t1112121581 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline UnityAction_3_t1112121581 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(UnityAction_3_t1112121581 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADNETWORKDATA_T3077572408_H
#ifndef RBFTESTXOR_T2349374253_H
#define RBFTESTXOR_T2349374253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// rbfTestXOR
struct  rbfTestXOR_t2349374253  : public MonoBehaviour_t3962482529
{
public:
	// System.Double[][] rbfTestXOR::xorData
	DoubleU5BU5DU5BU5D_t1159594487* ___xorData_2;
	// System.Double[][] rbfTestXOR::xorDataTwo
	DoubleU5BU5DU5BU5D_t1159594487* ___xorDataTwo_3;
	// RadialBasisFunctionNetwork rbfTestXOR::rbfn
	RadialBasisFunctionNetwork_t3778331090 * ___rbfn_4;
	// RadialBasisFunctionNetwork rbfTestXOR::rbfnTwo
	RadialBasisFunctionNetwork_t3778331090 * ___rbfnTwo_5;

public:
	inline static int32_t get_offset_of_xorData_2() { return static_cast<int32_t>(offsetof(rbfTestXOR_t2349374253, ___xorData_2)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_xorData_2() const { return ___xorData_2; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_xorData_2() { return &___xorData_2; }
	inline void set_xorData_2(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___xorData_2 = value;
		Il2CppCodeGenWriteBarrier((&___xorData_2), value);
	}

	inline static int32_t get_offset_of_xorDataTwo_3() { return static_cast<int32_t>(offsetof(rbfTestXOR_t2349374253, ___xorDataTwo_3)); }
	inline DoubleU5BU5DU5BU5D_t1159594487* get_xorDataTwo_3() const { return ___xorDataTwo_3; }
	inline DoubleU5BU5DU5BU5D_t1159594487** get_address_of_xorDataTwo_3() { return &___xorDataTwo_3; }
	inline void set_xorDataTwo_3(DoubleU5BU5DU5BU5D_t1159594487* value)
	{
		___xorDataTwo_3 = value;
		Il2CppCodeGenWriteBarrier((&___xorDataTwo_3), value);
	}

	inline static int32_t get_offset_of_rbfn_4() { return static_cast<int32_t>(offsetof(rbfTestXOR_t2349374253, ___rbfn_4)); }
	inline RadialBasisFunctionNetwork_t3778331090 * get_rbfn_4() const { return ___rbfn_4; }
	inline RadialBasisFunctionNetwork_t3778331090 ** get_address_of_rbfn_4() { return &___rbfn_4; }
	inline void set_rbfn_4(RadialBasisFunctionNetwork_t3778331090 * value)
	{
		___rbfn_4 = value;
		Il2CppCodeGenWriteBarrier((&___rbfn_4), value);
	}

	inline static int32_t get_offset_of_rbfnTwo_5() { return static_cast<int32_t>(offsetof(rbfTestXOR_t2349374253, ___rbfnTwo_5)); }
	inline RadialBasisFunctionNetwork_t3778331090 * get_rbfnTwo_5() const { return ___rbfnTwo_5; }
	inline RadialBasisFunctionNetwork_t3778331090 ** get_address_of_rbfnTwo_5() { return &___rbfnTwo_5; }
	inline void set_rbfnTwo_5(RadialBasisFunctionNetwork_t3778331090 * value)
	{
		___rbfnTwo_5 = value;
		Il2CppCodeGenWriteBarrier((&___rbfnTwo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RBFTESTXOR_T2349374253_H
#ifndef HEROCARDS_T2362710087_H
#define HEROCARDS_T2362710087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroCards
struct  HeroCards_t2362710087  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<HeroCard> HeroCards::heroCards
	List_1_t360954787 * ___heroCards_2;
	// BasicHeroCards HeroCards::basicHeroCards
	BasicHeroCards_t2570609810 * ___basicHeroCards_3;
	// AdvancedHeroCards HeroCards::advancedHeroCards
	AdvancedHeroCards_t368746982 * ___advancedHeroCards_4;
	// UnityEngine.UI.Text HeroCards::Title
	Text_t1901882714 * ___Title_5;
	// UnityEngine.UI.Text HeroCards::CardText
	Text_t1901882714 * ___CardText_6;
	// UnityEngine.GameObject HeroCards::TypeText
	GameObject_t1113636619 * ___TypeText_7;
	// UnityEngine.GameObject HeroCards::TypeTextList
	GameObject_t1113636619 * ___TypeTextList_8;

public:
	inline static int32_t get_offset_of_heroCards_2() { return static_cast<int32_t>(offsetof(HeroCards_t2362710087, ___heroCards_2)); }
	inline List_1_t360954787 * get_heroCards_2() const { return ___heroCards_2; }
	inline List_1_t360954787 ** get_address_of_heroCards_2() { return &___heroCards_2; }
	inline void set_heroCards_2(List_1_t360954787 * value)
	{
		___heroCards_2 = value;
		Il2CppCodeGenWriteBarrier((&___heroCards_2), value);
	}

	inline static int32_t get_offset_of_basicHeroCards_3() { return static_cast<int32_t>(offsetof(HeroCards_t2362710087, ___basicHeroCards_3)); }
	inline BasicHeroCards_t2570609810 * get_basicHeroCards_3() const { return ___basicHeroCards_3; }
	inline BasicHeroCards_t2570609810 ** get_address_of_basicHeroCards_3() { return &___basicHeroCards_3; }
	inline void set_basicHeroCards_3(BasicHeroCards_t2570609810 * value)
	{
		___basicHeroCards_3 = value;
		Il2CppCodeGenWriteBarrier((&___basicHeroCards_3), value);
	}

	inline static int32_t get_offset_of_advancedHeroCards_4() { return static_cast<int32_t>(offsetof(HeroCards_t2362710087, ___advancedHeroCards_4)); }
	inline AdvancedHeroCards_t368746982 * get_advancedHeroCards_4() const { return ___advancedHeroCards_4; }
	inline AdvancedHeroCards_t368746982 ** get_address_of_advancedHeroCards_4() { return &___advancedHeroCards_4; }
	inline void set_advancedHeroCards_4(AdvancedHeroCards_t368746982 * value)
	{
		___advancedHeroCards_4 = value;
		Il2CppCodeGenWriteBarrier((&___advancedHeroCards_4), value);
	}

	inline static int32_t get_offset_of_Title_5() { return static_cast<int32_t>(offsetof(HeroCards_t2362710087, ___Title_5)); }
	inline Text_t1901882714 * get_Title_5() const { return ___Title_5; }
	inline Text_t1901882714 ** get_address_of_Title_5() { return &___Title_5; }
	inline void set_Title_5(Text_t1901882714 * value)
	{
		___Title_5 = value;
		Il2CppCodeGenWriteBarrier((&___Title_5), value);
	}

	inline static int32_t get_offset_of_CardText_6() { return static_cast<int32_t>(offsetof(HeroCards_t2362710087, ___CardText_6)); }
	inline Text_t1901882714 * get_CardText_6() const { return ___CardText_6; }
	inline Text_t1901882714 ** get_address_of_CardText_6() { return &___CardText_6; }
	inline void set_CardText_6(Text_t1901882714 * value)
	{
		___CardText_6 = value;
		Il2CppCodeGenWriteBarrier((&___CardText_6), value);
	}

	inline static int32_t get_offset_of_TypeText_7() { return static_cast<int32_t>(offsetof(HeroCards_t2362710087, ___TypeText_7)); }
	inline GameObject_t1113636619 * get_TypeText_7() const { return ___TypeText_7; }
	inline GameObject_t1113636619 ** get_address_of_TypeText_7() { return &___TypeText_7; }
	inline void set_TypeText_7(GameObject_t1113636619 * value)
	{
		___TypeText_7 = value;
		Il2CppCodeGenWriteBarrier((&___TypeText_7), value);
	}

	inline static int32_t get_offset_of_TypeTextList_8() { return static_cast<int32_t>(offsetof(HeroCards_t2362710087, ___TypeTextList_8)); }
	inline GameObject_t1113636619 * get_TypeTextList_8() const { return ___TypeTextList_8; }
	inline GameObject_t1113636619 ** get_address_of_TypeTextList_8() { return &___TypeTextList_8; }
	inline void set_TypeTextList_8(GameObject_t1113636619 * value)
	{
		___TypeTextList_8 = value;
		Il2CppCodeGenWriteBarrier((&___TypeTextList_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROCARDS_T2362710087_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (CallBackEventRaw_t2706945269), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (CallBackEventProcessed_t4150531253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (GSFU_Demo_Runtime_t606846259), -1, sizeof(GSFU_Demo_Runtime_t606846259_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2502[2] = 
{
	GSFU_Demo_Runtime_t606846259_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
	GSFU_Demo_Runtime_t606846259_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (GSFU_Demo_Utils_t2030822237), -1, sizeof(GSFU_Demo_Utils_t2030822237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2503[2] = 
{
	GSFU_Demo_Utils_t2030822237_StaticFields::get_offset_of_player_0(),
	GSFU_Demo_Utils_t2030822237_StaticFields::get_offset_of_tableName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (PlayerInfo_t3792932304)+ sizeof (RuntimeObject), sizeof(PlayerInfo_t3792932304_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2504[4] = 
{
	PlayerInfo_t3792932304::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlayerInfo_t3792932304::get_offset_of_level_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlayerInfo_t3792932304::get_offset_of_health_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlayerInfo_t3792932304::get_offset_of_role_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (GSFUJsonHelper_t2039638717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (Actions_t3679456191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (AdjustAudio_t1754716221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[7] = 
{
	AdjustAudio_t1754716221::get_offset_of_Mixer_2(),
	AdjustAudio_t1754716221::get_offset_of_SliderMusic_3(),
	AdjustAudio_t1754716221::get_offset_of_SliderSFX_4(),
	AdjustAudio_t1754716221::get_offset_of_AudioToggleButtonImage_5(),
	AdjustAudio_t1754716221::get_offset_of_TurnAudioOffSprite_6(),
	AdjustAudio_t1754716221::get_offset_of_TurnAudioOnSprite_7(),
	AdjustAudio_t1754716221::get_offset_of_clickAudio_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (ARControl_t2334390846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[6] = 
{
	ARControl_t2334390846::get_offset_of_GameCamera_2(),
	ARControl_t2334390846::get_offset_of_ARCamera_3(),
	ARControl_t2334390846::get_offset_of_LogCamera_4(),
	ARControl_t2334390846::get_offset_of_ARHideObjects_5(),
	ARControl_t2334390846::get_offset_of_isARon_6(),
	ARControl_t2334390846::get_offset_of_AutoStart_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (ARTrackableEventHandler_t2777929902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[2] = 
{
	ARTrackableEventHandler_t2777929902::get_offset_of_mTrackableBehaviour_2(),
	ARTrackableEventHandler_t2777929902::get_offset_of_CardDetails_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (AutoSave_t3127297172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (AutoSaveContinue_t69711145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (ButtonDamageOverlay_t2640942607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[2] = 
{
	ButtonDamageOverlay_t2640942607::get_offset_of_Intensity_2(),
	ButtonDamageOverlay_t2640942607::get_offset_of_damageMaterial_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (ButtonOverlayPosition_t1621774379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[1] = 
{
	ButtonOverlayPosition_t1621774379::get_offset_of_RightAngles_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (CampaignLoadScene_t453142025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[7] = 
{
	CampaignLoadScene_t453142025::get_offset_of_SaveSlot1_2(),
	CampaignLoadScene_t453142025::get_offset_of_SaveSlot2_3(),
	CampaignLoadScene_t453142025::get_offset_of_SaveSlot3_4(),
	CampaignLoadScene_t453142025::get_offset_of_SaveSlot4_5(),
	CampaignLoadScene_t453142025::get_offset_of_saveSlots_6(),
	CampaignLoadScene_t453142025::get_offset_of_loadScene_7(),
	CampaignLoadScene_t453142025::get_offset_of_clickAudio_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (U3CUISaveFileExistU3Ec__AnonStorey0_t3461180196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[2] = 
{
	U3CUISaveFileExistU3Ec__AnonStorey0_t3461180196::get_offset_of_saveName_0(),
	U3CUISaveFileExistU3Ec__AnonStorey0_t3461180196::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (U3CUISaveFileNotExistU3Ec__AnonStorey1_t2060043828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[2] = 
{
	U3CUISaveFileNotExistU3Ec__AnonStorey1_t2060043828::get_offset_of_saveName_0(),
	U3CUISaveFileNotExistU3Ec__AnonStorey1_t2060043828::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (CampaignManager_t2990572144), -1, sizeof(CampaignManager_t2990572144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2518[11] = 
{
	CampaignManager_t2990572144_StaticFields::get_offset_of_CampaignMissions_0(),
	CampaignManager_t2990572144_StaticFields::get_offset_of_heroesDiedInCampaign_1(),
	CampaignManager_t2990572144::get_offset_of_zombieCampaignObjective_2(),
	CampaignManager_t2990572144::get_offset_of_heroCampaignItems_3(),
	0,
	CampaignManager_t2990572144_StaticFields::get_offset_of_currentMissionIndex_5(),
	CampaignManager_t2990572144_StaticFields::get_offset_of_heroDeathCount_6(),
	CampaignManager_t2990572144_StaticFields::get_offset_of_campaignState_7(),
	CampaignManager_t2990572144_StaticFields::get_offset_of_lastCampaignFile_8(),
	CampaignManager_t2990572144_StaticFields::get_offset_of_campaignOver_9(),
	CampaignManager_t2990572144_StaticFields::get_offset_of_heroesWon_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (CampaignState_t3637050575)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[2] = 
{
	CampaignState_t3637050575::get_offset_of_MissionIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CampaignState_t3637050575::get_offset_of_HeroesDied_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (CampaignStatus_t3636067551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[2] = 
{
	CampaignStatus_t3636067551::get_offset_of_CampaignManager_2(),
	CampaignStatus_t3636067551::get_offset_of_loadScene_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (CustomImageEffect_t473996489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[1] = 
{
	CustomImageEffect_t473996489::get_offset_of_EffectMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (DestroyParent_t190572050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[3] = 
{
	DestroyParent_t190572050::get_offset_of_Button_2(),
	DestroyParent_t190572050::get_offset_of_Card_3(),
	DestroyParent_t190572050::get_offset_of_clickAudio_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (DownloadNetworkData_t3077572408), -1, sizeof(DownloadNetworkData_t3077572408_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2523[1] = 
{
	DownloadNetworkData_t3077572408_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (ExpansionManager_t4180898005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (FileIo_t1459473183), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (GameData_t415813024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[5] = 
{
	GameData_t415813024::get_offset_of_gameSessionName_0(),
	GameData_t415813024::get_offset_of_mission_1(),
	GameData_t415813024::get_offset_of_buildings_2(),
	GameData_t415813024::get_offset_of_roundData_3(),
	GameData_t415813024::get_offset_of_U3CGameWonU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (GameLog_t2697247111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[14] = 
{
	GameLog_t2697247111::get_offset_of_Contents_2(),
	GameLog_t2697247111::get_offset_of_ContentsRender_3(),
	GameLog_t2697247111::get_offset_of_TextPrefab_4(),
	GameLog_t2697247111::get_offset_of_ImagePrefab_5(),
	GameLog_t2697247111::get_offset_of_LogCamera_6(),
	GameLog_t2697247111::get_offset_of_logRenderTexture_7(),
	GameLog_t2697247111::get_offset_of_grab_8(),
	GameLog_t2697247111::get_offset_of_dupContent_9(),
	GameLog_t2697247111::get_offset_of_textObjects_10(),
	GameLog_t2697247111::get_offset_of_imageObjects_11(),
	GameLog_t2697247111::get_offset_of_GameLogSave_12(),
	0,
	0,
	GameLog_t2697247111::get_offset_of_imageHeight_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (GameSaveData_t3577483891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[18] = 
{
	GameSaveData_t3577483891::get_offset_of_HandDeck_0(),
	GameSaveData_t3577483891::get_offset_of_DiscardDeck_1(),
	GameSaveData_t3577483891::get_offset_of_RemainsDeck_2(),
	GameSaveData_t3577483891::get_offset_of_ObjectivesStates_3(),
	GameSaveData_t3577483891::get_offset_of_ZombiesOnBoard_4(),
	GameSaveData_t3577483891::get_offset_of_Spawn_5(),
	GameSaveData_t3577483891::get_offset_of_ZombiesWinOnTie_6(),
	GameSaveData_t3577483891::get_offset_of_ZombieMovesAfterSpawn_7(),
	GameSaveData_t3577483891::get_offset_of_TheHungryOne_8(),
	GameSaveData_t3577483891::get_offset_of_FightCardUsed_9(),
	GameSaveData_t3577483891::get_offset_of_Round_10(),
	GameSaveData_t3577483891::get_offset_of_HeroesDiedCount_11(),
	GameSaveData_t3577483891::get_offset_of_NumberOfHeroesInGame_12(),
	GameSaveData_t3577483891::get_offset_of_GameLog_13(),
	GameSaveData_t3577483891::get_offset_of_CardsPlayed_14(),
	GameSaveData_t3577483891::get_offset_of_GameData_15(),
	GameSaveData_t3577483891::get_offset_of_PhaseName_16(),
	GameSaveData_t3577483891::get_offset_of_ZombieCanBeKilled_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (RemainsInPlayCardDetails_t970086784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[3] = 
{
	RemainsInPlayCardDetails_t970086784::get_offset_of_Name_0(),
	RemainsInPlayCardDetails_t970086784::get_offset_of_RemainsInPlayName_1(),
	RemainsInPlayCardDetails_t970086784::get_offset_of_CardText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (GameLogEntries_t679889234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[6] = 
{
	GameLogEntries_t679889234::get_offset_of_LogType_0(),
	GameLogEntries_t679889234::get_offset_of_LogText_1(),
	GameLogEntries_t679889234::get_offset_of_LogTexture_2(),
	GameLogEntries_t679889234::get_offset_of_LogTextureFormat_3(),
	GameLogEntries_t679889234::get_offset_of_TextureWidth_4(),
	GameLogEntries_t679889234::get_offset_of_TextureHeight_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (Type_t2881192505)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2531[3] = 
{
	Type_t2881192505::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (GameSession_t4087811243), -1, sizeof(GameSession_t4087811243_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2532[9] = 
{
	GameSession_t4087811243::get_offset_of_missionOptions_0(),
	GameSession_t4087811243::get_offset_of_currentMission_1(),
	GameSession_t4087811243::get_offset_of_mapTiles_2(),
	GameSession_t4087811243::get_offset_of_heroCharacters_3(),
	GameSession_t4087811243::get_offset_of_zombies_4(),
	GameSession_t4087811243::get_offset_of_rbfSession_5(),
	GameSession_t4087811243::get_offset_of_currentGameDifficulty_6(),
	GameSession_t4087811243::get_offset_of_networkCreated_7(),
	GameSession_t4087811243_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (GameSettings_t2345380323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[30] = 
{
	GameSettings_t2345380323::get_offset_of_LoadScene_2(),
	GameSettings_t2345380323::get_offset_of_CoreHeroes_3(),
	GameSettings_t2345380323::get_offset_of_GHHeroes_4(),
	GameSettings_t2345380323::get_offset_of_GHSpecial_5(),
	GameSettings_t2345380323::get_offset_of_HP1Heroes_6(),
	GameSettings_t2345380323::get_offset_of_TPHeroes_7(),
	GameSettings_t2345380323::get_offset_of_BFHeroes_8(),
	GameSettings_t2345380323::get_offset_of_AEHeroes_9(),
	GameSettings_t2345380323::get_offset_of_HP2Heroes_10(),
	GameSettings_t2345380323::get_offset_of_expansionsWithHeroes_11(),
	GameSettings_t2345380323::get_offset_of_heroObjects_12(),
	GameSettings_t2345380323::get_offset_of_HeroContent_13(),
	GameSettings_t2345380323::get_offset_of_heroContentHeight_14(),
	GameSettings_t2345380323::get_offset_of_numPlayers_15(),
	GameSettings_t2345380323::get_offset_of_GameType_16(),
	GameSettings_t2345380323::get_offset_of_scenario_17(),
	GameSettings_t2345380323::get_offset_of_coreHeroOptions_18(),
	GameSettings_t2345380323::get_offset_of_selectedHeroOptions_19(),
	GameSettings_t2345380323::get_offset_of_isWellStockedBuildings_20(),
	GameSettings_t2345380323::get_offset_of_isHeroesReplenish_21(),
	GameSettings_t2345380323::get_offset_of_isHeroStartingCards1_22(),
	GameSettings_t2345380323::get_offset_of_isHeroStartingCards2_23(),
	GameSettings_t2345380323::get_offset_of_isFreeSearchMarkers_24(),
	GameSettings_t2345380323::get_offset_of_isGraveDeadZombiesInGame_25(),
	GameSettings_t2345380323::get_offset_of_isZombieAutoSpawn_26(),
	GameSettings_t2345380323::get_offset_of_isZombieHorde21_27(),
	GameSettings_t2345380323::get_offset_of_clickAudio_28(),
	GameSettings_t2345380323::get_offset_of_ScenarioList_29(),
	GameSettings_t2345380323::get_offset_of_SelectedHeroes_30(),
	GameSettings_t2345380323::get_offset_of_SpecialRules_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (GsfuUtilsLnoe_t3057049897), -1, sizeof(GsfuUtilsLnoe_t3057049897_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2534[4] = 
{
	GsfuUtilsLnoe_t3057049897_StaticFields::get_offset_of_TrainData_0(),
	GsfuUtilsLnoe_t3057049897_StaticFields::get_offset_of_NetworkSettingsLength_1(),
	GsfuUtilsLnoe_t3057049897_StaticFields::get_offset_of_gameLog_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (GameLog_t3659206319)+ sizeof (RuntimeObject), sizeof(GameLog_t3659206319_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2535[12] = 
{
	GameLog_t3659206319::get_offset_of_GameSessionName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameLog_t3659206319::get_offset_of_Mission_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameLog_t3659206319::get_offset_of_Buildings_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameLog_t3659206319::get_offset_of_Heroes_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameLog_t3659206319::get_offset_of_RoundsLeft_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameLog_t3659206319::get_offset_of_ZombiesDead_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameLog_t3659206319::get_offset_of_ZombiesOnBoard_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameLog_t3659206319::get_offset_of_HeroesDead_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameLog_t3659206319::get_offset_of_BuildingsTakenOver_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameLog_t3659206319::get_offset_of_BuildingsLightsOut_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameLog_t3659206319::get_offset_of_SpawningPits_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GameLog_t3659206319::get_offset_of_GameWon_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (NetworkSettings_t4022558215)+ sizeof (RuntimeObject), sizeof(NetworkSettings_t4022558215_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2536[9] = 
{
	NetworkSettings_t4022558215::get_offset_of_NumInputs_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkSettings_t4022558215::get_offset_of_NumRbfNodes_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkSettings_t4022558215::get_offset_of_NumOutputs_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkSettings_t4022558215::get_offset_of_Centers_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkSettings_t4022558215::get_offset_of_Width_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkSettings_t4022558215::get_offset_of_Weights_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkSettings_t4022558215::get_offset_of_Bias_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkSettings_t4022558215::get_offset_of_InputRangeLow_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkSettings_t4022558215::get_offset_of_InputRangeHigh_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (HeroCards_t2362710087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[7] = 
{
	HeroCards_t2362710087::get_offset_of_heroCards_2(),
	HeroCards_t2362710087::get_offset_of_basicHeroCards_3(),
	HeroCards_t2362710087::get_offset_of_advancedHeroCards_4(),
	HeroCards_t2362710087::get_offset_of_Title_5(),
	HeroCards_t2362710087::get_offset_of_CardText_6(),
	HeroCards_t2362710087::get_offset_of_TypeText_7(),
	HeroCards_t2362710087::get_offset_of_TypeTextList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (HeroCard_t3183847341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[4] = 
{
	HeroCard_t3183847341::get_offset_of_ReferenceName_0(),
	HeroCard_t3183847341::get_offset_of_Name_1(),
	HeroCard_t3183847341::get_offset_of_CardTypes_2(),
	HeroCard_t3183847341::get_offset_of_Errata_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (HeroCardTypes_t1515449340)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2539[16] = 
{
	HeroCardTypes_t1515449340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (BasicHeroCards_t2570609810), -1, sizeof(BasicHeroCards_t2570609810_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2540[26] = 
{
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_Ammo_0(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_AtLast_1(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_BaseballBat_2(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_Chainsaw_3(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_Crowbar_4(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_DeputyTaylor_5(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_DocBrody_6(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_Faith_7(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_FarmerSty_8(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_FireAxe_9(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_FirstAid_10(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_GetBackYouDevils_11(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_Jeb_12(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_JustAScratch_13(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_Keys_14(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_MeatCleaver_15(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_MrHyde_16(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_Pitchfork_17(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_PrincipalGomez_18(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_PumpShotgun_19(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_Recovery_20(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_Revolver_21(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_SignalFlare_22(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_Torch_23(),
	BasicHeroCards_t2570609810_StaticFields::get_offset_of_WeldingTorch_24(),
	BasicHeroCards_t2570609810::get_offset_of_BasicCards_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (AdvancedHeroCards_t368746982), -1, sizeof(AdvancedHeroCards_t368746982_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2541[11] = 
{
	AdvancedHeroCards_t368746982_StaticFields::get_offset_of_BlockedWindows_0(),
	AdvancedHeroCards_t368746982_StaticFields::get_offset_of_Dynamite_1(),
	AdvancedHeroCards_t368746982_StaticFields::get_offset_of_EscapeThroughTheWindows_2(),
	AdvancedHeroCards_t368746982_StaticFields::get_offset_of_FireExtinguisher_3(),
	AdvancedHeroCards_t368746982_StaticFields::get_offset_of_Gasoline_4(),
	AdvancedHeroCards_t368746982_StaticFields::get_offset_of_HeroicResolve_5(),
	AdvancedHeroCards_t368746982_StaticFields::get_offset_of_HerosRage_6(),
	AdvancedHeroCards_t368746982_StaticFields::get_offset_of_JustWhatINeeded_7(),
	AdvancedHeroCards_t368746982_StaticFields::get_offset_of_Lighter_8(),
	AdvancedHeroCards_t368746982_StaticFields::get_offset_of_OldBetsy_9(),
	AdvancedHeroCards_t368746982::get_offset_of_AdvancedCards_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (HeroDeadScreen_t3673982372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[12] = 
{
	HeroDeadScreen_t3673982372::get_offset_of_Option1_2(),
	HeroDeadScreen_t3673982372::get_offset_of_Option2_3(),
	HeroDeadScreen_t3673982372::get_offset_of_Option3_4(),
	HeroDeadScreen_t3673982372::get_offset_of_Option4_5(),
	HeroDeadScreen_t3673982372::get_offset_of_TextInstructions_6(),
	HeroDeadScreen_t3673982372::get_offset_of_WhatHeroPanel_7(),
	HeroDeadScreen_t3673982372::get_offset_of_LastHeroPanel_8(),
	HeroDeadScreen_t3673982372::get_offset_of_ContinuePanel_9(),
	HeroDeadScreen_t3673982372::get_offset_of_zombieTurn_10(),
	HeroDeadScreen_t3673982372::get_offset_of_heroesInGame_11(),
	HeroDeadScreen_t3673982372::get_offset_of_deadHero_12(),
	HeroDeadScreen_t3673982372::get_offset_of_clickAudio_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (Heroes_t1064386291), -1, sizeof(Heroes_t1064386291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2543[11] = 
{
	Heroes_t1064386291::get_offset_of_heroTargetPriority_0(),
	Heroes_t1064386291::get_offset_of_coreHeroes_1(),
	Heroes_t1064386291::get_offset_of_growingHungerHeroes_2(),
	Heroes_t1064386291::get_offset_of_heroPackOneHeroes_3(),
	Heroes_t1064386291::get_offset_of_timberPeakHeroes_4(),
	Heroes_t1064386291::get_offset_of_bloodInTheForestHeroes_5(),
	Heroes_t1064386291::get_offset_of_tenthAnniversaryEditionHeroes_6(),
	Heroes_t1064386291::get_offset_of_heroPackTwoHeroes_7(),
	Heroes_t1064386291::get_offset_of_selectableHeroes_8(),
	Heroes_t1064386291::get_offset_of_GameHeroes_9(),
	Heroes_t1064386291_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (U3CHeroesInTheGameU3Ec__AnonStorey1_t809524509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[1] = 
{
	U3CHeroesInTheGameU3Ec__AnonStorey1_t809524509::get_offset_of_ppHeroKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (U3CHeroesInTheGameU3Ec__AnonStorey0_t809590045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[1] = 
{
	U3CHeroesInTheGameU3Ec__AnonStorey0_t809590045::get_offset_of_deadHero_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (U3CHeroesInTheGameU3Ec__AnonStorey2_t809721117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[2] = 
{
	U3CHeroesInTheGameU3Ec__AnonStorey2_t809721117::get_offset_of_i_0(),
	U3CHeroesInTheGameU3Ec__AnonStorey2_t809721117::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (U3CRandomHeroWithKeywordU3Ec__AnonStorey3_t371716733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[1] = 
{
	U3CRandomHeroWithKeywordU3Ec__AnonStorey3_t371716733::get_offset_of_keyword_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (U3CRandomHeroWithKeywordU3Ec__AnonStorey4_t3092705917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2548[2] = 
{
	U3CRandomHeroWithKeywordU3Ec__AnonStorey4_t3092705917::get_offset_of_hero_0(),
	U3CRandomHeroWithKeywordU3Ec__AnonStorey4_t3092705917::get_offset_of_U3CU3Ef__refU243_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (Hero_t2261352770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[8] = 
{
	Hero_t2261352770::get_offset_of_Name_0(),
	Hero_t2261352770::get_offset_of_Title_1(),
	Hero_t2261352770::get_offset_of_Keyword_2(),
	Hero_t2261352770::get_offset_of_Gender_3(),
	Hero_t2261352770::get_offset_of_StartLocation_4(),
	Hero_t2261352770::get_offset_of_FlavorText_5(),
	Hero_t2261352770::get_offset_of_Health_6(),
	Hero_t2261352770::get_offset_of_StartItem_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (GenderType_t1790903313)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2550[3] = 
{
	GenderType_t1790903313::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (KeywordType_t3224742114)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2551[14] = 
{
	KeywordType_t3224742114::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (Becky_t970128288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (Billy_t1030081379), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (FatherJoseph_t936646166), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (JakeCartwright_t2726891737), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (Jenny_t1784592017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (Johnny_t2600401634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (Sally_t1012780404), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (SheriffAnderson_t3506115239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (Amanda_t1400640352), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (Kenny_t1784591794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (Sam_t2348450800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (Rachelle_t2663978534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (Jade_t2989863974), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (MrGoddard_t854555955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (Stacy_t2609714299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (Victor_t190053894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (Alice_t2236182153), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (EdBaker_t1130674762), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (JakeCartwrightSurvivor_t432754528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (Nikki_t639850332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (SallySurvivor_t524194474), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (SheriffAndersonSurvivor_t2620316193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (AgentCarter_t1285216719), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (SisterOphelia_t2122755205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (DeputyTaylor_t3698009909), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (DocBrody_t2824236715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (Jeb_t3107703536), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (MrHyde_t3080480473), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (Angela_t1405538042), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (Bear_t582637133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (DrYamato_t1382649438), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (Maria_t2214739604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (iOSHide_t3784820258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[1] = 
{
	iOSHide_t3784820258::get_offset_of_AppleHideObjects_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (LoadScene_t3470671713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[2] = 
{
	LoadScene_t3470671713::get_offset_of_LoadingScreen_2(),
	LoadScene_t3470671713::get_offset_of_clickAudio_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (U3CLoadingSceneU3Ec__Iterator0_t983482384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[5] = 
{
	U3CLoadingSceneU3Ec__Iterator0_t983482384::get_offset_of_scene_0(),
	U3CLoadingSceneU3Ec__Iterator0_t983482384::get_offset_of_U3CasyncU3E__0_1(),
	U3CLoadingSceneU3Ec__Iterator0_t983482384::get_offset_of_U24current_2(),
	U3CLoadingSceneU3Ec__Iterator0_t983482384::get_offset_of_U24disposing_3(),
	U3CLoadingSceneU3Ec__Iterator0_t983482384::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (BackProp_t3634268158), -1, sizeof(BackProp_t3634268158_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2587[10] = 
{
	0,
	0,
	0,
	BackProp_t3634268158::get_offset_of_weights_5(),
	BackProp_t3634268158::get_offset_of_inputValues_6(),
	BackProp_t3634268158::get_offset_of_targetValues_7(),
	0,
	0,
	0,
	BackProp_t3634268158_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (FeedForwardProgram_t2707572273), -1, sizeof(FeedForwardProgram_t2707572273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2588[6] = 
{
	0,
	0,
	0,
	FeedForwardProgram_t2707572273::get_offset_of_weights_5(),
	FeedForwardProgram_t2707572273::get_offset_of_inputValues_6(),
	FeedForwardProgram_t2707572273_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (U3CStartU3Ec__Iterator0_t1232402489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[6] = 
{
	U3CStartU3Ec__Iterator0_t1232402489::get_offset_of_U3CnnU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1232402489::get_offset_of_U3CoutputValuesU3E__0_1(),
	U3CStartU3Ec__Iterator0_t1232402489::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t1232402489::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t1232402489::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t1232402489::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (NeuralNetwork_t3692603454), -1, sizeof(NeuralNetwork_t3692603454_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2590[17] = 
{
	NeuralNetwork_t3692603454_StaticFields::get_offset_of_rnd_0(),
	NeuralNetwork_t3692603454::get_offset_of_numInput_1(),
	NeuralNetwork_t3692603454::get_offset_of_inputs_2(),
	NeuralNetwork_t3692603454::get_offset_of_inputToHiddenWeights_3(),
	NeuralNetwork_t3692603454::get_offset_of_inputToHiddenPrevWeightsDelta_4(),
	NeuralNetwork_t3692603454::get_offset_of_numHidden_5(),
	NeuralNetwork_t3692603454::get_offset_of_hiddenBiases_6(),
	NeuralNetwork_t3692603454::get_offset_of_hiddenToOutputWeights_7(),
	NeuralNetwork_t3692603454::get_offset_of_hiddenOutputs_8(),
	NeuralNetwork_t3692603454::get_offset_of_hiddenGradients_9(),
	NeuralNetwork_t3692603454::get_offset_of_hiddenPrevBiasesDelta_10(),
	NeuralNetwork_t3692603454::get_offset_of_hiddenToOutputPrevWeightDelta_11(),
	NeuralNetwork_t3692603454::get_offset_of_numOutput_12(),
	NeuralNetwork_t3692603454::get_offset_of_outputs_13(),
	NeuralNetwork_t3692603454::get_offset_of_outputBiases_14(),
	NeuralNetwork_t3692603454::get_offset_of_outputGradients_15(),
	NeuralNetwork_t3692603454::get_offset_of_outputPrevBiasesDelta_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (U3CSoftmaxU3Ec__AnonStorey0_t2838892455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2591[1] = 
{
	U3CSoftmaxU3Ec__AnonStorey0_t2838892455::get_offset_of_max_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (Normalize_t1352299554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[3] = 
{
	Normalize_t1352299554::get_offset_of_sourceData_2(),
	Normalize_t1352299554::get_offset_of_encodedData_3(),
	Normalize_t1352299554::get_offset_of_numericData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (U3CGaussNormalU3Ec__AnonStorey0_t3607787937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[2] = 
{
	U3CGaussNormalU3Ec__AnonStorey0_t3607787937::get_offset_of_column_0(),
	U3CGaussNormalU3Ec__AnonStorey0_t3607787937::get_offset_of_mean_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (Perceptrons_t1682201663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[4] = 
{
	Perceptrons_t1682201663::get_offset_of_trainData_2(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (Perceptron_t1002209887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[5] = 
{
	Perceptron_t1002209887::get_offset_of_numInput_0(),
	Perceptron_t1002209887::get_offset_of_inputs_1(),
	Perceptron_t1002209887::get_offset_of_weights_2(),
	Perceptron_t1002209887::get_offset_of_bias_3(),
	Perceptron_t1002209887::get_offset_of_rnd_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (rbfTest_t1216387495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[9] = 
{
	rbfTest_t1216387495::get_offset_of_numInputs_2(),
	rbfTest_t1216387495::get_offset_of_numRBFs_3(),
	rbfTest_t1216387495::get_offset_of_numOutputs_4(),
	rbfTest_t1216387495::get_offset_of_allDataIris_5(),
	rbfTest_t1216387495::get_offset_of_normalizedIrisdata_6(),
	rbfTest_t1216387495::get_offset_of_trainData_7(),
	rbfTest_t1216387495::get_offset_of_testData_8(),
	rbfTest_t1216387495::get_offset_of_rbfn_9(),
	rbfTest_t1216387495::get_offset_of_rbfnTwo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (rbfTestXOR_t2349374253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[4] = 
{
	rbfTestXOR_t2349374253::get_offset_of_xorData_2(),
	rbfTestXOR_t2349374253::get_offset_of_xorDataTwo_3(),
	rbfTestXOR_t2349374253::get_offset_of_rbfn_4(),
	rbfTestXOR_t2349374253::get_offset_of_rbfnTwo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (Training_t3060993675), -1, sizeof(Training_t3060993675_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2598[10] = 
{
	0,
	0,
	0,
	Training_t3060993675::get_offset_of_allDataIris_5(),
	0,
	0,
	0,
	Training_t3060993675::get_offset_of_trainData_9(),
	Training_t3060993675::get_offset_of_testData_10(),
	Training_t3060993675_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (xorTest_t823650901), -1, sizeof(xorTest_t823650901_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2599[10] = 
{
	0,
	0,
	0,
	xorTest_t823650901::get_offset_of_xorData_5(),
	0,
	0,
	0,
	xorTest_t823650901::get_offset_of_trainData_9(),
	xorTest_t823650901::get_offset_of_testData_10(),
	xorTest_t823650901_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
