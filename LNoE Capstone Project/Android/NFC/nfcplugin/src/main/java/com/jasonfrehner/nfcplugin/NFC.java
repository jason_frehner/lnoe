package com.jasonfrehner.nfcplugin;

import android.app.Activity;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcF;
import android.util.Log;
import java.io.IOException;
import java.util.Arrays;

import com.unity3d.player.UnityPlayer;

//https://developer.android.com/guide/topics/connectivity/nfc/advanced-nfc.html

public class NFC extends Fragment
{
    private static final String TAG = NFC.class.getSimpleName();

    private static boolean isNFCEnabled = false;


    public static void writeTag(String tagText) {
        Activity currentActivity = UnityPlayer.currentActivity;
        Tag tag = currentActivity.getIntent().getParcelableExtra("android.nfc.extra.TAG");

        // Convert String message
        NdefRecord[] record = new NdefRecord[] {
                NdefRecord.createTextRecord("UTF-8", tagText),
        };

        NdefMessage message = new NdefMessage(record);

        Log.d(TAG, "writeTag: " + tag);

        Ndef ndefTag = Ndef.get(tag); // Get the tag.
        Log.d(TAG, "writeTag: " + ndefTag);


        try
        {
            ndefTag.connect();
            // Write message to tag.
            ndefTag.writeNdefMessage(message);
        }catch (IOException | FormatException e)
        {
            e.printStackTrace();

        }finally
        {
            try
            {
                ndefTag.close();
            } catch (IOException e)
            {
                Log.e(TAG, "Error closing tag: ", e);
            }
        }
        removeTagFromIntent();
    }

    public static String readTag(Tag tag) {
        Ndef ndefTag = Ndef.get(tag); // Get the tag.


        if (ndefTag.getTag().getId() != null)
        {
            Log.d(TAG, "read tag: " + Arrays.toString(ndefTag.getTag().getId()));
            try
            {
                ndefTag.connect(); // Connect to the tag.

                // Convert the payload to a string.
                if (ndefTag.getNdefMessage() == null)
                {
                    return null;
                }
                byte[] payload = ndefTag.getNdefMessage().getRecords()[0].getPayload();
                return new String(payload, "UTF-8");

            } catch (IOException e)
            {
                Log.e(TAG, "IOException while reading message...", e);
            } catch (FormatException e)
            {
                e.printStackTrace();
            } finally
            {
                try
                {
                    ndefTag.close();
                } catch (IOException e)
                {
                    Log.e(TAG, "Error closing tag: ", e);
                }
//                removeTagFromIntent();
            }
            removeTagFromIntent();
        }
        return null;
    }

    public static void removeTagFromIntent () {
        Log.e(TAG, "clearing extra tag");
        Activity currentActivity = UnityPlayer.currentActivity;
        currentActivity.getIntent().removeExtra("android.nfc.extra.TAG");
    }

    public static void foreground () {
        Activity currentActivity = UnityPlayer.currentActivity;
        PendingIntent pendingIntent = PendingIntent.getActivity(currentActivity, 0, new Intent(currentActivity,
                currentActivity.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter[] intentFilter = new IntentFilter[] { new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED)};
        String[][] techList = new String[][] { new String[] {NfcF.class.getName()} };
        NfcAdapter adapter = NfcAdapter.getDefaultAdapter(currentActivity);

        if (adapter != null)
        {
            adapter.enableForegroundDispatch(currentActivity, pendingIntent, intentFilter, techList);
            isNFCEnabled = true;
        }
    }

    public static boolean nfcEnabled () {
        return isNFCEnabled;
    }

}
